<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('ledgers');
		$this->load->model('levels');
	}


	public function index() {
		redirect('account/add');
	}

	public function add() {

		unauth_secure();
		$data['modules'] = array('setup/account');
		
		$data['names'] = $this->accounts->getDistinctFields('name');
		$data['countries'] = $this->accounts->getDistinctFields('country');
		$data['typess'] = $this->accounts->getDistinctFields('etype');
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');

		$data['accounts'] = $this->accounts->fetchAll();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['page_title'] = 'Add Account Voucher';
		
		$this->load->view('template/header',$data);

		$this->load->view('setup/addaccount', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {
		
		$maxId = $this->accounts->getMaxId() + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}
	public function searchAccount()
	{
        $search = $_POST['search']; // trim(Input::get('search'));
        $type = $_POST['type']; ; //trim(Input::get('type'));

        $result = $this->accounts->searchAccount($search,$type);
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }
    public function savePls() {

    	if (isset($_POST)) {

    		$ledger = json_decode($_POST['ledger'], true);
    		$plsmain = json_decode($_POST['plsmain'], true);
    		$plspartner = json_decode($_POST['plspartner'], true);
    		$plscompany = json_decode($_POST['plscompany'], true);

    		$plsexpense = json_decode($_POST['plsexpense'], true);
    		$plsopeningstock = json_decode($_POST['plsopeningstock'], true);
    		$plsclosingstock = json_decode($_POST['plsclosingstock'], true);


    		$vrnoa = $_POST['vrnoa'];
    		$etype = $_POST['etype'];
    		$voucher_type_hidden = $_POST['voucher_type_hidden'];

    		$response = array();

    		$error = $this->accounts->isPlsAlreadySaved($plsmain['sdate'],$plsmain['edate'],$plsmain['vrnoa']);

    		if (!$error) {
    			$result = $this->accounts->savePls($plsmain, $plspartner,$plscompany,$plsexpense,$plsopeningstock,$plsclosingstock, $vrnoa, $etype);

    			$result = $this->ledgers->save($ledger, $result, $etype,$voucher_type_hidden);	

    			if ( $result === false ) {
    				$response['error'] = 'true';
    			} else {
    				$response = $result;
    			}

    		}else{
    			$response['error'] = 'Already Post';


    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }
    public function deletePls() {

    	if (isset( $_POST )) {

    		$vrnoa = $_POST['vrnoa'];
    		$etype = $_POST['etype'];
    		$company_id = $_POST['company_id'];
    		$result = $this->accounts->deletePls($vrnoa, $etype,$company_id);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }
    public function fetchPlsAll() {

    	if (isset( $_POST )) {
    		$response = array();

    		$vrnoa = $_POST['vrnoa'];
    		$result = $this->accounts->fetchPlsMain($vrnoa);

    		if ( $result === false ) {
    			$response['error'] = 'false';
    		} else {
    			$response['plsmain'] = $result;
    			$response['plspartner'] = $this->accounts->fetchPlsPartner($vrnoa);
    			$response['plscompany'] = $this->accounts->fetchPlsCompany($vrnoa);
    			$response['plsexpense'] = $this->accounts->fetchPlsExpense($vrnoa);
    			$response['plsopeningstock'] = $this->accounts->fetchPlsOpeningStock($vrnoa);
    			$response['plsclosingstock'] = $this->accounts->fetchPlsClosingStock($vrnoa);
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }
    public function searchAccountAll()
    {
        $search = $_POST['search']; // trim(Input::get('search'));
        $type = $_POST['type']; ; //trim(Input::get('type'));

        $result = $this->accounts->searchAccountAll($search,$type);
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }
    public function getAccountinfobyid()
    {
        $pid = $_POST['pid']; // trim(Input::get('search'));


        $result = $this->accounts->getAccountinfobyid($pid);
        
        $response = "";
        if($result === false) {
        	$response = 'false';
        } else {
        	$response = $result;
        }
        
        
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
    }

    public function getMaxChequeId() {

    	if (isset($_POST)) {

    		$etype = $_POST['etype'];
    		$company_id = $_POST['company_id'];

    		$maxId = $this->accounts->getMaxChequeId($etype,$company_id) + 1;
    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($maxId));
    	}

    }

    public function save() {

    	if (isset($_POST)) {

    		$accountDetail = $_POST['accountDetail'];
    		$accountDetail['account_id'] = $this->levels->genAccStr($accountDetail['level3']);
    		$result = $this->accounts->save( $accountDetail );

    		$response = array();
    		if ($result === false) {
    			$response['error'] = true;
    		} else {
    			$response['error'] = false;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function fetchAccount() {

    	if (isset( $_POST )) {

    		$pid = $_POST['pid'];
    		$result = $this->accounts->fetchAccount($pid);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function fetchAccountByName() {

    	if (isset( $_POST )) {

    		$name = $_POST['name'];
    		$result = $this->accounts->fetchAccountByName($name);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function fetchAll(){
    	$activee=(isset($_POST['active'])? $_POST['active']:-1);
    	$typee=(isset($_POST['typee'])? $_POST['typee']:'ALL');
    	$result = $this->accounts->fetchAll($activee, $typee);
    	$this->output
    	->set_content_type('application/json')
    	->set_output(json_encode($result));
    }



    public function getAllParties() {

    	$etype = '';
    	if (isset($_POST) && !empty($_POST)) {
    		$etype = $_POST['etype'];
    	}

    	$result = $this->accounts->getAllParties($etype);
    	$this->output
    	->set_content_type('application/json')
    	->set_output(json_encode($result));	
    }

    public function fetchPartyOpeningBalance()
    {
    	if (isset($_POST)) {			
    		$to = $_POST['to'];
    		$party_id = $_POST['pid'];

    		$result = $this->accounts->fetchPartyOpeningBalance( $to, $party_id );
    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($result));
    	}
    }

    public function fetchRunningTotal()
    {
    	if (isset($_POST)) {

    		$endDate = $_POST['to'];
    		$party_id = $_POST['pid'];

    		$result = $this->accounts->fetchRunningTotal($endDate, $party_id);
    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($result));
    	}	
    }

    public function getAccLedgerReport() {

    	if (isset( $_POST )) {

    		$from = $_POST['from'];
    		$to = $_POST['to'];
    		$pid = $_POST['pid'];
    		$brid = $_POST['brid'];
    		$result = $this->ledgers->getAccLedgerReport($from, $to, $pid,$brid);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function getChartOfAccounts()
    {
    	$coas = $this->accounts->getChartOfAccounts();
    	$json = json_encode($coas);

    	echo $json;
    }

    public function fetchClosingBalance()
    {
    	$to = $_POST['to'];

    	$result = $this->accounts->fetchClosingBalance( $to );

    	$json = json_encode($result);
    	echo $json;	
    }

	/////////////////////////////////////////////////////
	// Fetches overall opening balance of the COMPANY //
	/////////////////////////////////////////////////////
    public function fetchOpeningBalance()
    {
    	$to = $_POST['to'];

    	$result = $this->accounts->fetchOpeningBalance( $to );

    	$json = json_encode($result);
    	echo $json;
    }
}

/* End of file account.php */
/* Location: ./application/controllers/account.php */