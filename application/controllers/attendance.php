<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance extends CI_Controller {

	private $brid = '';
	public function __construct() {
		parent::__construct();
		$this->load->model('branches');
		$this->load->model('attendances');

		$this->brid = $this->session->userdata('brid');
	}

	public function index(){
		unauth_secure();
	}

	public function student() {

		unauth_secure();
		$data['modules'] = array('attendance/student');

		$data['page_title'] = 'Attendance Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('attendance/student', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {

		$result = $this->attendances->getMaxId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST)) {

			$date = $_POST['date'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$dcno = $_POST['dcno'];

			$saved = $this->attendances->isAttendanceAlreadySaved($date, $claid, $secid, $this->session->userdata('brid'), $dcno);

			if ($saved) {

				$response = 'duplicate';
			} else {

				$atndcs = objectToArray(json_decode($_POST['atndcs'], true));				

				$result = $this->attendances->save( $atndcs, $dcno, $this->brid );

				$response = array();
				if ($result === false) {
					$response['error'] = true;
				} else {
					$response['error'] = false;
				}
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];

			$results = $this->attendances->fetch($dcno, $this->brid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function studentAttendanceStatusWiseReport() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$stdid = $_POST['stdid'];
			$status = $_POST['status'];

			$result = $this->attendances->studentAttendanceStatusWiseReport($from, $to, $this->brid, $claid, $secid, $stdid, $status);

			$response = "";
			if ($result == false) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function studentAttendanceMonthWiseReport() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$stdid = $_POST['stdid'];

			$result = $this->attendances->studentAttendanceMonthWiseReport($from, $to, $this->brid, $claid, $secid, $stdid);

			$response = "";
			if ($result == false) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function monthlyAttendanceReport() {

		if (isset($_POST)) {

			$month = $_POST['month'];
			$year = $_POST['year'];
			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];

			$result = $this->attendances->monthlyAttendanceReport($month, $year, $this->brid, $claid, $secid);

			$response = "";
			if ($result == false) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function dailyAttendanceReport() {

		if (isset($_POST)) {

			$result = $this->attendances->dailyAttendanceReport($this->brid);

			$response = "";
			if ($result == false) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function postStaff() {

		if (isset($_POST)) {

			$data = objectToArray(json_decode($_POST['postData']));
			$vouchers = $data['vouchers'];

			$result = $this->attendances->post( $vouchers, $this->brid );

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
		}
	}

	public function fetchStaff() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];

			$results = $this->attendances->fetchStaff($dcno, $this->brid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteAttendance() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			
			$result = $this->attendances->deleteAttendance($dcno, $etype, $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function staff() {

		unauth_secure();
		$data['modules'] = array('attendance/staff');

		$data['page_title'] = 'Add Account Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('attendance/staff', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxStaffAtndId() {

		$result = $this->attendances->getMaxStaffAtndId($this->brid) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function saveStaff() {

		if (isset($_POST)) {

			$atndcs = $_POST['atndcs'];
			$dcno = $_POST['dcno'];

			$result = $this->attendances->saveStaff( $atndcs, $dcno, $this->brid );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function isVoucherAlreadySaved() {

		if (isset( $_POST )) {

			$date = $_POST['date'];
			$dcno = $_POST['dcno'];

			$result = $this->attendances->isVoucherAlreadySaved($date, $this->brid, $dcno);

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
		}
	}

	public function fetchStaffForTimeInOut() {

		if (isset( $_POST )) {

			$staid = $_POST['staid'];
			$result = $this->attendances->fetchStaffForTimeInOut($staid, $this->brid);

			$response = array();
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
}

/* End of file attendance.php */
/* Location: ./application/controllers/attendance.php */