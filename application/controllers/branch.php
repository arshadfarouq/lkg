<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('branches');
	}

	public function index() {
		redirect('branch/add');
	}

	public function add() {
		unauth_secure();
		$data['modules'] = array('branch');
		$data['branchNames'] = $this->branches->getAllBranches();
		$data['branches'] = $this->branches->fetchAll();

		$data['page_title'] = 'Add Branch ';
		$this->load->view('template/header',$data);

		$this->load->view('setup/addbranch', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {

		$maxId = $this->branches->getMaxId() + 1;

		// sets the json format for codeigneter
		$this->output
		->set_content_type('application/json')
		->set_output($maxId);
	}

	public function save() {

		if (isset($_POST)) {
			$branchDetail = $_POST['branchDetail'];

			$result = $this->branches->save( $branchDetail );
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$json = json_encode($response);
			$this->output
			->set_content_type('application/json')
			->set_output($json);
		}
	}

	public function validateField() {

		if (isset($_POST)) {

			$fieldValidate = array();
			$fieldValidate[$_POST['field']] = $_POST['value'];

			$result = $this->branches->validateField($fieldValidate);

			$response = array();
			if ($result === true) {
				$response['valid'] = 'true';
			} else {
				$response['valid'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function getAllBranches() {

		$result = $this->branches->getAllBranches();

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function fetchBranch() {

		if (isset($_POST)) {

			$brid = $_POST['brid'];

			$result = $this->branches->fetchBranch($brid);

			$response = array();
			if ($result === false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAll() {

		$result = $this->branches->fetchAll();

		$response = array();
		if ($result === false) {
			$response = 'false';
		} else {
			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}
}

/* End of file branch.php */
/* Location: ./application/controllers/branch.php */