<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charge extends CI_Controller {

	private $brid = '';

	public function __construct() {
		parent::__construct();
		$this->load->model('charges');
		$this->load->model('branches');
		$this->load->model('accounts');

		$this->brid = $this->session->userdata('brid');
	}

	public function index() {
		redirect('charge/add');
	}

	public function add() {

		unauth_secure();
		$data['modules'] = array('charge');
		$data['chargeTypes'] = $this->charges->getChargeTypes($this->brid);
		$data['charges'] = $this->charges->fetchAll($this->brid);
		$data['parties'] = $this->accounts->getAllParties('account', $this->brid);
		$data['page_title'] = 'Add Charge ';
		$this->load->view('template/header.php',$data);

		$this->load->view('setup/addcharges.php', $data);

		$this->load->view('template/mainnav.php');
		$this->load->view('template/footer.php', $data);
	}

	public function registrationVoucher() {
		
		unauth_secure();
		$data['modules'] = array('feevouchers/registrationvoucher');
		$data['registration_fee'] = $this->charges->fetchChargeByName('registration fee', $this->brid);
		$data['charges'] = $this->charges->fetchAll($this->brid);

		$data['page_title'] = 'Add Account ';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/registrationvoucher', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {

		$maxId = $this->charges->getMaxId($_POST['brid']) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxStudentChargesId() {

		$maxId = $this->charges->getMaxStudentChargesId($this->brid) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getChargeTypes() {

		$result = $this->charges->getChargeTypes();
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST)) {

			$chargeDetail = $_POST['chargeDetail'];

			$result = $this->charges->save( $chargeDetail );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveStudentChargesVoucher() {

		if (isset($_POST)) {

			$regObj = $_POST['regObj'];

			$result = $this->charges->saveStudentChargesVoucher( $regObj, $this->brid );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchCharge() {

		if (isset( $_POST )) {

			$chid = $_POST['chid'];
			$brid = $_POST['brid'];
			$result = $this->charges->fetchCharge($chid, $brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchChargeByName() {

		if (isset( $_POST )) {

			$description = $_POST['description'];
			$result = $this->charges->fetchChargeByName($description);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchStudentCharges() {

		if (isset( $_POST )) {

			$rid = $_POST['rid'];
			$result = $this->charges->fetchStudentCharges($rid, $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteStudentChargeVoucher() {

		if (isset($_POST)) {

			$result = $this->charges->deleteStudentChargeVoucher($_POST['rid'], $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAll() {

		$result = $this->charges->fetchAll();
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function chargesDefinitionReport() {
		
		$result = $this->charges->chargesDefinitionReport($this->brid);
		$response = array();
		$response['charges'] = $result;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}
}


/* End of file charges.php */
/* Location: ./application/controllers/charges.php */