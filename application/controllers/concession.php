<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Concession extends CI_Controller {

	private $brid = '';

	public function __construct(){
		parent::__construct();
		$this->load->model('concessions');
		$this->load->model('charges');
		$this->load->model('branches');
		$this->load->model('accounts');
		$this->load->model('ledgers');
		$this->load->model('students');
		$this->load->model('charges');

		$this->brid = $this->session->userdata('brid');
	}

	public function index() {
		unauth_secure();
		$data['modules'] = array('feevouchers/addfeeConcession');
		
		$data['charges'] = $this->charges->fetchAll($this->brid);
		$data['students'] = $this->students->fetchAll('', $this->brid);


		$data['page_title'] = 'Fee Concession Voucher';
		$this->load->view('template/header',$data);

		$this->load->view('feevouchers/addfeeConcession', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	


	public function getMaxId() {
		$etype = $_POST['etype'];
		$brid = $_POST['brid'];

		$maxId = $this->concessions->getMaxId($etype,$brid ) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxFeeAssignId() {

		$maxId = $this->concessions->getMaxFeeAssignId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxFeeRecId() {

		$maxId = $this->concessions->getMaxFeeRecId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxFeeIssuanceId() {

		if (isset($_POST)) {

			$etype = $_POST['etype'];
			$maxId = $this->concessions->getMaxFeeIssuanceId($etype, $this->brid) + 1;

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($maxId));
		}
	}

	public function getAllFeeCategories() {

		$result = $this->concessions->getAllFeeCategories();
		$this->output
		-> set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST)) {

			$ledgers = $_POST['ledgers'];
			$main = $_POST['main'];
			$detail = $_POST['detail'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			$brid = $_POST['brid'];


			$result = $this->concessions->save($main, $detail, $dcno, $brid,$etype);

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	public function searchConcession()
	{
        $search = $_POST['search']; // trim(Input::get('search'));
        $type = $_POST['type']; ; //trim(Input::get('type'));

        $result = $this->concessions->searchConcession($search,$type);
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }
	public function fetchAll_concessions() {

    	if (isset( $_POST )) {

    		$from = $_POST['from'];
    		$to = $_POST['to'];
    		$orderby = $_POST['orderby'];
    		// $status = $_POST['status'];
    		$crit='';
    		if(isset($_POST['crit']))
    			$crit = $_POST['crit'];

    		$result = $this->concessions->fetchAll_report($from,$to,$orderby,$crit);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}
    		$json = json_encode($response);
    		echo $json;
    	}
    }


	public function saveFeeIssuance() {

		if (isset($_POST)) {

			$ledgers = objectToArray(json_decode($_POST['ledgers']));
			$studs = objectToArray(json_decode($_POST['studs']));
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$resultData = $this->concessions->saveFeeIssuance($studs, $dcno, $this->brid);
			$result = $this->ledgers->save($ledgers, $dcno, $etype, $this->brid);

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $resultData;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveAdmissionCharges() {

		if (isset($_POST)) {

			$ledgers = $_POST['ledgers'];
			$main = $_POST['main'];
			$detail = $_POST['detail'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$result = $this->concessions->saveAdmissionCharges($main, $detail, $dcno, $this->brid);
			$result = $this->ledgers->save($ledgers, $dcno, $etype, $this->brid);


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveFeeRecieve() {

		if (isset($_POST)) {

			$ledgers = $_POST['ledgers'];
			$feerec = $_POST['feerec'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$result = $this->concessions->saveFeeRecieve($feerec, $dcno, $this->brid);
			$result = $this->ledgers->save($ledgers, $dcno, $etype, $this->brid);


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchFeeCategory() {

		if (isset( $_POST )) {

			$fcid = $_POST['fcid'];
			$brid = $_POST['brid'];

			$result = $this->concessions->fetchFeeCategory($fcid, $brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAll() {

		$result = $this->concessions->fetchAll();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {
			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

	public function saveAssignFeetoClass() {

		if (isset($_POST)) {

			$main = $_POST['main'];
			$detail = $_POST['detail'];
			$claid = $_POST['claid'];
			$fcid = $_POST['fcid'];
			$dcno = $_POST['dcno'];
			$brid = $this->brid;

			$error = $this->concessions->isFeeAlreadyAssignedToClass($dcno, $brid, $claid, $fcid);

			if (!$error) {

				$faid = $this->concessions->saveAssignFeetoClassMain($main, $this->brid);
				$result = $this->concessions->saveAssignFeetoClassDetail($detail, $faid, $this->brid);

				$response = array();
				if ( $result === false ) {
					$response['error'] = 'true';
				} else {
					$response['error'] = 'false';
				}

				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			} else {
				$response = array();
				$response['error'] = 'duplicate';
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			}
		}
	}

	

	public function deleteVoucher() {

		if (isset($_POST)) {
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			$brid = $_POST['brid'];
			
			$result = $this->concessions->deleteVoucher($dcno, $etype, $brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	

	
	public function fetch() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			$brid = $_POST['brid'];


			$charges = $this->concessions->fetch($dcno, $etype, $brid);

			$response = array();
			if ( $charges === false ) {
				$response = 'false';
			} else {
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	

	
}

/* End of file fee.php */
/* Location: ./application/controllers/fee.php */