<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doc extends CI_Controller 
{
  private $brid = '';

  function __construct()
  {
    parent::__construct();
    $this->load->model('charges');
    $this->load->model('branches');
    $this->load->model('accounts');
    
    $this->load->model('results');
    $this->load->model('attendances');
    $this->load->model('subjects');
    $this->load->model('students');
    $this->load->model('ledgers');
    $this->load->model('payments');
    $this->load->model('concessions');
    $this->load->model('fees');


    $this->brid = $this->session->userdata('brid');
  }

  public function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
      0                   => 'zero',
      1                   => 'one',
      2                   => 'two',
      3                   => 'three',
      4                   => 'four',
      5                   => 'five',
      6                   => 'six',
      7                   => 'seven',
      8                   => 'eight',
      9                   => 'nine',
      10                  => 'ten',
      11                  => 'eleven',
      12                  => 'twelve',
      13                  => 'thirteen',
      14                  => 'fourteen',
      15                  => 'fifteen',
      16                  => 'sixteen',
      17                  => 'seventeen',
      18                  => 'eighteen',
      19                  => 'nineteen',
      20                  => 'twenty',
      30                  => 'thirty',
      40                  => 'fourty',
      50                  => 'fifty',
      60                  => 'sixty',
      70                  => 'seventy',
      80                  => 'eighty',
      90                  => 'ninety',
      100                 => 'hundred',
      1000                => 'thousand',
      1000000             => 'million',
      1000000000          => 'billion',
      1000000000000       => 'trillion',
      1000000000000000    => 'quadrillion',
      1000000000000000000 => 'quintillion'
      );

    if (!is_numeric($number)) {
      return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
          // overflow
      trigger_error(
        'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
        E_USER_WARNING
        );
      return false;
    }

    if ($number < 0) {
      return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
      list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
      case $number < 21:
      $string = $dictionary[$number];
      break;
      case $number < 100:
      $tens   = ((int) ($number / 10)) * 10;
      $units  = $number % 10;
      $string = $dictionary[$tens];
      if ($units) {
        $string .= $hyphen . $dictionary[$units];
      }
      break;
      case $number < 1000:
      $hundreds  = $number / 100;
      $remainder = $number % 100;
      $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
      if ($remainder) {
        $string .= $conjunction . $this->convert_number_to_words($remainder);
      }
      break;
      default:
      $baseUnit = pow(1000, floor(log($number, 1000)));
      $numBaseUnits = (int) ($number / $baseUnit);
      $remainder = $number % $baseUnit;
      $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
      if ($remainder) {
        $string .= $remainder < 100 ? $conjunction : $separator;
        $string .= $this->convert_number_to_words($remainder);
      }
      break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
      $string .= $decimal;
      $words = array();
      foreach (str_split((string) $fraction) as $number) {
        $words[] = $dictionary[$number];
      }
      $string .= implode(' ', $words);
    }

    return $string;
  }
  public function Account_Flow( $dt1 , $dt2, $party_id , $company_id, $email_pdf = -1, $user_print )
  {
    $this->load->library('wkhtmltopdf');
    $data = array();

    $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];

    $pledger_1=$this->accounts->Account_Flow($dt1, $dt2, $party_id ,$company_id);
    $data['pledger'] = $pledger_1;
    $data['title'] = 'Account Flow Statement';
        // $data['etype'] = $etype;
    $data['user'] = $user_print;
    $date_between= (string)$dt1 . ' To '. (string)$dt2; 

    $data['date_between']=$date_between;

        // if (empty($data['pledger'])) {
        //     redirect('payment');
        // }



         // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
         // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/AccountFlowPdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
      ));

    if ( $email_pdf == 1 ) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs( $save_path );
      sleep(5);
      $this->send_mail( $save_path );
    } else {
      $this->wkhtmltopdf->send();
    } 
  }

  public function AccountFlow($dt1 , $dt2, $party_id , $company_id, $email_pdf = -1, $user_print)
  { 
    $this->load->library('wkhtmltopdf');
    $data = array();
    $balData = $this->accounts->fetchOpeningBalance_AccountsCashFlow($dt1, $party_id, $company_id);
    $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];
    $pledger_1 = $this->accounts->Account_Flow($dt1, $dt2, $party_id, $company_id);

    $data['pledger'] = $pledger_1;
    $data['title'] = 'Cash Flow Statement';
        // $data['etype'] = $etype;
    $data['user'] = $user_print;
    $date_between = (string)$dt1 . ' To ' . (string)$dt2;
    $data['date_between'] = $date_between;
    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/AccountFlowPdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait',
      'margin-left' => '5',
      'margin-right' => '5',
      'footer-spacing' => '3.0',
      'footer-center' => '[page]/[toPage]',
      'footer-font-size' => '9'
      ));

    $this->wkhtmltopdf->send();
  }
  public function JvVocuherPrintPdf( $etype, $vrnoa, $company_id, $email_pdf = -1, $user_print ,$pr=1)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();


    $data['pledger'] = $this->ledgers->fetch($vrnoa, $etype,$company_id );
    $data['title'] = 'Journal Voucher';
    $data['etype'] = $etype;
    $data['user'] = $user_print;



    if ($pr==1){
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));  
    }else{
      $data['header_img'] = '';  
    }


    $this->wkhtmltopdf->addPage($this->load->view('reportprints/JvVoucherPrintPdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait',
      'margin-top'=> 0,

      ));

    if ( $email_pdf == 1 ) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs( $save_path );
      sleep(5);
      $this->send_mail( $save_path );
    } else {
      $this->wkhtmltopdf->send();
    } 
  }
  public function CashVocuherPrintPdf( $etype, $vrnoa, $company_id, $email_pdf = -1, $user_print, $pr=1  ,$prnt='lg')
  {
    $this->load->library('wkhtmltopdf');

    $data = array();


    $data['pledger'] = $this->ledgers->fetch($vrnoa, $etype,$company_id );
    if ($etype =='cpv'){
      $data['title'] = 'Cash Payment Voucher';
    }
    else
    {
      $data['title'] = 'Cash Receipt Voucher';
    }
    $data['etype'] = $etype;
    $data['user'] = $user_print;

        // if (empty($data['pledger'])) {
        //     redirect('payment');
        // }

        // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

        // $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
        // $data['previousBalance'] = $balData[0]['RTotal'];

    if ($pr==1){
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));  
    }else{
      $data['header_img'] = '';  
    }

    if ($prnt=='sm'){
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/paymentsm', $data, true));
    }else{
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/CashVoucherPrintPdf', $data, true));
    }

    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait',
      'margin-top'=> 0,
      ));

    if ( $email_pdf == 1 ) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs( $save_path );
      sleep(5);
      $this->send_mail( $save_path );

    } else {
      $this->wkhtmltopdf->send();
    } 
  }
  public function pdf_doublecheque( $etype, $vrnoa, $company_id )
  {
    $this->load->library( 'wkhtmltopdf' );
    $data = array();

    if ( !$etype ) {
      redirect('user/dashboard');
    }

    $vr = $this->accounts->fetchChequeVoucher( $vrnoa, $etype, $company_id );
    $data['vrdetail'] = $vr[0];
    $data['title'] = ( $etype === 'pd_issue' ) ? 'Cheque Issue' : 'Cheque Received';

    if ( empty($data['vrdetail']) ) {
      redirect('user/dashboard');
    }

    $data['header_img'] = base_url('application/assets/img/header_imgs/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/doublecheque_pdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'landscape'
      ));
    // $this->wkhtmltopdf->send( strtolower($etype) . 'Cheque.pdf');
    $this->wkhtmltopdf->send();
  }

  public function pdf_singlecheque( $etype, $vrnoa, $company_id,$email_pdf, $user_print ,$hd=1)
  {
    $this->load->library( 'wkhtmltopdf' );
    $data = array();

    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }
    $vr=$this->accounts->fetchChequeVoucher( $vrnoa, $etype, $company_id );
    $data['pos_pd_cheque'] = $vr[0];
    $data['title'] = ( $etype === 'pd_issue' ) ? 'Cheque Issue' : 'Cheque Received';

    if ( empty($data['pos_pd_cheque']) ) {
      redirect('user/dashboard');
    }
    $data['user'] = $user_print;


    if ($hd==1){
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));  
    }else{
      $data['header_img'] = '';  
    }




    $this->wkhtmltopdf->addPage($this->load->view('reportprints/ChequePdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait',
      'margin-top'=>0,
      ));
    // $this->wkhtmltopdf->send( strtolower($etype) . 'Cheque.pdf');
    $this->wkhtmltopdf->send();
  }

  // Using WKHTML - Working Perfectly
  // public function pdf_singlevoucher( $etype, $email_pdf = -1,$from,$to)
  public function pdf_singlevoucher( $etype, $email_pdf = -1)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    if ( !$etype ) {
      redirect('user/dashboard');
    } else if ( $etype == 'chargedef' ) {

      $data['vrdetail'] = $this->charges->chargesDefinitionReport($this->brid);
      $data['title'] = 'Charge Definition';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['header_img'] = base_url('assets/img/pic1.png/' );
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_pdf', $data,true));
      $this->wkhtmltopdf->setOptions(array(
       'orientation' => 'portrait'
       ));

    }  else if ( $etype == 'purchasereturn') {
      $data['vrdetail'] = $this->purchasereturns->fetchVoucher( $vrnoa, $company_id );
      $data['title'] = 'Purchase Return';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];

    } else if ( $etype == 'purchaseorder') {
      $data['vrdetail'] = $this->purchases->fetchOrderVoucher( $vrnoa, $company_id );
      $data['title'] = 'Purchase Order';

      $etype = 'quotation';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];

    } else if ( $etype == 'salereturn') {
      $data['vrdetail'] = $this->salereturns->fetchVoucher( $vrnoa, $company_id );
      $data['title'] = 'Sale Return';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];

    } else if ( $etype == 'saleorder') {
      $data['vrdetail'] = $this->sales->fetchOrderVoucher( $vrnoa, $company_id );
      $data['title'] = 'Quotation';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];

    }
     /* $data['header_img'] = base_url('assets/img/pic1.png/' );
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_pdf', $data,true));
      $this->wkhtmltopdf->setOptions(array(
          'orientation' => 'portrait'
          ));*/
      // $this->wkhtmltopdf->send( strtolower($etype) . 'Voucher.pdf');
      //$this->wkhtmltopdf->send();
          if ( $email_pdf == 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        
         
      
        public function Print_FeeConcession(  $vrnoa,$etype, $brid, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

         
            $data['vrdetail'] = $this->concessions->fetch( $vrnoa, $etype, $brid);
            $data['title'] = 'Fee Concession Voucher';
            $data['from'] = $vrnoa;
            $data['to'] = $etype;
           

            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('print/feeconcession', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));
            

           // $this->load->view('print/feeconcession', $data);
            
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_monthlyFeeAssignReport( $etype,$from,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'monthlyFeeAssignReport' ) {

            $data['vrdetail'] = $this->fees->monthlyFeeAssignReport($from, $to, $this->brid);
            $data['title'] = 'Monthly Fee Assign Report';
            $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/monthlyFeeAssignReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_FeeAssign( $etype,$from,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'FeeAssign' ) {

            $data['vrdetail'] = $this->fees->feeAssignReport($from, $to, $this->brid);
            $data['title'] = 'Fee Assign';
            $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/FeeAssign_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_monthlyFeeAssignChargesWiseReport( $etype,$from,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');

          } else if ( $etype == 'monthlyFeeAssignReportChargesWise' ) {

            $data['vrdetail'] = $this->fees->monthlyFeeIssuanceReportChargesWise($from, $to, $this->brid);
            $data['title'] = 'Monthly Fee Charges Report';
            $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );

            $this->wkhtmltopdf->addPage($this->load->view('reportprints/monthlyFeeAssignChargesWiseReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));
            

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_defaultersReport( $etype,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');

          } else if ( $etype == 'defaultersReport' ) {

            $data['vrdetail'] = $this->fees->feeDefaulterReport($to, $this->brid);
            $data['title'] = 'Defaulters Report';
        // $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );

            $this->wkhtmltopdf->addPage($this->load->view('reportprints/defaultersReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_FeeAssignReport( $etype,$from,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');

          } else if ( $etype == 'FeeConcessionReport' ) {

            $data['vrdetail'] = $this->fees->feeConcessionReport($from, $to, $this->brid);
            $data['title'] = 'Fee Concession Report';
            $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );

            $this->wkhtmltopdf->addPage($this->load->view('reportprints/feeConcessionReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_feeReceiveStudentWiseReport( $etype,$from,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');

          } else if ( $etype == 'feeReceiveStudentWise' ) {

            $data['vrdetail'] =  $this->fees->feeRecStudentWiseReport($from, $to, $this->brid);
            $data['title'] = 'Fee Receive Student Wise Report';
            $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );

            $this->wkhtmltopdf->addPage($this->load->view('reportprints/pdf_feeReceiveStudentWiseReport',$data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_MonthlyfeeReceiveChargesWiseReport( $etype,$from,$to, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');

          } else if ( $etype == 'monthlyFeeReceiveChargesWiseReport' ) {

            $data['vrdetail'] = $this->fees->monthlyFeeRcvReportChargesWise($from, $to, $this->brid);
            $data['title'] = 'Monthly Fee Receive Charges Wise Report';
            $data['from'] = $from;
            $data['to'] = $to;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );

            $this->wkhtmltopdf->addPage($this->load->view('reportprints/feeReceiveChargeWiseReport_pdf',$data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_resultSubjectWise( $etype,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'resultSubjectWise_pdf' ) {

            $data['vrdetail'] = $this->results->resultSubjectWiseReport($this->brid);
            $data['title'] = 'Result (Subject Wise)';


            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/resultSubjectWise_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_studentResult( $etype,$brid,$claid,$secid,$session,$stdid,$term,$className,$sectionName,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'studentResult' ) {

            $data['vrdetail'] = $this->results->studentResultReport($this->brid, $claid, $secid, $session, $stdid, $term);
            $data['title'] = 'Student Result';
            $data['brid'] = $brid;
            $data['className'] = $className;
            $data['sectionName'] = $sectionName;
            $data['session'] = $session;
            $data['stdid'] = $stdid;
            $data['term'] = $term;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/studentResult_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_overAllSemResult( $etype,$brid,$claid,$secid,$session,$stdid,$className,$sectionName,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'overAllSemResult' ) {

            $data['vrdetail'] = $this->results->overallSemResultReport($this->brid, $claid, $secid, $session, $stdid);
            $data['title'] = 'Overall Semester Result';
            $data['brid'] = $brid;
            $data['className'] = $className;
            $data['sectionName'] = $sectionName;
            $data['session'] = $session;
            $data['stdid'] = $stdid;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/overallSemesterResult_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_studentResultTeacherWise( $etype,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'studentResultTeacherWise' ) {

            $data['vrdetail'] =  $this->results->stuResultTeacherWiseReport($this->brid);
            $data['title'] = 'Overall Semester Result';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/studentResultTeacherWise_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_studentattandanceStatusWise( $etype,$from,$to,$brid,$claid,$secid,$stdid,$status,$className,$sectionName,$statusName,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'studentAttandanceStatus' ) {

            $data['vrdetail'] = $this->attendances->studentAttendanceStatusWiseReport($from, $to, $this->brid, $claid, $secid, $stdid, $status);
            $data['title'] = 'Student Attendance Status Wise';
            $data['from'] = $from;
            $data['to'] = $to;
            $data['claid'] = $claid;
            $data['secid'] = $secid;
            $data['brid'] = $brid;
            $data['className'] = $className;
            $data['sectionName'] = $sectionName;
            $data['status'] = $status;
            $data['stdid'] = $stdid;
            $data['statusName'] = $statusName;


            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/studentAttendanceStatusWise_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_monthlyAttendanceReport( $etype,$year,$month,$brid,$claid,$secid,$month_yearOne,$monthOne,$yearOne,$daysOne,$className,$SectionName,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

      // if ( !$etype ) {
      //   redirect('user/dashboard');
      // } else
          if ( $etype == 'monthlyAttendanceReport' ) {

            $data['vrdetail'] = $this->attendances->monthlyAttendanceReport($month, $year, $this->brid, $claid, $secid);

            $data['title'] = 'Monthly Attendance Report';
            $data['year'] = $year;
            $data['month'] = $month;
            $data['claid'] = $claid;
            $data['secid'] = $secid;
            $data['brid'] = $brid;
            $data['month_yearOne'] = $month_yearOne;
            $data['monthOne'] = $monthOne;
            $data['yearOne'] = $yearOne;
            $data['daysOne'] = $daysOne;
            $data['className'] = $className;
            $data['SectionName'] = $SectionName;



            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/monthlyAttendanceReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_dailAttandanceReport( $etype,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } elseif ( $etype == 'dailyAttendanceReport' ) {

            $data['vrdetail'] = $this->attendances->dailyAttendanceReport($this->brid);
            $data['title'] = 'Daily Attendance Report';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/dailyAttendanceReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        public function pdf_studentAttendanceMonthWise( $etype,$from,$to,$brid,$claid,$secid,$stdid,$className,$sectionName,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'studentAttendanceMonthWise' ) {

            $data['vrdetail'] =  $this->attendances->studentAttendanceMonthWiseReport($from, $to, $this->brid, $claid, $secid, $stdid);
            $data['title'] = 'Monthly Attendance Report';
            $data['from'] = $from;
            $data['to'] = $to;
            $data['className'] = $className;
            $data['secid'] = $secid;
            $data['stdid'] = $stdid;
            $data['sectionName'] = $sectionName;




            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/studentAttendancMonthsWise_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function admissionWithdrawlReport_pdf( $etype,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'admissionWithdrawlReport' ) {

            $data['vrdetail'] =  $this->students->admissionWithdrawlReport($this->brid);
            $data['title'] = 'Admission Withdrawl Report';





            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/admissionWithdrawlReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        public function pdf_SubjectViewReport( $etype, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'SubjectViewReport' ) {

            $data['vrdetail'] =  $this->subjects->subjectViewReport($this->brid);
            $data['title'] = 'Subject View Report';


            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/SubjectViewReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));
            

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        public function pdf_SubjectAssigneClassWise( $etype, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'SubjectAssigneClassWise' ) {

            $data['vrdetail'] =  $this->subjects->subjectViewClassWiseReport($this->brid);
            $data['title'] = 'Subject Assigned Class Report';


            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/subjectAssignedDetail(Class Wise)Report_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        public function pdf_SubjectAssignedDetailTeacherWise( $etype, $email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'SubjectAssignedDetailTeacherWise' ) {

            $data['vrdetail'] =  $this->subjects->subjectViewTeacherWiseReport($this->brid);
            $data['title'] = 'Subject Assigned Teacher Report';


            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/subjectAssignedDetail(Teacher Wise)Report_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }


        public function pdf_studentStatusReport( $etype,$brid,$claid,$secid,$status,$statusName,$sectionName,$className,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'studentStatusReport' ) {

            $data['vrdetail'] = $this->students->fetchStuReportByStatus($status, $this->brid, $claid, $secid);
            $data['title'] = 'Student Status Report';

            $data['brid'] = $brid;
            $data['secid'] = $secid;
            $data['status'] = $status;
            $data['claid'] = $claid;
            $data['statusName'] = $statusName;
            $data['sectionName'] = $sectionName;
            $data['className'] = $className;





            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/studentStatusReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_addmissionWithdrawlReport( $etype,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'admissionWithdrawlReport' ) {

            $data['vrdetail'] = $this->students->admissionWithdrawlReport($this->brid);
            $data['title'] = 'Admission Withdrawl Report';







            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/studentStatusReport_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_ledger( $dt1 , $dt2 , $party_id, $company_id, $email_pdf = -1, $user_print,$hd=1 )
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          $data['pledger'] = $this->ledgers->getAccLedgerReport22($dt1 , $dt2 ,$party_id ,$company_id);
          $data['title'] = 'Account Ledger';
        // $data['etype'] = $etype;
          $data['user'] = $user_print;
          $date_between= date('d-M-y', strtotime($dt1)) . ' To '. date('d-M-y', strtotime($dt2));


          $data['date_between']=$date_between;

        // if (empty($data['pledger'])) {
        //     redirect('payment');
        // }

        // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

          $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
          $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


          if ($hd==1){
            $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));  
          }else{
            $data['header_img'] = '';  
          }

          $this->wkhtmltopdf->addPage($this->load->view('reportprints/acc_ledger', $data, true));
          $this->wkhtmltopdf->setOptions(array(
            'orientation' => 'portrait',
            'footer-center'=> 'Page: [page]/[toPage]',
            'margin-left'=>5,
            'margin-right'=>5,
            'margin-top'=>5

            ));

          if ( $email_pdf == 1 ) {
            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );
          } else {
            $this->wkhtmltopdf->send();
          } 
        }


        public function pdf_chequeReports( $etype,$startDate,$endDate,$type,$grandBal,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'chequesReports' ) {

            $data['vrdetail'] = $this->accounts->getChequeReportData($startDate, $endDate, $type, $this->brid);
            $data['title'] = 'Cheque Reports';
            $data['from'] = $startDate;
            $data['to'] = $endDate;
            $data['type'] = $type;
            $data['grandBal'] = $grandBal;



            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/chequeReports_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        public function pdf_chartsOfAccounts( $etype,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'chartsOfAccounts' ) {

            $data['vrdetail'] =  $this->accounts->getChartOfAccounts($this->brid);
            $data['title'] = 'Chart of Accounts';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/chartsOfAccounts_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } 
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        public function pdf_accountReports( $etype,$startDate, $endDate, $what,$email_pdf = -1)
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype === 'cpv' || $etype === 'crv' ) {

            $data['vrdetail'] =  $this->payments->fetchCashReportData($startDate, $endDate, $what, $etype, $this->brid);
            $data['title'] = 'Account Reports';
            $data['startDate'] = $startDate;
            $data['endDate'] = $endDate;
            $data['what'] = $what;
            $data['etype'] = $etype;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/accountReports_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          } else if ($etype === 'jv') { 

            $data['vrdetail'] =  $this->accounts->fetchExpenseReportData($startDate, $endDate, $what, $etype, $this->brid);
            $data['title'] = 'Account Reports';
            $data['startDate'] = $startDate;
            $data['endDate'] = $endDate;
            $data['what'] = $what;
            $data['etype'] = $etype;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/accountReports_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          }
          else if ($etype === 'expense') {
            $data['vrdetail'] =  $this->payments->fetchJVReportData($startDate, $endDate, $what, $etype, $this->brid);
            $data['title'] = 'Account Reports';
            $data['startDate'] = $startDate;
            $data['endDate'] = $endDate;
            $data['what'] = $what;
            $data['etype'] = $etype;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/accountReports_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));
          }
          else if ($etype === 'daybook') {
        // get more data see etype daybook in js file
            $data['vrdetail'] =  $this->accounts->fetchDayBookReportData($startDate, $endDate, $what, $etype, $this->brid);
            $data['title'] = 'Account Reports';
            $data['startDate'] = $startDate;
            $data['endDate'] = $endDate;
            $data['what'] = $what;
            $data['etype'] = $etype;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/accountReports_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));

          }
          else if ($etype === 'payable' || $etype === 'receiveable') {
            $data['vrdetail'] =  $this->accounts->fetchPayRecvReportData($startDate, $endDate, $etype, $this->brid);
            $data['title'] = 'Account Reports';
            $data['startDate'] = $startDate;
            $data['endDate'] = $endDate;
            $data['what'] = $what;
            $data['etype'] = $etype;

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }
            $data['header_img'] = base_url('assets/img/pic1.png/' );
            $this->wkhtmltopdf->addPage($this->load->view('reportprints/accountReports_pdf', $data,true));
            $this->wkhtmltopdf->setOptions(array(
              'orientation' => 'portrait',
              'page-height' => 146,
              'page-width'  => 104,
              'margin-top' => '0',
              'margin-left' => '4',
              'margin-right' => '3',
              'footer-spacing' => '3.0',
              'footer-center' => '[page]/[toPage]',
              'footer-font-size' => '7'
              ));
          }
          if ( $email_pdf === 1 ) {

            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/zzm\application\assets\documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );

          } else {
            $this->wkhtmltopdf->send();
          } 
        }
        public function pdf_TrialBalance( $dt1 , $dt2 , $company_id, $email_pdf = -1, $user_print, $l1 , $l2 ,$l3 )
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          $data['pledger'] = $this->accounts->fetchTrialBalanceData($dt1, $dt2,$company_id, $l1 , $l2 ,$l3);
          $data['title'] = 'Trial Balance';
        // $data['etype'] = $etype;
          $data['user'] = $user_print;
          $date_between= (string)$dt1 . ' To '. (string)$dt2; 

          $data['date_between']=$date_between;

        // if (empty($data['pledger'])) {
        //     redirect('payment');
        // }

        // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

         // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
         // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


          $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

          $this->wkhtmltopdf->addPage($this->load->view('reportprints/TrialBalancePdf', $data, true));
          $this->wkhtmltopdf->setOptions(array(
            'orientation' => 'portrait'
            ));

          if ( $email_pdf == 1 ) {
            $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
            $this->wkhtmltopdf->saveAs( $save_path );
            sleep(5);
            $this->send_mail( $save_path );
          } else {
            $this->wkhtmltopdf->send();
          } 
        }

        


  // Using WKHTML - Working Perfectly
        public function pdf_doublevoucher( $etype, $vrnoa, $company_id )
        {
          $this->load->library('wkhtmltopdf');

          $data = array();

          if ( !$etype ) {
            redirect('user/dashboard');
          } else if ( $etype == 'sale' ) {

            $data['vrdetail'] = $this->sales->fetchVoucher( $vrnoa, $company_id );
            $data['title'] = 'Sale';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }

            $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );
            $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
            $data['previousBalance'] = $balData[0]['RTotal'];

          } else if ( $etype == 'purchase') {
            $data['vrdetail'] = $this->purchases->fetchVoucher( $vrnoa, $company_id );
            $data['title'] = 'Purchase';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }

            $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );
            $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
            $data['previousBalance'] = $balData[0]['RTotal'];
          } else if ( $etype == 'purchasereturn') {
            $data['vrdetail'] = $this->purchasereturns->fetchVoucher( $vrnoa, $company_id );
            $data['title'] = 'Purchase Return';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }

            $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

            $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
            $data['previousBalance'] = $balData[0]['RTotal'];

          } else if ( $etype == 'purchaseorder') {
            $data['vrdetail'] = $this->purchases->fetchOrderVoucher( $vrnoa, $company_id );
            $data['title'] = 'Quotation';

            $etype = 'quotation';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }

            $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );
            $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
            $data['previousBalance'] = $balData[0]['RTotal'];

          } else if ( $etype == 'salereturn') {
            $data['vrdetail'] = $this->salereturns->fetchVoucher( $vrnoa, $company_id );
            $data['title'] = 'Sale Return';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }

            $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );
            $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
            $data['previousBalance'] = $balData[0]['RTotal'];
          } else if ( $etype == 'saleorder') {
            $data['vrdetail'] = $this->sales->fetchOrderVoucher( $vrnoa, $company_id );
            $data['title'] = 'Quotation';

            if (empty($data['vrdetail'])) {
              redirect('user/dashboard');
            }

            $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );
            $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['party_id'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
            $data['previousBalance'] = $balData[0]['RTotal'];
          }

          $data['header_img'] = base_url('application/assets/uploads/header_imgs/' . $this->session->userdata('header_img'));

      // $this->load->view('reportprints/doublevoucher_pdf2', $data);

          $this->wkhtmltopdf->addPage($this->load->view('reportprints/doublevoucher_pdf2', $data, true));
          $this->wkhtmltopdf->setOptions(array(
            'orientation' => 'landscape'
            ));

          $this->wkhtmltopdf->send();
        }
      }
      ?>