<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fee extends CI_Controller {

	private $brid = '';

	public function __construct(){
		parent::__construct();
		$this->load->model('fees');
		$this->load->model('charges');
		$this->load->model('branches');
		$this->load->model('accounts');
		$this->load->model('ledgers');
		$this->load->model('students');
		$this->load->model('charges');

		$this->brid = $this->session->userdata('brid');
	}

	public function index() {
		redirect('fee/add');
	}

	public function add() {

		unauth_secure();
		$data['modules'] = array('fee');
		$data['feeCategoryNames'] = $this->fees->getAllFeeCategories($this->brid);
		$data['feeCategories'] = $this->fees->fetchAll($this->brid);

		$data['page_title'] = 'Fee Define';
		$this->load->view('template/header',$data);

		$this->load->view('setup/addfeecategory', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function assignToClass() {
		unauth_secure();
		$data['modules'] = array('assignfee');
		$data['charges'] = $this->charges->fetchAll($this->brid);
		$data['feeCategories'] = $this->fees->fetchAll($this->brid);

		$data['page_title'] = 'Assign Fee To Class';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/assignfeetoclass', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function issuance() {
		unauth_secure();
		$data['modules'] = array('feevouchers/feeissuance');
		$data['feeCategories'] = $this->fees->fetchAll($this->brid);

		$data['page_title'] = 'Monthly Fee Issuance';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/feeissuance', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function recieve() {
		unauth_secure();
		$data['modules'] = array('feevouchers/feerecieve');
		$data['accounts'] = $this->accounts->getAllParties('student', $this->brid);
		$data['charges'] = $this->charges->fetchAll($this->brid);
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		

		$data['page_title'] = 'Fee Receive Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/feerecievevoucher', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}



	public function admission() {
		unauth_secure();
		$data['modules'] = array('feevouchers/admissioncharges');
		$data['charges'] = $this->charges->fetchAll($this->brid);
		$data['students'] = $this->students->fetchAll('', $this->brid);

		$data['page_title'] = 'Admission Charges Voucher ';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/admissioncharges', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function Concession() {
		unauth_secure();
		$data['modules'] = array('feevouchers/concession');
		$data['charges'] = $this->charges->fetchAll($this->brid);
		$data['students'] = $this->students->fetchAll('', $this->brid);

		$data['page_title'] = 'Add Concession ';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/concession', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {

		$maxId = $this->fees->getMaxId($_POST['brid']) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxFeeAssignId() {

		$maxId = $this->fees->getMaxFeeAssignId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxFeeRecId() {

		$maxId = $this->fees->getMaxFeeRecId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxFeeIssuanceId() {

		if (isset($_POST)) {

			$etype = $_POST['etype'];
			$maxId = $this->fees->getMaxFeeIssuanceId($etype, $this->brid) + 1;

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($maxId));
		}
	}

	public function getAllFeeCategories() {

		$result = $this->fees->getAllFeeCategories();
		$this->output
		-> set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST)) {

			$feeCatDetail = $_POST['feeCatDetail'];

			$result = $this->fees->save($feeCatDetail);

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveFeeIssuance() {

		if (isset($_POST)) {

			$ledgers = objectToArray(json_decode($_POST['ledgers']));
			$studs = objectToArray(json_decode($_POST['studs']));
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$resultData = $this->fees->saveFeeIssuance($studs, $dcno, $this->brid,$ledgers);
			

			$response = array();
			if ( $resultData === false ) {
				$response['error'] = 'true';
			} else {
				$response = $resultData;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveAdmissionCharges() {

		if (isset($_POST)) {

			$ledgers = $_POST['ledgers'];
			$main = $_POST['main'];
			$detail = $_POST['detail'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$result = $this->fees->saveAdmissionCharges($main, $detail, $dcno, $this->brid);
			$result = $this->ledgers->save($ledgers, $dcno, $etype, $this->brid);


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveFeeRecieve() {

		if (isset($_POST)) {

			$response = array();

			$feerec = $_POST['feerec'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			if(isset($_POST['ledgers'])){
				$ledgers = json_decode($_POST['ledgers'], true);
			}else{
				$ledgers = "";
			}

			$chalan_no = $feerec['rno'];

			
			if($chalan_no !=='')
				$result1 = $this->fees->updateVrnoaCheck($dcno,$chalan_no, $this->brid);
			else
				$result1 = true;

			if ($result1===true){
				$result =$this->fees->saveFeeRecieve($feerec, $dcno, $this->brid, $ledgers);
				if ( $result === false ) {
					$response['error'] = 'true';
				} else {
					$response['error'] = 'false';
				}

			}else{
					$response['error'] = 'duplicate';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchFeeCategory() {

		if (isset( $_POST )) {

			$fcid = $_POST['fcid'];
			$brid = $_POST['brid'];

			$result = $this->fees->fetchFeeCategory($fcid, $brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
public function fetchAll_Admission() {

    	if (isset( $_POST )) {

    		$from = $_POST['from'];
    		$to = $_POST['to'];
    		$orderby = $_POST['orderby'];
    		$brid = $_POST['brid'];
    		
			$brid = $_POST['cl'];
			$brid = $_POST['sec'];
			$brid = $_POST['stu'];
			$brid = $_POST['ldgr'];
    		// $status = $_POST['status'];
    		$crit='';
    		if(isset($_POST['crit']))
    			$crit = $_POST['crit'];

    		$result = $this->fees->fetchAll_Admission($from,$to,$orderby,$crit,$brid);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}
    		$json = json_encode($response);
    		echo $json;
    	}
    }
	public function fetchAll() {

		$result = $this->fees->fetchAll();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {
			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

	public function saveAssignFeetoClass() {

		if (isset($_POST)) {

			$main = $_POST['main'];
			$detail = $_POST['detail'];
			$claid = $_POST['claid'];
			$fcid = $_POST['fcid'];
			$dcno = $_POST['dcno'];
			$brid = $this->brid;

			$error = $this->fees->isFeeAlreadyAssignedToClass($dcno, $brid, $claid, $fcid);

			if (!$error) {

				$faid = $this->fees->saveAssignFeetoClassMain($main, $this->brid);
				$result = $this->fees->saveAssignFeetoClassDetail($detail, $faid, $this->brid);

				$response = array();
				if ( $result === false ) {
					$response['error'] = 'true';
				} else {
					$response['error'] = 'false';
				}

				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			} else {
				$response = array();
				$response['error'] = 'duplicate';
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			}
		}
	}

	public function fetchFeeAssign() {

		if (isset( $_POST )) {

			$faid = $_POST['faid'];

			$main = $this->fees->fetchFeeAssignMain($faid, $this->brid);
			$detail = $this->fees->fetchFeeAssignDetail($faid, $this->brid);

			$response = array();
			if ( $detail === false ) {
				$response = 'false';
			} else {
				$response['main'] = $main;
				$response['detail'] = $detail;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteVoucher() {

		if (isset($_POST)) {

			$result = $this->fees->deleteVoucher($_POST['faid'], $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteFeeRecieveVoucher() {

		if (isset($_POST)) {

			$result = $this->fees->deleteFeeRecieveVoucher($_POST['frid'], $this->brid);
			

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function getStudentsWithChargesForFeeIssuance() {

		if (isset($_POST)) {

			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$fcid = $_POST['fcid'];

			$from = $_POST['from'];


			$students = $this->students->getStudentsForFeeIssue($this->brid, $claid, $fcid,$from);
			$charges = $this->charges->getChargesForFeeIssue($this->brid, $claid, $fcid);
			$concessions = $this->students->getStudentsForFeeConcession($this->brid, $claid, $fcid,$from);


			$response = array();
			if ( $students === false ) {
				$response = 'false';
			} else {
				$response['students'] = $students;
				$response['charges'] = $charges;
				$response['concessions'] = $concessions;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	// fetchs the previous saved record
	public function fetchStudentsWithCharges() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$students = $this->fees->fetchStudents($dcno, $etype, $this->brid);
			$charges = $this->fees->fetchCharges($dcno, $etype, $this->brid);

			$response = array();
			if ( $students === false ) {
				$response = 'false';
			} else {
				$response['students'] = $students;
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	public function fetch() {

		if (isset($_POST)) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];

			$charges = $this->fees->fetchFee($vrnoa, $etype, $this->brid);

			$response = array();
			if ( $charges === false ) {
				$response = 'false';
			} else {
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	// fetchs the previous saved record
	public function fetchFeeIssue() {

		if (isset($_POST)) {

			$fmid = $_POST['fmid'];
			$etype = $_POST['etype'];

			$charges = $this->fees->fetchFeeIssue($fmid, $etype, $this->brid);

			$response = array();
			if ( $charges === false ) {
				$response = 'false';
			} else {
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchFeeIssueByChalan() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$stdid = $_POST['stdid'];

			$etype = $_POST['etype'];

			$charges = $this->fees->fetchFeeIssueByChalan($dcno, $etype, $this->brid,$stdid);

			$response = array();
			if ( $charges === false ) {
				$response = 'false';
			} else {
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}


	// fetchs the previous saved record
	public function fetchFeeRecieve() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];

			$feerec = $this->fees->fetchFeeRecieve($dcno, $this->brid);
			$charges = $this->fees->fetchChargesForFeeRecieve($dcno, $this->brid);

			$response = array();
			if ( $feerec === false ) {
				$response = 'false';
			} else {
				$response['feerec'] = $feerec;
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteVoucherOtherIsuAdmi() {

		if (isset($_POST)) {

			

			$result = $this->fees->deleteVoucherOtherIsuAdmi($_POST['dcno'], $_POST['etype'], $this->brid);
			
			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function simpleVrnoCheck() {

		if (isset($_POST)) {

			$name = $_POST['name'];

			$result = $this->fees->simpleVrnoCheck($name);

			$response = array();
			if ($result) {
				$response['error'] = 'false';
			} else {
				$response['error'] = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function updateVrnoaCheck() {

		if (isset($_POST)) {

			$vrno = $_POST['vrno'];
			$chalanno = $_POST['chalanno'];
			$brid = $_POST['brid'];


			$result = $this->fees->updateVrnoaCheck($vrno, $chalanno,$brid);

			$response = array();
			if ($result) {
				$response['error'] = 'false';
			} else {
				$response['error'] = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	
	public function monthlyFeeAssignReport() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$orderby = $_POST['orderby'];
    		 // $brid = $_POST['brid'];
			$crit='';
			if(isset($_POST['crit']))
				$crit = $_POST['crit'];

			$result = $this->fees->monthlyFeeAssignReport($from,$to,$orderby,$crit,$this->brid);
    		// print_r($result);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$json = json_encode($response);
			echo $json;
		}
	}

	public function monthlyFeeIssuanceReportChargesWise() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];

			$result = $this->fees->monthlyFeeIssuanceReportChargesWise($from, $to, $this->brid);

			$response = array();
			if ($result === false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function feeAssignReport() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];

			$result = $this->fees->feeAssignReport($from, $to, $this->brid);

			$response = array();
			if ($result === false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function feeConcessionReport() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];

			$result = $this->fees->feeConcessionReport($from, $to, $this->brid);

			$response = array();
			if ($result === false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	public function feeRecStudentWiseReport() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$orderby = $_POST['orderby'];
			$brid = $_POST['brid'];
			$brid = $_POST['cl'];
			$brid = $_POST['sec'];
			$brid = $_POST['stu'];
			$brid = $_POST['ldgr'];

			$crit='';
			if(isset($_POST['crit']))
				$crit = $_POST['crit'];

			$result = $this->fees->feeRecStudentWiseReport($from,$to,$orderby,$crit,$brid);


			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$json = json_encode($response);
			echo $json;
		}
	}

	// public function feeRecStudentWiseReport() {

	// 	if (isset($_POST)) {

	// 		$from = $_POST['from'];
	// 		$to = $_POST['to'];

	// 		$result = $this->fees->feeRecStudentWiseReport($from, $to, $this->brid);

	// 		$response = array();
	// 		if ($result === false) {
	// 			$response = 'false';
	// 		} else {
	// 			$response = $result;
	// 		}

	// 		$this->output
	// 		->set_content_type('application/json')
	// 		->set_output(json_encode($response));
	// 	}
	// }

	public function monthlyFeeRcvReportChargesWise() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];

			$result = $this->fees->monthlyFeeRcvReportChargesWise($from, $to, $this->brid);

			$response = array();
			if ($result === false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
public function feeDefaulterReport() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$orderby = $_POST['orderby'];
			$brid = $_POST['brid'];
			$brid = $_POST['cl'];
			$brid = $_POST['sec'];
			$brid = $_POST['stu'];
			$brid = $_POST['ldgr'];
		
			$crit='';
			if(isset($_POST['crit']))
				$crit = $_POST['crit'];

			$result = $this->fees->feeDefaulterReport($from,$to,$orderby,$crit,$brid);


			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$json = json_encode($response);
			echo $json;
		}
	}
	// public function feeDefaulterReport() {

	// 	if (isset($_POST)) {

	// 		$to = $_POST['to'];

	// 		$result = $this->fees->feeDefaulterReport($to, $this->brid);

	// 		$response = "";
	// 		if ($result == false) {
	// 			$response = 'false';
	// 		} else {
	// 			$response = $result;
	// 		}

	// 		$this->output
	// 		->set_content_type('application/json')
	// 		->set_output(json_encode($response));
	// 	}
	// }
}

/* End of file fee.php */
/* Location: ./application/controllers/fee.php */