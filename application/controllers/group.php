<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('groups');
	}

	public function index() {
		redirect('group/addClass');
	}

	public function addClass() {
		unauth_secure();
		$data['modules'] = array('_class');
		$data['branchNames'] = $this->groups->getAllBranches();
		$data['_classes'] = $this->groups->fetchAllClasses($this->session->userdata('brid'));

		$data['page_title'] = 'Add Class ';
		$this->load->view('template/header',$data);

		$this->load->view('setup/addclass', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function addSection() {
		unauth_secure();
		$data['modules'] = array('section');
		$data['sections'] = $this->groups->fetchAllSections($this->session->userdata('brid'));

		$data['page_title'] = 'Add Section ';
		$this->load->view('template/header',$data);

		$this->load->view('setup/addsection', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {
		unauth_secure();
		if (isset($_POST)) {

			$table = $_POST['table'];
			$brid = $_POST['brid'];

			$maxId = $this->groups->getMaxId( $table, $brid ) + 1;

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($maxId));
		}
	}

	public function getAllBranches() {

		$result = $this->groups->getAllBranches();

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function fetchSectionsByBranchAndClass() {

		if (isset($_POST)) {

			$brid = $_POST['brid'];
			$claid = $_POST['claid'];

			$result = $this->groups->fetchSectionsByBranchAndClass( $brid, $claid );

			$response = '';
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchClassesByBranch() {

		if (isset($_POST)) {

			$brid = $_POST['brid'];

			$result = $this->groups->fetchClassesByBranch( $brid );

			$response = '';
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveClass() {

		if (isset($_POST)) {

			$classDetail = $_POST['classDetail'];

			$result = $this->groups->saveClass( $classDetail );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	
	public function saveSection() {

		if (isset($_POST)) {

			$sectionDetail = $_POST['sectionDetail'];

			$result = $this->groups->saveSection( $sectionDetail );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchClass() {

		if (isset( $_POST )) {

			$claid = $_POST['claid'];
			$brid = $_POST['brid'];

			$result = $this->groups->fetchClass($claid, $brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
	
	public function fetchSection() {

		if (isset( $_POST )) {

			$secid = $_POST['secid'];
			$brid = $_POST['brid'];

			$result = $this->groups->fetchSection($secid, $brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAllClasses() {

		$result = $this->groups->fetchAllClasses();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

	public function fetchAllSections() {

		$result = $this->groups->fetchAllSections();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}
}