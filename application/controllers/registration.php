<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {

	private $brid = "";
	public function __construct() {
		parent::__construct();
		$this->load->model('registrations');
		$this->load->model('charges');
		$this->load->model('branches');
		$this->load->model('accounts');
		$this->load->model('ledgers');

		$this->brid = $this->session->userdata('brid');
	}

	public function index()	{
		redirect('registration/receive');
	}

	public function receive() {
		unauth_secure();
		$data['modules'] = array('feevouchers/registrationreceive');
		$data['registration_fee'] = $this->charges->fetchChargeByName('registration fee', $this->brid);
		$data['accounts'] = $this->accounts->getAllParties('student', $this->brid);
		$data['charges'] = $this->charges->fetchAll($this->brid);

		$data['page_title'] = 'Add Registration Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('feevouchers/registrationreceive', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchStudentCharges() {

		if (isset( $_POST )) {

			$rid = $_POST['rid'];
			$result = $this->registrations->fetchStudentCharges($rid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function getMaxRegReceiveId() {

		$maxId = $this->registrations->getMaxRegReceiveId($this->brid) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function saveRegRecieve() {

		if (isset($_POST)) {

			$ledgers = $_POST['ledgers'];
			$regrec = $_POST['regrec'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];

			$result = $this->registrations->saveRegRecieve($regrec, $dcno, $this->brid);
			$result = $this->ledgers->save($ledgers, $dcno, $etype, $this->brid);


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	// fetchs the previous saved record
	public function fetchRegRecieve() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];

			$regrec = $this->registrations->fetchRegRecieve($dcno, $this->brid);
			$charges = $this->registrations->fetchChargesForRegRecieve($dcno, $this->brid);

			$response = array();
			if ( $regrec === false ) {
				$response = 'false';
			} else {
				$response['regrec'] = $regrec;
				$response['charges'] = $charges;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteRegRecieveVoucher() {

		if (isset($_POST)) {

			$result = $this->registrations->deleteRegRecieveVoucher($_POST['frid'], $this->brid);
			$result = $this->ledgers->deleteVoucher($_POST['frid'], 'frvproc', $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
}

/* End of file registration.php */
/* Location: ./application/controllers/registration.php */