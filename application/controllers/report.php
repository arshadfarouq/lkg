<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('users');
		
		$this->load->model('charges');
		$this->load->model('students');
		$this->load->model('branches');
		$this->load->model('reports');
		$this->load->model('payments');
		$this->load->model('accounts');
		$this->load->model('companies');
		$this->load->model('concessions');
		
		$this->load->model('levels');
		$this->brid = $this->session->userdata('brid');
		
	}
	public function index(){
		unauth_secure();
	}
	public function staffStatus() {
		unauth_secure();
		$data['modules'] = array('reports/staff/staffstatus');

		$data['page_title'] = ' Staff Status Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/staff/staffstatus', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function subjectView() {
		unauth_secure();
		$data['modules'] = array('reports/subject/subjectview');

		$data['page_title'] = ' Subject Report';
		$this->load->view('template/header',$data);

		$this->load->view('reports/subject/subjectview');

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function feeAssign() {
		unauth_secure();
		$data['modules'] = array('reports/fee/feeassign');

		$data['page_title'] = ' Fee Assign Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/feeassign');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function subjectViewTeacherWise() {
		unauth_secure();
		$data['modules'] = array('reports/subject/subjectviewteacherwise');

		$data['page_title'] = ' Subject Teacher Report';
		$this->load->view('template/header',$data);

		$this->load->view('reports/subject/subjectviewteacherwise');

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function subjectViewClassWise() {
		unauth_secure();
		$data['modules'] = array('reports/subject/subjectviewclasswise');

		$data['page_title'] = ' Subject Class Report';
		$this->load->view('template/header',$data);

		$this->load->view('reports/subject/subjectviewclasswise');

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function defaulters() {
		unauth_secure();
		$data['modules'] = array('reports/fee/defaulters');

		$data['categories'] = $this->students->fetchAllCategories($this->brid);
		$data['section'] = $this->students->fetchAllsection($this->brid);
		$data['charges'] = $this->charges->fetchAllCarges($this->brid);
		$data['classes'] = $this->students->fetchAllclass($this->brid);
		$data['branch'] = $this->students->fetchAllbranch($this->brid);
		$data['occuption'] = $this->students->fetchByCol('focc');
		$data['genders'] = $this->students->fetchByCol('gender');
		$data['parties'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();

		$data['page_title'] = ' Defaulters Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/defaulters', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function monthlyFeeAssignChargesWise() {
		unauth_secure();
		$data['modules'] = array('reports/fee/monthlyfeeassignchargeswise');

		$data['page_title'] = ' Monthly Fee Assign Charges Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/monthlyfeeassignchargeswise');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function monthlyFeeAssign() {
		unauth_secure();
		$data['modules'] = array('reports/fee/monthlyfeeassign');
		
		$data['categories'] = $this->students->fetchAllCategories($this->brid);
		$data['section'] = $this->students->fetchAllsection($this->brid);
		$data['classes'] = $this->students->fetchAllclass($this->brid);
		$data['branch'] = $this->students->fetchAllbranch($this->brid);
		$data['occuption'] = $this->students->fetchByCol('focc');
		$data['genders'] = $this->students->fetchByCol('gender');
		$data['parties'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();



		$data['page_title'] = ' Monthly Fee Assign Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/monthlyfeeassign');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function chargesDefinition() {
		unauth_secure();
		$data['modules'] = array('reports/fee/chargedefinition');

		$data['page_title'] = ' Charges Definition Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/chargedefinition');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function studentStatus() {
		unauth_secure();
		$data['modules'] = array('reports/student/studentstatus');

		$data['page_title'] = ' Student Status Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/student/studentstatus', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function admissionWithdrawl() {
		unauth_secure();
		$data['modules'] = array('admissionWithdrawl');

		$data['students'] = $this->students->admissionWithdrawlReport($this->brid);

		$data['page_title'] = ' Admission With Drawl Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/student/admissionWithdrawl', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function feeConcession() {
		unauth_secure();
		$data['modules'] = array('reports/fee/feeconcession');

		$data['page_title'] = ' Fee Concession Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/feeconcession');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function feeRecStudentWise() {
		unauth_secure();
		$data['modules'] = array('reports/fee/feerecstudentwise');

		$data['categories'] = $this->students->fetchAllCategories($this->brid);
		$data['section'] = $this->students->fetchAllsection($this->brid);
		$data['charges'] = $this->charges->fetchAllCarges($this->brid);
		$data['classes'] = $this->students->fetchAllclass($this->brid);
		$data['branch'] = $this->students->fetchAllbranch($this->brid);
		$data['occuption'] = $this->students->fetchByCol('focc');
		$data['genders'] = $this->students->fetchByCol('gender');
		$data['parties'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();



		$data['page_title'] = 'Fee Recevie Student Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/fee/feerecstudentwise');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function Admission() {
		unauth_secure();
		$data['modules'] = array('reports/admission/admissionReport');

		$data['categories'] = $this->students->fetchAllCategories($this->brid);
		$data['section'] = $this->students->fetchAllsection($this->brid);
		$data['charges'] = $this->charges->fetchAllCarges($this->brid);
		$data['classes'] = $this->students->fetchAllclass($this->brid);
		$data['branch'] = $this->students->fetchAllbranch($this->brid);
		$data['occuption'] = $this->students->fetchByCol('focc');
		$data['genders'] = $this->students->fetchByCol('gender');
		$data['parties'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();

		$data['page_title'] = 'Admission Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/admission/admissionReport');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function resultSubjectWise() {
		unauth_secure();
		$data['modules'] = array('reports/examination/resultsubjectwise');

		$data['page_title'] = ' Result Subject Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/examination/resultsubjectwise');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function studentResult() {
		unauth_secure();
		$data['modules'] = array('reports/examination/studentresult');

		$data['page_title'] = ' Student Result Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/examination/studentresult', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function overallSemResult() {
		unauth_secure();
		$data['modules'] = array('reports/examination/overallsemresult');

		$data['page_title'] = ' Over All Result Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/examination/overallsemresult', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function stuResultTeacherWise() {
		unauth_secure();
		$data['modules'] = array('reports/examination/sturesultteacherwise');

		$data['page_title'] = ' Student Result Teacher Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/examination/sturesultteacherwise');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function StuAtndncStatusWise() {
		unauth_secure();
		$data['modules'] = array('attendance/reports/studentattendancestatuswise');

		$data['page_title'] = ' Student Attendance Status Report';
		$this->load->view('template/header',$data);
		$this->load->view('attendance/reports/studentattendancestatuswise', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function monthlyAttendanceReport() {
		unauth_secure();
		$data['modules'] = array('attendance/reports/monthlyatndncreport');

		$data['page_title'] = ' Monthly Attendance Report';
		$this->load->view('template/header',$data);
		$this->load->view('attendance/reports/monthlyatndncreport', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function StuAtndncMonthWise() {
		unauth_secure();
		$data['modules'] = array('attendance/reports/studentattendancemonthwise');

		$data['page_title'] = ' Student Attendance Month Report';
		$this->load->view('template/header',$data);
		$this->load->view('attendance/reports/studentattendancemonthwise', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function dailyAttendanceReport() {
		unauth_secure();
		$data['modules'] = array('attendance/reports/dailyattendancereport');

		$data['page_title'] = ' Daily Attendance Report';
		$this->load->view('template/header',$data);
		$this->load->view('attendance/reports/dailyattendancereport', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function student()
	{
		$data['modules'] = array('reports/inventory/ChartOfStudents');

		// $data['students'] = $this->students->fetchStudentAll($this->brid);
		$data['categories'] = $this->students->fetchAllCategories($this->brid);
		$data['section'] = $this->students->fetchAllsection($this->brid);
		$data['classes'] = $this->students->fetchAllclass($this->brid);
		$data['branch'] = $this->students->fetchAllbranch($this->brid);
		$data['parties'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();	

		$data['page_title'] = ' Student Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/inventory/ChartOfStudentsv', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function concession()
	{
		$data['modules'] = array('reports/inventory/FeeConcession');

		// $data['students'] = $this->students->fetchStudentAll($this->brid);
		$data['categories'] = $this->students->fetchAllCategories($this->brid);
		$data['section'] = $this->students->fetchAllsection($this->brid);
		$data['classes'] = $this->students->fetchAllclass($this->brid);
		$data['branch'] = $this->students->fetchAllbranch($this->brid);
		$data['occuption'] = $this->students->fetchByCol('focc');
		$data['genders'] = $this->students->fetchByCol('gender');
		$data['parties'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();


		$data['page_title'] = ' Concession Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/inventory/FeeConcessionsv', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function getAllCheques()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];
		$type = $_POST['type'];

		$reportData = $this->accounts->getAllCheques($from, $to, $type);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($reportData));			 
	}
	public function agingSheet()
	{
		
		unauth_secure();
		
		$data['modules'] = array('reports/accounts/agingsheet');
		
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['currdate'] = date("Y/m/d");
		$data['l3sDebitors'] = $this->levels->fetchAllLevel3_crit('DEBTORS');
		$data['l3sCreditors'] = $this->levels->fetchAllLevel3_crit('CREDITORS');

		$data['wrapper_class'] = "account_ledger";
		$data['page'] = "accountledger";

		$data['companies'] = $this->companies->getAll();

		$data['page_title'] = ' Aging Sheet Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/accounts/agingsheet',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);


	}
	public function fetchAgingSheetData()
	{
		// Redirect if unauthorized user.
		unauth_secure();

		if (isset($_POST)) {

			$company_id = $_POST['company_id'];
			$party_id = $_POST['party_id'];
			$startDate = $_POST['startDate'];
			$endDate = $_POST['endDate'];
			$type = $_POST['type'];
			$crit = $_POST['crit'];

			$sheetData = $this->accounts->fetchAgingSheetData( $party_id, $company_id, $startDate, $endDate, $type,$crit );
			// $json = json_encode($sheetData);

			// echo $json;

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sheetData));

			

		}
	}

	public function invoiceAging()
	{
		
		unauth_secure();
		
		$data['modules'] = array('reports/accounts/invoiceaging');
		
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['currdate'] = date("Y/m/d");
		

		$data['wrapper_class'] = "invoiceAgingSheet";
		$data['page'] = "invoiceAgingSheet";

		$data['companies'] = $this->companies->getAll();

		$data['page_title'] = ' Invoice Aging Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/accounts/invoiceaging',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);


	}
	public function fetchInvoiceAgingData()
	{
		unauth_secure();

		$from = $_POST['from'];
		$to = $_POST['to'];
		$reptType = $_POST['reportType'];
		$party_id = $_POST['party_id'];
		$company_id = $_POST['company_id'];

		$reportData = $this->accounts->fetchInvoiceAgingData( $from, $to, $reptType, $party_id, $company_id);
		$json = json_encode( $reportData );

		echo $json;
	}

	public function cashFlowStatement()
	{
		unauth_secure();
		$data['modules'] = array('reports/accounts/cashflowstatement');

		$data['page_title'] = ' Cash Flow Statement Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/accounts/cashflowstatement',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function purchase() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "purchase";
		$data['title'] = "Purchase ";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		// $data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		$data['sizes'] = $this->items->fetchByCol('size');
		$data['colors'] = $this->items->fetchByCol('model');

		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Purchase Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}

	public function distributionpayment() {
		unauth_secure();
		$data['modules'] = array('reports/accounts/distributionpayment');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Distribution Payment";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Distribution Payment Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/accounts/distributionpayment',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}

	public function distributionreceive() {
		unauth_secure();
		$data['modules'] = array('reports/accounts/distributionreceive');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Distribution Receive";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Distribution Receive Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/accounts/distributionreceive',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function distributionsale() {
		unauth_secure();
		$data['modules'] = array('reports/accounts/distributionsale');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Distribution Sale";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Distribution Sale Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/accounts/distributionsale',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}



	public function purchaseorder() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "pur_order";
		$data['title'] = "Purchase Order";


		$data['currdate'] = date("Y/m/d");
		
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');

		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		
		
		$data['page_title'] = ' Purchase order Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function purchaseorderbalance() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purOrderSummary');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Purchase Order Balance";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Purchase Order Balance Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/purOrderSummary',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function saleorderbalance() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purOrderSummary');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Sale Order Balance";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Sale Order Balance Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/purOrderSummary',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}

	public function saleorder() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "sale_order";
		$data['title'] = "Sale Order";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Sale Order Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	// 
	public function orderParts() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/orders');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Order Parts";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Order Parts Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/orders',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function orderLoading() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/orders');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Order Loading";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Order Loading Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/orders',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	// 


	public function purchaseReturn() {
		// unauth_secure();
		// $data['modules'] = array('reports/purchase/purchasereturn');
		// $data['page_title'] = ' Account Report';
		// $this->load->view('template/header',$data);
		// $this->load->view('reports/purchase/purchasereturn');
		// $this->load->view('template/mainnav');
		// $this->load->view('template/footer', $data);
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase__report";
		$data['page'] = "purchase_report";
		$data['title'] = "Purchase Return";
		$data['etype'] = "purchasereturn";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		$data['sizes'] = $this->items->fetchByCol('size');
		$data['colors'] = $this->items->fetchByCol('model');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$data['page_title'] = ' Purchase Return Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function salesummary() {
		unauth_secure();
		$data['modules'] = array('reports/sale/salesummary');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "sale";
		$data['title'] = "Sale";


		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
		$data['brands'] = $this->items->fetchAllBrands();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$data['page_title'] = ' Sale Summary Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/sale/salesummary',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function grahisheet() {
		unauth_secure();
		$data['modules'] = array('reports/sale/grahisheet');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "grahisheet";
		$data['title'] = "Grahi Sheet";
		$data['setting_configur'] = $this->accounts->getsetting_configur();


		$data['currdate'] = date("Y/m/d");
		
		$data['parties'] = $this->accounts->fetchAll(1);
		
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		
		$data['page_title'] = ' Grahi Sheet Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/sale/grahisheet',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function salereturnsummary() {
		unauth_secure();
		$data['modules'] = array('reports/sale/salesummary');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "sale";
		$data['title'] = "Sale Return";


		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		// $data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
		$data['brands'] = $this->items->fetchAllBrands();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$data['page_title'] = ' Sale Return Summary Report';
		$this->load->view('template/header',$data);
		$this->load->view('reports/sale/salesummary',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function saleholdbills() {
		unauth_secure();
		$data['modules'] = array('reports/sale/saleholdbill');
		/*$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Sale Return";*/

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
	/*	$data['brands'] = $this->items->fetchAllBrands();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['uoms'] = $this->items->fetchByCol('uom');*/
		// End Advanced Item
		// Advanced Account
			/*$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();*/
		// End Advanced Account
			$data['page_title'] = ' Sale Hold Bills Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/sale/salebillhold',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}


		public function purchasesummary() {
			unauth_secure();
			$data['modules'] = array('reports/sale/salesummary');
			$data['wrapper_class'] = "purchase_report";
			$data['page'] = "purchase_report";
			$data['title'] = "Purchase";
			$data['etype'] = "purchase";


			$data['currdate'] = date("Y/m/d");
		// Advanced
			$data['parties'] = $this->accounts->fetchAll(1);
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
			$data['brands'] = $this->items->fetchAllBrands();
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
			$data['page_title'] = ' Purchase Summary Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/sale/salesummary',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}
		public function purchasreturnesummary() {
			unauth_secure();
			$data['modules'] = array('reports/sale/salesummary');
			$data['wrapper_class'] = "purchase_report";
			$data['page'] = "purchase_report";
			$data['title'] = "Purchase Return";
			$data['etype'] = "purchasereturn";


			$data['currdate'] = date("Y/m/d");
		// Advanced
			$data['parties'] = $this->accounts->fetchAll(1);
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
			$data['brands'] = $this->items->fetchAllBrands();
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
			$data['page_title'] = ' Purchase Return Summary Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/sale/salesummary',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}
		public function sale() {
			unauth_secure();
			$data['modules'] = array('reports/purchase/purchase');
			$data['wrapper_class'] = "purchase_report";
			$data['page'] = "purchase_report";
			$data['etype'] = "sale";
			$data['title'] = "Sale";

			$data['currdate'] = date("Y/m/d");
		// Advanced
			$data['parties'] = $this->accounts->fetchAll(1);
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
			$data['brands'] = $this->items->fetchAllBrands();
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['uoms'] = $this->items->fetchByCol('uom');
			$data['sizes'] = $this->items->fetchByCol('size');
			$data['colors'] = $this->items->fetchByCol('model');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
			$data['page_title'] = ' Account Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/purchase/purchase',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function saleReturn() {
			unauth_secure();
			$data['modules'] = array('reports/purchase/purchase');
			$data['wrapper_class'] = "purchase_report";
			$data['page'] = "purchase_report";
			$data['title'] = "Sale Return";
			$data['etype'] = "salereturn";

			$data['currdate'] = date("Y/m/d");
		// Advanced
			$data['parties'] = $this->accounts->fetchAll(1);
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
			$data['sizes'] = $this->items->fetchByCol('size');
			$data['colors'] = $this->items->fetchByCol('model');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account

			$data['page_title'] = ' Sale Return Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/purchase/purchase', $data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function saleOrderWise() {
			unauth_secure();
			$data['modules'] = array('reports/sale/saleorderwise');
			$data['page_title'] = ' Sale OrderWise Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/sale/saleorderwise');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function detailedprofitloss()
		{
		// Redirect if unauthorized user.
			unauth_secure();
		// if ($this->session->userdata('viewProfitLoss') === 'false'){
		// 	return;
		// }

			$data['wrapper_class'] = "profitloss";
			$data['page'] = "profitloss";

			$data['currdate'] = date("Y/m/d");

			$data['modules'] = array('reports/accounts/detailed_profitloss_report');

			$data['vrnoa'] = $this->accounts->getMaxIdPls();
			$data['parties'] = $this->accounts->fetchAll();
			$data['setting_configur'] = $this->accounts->getsetting_configur();

			// $data['items'] = $this->items->fetchAll();


		// $data['companies'] = $this->companies->getAll();

			$this->load->view('template/header', $data);
			$this->load->view('template/mainnav', $data);
			$this->load->view('reports/accounts/detailedprofitloss', $data);
			$this->load->view('template/footer');
		}



		public function itemconversion() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/itemconversion');
			$data['wrapper_class'] = "purchase_report";
			$data['page'] = "purchase_report";
			$data['etype'] = "item_conversion";

			$data['currdate'] = date("Y/m/d");
		// Advanced
			$data['parties'] = $this->accounts->fetchAll(1);
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
			$data['brands'] = $this->items->fetchAllBrands();
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
			$data['page_title'] = ' Account Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/itemconversion',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function waiterCashPending() {
			unauth_secure();
			$data['modules'] = array('reports/sale/waitercashpending');
			$data['page_title'] = ' Waiter Cash Pending Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/sale/waitercashpending');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function fetchSaleReportData()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->sales->fetchSaleReportData($startDate, $endDate, $what, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchStockReport()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$company_id = $_POST['company_id'];
			$crit = $_POST['crit'];
			$etype = $_POST['etype'];



			$sreportData = $this->stocks->fetchStockReport($startDate, $endDate, $what, $company_id,$crit,$etype);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}
		public function fetchStockOrderReport()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$company_id = $_POST['company_id'];
			$crit = $_POST['crit'];
			$etype = $_POST['etype'];



			$sreportData = $this->stocks->fetchStockOrderReport($startDate, $endDate, $what, $company_id,$crit,$etype);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchDailyVoucherReport()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];

			$sreportData = $this->stocks->fetchDailyVoucherReport($startDate, $endDate);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchSaleProductionData()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->sales->fetchSaleProductionData($startDate, $endDate, $what, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchMaterialReturnData()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->purchases->fetchMaterialReturnData($startDate, $endDate, $what, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchConsumptionData()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->purchases->fetchConsumptionData($startDate, $endDate, $what, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchStockNavigationData()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->purchases->fetchStockNavigationData($startDate, $endDate, $what, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function fetchSaleReturnReportData()
		{
			$what = $_POST['what'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->sales->fetchSaleReturnReportData($startDate, $endDate, $what, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function saleReportOrderWise()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$type = $_POST['type'];

			$sreportData = $this->sales->saleReportOrderWise($startDate, $endDate, $type);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function waiterCashPendingReport()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];

			$sreportData = $this->sales->waiterCashPendingReport($startDate, $endDate);
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
		}

		public function itemLedger() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/itemlegderreport');
			$data['departments'] = $this->departments->fetchAllDepartments();

			$data['page_title'] = ' Item Ledger Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/itemlegderreport', $data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function production() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/production');
			$data['page_title'] = ' Production Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/production');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function stockNavigation() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/stocknavigation');
			$data['page_title'] = ' Stock Navigation Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/stocknavigation');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function dailyVoucher() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/dailyReport');
			$data['page_title'] = ' Daily Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/dailyReport');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function stock() {
			unauth_secure();
			$data['etype'] = 'withoutValue';
			$data['title'] = 'Stock Report';

			$data['modules'] = array('reports/inventory/stock');

			
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
			$data['sizes'] = $this->items->fetchByCol('size');
			$data['colors'] = $this->items->fetchByCol('model');

			$data['page_title'] = ' Stock Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/stock',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}
		public function StockRequired() {
			unauth_secure();
			$data['etype'] = 'withoutValue';
			$data['title'] = 'Stock Required Report';

			$data['modules'] = array('reports/inventory/stockOrder');

			
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
			$data['sizes'] = $this->items->fetchByCol('size');
			$data['colors'] = $this->items->fetchByCol('model');



			$data['page_title'] = ' Stock Required Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/stockOrder',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function stockvalue() {
			unauth_secure();
			$data['etype'] = 'wValue';
			$data['title'] = 'Stock Value Report';

			$data['modules'] = array('reports/inventory/stock');

			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
			$data['sizes'] = $this->items->fetchByCol('size');
			$data['colors'] = $this->items->fetchByCol('model');

			$data['page_title'] = ' Stock value Report';
			$this->load->view('template/header',$data);	
			$this->load->view('reports/inventory/stock',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function expiredstock() {
			unauth_secure();
			$data['etype'] = 'expiredstock';
			$data['title'] = 'Expired Stock Report';

			$data['modules'] = array('reports/inventory/stock');

			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
			$data['sizes'] = $this->items->fetchByCol('size');
			$data['colors'] = $this->items->fetchByCol('model');

			$data['page_title'] = ' Expired Stock Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/stock',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}


		public function materialReturn() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/materialreturn');
			$data['page_title'] = ' Material Return Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/materialreturn');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function consumption() {
			unauth_secure();
			$data['modules'] = array('reports/inventory/consumption');
			$data['page_title'] = ' Consumption Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/inventory/consumption');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function accountLedger() {
			unauth_secure();
			$data['modules'] = array('reports/accounts/accountledger');
			// $data['parties'] = $this->accounts->fetchAll();

			$data['page_title'] = ' Account Ledger Report';
			$this->load->view('template/header',$data);

			$this->load->view('reports/accounts/accountledger', $data);

			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function chartOfAccounts()
		{
			unauth_secure();
			$data['modules'] = array('reports/accounts/chartofaccounts');
			$data['page_title'] = ' Chart Of Accounts Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/accounts/chartofaccounts');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function getChartOfAccounts()
		{
			unauth_secure();
			$coas = $this->accounts->getChartOfAccounts();
			$json = json_encode($coas);

			echo $json;
		}

		public function cheques()
		{
			unauth_secure();
			$data['modules'] = array('reports/accounts/chequesReport');
			$data['page_title'] = ' Cheques Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/accounts/cheques');
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function getChequeReportData()
		{
			$startDate = $_POST['startDate'];
			$endDate = $_POST['endDate'];
			$type = $_POST['type'];
			$company_id= $_POST['company_id'];

			$reportData = $this->accounts->getChequeReportData($startDate, $endDate, $type,$company_id);

			$json = json_encode($reportData);

			echo $json;
		}

		public function getProfitLossReportData()
		{
		// what may be item, party, voucher
        // filterCrit is whose data is required. It may be an itemName, partyName or nothing at all.

			$what = $_POST['what'];
			$filterCrit = $_POST['filterCrit'];
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];

			$sreportData = $this->sales->fetchProfitLossReportData($what, $startDate, $endDate, $filterCrit);
			$json = json_encode($sreportData);

			echo $json;
		}

		public function profitloss()
		{
			unauth_secure();
			$data['modules'] = array('reports/accounts/profitloss_report');
			$data['wrapper_class'] = "purchase_report";
			$data['page'] = "purchase_report";
			$data['etype'] = "profit";

			$data['currdate'] = date("Y/m/d");

			$data['parties'] = $this->accounts->fetchAll(1);
			$data['departments'] = $this->departments->fetchAllDepartments();
			// $data['items'] = $this->items->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();


			$data['brands'] = $this->items->fetchAllBrands();
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['uoms'] = $this->items->fetchByCol('uom');

			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();

			$data['page_title'] = ' Profitloss Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/accounts/profitloss',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function accounts()
		{
			unauth_secure();
			$data['modules'] = array('reports/accounts/accounts_reports');

			$data['parties'] = $this->accounts->fetchAll(1);
			$data['userone'] = $this->users->fetchAll();
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account

			$data['page_title'] = ' Account Report';
			$this->load->view('template/header',$data);
			$this->load->view('reports/accounts/accounts',$data);
			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
		}

		public function fetchPayRecvReportData()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];
			$what = $_POST['what'];
			$crit = $_POST['crit'];



			$payrecvreportData = $this->accounts->fetchPayRecvReportData($startDate, $endDate, $etype, $company_id,$what,$crit);
			$json = json_encode($payrecvreportData);

			echo $json;
		}

		public function fetchDayBoookReportData()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$what = $_POST['what'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];
			$crit = $_POST['crit'];

			$dbreportData = $this->accounts->fetchDayBookReportData($startDate, $endDate, $what, $etype,$company_id,$crit);
			$json = json_encode($dbreportData);

			echo $json;
		}

		public function fetchExpenseReportData()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$what = $_POST['what'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];
			$crit = $_POST['crit'];
			
			$ereportData = $this->accounts->fetchExpenseReportData($startDate, $endDate, $what, $etype,$company_id,$crit);
			$json = json_encode($ereportData);

			echo $json;
		}

		public function fetchJVReportData()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$what = $_POST['what'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];
			$crit = $_POST['crit'];
			
			$jvreportData = $this->payments->fetchJVReportData($startDate, $endDate, $what, $etype,$company_id,$crit);
			$json = json_encode($jvreportData);

			echo $json;
		}

		public function fetchCashReportData()
		{
			$startDate = $_POST['from'];
			$endDate = $_POST['to'];
			$what = $_POST['what'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];
			$crit = $_POST['crit'];
			
			$creportData = $this->payments->fetchCashReportData($startDate, $endDate, $what, $etype,$company_id,$crit);
			$json = json_encode($creportData);

			echo $json;
		}
		public function fetchBalanceSheet()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			if (isset($_POST)) {

				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$company_id = $_POST['company_id'];
				$type = $_POST['type'];

				$tb_data = $this->accounts->fetchBalanceSheet($startDate, $endDate, $company_id, $type);

				$json = json_encode($tb_data);

				echo $json;			
			}
		}
		public function fetchNetSum_Etype()
		{
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$from = $_POST['from'];
			$to = $_POST['to'];
			$sum = $this->accounts->fetchNetSum_Etype($from,$to, $company_id ,$etype);

			$json = json_encode($sum);
			echo $json;
		}

		public function fetchNetWPPF()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->fetchNetWPPF($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function fetchNetPFT()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->fetchNetPFT($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function fetchNetOperatingExpenses()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->fetchNetOperatingExpenses($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function fetchNetExpense()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->fetchNetExpense($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function fetchOtherIncomeSum()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->fetchOtherIncomeSum($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function getIncomeReportData()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->getIncomeReportData($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function getExpenseReportData()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->getExpenseReportData($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}
		public function getPartnerCapitalReportData()
		{
		// Redirect if unauthorized user.
			unauth_secure();


			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->getPartnerCapitalReportData( $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}

		public function getCompanyCapitalReportData()
		{
		// Redirect if unauthorized user.
			unauth_secure();


			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->getCompanyCapitalReportData( $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}



		public function fetchNetFinanceCost()
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$data = $this->accounts->fetchNetFinanceCost($from, $to, $company_id);
			$json = json_encode($data);
			echo $json;
		}
		public function fetchOpeningStockReportData() 
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$stockRows = $this->accounts->fetchOpeningStockReportData( $from, $to, $company_id );

			$json = json_encode($stockRows);
			echo $json;
		}

		public function fetchClosingStockReportData() 
		{
		// Redirect if unauthorized user.
			unauth_secure();

			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];

			$stockRows = $this->accounts->fetchClosingStockReportData( $from, $to, $company_id );

			$json = json_encode($stockRows);
			echo $json;
		}

		public function fetchDisPayRecvCount()
		{
		//$startDate = $_POST['from'];
		//$endDate = $_POST['to'];
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];

			$payrecvreportData = $this->accounts->fetchDisPayRecvCount($company_id, $etype);
			$json = json_encode($payrecvreportData);

			echo $json;
		}
	}

	/* End of file report.php */
/* Location: ./application/controllers/report.php */