<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Result extends CI_Controller {

	private $brid = '';
	public function __construct() {
		parent::__construct();
		$this->load->model('branches');
		$this->load->model('results');

		$this->brid = $this->session->userdata('brid');
	}

	public function index(){
		redirect('result/add');
	}

	public function getMaxId() {

		$result = $this->results->getMaxId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function add() {
		unauth_secure();
		$data['modules'] = array('addresult');

		$data['page_title'] = 'Add Result Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('examination/addresult', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function save() {

		if (isset($_POST)) {

			$results = $_POST['results'];
			$dcno = $_POST['dcno'];

			$result = $this->results->save( $results, $dcno, $this->brid );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];

			$results = $this->results->fetch($dcno, $this->brid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchColByBrClsNSec() {

		if (isset( $_POST )) {

			$col = $_POST['col'];
			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];

			$results = $this->results->fetchColByBrClsNSec($col, $brid, $claid, $secid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function delete() {

		if (isset($_POST)) {

			$result = $this->results->delete($_POST['dcno'], $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function resultSubjectWiseReport() {

		$result = $this->results->resultSubjectWiseReport($this->brid);
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function stuResultTeacherWiseReport() {

		$result = $this->results->stuResultTeacherWiseReport($this->brid);
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function overallSemResultReport() {

		if (isset( $_POST )) {

			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$session = $_POST['session'];
			$stdid = $_POST['stdid'];

			$results = $this->results->overallSemResultReport($this->brid, $claid, $secid, $session, $stdid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function studentResultReport() {

		if (isset( $_POST )) {

			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$session = $_POST['session'];
			$stdid = $_POST['stdid'];
			$term = $_POST['term'];

			$results = $this->results->studentResultReport($this->brid, $claid, $secid, $session, $stdid, $term);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}
			
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}		
	}
}

/* End of file result.php */
/* Location: ./application/controllers/result.php */