<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting_configuration extends CI_Controller
{
	
	public function __construct() {
		parent::__construct();
		$this->load->model('setting_configurations');
		$this->load->model('accounts');
		$this->load->model('levels');
		
	}
	
	public function index(){
		unauth_secure();

		$data['modules'] = array( 'setup/addSettingConfiguration');
		$data['party']   = $this->accounts->fetchAll();
		$data['levels3']   = $this->levels->getLevel3();
		$data['levels2']   = $this->levels->getLevel2();
		$data['levels1']   = $this->levels->getLevel1();

		$data['page_title'] = 'Setting Configuration Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('setup/addsetting_configuration', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	

	public function EmailConfiguration(){
		$data['modules'] = array( 'setup/addEmailCredential');
		$data['page_title'] = 'Email Configuration Voucher';
		$this->load->view('template/header',$data);
		$this->load->view('setup/addEmailCredential',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer',$data);
	}

	public function save() {

		if (isset($_POST)) {

			$obj = $_POST['obj'];
			$result = $this->setting_configurations->save($obj);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response)); 
		}

	}
	public function saveEmail() {

		if (isset($_POST)) {

			$obj = $_POST['obj'];
			$result = $this->setting_configurations->saveEmail($obj);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response)); 
		}

	}


	public function fetch() {

		// if (isset( $_POST )) {

		$result = $this->setting_configurations->fetch();

		$response = "";
		if ( $result === false ) {
			$response = 'false';
		} else {
			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
		// }
	}
	public function fetchEmail() {

		if (isset( $_POST )) {

			$result = $this->setting_configurations->fetchEmail();

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
}?>