<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shift extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('shifts');
	}

	public function index() {
		redirect('shift/add');
	}

	public function add() {
		unauth_secure();
		$data['modules'] = array('shift/addshift');
		$data['shifts'] = $this->shifts->fetchAllShifts();
		$data['page_title'] = 'Add Shift Voucher';
$this->load->view('template/header',$data);
		$this->load->view('setup/shift/addshift', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxShiftId() {
		$result = $this->shifts->getMaxShiftId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function saveShift() {

		if (isset($_POST)) {

			$shift = $_POST['shift'];
			$result = $this->shifts->saveShift( $shift );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchShift() {

		if (isset( $_POST )) {

			$shid = $_POST['shid'];

			$result = $this->shifts->fetchShift($shid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchAllShifts() {

		$result = $this->shifts->fetchAllShifts();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response));
	}
}

/* End of file shift.php */
/* Location: ./application/controllers/shift.php */