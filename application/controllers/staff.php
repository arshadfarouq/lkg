<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {

	private $brid = '';

	public function __construct() {
		parent::__construct();
		$this->load->model('branches');
		$this->load->model('accounts');
		$this->load->model('staffs');
		$this->load->model('levels');

		$this->brid = $this->session->userdata('brid');
	}

	public function index() {
		redirect('staff/add');
	}

	public function add() {
		unauth_secure();
		$data['modules'] = array('staff');
		$data['types'] = $this->staffs->getAllTypes($this->brid);
		$data['agreements'] = $this->staffs->getAllAgreements($this->brid);
		$data['religions'] = $this->staffs->getAllReligions($this->brid);
		$data['banks'] = $this->staffs->getAllBankNames($this->brid);
		$data['page_title'] = 'Add Staff ';
		
		$this->load->view('template/header',$data);
		$this->load->view('setup/addstaff.php', $data);

		$this->load->view('template/mainnav.php');
		$this->load->view('template/footer.php', $data);
	}

	public function getMaxId() {

		$result = $this->staffs->getMaxId($this->brid) + 1;

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST)) {

			$partyDetail = json_decode(html_entity_decode($_POST['acc'], true));
			$partyDetail = (array)$partyDetail;
			$ldetail = $this->levels->getLevel3ByName('asset employeed', $this->brid);

			$partyDetail['level3'] = $ldetail['l3'];
			$partyDetail['account_id'] = $this->levels->genAccStr($partyDetail['level3'], $this->brid);
			$partyDetail['pid'] = ($partyDetail['pid'] == '') ? ($this->accounts->getMaxId($this->brid) + 1) : $partyDetail['pid'];
			$pid = $this->accounts->save($partyDetail);

			$staid = $this->staffs->save($_POST, $pid, $this->brid);

			$salary = (array)(json_decode(html_entity_decode($_POST['salary'], true)));
			$this->staffs->saveSalary($salary);

			$qualifications = (array)(json_decode(html_entity_decode($_POST['quali'], true)));
			$this->staffs->saveQualification($qualifications, $staid);

			$experiences = (array)(json_decode(html_entity_decode($_POST['exp'], true)));
			$this->staffs->saveExperience($experiences, $staid);

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode('true'));
		}
	}

	public function fetchStaff() {

		if (isset( $_POST )) {

			$staid = $_POST['staid'];

			$staff = $this->staffs->fetchStaff($staid, $this->brid);
			$salary = $this->staffs->fetchStaffSalary($staid);
			$quali = $this->staffs->fetchStaffQualification($staid);
			$exp = $this->staffs->fetchStaffExperience($staid);

			$response = array();
			if ( $staff === false ) {
				$response = 'false';
			} else {
				$response['staff'] = $staff;
				$response['salary'] = $salary;
				$response['quali'] = $quali;
				$response['exp'] = $exp;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchStaffReportByStatus() {

		if (isset( $_POST )) {

			$status = $_POST['status'];
			$results = $this->staffs->fetchStaffReportByStatus($status, $this->brid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAllTeachers() {

		if (isset( $_POST )) {
			$results = $this->staffs->fetchAllTeachers($this->brid);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
}