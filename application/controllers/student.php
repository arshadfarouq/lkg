<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Controller {

	private $brid = "";

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('students');
		$this->load->model('subjects');
		$this->load->model('admissions');
		$this->load->model('branches');
		$this->load->model('groups');
		$this->load->model('fees');
		$this->load->model('levels');
        $this->load->model('users');
        $this->load->model('files_model');


		// $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
		// $this->load->library('upload');
        $this->brid = $this->session->userdata('brid');
    }

    public function index() {
      redirect('student/addStudent');
  }



  public function addStudent() {
      unauth_secure();
      $data['modules'] = array('student');
      $data['_classes'] = $this->groups->fetchAllClasses($this->brid);
      $data['sections'] = $this->groups->fetchAllSections($this->brid);
      $data['feeCategories'] = $this->fees->fetchAll($this->brid);
      $data['subjects'] = $this->subjects->fetchAll($this->brid);
      $data['previousSchools'] = $this->students->getPreviousSchools($this->brid);
      $data['students'] = $this->students->fetchAll('', $this->brid);
      $data['allStudents'] =  $this->students->fetchAllStudent($this->brid);

      $data['page_title'] = 'Add Student ';
      $this->load->view('template/header',$data);
      $this->load->view('setup/addstudent', $data);
      $this->load->view('template/mainnav');
      $this->load->view('template/footer', $data);
  }
  public function searchStudentAll()
  {
        $search = $_POST['search']; // trim(Input::get('search'));
        $type = $_POST['type']; ; //trim(Input::get('type'));

        $result = $this->students->searchStudentAll($search,$type);
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }
    public function searchStudent()
    {
        $search = $_POST['search']; // trim(Input::get('search'));
        $type = $_POST['type']; ; //trim(Input::get('type'));

        $result = $this->students->searchStudent($search,$type);
        

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }
    public function fetchAll_Students() {

    	if (isset( $_POST )) {

    		$from = $_POST['from'];
    		$to = $_POST['to'];
    		$orderby = $_POST['orderby'];
    		$status = $_POST['status'];
    		$crit='';
    		if(isset($_POST['crit']))
    			$crit = $_POST['crit'];

    		$result = $this->students->fetchAll_report($from,$to,$orderby,$status,$crit);

    		$response = "";
    		if ( $result === false ) {
    			$response = 'false';
    		} else {
    			$response = $result;
    		}
    		$json = json_encode($response);
    		echo $json;
    	}
    }
    public function upload_file()
    {
    	$status = "";
    	$msg = "";
    	$file_element_name = 'userfile';

    	if (empty($_POST['title']))
    	{
    		$status = "error";
    		$msg = "Please enter a title";
    	}

    	if ($status != "error")
    	{
    		$config['upload_path'] = './files/';
    		$config['allowed_types'] = 'gif|jpg|png|doc|txt';
    		$config['max_size'] = 1024 * 8;
    		$config['encrypt_name'] = TRUE;

    		$this->load->library('upload', $config);

    		if (!$this->upload->do_upload($file_element_name))
    		{
    			$status = 'error';
    			$msg = $this->upload->display_errors('', '');
    		}
    		else
    		{
    			$data = $this->upload->data();
    			$file_id = $this->files_model->insert_file($data['file_name'], $_POST['title']);
    			if($file_id)
    			{
    				$status = "success";
    				$msg = "File successfully uploaded";
    			}
    			else
    			{
    				unlink($data['full_path']);
    				$status = "error";
    				$msg = "Something went wrong when saving the file, please try again.";
    			}
    		}
    		@unlink($_FILES[$file_element_name]);
    	}
    	echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function batchUpdate() {
    	unauth_secure();
    	$data['modules'] = array('batchupdate');
    	$data['branches'] = $this->branches->fetchAll();

    	$data['page_title'] = 'Add Account ';
        $this->load->view('template/header',$data);

        $this->load->view('setup/batchupdate', $data);

        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function transfer() {
    	unauth_secure();
    	$data['modules'] = array('transferstudent');

    	$data['page_title'] = 'Add Account ';
        $this->load->view('template/header',$data);
        $this->load->view('setup/transferstudent', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function struckoffReadmit() {
    	unauth_secure();
    	$data['modules'] = array('struckoffreadmit');
    	$data['students'] = $this->students->fetchAll('all', $this->brid);

    	$data['page_title'] = 'Add Account Voucher';
        $this->load->view('template/header',$data);

        $this->load->view('setup/struckoffreadmit', $data);

        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function getMaxId() {

    	$result = $this->students->getMaxId($this->brid) + 1;

    	$this->output
    	->set_content_type('application/json')
    	->set_output(json_encode($result));
    }

    public function saveMultiple() {

    	if (isset($_POST)) {

    		$studentsDetail = $_POST['studentsDetail'];
    		$result = $this->students->saveMultiple($studentsDetail, $this->brid);

    		$response = array();
    		if ( $result === false ) {
    			$response['error'] = 'true';
    		} else {
    			$response['error'] = 'false';
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function save() {

    	if (isset($_POST)) {

    		$admiMain = json_decode(html_entity_decode($_POST['admiMain'], true));
    		$admiDetail = json_decode(html_entity_decode($_POST['admiDetail'], true));
    		$kinships = json_decode(html_entity_decode($_POST['kinships'], true));
    		unset($_POST['admiMain']);
    		unset($_POST['admiDetail']);
    		unset($_POST['kinships']);

			// if (isset($kinships) && count($kinships) > 0)
    		$this->students->saveKinship($kinships, $this->brid, $_POST['stdid']);

    		$partyDetail = $this->preparePartyAccount($_POST);
    		$partyDetail['account_id'] = $this->levels->genAccStr($partyDetail['level3'], $this->brid);
    		$partyDetail['pid'] = ($partyDetail['pid'] == '') ? ($this->accounts->getMaxId($this->brid) + 1) : $partyDetail['pid'];
    		$pid = $this->accounts->save($partyDetail, $this->brid);

    		$result = $this->students->save($_POST, $pid, $this->brid);

    		$response = array();
    		$admiDetailTemp = array();

    		if ( $result === false ) {
    			$response['error'] = 'true';
    		} else {

    			$admiMain = (array)$admiMain;
    			$admiDetail = (array) $admiDetail;

				// check if its empty
    			if (count($admiDetail) > 0) {

    				$aid = $this->admissions->save( (array)$admiMain );

    				foreach ($admiDetail as $detail) {

    					$detail = (array)$detail;
    					$detail['aid'] = $aid;
    					$admiDetailTemp[] = $detail;
    				}
    				$this->admissions->saveDetail( $admiDetailTemp, $aid );
    			}

    			$response['error'] = 'false';
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($_POST));
    	}
    }
    public function saveExecel() {

    	if (isset($_POST)) {

			// $admiMain = json_decode(html_entity_decode($_POST['admiMain'], true));
			// $admiDetail = json_decode(html_entity_decode($_POST['admiDetail'], true));
			// $kinships = json_decode(html_entity_decode($_POST['kinships'], true));
			// unset($_POST['admiMain']);
			// unset($_POST['admiDetail']);
			// unset($_POST['kinships']);

			// if (isset($kinships) && count($kinships) > 0)
			// 	$this->students->saveKinship($kinships, $this->brid, $_POST['stdid']);

			// $partyDetail = $this->preparePartyAccount($_POST);
			// $partyDetail['account_id'] = $this->levels->genAccStr($partyDetail['level3'], $this->brid);
			// $partyDetail['pid'] = ($partyDetail['pid'] == '') ? ($this->accounts->getMaxId($this->brid) + 1) : $partyDetail['pid'];
			// $pid = $this->accounts->save($partyDetail, $this->brid);

    		$result = $this->students->saveExecel($_POST['studentExcelObj'], $this->brid);

    		$response = array();
			// $admiDetailTemp = array();

    		if ( $result === false ) {
    			$response['error'] = 'true';
    		}else {
    			$response['error'] = 'false';
    		}
    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($_POST));
    	}
    }

    public function updateBatch() {

    	if (isset($_POST)) {

    		$studentObj = $_POST['studentObj'];

    		$result = $this->students->updateBatch($studentObj, $this->brid);


    		$response = array();

    		if ( $result === false ) {
    			$response['error'] = 'true';
    		} else {

    			$response['error'] = 'false';
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function preparePartyAccount($data) {

		// prepare the data to save
    	$studentData = array();
    	$studentData['pid'] = $data['pid'];
    	$studentData['active'] = "1";
    	$studentData['name'] = $data['name'] . ' ' . $data['stdid'];
    	$studentData['brid'] = $data['brid'];

    	$result = $this->levels->getLevel3ByName('asset employeed', $this->brid);
    	$studentData['level3'] = $result['l3'];

    	$studentData['dcno'] = '0';
    	$studentData['phone'] = $data['phone'];
    	$studentData['mobile'] = $data['mobile'];
    	$studentData['etype'] = 'student';
    	$studentData['address'] = $data['address'];

    	return $studentData;
    }

    public function fetchStudent() {

    	if (isset( $_POST )) {

    		$stdid = $_POST['stdid'];

    		$studentDetail = $this->students->fetchStudent($stdid, $this->brid);
    		$testDetail = $this->admissions->fetch($stdid, $this->brid);
    		$kinships = $this->students->fetchAllKinships($stdid, $this->brid);

    		$response = array();
    		if ( $studentDetail === false ) {
    			$response = 'false';
    		} else {
    			$response['studentDetail'] = $studentDetail;
    			$response['testDetail'] = $testDetail;
    			$response['kinships'] = $kinships;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }


    public function fetchAll() {

    	$students = $this->students->fetchAll();
    	$this->output
    	->set_content_type('application/json')
    	->set_output(json_encode($students));
    }

    public function fetchStudentByPid() {

    	if (isset( $_POST )) {

    		$pid = $_POST['pid'];

    		$studentDetail = $this->students->fetchStudentByPid($pid, $this->brid);

    		$response = '';
    		if ( $studentDetail === false ) {
    			$response = 'false';
    		} else {
    			$response = $studentDetail;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }



    public function fetchStudentByBranchClassSection() {

    	if (isset( $_POST )) {

    		$brid = $_POST['brid'];
    		$claid = $_POST['claid'];
    		$secid = $_POST['secid'];

    		$results = $this->students->fetchStudentByBranchClassSection($this->brid, $claid, $secid);

    		$response = array();
    		if ( $results === false ) {
    			$response = 'false';
    		} else {
    			$response = $results;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function fetchStuReportByStatus() {

    	if (isset( $_POST )) {

    		$status = $_POST['status'];
    		$brid = $_POST['brid'];
    		$claid = $_POST['claid'];
    		$secid = $_POST['secid'];

    		$results = $this->students->fetchStuReportByStatus($status, $this->brid, $claid, $secid);

    		$response = array();
    		if ( $results === false ) {
    			$response = 'false';
    		} else {
    			$response = $results;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($response));
    	}
    }

    public function admissionWithdrawlReport() {

    	if (isset( $_POST )) {

    		$brid = $_POST['brid'];

    		$results = $this->students->admissionWithdrawlReport($brid);

    		$response = array();
    		if ( $results === false ) {
    			$response = 'false';
    		} else {
    			$response = $results;
    		}

    		$this->output
    		->set_content_type('application/json')
    		->set_output(json_encode($results));
    	}
    }
}

/* End of file student.php */
/* Location: ./application/controllers/student.php */