<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject extends CI_Controller {

	private $brid = '';
	public function __construct() {
		parent::__construct();
		$this->load->model('subjects');
		$this->load->model('staffs');
		$this->load->model('branches');

		$this->brid = $this->session->userdata('brid');
	}

	public function index() {
		redirect('subject/add');
	}

	public function add() {
		unauth_secure();
		$data['modules'] = array('subject');
		$data['subjects'] = $this->subjects->fetchAll($this->brid);

		$data['page_title'] = 'Add Subject Voucher';
		$this->load->view('template/header',$data);

		$this->load->view('setup/addsubject', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function assignToClass() {
		unauth_secure();
		$data['modules'] = array('assignsubjecttoclass');
		$data['subjects'] = $this->subjects->fetchAll($this->brid);

		$data['page_title'] = 'Add Assign To Class Voucher';
		$this->load->view('template/header',$data);

		$this->load->view('setup/assignsubjecttoclass', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function assignToTeacher() {
		unauth_secure();
		$data['modules'] = array('assignsubjecttoteacher');
		$data['subjects'] = $this->subjects->fetchAll($this->brid);
		$data['teachers'] = $this->staffs->fetchAllTeachers($this->brid);

		$data['page_title'] = 'Add Assign To Teacher Voucher';
		$this->load->view('template/header',$data);

		$this->load->view('setup/assignsubjecttoteacher', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchAll() {

		$result = $this->subjects->fetchAll($this->brid);

		$response = "";
		if ($result === false) {

			$response = 'false';
		} else {

			$response = $result;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

	public function getMaxId() {

		$maxId = $this->subjects->getMaxId($this->brid) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxIdSubjectClass() {

		$maxId = $this->subjects->getMaxIdSubjectClass($this->brid) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function getMaxIdSubjectTeacher() {

		$maxId = $this->subjects->getMaxIdSubjectTeacher($this->brid) + 1;
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($maxId));
	}

	public function save() {

		if (isset($_POST)) {

			$subjectDetail = $_POST['subjectDetail'];

			$result = $this->subjects->save( $subjectDetail, $this->brid );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function saveAssignSubjecttoClass() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$claid = $_POST['claid'];
			$brid = $this->session->userdata('brid');
			$subjectAssignObj = $_POST['subjectAssignObj'];

			$error = $this->subjects->isSubjAlreadyAssignedToClass($dcno, $claid, $brid);

			if (!$error) {

				$result = $this->subjects->saveAssignSubjecttoClass( $subjectAssignObj, $dcno, $this->brid );

				$response = array();
				if ($result === false) {
					$response['error'] = true;
				} else {
					$response['error'] = false;
				}

				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			} else {
				$response = array();
				$response['error'] = 'duplicate';
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			}
		}
	}

	public function saveAssignSubjecttoTeacher() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$brid = $this->session->userdata('brid');
			$sdetail = $_POST['sdetail'];

			$error = $this->subjects->isSubjAlreadyAssignedToTeacher($dcno, $claid, $secid, $brid);

			if (!$error) {

				$result = $this->subjects->saveAssignSubjecttoTeacher( $sdetail, $dcno, $this->brid );

				$response = array();
				if ($result === false) {
					$response['error'] = true;
				} else {
					$response['error'] = false;
				}

				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			} else {

				$response = array();
				$response['error'] = 'duplicate';
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			}


		}
	}

	public function fetchSubject() {

		if (isset( $_POST )) {

			$sbid = $_POST['sbid'];

			$result = $this->subjects->fetchSubject($sbid, $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAssignedSubjectstoClass() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];

			$result = $this->subjects->fetchAssignedSubjectstoClass($dcno, $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchAssignedSubjectstoTeacher() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];

			$result = $this->subjects->fetchAssignedSubjectstoTeacher($dcno, $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteVoucher() {

		if (isset($_POST)) {

			$result = $this->subjects->deleteVoucher($_POST['dcno'], $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function deleteVoucherST() {

		if (isset($_POST)) {

			$result = $this->subjects->deleteVoucherST($_POST['dcno'], $this->brid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function subjectViewReport() {

		$result = $this->subjects->subjectViewReport($this->brid);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function subjectViewClassWiseReport() {

		$result = $this->subjects->subjectViewClassWiseReport($this->brid);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function subjectViewTeacherWiseReport() {

		$result = $this->subjects->subjectViewTeacherWiseReport($this->brid);

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($result));
	}

	public function fetchSubjectByBrNCls() {

		if (isset($_POST)) {

			$brid = $_POST['brid'];
			$claid = $_POST['claid'];

			$result = $this->subjects->fetchSubjectByBrNCls( $this->brid, $claid );

			$response = "";
			if ($result == false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchTeacherNameByBrClsSecNSbj() {

		if (isset($_POST)) {

			$brid = $_POST['brid'];
			$claid = $_POST['claid'];
			$secid = $_POST['secid'];
			$sbid = $_POST['sbid'];

			$result = $this->subjects->fetchTeacherNameByBrClsSecNSbj( $brid, $claid, $secid, $sbid );

			$response = "";
			if ($result == false) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}
}

/* End of file subject.php */
/* Location: ./application/controllers/subject.php */