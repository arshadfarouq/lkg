<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('branches');
		$this->load->model('users');
	}


	public function index() {

		// if that stupid user is already logged in then redirect him to dashboard
		auth_secure();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('uname', 'Username', 'required');
		$this->form_validation->set_rules('pass', 'Password', 'required|callback_has_match');

		if ($this->form_validation->run() == false) {

			$data['errors'] = isset($_POST['submit']) ? true : false;
			
			$this->load->view('template/loginheader');
			$this->load->view('user/login', $data);
			$this->load->view('template/loginfooter');
			
		} else {
			$this->users->login_user($_POST);
			redirect('user/dashboard');
			// redirect('account');
		}
		
	}

	public function has_match()
	{
		$username = $this->input->post('uname');
		$password = $this->input->post('pass');


		if ($this->users->has_match($username, $password) === true) {
			return true;
		}
		else {
			$this->form_validation->set_message('has_match', 'Invalid Username/password entered');
			return false;
		}
	}

	public function dashboard()
	{
		unauth_secure();
		// $data['modules'] = array('user/dashboard');

		$data['page_title'] = 'Dashboard ';
		$this->load->view('template/header',$data);
		$this->load->view('user/dashboard', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function add()
	{
		unauth_secure();
		$data['modules'] = array('user/adduser');
		$data['branches'] = $this->branches->fetchAll();
		$data['rolegroups'] = $this->users->fetchAllRoleGroup();
		$data['users'] = $this->users->fetchAllUsers();

		$data['page_title'] = 'Add User ';
		$this->load->view('template/header',$data);
		$this->load->view('user/adduser', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function privillages()
	{
		unauth_secure();
		$data['modules'] = array('user/privillagesgroup');
		$data['page_title'] = 'Add Privillages ';
		$this->load->view('template/header',$data);

		$this->load->view('user/privillagesgroup');

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxRoleGroupId() {

		$result = $this->users->getMaxRoleGroupId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getMaxId() {

		$result = $this->users->getMaxId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function saveRoleGroup() {

		if (isset($_POST)) {

			$rolegroup = $_POST['data'];
			$result = $this->users->saveRoleGroup( $rolegroup );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function save() {

		if (isset($_POST)) {

			$user = $_POST['user'];
			$result = $this->users->save( $user );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetchRoleGroup() {

		if (isset( $_POST )) {

			$rgid = $_POST['rgid'];
			$result = $this->users->fetchRoleGroup($rgid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$uid = $_POST['uid'];
			$result = $this->users->fetch($uid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
		}
	}

	public function privillagesAssigned() {

		$result = $this->users->privillagesAssigned();
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function login() {

		if (isset($_POST)) {

			$uname = $_POST['uname'];
			$pass = $_POST['pass'];

			$result = $this->users->login($uname, $pass);
			
			var_dump($this->session->all_userdata());

			$response = array();
			if ($result == false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}


			return $this->output->set_content_type('application/json')->set_output(json_encode($response));
		}
	}

	public function logout() {

		unauth_secure();
		$this->session->sess_destroy();
		redirect('user');
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */