<?php

	// redirect user to login page if its not an authenticate user
	function unauth_secure() {
		// get current codeigneter instance
		$CI =& get_instance();
		$user = $CI->session->userdata('uid');

		if ($user == false) {
			redirect('user');
		}
	}

	// redirect user to the dashboard if he's already logged in
	function auth_secure() {

		// get the current codeigneter instance
		$CI =& get_instance();
		$user = $CI->session->userdata('uid');

		if ($user != false) {
			redirect('user/dashboard');
		}
	}

?>