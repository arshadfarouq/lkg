<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId() {

		$this->db->select_max('pid');
		$result = $this->db->get('party');

		$row = $result->row_array();
		$maxId = $row['pid'];
		return $maxId;
	}
	public function getMaxIdPls() {

		$this->db->select_max('vrnoa');
		$result = $this->db->get('plsmain');

		$row = $result->row_array();
		$maxId = $row['vrnoa'] + 1;

		return $maxId;

	}
	public function getPartnerCapitalReportData( $to, $company_id )
{
	$query = "  SELECT party.PID,party.NAME, IFNULL(led.AMOUNT,0) 'BALANCE'
	FROM party
	INNER JOIN level3 ON party.level3 = level3.l3
	INNER JOIN level2 ON level2.l2 = level3.l2
	INNER JOIN level1 ON level1.l1 = level2.l1
	left join(
		SELECT pid, IFNULL(SUM(CREDIT),0) - IFNULL(SUM(DEBIT),0) AS 'AMOUNT'
		FROM pledger
		WHERE DATE <='{$to}' $company_id
		GROUP BY pid
		) led ON led.pid = party.pid
WHERE  level3.name ='PARTNER CAPITAL ACCOUNT' ";
$result = $this->db->query($query);
return $result->result_array();
}
	public function savePls($plsmain, $plspartner,$plscompany,$plsexpense,$plsopeningstock,$plsclosingstock, $vrnoa, $etype)
	{

		$company_id = $plsmain['company_id'];


		$this->db->where(array('vrnoa' => $vrnoa, 'company_id' => $plsmain['company_id']));
		$result = $this->db->get('plsmain');


		$vrnoa = "";
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$vrnoa = $result['vrnoa'];
			$this->db->where(array('vrnoa' => $vrnoa, 'company_id' => $plsmain['company_id']));
			$this->db->update('plsmain', $plsmain);

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plscompany');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plspartner');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsexpense');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsopeningstock');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsclosingstock');

		} else {

			$vrnoa = $this->getMaxIdPls();
			$this->db->insert('plsmain', $plsmain);

		}

		foreach ($plspartner as $detail) {
			$detail['vrnoa'] = $vrnoa;
			$this->db->insert('plspartner', $detail);
		}
		foreach ($plscompany as $detail) {
			$detail['vrnoa'] = $vrnoa;
			$this->db->insert('plscompany', $detail);
		}
		foreach ($plsexpense as $detail) {
			$detail['vrnoa'] = $vrnoa;
			$this->db->insert('plsexpense', $detail);
		}
		foreach ($plsopeningstock as $detail) {
			$detail['vrnoa'] = $vrnoa;
			$this->db->insert('plsopeningstock', $detail);
		}

		foreach ($plsclosingstock as $detail) {
			$detail['vrnoa'] = $vrnoa;
			$this->db->insert('plsclosingstock', $detail);
		}


		return $vrnoa;
	}
	public function deletePls($vrnoa, $etype, $company_id)
	{

		$this->db->where(array('etype' => $etype, 'dcno' => $vrnoa, 'company_id' => $company_id));
		$result = $this->db->delete('pledger');

		$this->db->where(array('vrnoa' => $vrnoa, 'company_id' => $company_id));
		$result = $this->db->get('plsmain');

		if ($result->num_rows() == 0) {
			return false;
		} else {



			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plscompany');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plspartner');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsexpense');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsopeningstock');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsclosingstock');

			$this->db->where(array('vrnoa' => $vrnoa));
			$this->db->delete('plsmain');

			return true;
		}
	}
	public function isPlsAlreadySaved($sdate, $edate, $vrnoa)
	{
		$qry ="SELECT * from plsmain where sdate <='{$edate}' and edate>='{$sdate}' and vrnoa<>$vrnoa ";

		$result = $this->db->query($qry);
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function fetchPlsMain($vrnoa) {

		$this->db->where(array(
			'vrnoa' => $vrnoa
			));
		$result = $this->db->get('plsmain');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	public function CheckDuplicateCheck($vrnoa, $etype,$company_id,$chk_no) {

		$result = $this->db->query("SELECT dcno FROM pledger WHERE etype = '". $etype ."' and company_id=". $company_id ." and chq_no='". $chk_no ."' and dcno<>". $vrnoa ."  ");
		// $row = $result->row_array();
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	public function fetchOpeningBalance_AccountsCashFlow($from, $pid, $companyId)
	{
		$result = $this->db->query("SELECT IFNULL(SUM(pledger.DEBIT), 0)-IFNULL(SUM(pledger.CREDIT),0) as 'OPENING_BALANCE'
			FROM pledger
			Inner join party on party.pid = pledger.pid
			Inner join level3 on level3.l3 = party.level3
			WHERE level3.name = 'CASH & BANK' AND `date` < '" . $from . "' and pledger.company_id = '" . $companyId . "' ");
		return $result->result_array();
	}
	public function searchAccount($search,$type)
	{
		$crit = "";
		$activee = 1;

		$qry="SELECT IFNULL(pl.balance,0) AS balance, IFNULL(party.city,'') AS city, IFNULL(party.address,'') AS address,
		IFNULL(party.cityarea,'') AS cityarea,  IFNULL(party.mobile,'') AS mobile,
		`party`.pid, `party`.pid AS party_id,`party`.name, ROUND(IFNULL(`party`.limit,0),0) AS `limit`, `party`.account_id, 
		`party`.level3, level3.name AS level3_name
		FROM `party`
		INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
		LEFT JOIN (
		SELECT IFNULL(SUM(debit),0)- IFNULL(SUM(credit),0) AS balance,pid
		FROM pledger
		GROUP BY pid) AS pl ON pl.pid= `party`.`pid`
		WHERE party.active = 1 AND (party.name LIKE '$search%' OR party.mobile LIKE '$search%' OR party.pid LIKE '$search%' OR party.address LIKE '$search%' OR party.city LIKE '$search%' OR party.cityarea LIKE '$search%' OR  level3.name LIKE '$search%')
		LIMIT 0, 20";


		$query= $this->db->query($qry);
		$result = $query->result_array();
		return $result;
	}
	public function searchAccountAll($search,$type)
	{
		$crit = "";
		$activee = 1;

		$qry="SELECT ifnull(pl.balance,0) as balance,party.city,party.address,party.cityarea
		,`party`.uname,party.mobile,`party`.pid,
		`party`.pid AS party_id,`party`.name,`party`.uname, `party`.limit, 
		`party`.account_id, `party`.mobile, `party`.address, `party`.level3, 
		level3.name AS level3_name
		FROM `party`
		INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
		LEFT JOIN (
		SELECT IFNULL(SUM(debit),0)- IFNULL(SUM(credit),0) AS balance,pid
		FROM pledger
		GROUP BY pid) AS pl ON pl.pid= `party`.`pid`
		WHERE party.active = 1 AND (party.name LIKE '%". $search ."%' 
		OR party.mobile LIKE '%". $search ."%'
		OR party.pid LIKE '%". $search ."%'
		OR party.address LIKE '%". $search ."%'
		OR party.city LIKE '%". $search ."%'
		OR party.cityarea LIKE '%". $search ."%'
		OR party.uname LIKE '%". $search ."%'
		OR level3.name LIKE '%". $search ."%' ) ;";


		$query= $this->db->query($qry);
		$result = $query->result_array();
		return $result;
	}

	public function getAccountinfobyid($pid)
	{

		$qry="SELECT ifnull(pl.balance,0) as balance,party.city,party.address,party.cityarea
		,`party`.name,party.mobile,`party`.pid,
		`party`.pid AS party_id,`party`.name,`party`.name, round(ifnull(`party`.limit,0),0) as `limit`, 
		`party`.account_id, `party`.mobile, `party`.address, `party`.level3, 
		level3.name AS level3_name
		FROM `party`
		INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
		LEFT JOIN (
		SELECT IFNULL(SUM(debit),0)- IFNULL(SUM(credit),0) AS balance,pid
		FROM pledger
		GROUP BY pid) AS pl ON pl.pid= `party`.`pid`
		WHERE party.active = 1 and party.pid=$pid
		LIMIT 0, 20;";
		// echo $qry;

		$query= $this->db->query($qry);
		$result = $query->result_array();
		return $result;
	}
	public function getIncomeAccounts()
	{
		$query = "select party_id, party.name from party inner join level3 on party.level3 = level3.l3 INNER join level2 on level2.l2 = level3.l2 WHERE level2.name='other income'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchNetOperatingExpenses($from, $to, $company_id) 
	{
		// $query = "SELECT party.NAME, SUM(DEBIT) as 'AMOUNT' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level1.name='expense' AND pledger.company_id={$company_id} AND pledger.DATE BETWEEN '{$from}' AND '{$to}' GROUP BY pledger.pid";
		$query = "SELECT SUM(DEBIT) as 'AMOUNT'FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level2.name in('OPERATING EXPENSES','OPPERATING EXP') $company_id AND pledger.DATE BETWEEN '{$from}' AND '{$to}'GROUP BY level2.name";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchNetExpense( $from, $to, $company_id)
	{
		$query = "SELECT SUM(debit) as 'EXPENSE_TOTAL' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level1.l1='EXPENSES' AND level1.name <> 'COST OF GOODS SOLD' $company_id AND pledger.date BETWEEN '{$from}' AND '{$to}' GROUP BY level1.name";
		$result =  $this->db->query($query);
		return $result->result_array();
	}
	public function fetchClosingStockReportData( $from, $to, $company_id )
	{
		
		$query = "SELECT cat.name AS DESCRIPTION,i.uom AS UOM,i.item_des NAME,i.item_id, dep.NAME AS dept_name, IF(i.uom='dozen', ROUND(IFNULL(SUM(d.qty)/12,0),0), ROUND(IFNULL(SUM(d.qty),0),0)) AS qty, ROUND(IFNULL(lp.rate,0),2) AS cost, ROUND(IFNULL(SUM(d.qty),0)* IFNULL(lp.rate,0),2) AS value
		FROM stockdetail d
		INNER JOIN item i ON i.item_id=d.item_id
		INNER JOIN brand b ON b.bid=i.bid
		INNER JOIN category cat ON cat.catid=i.catid
		INNER JOIN subcategory subcat ON subcat.subcatid=i.subcatid
		INNER JOIN stockmain m ON m.stid = d.stid
		INNER JOIN company ON company.company_id = m.company_id
		LEFT JOIN department dep ON dep.did = d.godown_id
		
		LEFT JOIN (
		SELECT d.item_id,d.rate AS rate
		FROM orderdetail d
		WHERE d.qty>0 AND d.oid=(
		SELECT `oid`
		FROM orderdetail
		WHERE item_id=d.item_id
		ORDER BY oid DESC
		LIMIT 1)
		GROUP BY d.item_id
		ORDER BY d.item_id) AS lp ON lp.item_id=d.item_id
		WHERE m.VRDATE <= '". $to ."' AND m.company_id<>0 $company_id
		GROUP BY cat.name,i.item_des";
		$result = $this->db->query( $query );
		return $result->result_array();
	}

	// public function fetchOpeningStockReportData( $from, $to, $company_id )
	// {
	// 	$query = "(SELECT SUM(QTY) as 'QTY', stockdetail.PRATE,  item.item_des as DESCRIPTION,  SUM(QTY)*PRATE as 'NETVALUE'FROM stockmain INNER JOIN stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN item ON item.item_id = stockdetail.item_id WHERE stockmain.company_id={$company_id} AND stockmain.vrdate < '{$from}' AND stockmain.etype <> 'pur_import'  GROUP BY stockdetail.item_id, PRATE HAVING IFNULL(sum(qty), 0) > 0 ORDER BY PRATE ) UNION ALL (SELECT SUM(QTY) as 'QTY', stockdetail.RATE as 'PRATE',  item.item_des as DESCRIPTION,  SUM(QTY)*RATE as 'NETVALUE'FROM stockmain INNER JOIN stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN item ON item.item_id = stockdetail.item_id WHERE stockmain.company_id={$company_id} AND stockmain.vrdate < '{$from}' AND stockmain.etype='pur_import'  GROUP BY stockdetail.item_id, RATE HAVING IFNULL(sum(qty), 0) > 0 ORDER BY RATE )";
	
	// 	$result = $this->db->query( $query );
	// 	return $result->result_array();
	// }
	public function fetchOpeningStockReportData( $from, $to, $company_id )
	{


		$query = "SELECT cat.name AS DESCRIPTION,i.uom AS UOM,i.item_des NAME,i.item_code, dep.NAME AS dept_name, IF(i.uom='dozen', ROUND(IFNULL(SUM(d.qty)/12,0),0), ROUND(IFNULL(SUM(d.qty),0),0)) AS qty, ROUND(IFNULL(lp.rate,0),2) AS cost, ROUND(IFNULL(SUM(d.qty),0)* IFNULL(lp.rate,0),2) AS value
		FROM stockdetail d
		INNER JOIN item i ON i.item_id=d.item_id
		INNER JOIN category cat ON cat.catid=i.catid
		INNER JOIN stockmain m ON m.stid = d.stid
		INNER JOIN company ON company.company_id = m.company_id
		LEFT JOIN department dep ON dep.did = d.godown_id
		LEFT JOIN (
		SELECT d.item_id,d.rate AS rate
		FROM stockdetail d
		WHERE d.qty>0 AND d.stid=(
		SELECT main.stid
		FROM stockdetail det
		inner join stockmain main on main.stid = det.stid
		WHERE det.item_id=d.item_id and main.etype = 'purchase'
		ORDER BY det.stid DESC
		LIMIT 1)
		GROUP BY d.item_id
		ORDER BY d.item_id) AS lp ON lp.item_id=d.item_id
		WHERE m.vrdate < '". $from ."' AND m.company_id<>0
		GROUP BY cat.name,i.item_des";

		$result = $this->db->query( $query );
		return $result->result_array();

		// $query = $this->db->query("call spw_Closing_Stock('$from', $company_id)");
		// return $query->result_array();

	}

	public function fetchOtherIncomeSum( $from, $to, $company_id )
	{
		$query = "SELECT IFNULL(SUM(pledger.credit), 0)-IFNULL(SUM(pledger.debit), 0) as 'INCOME_TOTAL' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level3.l2 = level2.l2 WHERE level2.name = 'other income' AND pledger.date BETWEEN '{$from}' AND '{$to}' $company_id GROUP BY level2.name";
		$result =  $this->db->query($query);
		return $result->result_array();
	}

	public function getIncomeReportData( $from, $to, $company_id )
	{
		$query = "SELECT party.NAME, ifnull(SUM(credit),0)-ifnull(SUM(debit),0) as 'AMOUNT' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2  WHERE level2.name='other income' $company_id AND pledger.DATE BETWEEN '{$from}' AND '{$to}' GROUP BY party.name HAVING IFNULL(SUM(pledger.credit),0)-IFNULL(SUM(pledger.debit),0)<>0;";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function getExpenseReportData( $from, $to, $company_id )
	{
		$query = "SELECT party.NAME, SUM(DEBIT) as 'AMOUNT' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level1.l1 ='EXPENSES' AND level2.name <> 'COST OF GOODS SOLD' $company_id AND pledger.DATE BETWEEN '{$from}' AND '{$to}' GROUP BY pledger.pid HAVING IFNULL(SUM(DEBIT),0)<>0;";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchBalanceSheet( $startDate, $endDate, $company_id, $type )
	{
		$query = '';

		if ($type === 'ASSETS') {
			$query = "SELECT level1.name,round(SUM(DEBIT)-SUM(CREDIT),0) AS 'AMOUNT', party.NAME AS 'PARTY_NAME', party.ACCOUNT_ID, level1.name AS 'L1NAME', level2.name AS 'L2NAME', level3.name AS 'L3NAME' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level2.l1 = level1.l1 WHERE level1.name = 'ASSETS' AND pledger.date BETWEEN '{$startDate}' AND '{$endDate}' $company_id GROUP BY party.name ORDER BY level1.name,account_id";
		} else if ($type === 'LIABILITIES AND OWNER S EQUITY') {
			$query = "SELECT level1.name,round(SUM(CREDIT)-SUM(DEBIT),0) AS 'AMOUNT', party.NAME AS 'PARTY_NAME', party.ACCOUNT_ID, level1.name AS 'L1NAME', level2.name AS 'L2NAME', level3.name AS 'L3NAME' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level2.l1 = level1.l1 WHERE level1.name = 'LIABILITIES AND OWNER S EQUITY' AND pledger.date BETWEEN '{$startDate}' AND '{$endDate}' $company_id GROUP BY party.name ORDER BY level1.name,account_id";
		}

		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchNetFinanceCost($from, $to, $company_id) 
	{
		$query = "SELECT SUM(DEBIT) as 'AMOUNT' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level2.name='finance cost' $company_id AND pledger.DATE BETWEEN '{$from}' AND '{$to}'GROUP BY level2.name";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchNetPFT($from, $to, $company_id) 
	{
		$query = "SELECT SUM(DEBIT) as 'AMOUNT' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level2.name='provision for taxation' $company_id AND pledger.DATE BETWEEN '{$from}' AND '{$to}'GROUP BY level2.name";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchNetWPPF($from, $to, $company_id) 
	{
		$query = "SELECT SUM(DEBIT) as 'AMOUNT' FROM pledger INNER JOIN party ON pledger.pid = party.pid INNER JOIN level3 ON party.level3 = level3.l3 INNER JOIN level2 ON level2.l2 = level3.l2 INNER JOIN level1 ON level1.l1 = level2.l1 WHERE level2.name='worker profit participation fund' $company_id AND pledger.DATE BETWEEN '{$from}' AND '{$to}'GROUP BY level2.name";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchAgingSheetData_Balance( $party_id, $company_id, $startDate, $endDate, $type,$crit )
	{
		$query = "";
		$company_id2="";
		if($company_id!=''){
			$company_id2=str_replace("l.","pledger.",$company_id);
		}

		
		
		$query =" SELECT p.name AS 'ACCOUNT',p.pid AS 'PID',IFNULL(SUM(CREDIT),0) AS CREDIT, IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT), 0) AS 'CURRENT_BALANCE', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 15 DAY) AND pledger.date <= '{$endDate}' $company_id2 ) AS '15_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 30 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 15 DAY) $company_id2) AS '30_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 45 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 30 DAY) $company_id2) AS '45_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 60 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 45 DAY) $company_id2) AS '60_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 75 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 60 DAY) $company_id2) AS '75_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 90 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 75 DAY) $company_id2) AS '90_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 105 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 90 DAY) $company_id2) AS '105_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 120 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 105 DAY) $company_id2) AS '120_DAYS', (
		SELECT IFNULL(SUM(DEBIT), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 120 DAY) $company_id2) AS 'LESSTHAN_120_DAYS'
		FROM pledger AS l
		INNER JOIN party AS p ON l.pid=p.pid
		WHERE p.level3 IN (
		SELECT l3
		FROM level3
		WHERE name LIKE '%$type%') AND l.date <= '{$endDate}' {$company_id} $crit

		GROUP BY l.pid;";
		


		$result = $this->db->query( $query );

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
		
		// return $result->result_array();
	}

	public function fetchAgingSheetData( $party_id, $company_id, $startDate, $endDate, $type,$crit )
	{
		$query = "";
		$company_id2="";
		if($company_id!=''){
			$company_id2=str_replace("l.","pledger.",$company_id);
		}

		
		
		$query ="SELECT p.name AS 'ACCOUNT',p.pid AS 'PID', IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0) AS 'CURRENT_BALANCE', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 15 DAY) AND pledger.date <= '{$endDate}' ) AS '15_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 30 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 15 DAY) ) AS '30_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 45 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 30 DAY) ) AS '45_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 60 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 45 DAY) ) AS '60_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 75 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 60 DAY) ) AS '75_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 90 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 75 DAY) ) AS '90_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 105 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 90 DAY) ) AS '105_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date > DATE_SUB('{$endDate}', INTERVAL 120 DAY) AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 105 DAY) ) AS '120_DAYS', (
		SELECT IFNULL((SUM(DEBIT)-SUM(CREDIT)), 0)
		FROM pledger
		WHERE pledger.pid = p.pid AND pledger.date <= DATE_SUB('{$endDate}', INTERVAL 120 DAY) ) AS 'LESSTHAN_120_DAYS'
		FROM pledger AS l
		INNER JOIN party AS p ON l.pid=p.pid
		WHERE p.level3 IN (
		SELECT l3
		FROM level3
		WHERE name LIKE '%$type%') AND l.date <= '{$endDate}'  $crit
		GROUP BY l.pid";
		


		$result = $this->db->query( $query );

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
		
		// return $result->result_array();
	}

	public function fetchOrderSummary($company_id, $startDate, $endDate, $po )
	{
		$query = "";
		$query = $this->db->query("call spw_OrderSummary('$startDate', '$endDate', $company_id ,$po)");
		return $query->result_array();
		
	}



	public function fetchByCol($col) {

		$result = $this->db->query("SELECT DISTINCT $col FROM party");
		return $result->result_array();
	}

	public function fetchNetChequeSum( $etype, $company_id )
	{			
		$query= $this->db->query("SELECT IFNULL(SUM(amount), 0) as NETTOTAL FROM pd_cheque WHERE etype='$etype' AND post='unpost' AND company_id={$company_id}");
		$result = $query->result_array();

		return $result;
	}
	public function fetchNetSum_Etype( $from,$to,$company_id, $etype )
	{
		if ($etype=='purchase'){
			$query = "SELECT IFNULL(SUM(debit),0)- IFNULL(SUM(credit),0) AS 'SALES_TOTAL'
			FROM pledger
			WHERE pid IN (
			SELECT purchase
			FROM setting_configuration) $company_id AND DATE BETWEEN '". $from ."' AND '". $to ."'";
		}else if($etype=='salereturn'){
			$query = "SELECT IFNULL(SUM(debit),0)-IFNULL(SUM(credit),0) AS 'SALES_TOTAL'
			FROM pledger
			where pid = (select $etype from setting_configuration) $company_id and date BETWEEN '". $from ."' AND '". $to ."' ";

		}else if($etype=='sale'){
			$query = "SELECT IFNULL(SUM(credit),0)- IFNULL(SUM(debit),0) AS 'SALES_TOTAL'
			FROM pledger
			WHERE pid IN (
			SELECT sale
			FROM setting_configuration) $company_id AND DATE BETWEEN '". $from ."' AND '". $to ."'";
		}else if($etype=='purchasereturn'){
			$query = "SELECT IFNULL(SUM(credit),0)-IFNULL(SUM(debit),0) AS 'SALES_TOTAL'
			FROM pledger
			where pid = (select $etype from setting_configuration) $company_id and date BETWEEN '". $from ."' AND '". $to ."' ";
		}	



		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function sendMessage( $mobile, $message )

	{

		$ptn = "/^[0-9]/";  // Regex

		$rpltxt = "92";  // Replacement string

		$mobile = preg_replace($ptn, $rpltxt, $mobile);



		// Create the SoapClient instance 

		$url = ZONG_API_SERVICE_URL; 

		// $soapClient = new SoapClient( $url);

		// var_dump($soapClient->__getFunctions());



		$post_string = '<?xml version="1.0" encoding="utf-8"?>'.

		'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .

		'<soap:Body>'.

		'<SendSingleSMS xmlns="http://tempuri.org/">'.

		'<Src_nbr>' . ZONG_API_MOB . '</Src_nbr>'.

		'<Password>' . ZONG_API_PASS . '</Password>'.

		'<Dst_nbr>' . $mobile . '</Dst_nbr>'.

		'<Mask>' . ZONG_API_MASK . '</Mask>'.

		'<Message>' . $message . '</Message>'.

		'</SendSingleSMS>'.

		'</soap:Body>'.

		'</soap:Envelope>';



		$soap_do = curl_init(); 

		curl_setopt($soap_do, CURLOPT_URL,            $url );   

		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 

		curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 

		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 

		curl_setopt($soap_do, CURLOPT_POST,           true ); 

		curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 

		curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($post_string) ));



		$result = curl_exec($soap_do);

		$err = curl_error($soap_do);



		return $result;

	}
	

	public function getsetting_configur()
	{
		$query = "SELECT * FROM setting_configuration";
		$result = $this->db->query($query);
		return $result->result_array();
	}
	public function getMaxChequeId( $etype,$company_id )
	{
		$this->db->select_max('dcno');
		$query = $this->db->get_where('pd_cheque', array('etype' => $etype,'company_id'	=>	$company_id));

		$result = $query->row_array();
		return $result['dcno'];
	}

	public function getDistinctFields($field) {

		$result = $this->db->query("SELECT DISTINCT $field FROM `party`");
		return $result->result_array();
	}

	public function save( $accountDetail ) {
		$this->db->where(array(
			'pid' => $accountDetail['pid']
			));
		$result = $this->db->get('party');

		$affect = 0;
		$pid = "";
		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'pid' => $accountDetail['pid']
				));
			$result = $this->db->update('party', $accountDetail);
			$affect = $this->db->affected_rows();

			$pid = $accountDetail['pid'];
		} else {	// if less than or equal to 0 then insert it

			unset($accountDetail['pid']);
			$result = $this->db->insert('party', $accountDetail);
			$affect = $this->db->affected_rows();

			$pid = $this->db->insert_id();
		}

		if ($affect === 0 && $pid == "") {
			return false;
		} else {
			return $pid;
		}
	}

	public function fetchAccount($pid) {

		$this->db->where(array(
			'pid' => $pid
			));
		$result = $this->db->get('party');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAccountByName($name) {

		$this->db->where(array(
			'name' => $name
			));
		$result = $this->db->get('party');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}
	// Select Only Those Accounts Where level is 3
	public function fetchAll($activee=-1, $typee="all") {
		$crit="";
		
		if ($typee!="all"){
			// $crit=" and level3.name in ('debitors','creditors')";	
			$crit =" and level3.name like '%$typee%' ";
		}



		if ($activee==-1){
			$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` where party.name <>'' $crit ");
		}else{
			$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` WHERE `party`.`active`=$activee  $crit");
		}	
		return $result->result_array();
	}

	// public function fetchAll() {
	// 	$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`");
	// 	return $result->result_array();
	// }

	public function fetchTypeParty($etype) {
		
		$result = $this->db->query('SELECT pid,name from party where etype = "'.$etype.'" ');
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	public function fetchAll_Users() {

		$result = $this->db->query("select * from user");
		return $result->result_array();
	}

	public function fetchAll_CashAccount() { 

		$result = $this->db->query("SELECT ifnull(pl.balance,0) as balance, `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name
			FROM `party`
			INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
			LEFT JOIN (
			select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid 
			from pledger  
			group by pid) as pl  ON pl.pid= `party`.`pid`
			WHERE `party`.name LIKE '%cash%'");
		return $result->result_array();
	}
	public function fetchAll_ExpAccount() { 

		$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` WHERE `level3`.name like '%expense%' ");
		return $result->result_array();
	}
	public function fetchAll_EmployeeAccount() { 

		$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` WHERE `level3`.name like '%employee%' ");
		return $result->result_array();
	}

	public function getAllParties($etype) {

		if ($etype === '') {

			$result = $this->db->query("SELECT `name`, `pid` FROM `party`");
			return $result->result_array();
		} else {

			$this->db->where(array(
				'etype' => $etype
				));
			$result = $this->db->get('party');
			return $result->result_array();
		}
	}

	public function fetchBanks() {
		$query = 'SELECT DISTINCT bank_name FROM pd_cheque ORDER BY bank_name';
		$result = $this->db->query( $query );

		return $result->result_array();
	}

	public function saveCheque( $data )
	{
		$this->db->where(array(
			'dcno' => $data['dcno'],
			'etype' => $data['etype']
			));
		$q = $this->db->get('pd_cheque');

		if ( $q->num_rows() > 0 ) {
			$this->db->where(array(
				'dcno' => $data['dcno'],
				'etype' => $data['etype']
				));
			$this->db->update('pd_cheque',$data);
			$rowCount = $this->db->affected_rows();

			if ( $rowCount !== 0 ) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			$this->db->insert('pd_cheque',$data);
			$rowCount = $this->db->affected_rows();
			if ( $rowCount !== 0 ) {
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function fetchCheques($etype, $date, $company_id)
	{
		$query = "SELECT pd_cheque.DCNO, pd_cheque.VRDATE, pd_cheque.CHEQUE_NO, pd_cheque.MATURE_DATE, party.NAME 'PARTY', pd_cheque.BANK_NAME 'BANK', pd_cheque.AMOUNT FROM pd_cheque INNER JOIN party on pd_cheque.party_id_cr = party.pid WHERE pd_cheque.etype='{$etype}' AND pd_cheque.vrdate <= '{$date}' AND pd_cheque.post='unpost' AND pd_cheque.company_id={$company_id} ORDER BY pd_cheque.DCNO, pd_cheque.VRDATE";
		$result = $this->db->query($query);

		return $result->result_array();
	}


	public function fetchChequeVoucher( $dcno, $etype,$company_id )
	{
		$query = $this->db->query("SELECT pd_cheque.*, DATE(pd_cheque.cheque_date) AS cheque_date, DATE(pd_cheque.vrdate) AS vrdate, DATE(pd_cheque.mature_date) AS mature_date, p1.name AS partyName, p2.name AS partyName2, u.uname AS user_name,c.company_name
			FROM pd_cheque
			INNER JOIN party AS p1 ON pd_cheque.party_id = p1.pid
			INNER JOIN party AS p2 ON pd_cheque.party_id_cr = p2.pid
			INNER JOIN user AS u ON pd_cheque.uid=u.uid
			INNER JOIN company c ON c.company_id=pd_cheque.company_id 
			where pd_cheque.etype='{$etype}' AND pd_cheque.company_id ={$company_id} AND pd_cheque.dcno={$dcno}");	
		return $query->result_array();
		// $query = $this->db->get_where('pd_cheque', array(
		// 	'etype' => $etype,
		// 	'dcno' => $dcno
		// ));

		// if ($query->num_rows() > 0) {
		// 	return $query->row_array();
		// }else {
		// 	return false;
		// }

	}

	public function removeChequeVoucher( $dcno, $etype ,$company_id )
	{
		$this->db->where(array(
			'etype' => $etype,
			'dcno' => $dcno,
			'company_id' => $company_id
			));

		$this->db->delete('pd_cheque');

		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getAllCheques($startDate, $endDate, $etype)
	{
		$query = "SELECT pd_cheque.DCNO, pcr.name 'ACCOUNT', p.name 'PARTY_NAME', BANK_NAME, CHEQUE_NO, CHEQUE_DATE, AMOUNT, STATUS, POST, VRDATE FROM pd_cheque INNER JOIN party AS pcr ON pd_cheque.party_id_cr = pcr.pid LEFT JOIN party AS p ON pd_cheque.party_id = p.pid WHERE pd_cheque.etype='{$etype}' AND VRDATE BETWEEN '{$startDate}' AND '{$endDate}'";
		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function fetchPartyOpeningBalance( $to, $party_id )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'OPENING_BALANCE' FROM pledger WHERE pledger.pid={$party_id} AND date < '{$to}'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchRunningTotal($endDate, $party_id)
	{
		$query = "SELECT SUM(DEBIT) - SUM(CREDIT) 'RTotal' FROM pledger WHERE DATE(DATE) <= '$endDate' AND pid =$party_id";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	function fetchTrialBalanceData($startDate, $endDate,$company_id, $l1 , $l2 ,$l3) 
	{

		$query = $this->db->query("call Trial_Balance('$startDate', '$endDate', $company_id, $l1, $l2, $l3)");
		return $query->result_array();
	}
	function fetchTrialBalanceData6($startDate, $endDate,$company_id ,$l1,$l2,$l3) 
	{

		$query = $this->db->query("call spw_trial_six('$startDate', '$endDate', $company_id,$l1,$l2,$l3)");
		return $query->result_array();
	}

	function Account_Flow($dt1, $dt2, $party_id ,$company_id) 
	{

		$query = $this->db->query("call spw_account_flow('$dt1', '$dt2', $party_id, $company_id)");
		return $query->result_array();
	}


	public function getChartOfAccounts()
	{
		$query = $this->db->query("SELECT party.ACCOUNT_ID, party.name AS 'PARTY_NAME', party.level3, level1.l1, level1.name AS 'L1NAME', level2.l2, level2.name AS 'L2NAME', level3.l3, level3.name AS 'L3NAME' FROM party party INNER JOIN level3 level3 ON party.level3 = level3.l3 INNER JOIN level2 level2 ON level3.l2 = level2.l2 INNER JOIN level1 level1 ON level1.l1 = level2.l1 ORDER BY account_id");
		return $query->result_array();
	}

	public function fetchDayBookReportData ($startDate, $endDate, $what, $etype,$company_id,$field,$crit,$orderBy,$groupBy,$name)
	{
		
		$ord='';
		if ($what == 'date') {
			$ord="DATE_FORMAT(pledger.date , '%d %b %y')";
		}else if ($what == 'invoice') {
			$ord='pledger.etype';
		}else if ($what == 'wo') {
			$ord='pledger.wo';
		}else if ($what == 'party') {
			$ord='party.name';		
		}else if ($what == 'user') {
			$ord='user.uname';
		}else if ($what == 'month' ) {
			$ord='month(pledger.date)';
		}else if ( $what == 'weekday' ) {
			$ord='dayname(pledger.date)';
		}else if ( $what == 'year') {				
			$ord='year(pledger.date)';
		}
		
		$query = "SELECT $ord as group_sort,dayname(pledger.date) as weekdate, month(pledger.date) as monthdate,year(pledger.date) as yeardate,pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,pledger.DEBIT as DEBIT ,pledger.credit  as 'CREDIT' ,pledger.DESCRIPTION AS REMARKS, DATE_FORMAT(pledger.date , '%d %b %y') as 'DATE', user.uname, company.company_name  FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 inner join user as user on user.uid=pledger.uid inner join company on company.company_id=pledger.company_id $crit  WHERE  pledger.date BETWEEN '$startDate' AND '$endDate'  and pledger.company_id=$company_id  ORDER BY  $ord  ,pledger.date";
		$result = $this->db->query($query);
		return $result->result_array();

	}

	public function getChequeReportData($startDate, $endDate, $etype,$company_id)
	{
		$query = "SELECT pd_cheque.DCNO, pcr.name 'ACCOUNT', party.name 'PARTY_NAME', BANK_NAME, CHEQUE_NO, CHEQUE_DATE, AMOUNT, STATUS,POST,VRDATE FROM (SELECT * FROM party party) pcr INNER JOIN pd_cheque pd_cheque ON pcr.pid =pd_cheque.party_id_cr INNER JOIN party party ON party.pid=pd_cheque.party_id AND pd_cheque.etype='{$etype}' AND vrdate BETWEEN '{$startDate}' AND '{$endDate}' AND pd_cheque.company_id=$company_id ORDER BY vrdate";
		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function fetchPayRecvReportData($startDate, $endDate, $etype,$company_id,$what,$crit)
	{
		$groupBy='';

		if($what=='type'){
			$groupBy = ' party.ETYPE ';
		}else if($what=='area'){
			$groupBy = ' party.CITYAREA ';
		}else{
			$groupBy = ' party.CITY ';
		}
		$HavingClause='';

		if ($etype === 'payable') {
			$HavingClause=' HAVING IFNULL(SUM(pledger.DEBIT),0)- IFNULL(SUM(pledger.CREDIT),0)<0 ';
		}else{
			$HavingClause=' HAVING IFNULL(SUM(pledger.DEBIT),0)- IFNULL(SUM(pledger.CREDIT),0)>0 ';
		}

		$supp = "";
		$customer = "";

		$result1 = $this->db->query("SELECT s.supplier,s.customers from setting_configuration s");
		$row = $result1->row_array();
		$supp = $row['supplier'];
		$customer = $row['customers'];

		$supp = ($supp==""?"0":$supp);

		$customer = ($customer==""?"0":$customer);			


		$query = "SELECT  IFNULL(SUM(pledger.DEBIT),0)- IFNULL(SUM(pledger.CREDIT),0) 'BALANCE', ifnull($groupBy,'') AS VOUCHER, party.NAME 'ACCOUNT_NAME', party.UNAME 'URDU_NAME', party.MOBILE, party.PHONE AS PHONE_OFF, party.ADDRESS, party.EMAIL
		FROM pledger
		INNER JOIN party ON pledger.pid = party.pid
		INNER JOIN level3 ON level3.l3 = party.level3
		INNER JOIN level2 ON level2.l2 = level3.l2
		INNER JOIN level1 ON level1.l1 = level2.l1
		WHERE pledger.DATE BETWEEN '$startDate' AND '$endDate' AND pledger.company_id=$company_id and party.level3 IN ($supp,$customer) $crit
		GROUP BY $groupBy, party.NAME
		$HavingClause
		ORDER BY $groupBy";

		$query = $this->db->query($query);
		return $query->result_array();

	}

	public function fetchClosingBalance( $to )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT), 0) as 'CLOSING_BALANCE' FROM pledger pledger WHERE pledger.pid=(SELECT pid FROM party party WHERE name='cash') AND date <= '{$to}'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchOpeningBalance( $to )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT),0) as 'OPENING_BALANCE' FROM pledger pledger WHERE pledger.pid=(SELECT pid FROM party party WHERE name='cash') AND date < '{$to}'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchOpeningBalance_Accounts( $from,$party_id,$company_id )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT),0) as 'OPENING_BALANCE' FROM pledger pledger WHERE pledger.pid=$party_id AND date < '{$from}' ";
		$result = $this->db->query($query);
		return $result->result_array();
	}


	

	public function fetchExpenseReportData ($startDate, $endDate, $what, $etype,$company_id,$crit)
	{
		$ord='';
		if ($what == 'date') {
			$ord='pledger.date';
		}
		else if ($what == 'invoice') {
			$ord='pledger.dcno';
		}
		else if ($what == 'party') {
			$ord='party.name';		
		}else {
			$ord='user.uname';
		}

		$query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,pledger.DEBIT AS DEBIT,pledger.credit AS 'CREDIT',pledger.DESCRIPTION AS REMARKS, pledger.DATE, user.uname, company.company_name
		FROM pledger pledger
		INNER JOIN party party ON pledger.pid = party.pid
		INNER JOIN user ON user.uid=pledger.uid
		INNER JOIN company ON company.company_id=pledger.company_id
		WHERE pledger.pid IN (
		SELECT pid AS PARTY_ID
		FROM party party
		INNER JOIN level3 level3 ON party.level3=level3.l3
		INNER JOIN level2 level2 ON level2.l2 = level3.l2
		INNER JOIN level1 level1 ON level2.l1 = level1.l1
		WHERE level1.NAME IN ('expense', 'expenses')) AND party.name NOT IN ('sale return', 'purchase', 'purchase import') AND pledger.date BETWEEN '$startDate' AND '$endDate' AND pledger.company_id=$company_id $crit
		ORDER BY $ord,pledger.date";

		$result = $this->db->query($query);
		return $result->result_array();

	}
	public function fetchInvoiceAgingData( $from, $to, $reptType, $party_id, $company_id)
	{
			// if ($party_id!==""){
			// 	$party_id=" and a.pid=" + $party_id + "" ;
			// }
		$company_id= (string)$company_id;
		$acompany_id="";
		$acompany_id= (string)$company_id;

		if($company_id!==""){
			$company_id = " and b.cid= " .$company_id ;
			$acompany_id = " and a.cid= " . $acompany_id;
		}
		$party_id= (string)$party_id;
		$aparty_id="";
		$aparty_id= (string)$party_id;

		if($party_id!==""){
			$party_id = " and a.pid= " .$party_id ;
			$aparty_id = " and a.pid= " . $aparty_id;
		}
		
		$query = "";
		if ( $reptType === 'payables' ) {
				// if ( $party_id ) {
				// 	// $query = "SELECT a.dcno, party.name as account, a.vrdate, IFNULL(a.due_date, '-') as due_date, IFNULL(DATEDIFF(a.due_date, CURDATE()), '-') as days_passed, sum(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.party_id =a.party_id and a.vrdate BETWEEN '$from' AND '$to' AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.party_id = party.party_id WHERE a.etype='PURCHASE' AND a.vrdate BETWEEN '$from' AND '$to' AND a.company_id = $company_id GROUP BY a.dcno,a.vrdate ORDER BY a.dcno ";
				// 	$query = "SELECT *, invoice_amount-paid as balance FROM (SELECT a.dcno, DATE_FORMAT(a.date,'%d %b %y'), party.name as account, DATE_FORMAT(IFNULL(a.date, '-'),'%d %b %y') as due_date, IFNULL(DATEDIFF(CURDATE(),a.date), '-') as days_passed, sum(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date BETWEEN '$from' AND '$to' AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='PURCHASE' AND a.date BETWEEN '$from' AND '$to' AND a.company_id = $company_id AND a.pid = $party_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
				// } else {
			$query = "SELECT *, invoice_amount-paid AS balance, aging.address, aging.email, aging.mobile, CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') <= 30 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '0_30', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') BETWEEN 31 AND 60 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '31_60', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') BETWEEN 61 AND 90 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '61_90', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') BETWEEN 91 AND 120 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '91_120', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') >120 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS 'abov_120' FROM (SELECT  concat( a.dcno ,'-',a.etype)as dcno,a.date as vrdate, DATE_FORMAT(a.date,'%d %b %y') date, party.address, party.email, party.mobile, party.name AS account, DATE_FORMAT(IFNULL(a.date, '-'),'%d %b %y') AS due_date, IFNULL(DATEDIFF(CURDATE(),a.date), '-') AS days_passed, round(SUM(a.credit),0) invoice_amount,(SELECT IFNULL(SUM(b.debit),0) FROM pledger b WHERE b.invoice = a.dcno AND b.pid =a.pid AND a.date BETWEEN '$from' AND '$to'   $company_id ) paid FROM pledger a INNER JOIN party ON a.pid = party.pid WHERE a.etype in ('purchase','fabricPurchase','yarnPurchase') and a.credit<>0 AND a.date BETWEEN '$from' AND '$to'  $acompany_id $party_id GROUP BY a.etype,a.dcno,a.date ORDER BY a.etype,a.dcno) AS aging";
				// }
		} else if ( $reptType === 'receiveables' ) {
				// if ($party_id) {
				// 	$query = "SELECT *, invoice_amount-paid as balance FROM (SELECT a.dcno, DATE_FORMAT(a.date,'%d %b %y'), party.name as account, DATE_FORMAT(IFNULL(a.date, '-'),'%d %b %y') as due_date, IFNULL(DATEDIFF(CURDATE(),a.date), '-') as days_passed, sum(a.debit) invoice_amount, (SELECT IFNULL(SUM(b.credit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date BETWEEN '$from' AND '$to' AND b.etype='CRV'  $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='SALE' AND a.date BETWEEN '$from' AND '$to' AND a.company_id = $company_id AND a.pid = $party_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
				// } else {
			$query = "SELECT *, invoice_amount-paid AS balance, aging.address, aging.email, aging.mobile, CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') <= 30 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '0_30', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') BETWEEN 31 AND 60 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '31_60', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') BETWEEN 61 AND 90 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '61_90', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') BETWEEN 91 AND 120 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS '91_120', CASE WHEN IFNULL(DATEDIFF(CURDATE(),vrdate), '-') >120 THEN ROUND(ifnull(invoice_amount,0)-ifnull(paid,0),0) ELSE  0 END AS 'abov_120' FROM (SELECT a.dcno,a.date as vrdate, DATE_FORMAT(a.date,'%d %b %y') date, party.address, party.email, party.mobile, party.name AS account, DATE_FORMAT(IFNULL(a.date, '-'),'%d %b %y') AS due_date, IFNULL(DATEDIFF(CURDATE(),a.date), '-') AS days_passed, round(SUM(a.debit),0) invoice_amount, (SELECT IFNULL(SUM(b.credit),0) FROM pledger b WHERE b.invoice = a.dcno AND b.pid =a.pid AND a.date BETWEEN '$from' AND '$to'   $company_id) paid FROM pledger a INNER JOIN party ON a.pid = party.pid WHERE a.etype='SALE' and a.debit<>0 AND a.date BETWEEN '$from' AND '$to'  $acompany_id $party_id GROUP BY a.dcno,a.date ORDER BY a.dcno) AS aging";
				// }		
		}

		$result = $this->db->query( $query );
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
		
			// return $result->result_array();
	}
	public function fetchPayRecvCount( $startDate, $endDate, $company_id, $etype)
	{
		if ($etype == 'payable') {
			$query = "SELECT COUNT(*) as NET_COUNT FROM (SELECT a.dcno, a.date, party.name as account, IFNULL(a.date, '-') as due_date, IFNULL(DATEDIFF(a.date, CURDATE()), '-') as days_passed, sum(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date = CURDATE() AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='PURCHASE' AND a.date = CURDATE() AND a.company_id = $company_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
			$query = $this->db->query($query);

			$result = $query->row_array();
			return $result['NET_COUNT'];

		} else if ( $etype === 'receiveable') {
			$query = "SELECT COUNT(*) as NET_COUNT FROM (SELECT a.dcno, a.date, party.name as account, IFNULL(a.date, '-') as due_date, IFNULL(DATEDIFF(a.date, CURDATE()), '-') as days_passed, sum(a.debit) invoice_amount, (SELECT IFNULL(SUM(b.credit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date = CURDATE() AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='SALE' AND a.date = CURDATE() AND a.company_id = $company_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
			$query = $this->db->query($query);

			$result = $query->row_array();
			return $result['NET_COUNT'];
		}
	}
}

/* End of file accounts.php */
/* Location: ./application/models/accounts.php */