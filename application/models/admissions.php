<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admissions extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxId() {

		$this->db->select_max('aid');
		$result = $this->db->get('admission_main');

		$row = $result->row_array();
		$maxId = $row['aid'];

		return $maxId;
	}


	public function save( $admissionTestDetail ) {

		// check if the class is already saved or not
		$this->db->where(array(
								'stdid' => $admissionTestDetail['stdid']
							));
		$result = $this->db->get('admission_main');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0) {

			// get the aid
			$this->db->where(array(
								'stdid' => $admissionTestDetail['stdid']
							));
			$result = $this->db->get('admission_main');
			$row = $result->row_array();

			$admissionTestDetail['aid'] = $row['aid'];

			// update it
			$this->db->where(array(
									'stdid' => $admissionTestDetail['stdid']
								));
			$result = $this->db->update('admission_main', $admissionTestDetail);
			$affect = $this->db->affected_rows();
		} else {	// if less than or equal to 0 then insert it

			$result = $this->db->insert('admission_main', $admissionTestDetail);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {

			$this->db->where(array(
								'stdid' => $admissionTestDetail['stdid']
							));
			$result = $this->db->get('admission_main');
			$row = $result->row_array();			

			return $row['aid'];
		}
	}


	public function saveDetail( $admissionTestDetail, $aid ) {
		
		$this->db->where(array(
								'aid' => $aid
							));
		$result = $this->db->delete('admission_detail');

		
		foreach ($admissionTestDetail as $detail) {
			
			$result = $this->db->insert('admission_detail', $detail);
			$affect = $this->db->affected_rows();
		}

	}

	public function fetch($stdid, $brid) {

		$result = $this->db->query("SELECT tclaid, aclaid, tm, om, `admission_main`.description, STATUS, secid, `admission_detail`.sbname, marks FROM `admission_main` INNER JOIN `admission_detail` ON `admission_main`.aid = `admission_detail`.aid WHERE stdid = $stdid AND brid = $brid");
		return $result->result_array();
	}
}

/* End of file admissions.php */
/* Location: ./application/models/admissions.php */