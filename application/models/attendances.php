<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendances extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId($brid) {

		$this->db->select_max('dcno');
		$this->db->where(array('brid'=>$brid));
		$result = $this->db->get('atndetail');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function save( $atndcs, $dcno, $brid ) {

		$this->db->where(array(
								'dcno' => $dcno,
								'brid' => $brid
							));
		$this->db->delete('atndetail');

		$affect = 0;
		foreach ($atndcs as $result) {

			$result['dcno'] = $dcno;
			$this->db->insert('atndetail', $result);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetch( $dcno, $brid ) {

		$result = $this->db->query("SELECT dcno, ad.brid, ad.claid, ad.secid, ad.stdid, status, ad.date, description, stu.name, stu.fname FROM atndetail AS ad INNER JOIN student AS stu ON ad.stdid = stu.stdid WHERE ad.dcno = $dcno AND ad.brid = $brid AND stu.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function studentAttendanceStatusWiseReport($from, $to, $brid, $claid, $secid, $stdid, $status) {

		$query = "";
		if ($claid == '' && $secid == '' && $stdid == '') {
			$query = "SELECT ad.status, DATE_FORMAT(DATE(ad.date), '%d-%m-%Y') AS date, stu.stdid, stu.name AS 'student_name', sec.name AS 'section_name', cls.name AS 'class_name', br.name AS 'branch_name'FROM (atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid) INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid";
		} else if ($secid == '') {
			$query = "SELECT ad.status, DATE_FORMAT(DATE(ad.date), '%d-%m-%Y') AS date, stu.stdid, stu.name AS 'student_name', sec.name AS 'section_name', cls.name AS 'class_name', br.name AS 'branch_name'FROM (atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid) INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND ad.claid = $claid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid";
		} else if ($stdid == '') {
			$query = "SELECT ad.status, DATE_FORMAT(DATE(ad.date), '%d-%m-%Y') AS date, stu.stdid, stu.name AS 'student_name', sec.name AS 'section_name', cls.name AS 'class_name', br.name AS 'branch_name'FROM (atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid) INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND ad.claid = $claid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND ad.secid = $secid AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid";
		} else {
			$query = "SELECT ad.status, DATE_FORMAT(DATE(ad.date), '%d-%m-%Y') AS date, stu.stdid, stu.name AS 'student_name', sec.name AS 'section_name', cls.name AS 'class_name', br.name AS 'branch_name'FROM (atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid) INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND ad.claid = $claid AND ad.secid = $secid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND ad.stdid = $stdid AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid";
		}

		if ($status != '-1') {
			$query .= " AND ad.status = '". $status ."'";
			$query .= " ORDER BY br.name, cls.name, sec.name, stu.name, ad.date";
		}

		$result = $this->db->query($query);

		if($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function studentAttendanceMonthWiseReport($from, $to, $brid, $claid, $secid, $stdid) {

		$query = "";
		if ($claid == '' && $secid == '') {
			$query = "SELECT ad.stdid, YEAR(ad.date) as year, MONTHNAME(ad.date) AS 'month', stu.name AS 'student_name', stu.fname AS 'father_name', br.name AS 'branch_name', cls.name AS 'class_name', sec.name AS 'section_name', COUNT(CASE WHEN ad.status='absent' THEN ad.stdid END) AS 'absent', COUNT(CASE WHEN ad.status='present' THEN ad.stdid END) AS 'present', COUNT(CASE WHEN ad.status='leave' THEN ad.stdid END) AS 'leave'FROM atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid GROUP BY MONTHNAME(ad.date), YEAR(ad.date), ad.stdid ORDER BY YEAR(ad.date) DESC, MONTHNAME(ad.date), br.name, cls.name, sec.name, stu.name";
		} else if ($secid == '') {
			$query = "SELECT ad.stdid, YEAR(ad.date) as year, MONTHNAME(ad.date) AS 'month', stu.name AS 'student_name', stu.fname AS 'father_name', br.name AS 'branch_name', cls.name AS 'class_name', sec.name AS 'section_name', COUNT(CASE WHEN ad.status='absent' THEN ad.stdid END) AS 'absent', COUNT(CASE WHEN ad.status='present' THEN ad.stdid END) AS 'present', COUNT(CASE WHEN ad.status='leave' THEN ad.stdid END) AS 'leave'FROM atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND ad.claid = $claid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid GROUP BY MONTHNAME(ad.date), YEAR(ad.date), ad.stdid ORDER BY YEAR(ad.date) DESC, MONTHNAME(ad.date), br.name, cls.name, sec.name, stu.name";
		} else if ($stdid == '') {
			$query = "SELECT ad.stdid, YEAR(ad.date) as year, MONTHNAME(ad.date) AS 'month', stu.name AS 'student_name', stu.fname AS 'father_name', br.name AS 'branch_name', cls.name AS 'class_name', sec.name AS 'section_name', COUNT(CASE WHEN ad.status='absent' THEN ad.stdid END) AS 'absent', COUNT(CASE WHEN ad.status='present' THEN ad.stdid END) AS 'present', COUNT(CASE WHEN ad.status='leave' THEN ad.stdid END) AS 'leave'FROM atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND ad.claid = $claid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND ad.secid = $secid AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid GROUP BY MONTHNAME(ad.date), YEAR(ad.date), ad.stdid ORDER BY YEAR(ad.date) DESC, MONTHNAME(ad.date), br.name, cls.name, sec.name, stu.name";
		}  else {
			$query = "SELECT ad.stdid, YEAR(ad.date) as year, MONTHNAME(ad.date) AS 'month', stu.name AS 'student_name', stu.fname AS 'father_name', br.name AS 'branch_name', cls.name AS 'class_name', sec.name AS 'section_name', COUNT(CASE WHEN ad.status='absent' THEN ad.stdid END) AS 'absent', COUNT(CASE WHEN ad.status='present' THEN ad.stdid END) AS 'present', COUNT(CASE WHEN ad.status='leave' THEN ad.stdid END) AS 'leave'FROM atndetail AS ad INNER JOIN section AS sec ON ad.secid=sec.secid INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE ad.brid = $brid AND ad.claid = $claid AND ad.secid = $secid AND date(ad.date) >= '". $from ."' AND date(ad.date) <= '". $to ."' AND ad.stdid = $stdid AND sec.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND br.brid = $brid GROUP BY MONTHNAME(ad.date), YEAR(ad.date), ad.stdid ORDER BY YEAR(ad.date) DESC, MONTHNAME(ad.date), br.name, cls.name, sec.name, stu.name";
		}

		$result = $this->db->query($query);

		if($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function monthlyAttendanceReport($month, $year, $brid, $claid, $secid) {

		$result = $this->db->query("SELECT ad.stdid, YEAR(ad.date) AS year, MONTHNAME(ad.date) AS 'month', DAY(ad.date) AS day, stu.name AS 'student_name', stu.fname AS 'father_name', br.name AS 'branch_name', cls.name AS 'class_name', sec.name AS 'section_name', ad.status FROM atndetail AS ad INNER JOIN student AS stu ON ad.stdid=stu.stdid INNER JOIN section AS sec ON ad.secid=sec.secid INNER JOIN class AS cls ON ad.claid=cls.claid INNER JOIN branch AS br ON ad.brid=br.brid WHERE MONTHNAME(ad.date) = '". $month ."' AND YEAR(ad.date) = $year AND ad.brid = $brid AND ad.claid = $claid AND ad.secid = $secid AND stu.brid = $brid AND sec.brid = $brid AND cls.brid = $brid AND br.brid = $brid GROUP BY ad.stdid, ad.date ORDER BY stu.name");

		if($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function dailyAttendanceReport($brid) {

		$result = $this->db->query("SELECT b.name AS 'branch_name', c.name AS 'class_name', s.name 'section_name', COUNT(*) AS 'total_students', COUNT(CASE WHEN ad.status='absent' THEN ad.stdid END) AS 'absent', COUNT(CASE WHEN ad.status='present' THEN ad.stdid END) AS 'present', COUNT(CASE WHEN ad.status='leave' THEN ad.stdid END) AS 'leave' FROM atndetail ad INNER JOIN branch b ON b.brid = ad.brid INNER JOIN class c ON c.claid = ad.claid INNER JOIN section s ON s.secid = ad.secid WHERE DATE(ad.DATE) = DATE(NOW()) AND ad.brid = $brid AND b.brid = $brid AND c.brid = $brid AND s.brid = $brid GROUP BY DATE, ad.claid, ad.secid ORDER BY ad.claid, ad.secid");

		if($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function post( $vouchers, $brid ) {

		$errors = array();
		foreach ($vouchers as $voucher) {

			$dcno = $this->getMaxStaffAtndId($brid);
			$dcno++;

			foreach ($voucher as $atnd) {

				$saved = $this->isAtndAlreadySaved($atnd['date'], $atnd['staid'], $brid);

				if ($saved === false) {
					$atnd['dcno'] = $dcno;
					$this->db->insert('staffatndetail', $atnd);
				} else {
					array_push($errors, $saved);
				}
			}
		}

		return $errors;
	}

	public function isAtndAlreadySaved($date, $staid, $brid) {

		$saved = array();

		$query = "SELECT d.name AS 'branch_name' FROM staffatndetail AS stf INNER JOIN branch AS d ON stf.brid = d.brid where stf.date = '". $date ."' AND stf.did = $did AND stf.staid = $staid AND stf.etype='vr_atnd' AND d.brid = $brid";
		$result = $this->db->query($query);

		if ($result->num_rows() > 0) {
			$result = $result->row_array();

			$saved['branch_name']	= $result['branch_name'];
			$saved['date']	= $date;
			$saved['brid']	= $brid;

			return $saved;
		} else {
			return false;
		}
	}

	public function isVoucherAlreadySaved($date, $dids, $dcno) {

		$_dids = array();

		foreach ($dids as $did) {

			$query = "select did from staffatndetail where date = '". $date ."' AND did = $did AND etype ='vr_atnd'";

			if ($dcno != "") {
				$query .= " AND dcno <> $dcno";
			}
			$result = $this->db->query($query);

			if ($result->num_rows() > 0) {
				$result = $result->row_array();
				array_push($_dids, $result['did']);
			}
		}

		if(count($_dids) > 0) {
			return $_dids;
		} else {
			return false;
		}
	}

	public function fetchStaff( $dcno, $brid ) {

		$result = $this->db->query("SELECT atn.atid, atn.staid, stf.type, atn.brid, atn.date, atn.dcno, atn.description, atn.status, atn.postdate, stf.name AS 'staff_name'FROM staffatndetail atn INNER JOIN staff stf ON stf.staid = atn.staid WHERE atn.dcno = $dcno AND atn.etype = 'vr_atnd' AND atn.brid = $brid AND stf.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function deleteAttendance($dcno, $etype, $brid) {

		$this->db->where(array(
								'dcno' => $dcno, 
								'etype' => $etype,
								'brid' => $brid
						));
		$result = $this->db->get('staffatndetail');

		if ($result->num_rows() > 0) {
			$this->db->where(array(
								'dcno' => $dcno, 
								'etype' => $etype,
								'brid' => $brid
						));
			$result = $this->db->delete('staffatndetail');
		} else {
			return false;
		}
	}

	public function getMaxStaffAtndId($brid) {

		$this->db->select_max('dcno');
		$this->db->where(array(
								'etype' => 'vr_atnd',
								'brid' => $brid
							));
		$result = $this->db->get('staffatndetail');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function saveStaff( $atndcs, $dcno, $brid ) {

		$this->db->where(array(
							'dcno' => $dcno, 
							'etype' => 'vr_atnd',
							'brid' => $brid
						));
		$this->db->delete('staffatndetail');

		$affect = 0;
		foreach ($atndcs as $result) {

			$result['dcno'] = $dcno;
			$this->db->insert('staffatndetail', $result);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchStaffForTimeInOut($staid, $brid) {

		$result = $this->db->query("SELECT * FROM staff WHERE staid = $staid AND brid = $brid");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {

			$result = $this->db->query("SELECT * FROM staffatndetail WHERE etype='man_atnd' AND DATE(DATE) = DATE(NOW()) AND staid = $staid AND brid = $brid ORDER BY TIME(postdate)");
			$data = $result->row_array();

			if ($result->num_rows() == 0 ) {
				$this->db->query("INSERT INTO staffatndetail(`staid`, `brid`, `date`, `dcno`, `description`, `status`, `postdate`, `tin`, `tout`, `etype`) (SELECT stf.staid, d.brid, NOW() AS DATE, (SELECT IFNULL(MAX(dcno), 0)+1 AS dcno FROM staffatndetail WHERE etype = 'man_atnd') AS dcno, '' AS description, 'Present', NOW() AS postdate, CONCAT('0000-00-00 ', TIME(NOW())) AS tin, '' AS tout, 'man_atnd' AS etype FROM branch AS d INNER JOIN staff AS stf ON d.brid=stf.brid WHERE stf.staid = $staid AND stf.brid = $brid AND d.brid = $brid)");
			} elseif (($result->num_rows() % 2) == 0) {
				$this->db->query("INSERT INTO staffatndetail(`staid`, `brid`, `date`, `dcno`, `description`, `status`, `postdate`, `tin`, `tout`, `etype`) (SELECT stf.staid, d.brid, NOW() AS DATE, (SELECT IFNULL(MAX(dcno), 0)+1 AS dcno FROM staffatndetail WHERE etype = 'man_atnd') AS dcno, '' AS description, 'Present', NOW() AS postdate, CONCAT('0000-00-00 ', TIME(NOW())) AS tin, '' AS tout, 'man_atnd' AS etype FROM branch AS d INNER JOIN staff AS stf ON d.brid=stf.brid WHERE stf.staid = $staid AND d.brid = $brid AND stf.brid = $brid)");
			} else {
				$this->db->query("INSERT INTO staffatndetail(`staid`, `brid`, `date`, `dcno`, `description`, `status`, `postdate`, `tin`, `tout`, `etype`) (SELECT stf.staid, d.brid, NOW() AS DATE, (SELECT IFNULL(MAX(dcno), 0)+1 AS dcno FROM staffatndetail WHERE etype = 'man_atnd') AS dcno, '' AS description, 'Present', NOW() AS postdate, '' AS tin, CONCAT('0000-00-00 ', TIME(NOW())) AS tout, 'man_atnd' AS etype FROM branch AS d INNER JOIN staff AS stf ON d.brid=stf.brid WHERE stf.staid = $staid AND stf.brid = $brid AND d.brid = $brid)");
			}

			$result = $this->db->query("SELECT stf.staid, d.brid, d.name AS 'dept_name', stf.name, stf.cnic, sal.designation, stf.photo, (CASE WHEN stfa.tin = '0000-00-00 00:00:00' THEN '00:00:00' ELSE DATE_FORMAT(stfa.tin, '%h:%i %p') END) AS tin, (CASE WHEN stfa.tout = '0000-00-00 00:00:00' THEN '00:00:00' ELSE DATE_FORMAT(stfa.tout, '%h:%i %p') END) AS tout FROM branch AS d INNER JOIN staff AS stf ON d.brid=stf.brid INNER JOIN salary AS sal ON stf.staid=sal.staid INNER JOIN staffatndetail AS stfa ON stfa.staid = stf.staid WHERE DATE(stfa.DATE) = DATE(NOW()) AND stfa.staid = $staid AND stfa.etype = 'man_atnd' AND d.brid = $brid AND stf.brid = $brid AND sal.brid = $brid AND stfa.brid = $brid ORDER BY postdate DESC LIMIT 2");
			return $result->result_array();
		}
	}

	public function isAttendanceAlreadySaved($date, $claid, $secid, $brid, $dcno) {

		$result = $this->db->query("SELECT * FROM atndetail WHERE brid = $brid AND claid = $claid AND secid = $secid AND DATE(DATE) = '". $date ."' AND dcno <> $dcno");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
}

/* End of file attendances.php */
/* Location: ./application/models/attendances.php */