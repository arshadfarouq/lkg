<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branches extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getMaxId()
	{
		$this->db->select_max('brid');
		$result = $this->db->get('branch');

		$row = $result->row_array();
		$maxId = $row['brid'];

		return $maxId;
	}

	public function save( $branchDetail )
	{

		// check if branch is already been defined
		$this->db->where(array(
								'brid' => $branchDetail['brid']
							));
		$result = $this->db->get('branch');


		$affect = 0;
		// if result is greater than 0 then update the record with the given id
		if ($result->num_rows() > 0 ) {

			$this->db->where('brid', $branchDetail['brid']);
			$affect = $this->db->update('branch', $branchDetail);

		} else {	// is result is less than or equal to 0 then insert it

			unset($branchDetail['brid']);
			$this->db->insert( 'branch', $branchDetail );
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function validateField( $fieldValidate ) {

		$this->db->where($fieldValidate);
		$this->db->get('branch');
		$affect = $this->db->affected_rows();

		if ($affect === 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getAllBranches() {

		$this->db->select('brid, name');
		$result = $this->db->get('branch');

		return $result->result_array();
	}

	public function fetchBranch( $brid ) {

		$this->db->where(array(
								'brid' => $brid
							));
		$result = $this->db->get('branch');

		if ($result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAll() {

		$result = $this->db->get('branch');
		
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

}

/* End of file branches.php */
/* Location: ./application/models/branches.php */