<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charges extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxId($brid) {

		$this->db->where(array('brid' => $brid));
		$this->db->select_max('chid');
		$result = $this->db->get('charges');

		$row = $result->row_array();
		$maxId = $row['chid'];
		return $maxId;
	}

	public function getMaxStudentChargesId($brid) {

		$this->db->select_max('rid');
		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('registration');

		$row = $result->row_array();
		$maxId = $row['rid'];
		return $maxId;
	}

	public function getChargeTypes($brid) {

		$result = $this->db->query("SELECT DISTINCT type FROM charges WHERE brid = $brid");
		return $result->result_array();
	}

	public function save( $chargeDetail ) {

		// check if the class is already saved or not
		$this->db->where(array(
			'chid' => $chargeDetail['chid'],
			'brid' => $chargeDetail['brid']
			));
		$result = $this->db->get('charges');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'chid' => $chargeDetail['chid'],
				'brid' => $chargeDetail['brid']
				));
			$result = $this->db->update('charges', $chargeDetail);
			$affect = $this->db->affected_rows();
		} else {	// if less than or equal to 0 then insert it
			$result = $this->db->insert('charges', $chargeDetail);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function saveStudentChargesVoucher( $regObj, $brid ) {

		// check if the class is already saved or not
		$this->db->where(array(
			'rid' => $regObj['rid'],
			'brid' => $brid
			));
		$result = $this->db->get('registration');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'rid' => $regObj['rid'],
				'brid' => $brid
				));
			$result = $this->db->update('registration', $regObj);
			$affect = $this->db->affected_rows();
		} else {	// if less than or equal to 0 then insert it

			$result = $this->db->insert('registration', $regObj);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchCharge($chid, $brid) {

		$result = $this->db->query("SELECT reg.*, p.name as party_name
			FROM charges AS reg
			left join party as p on p.pid=reg.pid
			where reg.chid = $chid AND reg.brid = $brid ");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchChargeByName($description) {

		$this->db->where(array(
			'description' => $description
			));
		$result = $this->db->get('charges');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchStudentCharges($rid, $brid) {
		
		$result = $this->db->query("select reg.rid, reg.name, reg.fname, reg.mobile, reg.dob, reg.address, reg.claid, reg.brid, reg.chid, reg.gender, reg.date, reg.fee, br.name as 'branch_name', cls.name as 'class_name'from registration as reg inner join branch as br on reg.brid = br.brid inner join class as cls on reg.claid = cls.claid where reg.rid = $rid AND reg.brid = $brid AND br.brid = $brid AND cls.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAll($brid) {

		$result = $this->db->query("SELECT charges.chid, charges.description, charges.charges, charges.type, branch.name AS 'branch_name', charges.pid FROM `charges` INNER JOIN branch ON charges.brid = branch.brid WHERE charges.brid = $brid");
		return $result->result_array();
	}
	public function fetchAllCarges()
	{
	
		$result = $this->db->get("charges");
		return $result->result_array();
	}

	public function deleteStudentChargeVoucher($rid, $brid) {

		$this->db->where(array(
			'rid' => $rid,
			'brid' => $brid
			));
		$result = $this->db->get('registration');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'rid' => $rid,
				'brid' => $brid
				));
			$result = $this->db->delete('registration');

		} else {
			return false;
		}
	}

	public function getChargesForFeeIssue($brid, $claid, $fid) {

		$result = $this->db->query("SELECT m.faid, c.chid, c.description, c.pid AS 'charges_pid', d.amount
			FROM fee_assign_main AS m
			INNER JOIN fee_assign_detail AS d ON m.faid = d.faid
			INNER JOIN charges AS c ON d.chid = c.chid
			WHERE m.brid = $brid AND m.claid = $claid AND m.fcid = $fid AND d.brid = $brid AND c.brid = $brid");
		return $result->result_array();
	}

	public function chargesDefinitionReport($brid) {

		$result = $this->db->query("select c.chid, c.description, c.charges, p.name as 'affected_account'from charges as c inner join party as p on c.pid = p.pid WHERE c.brid = $brid AND p.brid = $brid");
		return $result->result_array();
	}
}

/* End of file charges.php */
/* Location: ./application/models/charges.php */