<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Concessions extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	

	public function getMaxId($etype, $brid) {

		$this->db->select_max('dcno');
		$this->db->where(array(
			'etype' => $etype,
			'brid' => $brid
			));
		$result = $this->db->get('concession_main');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}



	public function save($main,$details, $dcno, $brid,$etype) {

		$this->db->trans_begin();

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'brid' => $brid
			));
		$result = $this->db->get('concession_main');
		$conid = "";
		if ( $result->num_rows() > 0 ) {

			$row = $result->row_array();
			$conid = $row['conid'];

			$this->db->where(array(
				'dcno' => $dcno,
				'etype' => $etype,
				'brid' => $brid
				));

			$this->db->update('concession_main', $main);

			

			$this->db->where(array(
				'conid' => $conid
				));

			$this->db->delete('concession_detail');
		} else {
			$this->db->insert('concession_main', $main);
			$conid = $this->db->insert_id();
		}

		foreach ($details as $detail) {
			$detail['conid'] = $conid;
			$this->db->insert('concession_detail', $detail);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}

	}



	public function deleteVoucher($dcno, $etype, $brid) {

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'brid' => $brid
			));
		$result = $this->db->get('concession_main');

		if ($result->num_rows() == 0) {
			return false;
		} else {

			$result = $result->row_array();
			$conid = $result['conid'];

			$this->db->where(array(
				'dcno' => $dcno,
				'etype' => $etype,
				'brid' => $brid
				));
			
			$result = $this->db->delete('concession_main');

			$this->db->where(array('conid' => $conid));
			$this->db->delete('concession_detail');

			return true;
		}
	}

	public function fetchAll_report($from, $to, $orderby, $crit)
	{

		
			$result = $this->db->query("SELECT $orderby as voucher,stu.name,stu.focc,br.name As branch_name,sec.name as section_name ,cl.name as class_name,stu.name,stu.mobile,stu.address, stu.claid, stu.brid, stu.secid, stu.fid, stu.pid,stu.fname,stu.gender,stu.address,stu.mobile,stu.rollno,stu.feetype,d.chno,d.amount,d.concession,d.namount,d.type,m.*, date_format(m.date,'%d/%m/%y') as  vrdate,date_format(m.dateto, '%d/%m/%y') dateto,date_format(m.datefrom, '%d/%m/%y') datefrom,ch.description,ch.pid as charges_pid,ch.chid,user.uname as user_name, fc.name AS 'fc_name'
		 		FROM concession_main AS m
		 		INNER JOIN concession_detail AS d ON m.conid = d.conid
		 		INNER JOIN student AS stu ON stu.stdid = m.stdid and stu.brid=m.brid
		 		inner JOIN charges ch on ch.chid=d.chno and ch.brid=m.brid
		 		inner JOIN branch br on br.brid=m.brid and br.brid=m.brid
		 		inner JOIN section sec on sec.secid=m.secid and sec.brid=m.brid
		 		inner JOIN class cl on cl.claid=m.claid and cl.brid=m.brid
		 		INNER JOIN fee_category AS fc ON stu.fid = fc.fcid and fc.brid=stu.brid
		 		inner join user on user.uid=m.uid and user.brid=m.brid
				where 1=1 $crit
				ORDER BY $orderby,br.name,cl.name,sec.name,stu.name limit 1000;");
		
		return $result->result_array();
	}
	public function fetch($vrnoa, $etype, $brid) {

		$result = $this->db->query("SELECT stu.name AS student_name,br.name As branch_name,sec.name as section_name ,cl.name as class_name,d.chno,d.amount,d.concession,d.namount,d.type,m.*, date_format(m.date,'%d/%m/%y') as vrdate,date_format(m.dateto, '%d/%m/%y') dateto,date_format(m.datefrom, '%d/%m/%y') datefrom,ch.description,ch.pid as charges_pid,ch.chid,user.uname as user_name
			FROM concession_main AS m
			INNER JOIN concession_detail AS d ON m.conid = d.conid
			INNER JOIN student AS stu ON stu.stdid = m.stdid and stu.brid=m.brid
			inner JOIN charges ch on ch.chid=d.chno and ch.brid=m.brid
			inner JOIN branch br on br.brid=m.brid and br.brid=m.brid
			inner JOIN section sec on sec.secid=m.secid and sec.brid=m.brid
			inner JOIN class cl on cl.claid=m.claid and cl.brid=m.brid
			inner join user on user.uid=m.uid and user.brid=m.brid
			where m.etype = '". $etype ."' and m.dcno = $vrnoa and m.brid = $brid ");


		return $result->result_array();

	}
	// public function fetch($vrnoa, $etype, $brid) {

	// 	$result = $this->db->query("SELECT d.chno,d.amount,d.concession,d.namount,d.type,m.*,ch.description,ch.pid as charges_pid,ch.chid,user.uname as user_name
	// 		FROM concession_main AS m
	// 		INNER JOIN concession_detail AS d ON m.conid = d.conid
	// 		INNER JOIN student AS stu ON stu.stdid = m.stdid and stu.brid=m.brid
	// 		inner JOIN charges ch on ch.chid=d.chno and ch.brid=m.brid
	// 		inner join user on user.uid=m.uid and user.brid=m.brid
	// 		where m.etype = '". $etype ."' and m.dcno = $vrnoa and m.brid = $brid ");


	// 	return $result->result_array();

	// }


}


/* End of file fees.php */
/* Location: ./application/models/fees.php */