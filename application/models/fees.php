<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fees extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId($brid) {

		$this->db->select_max('fcid');
		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('fee_category');
		

		$row = $result->row_array();
		$maxId = $row['fcid'];

		return $maxId;
	}

	public function getMaxFeeIssuanceId($etype, $brid) {

		$this->db->select_max('dcno');
		$this->db->where(array(
			'etype' => $etype,
			'brid' => $brid
			));
		$result = $this->db->get('feemain');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function getMaxFeeAssignId($brid) {

		$this->db->select_max('faid');
		$this->db->where(array( 'brid' => $brid ));
		$result = $this->db->get('fee_assign_main');

		$row = $result->row_array();
		$maxId = $row['faid'];

		return $maxId;
	}

	public function getMaxFeeRecId($brid) {

		$this->db->select_max('frid');
		$this->db->where(array( 'brid' => $brid ));
		$result = $this->db->get('feerec');

		$row = $result->row_array();
		$maxId = $row['frid'];

		return $maxId;
	}

	public function getAllFeeCategories($brid) {

		$this->db->where(array('brid' => $brid));
		$this->db->select('fcid ,name');
		$result = $this->db->get('fee_category');;

		return $result->result_array();
	}

	public function save( $feeCatDetail ) {

		// check if the fee category is already saved or not
		$this->db->where(array(
			'fcid' => $feeCatDetail['fcid'],
			'brid' => $feeCatDetail['brid']
			));
		$result = $this->db->get('fee_category');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0 ) {

			$this->db->where(array(
				'fcid' => $feeCatDetail['fcid'],
				'brid' => $feeCatDetail['brid']
				));
			$affect = $this->db->update('fee_category',$feeCatDetail);
		} else {	// if less than or equal to 0 then insert it
			$this->db->insert('fee_category', $feeCatDetail);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}


	public function saveFeeRecieve($feerec, $dcno, $brid, $ledgers) {

		$this->db->trans_begin();

		

		// $this->db->where(array(
		// 	'frid' => $dcno,
		// 	'brid' => $brid
		// 	));

		// $result = $this->db->get('feerec');

		// if ( $result->num_rows() > 0 ) {

		$this->db->where(array(
			'frid' => $dcno,
			'brid' => $brid
			));
		$this->db->delete('feerec');
		// }

		$affect = 0;

		$this->db->insert('feerec', $feerec);
		// $affect = $this->db->affected_rows();

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => 'frv',
			'brid' => $brid
			));
		$affect = $this->db->delete('pledger');
		
		if($ledgers!=""){

			$affect = 0;
			foreach ($ledgers as $ledger) {
				$ledger["dcno"] = $dcno;
				$this->db->insert('pledger', $ledger);
				$affect = $this->db->affected_rows();
			}
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}

		
	}


	public function saveFeeIssuance($studs, $dcno, $brid, $ledgers) {

		$this->db->trans_begin();

		$etype='fee issue';
		$this->db->query("DELETE from feedetail WHERE fmid in (SELECT fmid from feemain where etype = '$etype' AND dcno = $dcno and brid=$brid ) ");

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'brid' => $brid
			));
		$this->db->delete('feemain');

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'brid' => $brid
			));
		$affect = $this->db->delete('pledger');
		

		$affect = 0;
		$lastInsertData = array();
		foreach ($studs as $stud) {
			$main = $stud['main'];

			$this->db->insert('feemain', $main);
			$affect = $this->db->affected_rows();
			$fmid = $this->db->insert_id();

			$id = $this->db->insert_id();
			$stdid = $main['stdid'];

			// $this->db->where(array(
			// 	'stdid' => $stdid,
			// 	'brid' => $brid
			// 	));
			// $this->db->select('arears');
			// $result = $this->db->get('student');
			// $result = $result->row_array();
			// $arears = $result['arears'];

			// array_push($lastInsertData, array(
			// 	'id' => $id,
			// 	'stdid' => $stdid,
			// 	'arears' => $arears
			// 	));

			$details = $stud['detail'];
			foreach ($details as $detail) {

				$detail['fmid'] = $fmid;
				$this->db->insert('feedetail', $detail);
				$affect = $this->db->affected_rows();
			}


		}

		if($ledgers!=""){

			$affect = 0;
			foreach ($ledgers as $ledger) {
				$ledger["dcno"] = $dcno;
				$this->db->insert('pledger', $ledger);
				$affect = $this->db->affected_rows();
			}
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
		
	}

	public function saveAdmissionCharges($main, $detail, $dcno, $brid) {

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => 'admission charges',
			'brid' => $brid
			));
		$result = $this->db->get('feemain');

		$fmid = "";
		if ( $result->num_rows() > 0 ) {

			$row = $result->row_array();
			$fmid = $row['fmid'];

			$this->db->where(array(
				'dcno' => $dcno,
				'etype' => 'admission charges',
				'brid' => $brid
				));
			$this->db->delete('feemain');

			$this->db->where(array(
				'fmid' => $fmid,
				'brid' => $brid
				));
			$this->db->delete('feedetail');
		}

		$affect = 0;

		$this->db->insert('feemain', $main);
		$affect = $this->db->affected_rows();
		$fmid = $this->db->insert_id();


		foreach ($detail as $d) {

			$d['fmid'] = $fmid;
			$this->db->insert('feedetail', $d);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	

	public function fetchFeeCategory( $fcid, $brid ) {

		$this->db->where(array(
			'fcid' => $fcid,
			'brid' => $brid
			));
		$result = $this->db->get('fee_category');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAll($brid) {

		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('fee_category');

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function saveAssignFeetoClassMain($main, $brid) {

		// check if the fee category is already saved or not
		$this->db->where(array(
			'faid' => $main['faid'],
			'brid' => $brid
			));
		$result = $this->db->get('fee_assign_main');

		$affect = 0;
		$faid = "";
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0 ) {

			$this->db->where(array(
				'faid' => $main['faid'],
				'brid' => $brid
				));
			$affect = $this->db->update('fee_assign_main',$main);

			$faid = $main['faid'];
		} else {	// if less than or equal to 0 then insert it
			$this->db->insert('fee_assign_main', $main);
			$affect = $this->db->affected_rows();

			$faid = $main['faid'];
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return $faid;
		}
	}

	public function saveAssignFeetoClassDetail($detail, $faid, $brid) {

		$this->db->where(array(
			'faid' => $faid,
			'brid' => $brid
			));
		$result = $this->db->get('fee_assign_detail');

		if ( $result->num_rows() > 0 ) {
			$this->db->where(array(
				'faid' => $faid,
				'brid' => $brid
				));
			$this->db->delete('fee_assign_detail');
		}

		$affect = 0;
		foreach ($detail as $dt) {

			$dt['faid'] = $faid;
			$this->db->insert('fee_assign_detail', $dt);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchFeeAssignMain($faid, $brid) {

		$this->db->where(array(
			'faid' => $faid,
			'brid' => $brid
			));
		$result = $this->db->get('fee_assign_main');

		return $result->result_array();
	}

	public function fetchFeeAssignDetail($faid, $brid) {

		$result = $this->db->query("SELECT `fadid`, `faid`, charges.`chid`, `amount`, `charges`.description FROM `fee_assign_detail` inner join charges on `fee_assign_detail`.chid = charges.chid where `faid` = $faid AND fee_assign_detail.brid = $brid AND charges.brid = $brid");

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function deleteVoucher($faid, $brid) {

		$this->db->where(array(
			'faid' => $faid,
			'brid' => $brid
			));
		$result = $this->db->get('fee_assign_main');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'faid' => $faid,
				'brid' => $brid
				));
			$result = $this->db->delete('fee_assign_main');

			$this->db->where(array(
				'faid' => $faid,
				'brid' => $brid
				));
			$result = $this->db->delete('fee_assign_detail');

		} else {
			return false;
		}
	}

	public function deleteFeeRecieveVoucher($frid, $brid) {

		$this->db->where(array(
			'dcno' => $frid,
			'etype' => 'frv',
			'brid' => $brid
			));
		$result = $this->db->delete('pledger');


		$this->db->where(array(
			'frid' => $frid, 
			'brid' => $brid
			));
		$result = $this->db->delete('feerec');

		return true;

	}

	// fetchs the previous saved record
	public function fetchStudents($dcno, $etype, $brid) {

		$qry="SELECT DISTINCT m.dcno, m.stdid, stu.name, stu.pid, stu.feetype, m.fid, m.secid, sec.name AS 'section_name', m.brid, m.date, m.claid, m.datefrom, m.dateto, m.lastdate, m.latefee,ifnull(are.balance,0) as arears,br.name As Brach_name,br.address,br.phone_no
		FROM `feemain` AS m
		INNER JOIN `feedetail` AS d ON m.fmid = d.fmid
		INNER JOIN student AS stu ON stu.stdid = m.stdid
		INNER JOIN class AS cls ON cls.claid = m.claid
		INNER JOIN section AS sec ON sec.secid = m.secid
		left  JOIN branch AS br ON m.brid = br.brid
		left join(
		select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid from pledger where date<(select datefrom from feemain where etype = '". $etype ."' and dcno = $dcno and brid = $brid limit 1) group by pid
		) as are on are.pid=stu.pid

		WHERE m.etype = '". $etype ."' AND m.dcno = $dcno AND m.brid = $brid AND d.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND br.brid =$brid";

		$result = $this->db->query($qry);

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	// fetchs the previous saved record
	public function fetchCharges($dcno, $etype, $brid) {

		$result = $this->db->query("SELECT d.chno AS 'chid', ch.description, ch.pid AS 'charges_pid', d.amount, d.concession, d.namount, m.stdid FROM `feemain` AS m INNER JOIN `feedetail` AS d ON m.fmid = d.fmid INNER JOIN charges AS ch ON ch.chid = d.chno WHERE m.etype = '". $etype ."' AND m.dcno = $dcno AND m.brid = $brid AND d.brid = $brid AND ch.brid = $brid ORDER BY m.stdid, d.chno");
		return $result->result_array();
	}
	public function fetchFee($vrnoa, $etype, $brid) {

		$result = $this->db->query("SELECT d.chno,d.amount,d.concession,d.namount,d.type,m.*,ifnull(are.balance,0) as arears
			FROM feemain AS m
			INNER JOIN feedetail AS d ON m.fmid = d.fmid
			INNER JOIN section AS sec ON sec.secid = stu.secid
			left join(
			select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid from pledger where date<(select datefrom from feemain where etype = '". $etype ."' and fmid = $vrnoa and brid = $brid) group by pid
			) as are on are.pid=stu.pid
			where m.etype = '". $etype ."' and m.fmid = $vrnoa and m.brid = $brid ");

		
		return $result->result_array();
		
	}

	public function fetchFeeIssue($fmid, $etype, $brid) {

		$result = $this->db->query("SELECT DISTINCT d.chno AS 'chid', ch.description AS 'charge_name', ch.pid, d.amount AS 'charge_amount', d.concession, d.namount, m.stdid, stu.pid AS 'student_pid', m.date
			FROM `feemain` AS m
			INNER JOIN `feedetail` AS d ON m.fmid = d.fmid
			INNER JOIN student AS stu ON stu.stdid = m.stdid
			INNER JOIN charges AS ch ON ch.chid = d.chno
			WHERE m.etype = '". $etype ."' AND m.fmid = $fmid AND m.brid = $brid AND d.brid = $brid AND stu.brid = $brid AND ch.brid = $brid
			GROUP BY d.chno");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function fetchFeeIssueByChalan($dcno, $etype, $brid,$stdid) {

		$result = $this->db->query("SELECT DISTINCT d.chno AS 'chid', ch.description AS 'charge_name', ch.pid, d.amount AS 'charge_amount', d.concession, d.namount, m.stdid, stu.pid AS 'student_pid', m.date,ifnull(d.arears,0) as arears,ifnull(m.latefee,0) as latefee,date_format(m.lastdate,'%Y/%m/%d') as lastdate
			FROM `feemain` AS m
			INNER JOIN `feedetail` AS d ON m.fmid = d.fmid
			INNER JOIN student AS stu ON stu.stdid = m.stdid
			INNER JOIN charges AS ch ON ch.chid = d.chno
			WHERE m.etype = '". $etype ."' AND m.dcno = $dcno and stu.stdid=$stdid AND m.brid = $brid AND d.brid = $brid AND stu.brid = $brid AND ch.brid = $brid
			GROUP BY d.chno");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}


	// fetchs the previous saved record
	public function fetchFeeRecieve($dcno, $brid) {

		$result = $this->db->query("SELECT frid, pid, fine, discount, feerec.date, netamount, rno,arears,user.uname as user_name,date_time
			FROM feerec
			inner join user on user.uid=feerec.uid and user.brid=feerec.brid
			WHERE frid = $dcno AND feerec.brid = $brid");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->row_array();
		}
	}

	// fetchs the previous saved record
	public function fetchChargesForFeeRecieve($dcno, $brid) {
		$result = $this->db->query("select pledger.pid, debit as 'charge_amount', pledger.chid, charges.description as 'charge_name'from pledger inner join charges on charges.chid = pledger.chid where etype = 'frv' and pledger.chid <> '0' and dcno = $dcno AND pledger.brid = $brid AND charges.brid = $brid");
		return $result->result_array();

	}
	public function fetchFeeRecieveReport($dcno, $brid) {

		$result = $this->db->query("Select  fr.*

			From ( feerec  as fr
			Inner Join  pledger as pdgr On pdgr.pid=fr.pid)

			INNER JOIN branch AS br ON  br.brid = fr.brid 
			where  fr.frid = $dcno AND fr.brid = $brid");
		return $result->result_array();
	}



	public function deleteVoucherOtherIsuAdmi($dcno, $etype, $brid) {

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'brid' => $brid
			));
		$result = $this->db->delete('pledger');

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'brid' => $brid
			));
		$result = $this->db->get('feemain');
		$records = $result->result_array();

		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'dcno' => $dcno,
				'etype' => $etype,
				'brid' => $brid
				));
			$result = $this->db->delete('feemain');

			foreach ($records as $record) {

				$this->db->where(array(
					'fmid' => $record['fmid'],
					'brid' => $brid
					));
				$this->db->delete('feedetail');
			}

		} else {
			return false;
		}
	}

	public function simpleVrnoCheck( $name ) {

		$this->db->where(array(
			'rno' => $name
			));
		$result = $this->db->get('feerec');

		// if we find some data then return false
		if ( $result->num_rows() > 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function updateVrnoaCheck($vrno, $chalanno,$brid) {

		$result = $this->db->query("SELECT *
			FROM feerec
			WHERE frid <> ". $vrno ." AND rno = '". $chalanno ."' AND brid = ". $brid ."");

		if ( $result->num_rows() > 0 ) {
			return false;
		} else {
			return true;
		}
	}
	
	public function monthlyFeeAssignReport($from, $to,$orderby, $brid,$crit) {

		$result = $this->db->query("SELECT $orderby AS voucher, fd.amount, DATE_FORMAT(fm.date, '%d/%m/%Y') AS date, fm.dcno, stu.name, stu.fname, br.name AS 'branch_name', sec.name AS 'section_name', ch.description, cls.name AS 'class_name', fm.etype, fm.fmid,us.uid,us.uname,stu.stdid 
			FROM (((((charges AS ch
			INNER JOIN feedetail AS fd ON ch.chid=fd.chno and fd.brid=ch.brid)
			INNER JOIN feemain AS fm ON fd.fmid=fm.fmid and fm.brid=ch.brid)
			INNER JOIN student AS stu ON fm.stdid=stu.stdid and stu.brid=ch.brid)
			INNER JOIN class AS cls ON fm.claid=cls.claid and cls.brid=ch.brid)
			INNER JOIN section AS sec ON stu.secid=sec.secid and sec.brid=ch.brid)
			INNER JOIN branch AS br ON stu.brid=br.brid 
			Left JOIN  user AS us ON us.uid=br.brid
			WHERE DATE(fm.date) >= '". $from ."' AND DATE(fm.date) <= '". $to ."'  AND br.brid = $brid $crit 
			ORDER BY $orderby, br.name, cls.name, sec.name, stu.name");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function monthlyFeeIssuanceReportChargesWise($from, $to, $brid) {

		$result = $this->db->query("SELECT fd.amount, DATE_FORMAT(fm.date, '%d-%m-%Y') AS DATE, fm.dcno, stu.fname, br.name as 'branch_name', sec.name as 'section_name', ch.description, cls.name as 'class_name', fm.etype, fm.fmid FROM ((((((charges AS ch INNER JOIN feedetail AS fd ON ch.chid=fd.chno) INNER JOIN feemain AS fm ON fd.fmid=fm.fmid) INNER JOIN student AS stu ON fm.stdid=stu.stdid) INNER JOIN class AS cls ON fm.claid=cls.claid) INNER JOIN section AS sec ON stu.secid=sec.secid) INNER JOIN branch AS br ON stu.brid=br.brid) WHERE DATE(`date`) >= '". $from ."' AND DATE(`date`) <= '". $to ."' AND ch.brid = $brid AND fd.brid = $brid AND fm.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND br.brid = $brid GROUP BY ch.description ORDER BY ch.description, br.name, cls.name, sec.name");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}
	public function fetchAll_Admission($from, $to, $orderby, $crit,$brid)
	{

		
		$result = $this->db->query("SELECT $orderby AS voucher,us.uid,us.uname,DATE_FORMAT(fm.date, '%d/%m/%Y') AS vrdate, stu.stdid, stu.name as student_name, stu.fname, fd.amount, ch.description, br.name AS 'branch_name', sec.name AS 'section_name', cls.name AS 'class_name', fd.concession, fd.namount
			FROM (((((feemain AS fm
			INNER JOIN student AS stu ON fm.stdid=stu.stdid)
			INNER JOIN feedetail AS fd ON fm.fmid=fd.fmid)
			INNER JOIN charges AS ch ON fd.chno=ch.chid)
			INNER JOIN section AS sec ON stu.secid=sec.secid)
			INNER JOIN class AS cls ON stu.claid=cls.claid)
			INNER JOIN branch AS br ON stu.brid=br.brid
			Left JOIN  user AS us ON us.uid=br.brid
			WHERE fm.etype='admission charges' AND DATE(`fm`.`date`) >= '". $from ."' AND DATE(`fm`.`date`) <= '". $to ."' AND ch.brid=$brid AND sec.brid=$brid AND stu.brid=$brid AND cls.brid = $brid AND fm.brid = $brid AND fd.brid = $brid AND br.brid = $brid  $crit
			ORDER BY $orderby , br.name, cls.name, sec.name ");
		
		return $result->result_array();
	}
	public function feeAssignReport($from, $to, $brid) {

		$result = $this->db->query("SELECT DATE_FORMAT(fam.fadate, '%d-%m-%Y') AS date, fam.faid, ch.description, fad.amount, fc.name AS 'fee_category', cls.name AS 'class_name' FROM ((((fee_assign_main AS fam INNER JOIN fee_assign_detail AS fad ON fam.faid=fad.faid) INNER JOIN fee_category AS fc ON fam.fcid=fc.fcid) INNER JOIN charges ch ON fad.chid=ch.chid) INNER JOIN class AS cls ON fam.claid=cls.claid) WHERE DATE(`fam`.`fadate`) >= '". $from ."' AND DATE(`fam`.`fadate`) <= '". $to ."' AND fam.brid = $brid AND fad.brid = $brid AND fc.brid = $brid AND ch.brid = $brid AND cls.brid =$brid ORDER BY cls.name, fc.name");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function feeConcessionReport($from, $to, $brid) {

		$result = $this->db->query("SELECT stu.stdid, stu.name, stu.fname, fd.amount, ch.description, br.name AS 'branch_name', sec.name AS 'section_name', cls.name AS 'class_name', fd.concession, fd.namount FROM (((((feemain AS fm INNER JOIN student AS stu ON fm.stdid=stu.stdid) INNER JOIN feedetail AS fd ON fm.fmid=fd.fmid) INNER JOIN charges AS ch ON fd.chno=ch.chid) INNER JOIN section AS sec ON stu.secid=sec.secid) INNER JOIN class AS cls ON stu.claid=cls.claid) INNER JOIN branch AS br ON stu.brid=br.brid WHERE DATE(`fm`.`date`) >= '". $from ."' AND DATE(`fm`.`date`) <= '". $to ."' AND fm.brid = $brid AND stu.brid = $brid AND fd.brid = $brid AND ch.brid = $brid AND sec.brid = $brid AND cls.brid = $brid AND br.brid = $brid HAVING fd.concession > 0 ORDER BY br.name, sec.name, stu.stdid");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function feeRecStudentWiseReport($from, $to,$orderby,$crit,$brid) {

		$result = $this->db->query("SELECT $orderby AS voucher, DATE_FORMAT(ldgr.date, '%d/%m/%Y') AS vrdate, ldgr.dcno,ldgr.chid,charges.chid, charges.description AS 'charge_name', ldgr.credit, date(ldgr.date) AS date, ldgr.invoice, br.name AS 'branch_name', ldgr.description, p.name, cls.name AS 'class_name', sec.name AS 'section_name', p.etype,us.uid,us.uname
			FROM	pledger AS ldgr 
			INNER JOIN ((((	student AS stu 
			INNER JOIN class AS cls ON stu.claid=cls.claid AND cls.brid=stu.brid )
			INNER JOIN section AS sec ON stu.secid=sec.secid AND sec.brid=stu.brid) 
			INNER JOIN branch AS br ON stu.brid=br.brid AND br.brid=stu.brid) 
			INNER JOIN party AS p ON stu.pid=p.pid AND p.brid=stu.brid) ON ldgr.pid=p.pid 
			Left JOIN  user AS us ON us.uid=br.brid
			Left JOIN charges ON charges.chid=ldgr.chid
			WHERE ldgr.etype='frv' AND DATE(`ldgr`.`date`) >= '". $from ."' AND DATE(`ldgr`.`date`) <= '". $to ."' AND ldgr.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND br.brid = $brid AND p.brid = $brid $crit 
			ORDER BY $orderby, br.name, cls.name, sec.name, p.name");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function monthlyFeeRcvReportChargesWise($from, $to, $brid) {

		$result = $this->db->query("SELECT ldgr.dcno, ldgr.credit,  ldgr.invoice, br.name AS 'branch_name', ldgr.description, ch.description, ldgr.chid, ch.chid, sec.name AS 'section_name', cls.name AS 'class_name'FROM ((((pledger AS ldgr INNER JOIN student AS stu ON ldgr.pid=stu.pid) INNER JOIN charges AS ch ON ldgr.chid=ch.chid) INNER JOIN class AS cls ON stu.claid=cls.claid) INNER JOIN section AS sec ON stu.secid=sec.secid) INNER JOIN branch AS br ON stu.brid=br.brid WHERE ldgr.chid<>0 AND ldgr.etype='frv' AND DATE(`ldgr`.`date`) >= '". $from ."' AND DATE(`ldgr`.`date`) <= '". $to ."' AND ldgr.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND br.brid = $brid ORDER BY ch.description, br.name, cls.name, sec.name");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}
	public function feeDefaulterReport($from, $to,$orderby,$crit,$brid) {

		$result = $this->db->query("SELECT $orderby AS voucher,DATE_FORMAT(ldgr.date, '%d/%m/%Y') AS date,ldgr.dcno,stu.mobile,stu.stdid as stdid, IFNULL(SUM(ldgr.debit),0)- IFNULL(SUM(ldgr.credit),0) AS balance,ldgr.pid, stu.name AS student_name, stu.fname AS father_name, stu.mobile, stu.stdid, br.name AS branch_name, cls.name AS class_name, sec.name AS section_name,us.uid,us.uname 
			FROM	pledger AS ldgr
			INNER JOIN (((	student AS stu 
			INNER JOIN class AS cls ON stu.claid=cls.claid)
			INNER JOIN section AS sec ON stu.secid=sec.secid) 
			INNER JOIN branch AS br ON stu.brid=br.brid) ON ldgr.pid=stu.pid
			Left JOIN  user AS us ON us.uid=br.brid
			WHERE  DATE(`ldgr`.`date`) >= '". $from ."' AND DATE(`ldgr`.`date`) <= '". $to ."'  AND ldgr.brid = $brid AND stu.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND br.brid = $brid 
			$crit 
			GROUP BY ldgr.pid HAVING IFNULL(SUM(ldgr.debit),0)- IFNULL(SUM(ldgr.credit),0) >0  
			order by $orderby, br.name, cls.name, sec.name");

		if ($result->num_rows() == 0) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	

	public function isFeeAlreadyAssignedToClass($dcno, $brid, $claid, $fcid) {

		$result = $this->db->query("SELECT * FROM fee_assign_main where faid <> $dcno and claid = $claid and brid = $brid AND fcid = $fcid");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
}


/* End of file fees.php */
/* Location: ./application/models/fees.php */