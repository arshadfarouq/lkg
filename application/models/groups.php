<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId( $table, $brid ) {

		$maxId = "";
		$col = "";
		$dbTableName = "";

		if ( $table === 'class' ) {

			$col = "claid";
			$dbTableName = "class";
		} else if ( $table === 'section' ) {

			$col = "secid";
			$dbTableName = "section";
		}

		$this->db->where(array('brid' => $brid));
		$this->db->select_max( $col );
		$result = $this->db->get( $dbTableName );

		$row = $result->row_array();
		$maxId = $row[$col];

		return $maxId;
	}

	public function getAllBranches() {

		$this->db->select('brid, name');
		$result = $this->db->get('branch');

		return $result->result_array();
	}

	public function fetchClassesByBranch( $brid ) {

		$result = $this->db->query('SELECT `class`.`claid`,`class`.`name` FROM `class` INNER JOIN `branch` ON `class`.`brid` = `branch`.`brid` WHERE `class`.`brid` = '. $brid );

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function fetchSectionsByBranchAndClass( $brid, $claid ) {

		$result = $this->db->query('SELECT `secid`, `name`, `claid`, `brid`, `room`, `staid` FROM `section` WHERE `claid` = '. $claid .' AND `brid` = '. $brid);

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function saveClass( $classDetail ) {

		// check if the class is already saved or not
		$this->db->where(array(
								'claid' => $classDetail['claid'],
								'brid' => $classDetail['brid']
							));
		$result = $this->db->get('class');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0) {

			$this->db->where(array(
									'claid' => $classDetail['claid'],
									'brid' => $classDetail['brid']
								));
			$result = $this->db->update('class', $classDetail);
			$affect = $this->db->affected_rows();
		} else {	// if less than or equal to 0 then insert it

			$result = $this->db->insert('class', $classDetail);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function saveSection( $sectionDetail ) {

		// check if the class is already saved or not
		$this->db->where(array(
								'brid' => $sectionDetail['brid'],
								'claid' => $sectionDetail['claid'],
								'secid' => $sectionDetail['secid']
							));
		$result = $this->db->get('section');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0) {

			$this->db->where(array(
									'brid' => $sectionDetail['brid'],
									'claid' => $sectionDetail['claid'],
									'secid' => $sectionDetail['secid']
								));
			$result = $this->db->update('section', $sectionDetail);
			$affect = $this->db->affected_rows();
		} else {	// if less than or equal to 0 then insert it
			$result = $this->db->insert('section', $sectionDetail);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchClass( $claid, $brid ) {

		$result = $this->db->query("SELECT `claid`, `class`.`name`, `class`.`brid`,`branch`.`name` AS 'branch_name' FROM `class` INNER JOIN `branch` ON `class`.`brid` = `branch`.`brid` WHERE `class`.`claid` = $claid AND `class`.`brid` = $brid ");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchSection( $secid, $brid ) {

		$result = $this->db->query("SELECT `secid`, `name`, `claid`, `brid`, `room`, `staid` FROM `section` WHERE `secid` = $secid AND brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllClasses($brid) {

		$result = $this->db->query("SELECT `claid`, `class`.`name`, `class`.`brid`,`branch`.`name` AS 'branch_name' FROM `class` INNER JOIN `branch` ON `class`.`brid` = `branch`.`brid` WHERE `class`.`brid` = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllSections($brid) {

		$result = $this->db->query("SELECT `section`.`secid`, `section`.`name`, `section`.`room`, `branch`.`name` AS `branch_name`, `class`.`name` AS `class_name` FROM `section` INNER JOIN `branch` ON `section`.`brid` = `branch`.`brid` INNER JOIN `class` on `section`.`claid` = `class`.`claid` WHERE `section`.`brid` = $brid GROUP BY section.brid, section.claid, section.secid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file groups.php */
/* Location: ./application/models/groups.php */