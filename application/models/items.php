<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Items extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMaxId()
    {

        $this->db->select_max('item_id');
        $result = $this->db->get('item');
        $result = $result->row_array();
        return $result['item_id'];
    }

    public function getMaxCatId()
    {
        $this->db->select_max('catid');
        $result = $this->db->get('category');
        $result = $result->row_array();
        return $result['catid'];
    }

    public function getMaxMade_Id()
    {
        $this->db->select_max('made_id');
        $result = $this->db->get('made');
        $result = $result->row_array();
        return $result['made_id'];
    }

    public function getMaxSubCatId()
    {
        $this->db->select_max('subcatid');
        $result = $this->db->get('subcategory');
        $result = $result->row_array();
        return $result['subcatid'];
    }

    public function getMaxBrandId()
    {
        $this->db->select_max('bid');
        $result = $this->db->get('brand');
        $result = $result->row_array();
        return $result['bid'];
    }

    public function getMaxRecipeId()
    {
        $this->db->select_max('rid');
        $result = $this->db->get('recipemain');
        $result = $result->row_array();
        return $result['rid'];
    }

    public function fetchByCol($col)
    {

        $result = $this->db->query("SELECT DISTINCT $col FROM item");
        return $result->result_array();
    }
    public function fetchAllCompanies()
    {

        $result = $this->db->query("SELECT company_id,company_name FROM company");
        return $result->result_array();
    }


    public function delete($item_id)
    {



        $this->db->where(array('item_id' => $item_id));
        $result = $this->db->get('stockdetail');

        if ($result->num_rows() > 0) {
            return 'used';
        }
        
        $this->db->where(array('item_id' => $item_id));
        $result = $this->db->get('orderdetail');

        if ($result->num_rows() > 0) {
            return 'used';
        }


        $this->db->where(array('item_id' => $item_id));
        $this->db->delete('item');

        return true;
    }

    public function deleteCatagory($catid)
    {



        $this->db->where(array('catid' => $catid));
        $result = $this->db->get('item');

        if ($result->num_rows() > 0) {
            return 'used';
        }
        
        
        $this->db->where(array('catid' => $catid));
        $this->db->delete('category');

        return true;
    }

    public function deleteSubCatagory($subcatid)
    {



        $this->db->where(array('subcatid' => $subcatid));
        $result = $this->db->get('item');

        if ($result->num_rows() > 0) {
            return 'used';
        }
        
        
        $this->db->where(array('subcatid' => $subcatid));
        $this->db->delete('subcategory');

        return true;
    }

    public function deleteBrand($bid)
    {



        $this->db->where(array('bid' => $bid));
        $result = $this->db->get('item');

        if ($result->num_rows() > 0) {
            return 'used';
        }
        
        
        $this->db->where(array('bid' => $bid));
        $this->db->delete('brand');

        return true;
    }

    public function deleteMade($made_id)
    {



        $this->db->where(array('made_id' => $made_id));
        $result = $this->db->get('item');

        if ($result->num_rows() > 0) {
            return 'used';
        }
        
        
        $this->db->where(array('made_id' => $made_id));
        $this->db->delete('made');

        return true;
    }

    public function save($item)
    {

        if (isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
            $item['photo'] = $this->upload_photo($item);
        } else {
            unset($item['photo']);
        }

        $item['item_code'] = $this->genItemString($item['catid'], $item['subcatid']);

        $this->db->where(array(
            'item_id' => $item['item_id']
            ));
        $result = $this->db->get('item');

        $affect = 0;
        $item_id = "";
        if ($result->num_rows() > 0) {

            $this->db->where('item_id', $item['item_id']);
            $affect = $this->db->update('item', $item);

            $item_id = $item['item_id'];
        } else {

            unset($item['item_id']);
            if($item['item_barcode']===''){
                $max_id = $this->getMaxId() + 1;
                $max_id = str_pad($max_id,4,"0",STR_PAD_LEFT);
                $item['item_barcode'] =$max_id;
            }
            $this->db->insert('item', $item);
            $affect = $this->db->affected_rows();

            $item_id = $this->db->insert_id();
        }
        

        if ($affect === 0) {
            return false;
        } else {
            return $item_id;
        }
    }

    public function upload_photo($photo)
    {
        $uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/assets/uploads/items/');

        $errors = array(1 => 'php.ini max file size exceeded',
            2 => 'html form max file size exceeded',
            3 => 'file upload was only partial',
            4 => 'no file was attached');

        ($_FILES['photo']['error'] == 0)
        or die($errors[$_FILES['photo']['error']]);

        @is_uploaded_file($_FILES['photo']['tmp_name'])
        or die('Not an HTTP upload');

        @getimagesize($_FILES['photo']['tmp_name'])
        or die('Only image uploads are allowed');

        $now = time();
        while (file_exists($uploadFilename = $uploadsDirectory . $now . '-' . $_FILES['photo']['name'])) {
            $now++;
        }

        // now let's move the file to its final location and allocate the new filename to it
        move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFilename) or die('Error uploading file');

        $path_parts = explode('/', $uploadFilename);
        $uploadFilename = $path_parts[count($path_parts) - 1];
        return $uploadFilename;
    }

    public function fetch($item_id)
    {

        $this->db->where(array(
            'item_id' => $item_id
            ));
        $result = $this->db->get('item');

        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function fetchItemsData($sortBy, $status, $crit)
    {

        $sql = "SELECT *,category.name AS catname,subcategory.name AS modelname,department_table.name AS depname,brand.name AS brandname,made.name AS madename
        FROM item
        left JOIN category ON item.catid = category.catid
        LEFT JOIN subcategory ON item.subcatid = subcategory.subcatid
        LEFT JOIN brand ON item.bid = brand.bid
        LEFT JOIN made ON item.made_id = made.made_id
        LEFT JOIN department_table ON department_table.department_id = item.department_id
        where item.active=" . $status . " $crit order by " . $sortBy;
        //die(print($sql));
        //$sql = "select * from item where item.active=".$status." $crit order by ".$sortBy;
        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchCategory($catid)
    {

        $this->db->where(array('catid' => $catid));
        $result = $this->db->get('category');

        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function fetchMade($made_id)
    {

        $this->db->where(array('made_id' => $made_id));
        $result = $this->db->get('made');

        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }


    public function fetchSubCategory($subcatid)
    {

        $this->db->where(array('subcatid' => $subcatid));
        $result = $this->db->get('subcategory');

        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function fetchBrand($bid)
    {

        $this->db->where(array('bid' => $bid));
        $result = $this->db->get('brand');

        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function isBrandAlreadySaved($brand)
    {
        $result = $this->db->query("SELECT * FROM brand WHERE bid <> " . $brand['bid'] . " AND name = '" . $brand['name'] . "'");
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function saveBrand($brand)
    {

        $this->db->where(array('bid' => $brand['bid']));
        $result = $this->db->get('brand');

        if ($result->num_rows() > 0) {

            $this->db->where(array('bid' => $brand['bid']));
            $this->db->update('brand', $brand);
            return true;
        } else {
            unset($brand['bid']);
            $this->db->insert('brand', $brand);
            $id = $this->db->insert_id();
            $this->db->where(array('bid' => $id));
            $result = $this->db->get('brand');
            return $result->row_array();
        }

        return true;
    }

    public function fetchAll($activee = -1)
    {
        $sts = '';
        if ($activee == -1) {
            $sts = '';
        } else {
            $sts = ' where item.active=1 ';
        }

        $result = $this->db->query("SELECT item.*,item.size, category.name AS 'category_name',
            subcategory.name AS 'subcategory_name', brand.name AS 'brand_name', made.name AS 'made_name',
            ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight, item.item_barcode
            FROM item
            left JOIN category ON item.catid = category.catid 
            left JOIN subcategory ON item.subcatid = subcategory.subcatid 
            left JOIN brand ON item.bid = brand.bid
            left JOIN made ON item.made_id = made.made_id
            LEFT JOIN department_table ON department_table.department_id = item.department_id
            left join  (select item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight from stockdetail group by item_id) sd on sd.item_id=item.item_id $sts");

        // $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name' FROM item
        // 		left JOIN category ON item.catid = category.catid
        // 		INNER JOIN subcategory ON item.subcatid = subcategory.subcatid
        // 		INNER JOIN brand ON item.bid = brand.bid
        // 		 $sts");


        return $result->result_array();
    }

    public function searchitem($search)
    {


        $qry="";
        $qry="SELECT ifnull(lp.rate,0) as item_last_prate,ifnull(avg_rate.avg_rate,0) as item_avg_rate,item.*,item.size, category.name AS 'category_name',
        subcategory.name AS 'subcategory_name', brand.name AS 'brand_name', made.name AS 'made_name', IFNULL(sd.stqty,0) AS stqty, IFNULL(sd.stweight,0) AS stweight, item.item_barcode
        FROM item
        left JOIN category ON item.catid = category.catid
        LEFT JOIN subcategory ON item.subcatid = subcategory.subcatid
        LEFT JOIN brand ON item.bid = brand.bid
        LEFT JOIN made ON item.made_id = made.made_id
        LEFT JOIN department_table ON department_table.department_id = item.department_id
        LEFT JOIN (
            SELECT item_id, IFNULL(SUM(qty),0) stqty, IFNULL(SUM(weight),0) stweight
            FROM stockdetail
            GROUP BY item_id) sd ON sd.item_id=item.item_id
left join(
    SELECT ifnull(SUM(d.netamount),0)/ ifnull(SUM(d.qty),0) AS avg_rate,d.item_id
    FROM stockdetail d
    INNER JOIN stockmain m ON m.stid = d.stid
    WHERE m.etype='PURCHASE'
    GROUP BY d.item_id
    HAVING IFNULL(SUM(d.qty),0)<>0
) as avg_rate on avg_rate.item_id=item.item_id
left join(
    SELECT t1.item_id, t1.rate,t1.stid
    FROM stockdetail t1
    JOIN (
        SELECT item_id, MAX(stdid) stdid
        FROM stockdetail
        WHERE STID IN (
            SELECT STID
            FROM stockmain
            WHERE ETYPE='purchase')
GROUP BY item_id) t2 ON t1.item_id = t2.item_id AND t1.stdid = t2.stdid
GROUP BY t1.item_id,t1.rate,t1.stid
) as lp on lp.item_id=item.item_id
WHERE item.active=1 AND(
    item.item_des LIKE '%".$search."%' 
    OR item.item_code LIKE '%".$search."%' 
    OR item.uom LIKE '%".$search."%' 
    OR item.uname LIKE '%".$search."%' 
    OR item.short_code LIKE '%".$search."%' 
    OR category.name LIKE '%".$search."%' 
    OR subcategory.name LIKE '%".$search."%' 
    OR item.item_barcode LIKE '%".$search."%' 
    OR brand.name LIKE '". $search ."%') 
LIMIT 0, 20;";
        // echo $qry;
$result = $this->db->query($qry);




return $result->result_array();
}
public function getiteminfobyid($item_id)
{


    $qry="";
    $qry="SELECT ifnull(lp.rate,0) as item_last_prate,ifnull(avg_rate.avg_rate,0) as item_avg_rate,item.*,ifnull(item.cost_price,0) as cost_price,ifnull(item.cost_price,0) as prate1,ifnull(item.srate,0) as srate1,item.size, category.name AS 'category_name',
    subcategory.name AS 'subcategory_name', brand.name AS 'brand_name', made.name AS 'made_name', IFNULL(sd.stqty,0) AS stqty, IFNULL(sd.stweight,0) AS stweight, item.item_barcode
    FROM item
    left JOIN category ON item.catid = category.catid
    LEFT JOIN subcategory ON item.subcatid = subcategory.subcatid
    LEFT JOIN brand ON item.bid = brand.bid
    LEFT JOIN made ON item.made_id = made.made_id
    LEFT JOIN department_table ON department_table.department_id = item.department_id
    LEFT JOIN (
        SELECT item_id, IFNULL(SUM(qty),0) stqty, IFNULL(SUM(weight),0) stweight
        FROM stockdetail
        GROUP BY item_id) sd ON sd.item_id=item.item_id
left join(
    SELECT ifnull(SUM(d.netamount),0)/ ifnull(SUM(d.qty),0) AS avg_rate,d.item_id
    FROM stockdetail d
    INNER JOIN stockmain m ON m.stid = d.stid
    WHERE m.etype='PURCHASE'
    GROUP BY d.item_id
    HAVING IFNULL(SUM(d.qty),0)<>0
) as avg_rate on avg_rate.item_id=item.item_id
left join(
    SELECT t1.item_id, t1.rate,t1.stid
    FROM stockdetail t1
    JOIN (
        SELECT item_id, MAX(stdid) stdid
        FROM stockdetail
        WHERE STID IN (
            SELECT STID
            FROM stockmain
            WHERE ETYPE='purchase')
GROUP BY item_id) t2 ON t1.item_id = t2.item_id AND t1.stdid = t2.stdid
GROUP BY t1.item_id,t1.rate,t1.stid
) as lp on lp.item_id=item.item_id
WHERE item.active=1 AND  item.item_id=$item_id
;";
        // echo $qry;
$result = $this->db->query($qry);




return $result->result_array();
}


public function fetchAll_report($from, $to, $orderby, $status)
{

    if ($status == 'all_item') {
        $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name' FROM item left JOIN category ON item.catid = category.catid INNER JOIN subcategory ON item.subcatid = subcategory.subcatid INNER JOIN brand ON item.bid = brand.bid order by $orderby ");
    } else {
        $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name' FROM item left JOIN category ON item.catid = category.catid INNER JOIN subcategory ON item.subcatid = subcategory.subcatid INNER JOIN brand ON item.bid = brand.bid where item.active=$status order by $orderby ");
    }
    return $result->result_array();
}

public function fetchItemsByStock()
{
    $result = $this->db->query("SELECT i.item_id, i.description, round(SUM(d.qty)) stock, round(d.prate) prate FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN item i on d.item_id = i.item_id GROUP BY d.item_id");
    return $result->result_array();
}

public function fetchItemLedgerReport($from, $to, $item_id, $company_id,$crit)
{
    $result = $this->db->query("SELECT party.name AS party_name, m.vrnoa, m.etype,date_format(m.vrdate,'%d/%m/%y') DATE, i.description, m.remarks, dep.name, d.qty, d.weight
        FROM stockmain m
        INNER JOIN stockdetail d ON m.stid = d.stid
        INNER JOIN item i ON i.item_id = d.item_id
        INNER JOIN department dep ON dep.did = d.godown_id
        LEFT JOIN party AS party ON party.pid=m.party_id
        WHERE d.item_id = $item_id AND m.company_id = $company_id AND DATE(m.vrdate) >= '" . $from . "' AND DATE(m.vrdate) <= '" . $to . "' $crit
        ORDER BY m.vrdate, m.vrnoa, dep.name, i.description");

    if ($result->num_rows() > 0) {
        return $result->result_array();
    } else {
        return false;
    }
}

public function fetchItemOpeningStock($to, $item_id, $company_id,$crit)
{
    $q = "SELECT IFNULL(SUM(d.qty), 0) AS 'OPENING_QTY', IFNULL(SUM(d.weight), 0) AS 'OPENING_WEIGHT'
    FROM stockmain AS m
    INNER JOIN stockdetail AS d ON m.stid = d.stid
    WHERE d.item_id = {$item_id} AND DATE(m.VRDATE) < '{$to}' AND m.company_id={$company_id} $crit" ;
    $query = $this->db->query($q);

    return $query->result_array();
}


public function saveCategory($category)
{

    $this->db->where(array('catid' => $category['catid']));
    $result = $this->db->get('category');

    if ($result->num_rows() > 0) {

        $this->db->where(array('catid' => $category['catid']));
        $result = $this->db->update('category', $category);
        return true;
    } else {
        unset($category['catid']);
        $this->db->insert('category', $category);
        $id = $this->db->insert_id();
        $this->db->where(array('catid' => $id));
        $result = $this->db->get('category');
        return $result->row_array();
    }
}

public function saveMade($made)
{

    $this->db->where(array('made_id' => $made['made_id']));
    $result = $this->db->get('made');

    if ($result->num_rows() > 0) {

        $this->db->where(array('made_id' => $made['made_id']));
        $this->db->update('made', $made);
        return true;
    } else {
        unset($made['made_id']);
        $this->db->insert('made', $made);
        $id = $this->db->insert_id();
        $this->db->where(array('made_id' => $id));
        $result = $this->db->get('made');
        return $result->row_array();
    }

        //return true;
}


public function saveSubCategory($category)
{

    $this->db->where(array('subcatid' => $category['subcatid']));
    $result = $this->db->get('subcategory');

    if ($result->num_rows() > 0) {
        $this->db->where(array('subcatid' => $category['subcatid']));
        $this->db->update('subcategory', $category);
        return true;
    } else {
        unset($category['subcatid']);
        $this->db->insert('subcategory', $category);
        $id = $this->db->insert_id();
        $this->db->where(array('subcatid' => $id));
        $result = $this->db->get('subcategory');
        return $result->row_array();
    }
}

public function saveRecipe($recipe, $recipedetail, $rid)
{

    $this->db->where(array('rid' => $rid));
    $result = $this->db->get('recipemain');

    $rid = "";
    if ($result->num_rows() > 0) {

        $result = $result->row_array();
        $rid = $result['rid'];

        $this->db->where(array('rid' => $rid));
        $this->db->update('recipemain', $recipe);

            // delete old items
        $this->db->where(array('rid' => $rid));
        $this->db->delete('recipedetail');
    } else {
        $this->db->insert('recipemain', $recipe);
        $rid = $this->db->insert_id();
    }

    foreach ($recipedetail as $detail) {
        $detail['rid'] = $rid;
        $this->db->insert('recipedetail', $detail);
    }
    return true;
}


public function isCategoryAlreadySaved($category)
{
    $result = $this->db->query("SELECT * FROM category WHERE catid <> " . $category['catid'] . " AND name = '" . $category['name'] . "'");
    if ($result->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

public function isItemAlreadySaved($item)
{

    $itembarcode=$item['item_barcode'];
    $result = $this->db->query("SELECT * FROM item WHERE item_id <> " . $item['item_id'] . " AND item_des = '" . $item['item_des'] . "'");
    if ($result->num_rows() > 0) {
        return true;
    } else {
        if($item['item_barcode']=!''){
            $qry="SELECT * FROM item WHERE item_id <> " . $item['item_id'] . " AND item_barcode = '" . $itembarcode . "'";
            
            $result = $this->db->query($qry);
            if ($result->num_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }else{

            return false;

        }
    }
}

public function isShortCodeAlreadySaved($item)
{

    $result = $this->db->query("SELECT * FROM item WHERE item_id <> " . $item['item_id'] . " AND short_code = '" . $item['short_code'] . "' and short_code <> ''  ");
    if ($result->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}


public function isMadeAlreadySaved($made)
{
    $result = $this->db->query("SELECT * FROM made WHERE made_id <> " . $made['made_id'] . " AND name = '" . $made['name'] . "'");
    if ($result->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

public function isSubCategoryAlreadySaved($subcategory)
{
    $result = $this->db->query("SELECT * FROM subcategory WHERE subcatid <> " . $subcategory['subcatid'] . " AND name = '" . $subcategory['name'] . "'");
    if ($result->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

public function fetchAllCategories()
{
    $result = $this->db->get("category");
    return $result->result_array();
}

public function fetchAllMades()
{
    $result = $this->db->get("made");
    return $result->result_array();
}

public function fetchAllSubCategories()
{
    $result = $this->db->query("SELECT subcategory.*, category.name AS 'category_name' FROM subcategory left JOIN category ON subcategory.catid = category.catid");
    return $result->result_array();
}

public function fetchAllBrands()
{
    $result = $this->db->get("brand");
    return $result->result_array();
}

public function genItemStr($subcatid)
{
    $query = $this->db->query("SELECT category.catid, subcatid FROM category INNER JOIN subcategory ON category.catid = subcategory.catid WHERE subcategory.subcatid = $subcatid");
    if ($query->num_rows() > 0) {
        $result = $query->result_array();

        $catid = str_pad($result[0]['catid'], 2, '0', STR_PAD_LEFT);
        $subcatid = str_pad($result[0]['subcatid'], 2, '0', STR_PAD_LEFT);

        $items_count = $this->count_items($subcatid);
        $code = $items_count + 1;
        $code = str_pad($code, 3, '0', STR_PAD_LEFT);

        return $catid . '-' . $subcatid . '-' . $code;
    }
}

public function genItemString($catid, $subcatid)
{
    $catid = str_pad($catid, 2, '0', STR_PAD_LEFT);
    $subcatid = str_pad($subcatid, 2, '0', STR_PAD_LEFT);

    $items_count = $this->count_items($subcatid);
    $code = $items_count + 1;
    $code = str_pad($code, 3, '0', STR_PAD_LEFT);

    return $catid . '-' . $subcatid . '-' . $code;
}

public function count_items($subcatid)
{
    $query = $this->db->get_where('item', array('subcatid' => $subcatid));
    return $query->num_rows();
}

public function recipeExists($rid, $item_id)
{

    $result = $this->db->query("SELECT * FROM recipemain WHERE item_id = $item_id AND rid <> $rid");

    if ($result->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

public function fetchRecipe($rid)
{
    $result = $this->db->query("SELECT recipemain.*, recipedetail.uom, ROUND(recipedetail.qty, 2) qty, ROUND(recipedetail.weight, 2) weight, recipedetail.item_id AS 'ditem_id', item.item_des FROM recipemain INNER JOIN recipedetail ON recipemain.rid = recipedetail.rid INNER JOIN item item ON item.item_id = recipedetail.item_id WHERE recipemain.rid = $rid");
    if ($result->num_rows() > 0) {
        return $result->result_array();
    } else {
        return false;
    }
}

public function deleteRecipe($rid)
{

    $this->db->where(array('rid' => $rid));
    $result = $this->db->get('recipemain');

    if ($result->num_rows() == 0) {
        return false;
    } else {
        $this->db->where(array('rid' => $rid));
        $result = $this->db->delete('recipemain');
        $this->db->where(array('rid' => $rid));
        $result = $this->db->delete('recipedetail');

        return true;
    }
}
}

/* End of file items.php */
/* Location: ./application/models/items.php */