<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ledgers extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId($etype,$company_id) {

		$this->db->where(array('etype' => $etype,'company_id'=>$company_id));
		$this->db->select_max('dcno');
		$result = $this->db->get('pledger');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function fetchAllLedgersPayments( $company_id,$etype ) {

		$result = $this->db->query("SELECT p.name AS party_name,pledger.description,pledger.debit,pledger.credit,pledger.deduction,pledger.invoice,time(pledger.date_time) as date_time,user.uname as user_name,pd.cheque_no as chq_no,pd.cheque_date as chq_date,pledger.dcno
			FROM pledger pledger
			INNER JOIN party p ON p.pid = pledger.pid
			INNER JOIN user ON user.uid = pledger.uid
			INNER JOIN company c ON c.company_id = pledger.company_id
			INNER JOIN pd_cheque pd ON pd.dcno = pledger.dcno and pd.etype=pledger.etype
			WHERE pledger.company_id= '".$company_id."' AND pledger.etype= '".$etype."'  AND pledger.date = CURDATE()
			ORDER BY pledger.pledid desc limit 10;");
		
		return $result->result_array();
		
	}
	public function fetchAccountVoucher( $vrnoa, $etype,$company_id ) {
		$sql = "SELECT p.pid,p.date_time AS datetime, DATE(p.date) AS date, p.dcno,party.account_id,party.name,p.description,p.debit,p.credit
		FROM pledger p
		INNER JOIN party party ON party.pid = p.pid
		LEFT JOIN ordermain m ON m.vrnoa = p.dcno AND m.etype = p.etype
		WHERE p.etype = '". $etype ."' AND p.dcno = $vrnoa AND p.company_id = $company_id";
		$result = $this->db->query($sql);
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllExpenses( $company_id ) {

		$result = $this->db->query("SELECT party.NAME, SUM(DEBIT) AS 'AMOUNT'
			FROM pledger
			INNER JOIN party ON pledger.pid = party.pid
			INNER JOIN level3 ON party.level3 = level3.l3
			INNER JOIN level2 ON level2.l2 = level3.l2
			INNER JOIN level1 ON level1.l1 = level2.l1
			WHERE level1.name ='EXPENSES' AND level2.name <> 'COST OF GOODS SOLD' and pledger.company_id=$company_id 
			AND pledger.DATE = CURDATE()
			GROUP BY pledger.pid
			HAVING IFNULL(SUM(DEBIT),0)<>0;");
		
		return $result->result_array();
		
	}
	

	

	public function save($ledgers, $dcno, $etype,$voucher_type_hidden) {

		$this->db->trans_begin();

		if ($voucher_type_hidden!='new'){
			$this->db->where(array(
				'dcno' => $dcno,
				'etype' => $etype,
				));
			$affect = $this->db->delete('pledger');
		}
		$affect = 0;
		foreach ($ledgers as $ledger) {
			$ledger["dcno"] = $dcno;
			$this->db->insert('pledger', $ledger);
			$affect = $this->db->affected_rows();
		}

		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function deleteVoucher($dcno, $etype,$company_id) {

		$this->db->where(array(
			'dcno' => $dcno,
			'etype' => $etype,
			'company_id'=> $company_id
			));

		$result = $this->db->get('pledger');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'dcno' => $dcno,
				'etype' => $etype,
				'company_id'=> $company_id
				));
			$result = $this->db->delete('pledger');

		} else {
			return false;
		}
	}

	public function fetch($dcno, $etype,$company_id) {

		$qry ="SELECT CAST(DATE_FORMAT(ldgr.date_time,'%d/%m/%y %h:%i %p') AS CHAR) AS date_time,ldgr.pledid,ldgr.uid, ldgr.pid, ldgr.description, ldgr.deduction, ldgr.date, ldgr.debit, ldgr.credit, ldgr.invoice, ldgr.dcno, ldgr.etype, cid, chid, rose, secid, pid_key, p.name AS 'party_name',u.uname AS user_name
			FROM pledger AS ldgr
			left  JOIN party AS p ON ldgr.pid = p.pid
			LEFT JOIN user AS u ON u.uid=ldgr.uid
			WHERE ldgr.etype = '". $etype ."' AND ldgr.dcno = ".$dcno." AND ldgr.company_id = $company_id";
			
		
		$result = $this->db->query($qry) ;

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function fetchVoucherRange($from, $to, $etype)
	{
		$query = $this->db->query("SELECT pledid, ldgr.pid, description, date, (debit+credit) AS amount, invoice, ldgr.dcno, ldgr.etype, cid, chid, rose, secid, pid_key, p.name AS 'party_name', round(ldgr.debit, 2) debit, round(ldgr.credit, 2) credit FROM pledger AS ldgr LEFT OUTER JOIN party AS p ON ldgr.pid = p.pid WHERE ldgr.date BETWEEN '". $from ."' AND '". $to ."' AND ldgr.etype = '". $etype ."'");
		return $query->result_array();
	}

	public function getAccLedgerReport($from, $to, $pid,$company_id) {

		$result = $this->db->query("CALL acc_ledger('". $from ."', '". $to ."', $pid, $company_id)");

		if ($result->num_rows > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function getAccLedgerReport22($from, $to, $pid, $company_id) {

		$result = $this->db->query("SELECT ldgr.pledid,ldgr.etype,ldgr.uid, ldgr.pid, ldgr.description, ldgr.deduction, ldgr.date, ldgr.debit, ldgr.credit, ldgr.invoice, ldgr.dcno, ldgr.etype, ldgr.cid, ldgr.chid, ldgr.rose, ldgr.secid, ldgr.pid_key, p.name AS 'party_name', p.mobile + '. ' + p.phone AS 'contact', p.address AS 'address',p.account_id as 'acc_id',u.uname as user_name FROM pledger AS ldgr LEFT OUTER JOIN party AS p ON ldgr.pid = p.pid inner join user as u on u.uid=ldgr.uid WHERE ldgr.pid = $pid and ldgr.date between '". $from ."' and '". $to ."' and ldgr.company_id =$company_id  order by date(ldgr.date),ldgr.pledid");

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

}

/* End of file ledgers.php */
/* Location: ./application/models/ledgers.php */