<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMaxVrno($etype, $company_id)
    {

        $result = $this->db->query("SELECT MAX(vrno) vrno FROM ordermain WHERE etype = '" . $etype . "' AND company_id=" . $company_id . "  AND DATE(vrdate) = DATE(NOW())");
        $row = $result->row_array();
        $maxId = $row['vrno'];

        return $maxId;
    }

    public function fetchAllPurchases( $company_id,$etype ) {

        $result = $this->db->query("SELECT round(ordermain.discount,0) as discount,round(ordermain.expense,0) as expense,round(ordermain.tax,0) as tax,ordermain.vrnoa, DATE_FORMAT(ordermain.vrdate,'%d %b %y') AS DATE,p.name AS party_name,ordermain.remarks,round(ordermain.taxpercent,1) as taxpercent,round(ordermain.exppercent,1) as exppercent,round(ordermain.discp,1)as discp,ordermain.paid,round(ordermain.namount,0) as namount,user.uname as user_name,TIME(ordermain.date_time) as date_time,ordermain.vrnoa
            FROM ordermain ordermain
            INNER JOIN party p ON p.pid = ordermain.party_id
            INNER JOIN user ON user.uid = ordermain.uid
            INNER JOIN company c ON c.company_id = ordermain.company_id
            WHERE ordermain.company_id= '".$company_id."' AND ordermain.etype= '".$etype."' AND ordermain.vrdate = CURDATE()
            ORDER BY ordermain.vrnoa DESC
            LIMIT 10");
        // if ( $result->num_rows() > 0 ) {
        return $result->result_array();
        // } else {
        //  return false;
        // }
    }
    

    public function last_5_srate($crit, $company_id, $etype, $date)
    {
        $result = $this->db->query("SELECT  m.vrnoa, date_format(m.vrdate,'%d/%m/%y') vrdate,m.etype, ROUND(d.qty, 2) qty,d.rate
          FROM ordermain AS m
          INNER JOIN orderdetail AS d ON m.oid = d.oid
          INNER JOIN item i ON i.item_id = d.item_id
          LEFT JOIN department dep ON dep.did = d.godown_id
          LEFT JOIN party party ON party.pid=m.party_id
          WHERE   m.etype = '" . $etype . "' and m.company_id = $company_id  and m.vrdate <= '" . $date . "' $crit
          ORDER BY m.vrdate  desc
          limit 5");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function lastPRate($companyId, $itemId, $date)
    {
        $result = $this->db->query("SELECT ifnull(orderdetail.rate,0) - ifnull(orderdetail.damount,0) as prate from ordermain inner join orderdetail on ordermain.oid = orderdetail.oid
            where ordermain.etype = 'purchase' and orderdetail.item_id = ".$itemId." and ordermain.vrdate <='".$date."' and ordermain.company_id = ".$companyId."
            ORDER BY ordermain.vrdate  desc");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    public function lastRateEtype($company_id, $item_id, $date,$party_id,$etype)
    {
        $result = $this->db->query("SELECT  rate ,discount
            FROM orderdetail
            WHERE item_id=$item_id AND oid IN (
                SELECT oid
                FROM ordermain
                WHERE etype='$etype' and party_id=$party_id and vrdate<='$date' and company_id=$company_id)
        ORDER BY oid DESC
        limit 1;");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchlastprate($vrnoa, $vrdate, $item_id, $company_id, $etype)
    {
        $result = $this->db->query("SELECT m.vrnoa,m.vrdate,m.etype,d.rate,d.prate,i.item_des from ordermain m
         INNER JOIN orderdetail d on d.oid = m.oid
         INNER JOIN item i on i.item_id = d.item_id
         where m.vrnoa <= '" . $vrdate . "' and m.etype =  '" . $etype . "' and d.item_id = $item_id and m.company_id = $company_id
         order by m.vrnoa desc limit 1");
// 				$result = $this->db->query("SELECT m.vrnoa,m.vrdate,m.etype,d.rate,i.item_des from ordermain m
// INNER JOIN orderdetail d on d.oid = m.oid
// INNER JOIN item i on i.item_id = d.item_id
// where m.vrdate <= '2015/05/11' and m.etype = 'purchase' and d.item_id = 13 
// order by m.vrdate desc limit 1");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function sendMessage($mobile, $message)

    {

        $ptn = "/^[0-9]/";  // Regex

        $rpltxt = "92";  // Replacement string

        $mobile = preg_replace($ptn, $rpltxt, $mobile);


        // Create the SoapClient instance

        $url = ZONG_API_SERVICE_URL;

        // $soapClient = new SoapClient( $url);

        // var_dump($soapClient->__getFunctions());


        $post_string = '<?xml version="1.0" encoding="utf-8"?>' .

        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .

        '<soap:Body>' .

        '<SendSingleSMS xmlns="http://tempuri.org/">' .

        '<Src_nbr>' . ZONG_API_MOB . '</Src_nbr>' .

        '<Password>' . ZONG_API_PASS . '</Password>' .

        '<Dst_nbr>' . $mobile . '</Dst_nbr>' .

        '<Mask>' . ZONG_API_MASK . '</Mask>' .

        '<Message>' . $message . '</Message>' .

        '</SendSingleSMS>' .

        '</soap:Body>' .

        '</soap:Envelope>';


        $soap_do = curl_init();

        curl_setopt($soap_do, CURLOPT_URL, $url);

        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);

        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($soap_do, CURLOPT_POST, true);

        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);

        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($post_string)));


        $result = curl_exec($soap_do);

        $err = curl_error($soap_do);


        return $result;

    }

    public function fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($etype == 'profit') {
            if ($type == 'detailed') {
                $query = $this->db->query("SELECT $field as voucher,$name,ordermain.etype,ifnull((((orderdetail.rate-orderdetail.prate)*orderdetail.qty)-orderdetail.damount),0) as pls,orderdetail.prate,orderdetail.discount as discountP,orderdetail.damount discountA,orderdetail.netamount as d_netamount,orderdetail.carton,orderdetail.pcs, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,ordermain.vrdate, ordermain.remarks, ordermain.vrnoa, ordermain.remarks,  orderdetail.qty, orderdetail.weight, orderdetail.rate, orderdetail.amount, orderdetail.netamount, item.item_des as 'item_des',item.uom FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype in('sale','salereturn') $crit  order by $orderBy");
                return $query->result_array();
            } else {
                $query = $this->db->query("SELECT $field as voucher,$name,ordermain.etype,ifnull((((SUM(orderdetail.rate)-SUM(orderdetail.prate))*SUM(orderdetail.qty))-SUM(orderdetail.damount)),0) as pls,round(SUM(orderdetail.prate)) prate,round(SUM(orderdetail.discount)) discountP,round(SUM(orderdetail.damount)) discountA,round(SUM(orderdetail.netamount)) d_netamount ,orderdetail.carton,orderdetail.pcs,date(ordermain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(ordermain.VRDATE) VRDATE, ordermain.vrnoa, round(SUM(orderdetail.qty)) qty, round(SUM(orderdetail.weight)) weight, round(SUM(orderdetail.rate)) rate, round(SUM(orderdetail.amount)) amount, round(sum(orderdetail.netamount)) netamount, ordermain.remarks FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype in('sale','sale_return') $crit group by $groupBy order by $orderBy");
                return $query->result_array();
            }
        } else {
            if ($type == 'detailed') {
                $query = $this->db->query("SELECT $field as voucher,$name,orderdetail.discount as discountP,orderdetail.damount discountA,orderdetail.netamount as d_netamount,orderdetail.carton,orderdetail.pcs, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,ordermain.vrdate, ordermain.remarks, ordermain.vrnoa, ordermain.remarks,  orderdetail.qty, orderdetail.weight, orderdetail.rate, orderdetail.amount, orderdetail.netamount, item.item_des as 'item_des',item.uom FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit  order by $orderBy");
                return $query->result_array();
            } else {
                $query = $this->db->query("SELECT $field as voucher,$name,round(SUM(orderdetail.discount)) discountP,round(SUM(orderdetail.damount)) discountA,round(SUM(orderdetail.netamount)) d_netamount ,orderdetail.carton,orderdetail.pcs,date(ordermain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(ordermain.VRDATE) VRDATE, ordermain.vrnoa, round(SUM(orderdetail.qty)) qty, round(SUM(orderdetail.weight)) weight, round(SUM(orderdetail.rate)) rate, round(SUM(orderdetail.amount)) amount, round(sum(orderdetail.netamount)) netamount, ordermain.remarks FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit group by $groupBy order by $orderBy");
                return $query->result_array();
            }
        }

    }

    public function fetchitemcReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT $field as voucher,$name,item.uom,dept.name as dept_name,orderdetail.discount as discountP,orderdetail.damount discountA,orderdetail.netamount as d_netamount,orderdetail.carton,orderdetail.pcs, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,ordermain.vrdate, ordermain.remarks, ordermain.vrnoa, ordermain.remarks,  orderdetail.qty, orderdetail.weight, orderdetail.rate,orderdetail.etype as detype, orderdetail.amount, orderdetail.netamount, item.item_des as 'item_des',item.uom FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid LEFT JOIN party party ON ordermain.party_id = party.pid              LEFT JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  LEFT JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 LEFT JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  LEFT JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit  order by $orderBy");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT $field as voucher,$name,round(SUM(orderdetail.discount)) discountP,round(SUM(orderdetail.damount)) discountA,round(SUM(orderdetail.netamount)) d_netamount ,orderdetail.carton,orderdetail.pcs,date(ordermain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(ordermain.VRDATE) VRDATE, ordermain.vrnoa, round(SUM(orderdetail.qty)) qty, round(SUM(orderdetail.weight)) weight, round(SUM(orderdetail.rate)) rate, round(SUM(orderdetail.amount)) amount, round(sum(orderdetail.netamount)) netamount, ordermain.remarks FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit group by $groupBy order by $orderBy");
            return $query->result_array();
        }

    }

    public function fetchSaleReportDataSummary($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($type !== 'detailed') {
            $query = $this->db->query("SELECT $field as voucher,$name,  WEEK(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate, 
                ordermain.vrnoa,ordermain.remarks,ordermain.vrdate,party.name as party_name,
                ordermain.tax,ordermain.discount,ordermain.expense,ordermain.paid,ordermain.namount,ordermain.customer_name,transporter.name
                FROM ordermain ordermain 
                INNER JOIN party party ON ordermain.party_id = party.pid 
                LEFT JOIN transporter ON transporter.transporter_id = ordermain.transporter_id
                WHERE  ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit  order by $orderBy");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT $field as voucher,$name,round(SUM(orderdetail.discount)) discountP,round(SUM(orderdetail.damount)) discountA,round(SUM(orderdetail.netamount)) d_netamount ,orderdetail.carton,orderdetail.pcs,date(ordermain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(ordermain.VRDATE) VRDATE, ordermain.vrnoa, round(SUM(orderdetail.qty)) qty, round(SUM(orderdetail.weight)) weight, round(SUM(orderdetail.rate)) rate, round(SUM(orderdetail.amount)) amount, round(sum(orderdetail.netamount)) netamount, ordermain.remarks FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON  orderdetail.godown_id = dept.did WHERE  ordermain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit group by $groupBy order by $orderBy");
            return $query->result_array();
        }

    }

    public function fetchReportDataMain($startDate, $endDate, $company_id, $etype, $uid, $isHold = "")
    {
        $where="";
        $where = ($uid !== 1) ? " and ordermain.uid =$uid " : "";

        $query = $this->db->query("SELECT ordermain.discount,ordermain.expense,ordermain.tax,ordermain.vrnoa,
         date_format(ordermain.vrdate,'%d/%m/%y') as vrdate,p.name AS party_name,ordermain.remarks,ordermain.taxpercent,
         ordermain.exppercent,ordermain.discp,ordermain.paid,ordermain.namount
         FROM ordermain ordermain
         INNER JOIN party p ON p.pid = ordermain.party_id
         INNER JOIN user ON user.uid = ordermain.uid
         INNER JOIN company c ON c.company_id = ordermain.company_id
         WHERE ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id= '" . $company_id . "' AND ordermain.etype= '" . $etype . "' " . $where . "
         ORDER BY ordermain.vrnoa");
        return $query->result_array();


    }

    public function fetchReportDataOrderMainHoldReport($startDate, $endDate, $company_id, $etype, $uid)
    {
        $query = $this->db->query("SELECT ordermain.discount,ordermain.expense,ordermain.tax,ordermain.vrnoa,
         ordermain.vrdate,p.name as party_name,ordermain.remarks,ordermain.taxpercent,
         ordermain.exppercent,ordermain.discp,ordermain.paid,ordermain.namount
         from ordermain ordermain
         INNER JOIN party p ON p.pid = ordermain.party_id
         INNER JOIN user user ON user.uid = ordermain.uid
         INNER JOIN company c ON c.company_id = ordermain.company_id
         WHERE ordermain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "'
         AND ordermain.company_id= '" . $company_id . "' AND ordermain.etype= '" . $etype . "'
         and ordermain.uid = " . $uid . " and is_hold = '1' order by ordermain.vrnoa");
        return $query->result_array();
    }

    public function fetchReportDataItemcMain($startDate, $endDate, $company_id, $etype, $uid)
    {

        $query = $this->db->query("SELECT  orderdetail.etype as detype,g.name as dept_name,item.uom,item.item_des,orderdetail.qty,orderdetail.rate,orderdetail.amount,ordermain.vrnoa,ordermain.vrdate,p.name as party_name,ordermain.remarks,ordermain.taxpercent,ordermain.exppercent,ordermain.discp,ordermain.paid,ordermain.namount from
         ordermain ordermain
         INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid
         INNER JOIN item item ON orderdetail.item_id = item.item_id
         LEFT JOIN party p ON p.pid = ordermain.party_id
         INNER JOIN USER USER ON user.uid = ordermain.uid
         INNER JOIN company c ON c.company_id = ordermain.company_id
         LEFT JOIN department AS g ON g.did = orderdetail.godown_id
         WHERE ordermain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id= $company_id AND ordermain.etype= '" . $etype . "'
         and ordermain.uid = $uid order by ordermain.vrnoa");
        return $query->result_array();


    }

    public function fetchReportDataOrderMain($startDate, $endDate, $company_id, $etype, $uid)
    {

        $query = $this->db->query("SELECT ordermain.vrnoa,ordermain.vrdate,p.name as party_name,ordermain.remarks,ordermain.taxpercent,ordermain.exppercent,ordermain.discp,ordermain.paid,ordermain.namount from
         ordermain ordermain
         INNER JOIN party p ON p.pid = ordermain.party_id
         INNER JOIN USER USER ON user.uid = ordermain.uid
         INNER JOIN company c ON c.company_id = ordermain.company_id
         WHERE ordermain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id= $company_id AND ordermain.etype= '" . $etype . "'
         and ordermain.uid = $uid order by ordermain.vrnoa");
        return $query->result_array();


    }


    public function fetchChartData($period, $company_id, $etype)
    {
        $period = strtolower($period);

        $query = '';

        if ($period === 'daily') {
            $query = "SELECT VRNOA, party.name AS ACCOUNT, NAMOUNT  FROM ordermain as ordermain INNER JOIN party as party  ON party.pid = ordermain.party_id  WHERE ordermain.etype='$etype' AND ordermain.vrdate = CURDATE() AND ordermain.company_id=$company_id order by ordermain.vrdate desc LIMIT 10";
        } else if ($period === 'weekly') {
            $query = "SELECT sum(case when date_format(vrdate, '%W') = 'Monday' then namount else 0 end) as 'Monday', sum(case when date_format(vrdate, '%W') = 'Tuesday' then namount else 0 end) as 'Tuesday', sum(case when date_format(vrdate, '%W') = 'Wednesday' then namount else 0 end) as 'Wednesday', sum(case when date_format(vrdate, '%W') = 'Thursday' then namount else 0 end) as 'Thursday', sum(case when date_format(vrdate, '%W') = 'Friday' then namount else 0 end) as 'Friday', sum(case when date_format(vrdate, '%W') = 'Saturday' then namount else 0 end) as 'Saturday', sum(case when date_format(vrdate, '%W') = 'Sunday' then namount else 0 end) as 'Sunday' from ordermain where    etype = '$etype' and vrdate between DATE_SUB(VRDATE, INTERVAL 7 DAY) and CURDATE() AND ordermain.company_id=$company_id group by WEEK(VRDATE) order by WEEK(VRDATE) desc LIMIT 1 ";
        } else if ($period === 'monthly') {
            $query = "SELECT   sum(case when date_format(vrdate, '%b') = 'Jan' then namount else 0 end) as 'Jan', sum(case when date_format(vrdate, '%b') = 'Feb' then namount else 0 end) as 'Feb', sum(case when date_format(vrdate, '%b') = 'Mar' then namount else 0 end) as 'Mar', sum(case when date_format(vrdate, '%b') = 'Apr' then namount else 0 end) as 'Apr',sum(case when date_format(vrdate, '%b') = 'May' then namount else 0 end) as 'May', sum(case when date_format(vrdate, '%b') = 'Jun' then namount else 0 end) as 'Jun',sum(case when date_format(vrdate, '%b') = 'Jul' then namount else 0 end) as 'Jul', sum(case when date_format(vrdate, '%b') = 'Aug' then namount else 0 end) as 'Aug', sum(case when date_format(vrdate, '%b') = 'Sep' then namount else 0 end) as 'Sep', sum(case when date_format(vrdate, '%b') = 'Oct' then namount else 0 end) as 'Oct' , sum(case when date_format(vrdate, '%b') = 'Nov' then namount else 0 end) as 'Nov' , sum(case when date_format(vrdate, '%b') = 'Dec' then namount else 0 end) as 'Dec' from ordermain where    etype = 'purchase' and MONTH(VRDATE) = MONTH(CURDATE()) AND ordermain.company_id=$company_id group by month(VRDATE) order by month(VRDATE)";
        } else if ($period === 'yearly') {
            $query = "SELECT YEAR(vrdate) as 'Year', MONTHNAME(STR_TO_DATE(MONTH(VRDATE), '%m')) as Month, sum(namount) AS TotalAmount FROM ordermain where  etype = 'purchase' and YEAR(VRDATE) = YEAR(CURDATE()) and ordermain.company_id=$company_id GROUP BY YEAR(vrdate), MONTH(vrdate) ORDER BY YEAR(vrdate), MONTH(vrdate)";
        }

        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function fetchNetSum($company_id, $etype)
    {
        // if ($etype=='purchase' || $etype='salereturn'){
        // 	$query = "SELECT IFNULL(SUM(DEBIT),0) as 'PRETURNS_TOTAL' FROM pledger WHERE pledger.etype='$etype' AND pledger.company_id=$company_id";
        // }else{
        // 	$query = "SELECT IFNULL(SUM(CREDIT), 0) as 'PRETURNS_TOTAL' FROM pledger WHERE pledger.etype='$etype' AND pledger.company_id=$company_id";
        // }
        if ($etype == 'sale') {
            $query = "SELECT IFNULL(SUM(NAMOUNT),0) as 'PRETURNS_TOTAL' FROM ordermain WHERE ordermain.etype='$etype' AND ordermain.company_id=$company_id";
        } else {
            $query = "SELECT IFNULL(SUM(NAMOUNT),0) as 'PRETURNS_TOTAL' FROM ordermain WHERE ordermain.etype='$etype' AND ordermain.company_id=$company_id";
        }


        $result = $this->db->query($query);

        return $result->result_array();
    }

    public function getMaxVrnoa($etype, $company_id)
    {

        $result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM ordermain WHERE etype = '" . $etype . "' and company_id=" . $company_id . " ");
        $row = $result->row_array();
        $maxId = $row['vrnoa'];

        return $maxId;
    }

    public function save($ordermain, $orderdetail, $vrnoa, $etype)
    {

        $this->db->trans_begin();

        $company_id = $ordermain['company_id'];
        $godown_id = $orderdetail[0]['godown_id'];

        $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $ordermain['company_id']));
        $result = $this->db->get('ordermain');


        $oid = "";
        if ($result->num_rows() > 0) {
            $result = $result->row_array();
            $oid = $result['oid'];
            $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $ordermain['company_id']));
            $this->db->update('ordermain', $ordermain);

            $this->db->where(array('oid' => $oid));
            $this->db->delete('orderdetail');
        } else {
            $this->db->insert('ordermain', $ordermain);
            $oid = $this->db->insert_id();
        }

        foreach ($orderdetail as $detail) {
            $detail['oid'] = $oid;
            $this->db->insert('orderdetail', $detail);
        }


        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }

    }

    public function fetchOrderBalance($vrnoa, $etype, $company_id)
    {

        $etype2=($etype=='pur_order')?'purchase':'sale';

        $result = $this->db->query("SELECT  ifnull(lp.rate,0) as item_last_prate,ifnull(avg_rate.avg_rate,0) as item_avg_rate, ifnull(bl.balance,0)as party_balance,party.uname as party_uname,party.mobile as party_mobile,party.city as party_city,party.address as party_address,party.`limit` as party_limit,
            party.cityarea as party_cityarea,i.uname as item_uname,i.uom as item_uom, CAST(DATE_FORMAT(m.date_time,'%d/%m/%y %h:%i %p') AS CHAR) AS date_time,u.uname user_name,party.name AS party_name,m.date_time,m.cash_tender,m.is_hold, m.freight,d.prate,i.short_code,b.name AS brand_name,b.bid,m.customer_name,m.mobile,d.discount AS discount1,d.damount,d.prate,d.uom,d.etype AS detype,d.pcs,d.packing,d.type, m.party_id_co,m.currency_id, d.received_by AS 'received',d.workdetail,g.name AS dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date, m.ddate AS 'due_date',m.ordno, m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, 
            ifnull(sum(d.qty),0)-ifnull(pur.purqty,0) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', round(((ifnull(sum(d.qty),0)-ifnull(pur.purqty,0))*ifnull(d.rate,0)),0)-round(((ifnull(sum(d.qty),0)-ifnull(pur.purqty,0))*ifnull(d.rate,0)) * ifnull(d.discount,0)/100  ,0) AS 's_amount', round(((ifnull(sum(d.qty),0)-ifnull(pur.purqty,0))*ifnull(d.rate,0)) * ifnull(d.discount,0)/100  ,0) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom, d.weight, i.item_barcode
            FROM ordermain AS m
            INNER JOIN orderdetail AS d ON m.oid = d.oid
            INNER JOIN item AS i ON i.item_id = d.item_id
            LEFT JOIN brand b ON b.bid = i.bid
            INNER JOIN department AS g ON g.did = d.godown_id
            LEFT JOIN party ON party.pid=m.party_id
            LEFT join user u on u.uid=m.uid
            left join(
                select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid
                from pledger 
                where date<=(select vrdate from ordermain where vrnoa=$vrnoa and etype='$etype' and company_id=$company_id)
                group by pid
                ) as bl on bl.pid=m.party_id
left join(
    select m.order_vrno, d.item_id,abs(ifnull(sum(d.qty),0)) as purqty
    from stockmain m
    inner join stockdetail d on m.stid=d.stid
    where m.etype='$etype2'  and m.company_id=$company_id
    group by m.order_vrno,d.item_id
    ) as pur on pur.order_vrno= m.vrnoa and pur.item_id=i.item_id
left join(
    SELECT ifnull(SUM(d.netamount),0)/ ifnull(SUM(d.qty),0) AS avg_rate,d.item_id
    FROM stockdetail d
    INNER JOIN stockmain m ON m.stid = d.stid
    WHERE m.etype='PURCHASE'
    GROUP BY d.item_id
    HAVING IFNULL(SUM(d.qty),0)<>0
) as avg_rate on avg_rate.item_id=i.item_id
left join(
    SELECT t1.item_id, t1.rate,t1.stid
    FROM stockdetail t1
    JOIN (
        SELECT item_id, MAX(stdid) stdid
        FROM stockdetail
        WHERE STID IN (
            SELECT STID
            FROM StockMain
            WHERE ETYPE='purchase')
GROUP BY item_id) t2 ON t1.item_id = t2.item_id AND t1.stdid = t2.stdid
GROUP BY t1.item_id,t1.rate,t1.stid
) as lp on lp.item_id=i.item_id
WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' 
group by m.vrnoa,i.item_id,pur.purqty,d.rate
");
if ($result->num_rows() > 0) {
    return $result->result_array();
} else {
    return false;
}
}


public function fetchPurOrderBalance($endDate, $company_id,$crit,$etype)
{

    $etype2=($etype=='pur_order')?'purchase':'sale';

    $qry="SELECT m.vrnoa as orderno, i.item_id,i.short_code,i.item_des,i.uom,i.model,ifnull(sum(d.qty),0) as orderqty,ifnull(pur.purqty,0) as purqty ,ifnull(sum(d.qty),0)-ifnull(pur.purqty,0) as balqty,ifnull(d.rate,0) as rate,
    ifnull(d.discount,0) as discount,
    round(((ifnull(sum(d.qty),0)-ifnull(pur.purqty,0))*ifnull(d.rate,0)),0)-round(((ifnull(sum(d.qty),0)-ifnull(pur.purqty,0))*ifnull(d.rate,0)) * ifnull(d.discount,0)/100  ,0) as amount
    from ordermain m
    inner join orderdetail d on m.oid=d.oid
    inner join item i on i.item_id=d.item_id
    left join(
        select m.order_vrno, d.item_id,abs(ifnull(sum(d.qty),0)) as purqty
        from stockmain m
        inner join stockdetail d on m.stid=d.stid
        where m.etype='$etype2'  and m.company_id=$company_id
        group by m.order_vrno,d.item_id
        ) as pur on pur.order_vrno=m.vrnoa and pur.item_id=i.item_id
where m.etype='$etype' and m.vrdate<='$endDate' and m.company_id=$company_id
group by m.vrnoa,i.item_id,pur.purqty,d.rate
having ifnull(sum(d.qty),0)-ifnull(pur.purqty,0)>0";

        // var_dump($qry);

$query = $this->db->query($qry);

return $query->result_array();



}

public function fetch($vrnoa, $etype, $company_id)
{

    $result = $this->db->query("SELECT m.salebillno,m.bilty_no,ifnull(bl.balance,0)as party_balance,party.uname as party_uname,party.mobile as party_mobile,party.city as party_city,party.address as party_address,party.`limit` as party_limit,
        party.cityarea as party_cityarea,i.uname as item_uname,i.uom as item_uom, CAST(DATE_FORMAT(m.date_time,'%d/%m/%y %h:%i %p') AS CHAR) AS date_time,u.uname user_name,party.name AS party_name,m.date_time,m.cash_tender,m.is_hold, m.freight,d.prate,i.short_code,b.name AS brand_name,b.bid,m.customer_name,m.mobile,d.discount AS discount1,d.damount,d.prate,d.uom,d.etype AS detype,d.pcs,d.packing,d.type, m.party_id_co,m.currency_id, d.received_by AS 'received',d.workdetail,g.name AS dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date, m.ddate AS 'due_date',m.ordno, m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom, d.weight, i.item_barcode
        FROM ordermain AS m
        INNER JOIN orderdetail AS d ON m.oid = d.oid
        INNER JOIN item AS i ON i.item_id = d.item_id
        LEFT JOIN brand b ON b.bid = i.bid
        INNER JOIN department AS g ON g.did = d.godown_id
        LEFT JOIN party ON party.pid=m.party_id
        LEFT join user u on u.uid=m.uid
        left join(
            select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid
            from pledger 
            where date<=(select vrdate from ordermain where vrnoa=$vrnoa and etype='$etype' and company_id=$company_id)
            group by pid
            ) as bl on bl.pid=m.party_id
WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' ");
if ($result->num_rows() > 0) {
    return $result->result_array();
} else {
    return false;
}
}

public function fetchitemc($vrnoa, $etype, $company_id)
{

    $result = $this->db->query("SELECT m.customer_name,m.mobile,d.discount as discount1,d.damount ,d.uom,d.etype as detype,d.pcs,d.packing ,d.type, m.party_id_co,m.currency_id, d.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom , d.weight  FROM ordermain AS m INNER JOIN orderdetail AS d ON m.oid = d.oid INNER JOIN item AS i ON i.item_id = d.item_id  LEFT JOIN department AS g ON g.did = d.godown_id WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' ");
    if ($result->num_rows() > 0) {
        return $result->result_array();
    } else {
        return false;
    }
}

public function fetchVoucher($vrnoa, $etype, $company_id)
{
    $query = $this->db->query("SELECT  ifnull(p.email,'') as party_email, m.freight,d.netamount AS d_netamount, d.carton,d.pcs,d.packing,d.type,p.name AS party_name,m.vrno,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no',d.damount AS item_dis, m.bilty_date AS 'due_date', m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, d.qty, ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom, d.weight
        FROM ordermain AS m
        INNER JOIN orderdetail AS d ON m.oid = d.oid
        INNER JOIN item AS i ON i.item_id = d.item_id
        INNER JOIN party AS p ON p.pid=m.party_id
        WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' ");
return $query->result_array();
}




public function fetchNavigation($vrnoa, $etype, $company_id)
{

    $result = $this->db->query("SELECT m.uid,m.vrno, m.vrnoa, DATE(m.vrdate) vrdate, m.received_by, m.remarks, m.etype, d.item_id, d.godown_id, d.godown_id2, ROUND(d.qty, 2) qty, ROUND(d.weight, 2) weight, d.uom, dep.name AS 'dept_to', i.item_des AS 'item_name', dep2.name AS 'dept_from' FROM ordermain m INNER JOIN orderdetail d ON m.oid = d.oid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN department dep2 ON dep2.did = d.godown_id2 WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' AND m.company_id = " . $company_id . " AND d.qty > 0");
    if ($result->num_rows() > 0) {
        return $result->result_array();
    } else {
        return false;
    }
}

public function fetchByCol($col)
{

    $result = $this->db->query("SELECT DISTINCT $col FROM ordermain");
    return $result->result_array();
}

public function fetchByColFromStkDetail($col)
{

    $result = $this->db->query("SELECT DISTINCT $col FROM orderdetail");
    return $result->result_array();
}

public function delete($vrnoa, $etype, $company_id)
{

    $this->db->where(array('etype' => $etype, 'dcno' => $vrnoa, 'company_id' => $company_id));
    $result = $this->db->delete('pledger');

    $this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
    $result = $this->db->get('ordermain');

    if ($result->num_rows() == 0) {
        return false;
    } else {

        $result = $result->row_array();
        $oid = $result['oid'];

        $this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
        $this->db->delete('ordermain');
        $this->db->where(array('oid' => $oid));
        $this->db->delete('orderdetail');

        return true;
    }
}


public function fetchPurchaseReturnReportData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.REMARKS, ordermain.VRNOA, ordermain.REMARKS, party.NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.NETAMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id ORDER BY ordermain.VRNOA");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT party.NAME, date(ordermain.VRDATE) VRDATE, ordermain.VRNOA, round(SUM(orderdetail.QTY)) QTY, round(SUM(orderdetail.RATE)) RATE, round(sum(orderdetail.NETAMOUNT)) NETAMOUNT, ordermain.REMARKS FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id Group by ordermain.VRNOA ORDER BY ordermain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'account') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.REMARKS, ordermain.VRNOA, ordermain.REMARKS, party.NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.NETAMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id ORDER BY ordermain.party_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT party.NAME, date(ordermain.VRDATE) VRDATE, ordermain.VRNOA, round(SUM(orderdetail.QTY)) QTY, round(SUM(orderdetail.RATE)) RATE, round(sum(orderdetail.NETAMOUNT)) NETAMOUNT, ordermain.REMARKS FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id Group by ordermain.party_id ORDER BY ordermain.party_id");
            return $query->result_array();
        }
    } else if ($what == 'godown') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.REMARKS, ordermain.VRNOA, ordermain.REMARKS, dept.name AS NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.NETAMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON orderdetail.godown_id = dept.did WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id ORDER BY orderdetail.godown_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT dept.NAME, ROUND(SUM(orderdetail.QTY)) QTY, ROUND(SUM(orderdetail.RATE)) RATE, ROUND(SUM(orderdetail.NETAMOUNT)) NETAMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = orderdetail.godown_id WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id GROUP BY orderdetail.godown_id ORDER BY orderdetail.godown_id");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.REMARKS, ordermain.VRNOA, ordermain.REMARKS, party.NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.NETAMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY orderdetail.item_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT item.item_des as NAME, ROUND(SUM(orderdetail.QTY)) QTY, ROUND(SUM(orderdetail.RATE)) RATE, ROUND(SUM(orderdetail.NETAMOUNT)) NETAMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY orderdetail.item_id ORDER BY orderdetail.item_id");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.REMARKS, ordermain.VRNOA, ordermain.REMARKS, party.NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.NETAMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.vrdate");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT date(ordermain.vrdate) as DATE, ROUND(SUM(orderdetail.QTY)) QTY, ROUND(SUM(orderdetail.RATE)) RATE, ROUND(SUM(orderdetail.NETAMOUNT)) NETAMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN party party ON ordermain.PARTY_ID = party.pid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='purchase return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.vrdate ORDER BY ordermain.vrdate");
            return $query->result_array();
        }
    }
}

public function fetchStockNavigationData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.VRNOA, orderdetail.UOM, orderdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON orderdetail.godown_id = frm.did INNER JOIN department _to ON orderdetail.godown_id2 = _to.did WHERE ordermain.ETYPE='navigation' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND orderdetail.QTY > 0 ORDER BY ordermain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'location') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.VRNOA, orderdetail.UOM, orderdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON orderdetail.godown_id = frm.did INNER JOIN department _to ON orderdetail.godown_id2 = _to.did WHERE ordermain.ETYPE='navigation' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND orderdetail.QTY > 0 ORDER BY frm.name");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.VRNOA, orderdetail.UOM, orderdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON orderdetail.godown_id = frm.did INNER JOIN department _to ON orderdetail.godown_id2 = _to.did WHERE ordermain.ETYPE='navigation' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND orderdetail.QTY > 0 ORDER BY item.item_des");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRDATE, ordermain.VRNOA, orderdetail.UOM, orderdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON orderdetail.godown_id = frm.did INNER JOIN department _to ON orderdetail.godown_id2 = _to.did WHERE ordermain.ETYPE='navigation' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND orderdetail.QTY > 0 ORDER BY ordermain.VRDATE");
            return $query->result_array();
        }
    }
}

public function fetchMaterialReturnData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.VRNOA");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, sum(orderdetail.QTY) QTY, sum(orderdetail.RATE) RATE, sum(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.VRNOA ORDER BY ordermain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'person') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.received_by");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT ordermain.received_by NAME, SUM(orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.received_by ORDER BY ordermain.received_by");
            return $query->result_array();
        }
    } else if ($what == 'location') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, dept.NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON orderdetail.godown_id = dept.did WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY orderdetail.godown_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT dept.NAME, SUM(orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = orderdetail.godown_id WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY orderdetail.godown_id ORDER BY orderdetail.godown_id");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY orderdetail.item_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT item.item_des NAME, (orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY orderdetail.item_id ORDER BY orderdetail.item_id");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.vrdate desc");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT ordermain.VRDATE NAME, (orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption_return' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.VRDATE ORDER BY ordermain.VRDATE");
            return $query->result_array();
        }
    }
}


public function fetchConsumptionData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.VRNOA");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, sum(orderdetail.QTY) QTY, sum(orderdetail.RATE) RATE, sum(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.VRNOA ORDER BY ordermain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'person') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.received_by");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT ordermain.received_by NAME, SUM(orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.received_by ORDER BY ordermain.received_by");
            return $query->result_array();
        }
    } else if ($what == 'location') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, dept.NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON orderdetail.godown_id = dept.did WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY orderdetail.godown_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT dept.NAME, SUM(orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = orderdetail.godown_id WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY orderdetail.godown_id ORDER BY orderdetail.godown_id");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY orderdetail.item_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT item.item_des NAME, (orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY orderdetail.item_id ORDER BY orderdetail.item_id");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT ordermain.VRNOA, ordermain.VRDATE, ordermain.received_by NAME, orderdetail.QTY, orderdetail.RATE, orderdetail.AMOUNT, item.item_des FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY ordermain.vrdate desc");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT ordermain.VRDATE NAME, (orderdetail.QTY) QTY, SUM(orderdetail.RATE) RATE, SUM(orderdetail.AMOUNT) AMOUNT FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetain.oid INNER JOIN item item ON orderdetail.ITEM_ID = item.ITEM_ID WHERE ordermain.ETYPE='consumption' AND ordermain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY ordermain.VRDATE ORDER BY ordermain.VRDATE");
            return $query->result_array();
        }
    }
}

public function fetchPRRangeSum($from, $to)
{
    $query = "SELECT IFNULL(SUM(CREDIT), 0)-IFNULL(SUM(DEBIT),0) as 'PRETURNS_TOTAL' FROM pledger pledger WHERE pid IN (SELECT pid FROM party party WHERE NAME='purchase return') AND date between '{$from}' AND '{$to}'";
    $result = $this->db->query($query);

    return $result->result_array();
}

public function fetchImportRangeSum($from, $to)
{
    $query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PIMPORTS_TOTAL' FROM pledger pledger WHERE pid IN ( SELECT pid FROM party party WHERE NAME='purchase import') AND date BETWEEN '{$from}' AND '{$to}'";
    $result = $this->db->query($query);

    return $result->result_array();
}

public function fetchRangeSum($from, $to)
{
    $query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PURCHASES_TOTAL' FROM pledger pledger WHERE pid IN ( SELECT pid FROM party party WHERE NAME='purchase') AND date BETWEEN '{$from}' AND '{$to}'";
    $result = $this->db->query($query);

    return $result->result_array();
}

public function backupHoldSaleVr($vrnoa, $etype, $companyId)
{
    $result = $this->db->query("select * from ordermain
     where vrnoa = '" . $vrnoa . "' and
     etype = '" . $etype . "' and company_id = '" . $companyId . "' ");
    $mainArray = $result->result_array();
    if (count($mainArray) > 0) {
        $mainData = array(
            "vrno" => $mainArray[0]["vrno"],
            "vrnoa" => $mainArray[0]["vrnoa"],
            "vrdate" => $mainArray[0]["vrdate"],
            "party_id" => $mainArray[0]["party_id"],
            "bilty_no" => $mainArray[0]["bilty_no"],
            "bilty_date" => $mainArray[0]["bilty_date"],
            "received_by" => $mainArray[0]["received_by"],
            "transporter_id" => $mainArray[0]["transporter_id"],
            "remarks" => $mainArray[0]["remarks"],
            "year_srno" => $mainArray[0]["year_srno"],
            "etype" => $mainArray[0]["etype"],
            "namount" => $mainArray[0]["namount"],
            "uid" => $mainArray[0]["uid"],
            "order_vrno" => $mainArray[0]["order_vrno"],
            "freight" => $mainArray[0]["freight"],
            "party_id_co" => $mainArray[0]["party_id_co"],
            "salebillno" => $mainArray[0]["salebillno"],
            "discp" => $mainArray[0]["discp"],
            "discount" => $mainArray[0]["discount"],
            "currency_id" => $mainArray[0]["currency_id"],
            "expense" => $mainArray[0]["expense"],
            "company_id" => $mainArray[0]["company_id"],
            "vehicle_id" => $mainArray[0]["vehicle_id"],
            "officer_id" => $mainArray[0]["officer_id"],
            "etype2" => $mainArray[0]["etype2"],
            "ddays" => $mainArray[0]["ddays"],
            "ddate" => $mainArray[0]["ddate"],
            "date_time" => $mainArray[0]["date_time"],
            "exppercent" => $mainArray[0]["exppercent"],
            "tax" => $mainArray[0]["tax"],
            "taxpercent" => $mainArray[0]["taxpercent"],
            "paid" => $mainArray[0]["paid"],
            "ordno" => $mainArray[0]["ordno"],
            "customer_name" => $mainArray[0]["customer_name"],
            "mobile" => $mainArray[0]["mobile"],
            "item_discount" => $mainArray[0]["item_discount"],
            "status" => "",
            "is_hold" => $mainArray[0]["is_hold"]
            );
$this->db->insert('ordermain', $mainData);

$resultDetail = $this->db->query("select * from orderdetail
    where oid = '" . $mainArray[0]["oid"] . "' ");
$detailArray = $resultDetail->result_array();
if (count($detailArray) > 0) {
    $detailData = array();
    foreach ($detailArray as $row) {
        $detailData = array(
            "oid" => $row["oid"],
            "item_id" => $row["item_id"],
            "godown_id" => $row["godown_id"],
            "uom" => $row["uom"],
            "qty" => $row["qty"],
            "qtyf" => $row["qtyf"],
            "rate" => $row["rate"],
            "amount" => $row["amount"],
            "discount" => $row["discount"],
            "damount" => $row["damount"],
            "netamount" => $row["netamount"],
            "bundle" => $row["bundle"],
            "weight" => $row["weight"],
            "godown_id2" => $row["godown_id2"],
            "type" => $row["type"],
            "prate" => $row["prate"],
            "job_id" => $row["job_id"],
            "workdetail" => $row["workdetail"],
            "received_by" => $row["received_by"],
            "carton" => $row["carton"],
            "pcs" => $row["pcs"],
            "packing" => $row["packing"]
            );
    }
    $this->db->insert('orderdetail', $detailData);
}
}
}
}

/* End of file purchases.php */
/* Location: ./application/models/purchases.php */