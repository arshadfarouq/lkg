<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Purchases extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMaxVrno($etype, $company_id)
    {

        $result = $this->db->query("SELECT MAX(vrno) vrno FROM stockmain WHERE etype = '" . $etype . "' AND company_id=" . $company_id . "  AND DATE(vrdate) = DATE(NOW())");
        $row = $result->row_array();
        $maxId = $row['vrno'];

        return $maxId;
    }

    public function fetchAllPurchases( $company_id,$etype ) {

        $result = $this->db->query("SELECT round(stockmain.discount,0) as discount,round(stockmain.expense,0) as expense,round(stockmain.tax,0) as tax,stockmain.vrnoa, DATE_FORMAT(stockmain.vrdate,'%d %b %y') AS DATE,p.name AS party_name,stockmain.remarks,round(stockmain.taxpercent,1) as taxpercent,round(stockmain.exppercent,1) as exppercent,round(stockmain.discp,1)as discp,stockmain.paid,round(stockmain.namount,0) as namount,user.uname as user_name,TIME(stockmain.date_time) as date_time,stockmain.vrnoa
            FROM stockmain stockmain
            INNER JOIN party p ON p.pid = stockmain.party_id
            INNER JOIN user ON user.uid = stockmain.uid
            INNER JOIN company c ON c.company_id = stockmain.company_id
            WHERE stockmain.company_id= '".$company_id."' AND stockmain.etype= '".$etype."' AND stockmain.vrdate = CURDATE()
            ORDER BY stockmain.vrnoa DESC
            LIMIT 10");
        // if ( $result->num_rows() > 0 ) {
        return $result->result_array();
        // } else {
        //  return false;
        // }
    }
    

    public function last_5_srate($crit, $company_id, $etype, $date)
    {
        $result = $this->db->query("SELECT  m.vrnoa, date_format(m.vrdate,'%d/%m/%y') vrdate,m.etype, ROUND(d.qty, 2) qty,d.rate
          FROM stockmain AS m
          INNER JOIN stockdetail AS d ON m.stid = d.stid
          INNER JOIN item i ON i.item_id = d.item_id
          LEFT JOIN department dep ON dep.did = d.godown_id
          LEFT JOIN party party ON party.pid=m.party_id
          WHERE   m.etype = '" . $etype . "' and m.company_id = $company_id  and m.vrdate <= '" . $date . "' $crit
          ORDER BY m.vrdate  desc
          limit 5");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function lastPRate($companyId, $itemId, $date)
    {
        $result = $this->db->query("SELECT ifnull(stockdetail.rate,0) - ifnull(stockdetail.damount,0) as prate from stockmain inner join stockdetail on stockmain.stid = stockdetail.stid
            where stockmain.etype = 'purchase' and stockdetail.item_id = ".$itemId." and stockmain.vrdate <='".$date."' and stockmain.company_id = ".$companyId."
            ORDER BY stockmain.vrdate  desc");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    public function lastRateEtype($company_id, $item_id, $date,$party_id,$etype)
    {
        $result = $this->db->query("SELECT  rate ,discount
            FROM stockdetail
            WHERE item_id=$item_id AND stid IN (
                SELECT stid
                FROM stockmain
                WHERE etype='$etype' and party_id=$party_id and vrdate<='$date' and company_id=$company_id)
        ORDER BY stid DESC
        limit 1;");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchlastprate($vrnoa, $vrdate, $item_id, $company_id, $etype)
    {
        $result = $this->db->query("SELECT m.vrnoa,m.vrdate,m.etype,d.rate,d.prate,i.item_des from stockmain m
         INNER JOIN stockdetail d on d.stid = m.stid
         INNER JOIN item i on i.item_id = d.item_id
         where m.vrnoa <= '" . $vrdate . "' and m.etype =  '" . $etype . "' and d.item_id = $item_id and m.company_id = $company_id
         order by m.vrnoa desc limit 1");
// 				$result = $this->db->query("SELECT m.vrnoa,m.vrdate,m.etype,d.rate,i.item_des from stockmain m
// INNER JOIN stockdetail d on d.stid = m.stid
// INNER JOIN item i on i.item_id = d.item_id
// where m.vrdate <= '2015/05/11' and m.etype = 'purchase' and d.item_id = 13 
// order by m.vrdate desc limit 1");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function sendMessage($mobile, $message)

    {

        $ptn = "/^[0-9]/";  // Regex

        $rpltxt = "92";  // Replacement string

        $mobile = preg_replace($ptn, $rpltxt, $mobile);


        // Create the SoapClient instance

        $url = ZONG_API_SERVICE_URL;

        // $soapClient = new SoapClient( $url);

        // var_dump($soapClient->__getFunctions());


        $post_string = '<?xml version="1.0" encoding="utf-8"?>' .

        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .

        '<soap:Body>' .

        '<SendSingleSMS xmlns="http://tempuri.org/">' .

        '<Src_nbr>' . ZONG_API_MOB . '</Src_nbr>' .

        '<Password>' . ZONG_API_PASS . '</Password>' .

        '<Dst_nbr>' . $mobile . '</Dst_nbr>' .

        '<Mask>' . ZONG_API_MASK . '</Mask>' .

        '<Message>' . $message . '</Message>' .

        '</SendSingleSMS>' .

        '</soap:Body>' .

        '</soap:Envelope>';


        $soap_do = curl_init();

        curl_setopt($soap_do, CURLOPT_URL, $url);

        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);

        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($soap_do, CURLOPT_POST, true);

        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);

        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($post_string)));


        $result = curl_exec($soap_do);

        $err = curl_error($soap_do);


        return $result;

    }

    public function last_stockLocatons( $item_id,$company_id,$etype ) {

        $result = $this->db->query("SELECT g.name as location, ifnull(sum(d.qty),0) as qty from stockdetail d  inner join department g on g.did=d.godown_id where d.item_id= $item_id group by g.name");

        if ( $result->num_rows() > 0 ) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($etype == 'profit') {
            if ($type == 'detailed') {
                $query = $this->db->query("SELECT $field AS voucher,$name,stockmain.etype, -IFNULL((((stockdetail.rate-stockdetail.prate)*stockdetail.qty)-stockdetail.damount),0) AS pls , -IFNULL((((stockdetail.rate-stockdetail.last_prate)*stockdetail.qty)-stockdetail.damount),0) AS last_prate_pls,ifnull(stockdetail.prate,0) as prate,ifnull(stockdetail.last_prate,0) as last_prate,stockdetail.discount AS discountP,stockdetail.damount discountA,stockdetail.netamount AS d_netamount,stockdetail.carton,stockdetail.pcs, DAYNAME(vrdate) AS weekdate, MONTH(vrdate) AS monthdate, YEAR(vrdate) AS yeardate,user.uname AS username, DATE_FORMAT(stockmain.vrdate,'%d/%m/%y') AS vrdate, stockmain.remarks, stockmain.vrnoa, stockmain.remarks, stockdetail.qty, stockdetail.weight, stockdetail.rate, stockdetail.amount, stockdetail.netamount, item.item_des AS 'item_des',item.uom
                    FROM stockmain stockmain
                    INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid
                    INNER JOIN party party ON stockmain.party_id = party.pid
                    INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3
                    INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2
                    INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1
                    INNER JOIN item item ON stockdetail.item_id = item.item_id
                    INNER JOIN user ON user.uid = stockmain.uid
                    INNER JOIN department dept ON stockdetail.godown_id = dept.did
                    left join area_officer on area_officer.officer_id=stockmain.officer_id
                    left join category on category.catid= item.catid
                    left join subcategory on subcategory.subcatid=item.subcatid
                    left join brand on brand.bid = item.bid
                    left join made on made.made_id = item.made_id
                    WHERE stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype IN('sale','salereturn') $crit
                    ORDER BY $orderBy");
return $query->result_array();
} else {
    $query = $this->db->query("SELECT $field AS voucher, ifnull(stockdetail.prate,0) * ifnull(sum(stockdetail.qty),0) as avgcost
        ,ifnull(sum(stockdetail.netamount),0) d_netamount
        ,ifnull(sum(stockdetail.damount),0) d_damount
        ,ifnull(sum(stockdetail.netamount),0) + (ifnull(stockdetail.prate,0) * ifnull(sum(stockdetail.qty),0)) as pls
        ,ifnull(sum(stockdetail.netamount),0) + (ifnull(stockdetail.last_prate,0) * ifnull(sum(stockdetail.qty),0)) as last_prate_pls
        , -ifnull(sum(stockdetail.qty),0) as qty,stockdetail.prate,stockdetail.last_prate
        FROM stockmain stockmain
        INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid
        INNER JOIN party party ON stockmain.party_id = party.pid
        INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3
        INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2
        INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1
        INNER JOIN item item ON stockdetail.item_id = item.item_id
        INNER JOIN user ON user.uid = stockmain.uid
        INNER JOIN department dept ON stockdetail.godown_id = dept.did
        left join area_officer on area_officer.officer_id=stockmain.officer_id
        left join category on category.catid= item.catid
        left join subcategory on subcategory.subcatid=item.subcatid
        left join brand on brand.bid = item.bid
        left join made on made.made_id = item.made_id
        WHERE stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype IN('sale','salereturn') $crit
        group BY $groupBy
        ORDER BY $orderBy");
return $query->result_array();
}
} else {
    if ($type == 'detailed') {
        if($etype=='pur_order' || $etype=='sale_order'){
            $query = $this->db->query("SELECT $field AS voucher,$name,ordermain.etype,orderdetail.discount AS discountP,orderdetail.damount discountA,orderdetail.netamount AS d_netamount,orderdetail.carton,orderdetail.pcs, DAYNAME(vrdate) AS weekdate, MONTH(vrdate) AS monthdate, YEAR(vrdate) AS yeardate,user.uname AS username,date_format(ordermain.vrdate,'%d/%m/%y') as vrdate, ordermain.remarks, ordermain.vrnoa, ordermain.remarks, orderdetail.qty, orderdetail.weight, orderdetail.rate, orderdetail.amount, orderdetail.netamount, item.item_des AS 'item_des',item.uom
                FROM ordermain ordermain
                INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid
                INNER JOIN party party ON ordermain.party_id = party.pid
                INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3
                INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2
                INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1
                INNER JOIN item item ON orderdetail.item_id = item.item_id
                INNER JOIN user ON user.uid = ordermain.uid
                INNER JOIN department dept ON orderdetail.godown_id = dept.did
                left join area_officer on area_officer.officer_id=ordermain.officer_id
                left join category on category.catid= item.catid
                left join subcategory on subcategory.subcatid=item.subcatid
                left join brand on brand.bid = item.bid
                left join made on made.made_id = item.made_id

                WHERE ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit

                ORDER BY $orderBy");
}else{  
    $query = $this->db->query("SELECT $field AS voucher,$name,stockmain.etype,stockdetail.discount AS discountP,stockdetail.damount discountA,stockdetail.netamount AS d_netamount,stockdetail.carton,stockdetail.pcs, DAYNAME(vrdate) AS weekdate, MONTH(vrdate) AS monthdate, YEAR(vrdate) AS yeardate,user.uname AS username,date_format(stockmain.vrdate,'%d/%m/%y') as vrdate, stockmain.remarks, stockmain.vrnoa, stockmain.remarks, stockdetail.qty, stockdetail.weight, stockdetail.rate, stockdetail.amount, stockdetail.netamount, item.item_des AS 'item_des',item.uom
        FROM stockmain stockmain
        INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid
        INNER JOIN party party ON stockmain.party_id = party.pid
        INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3
        INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2
        INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1
        INNER JOIN item item ON stockdetail.item_id = item.item_id
        INNER JOIN user ON user.uid = stockmain.uid
        INNER JOIN department dept ON stockdetail.godown_id = dept.did
        left join area_officer on area_officer.officer_id=stockmain.officer_id
        left join category on category.catid= item.catid
        left join subcategory on subcategory.subcatid=item.subcatid
        left join brand on brand.bid = item.bid
        left join made on made.made_id = item.made_id

        WHERE stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit
        
        ORDER BY $orderBy");
}
return $query->result_array();
} else {
    $query = $this->db->query("SELECT $field AS voucher,$name,stockmain.etype, ROUND(SUM(stockdetail.discount)) discountP, ROUND(SUM(stockdetail.damount)) discountA, ROUND(SUM(stockdetail.netamount)) d_netamount,stockdetail.carton,stockdetail.pcs, DATE(stockmain.vrdate) AS DATE, DAYNAME(vrdate) AS weekdate, MONTH(vrdate) AS monthdate, YEAR(vrdate) AS yeardate,user.uname AS username, DATE(stockmain.VRDATE) VRDATE, stockmain.vrnoa, ROUND(SUM(stockdetail.qty)) qty, ROUND(SUM(stockdetail.weight)) weight, ROUND(SUM(stockdetail.rate)) rate, ROUND(SUM(stockdetail.amount)) amount, ROUND(SUM(stockdetail.netamount)) netamount, stockmain.remarks
        FROM stockmain stockmain
        INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid
        INNER JOIN party party ON stockmain.party_id = party.pid
        INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3
        INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2
        INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1
        INNER JOIN item item ON stockdetail.item_id = item.item_id
        INNER JOIN user ON user.uid = stockmain.uid
        INNER JOIN department dept ON stockdetail.godown_id = dept.did
         left join area_officer on area_officer.officer_id=stockmain.officer_id
        left join category on category.catid= item.catid
        left join subcategory on subcategory.subcatid=item.subcatid
        left join brand on brand.bid = item.bid
        left join made on made.made_id = item.made_id
        
        WHERE stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit
        GROUP BY $groupBy
        ORDER BY $orderBy");
return $query->result_array();
}
}

}

public function fetchitemcReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
{
    if ($type == 'detailed') {
        $query = $this->db->query("SELECT $field as voucher,$name,item.uom,dept.name as dept_name,stockdetail.discount as discountP,stockdetail.damount discountA,stockdetail.netamount as d_netamount,stockdetail.carton,stockdetail.pcs, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,date_format(stockmain.vrdate,'%d/%m/%y') as vrdate, stockmain.remarks, stockmain.vrnoa, stockmain.remarks,  stockdetail.qty, stockdetail.weight, stockdetail.rate,stockdetail.etype as detype, stockdetail.amount, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid LEFT JOIN party party ON stockmain.party_id = party.pid              LEFT JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  LEFT JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 LEFT JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid  LEFT JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit  order by $orderBy");
        return $query->result_array();
    } else {
        $query = $this->db->query("SELECT $field as voucher,$name,round(SUM(stockdetail.discount)) discountP,round(SUM(stockdetail.damount)) discountA,round(SUM(stockdetail.netamount)) d_netamount ,stockdetail.carton,stockdetail.pcs,date(stockmain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(stockmain.VRDATE) VRDATE, stockmain.vrnoa, round(SUM(stockdetail.qty)) qty, round(SUM(stockdetail.weight)) weight, round(SUM(stockdetail.rate)) rate, round(SUM(stockdetail.amount)) amount, round(sum(stockdetail.netamount)) netamount, stockmain.remarks FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit group by $groupBy order by $orderBy");
        return $query->result_array();
    }

}

public function fetchSaleReportDataSummary($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
{
    if ($type !== 'detailed') {
        $query = $this->db->query("SELECT $field as voucher,$name,stockmain.etype,  WEEK(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate, 
            stockmain.vrnoa,stockmain.remarks,date_format(stockmain.vrdate,'%d/%m/%y') as vrdate,party.name as party_name,
            stockmain.tax,stockmain.discount,stockmain.expense,stockmain.paid,stockmain.namount,stockmain.customer_name,transporter.name
            FROM stockmain stockmain 
            INNER JOIN party party ON stockmain.party_id = party.pid 
            LEFT JOIN transporter ON transporter.transporter_id = stockmain.transporter_id
            left join area_officer on area_officer.officer_id=stockmain.officer_id
            left join user on user.uid=stockmain.uid
            WHERE  stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit  
            group by $groupBy
            order by $orderBy");
        return $query->result_array();
    } else {
        $qry="SELECT $field AS voucher,$name,stockmain.etype, WEEK(vrdate) AS weekdate, MONTH(vrdate) AS monthdate, YEAR(vrdate) AS yeardate, 
        stockmain.vrnoa,stockmain.remarks, DATE_FORMAT(stockmain.vrdate,'%d/%m/%y') AS vrdate,party.name AS party_name,
        stockmain.tax,stockmain.discount,stockmain.expense,stockmain.paid,stockmain.namount,stockmain.customer_name,transporter.name
        FROM stockmain stockmain
        INNER JOIN party party ON stockmain.party_id = party.pid
        LEFT JOIN transporter ON transporter.transporter_id = stockmain.transporter_id
        LEFT JOIN area_officer ON area_officer.officer_id=stockmain.officer_id
        LEFT JOIN user ON user.uid=stockmain.uid
        WHERE stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit
        
        ORDER BY $orderBy";
        
        $query = $this->db->query($qry);

        return $query->result_array();
    }

}
public function fetchSaleReportGrahiSheet($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
{

    $qry="SELECT 
    ifnull($field,'-') as voucher,
    party.pid,
    party.uname AS party_uname,
    party.name AS partyname,
    party.address AS partyaddress,
    party.mobile,
    party.city,
    party.cityarea,
    ifnull(SM.totalamount,0) AS totalamount,
    ifnull(SM.totalreceived,0) AS totalreceived,
    ifnull(pb.pb,0) AS previousbalance,
    ifnull(dp.debit,0) AS totaldebit,
    ifnull(dp.credit,0) AS totalcredit

    FROM party 
    left JOIN(
     SELECT   ifnull(SUM(namount),0) AS totalamount,
     ifnull(SUM(stockmain.paid),0) AS totalreceived    ,
     party_id
     FROM stockmain
     WHERE stockmain.etype='$etype' AND stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "'  
     GROUP BY party_id
 ) SM  ON party.pid = SM.party_id

LEFT JOIN(
    SELECT ROUND(ifnull(SUM(debit),0)-ifnull(SUM(credit),0),0) AS pb,pid
    FROM pledger 
    WHERE date < '" . $startDate . "'
    GROUP BY pid
) AS pb on pb.pid=party.pid
LEFT JOIN(
    SELECT ifnull(SUM(debit),0) AS debit,ifnull(SUM(credit),0) AS credit,pid
    FROM pledger 
    WHERE date BETWEEN '" . $startDate . "'  AND '" . $endDate . "' AND etype <> '$etype'
    GROUP BY pid
) AS dp on dp.pid=party.pid
WHERE party.level3=$type
order by $orderBy";
// echo $qry;
$query = $this->db->query($qry);
return $query->result_array();


}

public function fetchReportDataMain($startDate, $endDate, $company_id, $etype, $uid, $isHold = "")
{
    $where="";
    $where = ($uid !== 1) ? " and stockmain.uid =$uid " : "";

    $query = $this->db->query("SELECT stockmain.discount,stockmain.expense,stockmain.tax,stockmain.vrnoa,
     date_format(stockmain.vrdate,'%d/%m/%y') as vrdate,p.name AS party_name,stockmain.remarks,stockmain.taxpercent,
     stockmain.exppercent,stockmain.discp,stockmain.paid,stockmain.namount
     FROM stockmain stockmain
     INNER JOIN party p ON p.pid = stockmain.party_id
     INNER JOIN user ON user.uid = stockmain.uid
     INNER JOIN company c ON c.company_id = stockmain.company_id
     WHERE stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id= '" . $company_id . "' AND stockmain.etype= '" . $etype . "' " . $where . "
     ORDER BY stockmain.vrnoa");
    return $query->result_array();


}

public function fetchReportDataOrderMainHoldReport($startDate, $endDate, $company_id, $etype, $uid)
{
    $query = $this->db->query("SELECT ordermain.discount,ordermain.expense,ordermain.tax,ordermain.vrnoa,
     ordermain.vrdate,p.name as party_name,ordermain.remarks,ordermain.taxpercent,
     ordermain.exppercent,ordermain.discp,ordermain.paid,ordermain.namount
     from ordermain ordermain
     INNER JOIN party p ON p.pid = ordermain.party_id
     INNER JOIN user user ON user.uid = ordermain.uid
     INNER JOIN company c ON c.company_id = ordermain.company_id
     WHERE ordermain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "'
     AND ordermain.company_id= '" . $company_id . "' AND ordermain.etype= '" . $etype . "'
     and ordermain.uid = " . $uid . " and is_hold = '1' order by ordermain.vrnoa");
    return $query->result_array();
}

public function fetchReportDataItemcMain($startDate, $endDate, $company_id, $etype, $uid)
{

    $query = $this->db->query("SELECT  stockdetail.etype as detype,g.name as dept_name,item.uom,item.item_des,stockdetail.qty,stockdetail.rate,stockdetail.amount,stockmain.vrnoa,date_format(stockmain.vrdate,'%d/%m/%y') as vrdate,p.name as party_name,stockmain.remarks,stockmain.taxpercent,stockmain.exppercent,stockmain.discp,stockmain.paid,stockmain.namount from
     stockmain stockmain
     INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid
     INNER JOIN item item ON stockdetail.item_id = item.item_id
     LEFT JOIN party p ON p.pid = stockmain.party_id
     INNER JOIN user user ON user.uid = stockmain.uid
     INNER JOIN company c ON c.company_id = stockmain.company_id
     LEFT JOIN department AS g ON g.did = stockdetail.godown_id
     WHERE stockmain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id= $company_id AND stockmain.etype= '" . $etype . "'
     and stockmain.uid = $uid order by stockmain.vrnoa");
    return $query->result_array();


}

public function fetchReportDataOrderMain($startDate, $endDate, $company_id, $etype, $uid)
{

    $query = $this->db->query("SELECT ordermain.vrnoa,ordermain.vrdate,p.name as party_name,ordermain.remarks,ordermain.taxpercent,ordermain.exppercent,ordermain.discp,ordermain.paid,ordermain.namount from
     ordermain ordermain
     INNER JOIN party p ON p.pid = ordermain.party_id
     INNER JOIN user user ON user.uid = ordermain.uid
     INNER JOIN company c ON c.company_id = ordermain.company_id
     WHERE ordermain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id= $company_id AND ordermain.etype= '" . $etype . "'
     and ordermain.uid = $uid order by ordermain.vrnoa");
    return $query->result_array();


}


public function fetchChartData($period, $company_id, $etype)
{
    $period = strtolower($period);

    $query = '';

    if ($period === 'daily') {
        $query = "SELECT VRNOA, party.name AS ACCOUNT, NAMOUNT  FROM stockmain as stockmain INNER JOIN party as party  ON party.pid = stockmain.party_id  WHERE stockmain.etype='$etype' AND stockmain.vrdate = CURDATE() AND stockmain.company_id=$company_id order by stockmain.vrdate desc LIMIT 10";
    } else if ($period === 'weekly') {
        $query = "SELECT sum(case when date_format(vrdate, '%W') = 'Monday' then namount else 0 end) as 'Monday', sum(case when date_format(vrdate, '%W') = 'Tuesday' then namount else 0 end) as 'Tuesday', sum(case when date_format(vrdate, '%W') = 'Wednesday' then namount else 0 end) as 'Wednesday', sum(case when date_format(vrdate, '%W') = 'Thursday' then namount else 0 end) as 'Thursday', sum(case when date_format(vrdate, '%W') = 'Friday' then namount else 0 end) as 'Friday', sum(case when date_format(vrdate, '%W') = 'Saturday' then namount else 0 end) as 'Saturday', sum(case when date_format(vrdate, '%W') = 'Sunday' then namount else 0 end) as 'Sunday' from stockmain where    etype = '$etype' and vrdate between DATE_SUB(VRDATE, INTERVAL 7 DAY) and CURDATE() AND stockmain.company_id=$company_id group by WEEK(VRDATE) order by WEEK(VRDATE) desc LIMIT 1 ";
    } else if ($period === 'monthly') {
        $query = "SELECT   sum(case when date_format(vrdate, '%b') = 'Jan' then namount else 0 end) as 'Jan', sum(case when date_format(vrdate, '%b') = 'Feb' then namount else 0 end) as 'Feb', sum(case when date_format(vrdate, '%b') = 'Mar' then namount else 0 end) as 'Mar', sum(case when date_format(vrdate, '%b') = 'Apr' then namount else 0 end) as 'Apr',sum(case when date_format(vrdate, '%b') = 'May' then namount else 0 end) as 'May', sum(case when date_format(vrdate, '%b') = 'Jun' then namount else 0 end) as 'Jun',sum(case when date_format(vrdate, '%b') = 'Jul' then namount else 0 end) as 'Jul', sum(case when date_format(vrdate, '%b') = 'Aug' then namount else 0 end) as 'Aug', sum(case when date_format(vrdate, '%b') = 'Sep' then namount else 0 end) as 'Sep', sum(case when date_format(vrdate, '%b') = 'Oct' then namount else 0 end) as 'Oct' , sum(case when date_format(vrdate, '%b') = 'Nov' then namount else 0 end) as 'Nov' , sum(case when date_format(vrdate, '%b') = 'Dec' then namount else 0 end) as 'Dec' from stockmain where    etype = 'purchase' and MONTH(VRDATE) = MONTH(CURDATE()) AND stockmain.company_id=$company_id group by month(VRDATE) order by month(VRDATE)";
    } else if ($period === 'yearly') {
        $query = "SELECT YEAR(vrdate) as 'Year', MONTHNAME(STR_TO_DATE(MONTH(VRDATE), '%m')) as Month, sum(namount) AS TotalAmount FROM stockmain where  etype = 'purchase' and YEAR(VRDATE) = YEAR(CURDATE()) and stockmain.company_id=$company_id GROUP BY YEAR(vrdate), MONTH(vrdate) ORDER BY YEAR(vrdate), MONTH(vrdate)";
    }

    $query = $this->db->query($query);
    return $query->result_array();
}

public function fetchNetSum($company_id, $etype)
{
        // if ($etype=='purchase' || $etype='salereturn'){
        // 	$query = "SELECT IFNULL(SUM(DEBIT),0) as 'PRETURNS_TOTAL' FROM pledger WHERE pledger.etype='$etype' AND pledger.company_id=$company_id";
        // }else{
        // 	$query = "SELECT IFNULL(SUM(CREDIT), 0) as 'PRETURNS_TOTAL' FROM pledger WHERE pledger.etype='$etype' AND pledger.company_id=$company_id";
        // }
    if ($etype == 'sale') {
        $query = "SELECT IFNULL(SUM(NAMOUNT),0) as 'PRETURNS_TOTAL' FROM stockmain WHERE stockmain.etype='$etype' AND stockmain.company_id=$company_id";
    } else {
        $query = "SELECT IFNULL(SUM(NAMOUNT),0) as 'PRETURNS_TOTAL' FROM stockmain WHERE stockmain.etype='$etype' AND stockmain.company_id=$company_id";
    }


    $result = $this->db->query($query);

    return $result->result_array();
}

public function getMaxVrnoa($etype, $company_id)
{

    $result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM stockmain WHERE etype = '" . $etype . "' and company_id=" . $company_id . " ");
    $row = $result->row_array();
    $maxId = $row['vrnoa'];

    return $maxId;
}

public function save($stockmain, $stockdetail, $vrnoa, $etype, $ledgers, $voucher_type_hidden)
{

    $this->db->trans_begin();

    $company_id = $stockmain['company_id'];
    $godown_id = $stockdetail[0]['godown_id'];

    $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $stockmain['company_id']));
    $result = $this->db->get('stockmain');


    $stid = "";
    if ($result->num_rows() > 0) {
        $result = $result->row_array();
        $stid = $result['stid'];
        $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $stockmain['company_id']));
        $this->db->update('stockmain', $stockmain);

        $this->db->where(array('stid' => $stid));
        $this->db->delete('stockdetail');
    } else {
        $this->db->insert('stockmain', $stockmain);
        $stid = $this->db->insert_id();
    }

    foreach ($stockdetail as $detail) {
        $detail['stid'] = $stid;
        $this->db->insert('stockdetail', $detail);
    }
    if ($etype == 'production') {
        $query = $this->db->query("call spw_ProductionLess($godown_id, $vrnoa, $company_id)");
            // return $query->result_array();
    }
    if($ledgers!=""){
        if ($voucher_type_hidden!='new'){
            $this->db->where(array(
                'dcno' => $vrnoa,
                'etype' => $etype,
                ));
            $affect = $this->db->delete('pledger');
        }
        $affect = 0;
        foreach ($ledgers as $ledger) {
            $ledger["dcno"] = $vrnoa;
            $this->db->insert('pledger', $ledger);
            $affect = $this->db->affected_rows();
        }
    }
    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return false;
    }
    else
    {
        $this->db->trans_commit();
        return true;
    }
}

public function fetch($vrnoa, $etype, $company_id)
{

    $result = $this->db->query("SELECT IFNULL(DATE_FORMAT(d.expirydate,'%d/%m/%Y'),'01/01/2000') expirydate, d.last_prate, m.salebillno,m.bilty_no, ifnull(bl.balance,0)as party_balance,party.uname as party_uname,party.mobile as party_mobile,party.city as party_city,party.address as party_address,party.`limit` as party_limit,
        party.cityarea as party_cityarea,i.uname as item_uname,i.uom as item_uom, CAST(DATE_FORMAT(DATE_ADD(m.date_time, INTERVAL 4 hour),'%d/%m/%y %h:%i %p') AS CHAR) AS date_time,u.uname user_name,party.name AS party_name,m.cash_tender,m.is_hold, m.freight,d.prate,i.short_code,b.name AS brand_name,b.bid,m.customer_name,m.mobile,d.discount AS discount1,d.damount,d.prate,d.uom,d.etype AS detype,d.pcs,d.packing,d.type, m.party_id_co,m.currency_id, d.received_by AS 'received',d.workdetail,g.name AS dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date, m.ddate AS 'due_date',m.ordno, m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom, d.weight, i.item_barcode
        FROM stockmain AS m
        INNER JOIN stockdetail AS d ON m.stid = d.stid
        INNER JOIN item AS i ON i.item_id = d.item_id
        LEFT JOIN brand b ON b.bid = i.bid
        INNER JOIN department AS g ON g.did = d.godown_id
        LEFT JOIN party ON party.pid=m.party_id
        LEFT join user u on u.uid=m.uid
        left join(
            select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid
            from pledger 
            where date<=(select vrdate from stockmain where vrnoa=$vrnoa and etype='$etype' and company_id=$company_id)
            group by pid
            ) as bl on bl.pid=m.party_id
WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' ");
if ($result->num_rows() > 0) {
    return $result->result_array();
} else {
    return false;
}
}

public function fetchitemc($vrnoa, $etype, $company_id)
{

    $result = $this->db->query("SELECT m.customer_name,m.mobile,d.discount as discount1,d.damount ,d.uom,d.etype as detype,d.pcs,d.packing ,d.type, m.party_id_co,m.currency_id, d.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom , d.weight  FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item AS i ON i.item_id = d.item_id  LEFT JOIN department AS g ON g.did = d.godown_id WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' ");
    if ($result->num_rows() > 0) {
        return $result->result_array();
    } else {
        return false;
    }
}

public function fetchVoucher($vrnoa, $etype, $company_id)
{
    $query = $this->db->query("SELECT IFNULL(DATE_FORMAT(d.expirydate,'%d/%m/%Y'),'01/01/2000') expirydate, date_format(d.expirydate,'%d/%m/%y') expirydate,ifnull(p.email,'') as party_email, m.freight,d.netamount AS d_netamount, d.carton,d.pcs,d.packing,d.type,p.name AS party_name,m.vrno,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no',d.damount AS item_dis, m.bilty_date AS 'due_date', m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, d.qty, ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom, d.weight
        FROM stockmain AS m
        INNER JOIN stockdetail AS d ON m.stid = d.stid
        INNER JOIN item AS i ON i.item_id = d.item_id
        INNER JOIN party AS p ON p.pid=m.party_id
        WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '" . $etype . "' ");
return $query->result_array();
}




public function fetchNavigation($vrnoa, $etype, $company_id)
{

    $result = $this->db->query("SELECT  ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount',IFNULL(DATE_FORMAT(d.expirydate,'%d/%m/%Y'),'01/01/2000') expirydate,CAST(DATE_FORMAT(m.date_time,'%d/%m/%y %h:%i %p') AS CHAR) AS date_time,user.uname as user_name, m.uid,m.vrno, m.vrnoa, DATE(m.vrdate) vrdate, m.received_by, m.remarks, m.etype, d.item_id, d.godown_id, d.godown_id2, ROUND(d.qty, 2) qty, ROUND(d.weight, 2) weight, d.uom, dep.name AS 'dept_to', i.item_des AS 'item_name', dep2.name AS 'dept_from'
        FROM stockmain m
        INNER JOIN stockdetail d ON m.stid = d.stid
        INNER JOIN item i ON i.item_id = d.item_id
        INNER JOIN department dep ON dep.did = d.godown_id
        INNER JOIN department dep2 ON dep2.did = d.godown_id2
        left join user on user.uid=m.uid
        WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' AND m.company_id = " . $company_id . " AND d.qty > 0");
    if ($result->num_rows() > 0) {
        return $result->result_array();
    } else {
        return false;
    }
}

public function fetchByCol($col)
{

    $result = $this->db->query("SELECT DISTINCT $col FROM stockmain");
    return $result->result_array();
}

public function fetchByColFromStkDetail($col)
{

    $result = $this->db->query("SELECT DISTINCT $col FROM stockdetail");
    return $result->result_array();
}

public function delete($vrnoa, $etype, $company_id)
{

    $this->db->where(array('etype' => $etype, 'dcno' => $vrnoa, 'company_id' => $company_id));
    $result = $this->db->delete('pledger');

    $this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
    $result = $this->db->get('stockmain');

    if ($result->num_rows() == 0) {
        return false;
    } else {

        $result = $result->row_array();
        $stid = $result['stid'];

        $this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
        $this->db->delete('stockmain');
        $this->db->where(array('stid' => $stid));
        $this->db->delete('stockdetail');

        return true;
    }
}

    // public function fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype)
    // {
    // 	// sr# date vr# partyname description  quantity rate amount
    // 	if ($what === 'voucher') {
    // 		if ($type == 'detailed') {
    // 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.WEIGHT, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype'  ORDER BY stockmain.VRNOA");
    // 			return $query->result_array();
    // 		} else {
    // 			$query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.WEIGHT)) WEIGHT, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' Group by stockmain.VRNOA ORDER BY stockmain.VRNOA");
    // 			return $query->result_array();
    // 		}
    // 	}

    // 	else if ($what == 'account') {
    // 		if ($type == 'detailed') {
    // 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockmain.party_id");
    // 			return $query->result_array();
    // 		} else {
    // 			$query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.WEIGHT)) WEIGHT, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' Group by stockmain.party_id ORDER BY stockmain.party_id");
    // 			return $query->result_array();
    // 		}
    // 	}

    // 	else if ($what == 'godown') {
    // 		if ($type == 'detailed') {
    // 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, dept.name AS NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept  ON stockdetail.godown_id = dept.did WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockdetail.godown_id");
    // 			return $query->result_array();
    // 		} else {
    // 			$query = $this->db->query("SELECT dept.NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.WEIGHT)) WEIGHT, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.godown_id AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockdetail.godown_id");
    // 			return $query->result_array();
    // 		}
    // 	}

    // 	else if ($what == 'item') {
    // 		if ($type == 'detailed') {
    // 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockdetail.item_id");
    // 			return $query->result_array();
    // 		} else {
    // 			$query = $this->db->query("SELECT item.item_des as NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.WEIGHT)) WEIGHT, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
    // 			return $query->result_array();
    // 		}
    // 	}

    // 	else if ($what == 'date') {
    // 		if ($type == 'detailed') {
    // 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockmain.vrdate");
    // 			return $query->result_array();
    // 		} else {
    // 			$query = $this->db->query("SELECT date(stockmain.vrdate) as DATE, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.WEIGHT)) WEIGHT, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' GROUP BY stockmain.vrdate ORDER BY stockmain.vrdate");
    // 			return $query->result_array();
    // 		}
    // 	}
    // }

public function fetchPurchaseReturnReportData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id ORDER BY stockmain.VRNOA");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id Group by stockmain.VRNOA ORDER BY stockmain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'account') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id ORDER BY stockmain.party_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id Group by stockmain.party_id ORDER BY stockmain.party_id");
            return $query->result_array();
        }
    } else if ($what == 'godown') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, dept.name AS NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id ORDER BY stockdetail.godown_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT dept.NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id GROUP BY stockdetail.godown_id ORDER BY stockdetail.godown_id");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockdetail.item_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT item.item_des as NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.vrdate");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT date(stockmain.vrdate) as DATE, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.vrdate ORDER BY stockmain.vrdate");
            return $query->result_array();
        }
    }
}

public function fetchStockNavigationData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockdetail.QTY > 0 ORDER BY stockmain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'location') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockdetail.QTY > 0 ORDER BY frm.name");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockdetail.QTY > 0 ORDER BY item.item_des");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRDATE, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, item.item_des, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockdetail.QTY > 0 ORDER BY stockmain.VRDATE");
            return $query->result_array();
        }
    }
}

public function fetchMaterialReturnData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.VRNOA");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, sum(stockdetail.QTY) QTY, sum(stockdetail.RATE) RATE, sum(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.VRNOA ORDER BY stockmain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'person') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.received_by");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT stockmain.received_by NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.received_by ORDER BY stockmain.received_by");
            return $query->result_array();
        }
    } else if ($what == 'location') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, dept.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockdetail.godown_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT dept.NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockdetail.godown_id ORDER BY stockdetail.godown_id");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockdetail.item_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT item.item_des NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.vrdate desc");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT stockmain.VRDATE NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.VRDATE ORDER BY stockmain.VRDATE");
            return $query->result_array();
        }
    }
}


public function fetchConsumptionData($startDate, $endDate, $what, $type)
{
        // sr# date vr# partyname description  quantity rate amount
    if ($what === 'voucher') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.VRNOA");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, sum(stockdetail.QTY) QTY, sum(stockdetail.RATE) RATE, sum(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.VRNOA ORDER BY stockmain.VRNOA");
            return $query->result_array();
        }
    } else if ($what == 'person') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.received_by");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT stockmain.received_by NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.received_by ORDER BY stockmain.received_by");
            return $query->result_array();
        }
    } else if ($what == 'location') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, dept.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockdetail.godown_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT dept.NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockdetail.godown_id ORDER BY stockdetail.godown_id");
            return $query->result_array();
        }
    } else if ($what == 'item') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockdetail.item_id");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT item.item_des NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
            return $query->result_array();
        }
    } else if ($what == 'date') {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY stockmain.vrdate desc");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT stockmain.VRDATE NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' GROUP BY stockmain.VRDATE ORDER BY stockmain.VRDATE");
            return $query->result_array();
        }
    }
}

public function fetchPRRangeSum($from, $to)
{
    $query = "SELECT IFNULL(SUM(CREDIT), 0)-IFNULL(SUM(DEBIT),0) as 'PRETURNS_TOTAL' FROM pledger pledger WHERE pid IN (SELECT pid FROM party party WHERE NAME='purchase return') AND date between '{$from}' AND '{$to}'";
    $result = $this->db->query($query);

    return $result->result_array();
}

public function fetchImportRangeSum($from, $to)
{
    $query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PIMPORTS_TOTAL' FROM pledger pledger WHERE pid IN ( SELECT pid FROM party party WHERE NAME='purchase import') AND date BETWEEN '{$from}' AND '{$to}'";
    $result = $this->db->query($query);

    return $result->result_array();
}

public function fetchRangeSum($from, $to,$etype)
{
    if($etype=='cash sale'){
        $etype='cash';
        $query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PURCHASES_TOTAL' FROM pledger pledger WHERE etype='sale' and pid IN ( SELECT $etype FROM setting_configuration) AND date BETWEEN '{$from}' AND '{$to}'";
    }elseif($etype=='invoicediscount'){
        
        $query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PURCHASES_TOTAL' FROM pledger pledger WHERE etype='sale' and pid IN ( SELECT $etype FROM setting_configuration) AND date BETWEEN '{$from}' AND '{$to}'";
    }else{
        $query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PURCHASES_TOTAL' FROM pledger pledger WHERE pid IN ( SELECT $etype FROM setting_configuration) AND date BETWEEN '{$from}' AND '{$to}'";    
    }
    
    $result = $this->db->query($query);

    return $result->result_array();
}

public function backupHoldSaleVr($vrnoa, $etype, $companyId)
{
    $result = $this->db->query("select * from stockmain
     where vrnoa = '" . $vrnoa . "' and
     etype = '" . $etype . "' and company_id = '" . $companyId . "' ");
    $mainArray = $result->result_array();
    if (count($mainArray) > 0) {
        $mainData = array(
            "vrno" => $mainArray[0]["vrno"],
            "vrnoa" => $mainArray[0]["vrnoa"],
            "vrdate" => $mainArray[0]["vrdate"],
            "party_id" => $mainArray[0]["party_id"],
            "bilty_no" => $mainArray[0]["bilty_no"],
            "bilty_date" => $mainArray[0]["bilty_date"],
            "received_by" => $mainArray[0]["received_by"],
            "transporter_id" => $mainArray[0]["transporter_id"],
            "remarks" => $mainArray[0]["remarks"],
            "year_srno" => $mainArray[0]["year_srno"],
            "etype" => $mainArray[0]["etype"],
            "namount" => $mainArray[0]["namount"],
            "uid" => $mainArray[0]["uid"],
            "order_vrno" => $mainArray[0]["order_vrno"],
            "freight" => $mainArray[0]["freight"],
            "party_id_co" => $mainArray[0]["party_id_co"],
            "salebillno" => $mainArray[0]["salebillno"],
            "discp" => $mainArray[0]["discp"],
            "discount" => $mainArray[0]["discount"],
            "currency_id" => $mainArray[0]["currency_id"],
            "expense" => $mainArray[0]["expense"],
            "company_id" => $mainArray[0]["company_id"],
            "vehicle_id" => $mainArray[0]["vehicle_id"],
            "officer_id" => $mainArray[0]["officer_id"],
            "etype2" => $mainArray[0]["etype2"],
            "ddays" => $mainArray[0]["ddays"],
            "ddate" => $mainArray[0]["ddate"],
            "date_time" => $mainArray[0]["date_time"],
            "exppercent" => $mainArray[0]["exppercent"],
            "tax" => $mainArray[0]["tax"],
            "taxpercent" => $mainArray[0]["taxpercent"],
            "paid" => $mainArray[0]["paid"],
            "ordno" => $mainArray[0]["ordno"],
            "customer_name" => $mainArray[0]["customer_name"],
            "mobile" => $mainArray[0]["mobile"],
            "item_discount" => $mainArray[0]["item_discount"],
            "status" => "",
            "is_hold" => $mainArray[0]["is_hold"]
            );
$this->db->insert('ordermain', $mainData);

$resultDetail = $this->db->query("select * from stockdetail
    where stid = '" . $mainArray[0]["stid"] . "' ");
$detailArray = $resultDetail->result_array();
if (count($detailArray) > 0) {
    $detailData = array();
    foreach ($detailArray as $row) {
        $detailData = array(
            "oid" => $row["stid"],
            "item_id" => $row["item_id"],
            "godown_id" => $row["godown_id"],
            "uom" => $row["uom"],
            "qty" => $row["qty"],
            "qtyf" => $row["qtyf"],
            "rate" => $row["rate"],
            "amount" => $row["amount"],
            "discount" => $row["discount"],
            "damount" => $row["damount"],
            "netamount" => $row["netamount"],
            "bundle" => $row["bundle"],
            "weight" => $row["weight"],
            "godown_id2" => $row["godown_id2"],
            "type" => $row["type"],
            "prate" => $row["prate"],
            "job_id" => $row["job_id"],
            "workdetail" => $row["workdetail"],
            "received_by" => $row["received_by"],
            "carton" => $row["carton"],
            "pcs" => $row["pcs"],
            "packing" => $row["packing"]
            );
    }
    $this->db->insert('orderdetail', $detailData);
}
}
}
}

/* End of file purchases.php */
/* Location: ./application/models/purchases.php */