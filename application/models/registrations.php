<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrations extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetchStudentCharges($rid) {

		$result = $this->db->query("select reg.rid, ch.description as 'description', ch.pid, reg.name, reg.fname, reg.mobile, reg.address, reg.claid, reg.brid, reg.chid, reg.gender, reg.date, reg.fee, br.name as 'branch_name', cls.name as 'class_name'from registration as reg inner join branch as br on reg.brid = br.brid inner join class as cls on reg.claid = cls.claid inner join charges as ch on reg.chid = ch.chid where reg.rid = $rid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function getMaxRegReceiveId($brid) {

		$this->db->select_max('frid');
		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('feerecproc');

		$row = $result->row_array();
		$maxId = $row['frid'];
		return $maxId;
	}

	public function saveRegRecieve($regrec, $dcno, $brid) {

		$this->db->where(array(
								'frid' => $dcno,
								'brid' => $brid
							));
		$result = $this->db->get('feerecproc');
		
		if ( $result->num_rows() > 0 ) {

			$this->db->where(array(
								'frid' => $dcno,
								'brid' => $brid
							));
			$this->db->delete('feerecproc');
		}

		$affect = 0;

		$this->db->insert('feerecproc', $regrec);
		$affect = $this->db->affected_rows();

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	// fetchs the previous saved record
	public function fetchRegRecieve($dcno, $brid) {

		$result = $this->db->query("select frid, pid, fine, discount, date, amount, rno from feerecproc where frid = $dcno AND brid = $brid");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->row_array();
		}
	}

	// fetchs the previous saved record
	public function fetchChargesForRegRecieve($dcno, $brid) {

		$result = $this->db->query("select ar_pledger_tbl.pid, credit as 'charge_amount', ar_pledger_tbl.chid, charges.description as 'charge_name'from ar_pledger_tbl inner join charges on charges.chid = ar_pledger_tbl.chid where etype = 'frvproc' and ar_pledger_tbl.chid <> '0' and dcno = $dcno AND ar_pledger_tbl.brid = $brid AND charges.brid = $brid");
		return $result->result_array();
	}

	public function deleteRegRecieveVoucher($frid, $brid) {
		
		$this->db->where(array(
						'frid' => $frid,
						'brid' => $brid
					));
		$result = $this->db->delete('feerecproc');
	}
}

/* End of file registrations.php */
/* Location: ./application/models/registrations.php */