<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Results extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId($brid) {

		$this->db->select_max('dcno');
		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('result');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function save( $results, $dcno, $brid ) {

		$this->db->where(array(
							'dcno' => $dcno,
							'brid' => $brid
						));
		$this->db->delete('result');

		$affect = 0;
		foreach ($results as $result) {

			$result['dcno'] = $dcno;
			$this->db->insert('result', $result);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetch( $dcno, $brid ) {

		$result = $this->db->query("SELECT r.dcno, r.brid, r.claid, r.stdid, r.secid, r.sbid, r.tmarks, r.obmarks, r.remarks, r.date, r.term, r.staid, r.session, r.grade, r.percentage, r.status, stu.name AS 'student_name'FROM result AS r INNER JOIN student AS stu ON stu.stdid = r.stdid WHERE r.dcno = $dcno AND r.brid = $brid AND stu.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function delete($dcno, $brid) {

		$this->db->where(array(
								'dcno' => $dcno,
								'brid' => $brid
							));
		$result = $this->db->get('result');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
								'dcno' => $dcno,
								'brid' => $brid
							));
			$result = $this->db->delete('result');

		} else {
			return false;
		}
	}

	public function fetchColByBrClsNSec( $col, $brid, $claid, $secid ) {

		$result = $this->db->query("SELECT DISTINCT $col FROM result WHERE brid = $brid AND claid = $claid AND secid= $secid;");

		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}

	}

	public function resultSubjectWiseReport($brid) {

		$result = $this->db->query("SELECT sec.name AS 'section_name', res.dcno, res.tmarks, res.term, sbj.name AS 'subject_name', res.session FROM (result AS res INNER JOIN section AS sec ON res.secid=sec.secid) INNER JOIN subject AS sbj ON res.sbid=sbj.sbid WHERE res.brid = $brid AND sec.brid = $brid AND sbj.brid = $brid GROUP BY res.term ORDER BY res.session, sec.name, res.term, sbj.name, res.dcno");
		return $result->result_array();
	}

	public function stuResultTeacherWiseReport($brid) {

		$result = $this->db->query("SELECT br.name AS 'branch_name', res.tmarks, res.percentage, res.remarks, DATE(res.date) AS date, res.term, res.session, sec.name AS 'section_name', stf.name AS 'teacher_name', res.obmarks, stu.name AS 'student_name', res.grade, res.status, sbj.name AS 'subject_name'FROM subject AS sbj INNER JOIN ((student AS stu INNER JOIN (staff AS stf INNER JOIN (branch AS br INNER JOIN result AS res ON br.brid=res.brid) ON stf.staid=res.staid) ON stu.stdid=res.stdid) INNER JOIN section AS sec ON res.secid=sec.secid) ON sbj.sbid=res.sbid WHERE sbj.brid = $brid AND stu.brid = $brid AND stf.brid = $brid AND br.brid = $brid AND res.brid = $brid AND sec.brid = $brid ORDER BY br.name, stf.name, res.session, res.term, sec.name, sbj.name");
		return $result->result_array();
	}

	public function overallSemResultReport($brid, $claid, $secid, $session, $stdid) {

		$result = $this->db->query("SELECT res.obmarks, res.term, stu.name AS 'student_name', stu.fname AS 'father_name', sec.name AS 'section_name', stu.stdid, sbj.name AS 'subject_name', res.tmarks, res.percentage FROM (section AS sec INNER JOIN (student AS stu INNER JOIN result AS res ON stu.stdid=res.stdid) ON sec.secid=res.secid) INNER JOIN subject AS sbj ON res.sbid=sbj.sbid WHERE res.brid = $brid AND res.claid = $claid AND res.secid = $secid AND res.session = $session AND res.stdid = $stdid AND sec.brid = $brid AND stu.brid = $brid AND sbj.brid = $brid ORDER BY sbj.name, res.obmarks");

		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function studentResultReport($brid, $claid, $secid, $session, $stdid, $term) {

		$result = $this->db->query("SELECT res.obmarks, res.remarks, res.term, stu.name AS 'student_name', stu.fname AS 'father_name', sec.name AS 'section_name', stu.stdid, sbj.name AS 'subject_name', res.tmarks, res.percentage FROM (section AS sec INNER JOIN (student AS stu INNER JOIN result AS res ON stu.stdid=res.stdid) ON sec.secid=res.secid) INNER JOIN subject AS sbj ON res.sbid=sbj.sbid WHERE res.brid = $brid AND res.claid = $claid AND res.secid = $secid AND res.session = $session AND res.stdid = $stdid AND res.term = '". $term ."' AND sec.brid = $brid AND stu.brid = $brid AND sbj.brid = $brid ORDER BY sbj.name, res.obmarks");

		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file results.php */
/* Location: ./application/models/results.php */