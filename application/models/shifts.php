<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shifts extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxShiftId() {

		$this->db->select_max('shid');
		$result = $this->db->get('shift');

		$row = $result->row_array();
		$maxId = $row['shid'];

		return $maxId;
	}

	public function saveShift( $shift ) {

		$this->db->where(array('shid' => $shift['shid']));
		$result = $this->db->get('shift');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('shid' => $shift['shid'] ));
			$result = $this->db->update('shift', $shift);
			$affect = $this->db->affected_rows();
		} else {

			unset($shift['shid']);
			$result = $this->db->insert('shift', $shift);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchShift( $shid ) {

		$result = $this->db->query("SELECT shid, name, TIME_FORMAT(tin, '%h: %i %p') AS tin, TIME_FORMAT(tout, '%h: %i %p') AS tout, TIME_FORMAT(resin, '%h: %i %p') AS resin, TIME_FORMAT(resout, '%h: %i %p') AS resout FROM shift WHERE shid = $shid");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAllShifts() {

		$result = $this->db->query("SELECT shid, name, TIME_FORMAT(tin, '%h: %i %p') AS tin, TIME_FORMAT(tout, '%h: %i %p') AS tout, TIME_FORMAT(resin, '%h: %i %p') AS resin, TIME_FORMAT(resout, '%h: %i %p') AS resout FROM shift");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file shifts.php */
/* Location: ./application/models/shifts.php */