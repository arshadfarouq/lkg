<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staffs extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getMaxId($brid) {

		$this->db->select_max('staid');
		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('staff');

		$row = $result->row_array();
		$maxId = $row['staid'];

		return $maxId;
	}

	public function getAllTypes($brid) {

		$types = $this->db->query("SELECT DISTINCT type FROM staff WHERE brid = $brid AND type <> '' ORDER BY type DESC");
		return $types->result_array();
	}

	public function getAllAgreements($brid) {

		$agreements = $this->db->query("SELECT DISTINCT agreement FROM staff WHERE agreement <> '' AND brid = $brid ORDER BY agreement DESC");
		return $agreements->result_array();
	}

	public function getAllReligions($brid) {

		$religions = $this->db->query("SELECT DISTINCT religion FROM staff WHERE religion <> '' AND brid = $brid ORDER BY religion DESC");
		return $religions->result_array();
	}

	public function getAllBankNames($brid) {

		$banks = $this->db->query("SELECT DISTINCT bankname FROM salary WHERE bankname <> '' AND brid = $brid ORDER BY bankname DESC");
		return $banks->result_array();
	}

	public function save( $staff, $pid, $brid ) {

		$staff = json_decode(html_entity_decode($staff['staff'], true));
		$staff = (array)$staff;
		$staff['pid'] = $pid;

		if ( isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
			$staff['photo'] = $this->upload_photo( $staff );
		} else {
			unset($staff['photo']);
		}


		$this->db->where(array(
								'staid' => $staff['staid'],
								'brid' => $brid
							));
		$result = $this->db->get('staff');

		$affect = 0;
		$staid = "";
		if ($result->num_rows() > 0 ) {

			$this->db->where(array(
								'staid' => $staff['staid'],
								'brid' => $brid
							));
			$affect = $this->db->update('staff', $staff);

			$staid = $staff['staid'];
		} else {
			$this->db->insert('staff', $staff);
			$affect = $this->db->affected_rows();

			$staid = $this->db->insert_id();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return $staid;
		}
	}

	public function upload_photo( $photo ){
		// $uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . 'arqam/assets/uploads/staff/');
		$uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/assets/uploads/staff/');


		// die($uploadsDirectory);
		// die(realpath(dirname(__FILE__)));

		$errors = array(1 => 'php.ini max file size exceeded',
		                2 => 'html form max file size exceeded',
		                3 => 'file upload was only partial',
		                4 => 'no file was attached');

		($_FILES['photo']['error'] == 0)
		    		or die($errors[$_FILES['photo']['error']]);

		@is_uploaded_file($_FILES['photo']['tmp_name'] )
					or die('Not an HTTP upload');

		@getimagesize($_FILES['photo']['tmp_name'])
		    or die('Only image uploads are allowed');

		$now = time();
		while(file_exists($uploadFilename = $uploadsDirectory.$now.'-'.$_FILES['photo']['name']))
		{
		    $now++;
		}

		// now let's move the file to its final location and allocate the new filename to it
		move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFilename) or die('Error uploading file');

		$path_parts = explode('/', $uploadFilename);
		$uploadFilename = $path_parts[count($path_parts) - 1];
		return $uploadFilename;
	}

	public function saveSalary( $salary ) {

		$this->db->where(array(
								'staid' => $salary['staid']
							));
		$result = $this->db->delete('salary');


		$this->db->insert('salary', $salary);
	}

	public function saveQualification( $qualifications, $staid ) {

		$this->db->where(array(
								'staid' => $staid
							));
		$result = $this->db->delete('qualification');


		foreach ($qualifications as $qualification) {

			$qualification = (array)$qualification;
			$this->db->insert('qualification', $qualification);
		}
	}

	public function saveExperience( $experiences, $staid ) {

		$this->db->where(array(
								'staid' => $staid
							));
		$result = $this->db->delete('experience');


		foreach ($experiences as $experience) {

			$experience = (array)$experience;
			$this->db->insert('experience', $experience);
		}
	}

	public function fetchStaff( $staid, $brid ) {

		$result = $result = $this->db->query("SELECT staid, brid, name, fname, religion, cast, photo, address, phone, birthdate, jdate, type, salary, date, mobile, agreement, gender, mstatus, active, cnic, pid FROM staff where staid = $staid AND brid = $brid");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchStaffSalary( $staid ) {

		$result = $this->db->query("select bs, designation, bpay, inipay, hrent, convallow, medallow, entertain, charge, bankname, acno, netpay, househ, scall, publicsall, saall, dearness, adhoc1, adhoc2, arrears, pfund, income, hostel, pessi, scont, recovery, totalpay, tdeduc, loan from salary where staid = $staid");
		return $result->result_array();
	}

	public function fetchStaffQualification( $staid ) {

		$result = $this->db->query("select quali, grade, year, subject, institute from qualification where staid = $staid");
		return $result->result_array();
	}

	public function fetchStaffExperience( $staid ) {

		$result = $this->db->query("select `job`, `from`, `to`, `pd1` from `experience` where staid = $staid");
		return $result->result_array();
	}

	public function fetchAllTeachers($brid) {

		$result = $this->db->query("SELECT `staid`, `name`, `type` FROM `staff` WHERE type = 'teacher' AND active = 1 AND brid = $brid");
		return $result->result_array();
	}

	public function fetchStaffReportByStatus($status, $brid) {

		$query = "";
		if ($status == '1' || $status == '0') {
			$query = "SELECT stf.staid, br.name AS 'branch_name', stf.name, stf.mobile, stf.phone, stf.address, stf.type, stf.active, IFNULL(group_concat(q.quali), '') quali, IFNULL(q.experience, '') experience FROM branch AS br INNER JOIN staff AS stf ON br.brid=stf.brid LEFT JOIN experience AS e ON stf.staid=e.staid LEFT JOIN qualification AS q ON stf.staid=q.staid WHERE stf.brid = $brid AND stf.active = $status AND br.brid = $brid AND stf.brid = $brid GROUP BY stf.staid ORDER BY br.name, stf.type, stf.name";
		} else {
			$query = "SELECT stf.staid, br.name AS 'branch_name', stf.name, stf.mobile, stf.phone, stf.address, stf.type, stf.active, IFNULL(group_concat(q.quali), '') quali, IFNULL(q.experience, '') experience FROM branch AS br INNER JOIN staff AS stf ON br.brid=stf.brid LEFT JOIN experience AS e ON stf.staid=e.staid LEFT JOIN qualification AS q ON stf.staid=q.staid WHERE stf.brid = $brid AND br.brid = $brid GROUP BY stf.staid ORDER BY br.name, stf.type, stf.name";
		}

		$result = $this->db->query($query);
		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}
}

/* End of file staffs.php */
/* Location: ./application/models/staffs.php */