<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stocks extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxVrno($etype) {

		$result = $this->db->query("SELECT MAX(vrno) vrno FROM stockmain WHERE etype = '". $etype ."' AND DATE(vrdate) = DATE(NOW())");
		$row = $result->row_array();
		$maxId = $row['vrno'];

		return $maxId;
	}

	public function getMaxVrnoa($etype) {

		$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM stockmain WHERE etype = '". $etype ."'");
		$row = $result->row_array();
		$maxId = $row['vrnoa'];

		return $maxId;
	}

	public function save( $stockmain, $stockdetail, $vrnoa, $etype ) {

		$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
		$result = $this->db->get('stockmain');

		if ($result->num_rows() > 0) {

			$result = $result->row_array();
			$stid = $result['stid'];

			////////////////////////////////
			// delete from stock main     //
			////////////////////////////////
			$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
			$result = $this->db->delete('stockmain');
			//////////////////////////////
			// delete from stock detail //
			//////////////////////////////
			$this->db->where(array('stid' => $stid));
			$result = $this->db->delete('stockdetail');
		}

		$this->db->insert('stockmain', $stockmain);
		$stid = $this->db->insert_id();

		$affect = 0;
		foreach ($stockdetail as $detail) {

			$detail['stid'] = $stid;
			$this->db->insert('stockdetail', $detail);
			$affect = $this->db->affected_rows();
		}

		if ( $affect == 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function delete( $vrnoa, $etype ) {

		$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
		$result = $this->db->get('stockmain');

		if ($result->num_rows() > 0) {

			$result = $result->row_array();
			$stid = $result['stid'];

			////////////////////////////////
			// delete from stock main     //
			////////////////////////////////
			$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
			$result = $this->db->delete('stockmain');
			//////////////////////////////
			// delete from stock detail //
			//////////////////////////////
			$this->db->where(array('stid' => $stid));
			$result = $this->db->delete('stockdetail');
		}

		return true;
	}


	public function fetchDailyVoucherReport($startDate, $endDate) {

		$query = $this->db->query("SELECT m.VRNOA, p.NAME, m.REMARKS, ROUND(d.QTY, 2) QTY, ROUND(d.NETAMOUNT, 2) NETAMOUNT, m.ETYPE FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN party p ON p.pid = m.party_id WHERE m.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY m.ETYPE");
		return $query->result_array();
	}

	public function fetchStockReport($startDate, $endDate, $what , $company_id ,$crit,$etype) {
		$get_crit='';
		if ($what == 'location') {
			$get_crit='dep.name';
		}else if ($what == 'item') {
			$get_crit='i.item_des';
		}else if ($what == 'category') {
			$get_crit='cat.name';
		}else if ($what == 'subcategory') {
			$get_crit='subcat.name';
		}else if ($what == 'brand') {
			$get_crit='b.name';
		}else if ($what == 'uom') {
			$get_crit='i.uom';
		}else if ($what == 'type') {
			$get_crit='i.barcode';
		}
		if($etype=='expiredstock'){
			$query = $this->db->query("SELECT  $get_crit as DESCRIPTION,date_format(d.expirydate,'%d/%m/%y') as expirydate,i.item_des NAME,i.item_id,i.uom ,IFNULL(SUM(d.qty),0) AS qty, ROUND(IFNULL(lp.rate,0),2) AS cost, ROUND(IFNULL(SUM(d.qty),0)* IFNULL(lp.rate,0),2) AS value,round(ifnull(av.avg_rate,0),2) as avg_rate
				FROM stockdetail d
				INNER JOIN item i ON i.item_id=d.item_id
				INNER JOIN stockmain m on m.stid = d.stid
				left JOIN category cat ON cat.catid = i.catid
				left JOIN subcategory subcat ON subcat.subcatid = i.subcatid
				left JOIN brand b ON b.bid = i.bid
				LEFT JOIN department dep ON dep.did = d.godown_id
				LEFT JOIN (SELECT d.item_id,(d.rate-(d.discount*d.rate/100)) AS rate FROM stockdetail d WHERE d.qty>0 AND d.stid=(
					SELECT stid FROM stockdetail WHERE item_id=d.item_id and qty>0 ORDER BY stid DESC LIMIT 1) 
			GROUP BY d.item_id
			ORDER BY d.item_id ) AS lp ON lp.item_id=d.item_id
			LEFT JOIN(      
				select IfNULL(SUM(netamount),0)/IfNULL(SUM(qty),0) AS avg_rate,item_id
				from stockdetail d       
				INNER JOIN stockmain m ON m.stid = d.stid
				where d.qty>0 and m.vrdate<='". $endDate ."'
				GROUP BY item_id
				HAVING IfNULL(SUM(qty),0)<>0      
				)AS av ON av.item_id = d.item_id   

WHERE d.expirydate <= '". $endDate ."' AND d.expirydate<> '2000-01-01' AND m.company_id= $company_id $crit
GROUP BY $get_crit,i.item_des,d.expirydate
having IFNULL(SUM(d.qty),0)>0");

return $query->result_array();

}else if($what!='summary'){
	$query = $this->db->query("SELECT  $get_crit as DESCRIPTION,i.item_des NAME,i.item_id,i.uom ,IFNULL(SUM(d.qty),0) AS qty, ROUND(IFNULL(lp.rate,0),2) AS cost, ROUND(IFNULL(SUM(d.qty),0)* IFNULL(lp.rate,0),2) AS value,round(ifnull(av.avg_rate,0),2) as avg_rate
		FROM stockdetail d
		INNER JOIN item i ON i.item_id=d.item_id
		INNER JOIN stockmain m on m.stid = d.stid
		left JOIN category cat ON cat.catid = i.catid
		left JOIN subcategory subcat ON subcat.subcatid = i.subcatid
		left JOIN brand b ON b.bid = i.bid
		LEFT JOIN department dep ON dep.did = d.godown_id
		LEFT JOIN (SELECT d.item_id,(d.rate-(d.discount*d.rate/100)) AS rate FROM stockdetail d WHERE d.qty>0 AND d.stid=(
			SELECT stid FROM stockdetail WHERE item_id=d.item_id and qty>0 ORDER BY stid DESC LIMIT 1) 
	GROUP BY d.item_id
	ORDER BY d.item_id ) AS lp ON lp.item_id=d.item_id
	LEFT JOIN(      
		select IfNULL(SUM(netamount),0)/IfNULL(SUM(qty),0) AS avg_rate,item_id
		from stockdetail d       
		INNER JOIN stockmain m ON m.stid = d.stid
		where d.qty>0 and m.vrdate<='". $endDate ."'
		GROUP BY item_id
		HAVING IfNULL(SUM(qty),0)<>0      
		)AS av ON av.item_id = d.item_id   
WHERE m.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND m.company_id= $company_id $crit
GROUP BY $get_crit,i.item_des
having IFNULL(SUM(d.qty),0)<>0");
return $query->result_array();

} else{

	$query = $this->db->query("SELECT i.item_id,i.item_des, ifnull(op.opqty,0) AS 'opqty', ifnull(cl.in,0) AS 'in', ifnull(cl.out,0) AS 'out', ifnull(op.opqty,0) + ifnull(cl.in,0) + ifnull(cl.out,0) AS 'balance', ROUND(IFNULL(lp.rate,0),2) AS cost,round(ifnull(av.avg_rate,0),2) as avg_rate
		FROM item i
		LEFT JOIN (
			SELECT d.item_id, SUM(IF(d.qty <0,d.qty,0)) AS 'out', SUM(IF(d.qty>0,d.qty,0)) AS 'in'
			FROM stockdetail d
			inner join item i 
			WHERE d.stid IN (
				SELECT stid
				FROM stockmain where vrdate  >='". $startDate ."' and vrdate  <='". $endDate ."' and company_id=$company_id) 
	GROUP BY d.item_id) AS cl ON cl.item_id=i.item_id
	LEFT JOIN (
		SELECT item_id, SUM(qty) AS 'opqty'
		FROM stockdetail
		WHERE stid IN (
			SELECT stid
			FROM stockmain where vrdate < '". $startDate ."' and company_id=$company_id)
	GROUP BY item_id) AS op ON op.item_id=i.item_id
	LEFT JOIN (SELECT d.item_id,(d.rate-(d.discount*d.rate/100)) AS rate FROM stockdetail d WHERE d.qty>0 AND d.stid=(
		SELECT stid FROM stockdetail WHERE item_id=d.item_id and qty>0 ORDER BY stid DESC LIMIT 1) 
GROUP BY d.item_id

ORDER BY d.item_id ) AS lp ON lp.item_id=i.item_id
LEFT JOIN(      
	select IfNULL(SUM(netamount),0)/IfNULL(SUM(qty),0) AS avg_rate,item_id
	from stockdetail d       
	INNER JOIN stockmain m ON m.stid = d.stid
	where d.qty>0 and m.vrdate<='". $endDate ."'
	GROUP BY item_id
	HAVING IfNULL(SUM(qty),0)<>0      
	)AS av ON av.item_id = i.item_id  

where 1=1 $crit
ORDER BY i.item_id,i.item_des");

return $query->result_array();
}

}
}

/* End of file stocks.php */
/* Location: ./application/models/stocks.php */