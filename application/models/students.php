<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students extends CI_Model {
	// private $brid = "";

	public function __construct() {
		parent::__construct();
		// $this->brid = $this->session->userdata('brid');
	}

	public function getMaxId($brid) {

		$this->db->select_max('stdid');
		$this->db->where(array('brid' => $brid));
		$result = $this->db->get('student');

		$row = $result->row_array();
		$maxId = $row['stdid'];

		return $maxId;
	}

	public function upload_photo( $photo )
	{
		// $uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . 'arqam/assets/uploads/students/');
		$uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/assets/uploads/students/');


		// die($uploadsDirectory);
		// die(realpath(dirname(__FILE__)));

		$errors = array(1 => 'php.ini max file size exceeded',
			2 => 'html form max file size exceeded',
			3 => 'file upload was only partial',
			4 => 'no file was attached');

		($_FILES['photo']['error'] == 0)
		or die($errors[$_FILES['photo']['error']]);

		@is_uploaded_file($_FILES['photo']['tmp_name'] )
		or die('Not an HTTP upload');

		@getimagesize($_FILES['photo']['tmp_name'])
		or die('Only image uploads are allowed');

		$now = time();
		while(file_exists($uploadFilename = $uploadsDirectory.$now.'-'.$_FILES['photo']['name']))
		{
			$now++;
		}

		// now let's move the file to its final location and allocate the new filename to it
		move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFilename) or die('Error uploading file');

		$path_parts = explode('/', $uploadFilename);
		$uploadFilename = $path_parts[count($path_parts) - 1];
		return $uploadFilename;
	}
	public function fetchByCol($col)
    {

        $result = $this->db->query("SELECT DISTINCT $col FROM student");
        return $result->result_array();
    }
	public function searchStudent($search,$type)
	{
		$crit = "";
		$activee = 1;

		$qry="SELECT stu.*
		FROM student stu
		WHERE stu.active = 1 AND (stu.name LIKE '$search%' OR stu.mobile LIKE '$search%'  OR stu.address LIKE '$search%' OR stu.city LIKE '$search%' OR stu.rollno LIKE '$search%' )
		LIMIT 0, 20";


		$query= $this->db->query($qry);
		$result = $query->result_array();
		return $result;
	}
	public function fetchAllCategories()
	{
		$result = $this->db->get("fee_category");
		return $result->result_array();
	}
	public function fetchAllsection()
	{
		$result = $this->db->get("section");
		return $result->result_array();
	}
	public function fetchAllclass()
	{
		$result = $this->db->get("class");
		return $result->result_array();
	}
	public function fetchAllbranch()
	{
		$result = $this->db->get("branch");
		return $result->result_array();
	}
	public function fetchAll_report($from, $to, $orderby, $status ,$crit)
	{

		if ($status == 'all_stu') {
			$result = $this->db->query("SELECT $orderby as voucher,us.uid,stdid, stu.name AS 'student_name',stu.mobile,stu.address, stu.claid, stu.brid, stu.secid, stu.fid, stu.pid, 
				cls.name AS 'class_name', br.name AS 'branch_name', cls.name AS 'class_name', fc.name AS 'fc_name',
				sec.name AS 'section_name',stu.*
				FROM student AS stu
				INNER JOIN branch AS br ON stu.brid = br.brid
				INNER JOIN class AS cls ON stu.claid = cls.claid and cls.brid=stu.brid
				INNER JOIN section AS sec ON stu.secid = sec.secid and sec.brid=stu.brid
				INNER JOIN fee_category AS fc ON stu.fid = fc.fcid and fc.brid=stu.brid
				Left JOIN  user AS us ON us.uid=br.brid
				where 1=1 $crit
				ORDER BY $orderby,br.name,cls.name,sec.name,stu.name limit 1000; ");
		} else {
			$result = $this->db->query("SELECT $orderby as voucher,us.uid,stdid, stu.name AS 'student_name',stu.mobile,stu.address, stu.claid, stu.brid, stu.secid, stu.fid, stu.pid, 
				cls.name AS 'class_name', br.name AS 'branch_name', cls.name AS 'class_name', fc.name AS 'fc_name',
				sec.name AS 'section_name',stu.*
				FROM student AS stu
				INNER JOIN branch AS br ON stu.brid = br.brid
				INNER JOIN class AS cls ON stu.claid = cls.claid and cls.brid=stu.brid
				INNER JOIN section AS sec ON stu.secid = sec.secid and sec.brid=stu.brid
				INNER JOIN fee_category AS fc ON stu.fid = fc.fcid and fc.brid=stu.brid
				Left JOIN  user AS us ON us.uid=br.brid
				WHERE stu.active=$status $crit
				ORDER BY $orderby,br.name,cls.name,sec.name,stu.name limit 1000;");
		}
		return $result->result_array();
	}
	public function searchStudentAll($search,$type)
	{
		$crit = "";
		$activee = 1;

		$qry="SELECT stdid, stu.name AS 'student_name',stu.mobile,stu.address, stu.claid, stu.brid, stu.secid, stu.fid, stu.pid, cls.name AS 'class_name', br.name AS 'branch_name', cls.name AS 'class_name', fc.name AS 'fc_name', sec.name AS 'section_name'
		FROM student AS stu
		INNER JOIN branch AS br ON stu.brid = br.brid
		INNER JOIN class AS cls ON stu.claid = cls.claid and cls.brid=stu.brid
		INNER JOIN section AS sec ON stu.secid = sec.secid and sec.brid=stu.brid
		INNER JOIN fee_category AS fc ON stu.fid = fc.fcid and fc.brid=stu.brid
		WHERE stu.active = 1 AND  (stu.name LIKE '%". $search ."%' 
		OR stu.mobile LIKE '%". $search ."%'
		OR stu.feetype LIKE '%". $search ."%'
		OR stu.address LIKE '%". $search ."%'
		OR stu.city LIKE '%". $search ."%'
		
		OR stu.birthdate LIKE '%". $search ."%' ) limit 1000;";


		$query= $this->db->query($qry);
		$result = $query->result_array();
		return $result;
	}
	public function saveMultiple($studentsDetail, $brid) {


		$affect = 0;
		foreach ($studentsDetail as $studentDetail) {

			$this->db->where(array(
				'stdid' => $studentDetail['stdid'],
				'brid' => $brid
				));
			$result = $this->db->get('student');

			// if the result returned is greater than 0 then its mean the its already been added so update this
			if ($result->num_rows() > 0 ) {

				$this->db->where(array(
					'stdid' => $studentDetail['stdid'],
					'brid' => $brid
					));
				$affect = $this->db->update('student',$studentDetail);
			} else {	// if less than or equal to 0 then insert it
				$this->db->insert('student', $studentDetail);
				$affect = $this->db->affected_rows();
			}
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchAllKinships($stdid, $brid) {
		$result = $this->db->query("SELECT k.*, s.name FROM kinship k LEFT JOIN student s ON k.kstdid = s.stdid WHERE k.brid = $brid AND s.brid = $brid AND k.stdid = $stdid");
		return $result->result_array();
	}

	public function saveKinship($kinships, $brid, $stdid) {

		$this->db->where(array(
			'brid' => $brid,
			'stdid' => $stdid
			));
		$this->db->delete('kinship');

		foreach ($kinships as $ks) {			
			$this->db->insert('kinship', $ks);
		}

		return true;
	}

	public function save( $studentDetail, $pid, $brid) {

		if ( isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
			$studentDetail['photo'] = $this->upload_photo( $studentDetail );
		} else {
			unset($studentDetail['photo']);
		}
		$studentDetail['pid'] = $pid;
		
		$this->db->where(array(
			'stdid' => $studentDetail['stdid'],
			'brid' => $brid
			));
		$result = $this->db->get('student');

		$affect = 0;
		$stdid = "";
		if ($result->num_rows() > 0 ) {

			$this->db->where(array(
				'stdid' => $studentDetail['stdid'],
				'brid' => $brid
				));
			$affect = $this->db->update('student',$studentDetail);

			$stdid = $studentDetail['stdid'];
		} else {
			$this->db->insert('student', $studentDetail);
			$affect = $this->db->affected_rows();

			$stdid = $this->db->insert_id();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return $stdid;
		}
	}
	public function saveExecel( $studentDetail, $brid) {

		// if ( isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
		// 	$studentDetail['photo'] = $this->upload_photo( $studentDetail );
		// } else {
		// 	unset($studentDetail['photo']);
		// }
		// $studentDetail['pid'] = $pid;
		
		// $this->db->where(array(
		// 						'stdid' => $studentDetail['stdid'],
		// 						'brid' => $brid
		// 					));
		// $result = $this->db->get('student');

		// $affect = 0;
		// $stdid = "";
		// if ($result->num_rows() > 0 ) {

		// 	$this->db->where(array(
		// 						'stdid' => $studentDetail['stdid'],
		// 						'brid' => $brid
		// 					));
		// 	$affect = $this->db->update('student',$studentDetail);

		// 	$stdid = $studentDetail['stdid'];
		// } else {
		foreach ($studentDetail as $sdss) 
		{	
			$sd = (array) $sdss;
			$this->db->insert('student', $sd);
			$affect = $this->db->affected_rows();

			$stdid = $this->db->insert_id();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return $stdid;
		}
	}

	public function updateBatch( $studentDetail, $brid ) {

		$this->db->where(array(
			'stdid' => $studentDetail['stdid'],
			'brid' => $brid
			));
		$result = $this->db->get('student');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0 ) {

			$this->db->where(array(
				'stdid' => $studentDetail['stdid'],
				'brid' => $brid
				));
			$affect = $this->db->update('student',$studentDetail);
		} else {	// if less than or equal to 0 then insert it
			
			$this->db->insert('student', $studentDetail);
		}
		$affect = $this->db->affected_rows();

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchStudent( $stdid, $brid ) {

		$this->db->where(array(
			'stdid' => $stdid,
			'brid' => $brid
			));
		$result = $this->db->get('student');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	public function fetchAllStudent($brid) {

		$result = $this->db->query("SELECT    cls.name AS 'class_name', sec.name AS 'section_name',stu.*
			FROM student AS stu
			INNER JOIN class AS cls ON stu.claid = cls.claid and cls.brid=stu.brid
			INNER JOIN section AS sec ON stu.secid = sec.secid and sec.brid=stu.brid
			where stu.brid= $brid ");
		return $result->result_array();
		// $this->db->where(array(
		// 	'brid' => $brid
		// 	));
		// $result = $this->db->get('student');

		// if ( $result->num_rows() > 0 ) {
		// 	return $result->result_array();
		// } else {
		// 	return false;
		// }
	}
	public function fetchStudentAll($activee = -1)
	{
		$sts = '';
		if ($activee == -1) {
			$sts = '';
		} else {
			$sts = ' where stu.active=1 ';
		}

		$result = $this->db->query("SELECT stdid, stu.name AS 'student_name',stu.mobile,stu.address, stu.claid, stu.brid, stu.secid, stu.fid, stu.pid, cls.name AS 'class_name', br.name AS 'branch_name', cls.name AS 'class_name', fc.name AS 'fc_name', sec.name AS 'section_name'
			FROM student AS stu
			INNER JOIN branch AS br ON stu.brid = br.brid
			INNER JOIN class AS cls ON stu.claid = cls.claid
			INNER JOIN section AS sec ON stu.secid = sec.secid
			INNER JOIN fee_category AS fc ON stu.fid = fc.fcid $sts");



		return $result->result_array();
	}
	public function fetchAll($active = "", $brid) {

		if ($active == "") {
			$result = $this->db->query("Select stdid, stu.name as 'student_name', stu.claid, stu.brid, stu.secid, stu.fid, stu.pid, cls.name as 'class_name', br.name as 'branch_name', cls.name as 'class_name', fc.name as 'fc_name', sec.name as 'section_name' from student as stu
				inner join branch as br on stu.brid = br.brid 
				inner join class as cls on stu.claid = cls.claid inner join section as sec on stu.secid = sec.secid
				inner join fee_category as fc on stu.fid = fc.fcid
				where active = 1 AND stu.brid = $brid AND br.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND fc.brid = $brid");
		} else {
			$result = $this->db->query("select stdid, stu.name as 'student_name', stu.claid, stu.brid, stu.secid, stu.fid, stu.pid, cls.name as 'class_name', br.name as 'branch_name', cls.name as 'class_name', fc.name as 'fc_name', sec.name as 'section_name' from student as stu inner join branch as br on stu.brid = br.brid inner join class as cls on stu.claid = cls.claid inner join section as sec on stu.secid = sec.secid inner join fee_category as fc on stu.fid = fc.fcid WHERE stu.brid = $brid AND br.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND fc.brid = $brid");
		}
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchStudentByPid( $pid, $brid ) {

		$result = $this->db->query("SELECT stu.stdid, stu.name, stu.fname, stu.rollno, cls.name as 'class_name', stu.claid, sec.name as 'section_name', stu.secid, stu.brid from student as stu inner join class as cls on stu.claid = cls.claid inner join section as sec on stu.secid = sec.secid where active = 1 and pid = $pid  AND stu.brid = $brid AND cls.brid = $brid AND sec.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function getPreviousSchools($brid) {

		$result = $this->db->query("SELECT DISTINCT pschool FROM student WHERE pschool <> '' AND brid = $brid");
		return $result->result_array();
	}

	public function fetchStudentByBranchClassSection($brid, $claid, $secid) {

		$result = $this->db->query("SELECT stdid, student.name, fname, mobile, rollno, feetype, fee_category.name AS category
			FROM student
			INNER JOIN fee_category ON student.fid = fee_category.fcid
			WHERE student.brid = $brid AND student.claid = $claid AND student.secid = $secid AND fee_category.brid = $brid AND active = 1");

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}
	
	public function getStudentsForFeeIssue($brid, $claid, $fid,$from) {

		$result = $this->db->query("SELECT stdid, stu.name, stu.pid, feetype, stu.fid, stu.secid, sec.name AS 'section_name',stu.brid,ifnull(are.balance,0) as arears
			FROM student AS stu
			INNER JOIN section AS sec ON sec.secid = stu.secid
			left join(
			select ifnull(sum(debit),0)-ifnull(sum(credit),0) as balance,pid from pledger where date<='$from' group by pid
			) as are on are.pid=stu.pid

			WHERE active = 1 AND stu.claid = $claid AND fid = $fid AND stu.brid = $brid AND sec.brid = $brid ORDER BY sec.secid");


			// WHERE active = 1 AND stu.claid = $claid AND stu.brid = $brid AND sec.brid = $brid ORDER BY sec.secid");




		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function getStudentsForFeeConcession($brid, $claid, $fid,$from) {

		$result = $this->db->query("SELECT stu.stdid ,d.chno,d.concession
			from student stu
			INNER JOIN section AS sec ON sec.secid = stu.secid
			inner join concession_main m on m.stdid= stu.stdid and m.brid=stu.brid
			inner join concession_detail d on m.conid=d.conid 
			WHERE active = 1 AND stu.claid = $claid AND fid = $fid AND stu.brid = $brid AND sec.brid = $brid ORDER BY sec.secid");
		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}


	public function fetchStuReportByStatus($status, $brid, $claid, $secid) {

		$query = "";
		if ($status == '1') {

			if ($claid == '' && $secid == '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and active = $status group by student.brid, student.claid, student.stdid";
			} elseif ($claid != '' && $secid == '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and student.claid = $claid and active = $status group by student.brid, student.claid, student.stdid";
			} elseif ($claid != '' && $secid != '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and student.claid = $claid and student.secid = $secid and active = $status group by student.brid, student.claid, student.stdid";
			}

		} elseif ($status == '0') {

			if ($claid == '' && $secid == '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and active = $status group by student.brid, student.claid, student.stdid";
			} elseif ($claid != '' && $secid == '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and student.claid = $claid and active = $status group by student.brid, student.claid, student.stdid";
			} elseif ($claid != '' && $secid != '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and student.claid = $claid and student.secid = $secid and active = $status group by student.brid, student.claid, student.stdid";
			}
		} else {

			if ($claid == '' && $secid == '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid group by student.brid, student.claid, student.stdid";
			} elseif ($claid != '' && $secid == '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and student.claid = $claid group by student.brid, student.claid, student.stdid";
			} elseif ($claid != '' && $secid != '') {
				$query = "select stdid, student.name, fname, mobile, rollno, feetype, fee_category.name as category, cls.name as 'class_name', sec.name as 'section_name'from student inner join fee_category on student.fid = fee_category.fcid inner join class as cls on student.claid = cls.claid inner join section as sec on student.secid = sec.secid where student.brid = $brid and student.claid = $claid and student.secid = $secid group by student.brid, student.claid, student.stdid";
			}
		}

		$result = $this->db->query($query);


		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function admissionWithdrawlReport($brid) {

		$result = $this->db->query("SELECT stdid, stu.fname, stu.name AS 'student_name', DATE(stu.adddate) ADDDATE, DATE(stu.birthdate) birthdate, stu.focc, stu.address, stu.mobile, ROUND(stu.arears) arears, stu.remarks, DATE(struckoff) struckoff, cls.name AS 'class_name', clss.name as 'class_left' FROM student AS stu INNER JOIN branch AS br ON stu.brid = br.brid INNER JOIN class AS clss ON stu.adsecid = clss.claid INNER JOIN class AS cls ON stu.claid = cls.claid INNER JOIN section AS sec ON stu.secid = sec.secid INNER JOIN fee_category AS fc ON stu.fid = fc.fcid WHERE stu.active = 0 AND stu.brid = $brid AND br.brid = $brid AND clss.brid = $brid AND cls.brid = $brid AND sec.brid = $brid AND fc.brid = $brid");
		return $result->result_array();
	}
}

/* End of file students.php */
/* Location: ./application/models/students.php */