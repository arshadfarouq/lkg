<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subjects extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function fetchAll($brid) {

		$this->db->where(array('brid' => $brid ));
		$result = $this->db->get('subject');

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function getMaxId($brid) {

		$this->db->select_max('sbid');
		$this->db->where(array('brid' => $brid ));
		$result = $this->db->get('subject');

		$row = $result->row_array();
		$maxId = $row['sbid'];
		return $maxId;
	}
	public function isFeeAlreadyAssignedToClass($category) {
		$result = $this->db->query("SELECT * FROM subject WHERE sbid <> ". $category['sbid'] ." AND name = '". $category['name'] ."'");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function getMaxIdSubjectClass($brid) {

		$this->db->select_max('dcno');
		$this->db->where(array('brid' => $brid ));
		$result = $this->db->get('subject_class');

		$row = $result->row_array();
		$maxId = $row['dcno'];
		return $maxId;
	}

	public function getMaxIdSubjectTeacher($brid) {

		$this->db->select_max('dcno');
		$this->db->where(array('brid' => $brid ));
		$result = $this->db->get('sdetail');

		$row = $result->row_array();
		$maxId = $row['dcno'];
		return $maxId;
	}

	public function save( $subjectDetail, $brid ) {

		// check if the class is already saved or not
		$this->db->where(array(
			'sbid' => $subjectDetail['sbid'],
			'brid' => $brid
			));
		$result = $this->db->get('subject');

		$affect = 0;
		// if the result returned is greater than 0 then its mean the its already been added so update this
		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'sbid' => $subjectDetail['sbid'],
				'brid' => $brid
				));
			$result = $this->db->update('subject', $subjectDetail);
			$affect = $this->db->affected_rows();
		} else {	// if less than or equal to 0 then insert it
			$result = $this->db->insert('subject', $subjectDetail);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function saveAssignSubjecttoClass( $subjectAssignObj, $dcno, $brid ) {

		$this->db->where(array(
			'dcno' => $dcno,
			'brid' => $brid
			));
		$result = $this->db->get('subject_class');

		if ( $result->num_rows() > 0 ) {
			$this->db->where(array(
				'dcno' => $dcno,
				'brid' => $brid
				));
			$this->db->delete('subject_class');
		}

		$affect = 0;
		foreach ($subjectAssignObj as $dt) {

			$dt['dcno'] = $dcno;
			$this->db->insert('subject_class', $dt);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function saveAssignSubjecttoTeacher( $sdetail, $dcno, $brid ) {

		$this->db->where(array(
			'dcno' => $dcno,
			'brid' => $brid
			));
		$result = $this->db->get('sdetail');

		if ( $result->num_rows() > 0 ) {
			$this->db->where(array(
				'dcno' => $dcno,
				'brid' => $brid
				));
			$this->db->delete('sdetail');
		}

		$affect = 0;
		foreach ($sdetail as $sd) {

			$sd['dcno'] = $dcno;
			$this->db->insert('sdetail', $sd);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchSubject($sbid, $brid) {

		$this->db->where(array(
			'sbid' => $sbid,
			'brid' => $brid
			));
		$result = $this->db->get('subject');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAssignedSubjectstoClass($dcno, $brid) {

		$result = $this->db->query("SELECT sc.`dcno`, sc.`sbid`, sc.`claid`, sc.`brid`, sub.name FROM `subject_class` AS sc INNER JOIN `subject` AS sub ON sc.sbid = sub.sbid WHERE dcno = $dcno AND sc.brid = $brid AND sub.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAssignedSubjectstoTeacher($dcno, $brid) {

		$result = $this->db->query("SELECT sd.`dcno`, sd.`sbid`, sd.`claid`, sd.`brid`, sub.name AS 'subject_name', sd.secid, stf.name AS 'teacher_name', sd.staid FROM `sdetail` AS sd INNER JOIN `subject` AS sub ON sd.sbid = sub.sbid INNER JOIN staff AS stf ON sd.staid = stf.staid WHERE dcno = $dcno AND sd.brid = $brid AND sub.brid = $brid AND stf.brid = $brid");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function deleteVoucher($dcno, $brid) {

		$this->db->where(array(
			'dcno' => $dcno,
			'brid' => $brid
			));
		$result = $this->db->get('subject_class');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'dcno' => $dcno,
				'brid' => $brid
				));
			$result = $this->db->delete('subject_class');

		} else {
			return false;
		}
	}

	public function deleteVoucherST($dcno, $brid) {

		$this->db->where(array(
			'dcno' => $dcno,
			'brid' => $brid
			));
		$result = $this->db->get('sdetail');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
				'dcno' => $dcno,
				'brid' => $brid
				));
			$result = $this->db->delete('sdetail');

		} else {
			return false;
		}
	}

	public function subjectViewReport($brid) {

		$result = $this->db->query("SELECT sc.dcno, cls.name AS 'class_name', sbj.name AS 'subject_name', br.name AS 'branch_name'FROM (subject_class AS sc INNER JOIN (branch AS br INNER JOIN class AS cls ON br.brid=cls.brid) ON sc.claid=cls.claid) INNER JOIN subject AS sbj ON sc.sbid=sbj.sbid WHERE sc.brid = $brid AND br.brid = $brid AND cls.brid = $brid AND sbj.brid = $brid ORDER BY br.name, cls.name");
		return $result->result_array();
	}

	public function subjectViewClassWiseReport($brid) {

		$result = $this->db->query("SELECT br.name AS 'branch_name', cls.name AS 'class_name', sbj.name AS 'subject_name' FROM subject_class sc INNER JOIN branch AS br ON sc.brid=br.brid INNER JOIN class AS cls ON sc.claid=cls.claid INNER JOIN subject AS sbj ON sc.sbid=sbj.sbid WHERE sc.brid = $brid AND br.brid = $brid AND cls.brid = $brid AND sbj.brid = $brid ORDER BY br.name, cls.name");
		return $result->result_array();
	}

	public function subjectViewTeacherWiseReport($brid) {

		$result = $this->db->query("SELECT br.name AS 'branch_name', cls.name AS 'class_name', stf.name AS 'teacher_name', sd.dcno, sec.name AS 'section_name', sbj.name AS 'subject_name' FROM (((((sdetail AS sd INNER JOIN section AS sec ON sd.secid=sec.secid) INNER JOIN staff AS stf ON sd.staid=stf.staid) INNER JOIN subject AS sbj ON sd.sbid=sbj.sbid) INNER JOIN branch AS br ON sd.brid=br.brid) INNER JOIN class AS cls ON sec.claid=cls.claid) WHERE sd.brid = $brid AND sec.brid = $brid AND stf.brid = $brid AND sbj.brid = $brid AND br.brid = $brid AND cls.brid = $brid ORDER BY stf.name, br.name, cls.name, sec.name");
		return $result->result_array();
	}

	public function fetchSubjectByBrNCls( $brid, $claid ) {

		$result = $this->db->query("SELECT sc.sbid, sbj.name, sc.claid, sc.brid FROM subject_class AS sc INNER JOIN subject AS sbj ON sc.sbid = sbj.sbid WHERE sc.brid = $brid AND sc.claid = $claid AND sbj.brid = $brid");

		if( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchTeacherNameByBrClsSecNSbj( $brid, $claid, $secid, $sbid ) {

		$result = $this->db->query("SELECT sd.staid, stf.name AS 'teacher_name', sbj.name AS 'subject_name'FROM sdetail AS sd INNER JOIN staff AS stf ON sd.staid = stf.staid INNER JOIN subject AS sbj ON sbj.sbid = sd.sbid WHERE sd.brid = $brid AND sd.claid = $claid AND sd.secid = $secid AND sd.sbid = $sbid");

		if( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function isSubjAlreadyAssignedToClass($dcno, $claid, $brid) {

		$result = $this->db->query("SELECT * FROM subject_class WHERE brid = $brid AND claid = $claid AND dcno <> $dcno");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function isSubjAlreadyAssignedToTeacher($dcno, $claid, $secid, $brid) {

		$result = $this->db->query("SELECT * FROM sdetail WHERE brid = $brid AND claid = $claid AND secid = $secid AND dcno <> $dcno");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
}

/* End of file subjects.php */
/* Location: ./application/models/subjects.php */