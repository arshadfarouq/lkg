<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxRoleGroupId() {

		$this->db->select_max('rgid');
		$result = $this->db->get('rolegroup');
		$result = $result->row_array();
		return $result['rgid'];
	}

	public function getMaxId() {

		$this->db->select_max('uid');
		$result = $this->db->get('user');
		$result = $result->row_array();
		return $result['uid'];
	}
public function fetchAll() {

		// $this->db->where(array('uid' => $uid));
		$result = $this->db->get('user');
			return $result->result_array();
		
	}
	public function saveRoleGroup( $rolegroup ) {

		$this->db->where(array('rgid' => $rolegroup['rgid']));
		$result = $this->db->get('rolegroup');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('rgid' => $rolegroup['rgid'] ));
			$result = $this->db->update('rolegroup', $rolegroup);
			$affect = $this->db->affected_rows();
		} else {

			unset($rolegroup['rgid']);
			$result = $this->db->insert('rolegroup', $rolegroup);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function save( $user ) {

		$this->db->where(array('uid' => $user['uid']));
		$result = $this->db->get('user');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('uid' => $user['uid'] ));
			$result = $this->db->update('user', $user);
			$affect = $this->db->affected_rows();
		} else {

			unset($user['uid']);
			$user['date'] = date('Y-m-d H:i:s');
			$result = $this->db->insert('user', $user);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}

	public function fetchRoleGroup( $rgid ) {

		$this->db->where(array('rgid' => $rgid));
		$result = $this->db->get('rolegroup');
		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetch( $uid ) {

		$this->db->where(array('uid' => $uid));
		$result = $this->db->get('user');
		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchAllUsers() {
		$result = $this->db->query("SELECT u.*, b.name AS 'branch_name', r.name AS 'role_name' FROM user u INNER JOIN branch b ON u.brid = b.brid INNER JOIN rolegroup r ON r.rgid = u.rgid");
		return $result->result_array();
	}

	public function fetchAllRoleGroup() {
		$result = $this->db->get('rolegroup');
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function privillagesAssigned() {

		$result = $this->db->query("SELECT u.uid, u.fullname, r.name, r.desc, DATE(u.date) AS date FROM user AS u INNER JOIN rolegroup AS r ON u.rgid = r.rgid");
		return $result->result_array();
	}

	public function login_user($data) {

		unset($data['submit']);

		$username = $this->db->escape_str($data['uname']);
		$password = $this->db->escape_str($data['pass']);

		if ($this->has_match($username, $password)) {

			$result = $this->db->query("SELECT u.uid, u.fullname, u.uname, r.rgid, r.name AS 'rolegroup_name', r.desc, u.`type`, u.brid, b.name as branch_name, b.address as branch_address,b.phone_no as branch_phone_no,u.company_id FROM user AS u INNER JOIN rolegroup AS r ON r.rgid = u.rgid INNER JOIN branch AS b ON b.brid = u.brid WHERE u.uname = '". $username ."' AND u.pass = '". $password ."'");

			$result = $result->row_array();
			$this->session->set_userdata($result);
		}
		else{
			return false;
		}
	}

	public function has_match($username, $password)
	{
		$query = $this->db->query("SELECT * FROM user WHERE BINARY uname='{$username}' AND BINARY pass='{$password}'");
		return ($query->num_rows() > 0);
	}
}

/* End of file users.php */
/* Location: ./application/models/users.php */