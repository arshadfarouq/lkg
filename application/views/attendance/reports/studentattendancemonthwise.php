<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Student Attendance Month Wise</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-2">
                                    <label for="">From</label>
                                    <input class="form-control ts_datepicker datepicker" type="text" id="from_date">
	                            </div>

	                            <div class="col-lg-2">
                                    <label for="">To</label>
                                    <input class="form-control ts_datepicker datepicker" type="text" id="to_date">
	                            </div>
							</div>

							<div class="row">
								<div class="col-lg-3">
									<label for="">Class</label>
									<select class="form-control" id="class_dropdown">
	                                    <option value="" disabled="" selected="">Choose class</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Section</label>
									<select class="form-control" id="section_dropdown">
	                                    <option value="" disabled="" selected="">Choose section</option>
	                                </select>
								</div>
								<div class="col-lg-2">
									<label for="">Std. Id</label>
									<select class="form-control" id="stdid_dropdown">
	                                    <option value="" disabled="" selected="">Choose student Id</option>
	                                </select>
								</div>
								<div class="col-lg-4" style='margin-top: 25px;'>
									<div class="pull-right">
										<a href='' class="btn btn-primary btnSearch">
	              							<i class="fa fa-search"></i>
	            						Search</a>
	            						<a href="" class="btn btn-warning btnReset">
					                      	<i class="fa fa-refresh"></i>
					                    Reset</a>
					                    <button type="button" class="btn btn-info  btnPrint">Print</button>
				                    </div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover center" id="atnd-table">
								<thead>
									<tr>
										<th rowspan='2' class='txtcenter level4row'>Student Name</th>
										<th colspan='3' class='txtcenter' style='background: #fff;'>January</th>
										<th colspan='3' class='txtcenter' style='background: #e6f3fd;'>February</th>
										<th colspan='3' class='txtcenter' style='background: #fff;'>March</th>
										<th colspan='3' class='txtcenter' style='background: #e6f3fd;'>April</th>
										<th colspan='3' class='txtcenter' style='background: #fff;'>May</th>
										<th colspan='3' class='txtcenter' style='background: #e6f3fd;'>June</th>
										<th colspan='3' class='txtcenter' style='background: #fff;'>July</th>
										<th colspan='3' class='txtcenter' style='background: #e6f3fd;'>August</th>
										<th colspan='3' class='txtcenter' style='background: #fff;'>Septemder</th>
										<th colspan='3' class='txtcenter' style='background: #e6f3fd;'>October</th>
										<th colspan='3' class='txtcenter' style='background: #fff;'>November</th>
										<th colspan='3' class='txtcenter' style='background: #e6f3fd;'>December</th>
									</tr>
									<tr>
										<th class='txtcenter' style='background: #fff;'>A</th>
										<th class='txtcenter' style='background: #fff;'>P</th>
										<th class='txtcenter' style='background: #fff;'>L</th>

										<th class='txtcenter' style='background: #e6f3fd;'>A</th>
										<th class='txtcenter' style='background: #e6f3fd;'>P</th>
										<th class='txtcenter' style='background: #e6f3fd;'>L</th>

										<th class='txtcenter' style='background: #fff;'>A</th>
										<th class='txtcenter' style='background: #fff;'>P</th>
										<th class='txtcenter' style='background: #fff;'>L</th>

										<th class='txtcenter' style='background: #e6f3fd;'>A</th>
										<th class='txtcenter' style='background: #e6f3fd;'>P</th>
										<th class='txtcenter' style='background: #e6f3fd;'>L</th>

										<th class='txtcenter' style='background: #fff;'>A</th>
										<th class='txtcenter' style='background: #fff;'>P</th>
										<th class='txtcenter' style='background: #fff;'>L</th>

										<th class='txtcenter' style='background: #e6f3fd;'>A</th>
										<th class='txtcenter' style='background: #e6f3fd;'>P</th>
										<th class='txtcenter' style='background: #e6f3fd;'>L</th>

										<th class='txtcenter' style='background: #fff;'>A</th>
										<th class='txtcenter' style='background: #fff;'>P</th>
										<th class='txtcenter' style='background: #fff;'>L</th>

										<th class='txtcenter' style='background: #e6f3fd;'>A</th>
										<th class='txtcenter' style='background: #e6f3fd;'>P</th>
										<th class='txtcenter' style='background: #e6f3fd;'>L</th>

										<th class='txtcenter' style='background: #fff;'>A</th>
										<th class='txtcenter' style='background: #fff;'>P</th>
										<th class='txtcenter' style='background: #fff;'>L</th>

										<th class='txtcenter' style='background: #e6f3fd;'>A</th>
										<th class='txtcenter' style='background: #e6f3fd;'>P</th>
										<th class='txtcenter' style='background: #e6f3fd;'>L</th>

										<th class='txtcenter' style='background: #fff;'>A</th>
										<th class='txtcenter' style='background: #fff;'>P</th>
										<th class='txtcenter' style='background: #fff;'>L</th>

										<th class='txtcenter' style='background: #e6f3fd;'>A</th>
										<th class='txtcenter' style='background: #e6f3fd;'>P</th>
										<th class='txtcenter' style='background: #e6f3fd;'>L</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>