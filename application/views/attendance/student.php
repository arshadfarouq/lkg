<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<div id="main_wrapper">

  	<div class="page_bar">
    	<div class="row">
      		<div class="col-lg-6">
        		<h1 class="page_title">Student Attendance</h1>
        		<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
      		</div>
      		<div class="col-lg-6">
				<div class="pull-right">
					<a href='' class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['student_attendance']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a href='' class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
				</div>
			</div> 	<!-- end of col -->
    	</div>
  	</div>

  	<div class="page_content">
    	<div class="container-fluid">

    		<div class="row">
    			<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-1">
                                    <label for="">DCNO</label>
                                    <input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['student_attendance']['update']; ?>' id="txtdcno">
                                    <input type="hidden" id="txtMaxdcnoHidden">
                                    <input type="hidden" id="txtdcnoHidden">
								</div>

								<div class="col-lg-2">
                                    <label for="">Date</label>
                                    <input class="form-control ts_datepicker" type="text" id="current_date">
								</div>
								<div class="col-lg-3">
									<label for="">Class</label>
									<select class="form-control" id="class_dropdown">
						                <option value="" disabled="" selected="">Choose class</option>
						            </select>
								</div>
								<div class="col-lg-3">
									<label for="">Section</label>
									<select class="form-control" id="section_dropdown">
						                <option value="" disabled="" selected="">Choose section</option>
						            </select>
								</div>
								<div class="col-lg-3" style='margin-top: 24px;'>
									<div class="pull-right">
										<a href='' class="btn btn-success btnSearch">
	              							<i class="fa fa-search"></i>
	            						Search</a>
									</div>
								</div>
							</div>

						</div>
					</div>

    			</div>	<!-- end of col-lg-12 -->
    		</div>	<!-- end of row -->

    		<div class="row">
    			<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover" id='atnd-table'>
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Std Id</th>
										<th>Name</th>
										<th>Father Name</th>
										<th>Status</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>

						</div>
					</div>

					<datalist id='status'>
						<option value="Present">
						<option value="Absent">
						<option value="Leave">
					</datalist>

    			</div>
    		</div>

    		<div class="row">
				<div class="col-lg-12">
					<div class="pull-right">
						<a href='' class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['student_attendance']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
						<a href='' class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
					</div>
				</div> 	<!-- end of col -->
			</div>	<!-- end of row -->

    	</div>	<!-- end of container-fluid -->
    </div>	<!-- end of page_content -->
</div>