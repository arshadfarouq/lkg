<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

  	<div class="page_bar">
    	<div class="row">
      		<div class="col-md-12">
        		<h1 class="page_title">Add Result</h1>
        		<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
      		</div>
    	</div>
  	</div>

  	<div class="page_content">
    	<div class="container-fluid">

			<div class="row">
	      		<div class="col-md-8">

					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-3">
		                          	<div class="input-group">
		                            	<span class="input-group-addon">Id</span>
		                            	<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['add_result']['update']; ?>' id="txtdcno">
	                                    <input type="hidden" id="txtMaxdcnoHidden">
	                                    <input type="hidden" id="txtdcnoHidden">
			                        </div>
								</div>
							</div>

							<div class="row">

								<div class="col-lg-6">
									<div class="input-group">
										<span class="input-group-addon">Class</span>
										<select class="form-control" id="class_dropdown">
		                                    <option value="" disabled="" selected="">Choose class</option>
		                                </select>
									</div>
								</div>

								<div class="col-lg-6">
									<div class="input-group">
										<span class="input-group-addon">Section</span>
										<select class="form-control" id="section_dropdown">
		                                    <option value="" disabled="" selected="">Choose section</option>
		                                </select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="input-group">
										<span class="input-group-addon">Subject</span>
										<select class="form-control" id="subject_dropdown">
		                                  	<option value="" disabled="" selected="">Choose Subject</option>
		                                </select>
									</div>
								</div>

								<div class="col-lg-6">
									<div class="input-group">
										<span class="input-group-addon">Teacher</span>
										<input type="text" class="form-control" id="txtTeacherName" data-staid='' readonly>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
		                          	<div class="input-group">
		                            	<span class="input-group-addon">Total Marks</span>
		                            	<input type="text" class="form-control num" id="txtTotalMarks">
			                        </div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="pull-right">
										<a href='' class="btn btn-default btnSearch">
		          							<i class="fa fa-search"></i>
		        						Search</a>
									</div>
								</div>
							</div>

						</div>
					</div>

	      		</div>

	      		<div class="col-lg-4">
	      			<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">Date</span>
                                        <input class="form-control ts_datepicker" type="text" id="current_date">
                                    </div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
		                          	<div class="input-group">
		                            	<span class="input-group-addon">Session</span>
		                            	<input type="text" class="form-control" id="txtSession">
			                        </div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
		                          	<div class="input-group">
		                            	<span class="input-group-addon">Term</span>
										<select class="form-control" id="term_dropdown">
		                                    <option value="" disabled="" selected="">Choose term</option>
		                                    <option value="sem-1">Sem-1</option>
		                                    <option value="sem-2">Sem-2</option>
		                                    <option value="sem-3">Sem-3</option>
		                                    <option value="annual">Annual</option>
		                                </select>
			                        </div>
								</div>
							</div>
						</div>
					</div>
	      		</div>
			</div>

			<div class="row">
				<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover ar-datatable" id="exam-table">
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Stdid</th>
										<th>Student</th>
										<th>O.Marks</th>
										<th>Percentage</th>
										<th>Status</th>
										<th>Remarks</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>

						</div>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="pull-right">
						<a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['add_result']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
						<a href='' class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['add_result']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
						<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
					</div>
				</div> 	<!-- end of col -->
			</div>	<!-- end of row -->

    	</div>  <!-- end of container fluid -->

  	</div>   <!-- end of page_content -->
</div>

<div id="TypeModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Type</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Type</span>
	        <input type="text" class="form-control" id="txtNewType">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewType"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>