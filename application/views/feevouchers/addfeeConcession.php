<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-6">
				<h1 class="page_title">Fee Concession Voucher</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>

				<input type="hidden" class='voucher_type_hidden' value='new'>

			</div>
			<div class="col-lg-6">
				<div class="pull-right">
					<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['fee_concession']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['fee_concession']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
					<a class="btn btn-info  btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
					<a href="#student-lookup" data-toggle="modal" class="btn btn-lg btn-default btnsearchstudent "><i class="fa fa-search"></i>&nbsp;Student F1</a>


				</div>
			</div> 	<!-- end of col -->
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<form action="">

										<div class="row">
											<div class="col-lg-1">
												<label for="">Id</label>
												<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['fee_concession']['update']; ?>' id="txtdcno">
												<input type="hidden" id="txtMaxdcnoHidden">
												<input type="hidden" id="txtdcnoHidden">
											</div>
											<div class="col-lg-1"></div>


											<div class="col-lg-2">
												<label for="">Current Date</label>
												<input class="form-control ts_datepicker" type="text" id="current_date">
											</div>

											<div class="col-lg-2">
												<label for="">Valid From</label>
												<input class="form-control ts_datepicker" type="text" id="from_date">
											</div>

											<div class="col-lg-2">
												<label for="">To Date</label>
												<input class="form-control ts_datepicker" type="text" id="to_date">
											</div>



										</div>

										<div class="row">
											<div class="col-lg-1">
												<label for="">Sudent Id</label>
												<select class="form-control" id="stdid_dropdown">
													<option value="" disabled="" selected="">Choose Student Id</option>
													<?php foreach ($students as $student): ?>
														<option value="<?php echo $student['stdid']; ?>" data-section_name = "<?php echo $student['section_name']; ?>" data-stdid='<?php echo $student['stdid']; ?>' data-student_name='<?php echo $student['student_name']; ?>' data-claid='<?php echo $student['claid']; ?>' data-brid='<?php echo $student['brid']; ?>' data-secid='<?php echo $student['secid']; ?>' data-fid='<?php echo $student['fid']; ?>' data-pid='<?php echo $student['pid']; ?>' data-class_name='<?php echo $student['class_name']; ?>' data-branch_name='<?php echo $student['branch_name']; ?>' data-fc_name='<?php echo $student['fc_name']; ?>'><?php echo $student['stdid']; ?></option>
													<?php endforeach ?>
												</select>
											</div>
											<div class="col-lg-2">
												<label for="">Student Name</label>
												<input type="text" class="form-control" id="txtStudentName" disabled='disabled'>
											</div>

											<div class="col-lg-2">
												<label for="">Branch</label>
												<input type="text" class="form-control" id="txtBranchName" disabled='disabled'>
											</div>

											<div class="col-lg-1">
												<label for="">Class</label>
												<input type="text" class="form-control" id="txtClassName" disabled='disabled'>
											</div>

											<div class="col-lg-1">
												<label for="">Section</label>
												<input type="text" class="form-control" id="txtSectionName" disabled='disabled'>
											</div>
											<div class="col-lg-2">
												<label for="">Fee Category</label>
												<input type="text" class="form-control" id="txtFeeCategory" disabled='disabled'>
											</div>

											<div class="col-lg-3">
												<label for="">Remarks</label>
												<input type="text" class="form-control" id="txtRemarks" >
											</div>

										</div>

										

										<div class="row"></div>
										<div class="container-wrap">
											<div class="row">
												<div class="col-lg-3">
													<label for="">Particulars</label>
													<select class="form-control select2" id="particulars_dropdown">
														<option value="" disabled="" selected="">Choose particular</option>
														<?php foreach ($charges as $charge): ?>
															<option value="<?php echo $charge['chid']; ?>" data-amount="<?php echo $charge['charges']; ?>" data-pid="<?php echo $charge['pid']; ?>" data-chid="<?php echo $charge['chid']; ?>"><?php echo $charge['description']; ?></option>
														<?php endforeach ?>
													</select>
												</div>
												<div class="col-lg-1">
													<label for="">Charges</label>
													<input type="text" class="form-control num" id="txtCharges">
												</div>

												<div class="col-lg-1">
													<label for="">Concession%</label>
													<input type="text" class="form-control num" id="txtConcession">
												</div>

												<div class="col-lg-2 hide">
													<label for="">Total</label>
													<input type="text" class="form-control num" id="txtTotal" disabled="disabled">
												</div>

												<div class="col-lg-1" style='margin-top: 26px;'>
													<a href="" class="btn btn-primary" id="btnAddCharges">+</a>
												</div>
											</div>
										</div>

										<div class="row"></div>
										<div class="row">
											<div class="col-lg-8">
												<table class="table table-striped" id="charges_table">
													<thead>
														<tr>
															<th>Sr#</th>                        
															<th>Particulars</th>
															<th class='text-right'>Amount</th>
															<th class='text-right'>Concession%</th>
															<th class="hide" class='text-right'>Net Amount</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody >

														<tr class="footer hide">
															<td></td>
															<td>Net Amount</td>
															<td id="txtTotalAmount">----</td>
															<td id="txtTotalConcession">----</td>
															<td class="hide" id="txtTotalNetAmount">----</td>
															<td></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-12">
												<div class="pull-right">
													<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['fee_concession']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
													<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['fee_concession']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
													<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
													<a class="btn btn-info  btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
													
													<a href="#student-lookup" data-toggle="modal" class="btn btn-lg btn-default btnsearchstudent "><i class="fa fa-search"></i>&nbsp;Student F1</a>

												</div>
											</div> 	<!-- end of col -->
										</div>	<!-- end of row -->

										<div class="row">


											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">User: </span>
													<input type="text" class=" form-control"  id="txtUserName" >

												</div>
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Posting: </span>
													<input type="text" class=" form-control"  id="txtPostingDate" >

												</div>
											</div>                                             
										</div>



									</form>	<!-- end of form -->

								</div>	<!-- end of panel-body -->
							</div>	<!-- end of panel -->
						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>

<div id="student-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h3 id="myModalLabel">Student Lookup</h3>
			</div>

			<div class="modal-body">
				<table class="table table-striped modal-table" id ="tblStudents">

					<thead>
						<tr style="font-size:16px;">
							<th>Id</th>
							<th>Name</th>
							<th>Mobile</th>
							<th>Address</th>
							<th>Class</th>
							<th>Section</th>
							<th style='width:3px;'>Actions</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
			<div class="modal-footer">

				<button class="btn btn-primary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
