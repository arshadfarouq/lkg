<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-6">
				<h1 class="page_title">Assign Fee to Class</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
			<div class="col-lg-6">
				<div class="pull-right">
					<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['assign_fee_to_class']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['assign_fee_to_class']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
				</div>
			</div> 	<!-- end of col -->
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<form action="">

										<div class="row">
											<div class="col-lg-1">
												<label for="">Id</label>
												<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['assign_fee_to_class']['update']; ?>' id="txtFeeAssignId">
												<input type="hidden" id="txtMaxFeeAssignIdHidden">
												<input type="hidden" id="txtFeeAssignIdHidden">
											</div>
											<div class="col-lg-3">
        										<label for="">Class</label>
        										<select class="form-control" id="class_dropdown">
				                                    <option value="" disabled="" selected="">Choose class</option>
				                                </select>
											</div>
											<div class="col-lg-3">
        										<label for="">Fee Category</label>
        										<select class="form-control" id="feeCategory_dropdown">
				                                    <option value="" disabled="" selected="">Choose class</option>
				                                    <?php foreach ($feeCategories as $key => $feeCategory): ?>
				                                    	<option value="<?php echo $feeCategory['fcid']; ?>"><?php echo $feeCategory['name']; ?></option>
				                                    <?php endforeach ?>
				                                </select>
											</div>
										</div>

										<div class="row"></div>
										<div class="container-wrap">
											<div class="row">
					                            <div class="col-lg-4">
				                                  	<label for="">Particulars</label>
				                                  	<select class="form-control" id="particulars_dropdown">
				                                      	<option value="" disabled="" selected="">Choose particular</option>
				                                      	<?php foreach ($charges as $charge): ?>
				                                          	<option value="<?php echo $charge['chid']; ?>" data-amount="<?php echo $charge['charges']; ?>" data-chid="<?php echo $charge['chid']; ?>"><?php echo $charge['description']; ?></option>
				                                      	<?php endforeach ?>
				                                  	</select>
					                            </div>
						                        <div class="col-lg-3">
					                                <label for="">Charges</label>
					                                <input type="text" class="form-control num" id="txtCharges">
												</div>
						                        <div class="col-lg-1" style='margin-top: 26px;'>
													<a href="" class="btn btn-primary" id="btnAddCharges">+</a>
						                        </div>
					                    	</div>
										</div>
										<div class="row"></div>


										<div class="row">
				                            <div class="col-lg-12">
				                                <table class="table table-striped" id="charges_table">
				                                    <thead>
				                                        <tr>
				                                            <th>Particulars</th>
				                                            <th>Charges</th>
				                                            <th>Action</th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>

				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>

										<div class="row">
											<div class="col-lg-12">
												<div class="pull-right">
													<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['assign_fee_to_class']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
													<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['assign_fee_to_class']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
													<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
												</div>
											</div> 	<!-- end of col -->
										</div>	<!-- end of row -->


									</form>	<!-- end of form -->

								</div>	<!-- end of panel-body -->
							</div>	<!-- end of panel -->
						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>