<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-6">
				<h1 class="page_title">Admission Charges</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
			<div class="col-lg-6">
				<div class="pull-right">
					<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['admission_charges']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['admission_charges']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
				</div>
			</div> 	<!-- end of col -->
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<form action="">

										<div class="row">
											<div class="col-lg-1">
												<label for="">Id</label>
												<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['admission_charges']['update']; ?>' id="txtdcno">
												<input type="hidden" id="txtMaxdcnoHidden">
												<input type="hidden" id="txtdcnoHidden">
											</div>
										</div>

										<div class="row">
											<div class="col-lg-3">
        										<label for="">Sudent Id</label>
        										<select class="form-control" id="stdid_dropdown">
					                                <option value="" disabled="" selected="">Choose Student Id</option>
					                                <?php foreach ($students as $student): ?>
					                                    <option value="<?php echo $student['stdid']; ?>" data-section_name = "<?php echo $student['section_name']; ?>" data-stdid='<?php echo $student['stdid']; ?>' data-student_name='<?php echo $student['student_name']; ?>' data-claid='<?php echo $student['claid']; ?>' data-brid='<?php echo $student['brid']; ?>' data-secid='<?php echo $student['secid']; ?>' data-fid='<?php echo $student['fid']; ?>' data-pid='<?php echo $student['pid']; ?>' data-class_name='<?php echo $student['class_name']; ?>' data-branch_name='<?php echo $student['branch_name']; ?>' data-fc_name='<?php echo $student['fc_name']; ?>'><?php echo $student['stdid']; ?></option>
					                                <?php endforeach ?>
					                            </select>
											</div>
											<div class="col-lg-3">
        										<label for="">Student</label>
        										<input type="text" class="form-control" id="txtStudentName" >
											</div>
										</div>

										<div class="row">
											<div class="col-lg-3">
        										<label for="">Branch</label>
        										<input type="text" class="form-control" id="txtBranchName" disabled='disabled'>
											</div>

											<div class="col-lg-3">
        										<label for="">Class</label>
        										<input type="text" class="form-control" id="txtClassName" disabled='disabled'>
											</div>

											<div class="col-lg-3">
        										<label for="">Section</label>
        										<input type="text" class="form-control" id="txtSectionName" disabled='disabled'>
											</div>
											<div class="col-lg-3">
        										<label for="">Fee Category</label>
        										<input type="text" class="form-control" id="txtFeeCategory" disabled='disabled'>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-2">
		                                    	<label for="">Current Date</label>
		                                    	<input class="form-control ts_datepicker" type="text" id="current_date">
			                                </div>

			                                <div class="col-lg-2">
		                                    	<label for="">Last Date</label>
		                                    	<input class="form-control ts_datepicker" type="text" id="last_date">
			                                </div>

			                                <div class="col-lg-1">
        										<label for="">Late Fee</label>
        										<input type="text" class="form-control num" id="txtLateFee" >
											</div>
										</div>

										<div class="row"></div>
										<div class="container-wrap">
											<div class="row">
					                            <div class="col-lg-3">
				                                  	<label for="">Particulars</label>
				                                  	<select class="form-control" id="particulars_dropdown">
				                                      	<option value="" disabled="" selected="">Choose particular</option>
				                                      	<?php foreach ($charges as $charge): ?>
				                                          	<option value="<?php echo $charge['chid']; ?>" data-amount="<?php echo $charge['charges']; ?>" data-pid="<?php echo $charge['pid']; ?>" data-chid="<?php echo $charge['chid']; ?>"><?php echo $charge['description']; ?></option>
				                                      	<?php endforeach ?>
				                                  	</select>
					                            </div>
						                        <div class="col-lg-3">
					                                <label for="">Charges</label>
					                                <input type="text" class="form-control num" id="txtCharges">
												</div>

												<div class="col-lg-2">
					                                <label for="">Concession</label>
					                                <input type="text" class="form-control num" id="txtConcession">
												</div>

												<div class="col-lg-2">
					                                <label for="">Total</label>
					                                <input type="text" class="form-control num" id="txtTotal" disabled="disabled">
												</div>

						                        <div class="col-lg-1" style='margin-top: 26px;'>
													<a href="" class="btn btn-primary" id="btnAddCharges">+</a>
						                        </div>
					                    	</div>
										</div>

										<div class="row"></div>
										<div class="row">
				                            <div class="col-lg-12">
				                                <table class="table table-striped" id="charges_table">
				                                    <thead>
				                                        <tr>
				                                            <th>Particulars</th>
				                                            <th>Amount</th>
				                                            <th>Concession</th>
				                                            <th>Net Amount</th>
				                                            <th>Action</th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>

				                                          <tr class="footer">
					                                          	<td>Net Amount</td>
					                                            <td id="txtTotalAmount">----</td>
					                                            <td id="txtTotalConcession">----</td>
					                                            <td id="txtTotalNetAmount">----</td>
					                                            <td></td>
				                                          </tr>
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>

										<div class="row">
											<div class="col-lg-12">
												<div class="pull-right">
													<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['admission_charges']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
													<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['admission_charges']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
													<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
												</div>
											</div> 	<!-- end of col -->
										</div>	<!-- end of row -->


									</form>	<!-- end of form -->

								</div>	<!-- end of panel-body -->
							</div>	<!-- end of panel -->
						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>