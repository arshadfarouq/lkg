<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-6">
				<h1 class="page_title">Monthly Fee Issuance</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='company_id' value='<?php echo $this->session->userdata('company_id'); ?>'>

				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
			<div class="col-lg-6">
				<div class="pull-right">
                	<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['monthly_fee_issuance']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
                    <a href="" class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['monthly_fee_issuance']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
                    <a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
                     <a class="btn btn-info btn-lg btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
                </div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">

                            	<div class="col-lg-3">
                                	<label for="">From Month</label>
                                	<input class="form-control ts_datepicker" type="text" id="fromMonth_date">
                                </div>

                                <div class="col-lg-3">
                                	<label for="">To Month</label>
                                	<input class="form-control ts_datepicker" type="text" id="toMonth_date">
                                </div>

                                <div class="col-lg-3">
                                	<label for="">Last Date</label>
                                	<input class="form-control ts_datepicker" type="text" id="last_date">
                                </div>

								<div class="col-lg-3">
                                	<label for="">Current Date</label>
                                	<input class="form-control ts_datepicker" type="text" id="current_date">
                                </div>
							</div>

							<div class="row">
								<div class="col-lg-1">
                                  	<label for="">Vr#</label>
                                  	<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['monthly_fee_issuance']['update']; ?>' id="txtdcno">
                                  	<input type="hidden" id="txtMaxdcnoHidden">
                                  	<input type="hidden" id="txtdcnoHidden">
								</div>

								<div class="col-lg-1 hide">
                                  	<label for="">Branch Name</label>
                                  	<input type="text" class="form-control "  id="txtbranchname" value='<?php echo $this->session->userdata('branch_name'); ?>'>
                                  	
								</div>
								<div class="col-lg-1 hide">
                                  	<label for="">Branch Address</label>
                                  	<input type="text" class="form-control "  id="txtbranchAddress" value='<?php echo $this->session->userdata('branch_address'); ?>'>
                                  	
								</div>
								<div class="col-lg-1 hide">
                                  	<label for="">Branch Phone</label>
                                  	<input type="text" class="form-control  "  id="txtbranchPhone" value='<?php echo $this->session->userdata('branch_phone_no'); ?>'>
                                  	
								</div>

								<div class="col-lg-2">
                                	<label for="">Late fee fine</label>
                                  	<input type="text" class="form-control num" id="txtLateFeeFine">
                                </div>
                                
								<div class="col-lg-3">
                                    <label for="">Fee Category</label>
                                    <select class="form-control" id="category_dropdown" >
                                        <option value="" disabled="" selected="">Choose fee category</option>
                                        <?php foreach ($feeCategories as $feecategory): ?>
                                        	<option value="<?php echo $feecategory['fcid']; ?>"><?php echo $feecategory['name']; ?></option>
                                        <?php endforeach ?>
                                    </select>
								</div>
								<div class="col-lg-3">
                                    <label for="">Class</label>
                                    <select class="form-control" id="class_dropdown">
                                        <option value="" disabled="" selected="">Choose Class</option>
                                    </select>
								</div>
								<div class="col-lg-3" style='margin-top: 28px;'>
									<div class="pull-right">
										<a class="btn btn-success btnSearch"><i class="fa fa-search"></i> Show</a>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>	<!-- end of configuration -->

			<div class="row">
				<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">


							<div class="row">
								<div class="col-lg-12">
									<table class="table table-striped table-hover" id="students_table">

										<thead>
										

										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="pull-right">
					                	<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['monthly_fee_issuance']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					                    <a href="" class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['monthly_fee_issuance']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					                    <a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
					                     <a class="btn btn-info btn-lg btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
					                </div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>