<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-6">
				<h1 class="page_title">Fee Receive</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>

				<input type="hidden" id="feereceiveid" value="<?php echo $setting_configur[0]['feereceive']; ?>">
				<input type="hidden" id="arearsid" value="<?php echo $setting_configur[0]['arears']; ?>">
				<input type="hidden" id="latefeefineid" value="<?php echo $setting_configur[0]['latefeefine']; ?>">
				<input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
				



			</div>
			<div class="col-lg-6">
				<div class="pull-right">
					<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['detailed_fee_reciept']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['detailed_fee_reciept']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
					<a class="btn btn-info  btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
				</div>
			</div> 	<!-- end of col -->
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<form action="">

										<div class="row">
											<div class="col-lg-1">
												<label for="">Id</label>
												<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['detailed_fee_reciept']['update']; ?>' id="txtFeeRecId" >
												<input type="hidden" id="txtMaxFeeRecIdHidden">
												<input type="hidden" id="txtFeeRecIdHidden">
											</div>

											<div class="col-lg-2">
												<label for="">Date</label>
												<input class="form-control ts_datepicker" type="text" id="current_date">
											</div>
										</div>

										<div class="row">
											<div class="col-lg-3">
												<label for="">Account</label>
												<select class="form-control" id="accounts_dropdown">
													<option value="" disabled="" selected="">Choose Account</option>
													<?php foreach ($accounts as $account): ?>
														<option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
													<?php endforeach ?>
												</select>
											</div>
											<div class="col-lg-1">
												<label for="">Chalan#</label>
												<input type="text" class="form-control num" id="txtvrno" >
												<input type="hidden" class="form-control" id="txtvrnoHidden">
											</div>

											<div class="col-lg-1">
												<label for="">Stdid</label>
												<input type="text" class="form-control" id="txtStdid" disabled>
											</div>

											<div class="col-lg-1">
												<label for="">Class</label>
												<input type="text" class="form-control" id="txtClassName" disabled='disabled' data-claid=''>
											</div>

											<div class="col-lg-1">
												<label for="">Section</label>
												<input type="text" class="form-control" id="txtSectionName"	disabled='disabled' data-secid=''>
											</div>

											<div class="col-lg-2">
												<label for="">Student Name</label>
												<input type="text" class="form-control" id="txtName" disabled='disabled' data-rollno=''>
											</div>

											<div class="col-lg-2">
												<label for="">Father Name</label>
												<input type="text" class="form-control" id="txtFatherName" disabled='disabled'>
											</div>
										</div>

										<div class="row"></div>
										<div class="container-wrap">
											<div class="row">
												<div class="col-lg-4">
													<label for="">Particulars</label>
													<select class="form-control" id="particulars_dropdown">
														<option value="" disabled="" selected="">Choose particular</option>
														<?php foreach ($charges as $charge): ?>
															<option value="<?php echo $charge['chid']; ?>" data-pid="<?php echo $charge['pid']; ?>" data-amount="<?php echo $charge['charges']; ?>" data-chid="<?php echo $charge['chid']; ?>"><?php echo $charge['description']; ?></option>
														<?php endforeach ?>
													</select>
												</div>
												<div class="col-lg-1">
													<label for="">Charges</label>
													<input type="text" class="form-control num" id="txtCharges">
												</div>
												<div class="col-lg-1" style='margin-top: 26px;'>
													<a href="" class="btn btn-primary" id="btnAddCharges">+</a>
												</div>
											</div>
										</div>
										<div class="row"></div>

										<div class="row">
											<div class="col-lg-8">
												<table class="table table-striped" id="charges_table">
													<thead>
														<tr>
															
															<th>Particulars</th>
															<th>Charges</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody class="myrows">

													</tbody>
												</table>
											</div>
										</div>

										<div class="row"></div>
										<div class="row">
											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">Discount</span>
													<input type="text" class="form-control num" id="txtDiscount">
												</div>
											</div>
											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">Other(Fines)</span>
													<input type="text" class="form-control num" id="txtOther">
												</div>
											</div>
											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">Arears</span>
													<input type="text" class="form-control num" id="txtArears">
												</div>
											</div>

											<div class="col-lg-1"></div>
											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">Net Amount</span>
													<input type="text" class="form-control" id="txtNetAmount" disabled="disabled">
												</div>
											</div>
										</div>

										<div class="row"></div>
										<div class="row">
											<div class="col-lg-12">
												<div class="pull-right">
													<a class="btn btn-lg btn-primary btnSave" data-insertbtn='<?php echo $vouchers['detailed_fee_reciept']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
													<a class="btn btn-lg btn-danger btnDelete" data-deletetbtn='<?php echo $vouchers['detailed_fee_reciept']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
													<a class="btn btn-lg btn-warning btnReset"><i class="fa fa-refresh"></i> Reset</a>
													<a class="btn btn-info  btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
												</div>
											</div> 	<!-- end of col -->
										</div>	<!-- end of row -->

										<div class="row">


											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">User: </span>
													<input type="text" class=" form-control"  id="txtUserName" >

												</div>
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Posting: </span>
													<input type="text" class=" form-control"  id="txtPostingDate" >

												</div>
											</div>                                             
										</div>
										
									</form>	<!-- end of form -->

								</div>	<!-- end of panel-body -->
							</div>	<!-- end of panel -->
						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>