<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Registration Receive</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<form action="">

										<div class="row">
											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">Id</span>
													<input type="text" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['regisration_recieve']['update']; ?>' id="txtRegRecId" >
													<input type="hidden" id="txtMaxRegRecIdHidden">
													<input type="hidden" id="txtRegRecIdHidden">
												</div>
											</div>

											<div class="col-lg-1"></div>

											<div class="col-lg-3">
			                                	<div class="input-group">
			                                    	<span class="input-group-addon">Date</span>
			                                    	<input class="form-control ts_datepicker" type="text" id="current_date">
			                                	</div>
			                                </div>
										</div>

										<div class="row">
											<div class="col-lg-4">

												<div class="input-group">
            										<span class="input-group-addon">Account</span>
            										<select class="form-control" id="accounts_dropdown">
					                                    <option value="" disabled="" selected="">Choose Account</option>
					                                    <?php foreach ($accounts as $account): ?>
					                                    	<option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
					                                    <?php endforeach ?>
					                                </select>
						            			</div>

											</div>
										</div>

										<div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Voucher#</span>
													<input type="text" class="form-control" id="txtvrno">
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-lg-2">
												<div class="input-group">
													<span class="input-group-addon">Stdid</span>
													<input type="text" class="form-control" id="txtStdid" disabled>
												</div>
											</div>

											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Class</span>
													<input type="text" class="form-control" id="txtClassName" disabled='disabled' data-claid=''>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="input-group">
													<span class="input-group-addon">Name</span>
													<input type="text" class="form-control" id="txtName" disabled='disabled' data-rollno=''>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Father Name</span>
													<input type="text" class="form-control" id="txtFatherName" disabled='disabled'>
												</div>
											</div>
										</div>

										<div class="row"></div>
										<div class="row"></div>

										<div class="container-wrap">													
											<div class="row">
					                            <div class="col-lg-5">
					                                <div class="input-group">
					                                  	<span class="input-group-addon">Particulars</span>
					                                  	<select class="form-control" id="particulars_dropdown">
					                                      	<option value="" disabled="" selected="">Choose particular</option>
					                                      	<?php foreach ($charges as $charge): ?>
					                                          	<option value="<?php echo $charge['chid']; ?>" data-amount="<?php echo $charge['charges']; ?>" data-pid="<?php echo $charge['pid']; ?>" data-chid="<?php echo $charge['chid']; ?>"><?php echo $charge['description']; ?></option>
					                                      	<?php endforeach ?>
					                                  	</select>
					                                </div>
					                            </div>
						                        <div class="col-lg-3">
						                            <div class="input-group">
						                                <span class="input-group-addon">Charges</span>
						                                <input type="text" class="form-control num" id="txtCharges">
						                            </div>
												</div>

						                        <div class="col-lg-1">
						                            <div class="input-group">
						                                <a href="" class="btn btn-primary" id="btnAddCharges">+</a>
													</div>
						                        </div>
					                    	</div>
										</div>
										<div class="row"></div>
										<div class="row"></div>

										<div class="row">
				                            <div class="col-lg-12">
				                                <table class="table table-striped" id="charges_table">
				                                    <thead>
				                                        <tr>
				                                            <th>Particulars</th>
				                                            <th>Charges</th>
				                                            <th>Action</th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>

				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>

				                        <div class="row"></div>

				                        <div class="row">
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Discount</span>
													<input type="text" class="form-control num" id="txtDiscount">
												</div>
											</div>
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Other(Fines)</span>
													<input type="text" class="form-control num" id="txtOther">
												</div>
											</div>
											<div class="col-lg-3"></div>
											<div class="col-lg-3">
												<div class="input-group">
													<span class="input-group-addon">Net Amount</span>
													<input type="text" class="form-control" id="txtNetAmount" disabled="disabled">
												</div>
											</div>
										</div>

										<div class="row"></div>
										<div class="row">
											<div class="col-lg-12">
												<div class="pull-right">
													<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['regisration_recieve']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
													<a class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['regisration_recieve']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
													<a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
												</div>
											</div> 	<!-- end of col -->
										</div>	<!-- end of row -->


									</form>	<!-- end of form -->

								</div>	<!-- end of panel-body -->
							</div>	<!-- end of panel -->
						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>