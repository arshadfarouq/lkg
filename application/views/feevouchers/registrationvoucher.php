<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Student Registration</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">

					<div id="add_subject" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-2">
													<div class="input-group">
				                                        <span  class="input-group-addon">Reg. Id</span>
				                                        <input type="text" id="txtRegtId" class="num txtidupdate form-control" data-txtidupdate='<?php echo $vouchers['regisration']['update']; ?>'>
														<input type="hidden" id="txtMaxRegIdHidden">
														<input type="hidden" id="txtRegIdHidden">
													</div>
				                                </div>
											</div>

											<div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-4">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">Date</span>
				                                        <input class="form-control ts_datepicker" type="text" id="txtRegDate">
				                                    </div>
				                                </div>
			                                </div>

			                                <div class="row">
			                                	<div class="col-lg-1"></div>
												<div class="col-lg-4">
													<div class="input-group">
				                                        <span  class="input-group-addon">Name</span>
				                                        <input type="text" class="form-control" id="txtName">
													</div>
				                                </div>
											</div>

											<div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-4">
													<div class="input-group">
				                                        <span  class="input-group-addon">Father Name</span>
				                                        <input type="text" class="form-control" id="txtFatherName">
													</div>
				                                </div>
											</div>

											<div class="row">
												<div class="col-lg-1"></div>
				                                <div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">Gender</span>
				                                        <select class="form-control" id="gender_dropdown">
				                                          	<option value="male">Male</option>
				                                          	<option value="female">Female</option>
				                                        </select>
				                                    </div>
				                                </div>
				                            </div>

				                            <div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-4">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">Date of Birth</span>
				                                        <input class="form-control ts_datepicker" type="text" id="dob_date">
				                                    </div>
				                                </div>
			                                </div>

											<div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-4">
													<div class="input-group">
				                                        <span class="input-group-addon">Address</span>
				                                        <input type="text" class="form-control" id="txtAddress">
													</div>
				                                </div>
											</div>

											<div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-4">
													<div class="input-group">
				                                        <span class="input-group-addon">Mobile #</span>
				                                        <input type="text" class="form-control num" id="txtMobile">
													<input type="hidden" id="txtNameHidden">
													</div>
				                                </div>
											</div>

			                                <div class="row">
			                                	<div class="col-lg-1"></div>
												<div class="col-lg-4">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">Class</span>
				                                        <select class="form-control" id="class_dropdown">
				                                          	<option value="" disabled="" selected="">Choose Class</option>
				                                        </select>
				                                    </div>
				                                </div>
			                                </div>

			                                <div class="row">
			                                	<div class="col-lg-1"></div>
												<div class="col-lg-4">
													<div class="input-group">
				                                        <span  class="input-group-addon">Fee</span>
				                                        <input type="text" class="form-control num" id="txtFee" value='<?php echo $registration_fee['charges']; ?>' data-chid='<?php echo $registration_fee['chid']; ?>'>
													</div>
				                                </div>
											</div>

											<div class="row">
												<div class="col-lg-1"></div>
												<div class="col-lg-6">
													<div>
														<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['regisration']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
														<a href='' class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['regisration']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
														<a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
				</div>
			</div>
		</div>
	</div>
</div>