<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admission Form</title>

	<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
	<style>
		body {
			color: #333;
			font-family: tahoma;
		}
		#wrap {
			margin-top: 20px;
			margin-bottom: 20px;
		}
		#schoollogo {
			width: 140px;
			height: auto;
		}
		.center{
			text-align: center;
		}
		.paddingspace {
			/*padding: 15px !important;*/

		}
		.bold {
			font-weight: bold;
		}
		.box {
			border: 2px solid #333;
			padding: 5px;
			display: block;
		}
		.shadow {
			box-shadow: 1px 1px 1px #333;
		}
		.underline {
			border-bottom: 1px solid #000;
			padding-bottom: 2px;
			margin-left: 8px;
			text-transform: capitalize;
		}
		.topline {
			border-top: 1px solid #000;
			padding-top: 2px;
		}
		.divider {
			margin-bottom: 10px;
		}
		.justify {
			text-align: justify;
		}




		@page{margin-top: 0mm; margin-left: 10mm;margin-right: 10mm;margin-bottom: 10mm; size: auto !important;  }
		@media print {
			/* .btnPdfDownload {
				display: none;
			} */
		}
	</style>
</head>
<body>

	<div id="wrap">
		<div class="container-fluid">



			<div class="row divider">
				<div class="col-xs-2">
					<img src="../../../assets/img/printlogo/schoollogo.png" alt="" id='schoollogo' />
				</div>
				<div class="col-xs-6">
					<div class="row">
						<div class="col-xs-12">
							<h3 class='center box shadow'>Registration Form</h3>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
					<table class='box paddingspace'>
						<tr>
							<th><span>Reg.#</span></th>
							<td><span class='underline' id='txtRegNo'></span></td>
						</tr>
						<tr>
							<th><span>Comp.Code #</span></th>
							<td><span class='underline' id='txtCompCode'></span></td>
						</tr>
						<tr>
							<th><span>Family ID</span></th>
							<td><span class='underline' id='txtFamilyId'></span></td>
						</tr>
						<tr>
							<td><span>Date of Registration</span>
							</td><td><span class='underline' id='txtRegDate'></span></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="row divider">
				<div class="col-xs-4 bold">
					<span>Student's Name: (in block letters)</span>
				</div>
				<div class="col-xs-6">
					<span class='underline' id='txtName'> </span>
				</div>
			</div>

			<div class="row divider">
				<div class="col-xs-4 bold">
					<span>Father's Name: (in block letters)</span>
				</div>
				<div class="col-xs-6">
					<span class='underline' id='txtFatherName'> </span>
				</div>
			</div>

			<div class="row divider">
				<div class="col-xs-4">
					<span class='bold'>Date of Birth (in figures) </span>
				</div>
				<div class="col-xs-2">
					<span class='underline' id='txtBirthDate'></span>
				</div>
				<div class="col-xs-4 bold">
					<span>Class in which admission is required</span>
				</div>
				<div class="col-xs-2">
					<span class='underline' id='txtAdmissionClass'> </span>
				</div>
			</div>
			<div class="row divider">
				
				<div class="col-xs-2 bold">
					<span>Gender</span>
				</div>
				<div class="col-xs-2">
					<span class='underline' id='txtGender'> </span>
				</div>
			</div>
			<div class="row divider">
				<div class="col-xs-3">
					<span class='bold'>Permanent Address </span>
				</div>
				<div class="col-xs-9">
					<span class='underline' id='txtPermanentAddress'></span>
				</div>
			</div>
			<div class="row divider">
				<div class="col-xs-3">
					<span class='bold'>Present Address </span>
				</div>
				<div class="col-xs-9">
					<span class='underline' id='txtPresentAddress'></span>
				</div>
			</div>
			<div class="row divider">
				<div class="col-xs-1">
					<span class='bold'>Phone: </span>
				</div>

				<div class="col-xs-2">
					<span class='underline' id='txtHomeNo'></span>
				</div>
				<div class="col-xs-1">
					<span class='bold pull-right'>Mobile</span>
				</div>
				<div class="col-xs-2">
					<span class='underline' id='txtMobileNo'></span>
				</div>
			</div>
			<div class="row divider">

			</div>
			
			<div class="row divider">
				<div class="col-xs-12">
					<p class='bold center'>Name of real brothers &amp; Sisters (Already in School)</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<table border='1' class="table" style="width: 90% !important;margin:-1px 10px 0px 0px !important;">
						<thead>
							<tr>
								<th class='center' style="width: 50px !important;">Id</th>
								<th class='center'>Name</th>
								<th class='center' style="width: 100px !important;">Relation</th>
							</tr>
						</thead>
						<tbody class="myrows">

						</tbody>

					</table>
				</div>
			</div>
			<div class="row divider">
				<div class="col-xs-12">
					<span class='bold'>To the best of my knowledge the above information is correct. </span>
				</div>
			</div>

			<div class="row divider">
				<div class="col-xs-2">
					<span class='bold'>Attached Documents: </span>
				</div>
				<div class="col-xs-7">
					<div class="row">
						<div class="col-xs-6">
							<span><input type="checkbox"></span><span> Previous School Certificate</span>
						</div>
						<div class="col-xs-6">
							<span><input type="checkbox"></span><span> CNIC Copy of Father/Guardian</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<span><input type="checkbox"></span><span> Birth Certificate/B Form</span>
						</div>
						<div class="col-xs-6">
							<span><input type="checkbox"></span><span> 2 Photographs (Passport Size)</span>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
					<span class='topline bold' style='margin-top: 40px; display: block;text-align: center;'>Father/ Guardian Signature</span>
				</div>
			</div>
			<div class="row divider">
				<div class="col-xs-12">
					<span class='bold'>Fee Detail</span> <sub> (To be filled by the office only)</sub>
				</div>
			</div>
			<div class="row divider">
				<div class="col-xs-7">
					<table class="table" border='1'>
						<tr>
							<th width="40%;">Admission Fee:</th>
							<td></td>
						</tr>
						<tr>
							<th width="40%;">Tution Fee: </th>
							<td></td>
						</tr>
						<tr>
							<th width="40%;">Exam Fee: </th>
							<td></td>
						</tr>
						<tr>
							<th width="40%;">Stationary Charges: </th>
							<td></td>
						</tr>
						<tr>
							<th width="40%;">Total: </th>
							<td></td>
						</tr>
					</table>
				</div>
				<div class="col-xs-2"></div>
				<div class="col-xs-3">
					<span class='topline bold' style='margin-top: 170px; display: block;text-align: center;'>Principal Signature</span>
				</div>
			</div>
		</div>
	</div>


	<script src='../../../assets/js/jquery.min.js'></script>
	<script src='../../../assets/bootstrap/js/bootstrap.min.js'></script>

	<script>

		var opener = window.opener;
		$('#txtRegNo').text($.trim(opener.$('#txtStudentIdHidden').val()));
		$('#txtCompCode').text($.trim(opener.$('#txtStudentIdHidden').val()));
		//$('#txtFamilyId').text($.trim(opener.$('#').val()));
		$('#txtRegDate').text($.trim(opener.$('#txtAdmiDate').val()));

		$('#txtName').text($.trim(opener.$('#txtStudentName').val()));
		$('#txtFatherName').text($.trim(opener.$('#txtFatherName').val()));
		$('#txtBirthDate').text($.trim(opener.$('#txtDOB').val()));
		$('#txtAdmissionClass').text($.trim(opener.$('#admiClass_dropdown').find('option:selected').text()));
		//$('#txtReligion').text($.trim(opener.$('#').val()));
		//$('#txtCast').text($.trim(opener.$('#').val()));
		$('#txtGender').text($.trim(opener.$('#gender_dropdown').val()));
		$('#txtPermanentAddress').text($.trim(opener.$('#txtbusinessAddress').val()));
		$('#txtPresentAddress').text($.trim(opener.$('#txtAddress').val()));
		$('#txtOfficeNo').text($.trim(opener.$('#').val()));
		$('#txtHomeNo').text($.trim(opener.$('#txtPhoneNo').val()));
		$('#txtMobileNo').text($.trim(opener.$('#txtMobileNo').val()));

		
		
		
		var studentid = "";
		var name = "";
		var relation = "";
		var htmls = "";
		
		
		var srno = 1;

		$('.myrows').empty();
		opener.$('#kinship_table').find('tbody tr').each(function(index, elem)
		{	
			
			studentid = $.trim($(elem).find('td.ksstdid').text());
			name = $.trim($(elem).find('td.name').text());

			relation = $.trim($(elem).find('td.relation').text());
			
			htmls = "<tr class='row-content'>";
			htmls += "<td class='TahomaFont'>"+srno+"</td>";
			htmls += "<td style='text-align:center;'>"+name+"</td>";

			htmls += "<td class='text-right TahomaFont'>" +  relation + "</td>";
			
			htmls += "</tr>";
			$(".myrows").append(htmls);

			console.log(htmls);

		});

		//$('#txtEmail').text($.trim(opener.$('#').val()));
		$('#txtPreviousSchool').text($.trim(opener.$('#previousSchool_dropdown').find('option:selected').text()));
		window.print();

		$('.btnPdfDownload').on('click', function(e) {
			e.preventDefault();
		});
	</script>
</body>
</html>