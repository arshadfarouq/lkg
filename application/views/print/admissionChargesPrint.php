<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Fee Receive</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
		<style>
			 * { margin: 0; padding: 2; font-family: tahoma !important; }
			 body { font-size: 10px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size: 10px !important; font-weight: bold !important; display: inline-block !important; width: 10px !important; } 
			 .field1 { font-size: 10px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border-collapse: none !important; }
			 table { width: 100% !important; border: 0.5px none black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:0px}
			 th {  padding: 2px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child { text-align: left !important; }
			 .voucher-table thead th {background: #ccc !important; } 
			 tfoot {border-top: 0.5px solid black !important; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 10px !important; border-top: 0.5px solid black !important; }
			 .invoice-type { border-bottom: 0.5px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px !important; font-family: tahoma !important; line-height: 14px !important; padding: 2px !important; } 
			 .rcpt-header { width: 90% !important;  margin: 0px; display: inline;  top: 0px; right: 0px; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 2px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 10px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 10px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 10px !important; }
			 thead th { font-size: 10px !important; font-weight: bold !important; padding: 2px !important; }
			 .fieldvalue { font-size: 10px !important; position: absolute !important; width: 497px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
			.foot-comments td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important; font-weight: bold !important;}


			.footer_company td { font-size: 8px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
			@page{margin-top: 5mm; margin-left: 2mm;margin-right: 2mm;margin-bottom: 2mm; size !important:  auto !important;  }

			
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
			tfoot tr td { font-size: 10px !important; padding: 2px !important;  }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 10px !important; font-weight: bold !important;}
			.footer {
	            position: absolute;
	            color: red;
	            bottom: 0;
        	}
        	tr { page-break-inside: avoid !important; }
		 	tr td ul li { page-break-inside: avoid !important; }
		</style>
	</head>
	<body>
	<div class="container" style="margin-top:-10px;">
		<div class="row">
			<div class="col-xs-12">
				<div class="row hd hdshowHide">
					<div class="col-lg-12">
						<img class="rcpt-header" src="../../../assets/img/pic1.png" alt="">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 headr_detail newone" style="height: 40px;">
						<span>
							<h1 class="text-center newone newone2" style="text-align:left !important; margin-left:50px ; font-weight:bold !important;font-family: 'open_sansregular' !important;"><b style="font-size:25px !important;position:relative;top:-23px;font-weight:bold !important;" class="set PageTitle"></b></h1>
						</span>
					</div><!-- end of col -->
				</div><!-- end of row -->
				<div class="row">
					<div class="col-lg-12">
						<table class="table" style="width: 90% !important;border:1px none !important;margin:-1px 20px 0px 27px !important;">

							<tr >
							<td ><span ><b style="font-size:10px !important;">Vr#:</b> </span></td>
								<td style="text-align:left !important;"><b><span class="vrNo" ></span></b></td>

								
							
							</tr>
							<tr >
							<td ><span ><b style="font-size:10px !important;">Current Date:</b> </span></td>
								<td style="text-align:left !important;"><b><span class="c_date" ></span></b></td>

								<td ><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Last Date:</b> </span></td>
								<td ><b><span class="L_date"></span></b></td>
							
							</tr>
							<tr style="margin-bottom: 10px !important;">
								
								<td ><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Student Id:</b> </span></td>
								<td ><b><span class="studentID"></span></b></td>
								
								
								<td style="width: 80px !important;text-align:left;"><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Late Fee:</b></span></td>
								<td style="text-align:left;"><b><span class="latefee" ></span></b></td>
							</tr>

							<tr style="margin-bottom: 10px !important;">
								
								<td ><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Branch:</b> </span></td>
								<td ><b><span class="branch"></span></b></td>
								
								
								<td style="width: 80px !important;text-align:left;"><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Fee Category:</b></span></td>
								<td style="text-align:left;"><b><span class="fee" ></span></b></td>
							</tr>
							<tr>
								<td ><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Class:</b> </span></td>
								<td ><b><span class="class_name"></span></b></td>
								<td style="width: 80px !important;text-align:left;"><span style="border-bottom:1px none;"><b style="font-size:10px !important;">Section:</b></span></td>
								<td style="text-align:left;"><b><span class="SectionName" ></span></b></td>
							</tr>
						</table>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<table id='charges_table' class="table table_size" style="width: 90% !important;border:1px solid !important;margin:0px 10px 0px 20px !important;">
							<thead style="background:#E8E8E8;">
								<tr style="font-family: 'open_sansregular';"> 
									
								
									<th style="width:100px !important;" class="text-left"><b class="th_text" style="font-size:12px !important; margin-left: 20px !important;">Particulars </b></th>
									
									<th style="width:30px !important;" class="text-right"><b class="th_text" style="font-size:12px !important;">Charges</b></th>
									<th style="width:30px !important;" class="text-right"><b class="th_text" style="font-size:12px !important;">Concession</b></th>
									<th style="width:60px !important;" class="text-right"><b class="th_text" style="font-size:12px !important;">	Net Amount</b></th>
									

									
								</tr>
							</thead>
							<tbody class="myrows">

							</tbody>
							<!-- <tfoot>
								<tr style="border-top:1px solid black !important;">
									<!-- <td style="width:5px;"></td>
									<td style="width:5px;" class="category_hide">999</td> 

									<td class="text-left" style="border-right:1px !important;text-align:left !important;"><b class="th_text total_font" style="font-size:25px !important;">Total</b></td>

									<td class="text-center"><b class="totalQty TahomaFont"></b></td>
									<!-- <td class="text-left"></td> -->
									<!-- <td class="text-left"></td>
									<td class="text-right"><b class="totalamount TahomaFont"></b></td>
								</tr>
							</tfoot> -->
							
						</table>
					</div>
				</div>

				<div class="row">

					<table style="width: 20% !important; margin: 10px 10px 0 550px;" id="mytable" class="pull-right">
						<tfoot style="border:none !important;" class='table_size_new1'>

						</tfoot>
					</table>


				</div>
				<div class="row">
					<table style="width: 30%;" id="PreBalanceTable">
						<tbody>

						</tbody>
					</table>
				</div>
			</div><!-- end of col -->
		</div><!-- row-fluid -->
		<br><br>
		<!-- <div class="row" >
			<div class="col-lg-12">
				<table class="table">
					<tfoot style="border:none !important;">
						<tr>
							<td class="text-left" style="width: 180px;text-align: center; "><b> :Signature </b></td>
							<td colspan="2" style="border-top:none !important;"></td>
							<td class="text-left" style="width: 180px;text-align: center; display:none !important;"><b> : </b></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
 -->
		<br>
		<div class="row">
			<div class="col-lg-12" style=" width: 20% !important; margin: 10px 10px 0 30px; border: !important; border-radius: 10px !important; ">
				<table class="table">
					<tfoot style="border:none !important;">
						<tr>
										<!-- <td class="text-left" style="border-top:none;width: 265px;text-align:center;position:relative;top:-5px;"><b > User : </b> <span class="userinfo"></span></td>
										
										<td colspan="2" style="border-top:none !important;"></td> -->
										<td class="text-right" style="border-top:none;width: 340px;text-align: right; font-size:12px !important;" ><b>Powered By: www.alnaharsolutions.com</b></td>
										
									</tr>
								</tfoot>
							</table>
						</div>
					</div>

				</div><!--container-fluid -->
				<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
				<script src="../../../assets/js/handlebars.js"></script>

				<script type="text/javascript">
					function sortTable() {
						var  table,rows, switching, i, x, y, shouldSwitch;
						table = document.getElementById("charges_table");
						switching = true;
				  /*Make a loop that will continue until
				  no switching has been done:*/
				  
				  while (switching) {
				    //start by saying: no switching is done:
				    switching = false;
				    rows = table.getElementsByTagName("tr");
				    /*Loop through all table rows (except the
				    first, which contains table headers):*/
				    for (i = 1; i < (rows.length - 1); i++) {
				      //start by saying there should be no switching:
				      shouldSwitch = false;
				      /*Get the two elements you want to compare,
				      one from current row and one from the next:*/
				      x = rows[i].getElementsByTagName("td")[1];
				      y = rows[i + 1].getElementsByTagName("td")[1];
				      //check if the two rows should switch place:
				      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
				        //if so, mark as a switch and break the loop:
				        shouldSwitch= true;
				        break;
				    }
				}
				if (shouldSwitch) {
				      /*If a switch has been marked, make the switch
				      and mark that a switch has been done:*/
				      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
				      switching = true;
				  }
				}
				var srno=1;

				$('#charges_table').find('tbody tr').each(function(index, elem)
				{

					$(elem).find('td.srno').text(srno);
					srno +=1;


				});


			}
			function getNumVal  (el){
				return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
			}
			function getNumText  (el){
				return isNaN(parseFloat(el.text())) ? 0 : parseFloat(el.text());
			}

			function getMonthName (monthNum) {
				switch(monthNum) {
					case 1 :
					return 'Jan';
					break;
					case 2 :
					return 'Feb';
					break;
					case 3 :
					return 'Mar';
					break;
					case 4 :
					return 'Apr';
					break;
					case 5 :
					return 'May';
					break;
					case 6 :
					return 'Jun';
					break;
					case 7 :
					return 'Jul';
					break;
					case 8 :
					return 'Aug';
					break;
					case 9 :
					return 'Sep';
					break;
					case 10 :
					return 'Oct';
					break;
					case 11 :
					return 'Nov';
					break;
					case 12 :
					return 'Dec';
					break;
				}
			}
			$(function(){

				var opener = window.opener;
				var PageTitle = opener.$('.page_title').text();
				if($.trim(PageTitle)=='Admission Charges'){
					var serialno = opener.$('#txtdcno').val();
				}else{
					var serialno = opener.$('#txtdcno').val();

				}
				

				var C_Date = opener.$('#current_date').val();
				var L_Date = opener.$('#last_date').val();
				
				var dateParts = [];
				var datePartes = [];
				var pre_bal_print = (opener.$('#switchPreBal').bootstrapSwitch('state') === true) ? '1' : '0';

				// var separator = ( C_Date.indexOf('/') === -1 ) ? '-' : '/';
				

				// dateParts = C_Date.split(separator);
				// var C_Date =    'd:   '+dateParts[2] + '-' + getMonthName(parseInt(dateParts[1], 10)) + '-' +  dateParts[0]  ;
				
			

			
				var SectionName = opener.$('#txtSectionName').val();
				
				var class_name = opener.$('#txtClassName').val();
			
				var remarks = opener.$('#txtRemarks').val();
				var studentID = opener.$('#stdid_dropdown').select2();
				var latefee = opener.$('#txtLateFee').val();
				var fee = opener.$('#txtFeeCategory').val();
				var branch = opener.$('#txtBranchName').val();
				var netAmnt = opener.$('#txtNetAmount').val();
				// var user = opener.$('#user_dropdown').find('option:selected').text();
				var disc = opener.$('.txtTotalDiscount').text();
				

				
			    var	particulars="";
				var concession = "";							
				var netamnt = "";
				var charges = "";
				
				var htmls = "";
			
				var totalSum = 0;
				var srno = 1;

				$('.myrows').empty();
				var sr=1;

				opener.$('#charges_table').find('tbody tr').each(function(index, elem)
				{	
					// godown_name = warehouse + "<br>";
					// product_id = $.trim($(elem).find('td.particulars').text());
					
					particulars = $.trim($(elem).find('td.particulars').text()) ;
					

					charges = $.trim($(elem).find('td.charges').text());
					concession = $.trim($(elem).find('td.concession').text());
					netamnt = $.trim($(elem).find('td.total').text());

					htmls = "<tr class='row-content' style='border-top: 1px solid black !important;'>";
								

					htmls += "<td style='font-size:12px !important;'>"+ particulars+"</td>";
					
					htmls += "<td  style='font-size:12px !important; text-align:right;'>"+parseFloat(charges).toFixed(0)+ "</td>";
					htmls += "<td  style='font-size:12px!important; text-align:right;'>"+parseFloat(concession).toFixed(0)+"</td>";
					htmls += "<td  style='font-size:12px !important; text-align:right;'>" +parseFloat(netamnt).toFixed(0)+ "</td>";
					htmls += "</tr>";
					$(".myrows").append(htmls);

					 // totalSum = parseFloat(totalSum) + parseFloat($.trim($(elem).find('td.charges').text()) );
					// srno +=1;

				});

				
				var gcharges     = getNumText(opener.$('#txtTotalAmount'));			
			
				var Concession = getNumText(opener.$('#txtTotalConcession'));
			
				var NetAmount = getNumText(opener.$('#txtTotalNetAmount'));
				


				var html1 = '';
			
				

				html1 += '<tr><td  style="font-size:12px !important; font-weight:bold !important; text-align:left; margin-left:10 !important;" class="text-center">Charges</td>';
				html1 += '<td style="font-size:12px !important; font-weight:bold !important;" id="txtTotalAmount" class="TahomaFont">'+ parseFloat(gcharges).toLocaleString() +'</td> ' ;
				html1 += '</tr>';
				html1 += '<tr><td  style="font-size:12px !important; font-weight:bold !important; text-align:left;" class="text-center">Concession</td>';
				html1 += '<td  style="font-size:12px !important; font-weight:bold !important;" id="txtTotalConcession" class="TahomaFont">'+ parseFloat(Concession).toLocaleString() +'</td> ' ;
				
				html1 += '</tr>';
				html1 += '<tr><td  style="font-size:12px !important; font-weight:bold !important; text-align:left;" class="text-center">Net Amount</td>';
				html1 += '<td  style="font-size:12px !important; font-weight:bold !important;" id="txtTotalNetAmount" class="TahomaFont">'+ parseFloat(NetAmount).toLocaleString() +'</td> ' ;
				html1 += '</tr>';
				
				


				$("#mytable tfoot").append(html1);

				if( pre_bal_print ==='1'){
					html1='';
					html1 +='<tr><td colspan="3" style="font-size:20px; text-align:rigth;" class="text-center">Gross Amount</td>';
					html1 +='<td colspan="" style="font-size:20px; text-right:right !important;" id="Balance" class="TahomaFont">'+ parseFloat(balance).toFixed(0) +'</td>';
					html1 +='</tr>';		    	
					$("#PreBalanceTable tbody").append(html1);
				}

				var hd = (opener.$('#switchPrintHeader').bootstrapSwitch('state') === true) ? '1' : '0';

				$('.hd').removeClass('hdshowHide');
				if( hd ==='0'){
					$('.rcpt-header').addClass('hdshowHide');
				}

				$('.PageTitle').html(PageTitle);

				$('.vrNo').html(serialno);
				$('.c_date').html(C_Date);
				$('.L_date').html(L_Date);
				
				
				$('.SectionName').html(SectionName);
				$('.class_name').html(class_name);
				$('.remarks').html(remarks);
				$('.studentID').html(studentID);
				$('.latefee').html(latefee);
				$('.fee').html(fee);
				$('.branch').html(branch);
				
				$('.dics').html(disc);
				$('.netamnt,.rcvd_amnt').html(netAmnt);
			
				$('.totalamount').html(opener.$('.txtTotalAmount').text());
				$('.totalQty').html(opener.$('.txtTotalQty').text());
				$('.totalweight').html(opener.$('.txtTotalWeight').text());
				// $('.userinfo').html(user);
				
				sortTable('charges_table');

				window.print();


			});

		</script>
	</body>
	</html>