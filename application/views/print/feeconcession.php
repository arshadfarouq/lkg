<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma !important; }
			 body { font-size:10px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size:12px !important; font-weight: bold !important; display: inline-block !important; width: 100px !important; } 
			 .field1 { font-size:12px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border-collapse: none !important; }
			 table { width: 100% !important; border: 2px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:1px}
			 th {  padding: 5px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child { text-align: left !important; }
			 .voucher-table thead th {background: #ccc !important; } 
			 tfoot {border-top: 1px solid black !important; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 11px !important; border-top: 1px solid black !important; }
			 .invoice-type { border-bottom: 1px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 12px !important; font-family: tahoma !important; line-height: 14px !important; padding: 4px !important; } 
			 .rcpt-header { width: 700px !important; margin: 0px; display: inline;  top: 0px; right: 0px; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 5px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 12px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 12px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 12px !important; }
			 thead th { font-size: 12px !important; font-weight: bold !important; padding: 10px !important; }
			 .fieldvalue { font-size:12px !important; position: absolute !important; width: 497px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 12px !important; padding: 10px !important; border-top: 1px solid black !important;}
			.footer_company td { font-size: 8px !important; padding: 10px !important; border-top: 1px solid black !important;}

			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
			tfoot tr td { font-size: 10px !important; padding: 10px !important;  }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 12px !important; font-weight: bold !important;}
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
				<div class="span12 centered">
					
					<div class="row-fluid">
						<div class="span12"><img class="rcpt-header" src="<?php echo $header_img;?>" alt=""></div>
					</div>
					
					<div class="row-fluid">
						<table class="voucher-table" style="border:none !important;">
							<tbody>
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Vr# </td>
									<td style=" width: 30px; font-size:12px !important;"> <?php echo $vrdetail[0]['dcno'];?> </td>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Date:</td>
									<td style=" width: 30px; font-size:12px !important;"> <?php echo $vrdetail[0]['vrdate'];?> </td>
									
								</tr>
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">From Date:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['datefrom'];?> </td>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">To Date:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['dateto'];?> </td>
								
									
								</tr>
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Student Id:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['stdid'];?> </td>
									
									
								</tr>
								<tr>
									
									<td  style=" width:10px; font-weight:bold !important; font-size:12px !important;"> Name:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['student_name'];?> </td>
								
									
								</tr>
								<tr>
									
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Branch:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['branch_name'];?> </td>
									
								
									
								</tr>
								<tr>
									
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Class:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['class_name'];?> </td>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Section:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['section_name'];?> </td>
								
									
								</tr>
								<tr>
									
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Remarks:</td>
									<td style=" width: 50px; "> <?php echo $vrdetail[0]['des'];?> </td>
									
								
									
								</tr>
								
							</tbody>
						</table>
					</div>

					<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
						<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 12px !important; font-size:16px !important; "><?php echo $title;?></h3>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 10px; ">Sr#</th>
									<th style=" width: 100px; text-align:left; ">Particulars</th>
									<th style=" width: 12px; ">Amount</th>
									<th style=" width: 20px; ">Concession%</th>
									<!-- <th style=" width: 30px; ">Weight</th> -->
									<!-- <th style=" width: 40px; text-align:left;">From</th> -->
									<!-- <th style=" width: 40px; text-align:left;">To</th> -->
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$serial = 1;
									$netQty = 0;
									$netAmount=0;
									$netWeight=0;
									foreach ($vrdetail as $row):
										$netQty += abs($row['amount']);
										// $netamount += $row['s_amount'];
										$netWeight += abs($row['concession']);
								?>
									<tr  class="item-row">
									   <td class='text-left'><?php echo $serial++;?></td>
									   <td class='text-left'><?php echo $row['description'];?></td>
									   <!-- <td class='text-centre'><?php echo $row['uom'];?></td> -->
									   <td class='text-right'><?php echo number_format(abs($row['amount']),2);?></td>
									   <td class='text-right'><?php echo number_format(abs($row['concession']),2);?></td>
									   <!-- <td class='text-left'><?php echo (($row['dept_name']));?></td> -->
									   
									</tr>
									
								<?php endforeach?>
							</tbody>
							<tfoot>
								<tr class="foot-comments">
									<td class="subtotal bold-td text-right" colspan="2">Subtotal:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netQty,2);?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netWeight,2);?></td>
									
								</tr>
							</tfoot>
						</table>
					</div>
					
					<br>
					<br>
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<td></td>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="row-fluid">
						<p>
							<span class="footer_company">User:<?php echo $vrdetail[0]['user_name'];?></span><br>
							<!-- <span class="footer_company">Sofware By: www.alnaharsolutions.com, Mob: 03009663902</span> -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>