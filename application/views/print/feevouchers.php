<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fee Voucher</title>

	<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
	<style>
		body {
			color: #333;
			font-family: tahoma;
		}
		.xs-small {
			font-size: 8px;
		}
		.small {
			font-size: 10px;
		}
		.medium {
			font-size: 11px;
		}
		#wrap {
			margin-top: 20px;
			margin-bottom: 20px;
			margin-right: 10px;

		}
		.schoollogo {
			width: 80px;
			height: auto;
		}
		.center{
			text-align: center;
		}
		.slips {
			margin-top: 12px !important;
			page-break-after: always; /* Always insert page break after this element */
			page-break-inside: avoid; /* Please don't break my page content up browser */
		}
		.slip {
			border-right: 1px dashed #333;
			padding-right: 5px;
		}
		.table {
			width: 100%;
		}
		.table td {
			padding-left: 5px !important;
			padding-top: 0px !important;
			padding-bottom: 0px !important;
			padding-right: 0px !important;
			border-top: none;
			border: 1px solid #333;
		}
		.table tr {
			border: 1px solid #333;
		}
		.table .thead {
			background: rgb(215, 215, 230);
		}
		.paddingspace {
			padding: 15px !important;
		}
		.bold {
			font-weight: bold;
		}
		.box {
			border: 1px solid #333;
			padding: 3px;
			display: block;
			margin-right: 10px;
		}
		.shadow {
			box-shadow: 1px 1px 1px #333;
		}
		.underline {
			border-bottom: 1px solid #000;
			padding-bottom: 2px;
			margin-left: 8px;
			text-transform: capitalize;
		}
		.inwords {
			margin-left: 8px;
			text-transform: capitalize;
			margin-top: 5px;
		}
		.topline {
			border-top: 1px solid #000;
			padding-top: 2px;
		}
		.divider {
			margin-bottom: 8px;
			margin-right: 1px;
		}
		.justify {
			text-align: justify;
		}

		@media print {
			.btnPdfDownload {
				display: none;
			}
			@page {
				size: landscape;
			}
		}
	</style>
</head>
<body>

	<div id="wrap">
		<div class="container-fluid">

			<div class="row divider hide">
				<div class="col-lg-12">
					<a class="btn btn-default btnPdfDownload"><i class="fa fa-download"></i> PDF Download</a>
				</div>
			</div>

			<div class="slips-container">
			</div>
		</div>


		<script src='../../../assets/js/jquery.min.js'></script>
		<script src='../../../assets/bootstrap/js/bootstrap.min.js'></script>

		<script>

			function pad(num, size) {
				var s = "000000000" + num;
				return s.substr(s.length-size);
			}

			function getNumVal (el){
				return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
			}


			var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
			var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
			var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];


        // American Numbering System
        var th = ['','thousand','million', 'billion','trillion'];
        // uncomment this line for English Number System
        // var th = ['','thousand','million', 'milliard','billion'];

        var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}


        var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

        var opener = window.opener;
        var latefee=0
        var latefee = getNumVal(opener.$('#txtLateFeeFine'));

        var className = $.trim(opener.$('#class_dropdown').find('option:selected').text());

		// get current date
		var d = new Date();
		var month = parseInt(d.getMonth());
		month++;
		var printDate = d.getDate() + "-" + month + "-" + d.getFullYear();
		var Branchname = opener.$('#txtbranchname').val();
		// alert(Branchname);

		var Branchaddress = opener.$('#txtbranchAddress').val();
		var Branchphone = opener.$('#txtbranchPhone').val();
		var lastDate = opener.$('#last_date').val();


		var dcno   = opener.$('#txtdcno').val();
		var datefrom = opener.$('#fromMonth_date').val();
		var dateto = opener.$('#toMonth_date').val();
		

		var feeFor = monthNames[parseInt(datefrom.split("/")[1])-1] + " " + datefrom.split("/")[0] + " - " + monthNames[parseInt(dateto.split("/")[1])-1] + " " + dateto.split("/")[0];

		var col = "";
		var row = "";

		// get the charge names
		var chargesName = [];
		opener.$('#students_table thead tr th.charges').each(function(index, elem) {

			chargesName.push($.trim($(elem).text()));
		});

		// get the hidden data that includes the challan numbers
		var hiddenData = opener.$('.printData p');

		// get all the student data row by row
		$.each(opener.$('#students_table tbody tr'), function(index, elem) {

			var _stdid = $(elem).find('td').eq(0).text();
			var totfee = $(elem).find('td').last().text();
			var name = $(elem).find('td').eq(1).text();
			var sectionName = $(elem).find('td').eq(2).text();
			var challanNo = "";
			var namount = "";
			var tamount = 0;
			var t_amount = 0;
			var totoalamount = 0;			
			var trs = "";
			var arears = 0; 
			arears = $.trim($(elem).find('.firstrow').data('arears'));


			$.each(hiddenData, function(indx, el) {

				var stdid = $.trim($(el).find('.stdid').text());
				if (stdid == _stdid) {
					challanNo = $.trim($(el).find('.id').text());
					
				}

			});
					// get the charges detail
					var counter = 0;
					$(elem).find('.charges').each(function(indexC, elemC) {

						namount = $.trim($(elemC).find('td').last().text());
						var amount = $.trim($(elemC).text()),
						amount = parseFloat(amount).toFixed(2);
						arears = parseFloat($(elemC).data('arears')).toFixed(2);


						trs +=	"<tr>"+
						"<td class='medium'>"+ chargesName[counter] +"</td>"+
						"<td class='medium text-right'> "+ parseFloat(parseFloat(amount).toFixed(0)).toLocaleString() +"</td>"+
						"</tr>";

						counter++;
						
						t_amount +=parseFloat(parseFloat(amount));
						tamount =parseFloat(parseFloat(t_amount) + parseFloat(arears));
						

						totoalamount =parseFloat(parseFloat(tamount)+ parseFloat(latefee));


						
					});

					

					



					col = "";
					row = "";

					row = "<div class='row slips'>";
					col = 	"<div class='col-xs-4'>"+
					"<div class='slip' >"+
					"<div class='row divider' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4'><img src='../../../assets/img/printlogo/schoollogo.png' alt='' class='schoollogo' /></div>"+
					"<div class='col-xs-8 bold text-center' ><span > "+Branchname+" </span></div>"+

					"<div class='col-xs-8 text-center'><span> "+Branchaddress+"</span></div>"+

					"<div class='col-xs-8 text-center'><span>  "+Branchphone+"</span></div>"+
					"</div>"+
					
					"<div class='row hide' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Campus: </span></div>"+
					"<div class='col-xs-8 small'><span>LKG School house # 363E Adam Chowk</span></div>"+

					"</div>"+
					"<div class='row hide' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>A/C Title: </span></div>"+
					"<div class='col-xs-8 small'><span>LKG School house</span></div>"+
					"</div>"+
					"<div class='row hide' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Credit A/C #: </span></div>"+
					"<div class='col-xs-8 small'><span>000-</span></div>"+
					"</div>"+
					"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Challan No: </span></div>"+
					"<div class='col-xs-2 small'><span>"+ pad(dcno,4) + _stdid +"</span></div>"+
					"<div class='col-xs-3 small bold'><span>Print Date:: </span></div>"+
					"<div class='col-xs-3 small'><span>"+ printDate +"</span></div>"+
					"</div>"+
					
					"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Student Name: </span></div>"+
					"<div class='col-xs-8 small'><span>"+ name +"</span></div>"+
					"</div>"+
					"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Acad Year: </span></div>"+
					"<div class='col-xs-2 small'><span>"+ d.getFullYear() +"</span></div>"+
					"<div class='col-xs-3 small bold'><span>Student Id: </span></div>"+
					"<div class='col-xs-3 small'><span>"+ _stdid +"</span></div>"+
					"</div>"+
					"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Class/Section: </span></div>"+
					"<div class='col-xs-8 small'><span> "+ className +" / "+ sectionName +"</span></div>"+
					"</div>"+
					
					"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4 small bold'><span>Fee For: </span></div>"+
					"<div class='col-xs-8 small'><span>"+ feeFor +"</span></div>"+
					"</div>"+
					"<div class='row divider'></div>"+
					"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-12'>"+
					"<table class='table'>"+
					"<tr class='thead'>"+
					"<td style='border-top: 1px solid #333;'><span class='small bold'>Fee Particulars</span></td>"+
					"<td style='border-top: 1px solid #333;'><span class='small bold'>Amount</span></td>"+
					"</tr>"+
					trs +
					"<td style='border-top: 1px solid #333;'><span class='small bold'>Total</span></td>"+
					"<td style='border-top: 1px solid #333;text-align:right; '><span class='small bold'>"+parseFloat(parseFloat(t_amount).toFixed(0)).toLocaleString()+"</span></td>"+

					"</table>"+
					"</div>"+
					"</div>"+
					"<div class='row' style='margin-right:1px;'>"+
					"<div class='col-xs-8 small bold'><span class='pull-right'>Arears: </span></div>"+
					"<div class='col-xs-4 medium text-right'><span> "+ parseFloat(parseFloat(arears).toFixed(0)).toLocaleString() +"</span></div>"+
					"</div>"+

					"<div class='row divider' >"+
					"<div class='col-xs-8 small bold'><span class='pull-right'>Amount Payable:(With In Due Date) </span></div>"+
					"<div class='col-xs-4 medium text-right'><span style='font-size: 15px!important;font-weight:  bold;'> Rs."+  parseFloat(tamount).toLocaleString() +"</span></div>"+
					"</div>"+
					
					
					"<div class='row divider'>"+
					"<div class='col-xs-8 small bold'><span class='pull-right'>Payable After:"+lastDate.split("/").reverse().join("-")+" </span></div>"+
					"<div class='col-xs-4 medium text-right'><span class='topline'>Rs. "+ parseFloat(parseFloat(totoalamount).toFixed(0)).toLocaleString()  +"</span></div>"+
					"</div>"+
					"<div class='box'>"+
					"<div class='row' style='margin-right:1px;'>"+
					"<div class='col-xs-12 small bold'><span class='underline'>Amount In Words: </span></div>"+
					"</div>"+
					"<div class='row'>"+
					"<div class='col-xs-12 small inwords'><span> "+ toWords(tamount) +"</span></div>"+
					"</div>"+
					"</div>"+
					"<div class='box' style='border-top: none;'>"+
					"<div class='row' style='margin-left:1px;'>"+
					"<div class='col-xs-5 small bold'><span>Received Date: </span></div>"+
					"<div class='col-xs-7 medium'><span></span></div>"+
					"</div>"+
					"</div>"+
					"<div class='box' style='border-top: none;'>"+
					"<div class='row'>"+
					"<div class='col-xs-12 small bold center'><span>1. Student Copy</span></div>"+
					"</div>"+
					"</div>"+

					"<div class='row' style='margin-left:1px;'>"+
					"<div class='col-xs-12 xs-small'><span>Note: </span></div>"+
					"</div>"+
					"<div class='row' style='margin-left:1px;'>"+
					"<div class='col-xs-12 xs-small'><span>1 - The challan must be deposited within due date to avoid late payment charges. Which is Rs "+ latefee +"/- per day. </span></div>"+
					"</div>"+
					"<div class='row ' style='margin-left:1px;'>"+
					"<div class='col-xs-12 xs-small'><span>2 - The fee may only be deposited in the Designated Bank Branch. The fee will not be received through cheques.</span></div>"+
					"</div>"+
					"<div class='row' style='margin-left:1px;'>"+
					"<div class='col-xs-12 xs-small'><span>3 - The fee once paid will not be refunded.</span></div>"+
					"</div>"+
					"</div>"+
					"</div>";

					col += 	"<div class='col-xs-4'>"+
					"<div class='slip'>"+
					"<div class='row divider' style='margin-right:1px; margin-left: -9px;'>"+
					"<div class='col-xs-4'><img src='../../../assets/img/printlogo/schoollogo.png' alt='' class='schoollogo' /></div>"+
					"<div class='col-xs-8 bold text-center'><span> "+Branchname+" </span></div>"+

					"<div class='col-xs-8 text-center'><span> "+Branchaddress+"</span></div>"+

					"<div class='col-xs-8 text-center'><span>  "+Branchphone+"</span></div>"+
					"</div>"+
							// "<div class='row divider'>"+
							// 	"<div class='col-xs-5'><p class='center small'>Susan Road Branch Faisalabad</p></div>"+
							// 	"<div class='col-xs-2'><p class='center small'>-</p></div>"+
							// 	"<div class='col-xs-5'><p class='center small'>Susan Road Branch Faisalabad</p></div>"+
							// "</div>"+
							"<div class='row hide'>"+
							"<div class='col-xs-4 small bold'><span>Campus: </span></div>"+
							"<div class='col-xs-8 small'><span>LKG School house # 363E Adam Chowk</span></div>"+
							"</div>"+
							"<div class='row hide'>"+
							"<div class='col-xs-4 small bold'><span>A/C Title: </span></div>"+
							"<div class='col-xs-8 small'><span>LKG School house</span></div>"+
							"</div>"+
							"<div class='row hide'>"+
							"<div class='col-xs-4 small bold'><span>Credit A/C #: </span></div>"+
							"<div class='col-xs-8 small'><span>000-</span></div>"+
							"</div>"+
							"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
							"<div class='col-xs-4 small bold'><span>Challan No: </span></div>"+
							"<div class='col-xs-2 small'><span>"+ pad(dcno,4) + _stdid +"</span></div>"+
							"<div class='col-xs-3 small bold'><span>Print Date:: </span></div>"+
							"<div class='col-xs-3 small'><span>"+ printDate +"</span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span> Student Name: </span></div>"+
							"<div class='col-xs-8 small'><span>"+ name +"</span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span>Acad Year: </span></div>"+
							"<div class='col-xs-2 small'><span>"+ d.getFullYear() +"</span></div>"+
							"<div class='col-xs-3 small bold'><span>Student Id: </span></div>"+
							"<div class='col-xs-3 small'><span>"+ _stdid +"</span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span>Class/Section: </span></div>"+
							"<div class='col-xs-8 small'><span> "+ className +" / "+ sectionName +"</span></div>"+
							"</div>"+
							
							"<div class='row' >"+
							"<div class='col-xs-4 small bold'><span>Fee For: </span></div>"+
							"<div class='col-xs-8 small'><span>"+ feeFor +"</span></div>"+
							"</div>"+
							"<div class='row divider'></div>"+
							"<div class='row' style='margin-right:1px;'>"+
							"<div class='col-xs-12'>"+
							"<table class='table'>"+
							"<tr class='thead'>"+
							"<td style='border-top: 1px solid #333;'><span class='small bold'>Fee Particulars</span></td>"+
							"<td style='border-top: 1px solid #333;'><span class='small bold'>Amount</span></td>"+
							"</tr>"+
							trs +
							"<td style='border-top: 1px solid #333;'><span class='small bold'>Total</span></td>"+
							"<td style='border-top: 1px solid #333; text-align:right;'><span class='small bold'>"+parseFloat(parseFloat(t_amount).toFixed(0)).toLocaleString()+"</span></td>"+
							"</table>"+
							"</div>"+
							"</div>"+
							"<div class='row' style='margin-right:1px;'>"+
							"<div class='col-xs-8 small bold'><span class='pull-right'>Arears: </span></div>"+
							"<div class='col-xs-4 medium text-right'><span> "+ parseFloat(parseFloat(arears).toFixed(0)).toLocaleString()+"</span></div>"+
							"</div>"+

							"<div class='row divider'>"+
							"<div class='col-xs-8 small bold'><span class='pull-right'>Amount Payable:(With In Due Date) </span></div>"+
							"<div class='col-xs-4 medium text-right'><span style='font-size: 15px!important;font-weight:  bold;'> Rs."+ parseFloat(tamount).toLocaleString() +"</span></div>"+
							"</div>"+
							
							
							"<div class='row divider'>"+
							"<div class='col-xs-8 small bold'><span class='pull-right'>Payable After:"+lastDate.split("/").reverse().join("-")+" </span></div>"+
							"<div class='col-xs-4 medium text-right'><span class='topline'>Rs. "+ parseFloat(parseFloat(totoalamount).toFixed(0)).toLocaleString()  +"</span></div>"+
							"</div>"+
							"<div class='box'>"+
							"<div class='row'>"+
							"<div class='col-xs-12 small bold'><span class='underline'>Amount In Words: </span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-12 small inwords'><span> "+ toWords(tamount) +"</span></div>"+
							"</div>"+
							"</div>"+
							"<div class='box' style='border-top: none;'>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-5 small bold'><span>Received Date: </span></div>"+
							"<div class='col-xs-7 medium'></div>"+
							"</div>"+
							"</div>"+
							"<div class='box' style='border-top: none;'>"+
							"<div class='row'>"+
							"<div class='col-xs-12 small bold center'><span>2. Bank Copy</span></div>"+
							"</div>"+
							"</div>"+

							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>Note: </span></div>"+
							"</div>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>1 - The challan must be deposited within due date to avoid late payment charges. Which is Rs "+ latefee +"/- per day. </span></div>"+
							"</div>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>2 - The fee may only be deposited in the Designated Bank Branch. The fee will not be received through cheques.</span></div>"+
							"</div>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>3 - The fee once paid will not be refunded.</span></div>"+
							"</div>"+
							"</div>"+
							"</div>";

							col += 	"<div class='col-xs-4'>"+
							"<div class='slip'>"+
							"<div class='row divider' style='margin-right:1px; margin-left: -9px;'>"+
							"<div class='col-xs-4'><img src='../../../assets/img/printlogo/schoollogo.png' alt='' class='schoollogo' /></div>"+
							"<div class='col-xs-8 bold text-center'><span> "+Branchname+" </span></div>"+

							"<div class='col-xs-8 text-center'><span> "+Branchaddress+"</span></div>"+

							"<div class='col-xs-8 text-center'><span>  "+Branchphone+"</span></div>"+
							"</div>"+
							// "<div class='row divider'>"+
							// 	"<div class='col-xs-5'><p class='center small'>Susan Road Branch Faisalabad</p></div>"+
							// 	"<div class='col-xs-2'><p class='center small'>-</p></div>"+
							// 	"<div class='col-xs-5'><p class='center small'>Susan Road Branch Faisalabad</p></div>"+
							// "</div>"+
							"<div class='row hide'>"+
							"<div class='col-xs-4 small bold'><span>Campus: </span></div>"+
							"<div class='col-xs-8 small'><span>LKG School house # 363E Adam Chowk</span></div>"+
							"</div>"+
							"<div class='row hide'>"+
							"<div class='col-xs-4 small bold'><span>A/C Title: </span></div>"+
							"<div class='col-xs-8 small'><span>LKG School house</span></div>"+
							"</div>"+
							"<div class='row hide'>"+
							"<div class='col-xs-4 small bold'><span>Credit A/C #: </span></div>"+
							"<div class='col-xs-8 small'><span>000-</span></div>"+
							"</div>"+
							"<div class='row' style='margin-right:1px; margin-left: -9px;'>"+
							"<div class='col-xs-4 small bold'><span>Challan No: </span></div>"+
							"<div class='col-xs-2 small'><span>"+ pad(dcno,4) + _stdid +"</span></div>"+
							"<div class='col-xs-3 small bold'><span>Print Date:: </span></div>"+
							"<div class='col-xs-3 small'><span>"+ printDate +"</span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span>Student Name: </span></div>"+
							"<div class='col-xs-8 small'><span>"+ name +"</span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span>Acad Year: </span></div>"+
							"<div class='col-xs-2 small'><span>"+ d.getFullYear() +"</span></div>"+
							"<div class='col-xs-3 small bold'><span>Student Id: </span></div>"+
							"<div class='col-xs-3 small'><span>"+ _stdid +"</span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span>Class/Section: </span></div>"+
							"<div class='col-xs-8 small'><span> "+ className +" / "+ sectionName +"</span></div>"+
							"</div>"+
							
							"<div class='row'>"+
							"<div class='col-xs-4 small bold'><span>Fee For: </span></div>"+
							"<div class='col-xs-8 small'><span>"+ feeFor +"</span></div>"+
							"</div>"+
							"<div class='row divider'></div>"+
							"<div class='row ' style='margin-right:1px;'>"+
							"<div class='col-xs-12'>"+
							"<table class='table'>"+
							"<tr class='thead'>"+
							"<td style='border-top: 1px solid #333;'><span class='small bold'>Fee Particulars</span></td>"+
							"<td style='border-top: 1px solid #333;'><span class='small bold'>Amount</span></td>"+
							"</tr>"+
							trs +
							"<td style='border-top: 1px solid #333;'><span class='small bold'>Total</span></td>"+
							"<td style='border-top: 1px solid #333;text-align:right;'><span class='small bold'>"+parseFloat(parseFloat(t_amount).toFixed(0)).toLocaleString()+"</span></td>"+
							"</table>"+
							"</div>"+
							"</div>"+
							"<div class='row' style='margin-right:1px;'>"+
							"<div class='col-xs-8 small bold'><span class='pull-right'>Arears: </span></div>"+
							"<div class='col-xs-4 medium text-right'><span> "+ parseFloat(parseFloat(arears).toFixed(0)).toLocaleString()+"</span></div>"+
							"</div>"+

							"<div class='row divider'>"+
							"<div class='col-xs-8 small bold'><span class='pull-right'>Amount Payable:(With In Due Date) </span></div>"+
							"<div class='col-xs-4 medium text-right'><span style='font-size: 15px!important;font-weight:  bold;'>Rs. "+ parseFloat(tamount).toLocaleString() +"</span></div>"+
							"</div>"+
							
							
							"<div class='row divider'>"+
							"<div class='col-xs-8 small bold'><span class='pull-right'>Payable After:"+lastDate.split("/").reverse().join("-")+" </span></div>"+
							"<div class='col-xs-4 medium text-right'><span class='topline'>Rs. "+ parseFloat(parseFloat(totoalamount).toFixed(0)).toLocaleString()  +"</span></div>"+
							"</div>"+
							"<div class='box'>"+
							"<div class='row'>"+
							"<div class='col-xs-12 small bold'><span class='underline'>Amount In Words: </span></div>"+
							"</div>"+
							"<div class='row'>"+
							"<div class='col-xs-12 small inwords'><span> "+ toWords(tamount) +"</span></div>"+
							"</div>"+
							"</div>"+
							"<div class='box' style='border-top: none;'>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-5 small bold'><span>Received Date: </span></div>"+
							"<div class='col-xs-7 medium'></div>"+
							"</div>"+
							"</div>"+
							"<div class='box' style='border-top: none;'>"+
							"<div class='row'>"+
							"<div class='col-xs-12 small bold center'><span>3. Campus Copy</span></div>"+
							"</div>"+
							"</div>"+

							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>Note: </span></div>"+
							"</div>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>1 - The challan must be deposited within due date to avoid late payment charges. Which is Rs "+ latefee +"/- per day. </span></div>"+
							"</div>"+
							"<div class='row' style='margin-left:1px;'> "+
							"<div class='col-xs-12 xs-small'><span>2 - The fee may only be deposited in the Designated Bank Branch. The fee will not be received through cheques.</span></div>"+
							"</div>"+
							"<div class='row' style='margin-left:1px;'>"+
							"<div class='col-xs-12 xs-small'><span>3 - The fee once paid will not be refunded.</span></div>"+
							"</div>"+
							"</div>"+
							"</div>";

							var temp = row + col;
							temp += "</div>";
							$(temp).appendTo('.slips-container');
						});
window.print();

$('.btnPdfDownload').on('click', function(e) {
	e.preventDefault();
});
</script>
</body>
</html>