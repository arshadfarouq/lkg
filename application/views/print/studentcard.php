<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student Card</title>

	<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
	<style>
		body {
			color: #333;
			font-family: tahoma;
		}
		#schoollogo {
			width: 140px;
			height: auto;
		}
		.center{
			text-align: center;
		}
		.cardheader {
			background: #92E769;
			padding: 10px;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
		}
		.cardbody {
			background: #C0FF96;
			height: 160px;
			border-bottom-left-radius: 5px;
			border-bottom-right-radius: 5px;
			padding: 10px;
		}
		.schoolname {
			font-family: "Open Sans Semibold";
			padding-top: 8px;
		}
		.divider {
			margin-bottom: 3px;
		}
		.justify {
			text-align: justify;
		}

		@media print {
			.btnPdfDownload {
				display: none;
			}

			.cardheader {
				background: #92E769 !important;
				-webkit-print-color-adjust: exact;
			}
			.cardbody {
				background: #C0FF96 !important;
				-webkit-print-color-adjust: exact;
			}
		}
	</style>
</head>
<body>

	<div id="wrap">
		<div class="container-fluid">

			<div class="row divider">
				<div class="col-xs-12">
					<a class="btn btn-default btnPdfDownload"><i class="fa fa-download"></i> PDF Download</a>
				</div>
			</div>

			<div class="row divider">
				<div class="col-xs-6">
					<div class="card cardfront">
						<div class="cardheader">
							<div class="row">

								<div class="col-xs-4">
									<div class="logo">
										<img src="../../../assets/img/printlogo/schoollogo.png" alt="" id='schoollogo' />
									</div>
								</div>

								<div class="col-xs-8">
									<div class="center schoolname">
										<p></p>
									</div>
								</div>

							</div>
						</div>
						<div class="cardbody">
							<div class="row divider ">
								<div class="col-xs-4"><span>Name</span></div>
								<div class="col-xs-8"><span id='txtName'> </span></div>
							</div>
							<div class="row divider">
								<div class="col-xs-4"><span>Father Name</span></div>
								<div class="col-xs-8"><span id='txtFatherName'> </span></div>
							</div>
							<div class="row divider">
								<div class="col-xs-4"><span>Class</span></div>
								<div class="col-xs-8"><span id='txtClass'> </span></div>
							</div>
							<div class="row divider">
								<div class="col-xs-4"><span>Section</span></div>
								<div class="col-xs-8"><span id='txtSection'> </span></div>
							</div>
							<div class="row divider">
								<div class="col-xs-4"><span>Roll No</span></div>
								<div class="col-xs-8"><span id='txtRollNo'> </span></div>
							</div>
							<div class="row divider">
								<div class="col-xs-4"><span>Date of Birth</span></div>
								<div class="col-xs-8"><span id='txtBirthDate'> </span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row divider"></div>
			<div class="row divider"></div>
			<div class="row divider"></div>
			<div class="row divider"></div>

			<div class="row divider">				
				<div class="col-xs-6">
					<div class="card cardback">
						<div class="cardheader">
							<div class="row">

								<div class="col-xs-4">
									<div class="logo">
										<img src="../../../assets/img/printlogo/schoollogo.png" alt="" id='schoollogo' />
									</div>
								</div>

								<div class="col-xs-8">
									<div class="center schoolname">
										<p></p>
									</div>
								</div>

							</div>
						</div>
						<div class="cardbody">
							<div class="row divider ">
								<div class="col-xs-4"><span>Address</span></div>
								<div class="col-xs-8"><span id='txtAddress'></span></div>
							</div>
							<div class="row divider">
								<div class="col-xs-4"><span>Valid Till</span></div>
								<div class="col-xs-8"><span id='validtill'> </span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>


	<script src='../../../assets/js/jquery.min.js'></script>
	<script src='../../../assets/bootstrap/js/bootstrap.min.js'></script>

	<script>

		var opener = window.opener;
		$('#txtName').text($.trim(opener.$('#txtStudentName').val()));
		$('#txtFatherName').text($.trim(opener.$('#txtFatherName').val()));
		$('#txtBirthDate').text($.trim(opener.$('#txtDOB').val().split('/').reverse().join('-')));
		$('#txtClass').text($.trim(opener.$('#admiClass_dropdown').find('option:selected').text()));
		$('#txtSection').text($.trim(opener.$('#section_dropdown').find('option:selected').text()));
		$('#txtRollNo').text($.trim(opener.$('#txtRollNo').val()));
		$('#txtAddress').text($.trim(opener.$('#txtAddress').val()));
		var date = new Date();
		$('#validtill').text(date.getFullYear());
		$('.schoolname p').text(opener.$('.address').val());

		window.print();

		$('.btnPdfDownload').on('click', function(e) {
			e.preventDefault();
		});
	</script>
</body>
</html>