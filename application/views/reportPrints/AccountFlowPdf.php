<!doctype html>
<html lang="en">
	<head>

		<meta charset="UTF-8">
		<title>Voucher</title>

		<style>
			* { margin: 0; padding: 0; font-family: tahoma; }
			body { font-size:12px; }
			p { margin: 0; /* line-height: 17px; */ }
			.field {font-weight: bold; display: inline-block; width: 120px; }
			.voucher-table{ border-collapse: collapse; }
			table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;margin-top: 8%;}
			th { border: 1px solid black; padding: 5px; }
			td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-bottom: 1px solid black; }
			td:first-child { text-align: left; }
			.voucher-table thead th {background: #ccc; } 
			tfoot {border-top: 1px solid black; } 
			.bold-td { font-weight: bold; border-bottom: 1px solid black;}
			.nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			.invoice-type { border-bottom: 1px solid black; }
			.relative { position: relative; }
			.signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			.signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			.inv-leftblock { width: 280px; }
			.text-left { text-align: left !important; }
			.text-right { text-align: right !important; }
			td {font-size: 8px; font-family: tahoma; line-height: 14px; padding: 2px; }
			.rcpt-header { width: 450px; margin: auto; display: block; }
			.inwords, .remBalInWords { text-transform: uppercase; }
			.barcode { margin: auto; }
			h3.invoice-type {font-size: 30px; line-height: 24px;}
			.extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			.nettotal { color: red; font-size: 12px;}
			.remainingBalance { font-weight: bold; color: blue;}
			.centered { margin: auto; }
			p { position: relative; font-size: 16px; }
			thead th { font-size: 13px; font-weight: normal; }

			@media print {
			 	.noprint, .noprint * { display: none; }
			}
			.pl20 { padding-left: 20px !important;}
			.pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 12px; padding: 10px; }

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative; top: 0px; }
			tfoot tr td { font-size: 12px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			tr{page-break-inside: avoid !important; }
		</style>
	</head>

	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
				<div class="span12 centered">
					<div class="span12">
						<?php if($header_img != "") {?>
							<div class="span12"><img style=" width:100% !important;height:100px;" class="" src="<?php echo $header_img;?>" alt=""></div>
						<?php }?>
					</div>
					<div class="row-fluid relative">
						<div class="span12">
							<br>
							<div class="block pull-left inv-leftblock" style="display:inline !important;">
								<span class="field" style="font-size: 13px;">Dated From: </span>
								<span class="fieldvalue" style="font-size: 13px;"><?php echo $date_between; ?></span>
								<span class="invoice-type text-left" style="position:relative; bottom:5px; float: right; font-size:17px; text-decoration: none; border-bottom:none;"><?php echo $title; ?></span>
							</div>
							<br>
						</div>
					</div>
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 150px; ">Account Detail</th>
									<th style=" width: 50px; ">Type</th>
									<th style=" width: 60px; ">In Flow</th>
									<th style=" width: 60px; ">Out Flow</th>
								</tr>
							</thead>

							<tbody>
								<?php 
									$serial = 1;
									$Total_Debit = 0.00;
									$Total_Credit = 0.00;
									
									if (empty($pledger)) {
            			
        							}
        							else{

										foreach ($pledger as $row): 
									
										$Total_Debit += $row['debit'];
									
										$Total_Credit += $row['credit'];

										$accountName = $row['party_name'];
										$accountNameE = array();

										$etype = '';//trim($row['etype']);
										if($etype == "cpv")
										{
											$etype = "Cash Payment";
										}
										elseif($etype == "crv")
										{
											$etype = "Cash Receipt";
										}
										elseif($etype == "pd_issue")
										{
											$etype = "Bank Payment";
										}
										elseif($etype == "pd_receive")
										{
											$etype = "Bank Receipt";
										}
										elseif($etype == "cnv")
										{
											$etype = "Credit Note";
										}
										elseif($etype == "dnv")
										{
											$etype = "Debit Note";
										}
								?>
									<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
									   <td  class='text-left'><?php echo trim($accountName); ?></td>
										<td style="text-align: center;"><?php echo $etype; ?></td>
									   <td  class="text-right"><?php echo number_format($row['debit'],2); ?></td>
									   <td  class="text-right"><?php echo number_format($row['credit'],2); ?></td>			   
									</tr>
								<?php   endforeach ?>
							</tbody>

							<tfoot>
								<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
									<td class="text-right " colspan="3">Opening: </td>
									<td class="text-right " style="padding-right: 5px;"><?php echo number_format($previousBalance,2); ?></td>
								</tr>
								<tr>
									<td class="text-right " colspan="3">Total In Flow: </td>
									<td class="text-right "><?php echo number_format($Total_Debit,2); ?></td>
								</tr>
								<tr>
									<td class="text-right " colspan="3">Total Out Flow: </td>
									<td class="text-right "><?php echo number_format($Total_Credit,2); ?></td>
								</tr>
								<tr>
									<td class="text-right " colspan="3">Balance: </td>
									<td class="text-right "><?php echo number_format(($Total_Debit + $previousBalance) - $Total_Credit ,2); ?></td>
								</tr>
							</tfoot>
							<?php   } ?>
						</table>
					</div>
					<br> 
					<br> 
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th>Accountant</th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="row-fluid">
						<p>
							<span class="loggedin_name">User: <?php echo $user; ?></span><br>
							<span class="website">Powered By: www.alnaharsolutions.pk (Online Web Based ERP)</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>	