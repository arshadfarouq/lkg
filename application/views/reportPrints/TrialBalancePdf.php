<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Voucher</title>

	<style>
		  * { margin: 0; padding: 0; font-family: tahoma; }
		 body { font-size:12px;  }
		 p { margin: 0; /* line-height: 17px; */ }
		 .field {font-weight: bold; display: inline-block; width: 150px; } 
		 .voucher-table{ border-collapse: collapse; }
		 table { width: 100%; border:none; border-collapse:collapse; table-layout:fixed;margin-top: 8%;}
		 th { }
		 tr{ page-break-inside: avoid;}
		 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
		 td:first-child { text-align: left; }
		 .voucher-table thead th { } 
		 .headerrr {background: #ccc; border: 1px solid black; padding: 5px; } 
		 tfoot {border-top: 1px solid black; } 
		 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
		 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
		 .invoice-type { border-bottom: 1px solid black; }
		 .relative { position: relative; }
		 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
		 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; font-size:12px }
		 .inv-leftblock { width: 280px; }
		 .text-left { text-align: left !important; }
		 .text-right { text-align: right !important; }
		 td {font-size: 8px; font-family: tahoma; line-height: 14px; padding: 5px;border-top: 0.5px solid black !important; } 
		 
		 .inwords, .remBalInWords { text-transform: uppercase; }
		 .barcode { margin: auto; }
		 h3.invoice-type {font-size: 20px; line-height: 24px;}
		 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
		 .nettotal { color: red; font-size: 12px;}
		 .l1row { color: red; font-size: 9px;font-weight: bold;}
		 .l1row-right { color: red; font-size: 9px;font-weight: bold;text-align: right !important;}

		 .l2row { color: green; font-size: 9px;font-weight: bold;}
		 .l2row-right { color: green; font-size: 9px;font-weight: bold;text-align: right !important;}

		 .l3row { color: blue; font-size: 9px;font-weight: bold;}
		 .l3row-right { color: blue; font-size: 9px;font-weight: bold;text-align: right !important;}

		 .remainingBalance { font-weight: bold; color: blue;}
		 .centered { margin: auto; }
		 p { position: relative; font-size: 16px; }
		 thead th { }
		 .headerrr td { font-size: 12px; font-weight: bold; }

		 .fieldvalue.cust-name {position: absolute; width: 497px; } 
		 @media print {
		 	.noprint, .noprint * { display: none; }
		 }
		 .pl20 { padding-left: 20px !important;}
		 .pl40 { padding-left: 40px !important;}
			
		.barcode { float: right; }
		.item-row td { font-size: 12px; padding: 5px; border: none;}
		.grandrow-right { color: black; font-size: 9px;font-weight: bold;text-align: right !important; border: none !important;}
		.rcpt-header { width: 700px !important; margin: 0px; display: inline;  top: 0px; right: 0px; }
		h3.invoice-type {font-size: 26px; border: none !important; margin: 0px !important; position: relative;  }
		tfoot tr td { font-size: 13px; padding: 5px; }
		.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
		table {margin-right: 200px !important;}
	</style>
</head>
<body>
	<div class="container-fluid" style="">
<div class="row-fluid">
	<div class="span12 centered">
		<div class="row-fluid">
						<div class="span12"><img class="rcpt-header" src="<?php echo $header_img;?>" alt=""></div>
					
		</div>
		<div class="row-fluid relative">
			<div class="span12">
					<div class="block pull-left inv-leftblock" style="width:150px !important; display:inline !important;">
						
						<p><span class="field">Dated From: </span><span class="fieldvalue inv-date"><?php echo $date_between; ?></span></p>
					</div>
					<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
						<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 2px !important; font-size:16px !important; "><?php echo $title;?></h3>
					</div>

			</div>
		</div>
		
		
		<div class="row-fluid">
			<table class="voucher-table">
				<thead>
					<tr class="headerrr">
						<th style=" width: 80px; ">Acc Id</th>
						<th style=" width: 250px; ">Account Title</th>
						<th style=" width: 80px; ">Debit</th>
						<th style=" width: 80px; ">Credit</th>
					</tr>
				</thead>

		<tbody>
			
			<?php 
				$serial = 1;
				$Total_Debit = 0;
				$Total_CREDIT = 0;
				$Rtotal = 0;
				// $Rtotal= $previousBalance;
				$l1='';
				$l2='';
				$l3='';
				
				if (empty($pledger)) {
						//location('payment');
					}
				else{

				foreach ($pledger as $row): 
				
						$Total_Debit += $row['DEBIT'];
				
						$Total_CREDIT += $row['CREDIT'];

						// $Rtotal += $row['DEBIT']-$row['CREDIT'];
			?>
				<?php if ($l1 !=$row['L1'] ){ ?>
					<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
					   <td 	class='l1row' ><?php echo $row['L1']; ?> </td>
					   <td  class='l1row'><?php echo $row['L1NAME']; ?></td>
					   <td  class="l1row-right"><?php echo ($row['L1DebSUM']!=0 ?  number_format($row['L1DebSUM'],2):'-'); ?></td>
					   <td  class="l1row-right"><?php echo ($row['L1CredSUM']!=0 ?  number_format($row['L1CredSUM'],2):'-'); ?></td>
					</tr>

				<?php $l1= $row['L1'];} 
						
				 ?>
				 <?php if ($l2 !=$row['L2'] ){ ?>
					<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
					   <td 	class='l2row' ><?php echo $row['L1'] .'-'. $row['L2'] ; ?> </td>
					   <td  class='l2row'><?php echo $row['L2NAME']; ?></td>
					   <td  class="l2row-right"><?php echo ($row['L2DebSUM']!=0 ?  number_format($row['L2DebSUM'],2):'-'); ?></td>
					   <td  class="l2row-right"><?php echo ($row['L2CredSUM']!=0 ?  number_format($row['L2CredSUM'],2):'-'); ?></td>
					</tr>

				<?php $l2= $row['L2'];} 
						
				 ?>
				 <?php if ($l3 !=$row['L3'] ){ ?>
					<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
					   <td 	class='l3row' ><?php echo $row['L1'].'-'.$row['L2'].'-'. $row['L3']; ?> </td>
					   <td  class='l3row'><?php echo $row['L3NAME']; ?></td>
					   <td  class="l3row-right"><?php echo ($row['L3DebSUM']!=0 ?  number_format($row['L3DebSUM'],2):'-'); ?></td>
					   <td  class="l3row-right"><?php echo ($row['L3CredSUM']!=0 ?  number_format($row['L3CredSUM'],2):'-'); ?></td>
					</tr>

				<?php $l3= $row['L3'];} 
						
				 ?>
				<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
				   <td 	class='text-left' ><?php echo $row['ACCOUNT_ID']; ?> </td>
				   <td  class='text-left'><?php echo $row['PARTY_NAME']; ?></td>
				   <td  class="text-right"><?php echo ($row['DEBIT']!=0 ? number_format($row['DEBIT'],2):'-'); ?></td>
				   <td  class="text-right"><?php echo ($row['CREDIT']!=0 ? number_format($row['CREDIT'],2):'-'); ?></td>
				</tr>

			<?php   endforeach ?>
			<?php  // } ?>
		</tbody>
				<tfoot>
				<!-- 	<tr class="foot-comments">
						<td class="vrqty bold-td text-right"><?php //echo $Total_DEBIT; ?></td>
						<td class="bold-td text-right" colspan="3">Subtotal</td>
						<td class="bold-td"></td>
					</tr> -->
					<tr style="amountborder-bottom:1px dotted #ccc;" class="item-row">
						<!-- <td class="bold-td"></td> -->
						<td class="text-right " colspan="2">Total</td>
						<td class="text-right "><?php echo number_format($Total_Debit,2); ?></td>
						<td class="text-right "><?php echo number_format($Total_CREDIT,2); ?></td>
					</tr>
				</tfoot>
				<?php   } ?>
			</table>
		</div>
		<!-- <div class="row-fluid">
			<div class="span12 add-on-detail" style="margin-top: 10px;">
				<p class="" style="text-transform: uppercase;">
					<strong>In words: </strong> <span class="inwords"></span>  <?php echo $Total_Amount; ?> ONLY <br>	
				</p>
			</div>
		</div> -->
		<!-- End row-fluid -->
		<br> 
		<br> 
		<br> 
		<br> 
		<div class="row-fluid">
			<div class="span12">
				<table class="signature-fields">
					<thead>
						<tr>
							<th >Approved By</th>
							<th>Accountant</th>
							<th>Received By</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div class="row-fluid">
			<p>
				<span class="loggedin_name">User: <?php echo $user; ?></span><br>
				<span class="website">Sofware By: www.alnaharsolutions.com, Mob: 03009663902</span>
			</p>
		</div>

	</div>
</div>
</div>
</body>
</html>	