<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma; }
			 body { font-size:12px; }
			 p { margin: 0; /* line-height: 17px; */ }
			 .field {font-weight: bold; display: inline-block; width: 160px;margin-top: 15px; } 
			 .voucher-table{ border-collapse: collapse;margin-top: -30px; }
			 table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
			 th { border: 1px solid black; padding: 5px; }
			 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
			 td:first-child { text-align: left; }
			 .voucher-table thead th {background: #ccc; } 
			 tfoot {border-top: 1px solid black; } 
			 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
			 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			 .invoice-type { border-bottom: 1px solid black; }
			 .relative { position: relative; }
			 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			 .inv-leftblock { width: 280px; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
			 .rcpt-header { width: 450px; margin: auto; display: block; }
			 .inwords, .remBalInWords { text-transform: uppercase; }
			 .barcode { margin: auto; }
			 h3.invoice-type {font-size: 20px; line-height: 24px;}
			 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			 .nettotal { color: red; font-size: 12px;}
			 .remainingBalance { font-weight: bold; color: blue;}
			 .centered { margin: auto; }
			 p { position: relative; font-size: 16px; }
			 thead th { font-size: 13px; font-weight: normal; }
			 .fieldvalue.cust-name {position: absolute; width: 497px; } 
			 @media print {
			 	.noprint, .noprint * { display: none; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 15px; padding: 10px;}

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important;}
			tfoot tr td { font-size: 13px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			.footer{clear: both;width: 100%;position: relative;top: 700px; display: inline-block;}
			.software{width: 45%;display: inline-block;text-align: left;}
			.Pages{width: 54%;display: inline-block;text-align: right;}
						table tbody td{border: none;}
			table tbody td{border-bottom: 1px solid black;}

			/*.{padding-top: 6%;}*/
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
			
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:550px !important; display:inline-block !important;">
									<h3 class="invoice-type text-left" style="font-size: 22px; border:none !important; margin: 0px !important; "><?php echo $title; ?></h3>

									<p><span class="field">From :</span><span class="fieldvalue inv-date"><?php echo  $from; ?></span></p>
									
									<p><span class="field">To :</span><span class="fieldvalue inv-date"><?php echo  $to; ?></span></p>
									<p><span class="field">Account :</span><span class="fieldvalue inv-date"><?php echo  str_replace("%20"," ",$accountName); ?></span></p>
									
									<p><span class="field">Opening Balance :</span><span class="fieldvalue inv-date"><?php echo  $openBal; ?></span></p>
									<p><span class="field">Total Debit :</span><span class="fieldvalue inv-date"><?php echo  $debitBal; ?></span></p>
									<p><span class="field">Total Credit :</span><span class="fieldvalue inv-date"><?php echo  $cerditBal; ?></span></p>
									<p><span class="field">Closing Balance :</span><span class="fieldvalue inv-date"><?php echo  $runBal; ?></span></p>

									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
								</div> --> 
								<div class="block pull-right" style="width:900px !important; float: right; display:inline !important;">
									<div class="span12"><img style="float:right; width:300px !important;" class="rcpt-header logo-img" src="<?php echo $header_img; ?>" alt=""></div>
									
									
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<!-- <th style="color: #000 !important;">Sr#</th>
									<th style="color: #000 !important;">Party ID</th> -->
									<th width="60px;" class="text-left" style="">Date</th>
									<th width="40px" class="text-left" style="">Vr#</th>
									<th width="80" class="text-left" style="">Type</th>
									<th width="200px" class="text-left" style="">Description</th>
									<th width="60px" class="text-left" style="">Debit</th>
									<th width="60px" class="text-left" style="">Credit</th>
									<th width="60px" class="text-left" style="">Total</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$netDebit = 0;
									$netCredit = 0;
									$netRTotal = 0;
									
									
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									foreach ($vrdetail as $key=>$row):

										$datalenght = count($vrdetail);
										$netDebit += floatval($row['DEBIT']);
										$netCredit += floatval($row['CREDIT']);
										if ($key == ($datalenght- 1)) {
											$netRTotal = $row['RTotal'];
										}

										$RTotal = 0;
										if (floatval($row['RTotal']) > 0) {
											$RTotal = $row['RTotal'] . " Dr";
												// alert(RTotal);
										} else {
											$RTotal = $row['RTotal'] . " Cr";
												// alert("RTotal is"+RTotal);
										}

								?>
								<tr>
									<td> <?php echo substr($row['VRDATE'],0,10); ?> </td>
									<td> <?php echo $row['VRNOA']; ?> </td>
									<td> <?php echo $row['ETYPE']; ?> </td>
									<td  > <?php echo $row['DESCRIPTION']; ?> </td>
									<td class="text-right" > <?php echo $row['DEBIT']; ?> </td>
									<td class="text-right" > <?php echo $row['CREDIT']; ?> </td>
									<td class="text-right"> <?php echo $RTotal; ?> </td>
								</tr>
								

								<?php endforeach ?>
							
								
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
			
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>	