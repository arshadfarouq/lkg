<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma; }
			 body { font-size:12px; }
			 p { margin: 0; /* line-height: 17px; */ }
			 .field {font-weight: bold; display: inline-block; width: 100px;margin-top: 15px; } 
			 .voucher-table{ border-collapse: collapse;margin-top: -5px; }
			 table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
			 th { border: 1px solid black; padding: 5px; }
			 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
			 td:first-child { text-align: left; }
			 .voucher-table thead th {background: #ccc; } 
			 tfoot {border-top: 1px solid black; } 
			 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
			 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			 .invoice-type { border-bottom: 1px solid black; }
			 .relative { position: relative; }
			 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			 .inv-leftblock { width: 280px; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
			 .rcpt-header { width: 450px; margin: auto; display: block; }
			 .inwords, .remBalInWords { text-transform: uppercase; }
			 .barcode { margin: auto; }
			 h3.invoice-type {font-size: 20px; line-height: 24px;}
			 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			 .nettotal { color: red; font-size: 12px;}
			 .remainingBalance { font-weight: bold; color: blue;}
			 .centered { margin: auto; }
			 p { position: relative; font-size: 16px; }
			 thead th { font-size: 13px; font-weight: normal; }
			 .fieldvalue.cust-name {position: absolute; width: 497px; } 
			 @media print {
			 	.noprint, .noprint * { display: none; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 15px; padding: 10px;}

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important;}
			tfoot tr td { font-size: 13px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			.footer{clear: both;width: 100%;position: relative;top: 700px; display: inline-block;}
			.software{width: 45%;display: inline-block;text-align: left;}
			.Pages{width: 54%;display: inline-block;text-align: right;}
						table tbody td{border: none;}
			table tbody td{border-bottom: 1px solid black;}
			.dont-show{display: none !important;}

			/*.{padding-top: 6%;}*/
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
			
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:550px !important; display:inline-block !important;">
									<h3 class="invoice-type text-left" style="font-size: 22px; border:none !important; margin: 0px !important; "><?php echo $title; ?></h3>

									<p><span class="field">From : </span><span class="fieldvalue inv-vrnoa"><?php echo $startDate; ?></span></p>									
									<p><span class="field">To :</span><span class="fieldvalue inv-date"><?php echo  $endDate; ?></span></p>
									
									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
								</div> --> 
								<div class="block pull-right" style="width:900px !important; float: right; display:inline !important;">
									<div class="span12"><img style="float:right; width:300px !important;" class="rcpt-header logo-img" src="<?php echo $header_img; ?>" alt=""></div>
									
									
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
									<?php
									function payment_head_template(){
										echo "<tr>";
											echo "<th width='20px;' class='text-left'>". "Serial" . "</th>";
											echo "<th width='40px;' class='text-left'>". "Date" . "</th>";
											echo "<th width='20px;' class='text-left'>". "Vr #" . "</th>";
											echo "<th width='100px;' class='text-left'>". "Account" . "</th>";
											echo "<th width='100px;' class='text-left'>". "Remarks" ."</th>";
											echo "<th width='100px;' class='text-left'>". "Amount" . "</th>";
										echo "</tr>";
									}
									// Selecting Head id payment-head-template
										if ($etype === 'cpv' || $etype === 'crv') {
											echo payment_head_template();
										} 
										elseif ($etype === 'jv') {
												echo "<tr>";
													echo "<th class='text-left'>". "Serial" . "</th>";
													echo "<th class='text-left'>". "Date" . "</th>";
													echo "<th class='text-left'>". "Vr #" . "</th>";
													echo "<th class='text-left'>". "Remarks" ."</th>";
													echo "<th class='text-left'>". "Credit" . "</th>";
													echo "<th class='text-left'>". "Debit" . "</th>";
												echo "</tr>";
										} 
										elseif ($etype === 'expense') {
											echo payment_head_template();
										} 
										elseif ($etype === 'daybook') {
											echo "<tr>";
												echo "<th class='text-left'>". "Serial" . "</th>";
												echo "<th class='text-left'>". "Date" . "</th>";
												echo "<th class='text-left'>". "Vr #" . "</th>";
												echo "<th class='text-left'>". "Account" . "</th>";
												echo "<th class='text-left'>". "Remarks" ."</th>";
												echo "<th class='dont-show text-left'> ". "Etype" . "</th>";
												echo "<th class='text-left'>". "Debit" . "</th>";
												echo "<th class='text-left'>". "Credit" . "</th>";
											echo "</tr>";
										} 
										elseif ($etype === 'payable' || $etype === 'receiveable') {
											echo "<tr>";
												echo "<th class='text-left'>". "Serial" . "</th>";
												echo "<th class='text-left'>". "Account" . "</th>";
												echo "<th class='text-left'>". "Address" . "</th>";
												echo "<th class='text-left'>". "Email" . "</th>";
												echo "<th class='text-left'>". "Contact" ."</th>";
												echo "<th class='dont-show text-left'> ". "Phone" . "</th>";
												echo "<th class='text-left'>". "Balance" . "</th>";
											echo "</tr>";
										} 
									?>
									<!-- <tr>
										<th class="no_sort">Sr#
										</th>
										<th class="no_sort">Date
										</th>
										<th class="no_sort">Voucher #
										</th>
										<th class="no_sort">Account
										</th>
										<th class="no_sort">Remarks
										</th>
										<th class="no_sort">Amount
										</th>
 								</tr> -->
							</thead>

							<tbody>
								
								<?php 
								// Cpv
									 $prevDate = '';
									 $prevParty = '';
									 $prevInvoice = '';
									 $netSum = 0;
									 $subSum = 0;
									 $Serial = 1;
								// jv
									 $netCredit = 0;
									 $subCredit = 0;
									 $netDebit = 0;
									 $subDebit = 0;

									// $sr = 0;
									// $
									 //$netSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);

									
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									
									 
									foreach ($vrdetail as $key=>$row):
										 $datalenght = count($vrdetail);
										// $netSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);
										// $netSum += $row['AMOUNT'];
									if ($etype === 'cpv' || $etype === 'crv') {
										$netSum += $row['AMOUNT'];//basically yeh outside tha if ke

										if (($what === 'date') && ($prevDate !== $row['DATE'])) {

										    if ($key !== 0) {
										        // echo out the date head id payment-subsum-template
										        echo "<tr>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td class='text-right' style='color:blue;' >". "Sub Total" ."</td>";
										        	echo "<td class='text-right' style='color:blue;'>". $subSum . "</td>";
										        	// echo "<td>". $row['subSum'] . "</td>";
										        echo "</tr>";
										        $subSum = 0;
										    }

										    // echo out the date head id payment-dhead-template
										    echo "<tr>"; 
										    	echo "<td>". "" . "</td>";
										    	echo "<td style='color:red;'>". substr($row['DATE'],0,10) . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" ."</td>";
										    echo "</tr>";

										    $prevDate = $row['DATE'];
										}

										else if (($what === 'invoice') && ($prevInvoice !== $row['VRNOA'])) {

										    if ($key !== 0) {
										    	//  Same as Above payment-subsum-template
										    	echo "<tr>";
										    		echo "<td>". "" . "</td>";
										    		echo "<td>". "" . "</td>";
										    		echo "<td>". "" . "</td>";
										    		echo "<td>". "" . "</td>";
										    		echo "<td class='text-right' style='color:blue;' >". "Sub Total" ."</td>";
										    		echo "<td class='text-right' style='color:blue;'>". $subSum . "</td>";
										    		// echo "<td>". $row['SUBSUM'] . "</td>";subSum
										    	echo "</tr>";
										      

										        $subSum = 0;
										    }

										    

										    //  payment-ihead-template
										    echo "<tr>";  
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". $row['VRNOA']."</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" . "</td>";
										    echo "</tr>";

										    $prevInvoice = $row['VRNOA'];
										}
										else if (($what === 'party') && ($prevParty !== $row['PARTY'])) {

										    if ($key !== 0) {
										    
										        //  Same as Above payment-subsum-template
										        echo "<tr>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" ."</td>";
										        	echo "<td class='text-right' style='color:blue;' >". "Sub Total" . "</td>";
										        	echo "<td class='text-right' style='color:blue;'>". $subSum."</td>";
										        	// echo "<td>". $row['SUBSUM']."</td>";
										        echo "</tr>";

										        $subSum = 0;
										    }

										    echo "<tr>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". $row['PARTY']."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" . "</td>";
										    echo "</tr>";
										    $prevParty = $row['PARTY'];
										}
										//  payment-row-template
										echo "<tr>";
											echo "<td>". $Serial++."</td>";
											echo "<td>". substr($row['DATE'],0,10)."</td>";
											echo "<td>". $row['VRNOA']."</td>";
											echo "<td>". $row['PARTY']."</td>";
											echo "<td>". $row['REMARKS']."</td>";
											echo "<td class='text-right'>". $row['AMOUNT']."</td>";
										echo "</tr>";
									


										// $subSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);
										// $subSum +=  (floatval($row['AMOUNT'] =  ? 0 : parseFloat($row['AMOUNT']);
										$subSum +=   floatval($row['AMOUNT']);

										if ($key === ($datalenght-1)) {
										    // echo out the date head  payment-subsum-template
										    echo "<tr>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td class='text-right'  style='color:blue;'>"."Sub Total"."</td>";
										    	// echo "<td class='text-right'>". $row['SUBSUM']."</td>";
										    	echo "<td class='text-right' style='color:blue;'>". $subSum."</td>";
										    echo "</tr>";
										   

										    $subSum = 0;
									}
								}
								else if($etype === 'jv'){
									if (($what === 'date') && ($prevDate !== $row['DATE'])) {

									    if ($key !== 0) {
									        //  id jv-subsum-template
									        echo "<tr>";
									        	echo "<td>". "" . "</td>";
									        	echo "<td>". "" . "</td>";
									        	echo "<td>". "" . "</td>";
									        	echo "<td>". "" . "</td>";
									        	echo "<td class='text-right' style='color:blue;' >". "Sub" ."</td>";
									        	echo "<td class='text-right' style='color:blue;'>". $row['subCredit'] . "</td>";
									        	echo "<td class='text-right' style='color:blue;'>". $row['subDebit'] . "</td>";
									        echo "</tr>";
									        $subCredit = $subDebit = 0;

									    }

									    // jv-dhead-template
									    echo "<tr>"; 
									    	echo "<td>". "" . "</td>";
									    	echo "<td style='color:red;'>". substr($row['DATE'],0,10) . "</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" ."</td>";
									    echo "</tr>";

									    $prevDate = $row['DATE'];
									}
									else if (($what === 'invoice') && ($prevInvoice !== $row['VRNOA'])) {

									    if ($key !== 0) {
									    	//  Same as Above jv-subsum-template
										      echo "<tr>";
										      	echo "<td>". "" . "</td>";
										      	echo "<td>". "" . "</td>";
										      	echo "<td>". "" . "</td>";
										      	echo "<td>". "" . "</td>";
										      	echo "<td class='text-right' style='color:blue;' >". "Sub" ."</td>";
										      	echo "<td class='text-right' style='color:blue;'>". $row['subCredit'] . "</td>";
										      	echo "<td class='text-right' style='color:blue;'>". $row['subDebit'] . "</td>";
										      echo "</tr>";
										      $subCredit = $subDebit = 0;

									    }

									    

									    //  jv-ihead-template
									    echo "<tr>";  
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". $row['VRNOA']."</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" ."</td>";
									    	echo "<td>". "" . "</td>";
									    echo "</tr>";

									    $prevInvoice = $row['VRNOA'];
									}
									else if (($what === 'party') && ($prevParty !== $row['PARTY'])) {

									    if ($key !== 0) {
									    
								           	//  Same as Above jv-subsum-template
								       	      echo "<tr>";
								       	      	echo "<td>". "" . "</td>";
								       	      	echo "<td>". "" . "</td>";
								       	      	echo "<td>". "" . "</td>";
								       	      	echo "<td>". "" . "</td>";
								       	      	echo "<td class='text-right' style='color:blue;' >". "Sub" ."</td>";
								       	      	echo "<td class='text-right' style='color:blue;'>". $row['subCredit'] . "</td>";
								       	      	echo "<td class='text-right' style='color:blue;'>". $row['subDebit'] . "</td>";
								       	      echo "</tr>";
								       	      $subCredit = $subDebit = 0;
									    }

									    echo "<tr>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". "" . "</td>";
									    	echo "<td>". $row['PARTY']."</td>";
									    	echo "<td>". "" ."</td>";
									    	echo "<td>". "" . "</td>";
									    echo "</tr>";
									    $prevParty = $row['PARTY'];
									}
										//  jv-row-template
										echo "<tr>";
											echo "<td>". $Serial++."</td>";
											echo "<td>". $row['DATE']."</td>";
											echo "<td>". $row['VRNOA']."</td>";
											echo "<td>". $row['PARTY']."</td>";
											echo "<td>". $row['REMARKS']."</td>";
											echo "<td class='text-right' >". $row['CREDIT']."</td>";
											echo "<td class='text-right' >". $row['DEBIT']."</td>";
										echo "</tr>";
									


										// $subSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);
										// $subSum +=  (floatval($row['AMOUNT'] =  ? 0 : parseFloat($row['AMOUNT']);
										$subCredit += floatval($row['CREDIT']);
										$netCredit += floatval($row['CREDIT']);

										$subDebit += floatval($row['DEBIT']);
										$netDebit += floatval($row['DEBIT']);

										if ($key === ($datalenght-1)) {
										    // jv-subsum-template
										    echo "<tr>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td class='text-right' style='color:blue;'>"."Sub "."</td>";
										    	// echo "<td class='text-right'>". $row['SUBSUM']."</td>";
										    	echo "<td class='text-right' style='color:blue;'>". $subCredit."</td>";
										    	echo "<td class='text-right' style='color:blue;'>". $subDebit."</td>";
										    echo "</tr>";
										   

										    $subCredit = $subDebit = 0;
									}
								}
									else if($etype === 'expense'){
										if (($what === 'date') && ($prevDate !== $row['DATE'])) {

										    if ($key !== 0) {
										        //  id payment-subsum-template
										        echo "<tr>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td>". "" . "</td>";
										        	echo "<td class='text-right' style='color:blue;' >". "Sub Total" ."</td>";
										        	echo "<td class='text-right' style='color:blue;'>". $row['subSum'] . "</td>";
										        echo "</tr>";
										        $subSum  = 0;

										    }

										    // payment-dhead-template
										    echo "<tr>"; 
										    	echo "<td>". "" . "</td>";
										    	echo "<td style='color:red;'>". substr($row['DATE'],0,10) . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" ."</td>";
										    echo "</tr>";

										    $prevDate = $row['DATE'];
										}
										else if (($what === 'invoice') && ($prevInvoice !== $row['VRNOA'])) {

										    if ($key !== 0) {
										    	//  payment-subsum-template
											      echo "<tr>";
											      	echo "<td>". "" . "</td>";
											      	echo "<td>". "" . "</td>";
											      	echo "<td>". "" . "</td>";
											      	echo "<td>". "" . "</td>";
											      	echo "<td class='text-right' style='color:blue;' >". "Sub Total" ."</td>";
											      	echo "<td class='text-right' style='color:blue;'>". $row['subSum'] . "</td>";
											      echo "</tr>";
											      $subSum = 0;

										    }

										    

										    //  payment-ihead-template
										    echo "<tr>";  
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". $row['VRNOA']."</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" . "</td>";
										    echo "</tr>";

										    $prevInvoice = $row['VRNOA'];
										}
										else if (($what === 'party') && ($prevParty !== $row['PARTY'])) {

										    if ($key !== 0) {
										    
									           	//  payment-subsum-template
									       	      echo "<tr>";
									       	      	echo "<td>". "" . "</td>";
									       	      	echo "<td>". "" . "</td>";
									       	      	echo "<td>". "" . "</td>";
									       	      	echo "<td>". "" . "</td>";
									       	      	echo "<td class='text-right' style='color:blue;' >". "Sub Total" ."</td>";
									       	      	echo "<td class='text-right' style='color:blue;'>". $row['subSum'] . "</td>";
									       	      echo "</tr>";
									       	      $subSum  = 0;
										    }

										    echo "<tr>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". "" . "</td>";
										    	echo "<td>". $row['PARTY']."</td>";
										    	echo "<td>". "" ."</td>";
										    	echo "<td>". "" . "</td>";
										    echo "</tr>";
										    $prevParty = $row['PARTY'];
										}
											//  jv-row-template
											echo "<tr>";
												echo "<td>". $Serial++."</td>";
												echo "<td>". $row['DATE']."</td>";
												echo "<td>". $row['VRNOA']."</td>";
												echo "<td>". $row['PARTY']."</td>";
												echo "<td>". $row['REMARKS']."</td>";
												echo "<td class='text-right'>". $row['AMOUNT']."</td>";
											echo "</tr>";
										


											// $subSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);
											// $subSum +=  (floatval($row['AMOUNT'] =  ? 0 : parseFloat($row['AMOUNT']);
											$subSum += floatval($row['AMOUNT']);
											$netSum += floatval($row['AMOUNT']);

											

											if ($key === ($datalenght-1)) {
											    // payment-subsum-template
											    echo "<tr>";
											    	echo "<td>". "" ."</td>";
											    	echo "<td>". "" ."</td>";
											    	echo "<td>". "" ."</td>";
											    	echo "<td>". "" ."</td>";
											    	echo "<td class='text-right'  style='color:blue;'>"."Sub Total "."</td>";
											    	// echo "<td class='text-right'>". $row['SUBSUM']."</td>";
											    	echo "<td class='text-right' style='color:blue;'>". $subSum."</td>";
											    echo "</tr>";
											   

											    // $subCredit = $subDebit = 0;
										}
									}
									else if($etype === 'daybook'){
											if (($what === 'date') && ($prevDate !== $row['DATE'])) {

											    if ($key !== 0) {
											        //  id payment-subsum-template
											        echo "<tr>";
											        	echo "<td>". "" . "</td>";
											        	echo "<td>". "" . "</td>";
											        	echo "<td class='dont-show'>". "" . "</td>";
											        	echo "<td>". "" . "</td>";
											        	echo "<td>". "" . "</td>";
											        	echo "<td class='text-right' style='color:blue;' >". "Sub Total" ."</td>";
											        	echo "<td class='text-right' style='color:blue;'>". $subCredit . "</td>";
											        	echo "<td class='text-right' style='color:blue;'>". $subDebit . "</td>";
											        	// echo "<td>". $row['subCredit'] . "</td>";
											        	// echo "<td>". $row['subDebit'] . "</td>";
											        echo "</tr>";
											        $subCredit  = 0;$subDebit=0;

											    }

											    // payment-dhead-template
											    echo "<tr>"; 
											    	echo "<td>". "" . "</td>";
											    	echo "<td style='color:red;'>". substr($row['DATE'],0,10) . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td class='dont-show'>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" ."</td>";
											    echo "</tr>";

											    $prevDate = $row['DATE'];
											}
											// Start from here
											else if (($what === 'invoice') && ($prevInvoice !== $row['VRNOA'])) {

											    if ($key !== 0) {
											    	//  payment-subsum-template
												      echo "<tr>";
												      	echo "<td>". "" . "</td>";
												      	echo "<td>". "" . "</td>";
												    	echo "<td class='dont-show'>". "" . "</td>";
												      	echo "<td>". "" . "</td>";
												      	echo "<td>". "" . "</td>";
												      	echo "<td class='text-right' style='color:blue;' >". "Sub " ."</td>";
												      	echo "<td class='text-right' style='color:blue;'>". $row['subCredit'] . "</td>";
												      	echo "<td class='text-right' style='color:blue;'>". $row['subDebit'] . "</td>";
												      echo "</tr>";
												      $subCredit  = 0;$subDebit=0;


											    }

											    

											    //  payment-ihead-template
											    echo "<tr>";  
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". $row['VRNOA']."</td>";
											    	echo "<td class='dont-show'>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" ."</td>";
											    	echo "<td>". "" . "</td>";
											    echo "</tr>";

											    $prevInvoice = $row['VRNOA'];
											}
											else if (($what === 'party') && ($prevParty !== $row['PARTY'])) {

											    if ($key !== 0) {
											    
										           	//  payment-subsum-template
										       	      echo "<tr>";
										       	      	echo "<td>". "" . "</td>";
										       	      	echo "<td>". "" . "</td>";
										       	      	echo "<td class='dont-show'>". "" . "</td>";
										       	      	echo "<td>". "" . "</td>";
										       	      	echo "<td>". "" . "</td>";
										       	      	echo "<td class='text-right' style='color:blue;' >". "Sub " ."</td>";
										       	      	echo "<td class='text-right' style='color:blue;'>". $row['subCredit'] . "</td>";
										       	      	echo "<td class='text-right' style='color:blue;'>". $row['subDebit'] . "</td>";
										       	      echo "</tr>";
												      $subCredit  = 0;$subDebit=0;

											    }

											    echo "<tr>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
									       	      	echo "<td class='dont-show'>". "" . "</td>";
											    	echo "<td>". $row['PARTY']."</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" ."</td>";
											    	echo "<td>". "" . "</td>";
											    	echo "<td>". "" . "</td>";
											    echo "</tr>";
											    $prevParty = $row['PARTY'];
											}
												//  jv-row-template
												echo "<tr>";
													echo "<td>". $Serial++."</td>";
													echo "<td>". $row['DATE']."</td>";
													echo "<td>". $row['VRNOA']."</td>";
													echo "<td>". $row['PARTY']."</td>";
													echo "<td>". $row['REMARKS']."</td>";
													echo "<td class='dont-show'>". $row['ETYPE']."</td>";
													echo "<td class='text-right'>". $row['DEBIT']."</td>";
													echo "<td class='text-right'>". $row['CREDIT']."</td>";
												echo "</tr>";
											


												// $subSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);
												// $subSum +=  (floatval($row['AMOUNT'] =  ? 0 : parseFloat($row['AMOUNT']);
												$netDebit += floatval($row['DEBIT']);
												$subDebit += floatval($row['DEBIT']);

												$netCredit += floatval($row['CREDIT']);
												$subCredit += floatval($row['CREDIT']);

												

												if ($key === ($datalenght-1)) {
												    // payment-subsum-template
												    echo "<tr>";
												    	echo "<td>". "" ."</td>";
												    	echo "<td>". "" ."</td>";
												    	echo "<td class='dont-show'>". "" ."</td>";
												    	echo "<td>". "" ."</td>";
												    	echo "<td>". "" ."</td>";
												    	echo "<td style='color:blue;'>"."Sub "."</td>";
												    	echo "<td class='text-right' style='color:blue;'>". $subDebit."</td>";
												    	echo "<td class='text-right' style='color:blue;'>". $subCredit."</td>";
												    echo "</tr>";
												   
												    $subCredit = 0;
												    $subDebit = 0;

												    // $subCredit = $subDebit = 0;
											}
									
									
									
									}
									else if($etype === 'payable' || $etype === 'receiveable'){
										
										if (($etype === 'payable') && ($row['BALANCE'] > 0)) return true;
										else if (($etype === 'receiveable') && ($row['BALANCE'] < 0)) return true;

                       					 	$netSum += floatval($row['BALANCE']);
										

										       echo "<tr>";
										       	echo "<td>". $Serial++."</td>";
										       	echo "<td>". $row['PARTY']."</td>";
										       	echo "<td>". $row['ADDRESS']."</td>";
										       	echo "<td>". $row['EMAIL']."</td>";
										       	echo "<td>". $row['MOBILE']."</td>";
										       	echo "<td>". $row['PHONE_OFF']."</td>";
										       	echo "<td class='text-right'>". $row['BALANCE']."</td>";
										       echo "</tr>";


									
										}

							// }
								?>
							

								

								<?php endforeach ?>
								<?php 
								if ($etype === 'cpv' || $etype === 'crv') {
									echo "<tr>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td class='text-right' style='color:blue;'  >"."Net Total"."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netSum."</td>";
										// echo "<td>". $row['NETSUM']."</td>";
									echo "</tr>";
								}
								if ($etype === 'jv') {
									// jv-netsum-template
									echo "<tr>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td class='text-right' style='color:blue;'>"."Net Amount"."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netCredit."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netDebit."</td>";
										// echo "<td>". $row['NETSUM']."</td>";
									echo "</tr>";
								}
								if ($etype === 'expense') {
									// jv-netsum-template
									echo "<tr>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td class='text-right' style='color:blue;'>"."Net Total"."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netSum."</td>";
										// echo "<td>". $row['NETSUM']."</td>";
									echo "</tr>";
								}
								if ($etype === 'daybook') {
									// jv-netsum-template
									echo "<tr>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td class='dont-show'>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td class='text-right' style='color:blue;'>"."Net Amount"."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netDebit."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netCredit."</td>";
										// echo "<td>". $row['NETSUM']."</td>";
									echo "</tr>";
								}
								if ($etype === 'payable' || $etype === 'receiveable') {
									// jv-netsum-template
									echo "<tr>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td>". "" ."</td>";
										echo "<td class='text-right' style='color:blue;'>". "Net"."</td>";
										echo "<td class='text-right' style='color:blue;'>". $netSum."</td>";
										// echo "<td>". $row['NETSUM']."</td>";
									echo "</tr>";
								}
								?>
								
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
			
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>	