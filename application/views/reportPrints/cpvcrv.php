<!DOCTYPE html>
<html>
<head>
	<title>Account Report</title>

	<link rel="stylesheet" href="../../../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">

	<style>

		* { margin: 0; padding: 0; font-family: tahoma; }
		body { font-size:14px; margin-top: 0px !important;
			margin-bottom: 0px !important;
			margin-left: -20px !important;
			margin-right: 0px !important; }

			table { width: 100%; border: none !important; border-collapse:collapse; border-collapse: collapse; }
			th { border: 1px solid black; padding: 5px; }
			td { /*text-align: center;*/ vertical-align: center; /*padding: 5px 10px;*/ border-left: none !important;}
			@media print {
				.noprint, .noprint * { display: none; }
			}
			.centered { margin: auto; }
			@page{margin-top: 5mm; margin-left: 2mm;margin-right: 2mm;margin-bottom: 2mm; size !important:  auto !important;  }

			.rcpt-header { margin: auto; display: block; }
			td:first-child { text-align: left; }

			.subsum_tr td, .netsum_tr td { border-top:1px solid black !important; border-bottom:1px solid black; }
			.level2head,.level1head,.level3head {border-top: 1px solid black;}
			.finalsum,.hightlight_tr td {border-top: 1px solid black; border-left:0 !important; border-right: 0 !important; border-bottom: 1px solid black; background: rgb(226, 226, 226); color: black;font-weight: bold !important; }
			.finalsum td {border-top: 1px solid black; border-left:0 !important; border-right: 0 !important; border-bottom: 1px solid black; background: rgb(250, 250, 250); color: black; }
			.field {font-weight: bold; display: inline-block; width: 80px; } 
			.voucher-table thead th {background: #ccc; padding:3px; text-align: center; font-size: 14px;} 
			tfoot {border-top: 1px solid black; } 
			.bold-td { font-weight: bold; border-bottom: 1px solid black;}
			.nettotal { font-weight: bold; font-size: 14px; border-top: 1px solid black; }
			.invoice-type { border-bottom: 1px solid black; }
			.relative { position: relative; }
			.signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			.signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			.inv-leftblock { width: 280px; }
			.text-left { text-align: left !important; }
			.text-right { text-align: right !important; }
			td {font-size: 14px; font-family: tahoma; line-height: 14px; padding: 4px;  text-transform: uppercase;border-top: 0.5px solid black !important;} 
			.rcpt-header { width: 450px; margin: auto; display: block; }
			.inwords, .remBalInWords { text-transform: uppercase; }
			.barcode { margin: auto; }
			h3.invoice-type {font-size: 20px; width: 209px; line-height: 24px;}
			.extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; } 
			.nettotal { color: red; }
			.remainingBalance { font-weight: bold; color: blue;}
			.centered { margin: auto; }
			p { position: relative; }
			.fieldvalue.cust-name {position: absolute; width: 497px; } 
			.shadowhead { border-bottom: 0px solid black; padding-bottom: 5px; } 
			.AccName { border-bottom: 0px solid black; padding-bottom: 5px; font-size: 16px; } 

			.txtbold { font-weight: bolder; } 
			@media print {
				a[href]:after {
					content: none !important;
				}
			}
	</style>
</head>
<body>
	<script id="tblrow-template" type="text/x-handlebars-template">
	  <tr>
	     <td>{{SERIAL}}</td>
	     <td>{{DATE}}</td>
	     <td>{{VRNOA}}</td>
	     <td>{{PARTY}}</td>
	     <td>{{CREDIT}}</td>
	     <td>{{DEBIT}}</td>
	  </tr>
	</script>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12 centered">
				
				<div class="row-fluid">
					<div class="span12 centered">
						
						<div class="row-fluid">
							<div class="span12">
								<table class="voucher-table">
									<thead>
										<col style="width:10px;">
										<col style="width:30px;">
										<col style="width:60px;">
										<col>
										<col>
										<col style="width:100px;">
										<tr>
											<th colspan="6">
												<h3 class="text-center shadowhead">[Cash Report]</h3>
												<p class="text-center"><span class="from"><strong>From:-</strong><span class="fromDate">[0000/00/00]</span></span> To <span class="to"><strong>To:-</strong><span class="toDate">[0000/00/00]</span></span></p>
											</th>
										</tr>
										<tr>
											<th style="width:10px;">#</th>
											<th style="width:30px;">Date</th>
											<th style="width:60px;">Vr #</th>
											<th>Party</th>
											<th>Remarks</th>
											<th style="width:100px;">Amount</th>
										</tr>
									</thead>
									<tbody id="htmlRows">
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<br>
				<!-- <p><strong>Note:</strong>  Here please find our acount statement and check it, if any discrepancy please let it be known within a week. Otherwise it would be assumed that our statement is correct. Thanks!</p> -->
				<br>
				<div class="row-fluid">
					<div class="span12">
						<table class="signature-fields">
							<thead>
								<tr>
									<th style="border-top: 1px solid black; border-left: 1px solid white; border-right: 1px solid white; border-bottom: 1px solid white;">Prepared By</th>
									<th style="border:1px solid white;"></th>
									<th style="border-top: 1px solid black; border-left: 1px solid white; border-right: 1px solid white; border-bottom: 1px solid white;">Received By</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
	<script src="../../../assets/js/handlebars.js"></script>

	<script type="text/javascript">
	function getMonthName (monthNum) {
				switch(monthNum) {
					case 1 :
						return 'Jan';
					break;
					case 2 :
						return 'Feb';
					break;
					case 3 :
						return 'Mar';
					break;
					case 4 :
						return 'Apr';
					break;
					case 5 :
						return 'May';
					break;
					case 6 :
						return 'Jun';
					break;
					case 7 :
						return 'Jul';
					break;
					case 8 :
						return 'Aug';
					break;
					case 9 :
						return 'Sep';
					break;
					case 10 :
						return 'Oct';
					break;
					case 11 :
						return 'Nov';
					break;
					case 12 :
						return 'Dec';
					break;
				}
			}
		$(function(){
			
			var opener = window.opener;
			
			var fromDate = opener.$('#from').val();
			var toDate = opener.$('#to').val();			
			var etype = opener.$('input[name=etype]:checked').val().toUpperCase();

			var netBal = opener.$('.netamt_td').text().trim();

			var parentRows = opener.$('#CPVRows tr');

			var rowsHtml = '';

			var netBalance = 0;
			var separator = ( fromDate.indexOf('/') === -1 ) ? '-' : '/';

			dateParts = fromDate.split(separator);
			var fromDate = dateParts[2] + '-' + getMonthName(parseInt(dateParts[1], 10)) + '-' + dateParts[0];

			separator = ( toDate.indexOf('/') === -1 ) ? '-' : '/';

			dateParts = toDate.split(separator);
			var toDate = dateParts[2] + '-' + getMonthName(parseInt(dateParts[1], 10)) + '-' + dateParts[0];
			var parentCopy = opener.$('#CPVRows tr').clone();

			parentCopy.find('.printRemove').remove();

			/*$(parentRows).each(function( index, elem ){

				var obj = {};

				obj.SERIAL = $(elem).find('.tblSerial').text().trim();
				obj.PARTY = $(elem).find('.tblParty').text().trim();
				obj.BALANCE = $(elem).find('.tblBalance').text().trim();

				netBalance += parseFloat(obj.BALANCE);

				var handler = $('#tblrow-template').html();
				var template = Handlebars.compile(handler);
				var html = template(obj);

				rowsHtml += html;
			});	*/

			var head = '';
			if (etype.toLowerCase() === 'cpv') {
				head = 'Cash Payment Report';
			} else if ( etype.toLowerCase() === 'crv' ) {
				head = 'Cash Receipt Report';
			} else {
				head = 'Expense Report';
			}

			$('#htmlRows').append(parentCopy);

			$('.fromDate').html(fromDate);
			$('.toDate').html(toDate);
			$('.shadowhead').html( head );
			$('.netBalance').html(netBal);

		});
	</script>
</body>
</html>