<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
		* { margin: 0; padding: 2; font-family: tahoma !important; }
		body { font-size: 10px !important; }
	p { margin: 0 !important; /* line-height: 17px !important; */ }
	.field { font-size: 10px !important; font-weight: bold !important; display: inline-block !important; width: 10px !important; } 
	.field1 { font-size: 10px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
	.voucher-table{ border-collapse: none !important; }
	table { width: 100% !important; border-bottom: 0.5px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:0px}
	th {  padding: 2px !important; }
	td { /*text-align: center !important;*/ vertical-align: top !important;  }
	td:first-child { text-align: left !important;  }
	.voucher-table thead th {background: #ccc !important; } 
	tfoot {border-top: 0.5px solid black !important; } 
	.bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
	.nettotal { font-weight: bold !important; font-size: 10px !important; border-top: 0.5px solid black !important; }
	.invoice-type { border-bottom: 0.5px solid black !important; }
	.relative { position: relative !important; }
	.signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
	.signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
	.inv-leftblock { width: 280px !important; }
	.text-left { text-align: left !important; }
	.text-right { text-align: right !important; }
	td {font-size: 10px !important; font-family: tahoma !important; line-height: 14px !important; padding: 2px !important;border-bottom: 0.5px !important; } 
	.rcpt-header { width: 450px !important;  margin: 0px; display: inline;  top: 0px; right: 0px; }
	.inwords, .remBalInWords { text-transform: uppercase !important; }
	.barcode { margin: auto !important; }
	h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
	.extra-detail span { background: #7F83E9 !important; color: white !important; padding: 2px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 10px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
	.nettotal { color: red !important; font-size: 10px !important;}
	.remainingBalance { font-weight: bold !important; color: blue !important;}
	.centered { margin: auto !important; }
	p { position: relative !important; font-size: 10px !important; }
	thead th { font-size: 10px !important; font-weight: bold !important; padding: 2px !important; }
	.fieldvalue.cust-name {position: absolute; width: 497px; margin-right:130px !important;  }

	@media print {
		.noprint, .noprint * { display: none !important; }
	}
	.pl20 { padding-left: 20px !important;}
	.pl40 { padding-left: 40px !important;}

	.barcode { float: right !important; }
	.item-row td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
	.foot-comments td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important; font-weight: bold !important;}


	.footer_company td { font-size: 8px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
	@page{margin-top: 5mm; margin-left: 2mm;margin-right: 2mm;margin-bottom: 2mm; size !important:  auto !important;  }


	h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
	tfoot tr td { font-size: 10px !important; padding: 2px !important;  }
	.nettotal, .subtotal, .vrqty,.vrweight { font-size: 10px !important; font-weight: bold !important;}
	.footer {
		position: absolute;
		color: red;
		bottom: 0;
	}
	tr { page-break-inside: avoid !important; }
	tr td ul li { page-break-inside: avoid !important; }

	/*.{padding-top: 6%;}*/
</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
				<div class="row-fluid">
						<div class="span12"><img class="rcpt-header" src="<?php echo $header_img;?>" alt=""></div>
					</div>
					
					<div class="row-fluid">
						<table class="voucher-table" style="border:none !important;">
							<tbody>
								
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">To :</td>
									<td style=" width: 50px; "> <?php echo $to;?> </td>
								
									
								</tr>
								
							</tbody>
						</table>
					</div>

					<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
						<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 12px !important; font-size:16px !important; "><?php echo $title;?></h3>
					</div>
			
					
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
								<th class="text-left" width="40px;" style=" padding: 2px; ">Sr#</th>
									<th class="text-left" width="20px" style="  ">Dcno</th>
									<th class="text-left" width="30px" style=" ">Name</th>
									<th class="text-left" width="50px" style="  ">Father Name</th>
									<th class="text-left" width="150px" style="  ">Mobile#</th>
									<th class="text-left" width="40px" style="  ">Balance</th>
									<!-- <th style=" padding: 0; ">Sr#</th>
									<th style="  ">Std id</th>
									<th style=" ">Name</th>
									<th style="  ">Father Name</th>
									<th style="  ">Mobile No</th>
									<th style="  ">Fee Balance</th> -->
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$branch_name = "";
									$class_name = "";
									$section_name = "";
									$counter = 1;
									
									function showClassSum($sumClass,$class_name){
										echo 	"<tr>";
										echo "<td colspan='2' style='text-align: right;'>" . $class_name . "</td>";
										echo "<td colspan='3' style='text-align: right;'>" ."Total:". "</td>";
										echo "<td style=''> " . $sumClass. "</td>";
										echo "</tr>";
									}
									function showSectionSum($section_total,$section_name) {
										echo  "<tr>";
										echo  "<td colspan='2' style=';text-align: right;'>". $section_name . "</td>";
										echo "<td colspan='3' style=';text-align: right;'>" ."Total:". "</td>";
										echo "<td style=';'> ". $section_total ."</td>";
										echo "</tr>";
									}
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									foreach ($vrdetail as $row):
									

								?>
								<?php if ($row['branch_name'] !== $branch_name): ?>
								 	 <tr>
								 	 <td colspan='6' style=''>				
								 	 <?php echo $row['branch_name']; ?></td>
								 	 </tr>
								 	<?php  $branch_name = $row['branch_name'];?>
																
								<?php endif; ?>

								<?php if ($row['class_name'] !== $class_name): ?>
								 	 <tr>
								 	 <td colspan='6' style=''>				
								 	 <?php echo $row['class_name']; ?></td>
								 	 </tr>
								 	<?php  $class_name = $row['class_name'];?>
																
								<?php endif; ?>

								<?php if ($row['section_name'] !== $section_name): ?>
								 	<tr>
	 	 								<td colspan='6' style=''> <?php echo  $row['section_name']; ?> </td>
	 	 							</tr>
								 	<?php  $section_name = $row['section_name'];?>
														
								<?php endif; ?>
									

								
								

							
									<tr style="border-bottom: 0.5px solid !important;">

										<td> <?php echo $counter++ ?> </td>
										<td> <?php echo $row['stdid']; ?> </td>
										<td> <?php echo $row['student_name']; ?> </td>
										<td> <?php echo $row['father_name']; ?> </td>
										<td> <?php echo $row['mobile']; ?> </td>
										<td> <?php echo round($row['balance'],2); ?> </td>
									</tr>
									<?php //$sumClass += floatval($row['amount']);
										 //$sumSection += floatval($row['amount']);	
										
		 							 ?>

								

								<?php endforeach ?>
								<?php //echo showSectionSum($sumSection,$section_name); 
								//echo showClassSum($sumClass,$class_name);?>
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
			
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				<!-- </div> -->
			</div>
		</div>
	</body>
	</html>	