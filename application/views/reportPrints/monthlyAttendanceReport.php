<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Voucher</title>
	<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
	

	<style type="text/css">
		* { margin: 0; padding: 0; font-family: tahoma; }
		 body { font-size:12px; width: 1300 !important; margin-left: auto; margin-right: auto; }
		 p { margin: 0; /* line-height: 17px; */ }
		.logo-img {
			padding-top: 0px;
			margin-top: 20px;
			float: right;
			/*margin-left: -9px;*/
		}
		.txt-right {
			text-align: right;
		}
		/*table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
		th { border: 1px solid black; padding: 5px; }
		td {  vertical-align: top;  border-left: 1px solid black;}
		td:first-child { text-align: left; }
		.voucher-table thead th {background: #ccc; }
		tfoot {border-top: 1px solid black; }
		td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; }
		h3.invoice-type {font-size: 20px; line-height: 24px;}		
		.tbb td {
			border: 1px solid #333;
			height: 12px;
			line-height: 0px;
			padding-top: 12px;
		}*/

		@page {
		 	 margin:20px auto 20px;
		     counter-increment: page;
		     counter-reset: page 1;
		     @top-right {
		         content: "Page " counter(page) " of " counter(pages);
		}
		.field {font-weight: bold; display: inline-block; width: 150px;margin-top: 15px; } 
		.invoice-type { border-bottom: 1px solid black; }
		h3.invoice-type {font-size: 20px; line-height: 24px;}
		h3.invoice-type { border: none !important; margin: 0px !important;}
		.inv-leftblock { width: 280px; }
		.tdstuName{width: 79px;}
		/*th.thstudName{width: 79px;}*/
/*th.txtcenter.level4row.thstudName{width: 79px;}*/

	</style>
	<!-- <script id="invoice-items-template" type="text/x-handlebars-template"> 
	  <tr class='tbb'>
	     <td class='text-left'>{{QTY}}</td>
	     <td class='text-left'>{{ITEM}}</td>
	     <td class='text-right'>{{RATE}}</td>
	     <td class="text-right">{{AMOUNT}}</td>
	  </tr>
	</script>-->
</head>
<body>
	<div class='container'>
		<div class='row'>
			<div style='display: inline-block; width: 100%'>
				<img class="logo-img" src="../../../assets/img/header_imgs.png" alt="">
			</div>
				<div class="row">
					<div class="col-lg-12">

						<div class="block pull-left inv-leftblock" style="width:100%; !important; display:inline-block !important;">
							<h3 class="invoice-type text-left" style="font-size: 22px; border:none !important; margin: 0px !important;text-align:right; ">Monthly Attendance Report</h3>

							<p><span class="field">Month-Year : </span><span class="fieldvalue month_year"></span></p>									
							<p><span class="field">Class :</span><span class="fieldvalue className"></span></p>
							<p><span class="field">Section :</span><span class="fieldvalue sectionName"></span></p>
							
							<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
						</div> --> 
						
					</div>
				</div>
			</div>
		<!-- 	<div  style='display: inline-block; width: 40%'>
				<div class="row">
					<div class="col-lg-12"><strong  class='pull-right'>Invoice</strong></div>
				</div>
				<div class='row row txt-right'>
						<strong style='width: 86px;display: inline-block;text-align: left;'>Date:</strong>
						<span class='' style='border-bottom:1px solid;'>2014-01-20</span>
				</div>
				<div class='row txt-right'>
					<strong style='width: 133px;display: inline-block;text-align: left;'>Invoice No:</strong>
					<span class='' style='border-bottom:1px solid;'>13</span>
				</div>
			</div> -->
		</div>


		<div class='row'>
			<div class='col-lg-12'>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<table class="table table-striped table-hover center" id="atnd-table">
										<thead class="table_attanHead">
										</thead>
										<tbody class="table_attanBody">
										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
			<!-- 	<div class=''>
					<table class='table' style=''>
						<thead class="table_head">
							<tr>
								<th style='width:25px;'>Qty</th>
								<th style='width:260px;'>Description</th>
								<th style='width:100px;'>Unit Price</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody class="table_body">
							
						</tbody>
						<tfoot style=''>
					
						</tfoot>
					</table>
				</div> -->
			</div>
		</div>
	
	</div>

	<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
    	<script type="text/javascript" src="../../../assets/js/handlebars.js"></script>
        	<script type="text/javascript" src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
        	<script type="text/javascript" src="../../../assets/js/app_modules/general.js"></script>


	<!-- <script src="../../assets/js/handlebars.js"></script> -->
	<script>

		function getMonthName (monthNum) {
			switch(monthNum) {
				case 1 :
					return 'Jan';
				break;
				case 2 :
					return 'Feb';
				break;
				case 3 :
					return 'Mar';
				break;
				case 4 :
					return 'Apr';
				break;
				case 5 :
					return 'May';
				break;
				case 6 :
					return 'Jun';
				break;
				case 7 :
					return 'Jul';
				break;
				case 8 :
					return 'Aug';
				break;
				case 9 :
					return 'Sep';
				break;
				case 10 :
					return 'Oct';
				break;
				case 11 :
					return 'Nov';
				break;
				case 12 :
					return 'Dec';
				break;

			}
		}
		 function search(month, year, brid, claid, secid) {

			$.ajax({
				url : base_url + 'index.php/attendance/monthlyAttendanceReport',
				type : 'POST',
				data : { 'brid' : brid, 'claid' : claid, 'secid' : secid, 'month' : month, 'year' : year },
				dataType : 'JSON',
				success : function(data) {

					$('#atnd-table').find('tbody tr').remove();
					if (data === 'false') {
						alert('No record found.');
					} else {
						populateData(data);
					}
				}, error : function(xhr, status, error) {
					console.log(xhr.responseText);
				}

			});
		}

		function populateData(data) {

			var month_year =  $.trim($('.month_year_picker').val());
			var month = parseInt(month_year.substring(0, 2));
			var year = parseInt(month_year.substring(3));
			var days = general.getDaysInMonth(month, year);
			var stdid = "";
			var counter = 1;
			// alert(days);

			$.each(data, function(index, elem) {

				if (elem.stdid != stdid) {
					
					var cols = new Array(days);
					console.log("Array is" +cols);
					$.each(data, function(ind, el) {

						if (el.stdid == elem.stdid) {
							cols[el.day] = el.status.substring(0, 1);
							// console.log(cols[el.day]);
							// alert(cols[el.day]);
						}
					});


					var row = "<tr><td class='txtcenter level4row'>"+ (counter++) +"</td><td class='txtcenter level4row tdstuName'>"+ elem.student_name +"</td>";
					for ( i = 1; i<=cols.length; i++) {
						console.log(typeof(cols[i]));

						if (i%2 == 0) {
							row += "<td class='txtcenter' style='background: #e6f3fd;'>"+ ((typeof(cols[i]) == "undefined") ? '-' : cols[i]) +"</td>";
							// alert(row);
						} else {
							row += "<td class='txtcenter' style='background: #fff;'>"+ ((typeof(cols[i]) == "undefined") ? '-' : cols[i]) +"</td>";
						}
					}
					row += "</tr>";
					$(row).appendTo('#atnd-table tbody');

					stdid = elem.stdid;
				}
			});
		}
		 function populateTableHeader(month, year) {
			$('#atnd-table thead').find('tr').remove();
			$('#atnd-table').find('tbody tr').remove();
			var days = general.getDaysInMonth(month, year);

			var cols = "<tr><th class='txtcenter level4row'>Sr#</th><th class='txtcenter level4row thstudName'>Student Name</th>";
			for ( i = 1; i<=days; i++) {

				if (i%2 == 0) {
					cols += "<th class='txtcenter' style='background: #e6f3fd;'>"+ i +"</th>";
				} else {
					cols += "<th class='txtcenter' style='background: #fff;'>"+ i +"</th>";
				}
			}
			cols += "</tr>";

			$(cols).appendTo('#atnd-table thead');
		}

		var month_year =  $.trim($('.month_year_picker').val());
		var month = parseInt(month_year.substring(0, 2));
		var year = parseInt(month_year.substring(3));

		// populateTableHeader(month,year);
		
		/**
		 * shows the remaining balance on the voucher
		 * @param  {string} endDate  date till which the balance is required
		 * @param  {int} party_id id of the party whose balance is required
		 * @param  {int} dcno    Voucher number that is not to be included in the balance
		 */
		// function showRemainingBalance ( endDate, party_id, dcno ) {
		// 	$.ajax({
		// 		url: window.opener.base_url + 'index.php/account_ledger/fetchPreviousBalance',
		// 		type: 'POST',
		// 		dataType : 'JSON',
		// 		data: { endDate : endDate, p_id : party_id, dcno : dcno },
		// 		success : function (data) {

		//             $('.balancedue').html(data[0].RTotal);
		//             // $('.thistotal').html( $('.nettotal').html() );
		//             // $('.rembal').html( parseFloat(data[0].RTotal) + parseFloat( $('.nettotal').html() )  )
		//             // $('.remBalInWords').html(toWords(data[0].RTotal));
		// 		},
		// 		error : function (error){
		// 			alert("Error : " + error);
		// 		}
		// 	});
		// }

  //       var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
  //       var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
  //       var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

  //       // Convert numbers to words
  //       // copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
  //       // permission to use this Javascript on your web page is granted
  //       // provided that all of the code (including this copyright notice) is
  //       // used exactly as shown (you can change the numbering system if you wish)

  //       // American Numbering System
  //       var th = ['','thousand','million', 'billion','trillion'];
  //       // uncomment this line for English Number System
  //       // var th = ['','thousand','million', 'milliard','billion'];

  //       var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}

		$(function (){

			var opener = window.opener;
			// console.log( "shs"+opener.$('.table_attanHead').clone());
			// var tblhead = opener.$('.table_attanHead').clone();
			// var tblbody = opener.$('.table_attanHead').clone();
			// $('.table_head').append(tblhead);
			// $('.table_body').append(tblbody);
			var tbl = opener.$('#atnd-table').clone();
			$('table').append(tbl);

			var className = opener.$('#class_dropdown option:selected').text();
			var SectionName = opener.$('#section_dropdown option:selected').text();
			// var month_year = opener.$('.month_year_picker option:selected').text();
			// alert(month_year);

			var month_yearsss =  opener.$('.month_year_picker').val();
			alert(month_yearsss);
			// var month = opener.parseInt(month_year.substring(0, 2));
			// var year = opener.parseInt(month_year.substring(3));
			// var new_monthYear = month + '-' + year; 
			$('.month_year').text(month_yearsss);
			$('.className').text(className);
			$('.sectionName').text(SectionName);


			// var invNum = opener.$("#txtVrno").val();
			// $('.freight').text(opener.$('#txtServiceCharges').val());
			// var through = ( opener.$("#drpThrough option:selected").val() ) ? opener.$("#drpThrough option:selected").text() : '-- N/A --';
			// var invoiceType = opener.$(".inv-type").text();
			// var remarks = opener.$("#txtRemarks").val();
			// var discount = parseFloat(opener.$("#txtDiscount").val()) ? parseFloat(opener.$("#txtDiscount").val()) : opener.$("#txtDiscount").val();
			// var netAmount = parseFloat(opener.$("#txtNetAmount").val());
			// var custName = opener.$('#drpParty option:selected').text();
			// var address = opener.$('#drpParty option:selected').data('address');
			// var custMobile = ( opener.$('#custmobile').val() ) ? opener.$('#custmobile').val() : '-- N/A --';
			// var subTotal = 0;

			// netAmount = netAmount.toFixed(2);

			// var justName = opener.$('#drpParty option:selected').text();
			// var date = opener.$("#txtDate").val();
			// var partyId = opener.$('#drpParty option:selected').val();


			// var origRows = opener.$("#itemRows").find("tr");

			// $('.voucher-table tbody').html('');

			// var gstnet = 0;
			// $(origRows).each( function (index, elem) {

			// 	var obj = {};

			// 	obj.SR = index + 1;
			// 	obj.QTY = parseInt($(elem).find('.tblQty').text());
			// 	obj.ITEM = $(elem).find('.tblItemName').text();
			// 	obj.RATE = parseFloat($(elem).find('.tblRate').text());
			// 	obj.gstval = parseFloat($(elem).find('.gstval').text());
			// 	obj.AMOUNT = obj.QTY * obj.RATE;

			// 	subTotal += obj.AMOUNT;
			// 	gstnet += parseFloat(obj.gstval);

			// 	var source   = $("#invoice-items-template").html();
			// 	var template = Handlebars.compile(source);
			// 	var html = template(obj);

			// 	$('.voucher-table tbody').append(html);
			// });

			// $('.gstval').html(gstnet);

			// var dateParts = [];

			// var separator = ( date.indexOf('/') === -1 ) ? '-' : '/';

			// dateParts = date.split(separator);
			// var newDate = dateParts[2] + '-' + getMonthName(parseInt(dateParts[1], 10)) + '-' + dateParts[0];

			// $('.inv-vrnoa').html(invNum);
			// $('.inv-date').html(newDate);
			// // $('.inv-through').html(through);
			// $('.cust-name').html(custName);
			// $('.address').html(address);
			// $('.invoice-type').prepend(invoiceType + " ");
			// $('.remarks-td').html(remarks);
			// $('.subtotal').html(subTotal);
			// 			// My Codings
			// var hiddbal = parseInt(opener.$('#txthiddBal').val());
			// var mynetAmount = parseInt(opener.$("#txtNetAmount").val());
			// var preBalance 	= hiddbal - mynetAmount;
			// $('.PreBal').text(preBalance);
			// $('.ThisInv').text(mynetAmount);
			// // var currentBalance = preBalance - mynetAmount;
			// $('.CurBal').text(hiddbal);

			// $(".nettotal").html(netAmount);
			// $(".thistotal").html(netAmount);
			// $('.cust-mobile').html(custMobile);
			// $('.inwords').html(toWords(netAmount));

			// if (through !== '-- N/A --') {
			// 	$('.inv-leftblock').append('<p><span class="field">Through</span><span class="fieldvalue inv-through">' + through + '</span></p>');
			// };

			// if (invoiceType.toLowerCase() == 'sale') {
			// 	$('.showOnSale').show()
			// };

			// if ((invoiceType.toLowerCase() === 'sale') && ($.trim(opener.$('#txtWarranty').val()) !== '')) {
			// 	$('.add-on-detail').append('<p><strong>Warranty: </strong>' + (opener.$('#txtWarranty').val()) + '</p>');
			// };

			// // $('.discount').html(discount);

			// if (discount > 0) {
			// 	$('.foot-comments').after('<tr><td class="remarks-td" colspan="3"></td> <td class="bold-td">Discount</td> <td class="discount text-right">' + discount + '</td></tr>');
			// };



			// $('.barcode').barcode(invNum + "-" + invoiceType.toUpperCase(), "code93")

			// if( invoiceType.indexOf('Quotation') !== -1 ) {
			// 	$('.extra-detail').html('<span>Quotation valid only until the stock lasts.</span>');
			// }

			// $('.dblPrint').on('click', function (){
			// 	var btn = opener.$('#btnDblPrint').trigger('click');
			// });

			// $('.singlePrint').on('click', function (){
			// 	window.print();
			// });

			// if (justName.toLowerCase() !== 'cash') {
			// 	showRemainingBalance(date, partyId, invNum);
			// } else if (justName.toLowerCase() === 'cash') {
			// 	$('.hideOnCash').remove();
			// }

			// var username = opener.$('.loggedin_name').val();
			// var company_name = opener.$('.company_name').val();
			// var user_mobile = opener.$('.user_mobile').val();
			// var company_contact = opener.$('.company_contact').val();
			// var user_email = opener.$('.user_email').val();

			// $('.loggedin_name').html(username);
			// $('.company_name').html(company_name);
			// $('.user_mobile').html(user_mobile);
			// $('.user_email').html(user_email);
			// $('.company_contact').html(company_contact);

			// // Barcode settings
			// $('.barcode div').css({ 'height' : 20 });
			// $('.barcode').find('div').last().remove();

			// window.print();
		});
	</script>

</body>

</html>