<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma; }
			 body { font-size:12px; }
			 p { margin: 0; /* line-height: 17px; */ }
			 .field {font-weight: bold; display: inline-block; width: 150px;margin-top: 15px; } 
			 .voucher-table{ border-collapse: collapse;margin-top: -30px; }
			 table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
			 th { border: 1px solid black; padding: 5px; }
			 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
			 td:first-child { text-align: left; }
			 .voucher-table thead th {background: #ccc; } 
			 tfoot {border-top: 1px solid black; } 
			 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
			 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			 .invoice-type { border-bottom: 1px solid black; }
			 .relative { position: relative; }
			 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			 .inv-leftblock { width: 280px; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
			 .rcpt-header { width: 450px; margin: auto; display: block; }
			 .inwords, .remBalInWords { text-transform: uppercase; }
			 .barcode { margin: auto; }
			 h3.invoice-type {font-size: 20px; line-height: 24px;}
			 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			 .nettotal { color: red; font-size: 12px;}
			 .remainingBalance { font-weight: bold; color: blue;}
			 .centered { margin: auto; }
			 p { position: relative; font-size: 16px; }
			 thead th { font-size: 13px; font-weight: normal; }
			 .fieldvalue.cust-name {position: absolute; width: 497px; } 
			 @media print {
			 	.noprint, .noprint * { display: none; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 15px; padding: 10px;}

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important;}
			tfoot tr td { font-size: 13px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			.footer{clear: both;width: 100%;position: relative;top: 700px; display: inline-block;}
			.software{width: 45%;display: inline-block;text-align: left;}
			.Pages{width: 54%;display: inline-block;text-align: right;}
						table tbody td{border: none;}
			table tbody td{border-bottom: 1px solid black;}

			/*.{padding-top: 6%;}*/
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
			
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:550px !important; display:inline-block !important;">
									<h3 class="invoice-type text-left" style="font-size: 22px; border:none !important; margin: 0px !important; "><?php echo $title; ?></h3>
									
									<p><span class="field">Month Year :</span><span class="fieldvalue inv-date"><?php echo  $monthOne."-".$yearOne; ?></span></p>

									<p><span class="field">Class :</span><span class="fieldvalue inv-date"><?php echo  $className; ?></span></p>
									<p><span class="field">Section : </span><span class="fieldvalue inv-vrnoa"><?php echo $SectionName; ?></span></p>	
									
																	
								
									
									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
								</div> --> 
								<div class="block pull-right" style="width:900px !important; float: right; display:inline !important;">
									<div class="span12"><img style="float:right; width:300px !important;" class="rcpt-header logo-img" src="<?php echo $header_img; ?>" alt=""></div>
									
									
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
							<tr><th class=" level4row">Sr#</th><th class=" level4row">Student Name</th><th class="" >1</th><th class="" >2</th><th class="" >3</th><th class="" >4</th><th class="" >5</th><th class="" >6</th><th class="" >7</th><th class="" >8</th><th class="" >9</th><th class="" >10</th><th class="" >11</th><th class="" >12</th><th class="" >13</th><th class="" >14</th><th class="" >15</th><th class="" >16</th><th class="" >17</th><th class="" >18</th><th class="" >19</th><th class="" >20</th><th class="" >21</th><th class="" >22</th><th class="" >23</th><th class="" >24</th><th class="" >25</th><th class="" >26</th><th class="" >27</th><th class="" >28</th><th class="" >29</th><th class="" >30</th><th class="" >31</th></tr>
							</thead>

							<tbody>
								
								<?php 
								$month_year =  $month_yearOne;
								$month = $monthOne;
								$year = $yearOne;
								$days = $daysOne;
								$stdid = "";
								$counter = 1;
									// echo  "dayone" .$days;
									
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									foreach ($vrdetail as $row):
									

								?>
								<?php if ($row['stdid'] !== $stdid): ?>
								 				
								 	 <?php $cols =  array($days);  
								 	
 // echo "var dump days is". var_dump($days) . "<br>" . "<br>";
	// 							 	 echo "var dump cols is". var_dump($cols) . "<br>" . "<br>"; ?>
								 	 	
								 	 	<?php  foreach ($vrdetail as $rowTwo):?>
									 	 	<?php if ($rowTwo['stdid'] === $row['stdid']): ?>
								 				<?php  $cols[$rowTwo['day']] = substr($rowTwo['status'],0,1); //echo "cols daytwo is" .$cols[$rowTwo['day']];?>
											<?php endif; ?>
										<?php endforeach; ?>

								 	<?php $tr =  "<tr>".
									 	      "<td colspan='3' style=''>". ($counter++) ."</td><td>" . $row['student_name'] ."</td>"; ?>
									<?php $length = count($cols);
									// echo $length;
									 for ($i=1; $i <= $length; $i++) { 
											if ($i%2 === 0 ) {
													// echo "fff" .gettype($cols[$i]);
												 // $tr +=  "<td>".  ((gettype($cols[$i]) == 'NULL') ? '-' : $cols[$i]) ."</td>";
												// }
												$oldtr = "<td >".((gettype($cols[$i]) == 'NULL') ? '-' : $cols[$i]) ."</td>";
											 	 // $tr += "<td >".((gettype($cols[$i]) == 'NULL') ? '-' : $cols[$i]) ."</td>";
											 	 $tr += $oldtr;
											 	 // echo "old tr is" . $oldtr;
											 	 echo $tr;
											}else{
													// gettype($cols[$i]);

												// $tr += "<td >".
												//  ((gettype($cols[$i]) == 'NULL') ? '-' 
												// 	: $cols[$i]) ."</td>";
												// $tr += "<td >".((gettype($cols[$i]) == 'NULL') ? '-' : $cols[$i]) ."</td>";
												$oldtrtwo = "<td >".((gettype($cols[$i]) == 'NULL') ? '-' : $cols[$i]) ."</td>";
												// echo "old trtwo is" . $oldtrtwo;
												 $tr += $oldtrtwo;
													echo $tr;
											}
									}	
									$trend = "</tr>"; 
									// $tr +=	"</tr>";
									  $tr +=	$trend;
										echo $tr;									  
									$stdid = $row['stdid'];					 
									?>
							

							
								<?php endif; ?>
								

								<?php endforeach ?>
							
								
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
			
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>	