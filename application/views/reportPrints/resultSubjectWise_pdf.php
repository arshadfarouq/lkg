<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma; }
			 body { font-size:12px; }
			 p { margin: 0; /* line-height: 17px; */ }
			 .field {font-weight: bold; display: inline-block; width: 100px;margin-top: 15px; } 
			 .voucher-table{ border-collapse: collapse;margin-top: 5px; }
			 table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
			 th { border: 1px solid black; padding: 5px; }
			 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
			 td:first-child { text-align: left; }
			 .voucher-table thead th {background: #ccc; } 
			 tfoot {border-top: 1px solid black; } 
			 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
			 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			 .invoice-type { border-bottom: 1px solid black; }
			 .relative { position: relative; }
			 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			 .inv-leftblock { width: 280px; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
			 .rcpt-header { width: 450px; margin: auto; display: block; }
			 .inwords, .remBalInWords { text-transform: uppercase; }
			 .barcode { margin: auto; }
			 h3.invoice-type {font-size: 20px; line-height: 24px;}
			 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			 .nettotal { color: red; font-size: 12px;}
			 .remainingBalance { font-weight: bold; color: blue;}
			 .centered { margin: auto; }
			 p { position: relative; font-size: 16px; }
			 thead th { font-size: 13px; font-weight: normal; }
			 .fieldvalue.cust-name {position: absolute; width: 497px; } 
			 @media print {
			 	.noprint, .noprint * { display: none; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 15px; padding: 10px;}

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important;}
			tfoot tr td { font-size: 13px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			.footer{clear: both;width: 100%;position: relative;top: 700px; display: inline-block;}
			.software{width: 45%;display: inline-block;text-align: left;}
			.Pages{width: 54%;display: inline-block;text-align: right;}
						table tbody td{border: none;}
			table tbody td{border-bottom: 1px solid black;}

			/*.{padding-top: 6%;}*/
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
			
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:550px !important; display:inline-block !important;">
									<h3 class="invoice-type text-left" style="font-size: 22px; border:none !important; margin: 0px !important; "><?php echo $title; ?></h3>

								
									
									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
								</div> --> 
								<div class="block pull-right" style="width:900px !important; float: right; display:inline !important;">
									<div class="span12"><img style="float:right; width:300px !important;" class="rcpt-header logo-img" src="<?php echo $header_img; ?>" alt=""></div>
									
									
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th width="20px;" class="text-left" style="  ">DCNO</th>
									<th width="20px;" class="text-left" style=" ">Semester</th>
									<th width="100px;" class="text-left" style="  ">Name</th>
									<th width="20px;" class="text-left" style="  ">Total Marks</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$session = "";
									$section_name = "";
									$term = 0;
									
									function showClassSum($sumClass,$class_name){
										echo 	"<tr>";
										echo "<td colspan='2' style='text-align: right;'>" . $class_name . "</td>";
										echo "<td colspan='3' style='text-align: right;'>" ."Total:". "</td>";
										echo "<td style=''> " . $sumClass. "</td>";
										echo "</tr>";
									}
									function showSectionSum($section_total,$section_name) {
										echo  "<tr>";
										echo  "<td colspan='2' style=';text-align: right;'>". $section_name . "</td>";
										echo "<td colspan='3' style=';text-align: right;'>" ."Total:". "</td>";
										echo "<td style=';'> ". $section_total ."</td>";
										echo "</tr>";
									}
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									foreach ($vrdetail as $row):
									

								?>
								<?php if ($row['session'] !== $session): ?>
								 	 <tr>
								 	 <td colspan='4' style='color:red;'>				
								 	 <?php echo $row['session']; ?></td>
								 	 </tr>
								 	<?php  $session = $row['session'];?>
																
								<?php endif; ?>

								<?php if ($row['section_name'] !== $section_name): ?>
								 	<tr>
	 	 								<td colspan='4' style='color:blue;'> <?php echo  $row['section_name']; ?> </td>
	 	 							</tr>
								 	<?php  $section_name = $row['section_name'];?>
							
								<?php endif; ?>
									

								
									<?php if ($row['term'] !== $term): ?>
										<tr>
											<td colspan='' style='color:green;'>  </td>
											<td colspan='3' style='color:green;'> <?php echo $row['term']; ?> </td>
										</tr>
										<?php $term = $row['term']; 
										 ?>
									<?php endif; ?>

							
									<tr>

										<td> <?php echo $row['dcno']; ?> </td>
										<td></td>
										<td> <?php echo $row['subject_name']; ?> </td>
										<td class="text-right"> <?php echo round($row['tmarks'],2); ?> </td>
									</tr>
									

								

								<?php endforeach ?>
								
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
			
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>	