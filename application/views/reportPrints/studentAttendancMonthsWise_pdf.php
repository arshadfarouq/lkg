<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma; }
			 body { font-size:12px; }
			 p { margin: 0; /* line-height: 17px; */ }
			 .field {font-weight: bold; display: inline-block; width: 100px;margin-top: 15px; } 
			 .voucher-table{ border-collapse: collapse;margin-top: -5px; }
			 table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
			 th { border: 1px solid black; padding: 5px; }
			 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
			 td:first-child { text-align: left; }
			 .voucher-table thead th {background: #ccc; } 
			 tfoot {border-top: 1px solid black; } 
			 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
			 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			 .invoice-type { border-bottom: 1px solid black; }
			 .relative { position: relative; }
			 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			 .inv-leftblock { width: 280px; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
			 .rcpt-header { width: 450px; margin: auto; display: block; }
			 .inwords, .remBalInWords { text-transform: uppercase; }
			 .barcode { margin: auto; }
			 h3.invoice-type {font-size: 20px; line-height: 24px;}
			 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			 .nettotal { color: red; font-size: 12px;}
			 .remainingBalance { font-weight: bold; color: blue;}
			 .centered { margin: auto; }
			 p { position: relative; font-size: 16px; }
			 thead th { font-size: 13px; font-weight: normal; }
			 .fieldvalue.cust-name {position: absolute; width: 497px; } 
			 @media print {
			 	.noprint, .noprint * { display: none; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 15px; padding: 10px;}

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important;}
			tfoot tr td { font-size: 13px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			.footer{clear: both;width: 100%;position: relative;top: 700px; display: inline-block;}
			.software{width: 45%;display: inline-block;text-align: left;}
			.Pages{width: 54%;display: inline-block;text-align: right;}
						table tbody td{border: none;}
			table tbody td{border-bottom: 1px solid black;}

			/*.{padding-top: 6%;}*/
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
			
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:550px !important; display:inline-block !important;">
									<h3 class="invoice-type text-left" style="font-size: 22px; border:none !important; margin: 0px !important; "><?php echo $title; ?></h3>

									<p><span class="field">From : </span><span class="fieldvalue inv-vrnoa"><?php echo $from; ?></span></p>									
									<p><span class="field">To :</span><span class="fieldvalue inv-date"><?php echo  $to; ?></span></p>
									<p><span class="field">Class : </span><span class="fieldvalue inv-vrnoa"><?php echo $className; ?></span></p>									
									<p><span class="field">Section :</span><span class="fieldvalue inv-date"><?php echo  $sectionName; ?></span></p>
									<p><span class="field">Std id : </span><span class="fieldvalue inv-vrnoa"><?php echo $stdid; ?></span></p>									
									
									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
								</div> --> 
								<div class="block pull-right" style="width:900px !important; float: right; display:inline !important;">
									<div class="span12"><img style="float:right; width:300px !important;" class="rcpt-header logo-img" src="<?php echo $header_img; ?>" alt=""></div>
									
									
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th rowspan="2" class="txtcenter level4row" width="100px;">Student Name</th>
									<th colspan="3" class="txtcenter" style="background: #fff;">January</th>
									<th colspan="3" class="txtcenter" style="background: #e6f3fd;">February</th>
									<th colspan="3" class="txtcenter" style="background: #fff;">March</th>
									<th colspan="3" class="txtcenter" style="background: #e6f3fd;">April</th>
									<th colspan="3" class="txtcenter" style="background: #fff;">May</th>
									<th colspan="3" class="txtcenter" style="background: #e6f3fd;">June</th>
									<th colspan="3" class="txtcenter" style="background: #fff;">July</th>
									<th colspan="3" class="txtcenter" style="background: #e6f3fd;">August</th>
									<th colspan="3" class="txtcenter" style="background: #fff;">Septemder</th>
									<th colspan="3" class="txtcenter" style="background: #e6f3fd;">October</th>
									<th colspan="3" class="txtcenter" style="background: #fff;">November</th>
									<th colspan="3" class="txtcenter" style="background: #e6f3fd;">December</th>
								</tr>
								<tr>
									<th class="txtcenter" style="background: #fff;">A</th>
									<th class="txtcenter" style="background: #fff;">P</th>
									<th class="txtcenter" style="background: #fff;">L</th>

									<th class="txtcenter" style="background: #e6f3fd;">A</th>
									<th class="txtcenter" style="background: #e6f3fd;">P</th>
									<th class="txtcenter" style="background: #e6f3fd;">L</th>

									<th class="txtcenter" style="background: #fff;">A</th>
									<th class="txtcenter" style="background: #fff;">P</th>
									<th class="txtcenter" style="background: #fff;">L</th>

									<th class="txtcenter" style="background: #e6f3fd;">A</th>
									<th class="txtcenter" style="background: #e6f3fd;">P</th>
									<th class="txtcenter" style="background: #e6f3fd;">L</th>

									<th class="txtcenter" style="background: #fff;">A</th>
									<th class="txtcenter" style="background: #fff;">P</th>
									<th class="txtcenter" style="background: #fff;">L</th>

									<th class="txtcenter" style="background: #e6f3fd;">A</th>
									<th class="txtcenter" style="background: #e6f3fd;">P</th>
									<th class="txtcenter" style="background: #e6f3fd;">L</th>

									<th class="txtcenter" style="background: #fff;">A</th>
									<th class="txtcenter" style="background: #fff;">P</th>
									<th class="txtcenter" style="background: #fff;">L</th>

									<th class="txtcenter" style="background: #e6f3fd;">A</th>
									<th class="txtcenter" style="background: #e6f3fd;">P</th>
									<th class="txtcenter" style="background: #e6f3fd;">L</th>

									<th class="txtcenter" style="background: #fff;">A</th>
									<th class="txtcenter" style="background: #fff;">P</th>
									<th class="txtcenter" style="background: #fff;">L</th>

									<th class="txtcenter" style="background: #e6f3fd;">A</th>
									<th class="txtcenter" style="background: #e6f3fd;">P</th>
									<th class="txtcenter" style="background: #e6f3fd;">L</th>

									<th class="txtcenter" style="background: #fff;">A</th>
									<th class="txtcenter" style="background: #fff;">P</th>
									<th class="txtcenter" style="background: #fff;">L</th>

									<th class="txtcenter" style="background: #e6f3fd;">A</th>
									<th class="txtcenter" style="background: #e6f3fd;">P</th>
									<th class="txtcenter" style="background: #e6f3fd;">L</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$branch_name = "";
									$class_name = "";
									$section_name = "";
									$year = "";
									$student_name = "";
									$counter = 1;
									
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									foreach ($vrdetail as $row):
									

								?>
								<?php if ($row['branch_name'] !== $branch_name): ?>
								 	 <tr>
								 	 <td colspan='37' style='color:red;'>				
								 	 <?php echo $row['branch_name']; ?></td>
								 	 </tr>
								 	<?php  $branch_name = $row['branch_name'];?>
																	
								<?php endif; ?>
								<?php if ($row['class_name'] !== $class_name): ?>
								 	 <tr>
								 	 <td colspan='37' style='color:green;'>				
								 	 <?php echo $row['class_name']; ?></td>
								 	 </tr>
								 	<?php  $class_name = $row['class_name'];?>
																	
								<?php endif; ?>

								<?php if ($row['section_name'] !== $section_name): ?>
								 	<tr>
	 	 								<td colspan='37' style='color:blue;'> <?php echo  $row['section_name']; ?> </td>
	 	 							</tr>
								 	<?php  $section_name = $row['section_name'];?>
															
								<?php endif; ?>

								<?php if ($row['year'] !== $year): ?>
								 	<tr>
	 	 								<td colspan='37' style='color:grey;'> <?php echo  $row['year']; ?> </td>
	 	 							</tr>
								 	<?php  $year = $row['year'];?>
															
								<?php endif; ?>

								<?php if ($row['student_name'] !== $student_name && $row['year'] === $year ): 
									    $m1A = ''; 	$m1L = ''; 	$m1P = '';
									  	$m2A = ''; 	$m2L = ''; 	$m2P = '';
									  	$m3A = ''; 	$m3L = ''; 	$m3P = '';
									  	$m4A = ''; 	$m4L = ''; 	$m4P = '';
									  	$m5A = ''; 	$m5L = ''; 	$m5P = '';
									  	$m6A = ''; 	$m6L = ''; 	$m6P = '';
									  	$m7A = ''; 	$m7L = ''; 	$m7P = '';
									  	$m8A = ''; 	$m8L = ''; 	$m8P = '';
									  	$m9A = ''; 	$m9L = ''; 	$m9P = '';
									  	$m10A = ''; 	$m10L = ''; 	$m10P = '';
									  	$m11A = ''; 	$m11L = ''; 	$m11P = '';
									  	$m12A = ''; 	$m12L = ''; 	$m12P = '';
									  	foreach ($vrdetail as $rowTwo):
									  		if ($rowTwo['student_name'] === $row['student_name'] && $rowTwo['year'] === $row['year']) {

									  			if (strtolower($rowTwo['month']) === 'january') {
									  				$m1A = $rowTwo['absent'];
									  				$m1L = $rowTwo['leave'];
									  				$m1P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'february') {
									  				$m2A = $rowTwo['absent'];
									  				$m2L = $rowTwo['leave'];
									  				$m2P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'march') {
									  				$m3A = $rowTwo['absent'];
									  				$m3L = $rowTwo['leave'];
									  				$m3P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'april') {
									  				$m4A = $rowTwo['absent'];
									  				$m4L = $rowTwo['leave'];
									  				$m4P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'may') {
									  				$m5A = $rowTwo['absent'];
									  				$m5L = $rowTwo['leave'];
									  				$m5P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'june') {
									  				$m6A = $rowTwo['absent'];
									  				$m6L = $rowTwo['leave'];
									  				$m6P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'july') {
									  				$m7A = $rowTwo['absent'];
									  				$m7L = $rowTwo['leave'];
									  				$m7P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'august') {
									  				$m8A = $rowTwo['absent'];
									  				$m8L = $rowTwo['leave'];
									  				$m8P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'septemder') {
									  				$m9A = $rowTwo['absent'];
									  				$m9L = $rowTwo['leave'];
									  				$m9P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'october') {
									  				$m10A = $rowTwo['absent'];
									  				$m10L = $rowTwo['leave'];
									  				$m10P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'november') {
									  				$m11A = $rowTwo['absent'];
									  				$m11L = $rowTwo['leave'];
									  				$m11P = $rowTwo['present'];
									  			}
									  			if (strtolower($rowTwo['month']) === 'december') {
									  				$m12A = $rowTwo['absent'];
									  				$m12L = $rowTwo['leave'];
									  				$m12P = $rowTwo['present'];
									  			}
									  		}
									  	endforeach;
									?>
							<tr>
								<td> <?php echo $row['student_name']; ?> </td>
								<td class='' style=''> <?php echo $m1A; ?> </td>
								<td class='' style=''> <?php echo $m1P; ?> </td>
								<td class='' style=''> <?php echo $m1L; ?> </td>

								<td class='' style=' '> <?php echo $m2A; ?> </td>
								<td class='' style=' '> <?php echo $m2P; ?> </td>
								<td class='' style=' '> <?php echo $m2L; ?> </td>

								<td class='' style=''> <?php echo $m3A; ?> </td>
								<td class='' style=''> <?php echo $m3P; ?> </td>
								<td class='' style=''> <?php echo $m3L; ?> </td>

								<td class='' style=' '> <?php echo $m4A; ?> </td>
								<td class='' style=' '> <?php echo $m4P; ?> </td>
								<td class='' style=' '> <?php echo $m4L; ?> </td>

								<td class='' style=''> <?php echo $m5A; ?> </td>
								<td class='' style=''> <?php echo $m5P; ?> </td>
								<td class='' style=''> <?php echo $m5L; ?> </td>

								<td class='' style=' '> <?php echo $m6A; ?> </td>
								<td class='' style=' '> <?php echo $m6P; ?> </td>
								<td class='' style=' '> <?php echo $m6L; ?> </td>

								<td class='' style=''> <?php echo $m7A; ?> </td>
								<td class='' style=''> <?php echo $m7P; ?> </td>
								<td class='' style=''> <?php echo $m7L; ?> </td>

								<td class='' style=' '> <?php echo $m8A; ?> </td>
								<td class='' style=' '> <?php echo $m8P; ?> </td>
								<td class='' style=' '> <?php echo $m8L; ?> </td>

								<td class='' style=''> <?php echo $m9A; ?> </td>
								<td class='' style=''> <?php echo $m9P; ?> </td>
								<td class='' style=''> <?php echo $m9L; ?> </td>

								<td class='' style=' '> <?php echo $m10A; ?> </td>
								<td class='' style=' '> <?php echo $m10P; ?> </td>
								<td class='' style=' '> <?php echo $m10L; ?> </td>

								<td class='' style=''> <?php echo $m11A; ?> </td>
								<td class='' style=''> <?php echo $m11P; ?> </td>
								<td class='' style=''> <?php echo $m11L; ?> </td>

								<td class='' style=' '> <?php echo $m12A; ?> </td>
								<td class='' style=' '> <?php echo $m12P; ?> </td>
								<td class='' style=' '> <?php echo $m12L; ?> </td>
							</tr>
								 	
								 	<?php $student_name = ''; ?>
															
					<?php endif; ?>
									
								
								

								

								<?php endforeach ?>
							
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
			
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>	