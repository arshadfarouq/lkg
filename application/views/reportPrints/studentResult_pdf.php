<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Voucher</title>

	<link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

	<style>
		* { margin: 0; padding: 2; font-family: tahoma !important; }
		body { font-size: 10px !important; }
	p { margin: 0 !important; /* line-height: 17px !important; */ }
	.field { font-size: 10px !important; font-weight: bold !important; display: inline-block !important; width: 10px !important; } 
	.field1 { font-size: 10px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
	.voucher-table{ border-collapse: none !important; }
	table { width: 100% !important; border-bottom: 0.5px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:0px}
	th {  padding: 2px !important; }
	td { /*text-align: center !important;*/ vertical-align: top !important;  }
	td:first-child { text-align: left !important;  }
	.voucher-table thead th {background: #ccc !important; } 
	tfoot {border-top: 0.5px solid black !important; } 
	.bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
	.nettotal { font-weight: bold !important; font-size: 10px !important; border-top: 0.5px solid black !important; }
	.invoice-type { border-bottom: 0.5px solid black !important; }
	.relative { position: relative !important; }
	.signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
	.signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
	.inv-leftblock { width: 280px !important; }
	.text-left { text-align: left !important; }
	.text-right { text-align: right !important; }
	td {font-size: 10px !important; font-family: tahoma !important; line-height: 14px !important; padding: 2px !important;border-bottom: 0.5px !important; } 
	.rcpt-header { width: 450px !important;  margin: 0px; display: inline;  top: 0px; right: 0px; }
	.inwords, .remBalInWords { text-transform: uppercase !important; }
	.barcode { margin: auto !important; }
	h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
	.extra-detail span { background: #7F83E9 !important; color: white !important; padding: 2px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 10px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
	.nettotal { color: red !important; font-size: 10px !important;}
	.remainingBalance { font-weight: bold !important; color: blue !important;}
	.centered { margin: auto !important; }
	p { position: relative !important; font-size: 10px !important; }
	thead th { font-size: 10px !important; font-weight: bold !important; padding: 2px !important; }
	.fieldvalue.cust-name {position: absolute; width: 497px; margin-right:130px !important;  }

	@media print {
		.noprint, .noprint * { display: none !important; }
	}
	.pl20 { padding-left: 20px !important;}
	.pl40 { padding-left: 40px !important;}

	.barcode { float: right !important; }
	.item-row td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
	.foot-comments td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important; font-weight: bold !important;}


	.footer_company td { font-size: 8px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
	@page{margin-top: 5mm; margin-left: 2mm;margin-right: 2mm;margin-bottom: 2mm; size !important:  auto !important;  }


	h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
	tfoot tr td { font-size: 10px !important; padding: 2px !important;  }
	.nettotal, .subtotal, .vrqty,.vrweight { font-size: 10px !important; font-weight: bold !important;}
	.footer {
		position: absolute;
		color: red;
		bottom: 0;
	}
	tr { page-break-inside: avoid !important; }
	tr td ul li { page-break-inside: avoid !important; }

	/*.{padding-top: 6%;}*/
</style>
</head>
<body>
	<div class="container-fluid" style="">
		<div class="row-fluid">
			
			<div class="span12 centered">
			<div class="row-fluid">
						<div class="span12"><img class="rcpt-header" src="<?php echo $header_img;?>" alt=""></div>
					</div>
					
					<div class="row-fluid">
						<table class="voucher-table" style="border:none !important;">
							<tbody>
								
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Term :</td>
									<td style=" width: 30px; font-size:12px !important;"> <?php echo $term;?> </td>
									
								</tr>
								
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Class :</td>
									<td style=" width: 30px; font-size:12px !important;"> <?php echo $className;?> </td>
									
								</tr>
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Status : </td>
									<td style=" width: 30px; font-size:12px !important;"> <?php echo $stdid;?> </td>
									
								</tr>
								
								<tr>
									<td  style=" width: 10px; font-weight:bold !important; font-size:12px !important;">Section :</td>
									<td style=" width: 30px; font-size:12px !important;"> <?php echo $sectionName;?> </td>
									
								</tr>
								
							</tbody>
						</table>
					</div>

					<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
						<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 12px !important; font-size:16px !important; "><?php echo $title;?></h3>
					</div>
				
						<br>
						<br>
						<br>

						<div class="row-fluid">
							<table class="voucher-table">
								<thead>
									<tr>
										<th width="5px" class="text-left" style=" padding: 0; ">Sr#</th>
										<th width="50px" class="text-left" style="  ">Subject</th>
										<th width="20px" class="text-left" style=" ">Total Marks</th>
										<th width="20px" class="text-left" style="  ">Obtained Marks</th>
										<th width="20px" class="text-left" style="  ">Percentage</th>
										<th width="50px" class="text-left" style="  ">Remarks</th>
									</tr>
								</thead>

								<tbody>

									<?php 
									$total_obmarks = 0;
									$total_tmarks = 0;
									$counter = 1;
									
									
									// echo "<pre>";
									// var_dump($vrdetail);
									// echo "</pre>";
									foreach ($vrdetail as $row):


										?>

									<?php $total_obmarks += floatval($row['obmarks']);
									$total_tmarks += floatval($row['tmarks']);	
									?>
									<tr style="border-bottom: 0.5px solid !important;">
										<td> <?php echo $counter++; ?> </td>
										<td> <?php echo $row['subject_name']; ?> </td>
										<td class="text-right"> <?php echo round($row['tmarks'],2); ?> </td>
										<td class="text-right"> <?php echo round($row['obmarks'],2); ?> </td>
										<td class="text-right"> <?php echo round($row['percentage'],2); ?> </td>
										<td> <?php echo $row['remarks']; ?> </td>
									</tr>

								<?php endforeach ?>
								<tr>
									<td>  </td>
									<td> Total </td>
									<td class="text-right"> <?php echo round($total_tmarks,2); ?> </td>
									<td class="text-right"> <?php echo round($total_obmarks,2); ?> </td>
									<td class="text-right"> <?php echo round((floatval($total_obmarks)/floatval($total_tmarks))*100,2); ?> </td>
									<td> </td>
								</tr>
								
							</tbody>
							
						</table>
					</div>

					<!-- End row-fluid -->
					<br> 
					<br> 

					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
			
			</div>
		</div>
	</body>
	</html>	