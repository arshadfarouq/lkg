<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma; }
			 body { font-size:12px; }
			 p { margin: 0; /* line-height: 17px; */ }
			 .field {font-weight: bold; display: inline-block; width: 100px; } 
			 .voucher-table{ border-collapse: collapse;margin-top: 60px; }
			 table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed;}
			 th { border: 1px solid black; padding: 5px; }
			 td { /*text-align: center;*/ vertical-align: top; /*padding: 5px 10px;*/ border-left: 1px solid black;}
			 td:first-child { text-align: left; }
			 .voucher-table thead th {background: #ccc; } 
			 tfoot {border-top: 1px solid black; } 
			 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
			 .nettotal { font-weight: bold; font-size: 11px !important; border-top: 1px solid black; }
			 .invoice-type { border-bottom: 1px solid black; }
			 .relative { position: relative; }
			 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
			 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
			 .inv-leftblock { width: 280px; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px; } 
			 .rcpt-header { width: 450px; margin: auto; display: block; }
			 .inwords, .remBalInWords { text-transform: uppercase; }
			 .barcode { margin: auto; }
			 h3.invoice-type {font-size: 20px; line-height: 24px;}
			 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; margin: 5px 0px; font-size: 10px; text-transform: uppercase; letter-spacing: 1px;}
			 .nettotal { color: red; font-size: 12px;}
			 .remainingBalance { font-weight: bold; color: blue;}
			 .centered { margin: auto; }
			 p { position: relative; font-size: 16px; }
			 thead th { font-size: 13px; font-weight: normal; }
			 .fieldvalue.cust-name {position: absolute; width: 497px; } 
			 @media print {
			 	.noprint, .noprint * { display: none; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right; }
			.item-row td { font-size: 15px; padding: 10px;}

			.rcpt-header { width: 205px !important; margin: 0px; display: inline; position: absolute; top: 0px; right: 0px; }
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative; top: 34px; }
			tfoot tr td { font-size: 13px; padding: 5px; }
			.nettotal, .subtotal, .vrqty { font-size: 14px !important; font-weight: normal !important;}
			.footer{clear: both;width: 100%;position: relative;top: 700px; display: inline-block;}
			.software{width: 45%;display: inline-block;text-align: left;}
			.Pages{width: 54%;display: inline-block;text-align: right;}
			.text-left{text-align: left;}

			/*.{padding-top: 6%;}*/
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
			
				<div class="span12 centered">
				<!-- 	<div class="row-fluid top-img-head" style="display:none; padding-top: 15px; padding-bottom: 15px;">
						<div class="span12">
							<img src="../../assets/uploads/header_imgs/header_img.png" class="top-head" alt="">
						</div>
					</div> -->
					<div class="row-fluid relative">
						<div class="span12">
								<!-- <div class="block pull-left inv-leftblock" style="width:250px !important; display:inline-block !important;">
									<p><span class="field">Invoice #</span><span class="fieldvalue inv-vrnoa"><?php echo $vrdetail[0]['vrnoa']; ?></span></p>									
									<p><span class="field">Date</span><span class="fieldvalue inv-date"><?php echo substr($vrdetail[0]['vrdate'], 0, 10); ?></span></p>
									<p><span class="field ">Customer</span><span class="fieldvalue cust-name"><?php echo $vrdetail[0]['custparty']; ?><?php //echo $vrdetail[0]['custname'] ? ' / ' . $vrdetail[0]['custname'] : ''; ?></span></p>
									<p><span class="field">Mobile</span><span class="fieldvalue cust-mobile"><?php echo $vrdetail[0]['custmobile'] ? $vrdetail[0]['custmobile'] : '-- N/A --'; ?></span></p> -->
									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p>
								</div> --> 
								<div class="block pull-right" style="width:900px !important; float: right; display:inline !important;">
									<div class="span12"><img style="float:right; width:300px !important;" class="rcpt-header logo-img" src="<?php echo $header_img; ?>" alt=""></div>
									<!-- <div class="span12"><img style="float:right; width:180px !important;" class="rcpt-header logo-img" src="../../assets/uploads/header_imgs/header_img.png"  alt=""></div> -->
									<h3 class="invoice-type text-right" style="font-size: 32px; border:none !important; margin: 0px !important; position: relative; top: 70px; "><?php echo $title; ?></h3>
									<!-- <h3 class="invoice-type text-right" style="font-size: 12px; border:none !important; margin: 0px !important; position: relative; top: 0px; "><?//php echo $header_img; ?></h3> -->
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th class="text-left" style=" width: 10px; padding: 2; ">Child</th>
									<th class="text-left" style=" width: 120px; ">Description</th>
									<th class="text-left" style=" width: 30px; ">Charges</th>
									<th class="text-left" style=" width: 40px; ">Affected Accounts</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$serial = 1;
									$netQty = 0;
									// echo "<pre>";
									// var_dump($header_img);
									// echo "</pre>";
									foreach ($vrdetail as $row): 
									// 	$netQty += abs($row['qty']);
								?>
									
									<tr style="border-bottom:1px dotted #ccc;" class="item-row">
									   <!-- <td><?php //echo $serial++; ?></td> -->
									   <td class='text-left'><?php  echo $row['chid']; ?></td>
									   <td class='text-left'><?php echo $row['description']; ?></td>
									   <td class='text-right'><?php echo $row['charges']; ?></td>
									   <td class='text-left'><?php echo $row['affected_account']; ?></td>
									  
									   <!-- <td class="text-right"><?php //echo intval($row['namount']); ?></td> -->
									</tr>

								<?php endforeach ?>
							</tbody>
							
						</table>
					</div>
			
					<!-- End row-fluid -->
					<br> 
					<br> 
				<!-- 	<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th>Accountant</th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div> -->
					
					<div class="footer">
						<div class="software">
							<p class="text-left " style="display: inline-block;">Software By:www.alnaharsolution.com</p>
						</div>
						<div class="Pages">
							<p class="text-right " style="display: inline-block;">Page:N of N</p>

						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>	