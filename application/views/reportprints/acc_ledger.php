<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

		<style>
			  * { margin: 0; padding: 0; font-family: tahoma !important; }
			 body { font-size:10px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size:10px !important; font-weight: bold !important; display: inline-block !important; width: 100px !important; } 
			 .field1 { font-size:10px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border-collapse: none !important; }
			 table { width: 100% !important; border: 0.5px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:0px}
			 th {  padding: 2px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child { text-align: left !important; }
			 .voucher-table thead th {background: #ccc !important; } 
			 tfoot {border-top: 0.5px solid black !important; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 10px !important; border-top: 0.5px solid black !important; }
			 .invoice-type { border-bottom: 1px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-top: 0.5px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 10px !important; font-family: tahoma !important; line-height: 14px !important; padding: 2px !important; } 
			 .rcpt-header { width: 700px !important; margin: 0px; display: inline;  top: 0px; right: 0px; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 20px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 2px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 10px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 10px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 10px !important; }
			 thead th { font-size: 10px !important; font-weight: bold !important; padding: 5px !important; }
			 .fieldvalue { font-size:10px !important; position: absolute !important; width: 497px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }

			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 10px !important; padding: 2px !important; border-top: 0.5px solid black !important;}
			.footer_company td { font-size: 8px !important; padding: 2px !important; border-top: 0.5px solid black !important;}

			
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; }
			tfoot tr td { font-size: 10px !important; padding: 2px !important;  }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 2px !important; font-weight: bold !important;}
			tr { page-break-inside: avoid;	}
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
				<div class="span12 centered">
					<div class="row-fluid">
						<div class="span12"><img class="rcpt-header" src="<?php echo $header_img;?>" alt=""></div>
					</div>
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:150px !important; display:inline !important;">
									<p><span class="field">Account Title:</span><span class="fieldvalue inv-vrnoa"><?php echo $pledger[0]['party_name']; ?></span></p>									
									<p><span class="field">Account Code:</span><span class="fieldvalue inv-date"><?php echo $pledger[0]['acc_id']; ?></span></p>
									<p><span class="field">Address:</span><span class="fieldvalue inv-date"><?php echo $pledger[0]['address']; ?></span></p>
									<p><span class="field">Contact:</span><span class="fieldvalue inv-date"><?php echo $pledger[0]['contact']; ?></span></p>
									<p><span class="field">Dated From: </span><span class="fieldvalue inv-date"><?php echo $date_between; ?></span></p>
								</div>
								<div class="block pull-right" style="width:300px !important; float: right; display:inline !important;">
									<h3 class="invoice-type text-right" style="border:0px !important; "><?php echo $title; ?></h3>
								</div>
						</div>
					</div>
					
					
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 40px; " class="text-left">Date#</th>
									<th style=" width: 50px; "class="text-left">Voucher</th>
									<th style=" width: 10px; " class="text-left">Inv#</th>
									<th style=" width: 150px; " class="text-left">Description</th>
									<th style=" width: 60px; " class="text-right">Debit</th>
									<th style=" width: 60px; " class="text-right">Credit</th>
									<th style=" width: 60px; " class="text-right">Balance</th>
									<th style=" width: 15px; " class="text-right">Dr/Cr</th>
								</tr>
							</thead>

							<tbody>
								<tr style="amountborder-bottom:0.5px dotted #ccc;" class="item-row">
										<td class="text-right " ></td>
										<td class="text-right " ></td>
										<td class="text-left " ></td>
										<td class="text-left" >Opening Balance</td>
										<td class="text-right " ></td>
										<td class="text-right " ></td>
										<td class="text-right " ><?php echo ($previousBalance!=0?number_format($previousBalance,2):'-');?></td>
										<td class="text-right " ><?php echo ($previousBalance<0?"Cr":"Dr");?></td>
								</tr>
								<?php 
									$serial = 1;
									$Total_Debit = 0;
									$Total_Credit = 0;
									$Rtotal = 0;
									$Rtotal= $previousBalance;
									
									
									if (empty($pledger)) {
            								//location('payment');
        								}
        							else{

									foreach ($pledger as $row): 
									
											$Total_Debit += $row['debit'];
									
											$Total_Credit += $row['credit'];
											$Rtotal += $row['debit']-$row['credit'];
								?>
									
									<tr style="amountborder-bottom:0.5px dotted #ccc;" class="item-row">
									   <td > <?php echo date('d-M-y', strtotime($row['date'])); ?></td>
									   <td 	class='text-left' ><?php echo $row['etype'] . '-' . $row['dcno']; ?> </td>
									   <td  class='text-left'><?php echo $row['invoice']; ?></td>
									   <td  class='text-left'><?php echo $row['description']; ?></td>
									   <td  class="text-right"><?php echo ($row['debit']!=0? number_format($row['debit'],2):'-'); ?></td>
									   <td  class="text-right"><?php echo ($row['credit']!=0? number_format($row['credit'],2):'-'); ?></td>
									   <td  class="text-right"><?php echo ($Rtotal!=0? number_format($Rtotal,2):'-'); ?></td>
									   <td  class="text-right"><?php echo ($Rtotal<0?"Cr":"Dr"); ?></td>									   
									</tr>

								<?php   endforeach ?>
								<?php  // } ?>
								<tr style="amountborder-bottom:0.5px dotted #ccc;" class="item-row">
									<!-- <td class="bold-td"></td> -->
									<td class="text-right " colspan="4">Total</td>
									<td class="text-right "><?php echo ($Total_Debit!=0? number_format($Total_Debit,2):'-'); ?></td>
									<td class="text-right "><?php echo ($Total_Credit!=0? number_format($Total_Credit,2):'-'); ?></td>
									<td class="text-right" ><?php echo ($Rtotal!=0? number_format($Rtotal,2):'-'); ?></td>
									<td class="text-right" ><?php echo ($Rtotal<0?"Cr":"Dr"); ?></td>
								</tr>
							</tbody>
							<!-- <tfoot> -->
							<!-- 	<tr class="foot-comments">
									<td class="vrqty bold-td text-right"><?php //echo $Total_Debit; ?></td>
									<td class="bold-td text-right" colspan="3">Subtotal</td>
									<td class="bold-td"></td>
								</tr> -->
								
							<!-- </tfoot> -->
							<?php } ?>
						</table>
					</div>
					<br> 
					<br> 
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th>Accountant</th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="row-fluid">
						<p>
							<span class="loggedin_name">User: <?php echo $user;?></span><br>
							<span class="website">Sofware By: www.alnaharsolutions.com, Mob: 03009663902</span>
							
						</p>
					</div>

				</div>
			</div>
		</div>
	</body>
	</html>	