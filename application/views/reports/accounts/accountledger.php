<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Account Ledger</h1>
			</div>
		</div>
	</div>
  <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h3 id="myModalLabel">Party Lookup</h3>
        </div>

        <div class="modal-body">
          <table class="table table-striped modal-table" id ="tblAccounts">

            <thead>
              <tr style="font-size:16px;">
                <th>Id</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>Address</th>
                <th style='width:3px;'>Actions</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
        <div class="modal-footer">

          <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <div class="page_content">
    <div class="container-fluid">

     <div class="row">
      <div class="col-lg-12">
       <div class="panel panel-default">
        <div class="panel-body">
         <div class="row">
         <input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
           <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
           <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
           <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
           <div class="col-lg-2">
             <label>From</label>
             <input class="form-control ts_datepicker" type="text" id="from_date" >
           </div>
           <div class="col-lg-2">
             <label>To</label>
             <input class="form-control ts_datepicker" type="text" id="to_date">
           </div>
           <div class="col-lg-4">

             <label for="">Account <img id="imgPartyLoader" class="hide" src="<?php echo base_url('assets/img/loader.gif'); ?>"></label>
             <div class="input-group" >
              <input type="text" class="form-control" id="txtPartyId">
              <input id="hfPartyId" type="hidden" value="" />
              <input id="hfPartyBalance" type="hidden" value="" />
              <input id="hfPartyCity" type="hidden" value="" />
              <input id="hfPartyAddress" type="hidden" value="" />
              <input id="hfPartyCityArea" type="hidden" value="" />
              <input id="hfPartyMobile" type="hidden" value="" />
              <input id="hfPartyUname" type="hidden" value="" />
              <input id="hfPartyLimit" type="hidden" value="" />
              <input id="hfPartyName" type="hidden" value="" />
              <input id="hfPartyEmail" type="hidden" value="" />

              <input id="txtHiddenEditQty" type="hidden" value="" />
              <input id="txtHiddenEditRow" type="hidden" value="" />
              <a  tabindex="-1" class="input-group-addon btn btn-primary active btnsearchparty" style="min-width:40px !important;" id="A2" data-target="#party-lookup" data-toggle="modal" href="#addCategory" rel="tooltip"
              data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Search Account (F1)"><i class="fa fa-search"></i></a>
            </div>
          </div>

          <div class="col-lg-4">
           <label>.</label>
           <div class="pull-right">
            <a class="btn btn-default btnSearch"><i class="fa fa-search"></i> Show F6</a>
            <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li ><a href="#" class="btnPrint3">Print F7</a></li>
                <li ><a href="#" class="btnPrintUrdu">Print Urdu</a></li>
                <li ><a href="#" class="btnPrint">Pdf F8</a></li>
                <li><a data-toggle="modal" href="#addEmail" rel="tooltip"
                  data-placement="top" data-original-title="Add Email" data-toggle="modal" class="btnPrintEmail">Email</a></li>
                  <li ><a href="#" class="btnPrint2">Account Flow F8</a></li>


                </ul>
              </div>


            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
   <div class="panel panel-default">
    <div class="panel-body">

     <div class="row">
      <div class="col-lg-2">
        <div class="form-group">                                                                
          <div class="input-group">
            <span class="switch-addon">Print Header?</span>
            <input type="checkbox" checked="" class="bs_switch" id="switchPrintHeader">
          </div>
        </div>
      </div>
      <div class="col-lg-10">
        <div class="pull-right acc_ledger">
          <ul class="stats">
            <li class='blue'>
              <div class="details">
                <span class="big opening-bal">0</span>
                <span>Opening Balance</span>
              </div>
            </li>
            <li class='red'>
              <div class="details">
                <span class="big net-debit">0</span>
                <span>Total Debit</span>
              </div>
            </li>
            <li class='green'>
              <div class="details">
                <span class="big net-credit">0</span>
                <span>Total Credit</span>
              </div>
            </li>
            <li class='brown'>
              <div class="details">
                <span class="big running-total">0</span>
                <span class="txtbold">Closing Balance</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="row">
      <div id="no-more-tables">								
      <table class="table table-striped table-hover ar-datatable rptTable saleRows col-lg-12 table-bordered table-striped table-condensed cf" id="datatable_example">
         <thead class="cf">
          <tr>

           <th style="width:10px;">Sr#</th>
           <th style="width:50px !important;">Date</th>
           <th style="width:50px !important;">Voucher</th>
           <th style="width:190px !important;">Description</th>
           <th style="text-align: right; width:20px !important;" class="numeric">Debit</th>
           <th style="text-align: right; width:20px !important;" class="numeric">Credit</th>
           <th style="text-align: right; width:10px !important;" class="numeric">Balance</th>
           <th style="width:1px !important;">D/C</th>
         </tr>
       </thead>
       <tbody id="CPVRows">
       </tbody>
     </table>
   </div>
 </div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div id="addEmail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        ×</button>
        <h3 id="myModalLabel">Email</h3>
      </div>

      <div class="modal-body">
        <div style="padding: 10px;">
          <div class="form-row control-group row-fluid">
            <label>Enter email address here:</label>
            <input id="txtAddEmail" type="text" style="width: 80%;">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal">
          Close</button>
          <button id="btnSendEmail" class="btn btn-primary">
            Send</button>
          </div>
        </div>
      </div>
    </div>