<script id="pr-head-template" type="text/x-handlebars-template">
  <tr>
    <th class="no_sort" style="width:50px;">Serial</th>
    <th class="no_sort EnglishName">Account</th>
    <th class="no_sort hidden UrduName">UrduName</th>
    <th class="no_sort">Address</th>
    <th class="no_sort">Contact</th>
    <th class="no_sort">Phone</th>
    <th class="no_sort" style="text-align:right; !important">Balance</th>
</tr>
</script>
<script id="pr-row-template" type="text/x-handlebars-template">
  <tr>
    <td class="no_sort tblSerial">{{SERIAL}}</td>
    <td class="no_sort tblParty EnglishName">{{PARTY}}</td>
    <td class="no_sort tblParty hidden UrduName">{{URDUNAME}}</td>
    <td class="no_sort tblAddress">{{ADDRESS}}</td>
    <td class="no_sort tblMobile">{{MOBILE}}</td>
    <td class="no_sort tblPhone">{{PHONE_OFF}}</td>
    <td class="no_sort tblBalance" style="text-align:right; !important">{{BALANCE}}</td>
</tr>
</script>

<script id="pr-row-template-group-footer" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td> </td>
    <td class="EnglishName"> </td>
    <td class="hidden UrduName"> </td>
    <td class="no_sort tblParty">{{TotalName}}</td>
    
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right">{{BalanceTotal}}</td>
</tr>
</script>
<script id="pr-row-template-group-head" type="text/x-handlebars-template">
  <tr style="background-color:gray !important;" class="">
    <td> </td>
    <td class="no_sort tblParty EnglishName">{{GroupName}}</td>
    <td class="no_sort hidden UrduName">{{GroupName}}</td>
    
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    
</tr>
</script>


<script id="pr-netsum-template" type="text/x-handlebars-template">
  <tr class="subsum_tr">
    <td class="no_sort EnglishName"></td>
    <td class="no_sort hidden UrduName"></td>

    <td class="no_sort"></td>
    <td class="no_sort"></td>
    <td class="no_sort"></td>
    <td class="no_sort text-right">Net</td>
    <td class="no_sort netamt_td" style="text-align:right; !important">{{NETSUM}}</td>
</tr>
</script>
<script id="db-head-template" type="text/x-handlebars-template">
  <tr>
    <th class="no_sort">Serial</th>
    <th class="no_sort" style="width:50px; !important">Date</th>
    <th class="no_sort"  >Vr #</th>
    <th class="no_sort" style="width:280px; !important" >Account</th>
    <th class="no_sort">Remarks</th>
    <th class="no_sort dont-show">Etype</th>
    <th class="no_sort">Debit</th>
    <th class="no_sort">Credit</th>
</tr>
</script>
<script id="db-phead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td class="dont-show"></td>
    <td>{{PARTY}}</td>
    <td class="printRemove"></td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="db-ihead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td style="text-transform:uppercase;">{{{VRNOA}}}</td>
    <td class="dont-show"></td>
    <td class="printRemove"></td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="db-dhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td>{{DATE1}}</td>
    <td></td>
    <td class="dont-show"></td>
    <td class="printRemove"></td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="jv-head-template" type="text/x-handlebars-template">
  <tr>
    <th class="no_sort">Serial</th>
    <th class="no_sort">Date</th>
    <th class="no_sort">Vr #</th>
    <th class="no_sort">Account</th>
    <th class="no_sort printRemove">Remarks</th>
    <th class="no_sort">Credit</th>
    <th class="no_sort">Debit</th>
</tr>
</script>
<script id="jv-phead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td></td>
    <td>{{PARTY}}</td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="jv-ihead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td>{{{VRNOA}}}</td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="jv-dhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td>{{DATE1}}</td>
    <td></td>
    <td></td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="payment-head-template" type="text/x-handlebars-template">
  <tr>
    <th class="no_sort">Serial</th>
    <th class="no_sort" style="width:59px !important; ">Date</th>
    <th class="no_sort">Vr#</th>
    <th class="no_sort">Account</th>
    <th class="no_sort">Remarks</th>
    <th class="no_sort text-right">Amount</th>
</tr>
</script>
<script id="db-row-template" type="text/x-handlebars-template">
  <tr>
    <td>{{SERIAL}}</td>
    <td>{{DATE}}</td>
    <td style="text-transform:uppercase;">{{{VRNOA}}}</td>
    <td>{{PARTY}}</td>
    <td class="printRemove">{{REMARKS}}</td>
    <td class="printRemove dont-show">{{ETYPE}}</td>
    <td class="text-right">{{DEBIT}}</td>
    <td class="text-right">{{CREDIT}}</td>
</tr>
</script>
<script id="jv-row-template" type="text/x-handlebars-template">
  <tr>
    <td>{{SERIAL}}</td>
    <td>{{DATE}}</td>
    <td>{{{VRNOA}}}</td>
    <td>{{PARTY}}</td>
    <td class="printRemove">{{REMARKS}}</td>
    <td class="text-right">{{CREDIT}}</td>
    <td class="text-right">{{DEBIT}}</td>
</tr>
</script>
<script id="jv-subsum-template" type="text/x-handlebars-template">
  <tr class="subsum_tr">
    <td></td>
    <td></td>
    <td></td>
    <td class="printRemove"></td>
    <td class="text-right">Sub</td>
    <td class="text-right">{{SUB_CREDIT}}</td>
    <td class="text-right">{{SUB_DEBIT}}</td>
</tr>
</script>
<script id="daybook-subsum-template" type="text/x-handlebars-template">
  <tr class="subsum_tr">
    <td></td>
    <td></td>
    <td class="dont-show"></td>
    <td class="printRemove"></td>
    <td class="printRemove"></td>
    <td class="text-right">Sub</td>
    <td class="text-right">{{SUB_DEBIT}}</td>
    <td class="text-right">{{SUB_CREDIT}}</td>
</tr>
</script>
<script id="daybook-netsum-template" type="text/x-handlebars-template">
  <tr class="netsum_tr">
    <td></td>
    <td></td>
    <td class="dont-show"></td>
    <td class="printRemove"></td>
    <td class="printRemove"></td>
    <td class="text-right">Net Amount</td>
    <td class="text-right">{{NET_DEBIT}}</td>
    <td class="text-right">{{NET_CREDIT}}</td>
</tr>
</script>
<script id="jv-netsum-template" type="text/x-handlebars-template">
  <tr class="netsum_tr">
    <td></td>
    <td></td>
    <td></td>
    <td class="printRemove"></td>
    <td class="text-right">Net Amount</td>
    <td class="text-right">{{NET_CREDIT}}</td>
    <td class="text-right">{{NET_DEBIT}}</td>
</tr>
</script>
<script id="payment-row-template" type="text/x-handlebars-template">
  <tr>
    <td>{{SERIAL}}</td>
    <td>{{DATE}}</td>
    <td>{{{VRNOA}}}</td>
    <td>{{PARTY}}</td>
    <td>{{REMARKS}}</td>
    <td class="text-right">{{AMOUNT}}</td>
</tr>
</script>
<script id="payment-phead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td></td>
    <td>{{PARTY}}</td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="payment-ihead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td>{{{VRNOA}}}</td>
    <td></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="payment-dhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td>{{DATE1}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="payment-netsum-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td style="font-weight:bold; text-align:right; color:white;">Net Total</td>
    <td class="text-right">{{NETSUM}}</td>
</tr>
</script>
<script id="payment-subsum-template" type="text/x-handlebars-template">
  <tr class="subsum_tr">
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td style="font-weight:bold; text-align:right; color:black;">Sub Total</td>
    <td class="text-right txtbold">{{SUBSUM}}</td>
</tr>
</script>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Account Reports</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">


                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <input class="form-control ts_datepicker" type="text" id="from">
                                                <input type="hidden" id="AccountNameTh" value="اکاؤنٹ">
                                                <input type="hidden" id="AddressTh" value="ایڈریس">
                                                <input type="hidden" id="EmailTh" value="ای میل">
                                                <input type="hidden" id="PhoneTh" value="فون">
                                                <input type="hidden" id="ContactTh" value="رابطہ">
                                                <input type="hidden" id="BalanceTh" value="بیلنس">
                                                


                                                <input type="hidden" id="PageHeadingPay" value="دینے والے">
                                                <input type="hidden" id="PageHeadingRec" value="لینے والے">     

                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <input class="form-control ts_datepicker" type="text" id="to">
                                            </div>
                                        </div>
                                    </div>

                                    <legend style='margin-top: 30px;'>Report Type</legend>
                                    <div class="row">
                                        <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                        <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                        <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                        <div class="col-lg-2">
                                            <label for="cpv" class="radio cpvRadio">
                                                <input type="radio" id="cpv" name="etype" value="cpv" checked="checked" />
                                                Cash Payment
                                            </label>
                                        </div>

                                        <div class="col-lg-2">
                                            <label for="crv" class="radio crvRadio">
                                                <input type="radio" id="crv" name="etype" value="crv" />
                                                Cash Receipt
                                            </label>
                                        </div>
                                        <div class="col-lg-1">
                                            <label for="jv" class="radio crvRadio">
                                                <input type="radio" id="jv" name="etype" value="jv" />
                                                Jv
                                            </label>
                                        </div>

                                        <div class="col-lg-2">
                                            <label for="expense" class="radio crvRadio">
                                                <input type="radio" id="expense" name="etype" value="expense" />
                                                Expense
                                            </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="payable" class="radio crvRadio">
                                                <input type="radio" id="payable" name="etype" value="payable" />
                                                Payable
                                            </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="receiveable" class="radio crvRadio">
                                                <input type="radio" id="receiveable" name="etype" value="receiveable" />
                                                Receiveable
                                            </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="daybook" class="radio crvRadio">
                                                <input type="radio" id="daybook" name="etype" value="daybook" />
                                                Day Book
                                            </label>
                                        </div>
                                    </div>

                                    <legend style='margin-top: 30px;'>Group By</legend>
                                    <div class="groupby-filter">


                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label for="date" class="radio cpvRadio">
                                                    <input type="radio" id='date' name="grouping" value="date" checked="checked" />
                                                    Date Wise
                                                </label>
                                            </div>

                                            <div class="col-lg-2">
                                                <label for="voucher" class="radio crvRadio">
                                                    <input type="radio" id='voucher' name="grouping" value="invoice" />
                                                    Voucher Wise
                                                </label>
                                            </div>

                                            <div class="col-lg-2">
                                                <label for="party" class="radio crvRadio">
                                                    <input type="radio" id='party' name="grouping" value="party" />
                                                    Party Wise
                                                </label>
                                            </div>

                                            <div class="col-lg-2">
                                                <label for="user1" class="radio crvRadio">
                                                    <input type="radio" id='user1' name="grouping" value="user" />
                                                    User Wise
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="groupby-filter-pr hide">


                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label for="city" class="radio cpvRadio">
                                                    <input type="radio" id='city' name="grouping" value="city"  />
                                                    City Wise
                                                </label>
                                            </div>

                                            <div class="col-lg-2">
                                                <label for="area" class="radio crvRadio">
                                                    <input type="radio" id='area' name="grouping" value="area" />
                                                    Area Wise
                                                </label>
                                            </div>

                                            <div class="col-lg-2">
                                                <label for="type" class="radio crvRadio">
                                                    <input type="radio" id='type' name="grouping" value="type" />
                                                    Type Wise
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="" class="btn btn-primary show-rept">Show Report</a>
                                            <a href="" class="btn btn-danger reset-rept">Reset Filters</a>
                                            <a href="" class="printCpvCrvBtn btn btn-primary">Print Report</a>
                                            <a href="" class="printPayRcvBtn btn btn-primary" style="display:none;">Print Report</a>
                                            <a href="" class="printPayRcvBtnUrdu btn btn-primary" style="display:none;">Urdu Print</a>

                                            <a href="" class="printDayBook btn btn-primary" style="display:none;">Print Report</a>
                                        </div>
                                    </div>
                                    <!-- Advanced Panels -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <button type="button" class="btn btnAdvaced">Advanced Option</button>
                                                </div>
                                            </div>
                                            <div class="panel-group panel-group1 panelDisplay" id="accordion" role="tablist" aria-multiselectable="true">


                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                      <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
                                                          Account Options
                                                      </a>
                                                  </h4>
                                              </div>
                                              <div id="collapsethree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                  <div class="panel-body">
                                                    <form class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-2" >
                                                                    <label >Account Name
                                                                    </label>        
                                                                    <select  class="form-control input-sm select2 " multiple="true" id="drpAccountID" data-placeholder="Choose Party....">

                                                                        <?php   foreach( $parties as $party):       ?>
                                                                         <option value=<?php echo $party['pid']?>><span><?php echo $party['name'];?></span></option>
                                                                     <?php   endforeach                ?>
                                                                 </select>
                                                             </div>
                                                             <div class="col-lg-2" >
                                                                <label >City
                                                                </label>        
                                                                <select  class="form-control input-sm select2 " multiple="true" id="drpCity" data-placeholder="Choose Party....">

                                                                    <?php   foreach( $cities as $citiy):       ?>
                                                                     <option value=<?php echo $citiy['city']?>><span><?php echo $citiy['city'];?></span></option>
                                                                 <?php   endforeach                ?>
                                                             </select>
                                                         </div>
                                                         <div class="col-lg-2">
                                                            <label >Area
                                                            </label>                    
                                                            <select  class="form-control input-sm select2" multiple="true" id="drpCityArea" data-placeholder="Choose Item....">

                                                                <?php   foreach( $cityareas as $cityarea):         ?>
                                                                 <option value=<?php echo $cityarea['cityarea']?>><span><?php echo $cityarea['cityarea'];?></span></option>
                                                             <?php   endforeach                ?>
                                                         </select>           
                                                     </div>
                                                     <div class="col-lg-2">
                                                        <label >Level 1
                                                        </label>                    
                                                        <select  class="form-control input-sm select2" multiple="true" id="drpl1Id" data-placeholder="Choose Item....">

                                                            <?php   foreach( $l1s as $l1):         ?>
                                                             <option value=<?php echo $l1['l1']?>><span><?php echo $l1['name'];?></span></option>
                                                         <?php   endforeach                ?>
                                                     </select>    
                                                 </div>

                                                 <div class="col-lg-2" >
                                                    <label >Level 2
                                                    </label>                    
                                                    <select  class="form-control input-sm select2" multiple="true" id="drpl2Id" data-placeholder="Choose User....">

                                                        <?php   foreach( $l2s as $l2):         ?>
                                                         <option value=<?php echo $l2['l2']?>><span><?php echo $l2['level2_name'];?></span></option>
                                                     <?php   endforeach                ?>  
                                                 </select>   
                                             </div>
                                             <div class="col-lg-2" >
                                                <label >Level 3
                                                </label>                    
                                                <select  class="form-control input-sm select2" multiple="true" id="drpl3Id" data-placeholder="Choose User....">

                                                    <?php   foreach( $l3s as $l3):         ?>
                                                     <option value=<?php echo $l3['l3']?>><span><?php echo $l3['level3_name'];?></span></option>
                                                 <?php   endforeach                ?>  
                                             </select>   
                                         </div>
                                         <div class="col-lg-3" >
                                            <label >Choose User
                                            </label>                    
                                            <select  class="form-control input-sm select2" multiple="true" id="drpuserId" data-placeholder="Choose User....">

                                                <?php   foreach( $userone as $user):         ?>
                                                 <option value=<?php echo $user['uid']?>><span><?php echo $user['uname'];?></span></option>
                                             <?php   endforeach                ?>  
                                         </select>   
                                     </div>
                                 </div>
                             </div>
                         </form>

                     </div>
                 </div>

             </div>
         </div>

     </div>
 </div>
 <!-- End Advanced Panels -->

 <div class="row">
    <div class="col-lg-12">

        <div class="container-fluid grand-debcred-block" style="display:none;">
            <div class="pull-right">
                <ul class="stats">
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big grand-debit" readonly="" >0</span>
                            <span>Grand Debit</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big grand-credit">0</span>
                            <span>Grand credit</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big opening-bal">0</span>
                            <span>Previous Cash</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big closing-bal">0</span>
                            <span>Current Cash</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big purchases-sum">0</span>
                            <span>Purchase</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big purchasereturns-sum">0</span>
                            <span>Purchase Return</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big sales-sum">0</span>
                            <span>Sale</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big cash-sales-sum">0</span>
                            <span>Cash Sale</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big discount-sales-sum">0</span>
                            <span>Sale Discount</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <br>
        <div class="container-fluid grand-debcred-block" style="display:none;">
            <div class="pull-right">
                <ul class="stats">
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big salereturns-sum">0</span>
                            <span>Sale Return</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big payments-sum">0</span>
                            <span>Payment</span>
                        </div>
                    </li>
                    <li class='blue'>
                        <i class="fa fa-money"></i>
                        <div class="details">
                            <span class="big receipts-sum">0</span>
                            <span>Receipt</span>
                        </div>
                    </li>
                                                        <!-- <li class='blue'>
                                                            <i class="fa fa-money"></i>
                                                            <div class="details">
                                                                <span class="big pimports-sum">0</span>
                                                                <span>Purchase Import</span>
                                                            </div>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="container-fluid grand-amount-block">
                                                <div class="pull-right">
                                                    <ul class="stats">
                                                        <li class='blue'>
                                                            <i class="fa fa-money"></i>
                                                            <div class="details">
                                                                <span class="big grand-amount">0</span>
                                                                <span>Grand Total</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <div class="box gradient">
                                                <div class="title">

                                                </div>
                                                <!-- End .title -->
                                                <div class="content top">
                                                    <table id="cpv_datatable_example" class="table table-striped full">
                                                        <thead>
                                                            <tr>
                                                                <th class="no_sort" >Sr#
                                                                </th>
                                                                <th class="no_sort">Date
                                                                </th>
                                                                <th class="no_sort">Voucher #
                                                                </th>
                                                                <th class="no_sort">Account
                                                                </th>
                                                                <th class="no_sort">Remarks
                                                                </th>
                                                                <th class="no_sort">Amount
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="CPVRows" class='parentTableRows'>
                                                        </tbody>
                                                    </table>
                                                    <!-- End row-fluid -->
                                                </div>
                                                <!-- End .content -->
                                            </div>

                                        </div>
                                    </div>

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->



                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>