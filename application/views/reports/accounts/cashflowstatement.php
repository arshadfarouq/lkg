<!-- main content -->
<div id="main_wrapper">
	<div class="page_bar">
		<div class="row">
			<div class="col-md-4">
				<h1 class="page_title"><span class="badge badge-primary badge_style"><i class="fa fa-list"></i></span> Cash Flow Statement</h1>
			</div>
      <div class="col-lg-9">
      </div>
		</div>
	</div>
	<div class="page_content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default" style="margin-top:-35px;">
						<div class="panel-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                    <div class="col-lg-2">
                      <label>From</label>
                      <input class="form-control ts_datepicker" type="text" id="from_date" >
                    </div>
                    <div class="col-lg-2">
                      <label>To</label>
                      <input class="form-control ts_datepicker" type="text" id="to_date">
                    </div>
                    <div class="col-lg-2">
                      <label style="color:#e7f0ef !important;">Account</label>
                      <a class="btn btn-default btnPrint2" style="padding-left: 1px; height: 34px; padding-top: 7px; margin-left: -26px;"><i class="fa fa-show"></i> Show F6</a>
                    </div>
                  </div>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</div>