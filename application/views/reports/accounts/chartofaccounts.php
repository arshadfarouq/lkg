<script id="ledger-level1-template" type="text/x-handlebars-template">
    <tr class='level1head active'>
        <td>{{ACCOUNT_ID}}</td>
        <td>{{L1NAME}}</td>
    </tr>
</script>
<script id="ledger-level2-template" type="text/x-handlebars-template">
    <tr class='level2head success'>
        <td>{{ACCOUNT_ID}}</td>
        <td>{{L2NAME}}</td>
    </tr>
</script>
<script id="ledger-level3-template" type="text/x-handlebars-template">
    <tr class='level3head info'>
        <td>{{ACCOUNT_ID}}</td>
        <td>{{L3NAME}}</td>
    </tr>
</script>
<script id="chartOfAccountRow-template" type="text/x-handlebars-template">
<tr>
     <td>{{ACCOUNT_ID}}</td>
     <td>{{PARTY_NAME}}</td>
</tr>
</script>

<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Chart of Accounts</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="btn-group">
                                  <button type="button" class="btn btn-primary btn-lg " id ="btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                  <button type="button" class="btn btn-primary btn-lg dropdown-toggle1" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">
                                    <li ><a href="#" class="" id="btnPrint" > Print F9</li>
                                  </ul>
                    </div>

                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            

                            <table id="datatable_example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="hidden-phone" data-hide="phone,tablet">Account Id
                                        </th>
                                        <th class="no_sort" data-class="expand">Account Name
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="chartOfAccountRows">
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>