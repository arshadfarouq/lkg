<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>

<style>
.span12 { margin-left: 0 !important; }
</style>
<script id="ledger-level0-template" type="text/x-handlebars-template">
<tr class='level0head'>
<td>{{ACCOUNT_ID}}</td>
<td>{{L0NAME}}</td>
<td class="L0HeadSum" style="text-align: right !important;"></td>
</tr>
</script>

<script id="ledger-level1-template" type="text/x-handlebars-template">
<tr class='level1head'>
<td>{{ACCOUNT_ID}}</td>
<td>{{L1NAME}}</td>
<td class="L1HeadSum" style="text-align: right !important;"></td>
</tr>
</script>
<script id="ledger-finalsum-template" type="text/x-handlebars-template">
<tr class='finalsum never-hide'>
<td style="text-align: right !important;">Total: </td>
<td style="text-align: right !important;"></td>
<td class="netAmount" style="text-align: right !important;"></td>
</tr>
</script>
<script id="ledger-level2-template" type="text/x-handlebars-template">
<tr class='level2head'>
<td>{{ACCOUNT_ID}}</td>
<td>{{L2NAME}}</td>
<td class="L2HeadSum" style="text-align: right !important;"></td>
</tr>
</script>
<script id="ledger-level3-template" type="text/x-handlebars-template">
<tr class='level3head'>
<td>{{ACCOUNT_ID}}</td>
<td>{{L3NAME}}</td>
<td class="L3HeadSum" style="text-align: right !important;"></td>
</tr>
</script>
<script id="ledger-template" type="text/x-handlebars-template">
<tr class='level4row'>
<td>{{ACCOUNT_ID}}</td>
<td>{{PARTY_NAME}}</td>
<td class="text-right amount" style="text-align:right !important;">{{AMOUNT}}</td>
</tr>
</script>
<script id="stock-template" type="text/x-handlebars-template">
<tr>
<th class="no_sort" style="width: 50px;">Sr#</th>
<th class="no_sort" style="width: 50px;">ItemId</th>
<th class="no_sort" style="width: 60px;">Article# </th>
<th class="no_sort" style="width: 900px;">Description </th>
<th class="no_sort" style="width: 50px;">Uom </th>
<th class="no_sort" style="text-align:right; width: 150px;">Qty </th>
<th class="no_sort" style="text-align:right; width: 150px;">Weight </th>
<th class="no_sort" style="text-align:right; width: 150px;">Cost </th>
<th class="no_sort" style="text-align:right; width: 150px;">Value </th>
</tr>
</script> 
<script id="general-grouptotal-template-value" type="text/x-handlebars-template">
<tr class="finalsum">
<td></td>
<td></td>
<td></td>

<td style="text-align:right !important;">{{TOTAL}}</td>
<td></td>
<td style="text-align:right !important;">{{TOTAL_QTY}}</td>
<td style="text-align:right !important;">{{TOTAL_WEIGHT}}</td>
<td></td>
<td style="text-align:right !important;">{{TOTAL_AMOUNT}}</td>
</tr>
</script>

<script id="general-vhead-template-value" type="text/x-handlebars-template">
<tr class="hightlight_tr">
<td></td>
<td></td>
<td></td>
<td>{{GROUP1}}</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</script>
<script id="general-item-template-value" type="text/x-handlebars-template">
<tr>
<td>{{SERIAL}}</td>
<td class="item_id">{{ITEM_ID}}</td>
<td>{{ARTICLE}}</td>
<td>{{DESCRIPTION}}</td>
<td>{{UOM}}</td>
<td class="qty" style="text-align:right !important;">{{QTY}}</td>
<td class="weight" style="text-align:right !important;">{{WEIGHT}}</td>
<td class="rate" style="text-align:right !important;">{{COST}}</td>
<td class="amount" style="text-align:right !important;">{{VALUE}}</td>
</tr>
</script>
<script id="general-head-template-value" type="text/x-handlebars-template">
<tr>
<th class="no_sort" style="width: 50px;">Sr#</th>
<th class="no_sort" style="width: 50px;">ItemId</th>
<th class="no_sort" style="width: 60px;">Article# </th>
<th class="no_sort" style="width: 900px;">Description </th>
<th class="no_sort" style="width: 50px;">Uom </th>
<th class="no_sort" style="text-align:right; width: 150px;">Qty </th>
<th class="no_sort" style="text-align:right; width: 150px;">Weight </th>
<th class="no_sort" style="text-align:right; width: 150px;">Cost </th>
<th class="no_sort" style="text-align:right; width: 150px;">Value </th>
</tr>
</script>

<script id="expense-template" type="text/x-handlebars-template">
  <tr>
  <td class="name" data-pid={{PID}}>{{NAME}}</td>
  <td class="text-right amount" style="text-align:right !important;">{{AMOUNT}}</td>
  </tr>
</script>
<script id="company-capital-template" type="text/x-handlebars-template">
  <tr>
    <td class="name" data-pid={{PID}}>{{NAME}}</td>
    <td class="text-right capital" style="text-align:right !important;">{{BALANCE}}</td>
    <td class="text-right profit" style="text-align:right !important;">{{PROFIT}}</td>
  </tr>
</script>

<script id="partner-capital-template" type="text/x-handlebars-template">
  <tr>
    <td class="name" data-pid={{PID}}>{{NAME}}</td>
    <td class="text-right capital" style="text-align:right !important;">{{BALANCE}}</td>
    <td class="text-right perage" style="text-align:right !important;">{{PERAGE}}</td>
    <td class="text-right profit" style="text-align:right !important;">{{PROFIT}}</td>
  </tr>
</script>


<div id="main_wrapper">
  <div class="container-fluid">   
    <!-- <input type="hidden" name="cid" class="cid" value="<?php echo $this->session->userdata('company_id'); ?>"> -->
    <!-- Take the globally set company id if the user is super admin and normal user id if other -->
    <!-- <input type="hidden" name="cid" class="cid" value="<?php echo (($this->session->userdata('user_type') === 'Super Admin') && ($this->session->userdata('fix_company_id') !== false)) ? $this->session->userdata('fix_company_id') : $this->session->userdata('company_id'); ?>"> -->
    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
    <input type="hidden" id="st_challanid" value="<?php echo $setting_configur[0]['st_challan']; ?>">
    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
    <input type="hidden" name="usertype" id="usertype" value="<?php echo $this->session->userdata('usertype'); ?>">
    <input type="hidden" name="hfCostOfGoodsSold" class="hfCostOfGoodsSold" value="">
    <input type="hidden" name="hfOperatingExpenses" class="hfOperatingExpenses" value="">
    <input type="hidden" name="hfFinanceCost" class="hfFinanceCost" value="">
    <input type="hidden" name="hfNetWPPF" class="hfNetWPPF" value="">
    <input type="hidden" id="voucher_type_hidden">
    <input type="hidden" name="hfNetPFT" class="hfNetPFT" value=""><br> <br>
    <?php if ($this->session->userdata('usertype') === 'Super Admin'): ?>
    <div class="row fixCompanyHide">
      <div class="input-group col-lg-12">
        <span class="input-group-addon fixwid">Chose Unit</span>
        <select name="company_id" id="drpCompanyId">
          <option value=""> All</option>
          <?php foreach ($companies as $company): ?>
          <option value="<?php echo $company['company_id']; ?>" <?php echo ( $company['company_id'] === $this->session->userdata('user_type') ) ? 'selected' : ''; ?>><?php echo $company['company_name'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
<?php endif; ?>
<div class="row">
  <div class="col-lg-12" style="margin-top:-30px;">
    <div class="box paint_hover">
      <div class="title">
        <h3><i class="fa fa-money"></i> Detailed Profit/Loss</h3>
      </div>
      <div class="box box-color box-bordered">
        <div class="box-title">
          <h5 style="color: white;">Select the filters</h5>
        </div>
        <div class="=container-fluid box-content">
          <div class="row dates-row" style="background: rgba(234, 233, 233, 0.7); padding: 20px 0px 20px; text-align: center; margin-bottom: 15px; ">
            <div class="col-lg-2">
              <div class="input-group col-lg-12">
                <span class="input-group-addon">Vr#</span>
                <input type="number" id="Vrnoa" class="form-control input-sm" value="<?php echo $vrnoa;?>"/>
                <input type="number" id="txtVrnoaHidden" class="form-control input-sm hidden" value="<?php echo $vrnoa;?>"/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group col-lg-12">
                <span class="input-group-addon">Date</span>
                <input type="text" id="VrDate" class="form-control ts_datepicker" value="<?php echo date('Y-m-d'); ?>" />
              </div>
            </div>
           
            <div class="col-lg-7">
              <div class="pull-right">
                <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                <a class="btn btn-sm btn-default btnSave" data-insertbtn='<?php echo $vouchers['plsvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['plsvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['plsvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['plsvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
              </div>
            </div>
          </div>
          <div class="row dates-row" style="background: rgba(234, 233, 233, 0.7); padding: 20px 0px 20px; text-align: center; margin-bottom: 15px; ">
            <div class="col-lg-3">
              <div class="input-group col-lg-12">
                <span class="input-group-addon">From</span>
                <input type="text" id="from" class="form-control ts_datepicker" value="<?php echo date('Y-m-d'); ?>" />
                <input type="text" id="fromOther" class="form-control hidden" value="<?php echo date('Y-m-d'); ?>" />
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group col-lg-12">
                <span class="input-group-addon">To</span>
                <input type="text" id="to" class="date form-control ts_datepicker" value="<?php echo date('Y-m-d'); ?>" />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="pull-right">
                <a href="#" class="btn btn-primary show-rept">Show Report</a>
                <a href="#" id="btnReset" class="btn btn-danger reload reset-rept">Reset Filters</a>
                <a href="#" class="btnProfitLossSheet btn btn-success">Profit/Loss Sheet</a>
                <a href="#" id="btnReset" class="btn btn-info print-normal-pls">Print Profit Loss</a>
              </div>
            </div>
          </div>
          <div class="row ra-inputs">
            <div class="col-lg-4">
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Opening Stock</span>
                <input class="form-control" type="text" name="opening_stock" id="inpOpeningStock" readonly="readonly">
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Purchase</span>
                <input class="form-control" type="text" name="purchase" id="inpPurchase" >
                <!-- <input class="form-control ts_datepicker" type="text" id="from_date"> -->
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">-Purchase Return</span>
                <input class="form-control" type="text" name="purchase_return" id="inpPurchaseReturn" >
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">=Net Purchase</span>
                <input class="form-control" type="text" name="net_purchase" id="inpNetPurchase" >
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Gross Profit/Loss</span>
                <input class="form-control" type="text" name="gross_profit_loss" id="inpGrossProfitLoss" >
              </div>
            </div>
            <div class="col-lg-4">
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Sale</span>
                <input class="form-control" type="text" name="sale" id="inpSale" >
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">-Sale Return</span>
                <input class="form-control" type="text" name="sale_return" id="inpSaleReturn" >
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">=Net Sale</span>
                <input class="form-control" type="text" name="net_sale" id="inpNetSale" >
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Closing Stock</span>
                <input class="form-control" type="text" name="closing_stock" id="inpClosingStock" >
              </div>
            </div>
            <div class="col-lg-4">
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Other Income</span>
                <input class="form-control" type="text" name="other_income" id="inpOtherIncome" >
              </div>
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Total Expenses</span>
                <input class="form-control" type="text" name="total_expenses" id="inpTotalExpenses" >
              </div>
            </div>
          </div>
          <div class="row ra-inputs">
            <div class="col-lg-4">
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Total</span>
                <input class="form-control" type="text" name="total_col_1" id="totalCol1" >
              </div>
            </div>
            <div class="col-lg-4">
              <div class="input-group col-lg-12">
                <span class="input-group-addon fixwid">Total</span>
                <input class="form-control" type="text" name="total_col_2" id="totalCol2" >
              </div>
            </div>
            <div class="col-lg-4">
              <div class="input-group col-lg-12">
                <span class="input-group-addon add-on highlight span5">Net Profit/Loss</span>
                <input class="form-control" type="text" name="net_profit_loss" id="netProfitLoss" >
              </div>
            </div>
          </div>
        </div>
      </div><br>
      <div class="content pls">
        <div class="row">
          <div class="box gradient" style="margin: 0px 0px 0px 15px !important;">
            <div class="row">
              <div class="col-lg-12">
                <ul class="nav nav-pills">
                  <li class="active"><a href="#partnerSheet" data-toggle="tab">Partner Profit Detail</a></li>
                  <li class=""><a href="#companySheet" data-toggle="tab">Company Profit Detail</a></li>
                  <li class=""><a href="#expenseSheet" data-toggle="tab">Expense Sheet</a></li>
                  <li class=""><a href="#openingStock" data-toggle="tab">Opening Stock</a></li>
                  <li class=""><a href="#closingStock" data-toggle="tab">Closing Stock</a></li>
                  <li><a href="#balanceSheet" id="bal-sheet" data-toggle="tab">Balance Sheet</a></li>
                </ul>
              </div>
            </div>
            <div class="tab-content">
              <div class="tab-pane active" id="partnerSheet">
                <div class="row hidden">
                  <div class="col-lg-12">
                    <a href="#" class="btn btn-primary btn-print-expense-sheet">Print</a>
                  </div>
                </div><br>
                <table class="partnerTable table table-striped full table-bordered">
                  <thead>
                    <tr>
                      <!-- <th class="no_sort">Sr#</th> -->
                      <th style="width:70%;">Account</th>
                      <th style="width:10%;">Capital</th>
                      <th style="width:10%;">PercentAge</th>
                      <th style="width:10%;">Profit</th>
                      
                    </tr>
                  </thead>
                  <tbody id="partnerRows">

                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="companySheet">
                <div class="row hidden">
                  <div class="col-lg-12">
                    <a href="#" class="btn btn-primary btn-print-expense-sheet">Print</a>
                  </div>
                </div><br>
                <table class="companyTable table table-striped full table-bordered">
                  <thead>
                    <tr>
                      <!-- <th class="no_sort">Sr#</th> -->
                      <th style="width:70%;">Account</th>
                      <th style="width:10%;">Capital</th>
                      <th style="width:10%;">Profit</th>
                      
                    </tr>
                  </thead>
                  <tbody id="companyRows">

                  </tbody>
                </table>
              </div>

              <div class="tab-pane" id="expenseSheet">
                <div class="row">
                  <div class="col-lg-12">
                    <a href="#" class="btn btn-primary btn-print-expense-sheet">Print</a>
                  </div>
                </div><br>
                <table class="expenseTable table table-striped full table-bordered">
                  <thead>
                    <tr>
                      <!-- <th class="no_sort">Sr#</th> -->
                      <th style="width:80%;">Account</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody id="expenseRows">

                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="openingStock">
                <div class="row">
                  <div class="col-lg-12">
                    <a href="#" class="btn btn-primary btn-print-opening-stock">Print</a>
                  </div>
                </div><br>
                <table id="datatable_example" class="table table-striped full table-bordered openingStockTable">
                  <thead class='dthead'>        

                  </thead>
                  <tbody id="openingStockRows" class="report-rows openingStockRows">

                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="closingStock">
                <div class="row">
                  <div class="col-lg-12">
                    <a href="#" class="btn btn-primary btn-print-closing-stock">Print</a>
                  </div>
                </div><br>
                <table id="datatable_example" class="table table-striped full table-bordered closingStockTable">
                  <thead class='dthead'>

                  </thead>
                  <tbody id="closingStockRows" class="report-rows closingStockRows">

                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="balanceSheet">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="input-group input-append ">
                        <span class="input-group-addon fixwid">View Detail</span>
                        <select name="view_type" class="form-control" id="drpViewType">
                          <option value="detail">Detailed</option>
                          <option value="summary">Summary</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-9">
                      <a href="#" class="btn btn-info btn-level0-view">Level-0</a>
                      <a href="#" class="btn btn-info btn-level1-view">Level-1</a>
                      <a href="#" class="btn btn-warning btn-level2-view">Level-2</a>
                      <a href="#" class="btn btn-primary btn-level3-view">Level-3</a>
                      <a href="#" class="btn btn-info btn-detailed-view">Detailed</a>
                      <a href="#" class="btn btn-primary btnPrintBalSheet">Print Balance Sheet</a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <h4>Assets : <span class="upperasstotal"></span></h4>
                      <table id="datatable_Vouchers" class="table full table-bordered table-striped table-hover">
                        <thead>
                          <tr class="never-hide">
                            <th class="no_sort" style="width:90px;">Account Id</th>
                            <th class="no_sort">Account Name</th>
                            <th style="width:100px;text-align:right;">Amount</th>
                          </tr>
                        </thead>
                        <tbody class="ASSETSRows">

                        </tbody>
                        <tfoot id="assetsFoot">
                          <tr class='stockhead never-hide'>
                            <td>00</td>
                            <td>Closing Stock</td>
                            <td class="closingStockBalSheet" style="text-align: right !important;"></td>
                          </tr>
                          <tr class="never-hide">
                            <td class="text-right">Net Total</td>
                            <td></td>
                            <td class="text-right netAssetsTotal"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                    <div class="col-lg-6">
                      <h4>Liabilities : <span class="upperliabtotal"></span></h4>
                      <table id="datatable_Vouchers" class="table full table-bordered table-striped table-hover">
                        <thead>
                          <tr class="never-hide">
                            <th class="no_sort" style="width:90px;">Account Id</th>
                            <th class="no_sort">Account Name</th>
                            <th style="width:100px;text-align:right;">Amount</th>
                          </tr>
                        </thead>
                        <tbody class="LIABILITIESRows">

                        </tbody>
                        <tfoot id="liabilitiesFoot">
                          <tr class='stockhead never-hide'>
                            <td>00</td>
                            <td>Profit/Loss</td>
                            <td class="plsBalSheet" style="text-align:right !important;"></td>
                          </tr>
                          <tr class="never-hide">
                            <td class="text-right">Net Total</td>
                            <td></td>
                            <td class="text-right netLiabilityTotal"></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><br><br><br><br>
      </div>
    </div>
  </div>
</div>
</div>  
</div>  