<script id="voucher-phead-template" type="text/x-handlebars-template">

    <tr class="level3head hightlight_tr odd">
        <td></td>
        <td></td>
        <td>{{account}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</script>
<script id="voucher-total-template" type="text/x-handlebars-template">
    <tr class="group-total-row tfoot_tbl">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Total Balance</td>
        <td style="text-align: right !important;">{{net_balance}}</td>
    </tr>
</script>

<script id="voucher-item-template" type="text/x-handlebars-template">

    <tr>
        <td>{{dcno}}</td>
        <td>{{date}}</td>
        <td>{{account}}</td>
        <td>{{days_passed}}</td>
        <td>{{due_date}}</td>
        <td style="text-align: right !important;">{{invoice_amount}}</td>
        <td style="text-align: right !important;">{{paid}}</td>
        <td style="text-align: right !important;">{{balance}}</td>
    </tr>
</script>



<script id="voucher-item-template2222" type="text/x-handlebars-template">
	<tr>
		<td>{{dcno}}</td>
		<td>{{vrdate}}</td>
		<td>{{account}}</td>
		<td>{{days_passed}}</td>
		<td>{{due_date}}</td>
		<td style="text-align: right !important;">{{invoice_amount}}</td>
		<td style="text-align: right !important;">{{paid}}</td>
		<td style="text-align: right !important;">{{balance}}</td>
	</tr>
</script>




<div id="main_wrapper">

    <div class="page_bar" style="margin-top: 22px;">
        <div class="row-fluid">
            <div class="col-md-12">
                <h1 class="page_title" style="margin:-20px 0px 0px 0px;"><i class="fa fa-list"></i> Invoice Aging Report</h1>

            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="panel panel-default">
                                <div class="panel-body" style="margin-top: -20px;">

                                    <div class="row">
                                      <div class="col-lg-12">
                                           <div class="form-group">
                                            <div class="row">
                                              <div class="col-lg-3">
                                                <label>From</label>
                                                <span class="add-on"><i class="icon-calendar"></i></span>

                                                <input type="date" id="from_date" placeholder="Starting Date" required class="form-control" value="<?php echo date('Y-m-d'); ?>">

                                                

                                              </div>
                                              <div class="col-lg-3">
                                                <label>To</label>
                                                <span class="add-on"><i class="icon-calendar"></i></span>

                                                <input type="date" id="to_date" placeholder="End date" class="form-control" value="<?php echo date('Y-m-d'); ?>">

                                                

                                              </div>
                                              <div class="col-lg-2">
                                                <label>Report Type</label>
                                                <select name="drpReptType" id="reptType" class="form-control select2">
                                                  <option value="payables">Payables</option>
                                                  <option value="receiveables">Receiveables</option>
                                                </select>
                                              </div>
                                              <div class="col-lg-3">
                                                <label>Account</label>
                                                <select name="drpAccount" id="drpAccount" class="form-control select2">

                                                  <option value="">Chose Account</option>

                                                  <?php foreach ($parties as $party): ?>
                                                  <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                                                  <?php endforeach ?>
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                    
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-12">
                                    <div class="pull-right">
                                      
                                        
                                            <button id="btnShow" type="button" class="btn btn-primary btn-sm show-report">
                                                <i class="fa fa-list"></i> Show</button>
                                            <button id="btnReset" type="reset" class="btn btn-primary btn-sm">
                                                <i class="fa fa-refresh"></i> Reset</button>
                                            <button id="btnPrint" class="btn btn-primary btn-sm">
                                                <i class="fa fa-print"></i> Print</button>
                                            
                                            
                                            <a id="btnExcel" class="btn btn-secondary btn-success btnExcel" data-toggle="modal" data-target="" rel="tooltip"><i class="fa fa-list"></i> Excel</a>
                                            <a id="btnEmail" class="btn btn-secondary btn-success" data-toggle="modal" data-target="#myModal" rel="tooltip"
                                                                    data-placement="top" data-original-title="Add Email"><i class="fa fa-envelope"></i> Email</a>
                                    
                                    
                                    </div>
                                    </div>
                                    </div>

                                    

                                    <div class="row">
        <div class="col-lg-12">
            
            <table id="datatable_example" class="table full table-bordered table-striped table-hover ">
                <thead class="dthead">
                    <tr >
                      <th>Vr#</th>
                      <th>Date</th>
                      <th>Account</th>
                      <th>Days Passed</th>
                      <th>Aging Date</th>
                      <th>Invoice Amount</th>
                      <th>Paid Amount</th>
                      <th>Balance</th>
                    </tr>
                </thead>
                <tbody class="saleRows">
                    
                </tbody>
            </table>
            <div class="container-fluid">
    
    <input type="hidden" name="cid" class="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
    <input type="hidden" name="company_name" id="company_name" value="<?php echo $this->session->userdata('company_name'); ?>">

    <?php if ($this->session->userdata('user_type') === 'superadmin'): ?>
        <div class="row">
          <label>Chose Company</label>
          <select name="company_id" id="drpCompanyId" class="form-control">
                    <?php foreach ($companies as $company): ?>
                        <option value="<?php echo $company['company_id']; ?>" <?php echo ( $company['company_id'] === $this->session->userdata('user_type') ) ? 'selected' : ''; ?>><?php echo $company['company_name'] ?></option>
                    <?php endforeach; ?>
                </select>
            
        </div>
    <?php endif; ?>
    

</div>

        </div>
    </div>

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->

                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background:#368ee0;color:white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-envelope"></i> Email</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
      <div class="row">
      <div class="col-lg-10">
      <label>Enter email address here:</label>
      <input id="txtAddEmail" type="text" class="form-control">

      </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button id="btnSendEmail" class="btn btn-primary">
            <i class="fa fa-share"></i> Send</button>
           <button class="btn" data-dismiss="modal">
            <i class="fa fa-times-circle"></i> Close</button>
        
        </div>
        
      </div>
    </div>
  </div>
</div>