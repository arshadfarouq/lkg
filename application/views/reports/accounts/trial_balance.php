<script id="ledger-level1-template" type="text/x-handlebars-template">
    <tr class='level1head info'>
        <td>{{ACCOUNT_ID}}</td>
        <td>{{L1NAME}}</td>
        <td style="text-align: right !important;">{{L1DebSUM}}</td>
        <td style="text-align: right !important;">{{L1CredSUM}}</td>
    </tr>
</script>
<script id="ledger-finalsum-template" type="text/x-handlebars-template">
    <tr class='finalsum'>
        <td></td>
        <td style="text-align: right !important;">Total Amount: </td>
        <td style="text-align: right !important;">{{totalDeb}}</td>
        <td style="text-align: right !important;">{{totalCred}}</td>
    </tr>
</script>
<script id="ledger-level2-template" type="text/x-handlebars-template">
    <tr class='level2head success'>
        <td>{{ACCOUNT_ID}}</td>
        <td>{{L2NAME}}</td>
        <td style="text-align: right !important;">{{L2DebSUM}}</td>
        <td style="text-align: right !important;">{{L2CredSUM}}</td>
    </tr>
</script>
<script id="ledger-level3-template" type="text/x-handlebars-template">
    <tr class='level3head active'>
        <td>{{ACCOUNT_ID}}</td>
        <td>{{L3NAME}}</td>
        <td style="text-align: right !important;">{{L3DebSUM}}</td>
        <td style="text-align: right !important;">{{L3CredSUM}}</td>
    </tr>
</script>
<script id="ledger-template" type="text/x-handlebars-template">
  <tr>
     <td>{{ACCOUNT_ID}}</td>
     <td>{{PARTY_NAME}}</td>
     <td class="text-right" style="text-align:right !important;">{{DEBIT}}</td>
     <td class="text-right" style="text-align:right !important;">{{CREDIT}}</td>
  </tr>
</script>

<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Trial Balance Report</h1>
            </div>
        </div>

    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon txt-addon">From</span>
                                        <input class="form-control ts_datepicker" type="text" id="from_date">
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon txt-addon">To</span>
                                        <input class="form-control ts_datepicker" type="text" id="to_date">
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" name="brid" id="brid" value="<?php echo $this->session->userdata('brid'); ?>">
                            <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-right">
                                       <!--  <a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print 2 Col F9</a>
                                        <a class="btn btn-default btnPrint6"><i class="fa fa-print"></i> Print 6 Col F8</a> -->
                                        <div class="btn-group">
                                              <button type="button" class="btn btn-primary btn-lg btnPrint" ><i class="fa fa-print"></i>Print F9</button>
                                              <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li ><a href="#" class="btnPrint">2 Column F9</li>
                                                <li ><a href="#" class="btnPrint6">6 Column F8</li>
                                              </ul>
                                        </div>

                                        <a class="btn btn-default btnSearch"><i class="fa fa-search"></i> Show F6</a>
                                        <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-3">
                                        <label>Level1 </label>
                                        <select id="drpLevel1" data-placeholder="Choose Level3....." class="form-control select2" name="level3">
                                            <option value="-1" disabled selected>Chose Level 1</option>
                                            <?php foreach ($level1s as $level3): ?>
                                                <option  value="<?php echo $level3['l1']; ?>"><?php echo $level3['name']; ?></option>
                                            <?php endforeach; ?>                                                
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Level2 </label>
                                        <select id="drpLevel2" data-placeholder="Choose Level2....." class="form-control select2" name="level3">
                                            <option value="-1" disabled selected>Chose Level 2</option>
                                            <?php foreach ($level2s as $level3): ?>
                                                <option data-l1="<?php echo $level3['l1']; ?>" value="<?php echo $level3['l2']; ?>"><?php echo $level3['name']; ?></option>
                                            <?php endforeach; ?>                                                
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <label>Level3 </label>
                                        <select id="drpLevel3" data-placeholder="Choose Level3....." class="form-control select2" name="level3">
                                            <option value="-1" disabled selected>Chose Level 3</option>
                                            <?php foreach ($level3s as $level3): ?>
                                                <option data-l1name="<?php echo $level3['l1name']; ?>" data-l1="<?php echo $level3['l1']; ?>" data-l2="<?php echo $level3['l2']; ?>" data-l2name="<?php echo $level3['l2name']; ?>" value="<?php echo $level3['l3']; ?>"><?php echo $level3['name']; ?></option>
                                            <?php endforeach; ?>                                                
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        
                            <table id="datatable_Vouchers" class="table full table-bordered table-striped table-hover">
                                <thead>
                                    <th class="no_sort">Account Id</th>
                                    <th class="no_sort">Account Title</th>
                                    <th class="no_sort">Debit</th>
                                    <th class="no_sort">Credit</th>
                                </thead>
                                <tbody class="trialBalRows">                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>