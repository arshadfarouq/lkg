<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Overall Semester Result</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">								
								<div class="col-lg-3">
									<label for="">Class</label>
									<select class="form-control" id="class_dropdown">
	                                    <option value="" disabled="" selected="">Choose class</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Section</label>
									<select class="form-control" id="section_dropdown">
	                                    <option value="" disabled="" selected="">Choose section</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Session</label>
									<select class="form-control" id="session_dropdown">
	                                    <option value="" disabled="" selected="">Choose session</option>		                                    
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Std. Id</label>
									<select class="form-control" id="stdid_dropdown">
	                                    <option value="" disabled="" selected="">Choose student Id</option>		                                    
	                                </select>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="pull-right">
										<a href='' class="btn btn-primary btnSearch">
	              							<i class="fa fa-search"></i>
	            						Search</a>
	            						<a href="" class="btn btn-warning btnReset">
					                      	<i class="fa fa-refresh"></i>
					                    Reset</a>
					                    <button type="button" class="btn btn-info  btnPrint">Print</button>
				                    </div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
	
			<div class="row">				
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover center" id="result-table">
								<thead>
									<tr>
										<th rowspan='2' class='txtcenter'>Subject</th>
										<th colspan='2' class='txtcenter'>Sem-1</th>
										<th colspan='2' class='txtcenter'>Sem-2</th>
										<th colspan='2' class='txtcenter'>Sem-3</th>
										<th colspan='2' class='txtcenter'>Annual</th>
									</tr>
									<tr class='txtcenter'>
										<th class='txtcenter'>Ob. Marks</th>
										<th class='txtcenter'>Total Marks</th>

										<th class='txtcenter'>Ob. Marks</th>
										<th class='txtcenter'>Total Marks</th>

										<th class='txtcenter'>Ob. Marks</th>
										<th class='txtcenter'>Total Marks</th>

										<th class='txtcenter'>Ob. Marks</th>
										<th class='txtcenter'>Total Marks</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>