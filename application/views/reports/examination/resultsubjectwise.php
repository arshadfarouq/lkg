<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Result (Subject Wise)</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			<div class="col-lg-12">
				<button type="button" class="btn btn-info  btnPrint pull-right">Print</button>
			</div>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						
							<table class="table table-striped table-hover" id="result-table">
								<thead>
									<tr>
										<th>Dcno</th>
										<th>Semester</th>
										<th>Name</th>
										<th>Total Marks</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>