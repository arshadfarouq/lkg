<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Student Result</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-3">
									<label for="">Class</label>
									<select class="form-control" id="class_dropdown">
	                                    <option value="" disabled="" selected="">Choose class</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Section</label>
									<select class="form-control" id="section_dropdown">
	                                    <option value="" disabled="" selected="">Choose section</option>
	                                </select>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3">
	                            	<label for="">Term</label>
									<select class="form-control" id="term_dropdown">
	                                    <option value="" disabled="" selected="">Choose term</option>
	                                    <option value="sem-1">Sem-1</option>
	                                    <option value="sem-2">Sem-2</option>
	                                    <option value="sem-3">Sem-3</option>
	                                    <option value="annual">Annual</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Session</label>
									<select class="form-control" id="session_dropdown">
	                                    <option value="" disabled="" selected="">Choose session</option>		                                    
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Std. Id</label>
									<select class="form-control" id="stdid_dropdown">
	                                    <option value="" disabled="" selected="">Choose student Id</option>		                                    
	                                </select>
								</div>
								<div class="col-lg-3" style='margin-top: 25px;'>
									<div class="pull-right">
										<a href='' class="btn btn-primary btnSearch">
	              							<i class="fa fa-search"></i>
	            						Search</a>
	            						<a href="" class="btn btn-warning btnReset">
					                      	<i class="fa fa-refresh"></i>
					                    Reset</a>
					                    <button type="button" class="btn btn-info  btnPrint">Print</button>

				                    </div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
	
			<div class="row">				
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover center" id="result-table">
								<thead>
									<tr class='txtcenter'>
										<th class='txtcenter'>Sr#</th>
										<th class='txtcenter'>Subject</th>
										<th class='txtcenter'>Total Marks</th>
										<th class='txtcenter'>Obtained Marks</th>
										<th class='txtcenter'>Percentage</th>
										<th class='txtcenter'>Remarks</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>