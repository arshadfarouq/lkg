<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Student Result (Teacher Wise)</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			<div class="col-lg-12">
				<button type="button" class="btn btn-info pull-right btn-md btnPrint">Print</button>
			</div>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						
						<table class="table table-striped table-hover" id="result-table">
							<thead>
								<tr>
									<th>Date</th>
									<th>Name</th>
									<th>Term</th>
									<th>Obtained Marks</th>
									<th>Total Marks</th>
									<th>Percentage</th>
									<th>Remarks</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>