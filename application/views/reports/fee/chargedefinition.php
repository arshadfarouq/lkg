<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Charge Definition</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			<div class="col-lg-12">
				<button type="button" class="btn btn-info pull-right btn-lg btnPrint">Print</button>

			</div>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						
							<table class="table table-striped table-hover" id="chargedef-table">
								<thead>
									<tr>
										<th>Chid</th>
										<th>Description</th>
										<th>Charges</th>
										<th>Affected Account</th>
									</tr>
								</thead>
								<tbody>
									<script id="chargedef" type="text/x-handlebars-template">
										{{#charges}}
										<tr>
											<td>{{chid}}</td>
											<td>{{description}}</td>
											<td>{{charges}}</td>
											<td>{{affected_account}}</td>
										</tr>
										{{/charges}}
									</script>
								</tbody>
							</table>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>