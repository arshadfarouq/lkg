<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Monthly Fee Assign Charges Wise Report</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon">From</span>
                                    <input class="form-control ts_datepicker datepicker" type="text" id="from_date">
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon">To</span>
                                    <input class="form-control ts_datepicker datepicker" type="text" id="to_date">
                                </div>
                            </div>

                            <div class="col-lg-6">
                            	<div class="pull-right">
	                            	<a href='' class="btn btn-default btnSearch">
	          							<i class="fa fa-search"></i>
	        						Search</a>
	        						<a href="" class="btn btn-default btnReset">
				                      	<i class="fa fa-refresh"></i>
				                    Reset</a>
				                    <button type="button" class="btn btn-info  btnPrint">Print</button>
			                    </div>
                            </div>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">							
							<table class="table table-hover table-striped" id="fee-table">
								<thead>
									<tr>
										<th>Description</th>										
										<th>Charges</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>