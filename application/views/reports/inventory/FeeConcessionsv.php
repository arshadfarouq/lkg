<script id="pr-head-template" type="text/x-handlebars-template">

  <tr>
        <th>Sr#</th>
        <th class="no_sort">Date</th>
        <th class="no_sort" style="width:50px;">Stid</th>

        <th class="no_sort">Student Name</th>
        <th class="no_sort">Class</th>
        <th class="no_sort">Section</th>
        <th class="no_sort">Fee Category</th>
        <th class="no_sort  text-right">Amount</th>
        <th class="no_sort  text-right">Concession</th>

    </tr>
</script>

<script id="payment-dhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
    <td></td>
    <td ></td>
    <td></td>
    <td>{{GROUP11}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    
</tr>
</script>

<script id="pr-row-template" type="text/x-handlebars-template">
  <tr>
      <td>{{SERIAL}}</td> 
      <td class="no_sort tblAddress">{{DATE}}</td>
      <td class="no_sort tblSerial"><a target="_blank" href={{HREFF}}>{{ST_ID}}</a></td>
      
      <td class="no_sort tblAddress">{{NAME}}</td>
      <td class="no_sort tblAddress">{{CLASS}}</td>
      <td class="no_sort tblAddress">{{SECTION}}</td>
      <td class="no_sort tblEmail">{{CATEGORY}}</td>
      <td class="no_sort tblMobile text-right">{{AMOUNT}}</td>
      <td class="no_sort tblPhone text-right">{{CONCESSION}}</td>

  </tr>
</script>
<script id="pr-sum-template" type="text/x-handlebars-template">
  <tr class="finalsum">
    <td></td> 
    <td class="no_sort tblSerial"></td>
    <td class="no_sort tblStudent"></td>
    <td class="no_sort tblMobile"></td>
    <td class="no_sort tblMobile"></td>
    <td class="no_sort tblMobile"></td>

    <td class="no_sort tblEmail"><b>{{TOTAL_HEAD}}</b></td>
    
    
    <td class="no_sort tblMobile text-right"><b>{{AMOUNT}}</b></td>
    <td class="no_sort tblMobile text-right"><b>{{CONCESSION}}</b></td> 
    

</tr>
</script>


<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Fee Concession Report</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">


                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <input class="form-control ts_datepicker" type="text" id="from">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <input class="form-control ts_datepicker" type="text" id="to">
                                            </div>
                                        </div>
                                    </div>

                                    <legend style='margin-top: 30px;'>Group By</legend>
                                    <div class="row">
                                        <div class="col-lg-1">
                                            <label for="conid" class="radio cpvRadio">
                                                <input type="radio" id='conid' name="grouping" value="m.conid" checked="checked" />
                                                Voucher
                                            </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="conid" class="radio cpvRadio">
                                                <input type="radio" id='conid' name="grouping" value="stu.name"  />
                                                Student Name 
                                            </label>
                                        </div>

                                        <div class="col-lg-1">
                                            <label for="section" class="radio crvRadio">
                                                <input type="radio" id='section' name="grouping" value="date_format(m.date,'%d/%m/%y')" />
                                                Date 
                                            </label>
                                        </div>
                                        <div class="col-lg-1">
                                            <label for="roll" class="radio crvRadio">
                                                <input type="radio" id='roll' name="grouping" value="year(m.date)" />
                                                Year 
                                            </label>
                                        </div> 

                                        <div class="col-lg-1">
                                            <label for="class" class="radio crvRadio">
                                                <input type="radio" id='class' name="grouping" value="date_format(m.date,'%b')" />
                                                Month 
                                            </label>
                                        </div>

                                        <div class="col-lg-1">
                                            <label for="branch" class="radio crvRadio">
                                                <input type="radio" id='branch' name="grouping" value="user.uname" />
                                                User 
                                            </label>
                                        </div>
                                        <div class="col-lg-1">
                                            <label for="name" class="radio crvRadio">
                                                <input type="radio" id='name' name="grouping" value="ch.description" />
                                                Charges 
                                            </label>
                                        </div> 

                                        <div class="col-lg-1">
                                            <label for="Gender" class="radio crvRadio">
                                                <input type="radio" id='Gender' name="grouping" value="stu.gender" />
                                                Gender 
                                            </label>
                                        </div> 
                                        <div class="col-lg-2">
                                            <label for="birthdate" class="radio crvRadio">
                                                <input type="radio" id='birthdate' name="grouping" value="stu.focc" />
                                                Father Occuption 
                                            </label>
                                        </div>    

                                    </div>


                                    <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="" class="btn btn-primary show-rept">Show Report</a>
                                            <a href="" class="btn btn-danger reset-rept">Reset Filters</a>
                                            <a href="" class="printCpvCrvBtn btn btn-primary">Print Report</a>
                                            <a href="" class="printPayRcvBtn btn btn-primary" style="display:none;">Print Report</a>
                                            <a href="" class="printDayBook btn btn-primary" style="display:none;">Print Report</a>
                                        </div>
                                    </div>

                                    <div class="row" style="display:none;">
                                        <div class="col-lg-12">

                                            <div class="container-fluid grand-debcred-block" style="display:none;">
                                                <div class="pull-right">
                                                    <ul class="stats">
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big grand-debit">0</span>
                                                                <span>Grand Debit</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big grand-credit">0</span>
                                                                <span>Grand credit</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big opening-bal">0</span>
                                                                <span>Previous Cash</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big closing-bal">0</span>
                                                                <span>Current Cash</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big purchases-sum">0</span>
                                                                <span>Purchase</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big purchasereturns-sum">0</span>
                                                                <span>Purchase Return</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big sales-sum">0</span>
                                                                <span>Sale</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="container-fluid grand-debcred-block" style="display:none;">
                                                <div class="pull-right">
                                                    <ul class="stats">
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big salereturns-sum">0</span>
                                                                <span>Sale Return</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big payments-sum">0</span>
                                                                <span>Payment</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big receipts-sum">0</span>
                                                                <span>Receipt</span>
                                                            </div>
                                                        </li>
                                                        <li class='blue'>
                                                            <i class="icon-money"></i>
                                                            <div class="details">
                                                                <span class="big pimports-sum">0</span>
                                                                <span>Purchase Import</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-9" style="display:none;">
                                            <div class="container-fluid grand-amount-block">
                                                <div class="pull-right">
                                                    <ul class="stats">
                                                        <li class='blue'>
                                                            <i class="fa fa-money"></i>
                                                            <div class="details">
                                                                <span class="big grand-amount">0</span>
                                                                <span>Grand Total</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                          <div class="row">
                                            <div class="col-lg-12">
                                              <button type="button" class="btn btnAdvaced">Advanced</button>
                                          </div>
                                      </div>
                                      <div class="panel-group panel-group1 panelDisplay" id="accordion" role="tablist" aria-multiselectable="true">

                                        <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                              <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo">
                                                General Info
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapsetwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                          <form class="form-group">
                                            <div class="row">
                                              <div class="col-lg-12">

                                               <div class="col-lg-2" >
                                                <label >Section
                                                </label>        
                                                <select  class="form-control input-sm select2 " multiple="true" id="drpSectionID" data-placeholder="Choose Section....">

                                                   <?php   foreach( $section as $project):       ?>
                                                     <option value=<?php echo $project['srno']?>><span><?php echo $project['name'];?></span></option>
                                                 <?php   endforeach  ?>
                                             </select>
                                         </div>
                                         <div class="col-lg-2">
                                          <label >Categeory
                                          </label>                    
                                          <select  class="form-control input-sm select2" multiple="true" id="drpCatogeoryid"    data-placeholder="Choose Category....">

                                           <?php   foreach( $categories as $categorie):         ?>
                                             <option value=<?php echo $categorie['srno']?>><span><?php echo $categorie['name'];?></span></option>
                                         <?php   endforeach                ?>
                                     </select>           
                                 </div>
                                 <div class="col-lg-2">
                                    <label >Class
                                    </label>                    
                                    <select  class="form-control input-sm select2" multiple="true" id="drpClass" data-placeholder="Choose Sub Class....">

                                       <?php   foreach( $classes as $sector):         ?>
                                         <option value=<?php echo $sector['srno']?>><span><?php echo $sector['name'];?></span></option>
                                     <?php   endforeach                ?>
                                 </select>    
                             </div>


                             <div class="col-lg-2" >
                              <label >Branch
                              </label>                    
                              <select  class="form-control input-sm select2" multiple="true" id="drpBranch" data-placeholder="Choose Branch....">

                                <?php   foreach( $branch as $sta):         ?>
                                   <option value=<?php echo $sta['brid']?>><span><?php echo $sta['name'];?></span></option>
                               <?php   endforeach                ?>  
                           </select>   
                       </div>
                       <div class="col-lg-3" >
                          <label >Choose User
                          </label>                    
                          <select  class="form-control input-sm select2" multiple="true" id="drpuserId" data-placeholder="Choose User....">

                             <?php   foreach( $userone as $user):         ?>
                               <option value=<?php echo $user['uid']?>><span><?php echo $user['uname'];?></span></option>
                           <?php   endforeach                ?>  
                       </select>   
                   </div>
               </div>
           </div>
       </form>

   </div>
</div>

</div>

<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="headingOne">
    <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          Student
      </a>
  </h4>
</div>
<div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
    <div class="panel-body">
      <form class="form-group">
        <div class="row">
          <div class="col-lg-12">


            <div class="col-lg-3" >
                <label for="">Student Name <img id="imgStudentLoader" class="hide" src="<?php echo base_url('assets/img/loader.gif'); ?>"></label>

                <div class="input-group" >
                    <input type="text" class="form-control" id="txtStudentId" tabindex="2">
                    <input id="hfStudentId" type="hidden" value="" />
                    <input id="hfStudentBalance" type="hidden" value="" />
                    <input id="hfStudentCity" type="hidden" value="" />
                    <input id="hfStudentAddress" type="hidden" value="" />
                    <input id="hfStudentCityArea" type="hidden" value="" />
                    <input id="hfStudentMobile" type="hidden" value="" />
                    <input id="hfStudentUname" type="hidden" value="" />
                    <input id="hfStudentLimit" type="hidden" value="" />
                    <input id="hfStudentName" type="hidden" value="" />
                    <input id="txtHiddenEditQty" type="hidden" value="" />
                    <input id="txtHiddenEditRow" type="hidden" value="" />
                </div>
            </div>
            <div class="col-lg-2" >
                <label >Gender
                </label>                    
                <select  class="form-control input-sm select2" multiple="true" id="drpGender" data-placeholder="Choose Color....">

                  <?php   foreach( $genders as $Gender):         ?>
                     <option value=<?php echo $Gender['gender']?>><span><?php echo $Gender['gender'];?></span></option>
                 <?php   endforeach                ?>  
             </select>   
         </div>
         <div class="col-lg-2" >
          <label >Father Occuption
          </label>                    
          <select  class="form-control input-sm select2" multiple="true" id="drpOccuption" data-placeholder="Choose Father Occuption....">

            <?php   foreach( $occuption as $focc):         ?>
               <option value=<?php echo $focc['focc']?>><span><?php echo $focc['focc'];?></span></option>
           <?php   endforeach                ?>  
       </select>   
   </div>

</div>
</div>
</form>
<div class="row">

  <button class="btn btn-success col-lg-2 col-lg-offset-10" id="reset_criteria"></button>

</div>

</div>
</div>
</div>


</div>

</div>
</div>


<div class="row">
    <div class="col-lg-12">

        <div class="box gradient">
            <div class="title">

            </div>
            <!-- End .title -->
            <div class="content top">
                <table id="cpv_datatable_example" class="table table-striped full">
                    <thead>
                        <tr>
                            <th class="no_sort">Sr#
                            </th>
                            <th class="no_sort">Name
                            </th>
                            <th class="no_sort">Father Name
                            </th>
                            <th class="no_sort">Gender
                            </th>
                            <th class="no_sort">Adress
                            </th>
                            <th class="no_sort">Mobile
                            </th>
                            <th class="no_sort">Roll#
                            </th>
                            <th class="no_sort">Fee Type
                            </th>

                        </tr>
                    </thead>
                    <tbody id="COIRows" class='parentTableRows'>
                    </tbody>
                </table>
                <!-- End row-fluid -->
            </div>
            <!-- End .content -->
        </div>

    </div>
</div>

</div>  <!-- end of panel-body -->
</div>  <!-- end of panel -->



</div>  <!-- end of col -->
</div>  <!-- end of row -->

</div>  <!-- end of level 1-->
</div>
</div>
</div>
</div>