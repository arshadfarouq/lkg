<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Admission Withdrawl Report</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
				<input type="hidden" class='address' value='<?php echo $this->session->userdata('name'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="panel panel-default">
			<button type="button" class="btn btn-info  btnPrint pull-right">Print</button>
		</div>
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							
							<table class="table table-striped table-hover" id="students-table">
								<thead>
									<tr>
										<th>Admi Date</th>
										<th>Adm#</th>
										<th>Name</th>
										<th>Dob</th>
										<th>Father Name</th>
										<th>Father Occu</th>
										<th>Address</th>
										<th>Contact</th>
										<!-- <th>Branch</th> -->
										<th>Class Admi</th>
										<th>Class Left</th>
										<th>Date</th>
										<th>Dues</th>
										<th>Remarks</th>
									</tr>
								</thead>
								<tbody>										
									<?php $count = 1; foreach ($students as $student): ?>
									<tr>
										<td><?php echo $student['ADDDATE']; ?></td>
										<td><?php echo $student['stdid']; ?></td>
										<td><?php echo $student['student_name']; ?></td>
										<td><?php echo $student['birthdate']; ?></td>
										<td><?php echo $student['fname']; ?></td>
										<td><?php echo $student['focc']; ?></td>
										<td><?php echo $student['address']; ?></td>
										<td><?php echo $student['mobile']; ?></td>
										<!-- <td><?php echo $student['branch_name']; ?></td> -->
										<td><?php echo $student['class_name']; ?></td>
										<td><?php echo $student['class_left']; ?></td>
										<td><?php echo $student['struckoff']; ?></td>
										<td><?php echo $student['arears']; ?></td>
										<td><?php echo $student['remarks']; ?></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	// $(".btnPrint").on('click', function () {
	// 	var etype = 'admissionWithdrawlReport';
		
	// 	var url = base_url + 'index.php/doc/admissionWithdrawlReport_pdf/' +etype ;
	// 			// alert(url);
	// 			window.open(url);
	// 		});
		</script>