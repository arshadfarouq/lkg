<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Student Status Report</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3">
									<label for="">Class</label>
									<select class="form-control" id="class_dropdown">
	                                    <option value="" disabled="" selected="">Choose class</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Section</label>
									<select class="form-control" id="section_dropdown">
	                                    <option value="" disabled="" selected="">Choose section</option>
	                                </select>
								</div>
								<div class="col-lg-3">
									<label for="">Status</label>
									<select class="form-control" id="status_dropdown">
	                                    <option value="" disabled="" selected="">Choose status</option>
	                                    <option value="1">Active</option>
	                                    <option value="0">Inactive</option>
	                                    <option value="-1">All</option>
	                                </select>
								</div>
								<div class="col-lg-3" style='margin-top: 15px;'>
									<div class="pull-right">
										<a href='' class="btn btn-primary btnSearch">
	              							<i class="fa fa-search"></i>
	            						Search</a>
	            						<a href="" class="btn btn-warning btnReset">
					                      	<i class="fa fa-refresh"></i>
					                    Reset</a>
					                    <button type="button" class="btn  btn-info  btnPrint">Print</button>
				                    </div>
								</div>
							</div>

						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-body">
							
								<table class="table table-striped table-hover" id="students-table">
									<thead>
										<tr>
											<th>Sr#</th>
											<th>Id</th>
											<th>Name</th>
											<th>Father Name</th>
											<th>Fee Category</th>
											<th>Fee Paying</th>
										</tr>
									</thead>
									<tbody>
																		
									</tbody>
								</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>