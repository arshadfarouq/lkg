<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Subject View Report</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			<div class="col-lg-12">
					<!-- <div class="dropdown btn-group pull-right">
					    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
					        Print1
					        <span class="caret"></span>
					    </a>
					    <ul class="dropdown-menu">
					        <li><a href="#">Print With Header</a></li>
					        <li><a href="#">Print Without Header</a></li>
					    </ul>
					</div> -->
				<button type="button" class="btn btn-info  btnPrint pull-right">Print</button>
			</div>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						
							<table class="table table-striped table-hover" id="subjectview-table">
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Subject</th>
										<th>DCNO</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>