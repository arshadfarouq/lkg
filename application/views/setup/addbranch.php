<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-11">
				<h1 class="page_title">Branches</h1>
			</div>
			<div class="col-lg-1">
				<button class="btn btn-primary btn-openmodal btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">Add</button>
			</div>
		</div>
	</div>
	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
							<table class="table table-striped table-hover ar-datatable">
								<thead>
									<tr>
										<th>Sr#</th>
										<th class="hide">Branch Id</th>
										<th>Name</th>
										<th>Phone No</th>
										<th style="padding-top:30px !important;">Address</th>
										<th>Opening Date</th>
										<th>Total Rooms</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php $counter = 1; foreach ($branches as $branch): ?>
									<tr>
										<td><?php echo $counter++; ?></td>
										<td  class="hide"><?php echo $branch['brid']; ?></td>
										<td><?php echo $branch['name']; ?></td>
										<td><?php echo $branch['phone_no']; ?></td>
										<td><?php echo $branch['address']; ?></td>
										<td><?php echo substr($branch['odate'], 0, 10); ?></td>
										<td><?php echo $branch['classes']; ?></td>
										<td><a href="" class="btn btn-primary btn-edit-branch showallupdatebtn"  data-showallupdatebtn='<?php echo $vouchers['branch']['update']; ?>' data-brid="<?php echo $branch['brid']; ?>"><span class="fa fa-edit"></span></a></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg">
				    <div class="modal-content">
					   <div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important";>
					      <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
					      <h4 class="modal-title" id="myModalLabel">Add/Update Branch</h4>
					   </div>
					   <div class="modal-body">
					    	<div class="container-fluid">
					    		<div class="row-fluid">
						        	<div class="col-lg-9 col-lg-offset-1">
						        		<form role="form">
						        			<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-3">
						        						<label for="exampleInputEmail1">Branch Id</label>
											    		<input type="number" id="txtbranchId" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['branch']['update']; ?>'>
											    		<input type="hidden" id="txtMaxbranchIdHidden">
														<input type="hidden" id="txtbranchIdHidden">
						        					</div>
						        				</div>
											</div>
											<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Name</label>
											    		<input type="text" id="txtname" class="form-control" placeholder="Branch name">
											    		<input type="hidden" id="txtnameHidden">
						        					</div>
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">A/C Tittle</label>
											    		<input type="text" class="form-control" id='txtAcTitle'>
						        					</div>
						        				</div>
											</div>
											<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Bank Name</label>
											    		<input type="text" class="form-control" id='txtBankName'>
						        					</div>
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Credit A/C #</label>
											    		<input type="text" class="form-control" id='txtCreditAc'>
						        					</div>
						        				</div>
											</div>
											<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Phone No</label>
											    		<input type="text" id="txtphoneNo" class="form-control num" placeholder="Phone no">
						        					</div>
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Opening Date</label>
											    		<input class="form-control ts_datepicker num" type="text" id="txtopeningDate">
						        					</div>
						        				</div>
											</div>
											<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-12">
						        						<label for="exampleInputEmail1">Address</label>
											    		<input type="text" id="txtaddress" class="form-control" placeholder="Branch address">
						        					</div>
						        				</div>
											</div>
											<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Total Rooms</label>
											    		<input type="text" id="txttotalRooms" class="form-control num" placeholder="Total rooms in branch" maxlength="3">
						        					</div>
						        					<div class="col-lg-6">
						        						<label for="exampleInputEmail1">Total Student</label>
											    		<input type="text" id="txttotalStudents" class="form-control num" placeholder="Total number of students in branch" maxlength="3">
						        					</div>
						        				</div>
											</div>
											<div class="row hide">
												<div class="col-lg-2">
												</div>
												<div class="col-lg-4">
													<select class="form-control" id="allBranches">
													<?php foreach ($branchNames as $branchName): ?>
														<option><?php echo $branchName['name']; ?></option>
													<?php endforeach ?>
												</select>
												</div>
											</div>
						        		</form>
						        	</div>
					        	</div>
					    	</div>
					        
					   </div>
					   <div class="modal-footer">
					   	<div class="pull-right">
						      	<a class="btn btn-success btnSave btn-lg" data-insertbtn='<?php echo $vouchers['branch']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
								<a class="btn btn-warning btnReset btn-lg"><i class="fa fa-refresh"></i> Reset</a>
								<a class="btn btn-danger btn-lg" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
							</div>
					   </div>
				    </div>
				  </div>
				</div>
				
			</div>
		</div>
	</div>
</div>