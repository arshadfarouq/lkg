<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>




<div id="chargeTypeModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Add Charge Type</h3>
		</div>
		<div class="modal-body">

			<div class="input-group">
				<span class="input-group-addon">Type</span>
				<input type="text" class="form-control" id="txtTypeNew">
			</div>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
				<a class="btn btn-default btnNewChargeType"><i class="fa fa-plus"></i> Add</a>
			</div>
		</div>
	</div>
</div>
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Add Charges</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<li class="active"><a data-toggle="tab" href="#add_Charges">Add/Update Charges</a></li>
					<li class=""><a data-toggle="tab" href="#view_all">View All</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_Charges" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<div class="input-group-addon id-addon">Id (Auto)</div>
														<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['charges']['update']; ?>' id="txtChargeId">
														<input type="hidden" id="txtMaxChargeIdHidden">
														<input type="hidden" id="txtChargeIdHidden">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Type</div>
														<select class="form-control" id="txtChargeType">
															<option value="" disabled="" selected="">Choose Type</option>
															<?php foreach ($chargeTypes as $chargeType): ?>
																<option value="<?php echo $chargeType['type']; ?>"><?php echo $chargeType['type']; ?></option>
															<?php endforeach ?>
														</select>

															<a href="#chargeTypeModel" class="btn btn-primary" data-toggle="modal" id="btnAddChargeType">+</a>
														
													</div>
												</div>
												<div class="col-lg-4">
													<div class="input-group" >
														<div class="input-group-addon txt-addon">Account</div>


														<input type="text" class="form-control" id="txtPartyId">
														<input id="hfPartyId" type="hidden" value="" />
														<input id="hfPartyBalance" type="hidden" value="" />
														<input id="hfPartyCity" type="hidden" value="" />
														<input id="hfPartyAddress" type="hidden" value="" />
														<input id="hfPartyCityArea" type="hidden" value="" />
														<input id="hfPartyMobile" type="hidden" value="" />
														<input id="hfPartyUname" type="hidden" value="" />
														<input id="hfPartyLimit" type="hidden" value="" />
														<input id="hfPartyName" type="hidden" value="" />
														<input id="txtHiddenEditQty" type="hidden" value="" />
														<input id="txtHiddenEditRow" type="hidden" value="" />
														<img id="imgPartyLoader" class="hide" src="<?php echo base_url('assets/img/loader.gif'); ?>">


													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Particulars</div>
														
														<input type="text" class="form-control" id="txtParticulars">
													</div>
												</div>
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Charges</div>
														
														<input type="text" class="form-control num" id="txtCharge">
													</div>
												</div>
												<div class="col-lg-4 hide">
													<div class="input-group">
														
														
														<select class="form-control" id="allFeeCategories">
															<?php foreach ($feeCategoryNames as $feeCategoryName): ?>
																<option><?php echo $feeCategoryName['name']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<!-- <a class="btn btn-sm btn-default btnSave"><i class="fa fa-save"></i> Save Changes</a> -->
														<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['charges']['insert']; ?>' data-updatebtn='<?php echo $vouchers['charges']['update']; ?>' data-deletebtn='<?php echo $vouchers['charges']['delete']; ?>' data-printbtn='<?php echo $vouchers['charges']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
														<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
														
														
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane fade">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ar-datatable">
											<thead>
												<tr>
													<td>Sr#</td>
													<td>Branch</td>
													<td>Type</td>
													<td>Particulars</td>
													<td>Charges</td>
													<td></td>
												</tr>
											</thead>
											<tbody>
												<?php $counter = 1; foreach ($charges as $charge): ?>
												<tr>
													<td><?php echo $counter++; ?></td>
													<td><?php echo $charge['branch_name']; ?></td>
													<td><?php echo $charge['type']; ?></td>
													<td><?php echo $charge['description']; ?></td>
													<td><?php echo $charge['charges']; ?></td>
													<td><a href="" class="btn btn-primary btn-edit-charge showallupdatebtn"  data-showallupdatebtn='<?php echo $vouchers['charges']['update']; ?>' data-chid="<?php echo $charge['chid']; ?>"><span class="fa fa-edit"></span></a></td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div> <!-- end of search_branch -->
			</div>
		</div>
	</div>
</div>
</div>