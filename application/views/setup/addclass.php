<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-11">
						<h1 class="page_title">Class</h1>
					</div>
					<div class="col-lg-1">
						<button class="btn btn-primary btn-openmodal btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">Add</button>
					</div>
				</div>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>
	<div class="page_content">
		<div class="container-fluid">
			<div class="col-md-12">
					<!--.......................starting of tabel.........................-->
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<table class="table table-striped table-hover ar-datatable">
										<thead>
											<tr>
												<th>Sr#</th>
												<th>Class</th>
												<th>Branch</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php $counter = 1; foreach ($_classes as $_class): ?>
												<tr>
													<td><?php echo $counter++; ?></td>
													<td><?php echo $_class['name'] ?></td>
													<td><?php echo $_class['branch_name']; ?></td>
													<td><a href="" class="btn btn-primary btn-edit-class showallupdatebtn"  data-showallupdatebtn='<?php echo $vouchers['class']['update']; ?>'  data-claid="<?php echo $_class['claid']; ?>"><span class="fa fa-edit"></span></a></td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div><!--panel body-->
							</div>
						</div>
					</div>

					<!-- Modal -->
					<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">

								<div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
									<button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
									<h4 class="modal-title" id="myModalLabel">Add/Update Class</h4>
								</div>

								<div class="modal-body">
							    	<div class="container-fluid">
							    		<div class="row-fluid">
								        	<div class="col-lg-9 col-lg-offset-1">
												
												<form role="form ">
					                        		<div class="form-group">
					                        			<div class="row">
						                        			<div class="col-lg-2">
						                        				<label for=''>Class Id</label>
						                        				<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['class']['update']; ?>' id="txtClassId">
																<input type="hidden" id="txtMaxClassIdHidden">
																<input type="hidden" id="txtClassIdHidden">
						                        			</div>
					                        			</div>
					                        		</div><!--form-group-->

					                        		<div class="form-group">
					                        			<div class="row">
					                        				<div class="col-lg-6">
					                        					<label>Class</label>
					                        					<input type="text" class="form-control" placeholder="Class name" id="txtClassName">
																<input type="hidden" id="txtClassNameHidden">
					                        				</div>
					                        			</div>
					                        		</div><!--form-group-->

					                        		<div class="row hide">
														<div class="col-lg-2"></div>
														<div class="col-lg-4">
															<select class="form-control" id="allClasses"></select>
														</div>
													</div><!--end of row hide-->
					                        	</form>

								        	</div>
							        	</div>
							    	</div>
							   </div>
							   <div class="modal-footer">
							   		<div class="pull-right">
								      	<a class="btn btn-success btnSave btn-lg" data-insertbtn='<?php echo $vouchers['class']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
										<a class="btn btn-warning btnReset btn-lg"><i class="fa fa-refresh"></i> Reset</a>
										<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
									</div>
							   </div>
							</div>
						</div>
					</div>

  				</div><!--col-lg-12-->
			</div><!--container-fluid-->
	</div><!--page-content-->
</div><!--mainwrapper-->