<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-11">
				<h1 class="page_title">Fee Category</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
			<div class="col-lg-1">
				<button class="btn btn-openmodal btn-primary btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">
				  Add
				</button>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table hover ar-datatable">
											<thead>
												<tr>
													<th>Sr#</th>
													<th class="hide">Fee Category Id</th>
													<th>Category Name</th>
													<th>Description</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php if (count($feeCategories) > 0): ?>
													<?php $counter = 1; foreach ($feeCategories as $feeCategory): ?>
														<tr>
															<td><?php echo $counter++; ?></td>
															<td class="hide"><?php echo $feeCategory['fcid']; ?></td>
															<td><?php echo $feeCategory['name']; ?></td>
															<td><?php echo $feeCategory['description']; ?></td>
															<td><a href="" class="btn btn-primary btn-edit-feeCategory showallupdatebtn"  data-showallupdatebtn='<?php echo $vouchers['fee_category']['update']; ?>' data-fcid="<?php echo $feeCategory['fcid']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach; ?>
												<?php endif ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

				<!-- Modal -->
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg">
				    <div class="modal-content">
					   <div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
							<button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="myModalLabel">Add/Update Fee Category</h4>
					   </div>
					   <div class="modal-body">
					    	<div class="container-fluid">
					    		<div class="row-fluid">
						        	<div class="col-lg-9 col-lg-offset-1">
						        		<form role="form">
						        			<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-3">
						        						<label for="exampleInputEmail1">Fee Category Id</label>
											    		<input type="number" id="txtFeeCatId" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['fee_category']['update']; ?>'>
														<input type="hidden" id="txtMaxFeeCatIdHidden">
														<input type="hidden" id="txtFeeCatIdHidden">
						        					</div>
						        				</div>
											</div>
											<div class="form-group">
						        				<div class="row">
						        					<div class="col-lg-5">
						        						<label for="exampleInputEmail1">Category</label>
											    		<input type="text" id="txtName" class="form-control" placeholder="Fee category">
														<input type="hidden" id="txtNameHidden">
						        					</div>
						        					<div class="col-lg-5">
						        						<label for="exampleInputEmail1">Description</label>
											    		<input type="text" id="txtDescription" class="form-control" placeholder="Category description">
						        					</div>
						        				</div>
											</div>
											
											<div class="row hide">
												<div class="col-lg-2">
												</div>
												<div class="col-lg-4">
													<select class="form-control" id="allFeeCategories">
														<?php foreach ($feeCategoryNames as $feeCategoryName): ?>
															<option><?php echo $feeCategoryName['name']; ?></option>
														<?php endforeach ?>
													</select>
												</div>
											</div>
						        		</form>
						        	</div>
					        	</div>
					    	</div>
					        
					   </div>
					   <div class="modal-footer">
					   	<div class="pull-right">
								<a class="btn btn-success btnSave btn-lg" data-insertbtn='<?php echo $vouchers['fee_category']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
								<a class="btn btn-warning btnReset btn-lg"><i class="fa fa-refresh"></i> Reset</a>
								<a class="btn btn-danger btn-lg" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
							</div>
					   </div>
				    </div>
				  </div>
				</div>
				
			</div>
		</div>
	</div>
</div>