<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-11">
						<h1 class="page_title">Section</h1>
					</div>
					<div class="col-lg-1">
						<!-- Button trigger modal -->
						<button class="btn btn-primary btn-openmodal btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">Add</button>
					</div>
				</div>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
			<div class="col-md-12">

				<!--.......................starting of tabel.........................-->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<table class="table table-striped table-hover ar-datatable">
									<thead>
										<tr>
											<th>Sr#</th>
											<th>Branch</th>
											<th>Class</th>
											<th>Section</th>
											<th>Room</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $counter = 1; foreach ($sections as $section): ?>
											<tr>
												<td><?php echo $counter++; ?></td>
												<td><?php echo $section['branch_name']; ?></td>
												<td><?php echo $section['class_name']; ?></td>
												<td><?php echo $section['name']; ?></td>
												<td><?php echo $section['room']; ?></td>
												<td><a href="" class="btn btn-primary btn-edit-section showallupdatebtn"  data-showallupdatebtn='<?php echo $vouchers['section']['update']; ?>' data-secid="<?php echo $section['secid']; ?>"><span class="fa fa-edit"></span></a></td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					
					<div class="modal-dialog modal-lg">
				    	<div class="modal-content">

						   	<div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
					      		<button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
	        					<h4 class="modal-title" id="myModalLabel">Add/Update Section</h4>
		      				</div>
	  						<div class="modal-body">
	    						<div class="container-fluid">
	                  				<div class="col-lg-11 col-lg-offset-1">
	                        			<div class="row">

				                        	<form role="form">
				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-2">
				                        					<label>Section Id</label>
				                        					<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['section']['update']; ?>' id="txtSectionId">
															<input type="hidden" id="txtSectionIdHidden">
															<input type="hidden" id="txtMaxSectionIdHidden">
				                        				</div>
				                        			</div><!--end of row-->
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-5">
				                        					<label>Class</label>
				                        					<select class="form-control" id="txtClassName">
																<option value="" disabled="" selected="">Choose Class</option>
															</select>
				                        				</div>
				                        				<div class="col-lg-5">
				                        					<label>Section</label>
				                        					<input type="hidden" id="txtSectionNameHidden">
															<input type="text" class="form-control" placeholder="Section name"  id="txtSectionName">
				                        				</div>
				                        			</div><!--end of row-->
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-5">
				                        					<label>Room</label>
				                        					<input type="text" class="form-control" placeholder="Section room no"  id="txtRoomNo" maxlength="3">
				                        				</div>
				                        			</div>
				                        		</div><!--form-group-->

				                        		<div class="row hide">
													<div class="col-lg-2"></div>
													<div class="col-lg-4">
														<select class="form-control" id="allSections"></select>
													</div>
												</div><!--row-hide-->
				                        	</form>

	                    				</div><!--end of row-->
	                  				</div><!--end of col-lg-11-->
					        	</div><!--container-fluid-->
					      	</div>

							<div class="modal-footer">
		        				<div class="pull-right">
									<a class="btn btn-default btnSave btn-success btn-lg" data-insertbtn='<?php echo $vouchers['section']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
									<a class="btn btn-default btnReset btn-warning btn-lg"><i class="fa fa-refresh"></i> Reset</a>
									<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
								</div>
							</div><!--modal-footer-->

						</div><!--modal-content-->
      				</div><!--modal-dialogue-->

    			</div><!--modal-->
  			</div><!--col-lg-12-->
		</div><!--container-fluid-->
	</div><!--page-content-->
</div>