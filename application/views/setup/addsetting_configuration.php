<!-- main content -->
<div id="main_wrapper">
  <div class="page_bar">
    <div class="row-fluid">
      <div class="col-lg-6">
        <h1 class="page_title"><span class="ion-android-storage"></span> Setting Configuration</h1>
      </div>
      <div class="col-lg-6">
        <div class="pull-right top-btns">
          <a class="btn btn-default btnSave"><i class="fa fa-save"></i> Save F10</a>
          <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
        </div>
      </div>
    </div><hr>
  </div>
  <div class="page_content">
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-12">
            <form action="" class="frst-form">
              <div class="form-group">
                <div class="row">
                </div>
              </div><!-- form-group -->  
              <legend style="margin-top: 30px;">General Setting</legend>
              <div class="form-group">
                <div class="row">
                 <div class="col-lg-2">
                  <label>Supplier</label>
                  <select class="form-control select2" id="supplier">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($levels3 as $partys): ?>
                      <option value="<?php echo $partys['l3']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Customers</label>
                  <select class="form-control select2" id="customers">
                    <option value="" selected="" disabled="">Choose ...</option>

                    <?php foreach ($levels3 as $partys): ?>
                      <option value="<?php echo $partys['l3']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Purchase</label>
                  <select class="form-control select2" id="purchase">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Purchase Return</label>
                  <select class="form-control select2" id="purchaseReturn">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Sale</label>
                  <select class="form-control select2" id="sale">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                

              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-lg-2">
                  <label>Sale Return</label>
                  <select class="form-control select2" id="saleReturn">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Item Discount</label>
                  <select class="form-control select2" id="itemsdiscount">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Invoice Discount</label>
                  <select class="form-control select2" id="invoicediscount">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Cash In Hand</label>
                  <select class="form-control select2" id="cash">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Freight</label>
                  <select class="form-control select2" id="freight">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                
                
                
              </div>
            </div><!-- form-group -->
            <div class="form-group">
              <div class="row">
                <div class="col-lg-2">
                  <label>Credit Card</label>
                  <select class="form-control select2" id="creditcard">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Bank Charges</label>
                  <select class="form-control select2" id="bankcharges">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Expenses</label>
                  <select class="form-control select2" id="expenses">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Tax</label>
                  <select class="form-control select2" id="tax">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Commission</label>
                  <select class="form-control select2" id="commission">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-lg-2">
                  <label>Fee Receive</label>
                  <select class="form-control select2" id="feereceive">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Arears</label>
                  <select class="form-control select2" id="arears">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-2">
                  <label>Late Fee Fine</label>
                  <select class="form-control select2" id="latefeefine">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                
                
                
              </div>
            </div>
            
            <div class="form-group">
              <div class="row">
                <div class="col-lg-2">
                 <label>Description In Ledger</label>
                 <div class="form-group">                                                                
                  <div class="input-group">
                   <span class="switch-addon">Ledger ?</span>
                   <input type="checkbox" checked="" class="bs_switch" id="switchledgerdes">
                 </div>
               </div>
             </div>

           </div>
         </div>

         
         
         <!-- general setting end -->
         <!-- balance sheet start -->
         <legend style="margin-top: 30px;">Balance Sheet</legend>
         <div class="form-group">
          <div class="row">
            <div class="col-lg-2">
              <label>Assets</label>
              <select class="form-control select2" id="assets">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels1 as $partys): ?>
                  <option value="<?php echo $partys['l1']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Liabilities</label>
              <select class="form-control select2" id="Liabilities">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels1 as $partys): ?>
                  <option value="<?php echo $partys['l1']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Income</label>
              <select class="form-control select2" id="inocome">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels1 as $partys): ?>
                  <option value="<?php echo $partys['l1']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Expenses</label>
              <select class="form-control select2" id="txtexpenses">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels1 as $partys): ?>
                  <option value="<?php echo $partys['l1']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>

            <div class="col-lg-2">
              <label>Sale Income</label>
              <select class="form-control select2" id="saleincome">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels2 as $partys): ?>
                  <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>

          </div>
        </div>

        <!--  -->
        <div class="form-group">
          <div class="row">
            <div class="col-lg-2">
              <label>Cost Of Goods Sold</label>
              <select class="form-control select2" id="costOfgOodssOld">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels2 as $partys): ?>
                  <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Operating Expenses</label>
              <select class="form-control select2" id="OperatingExpenses">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels2 as $partys): ?>
                  <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Other Expenses</label>
              <select class="form-control select2" id="OtherExpenses">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels2 as $partys): ?>
                  <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Other Income</label>
              <select class="form-control select2" id="Otherincome">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels2 as $partys): ?>
                  <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-2">
              <label>Finance Cost</label>
              <select class="form-control select2" id="financecost">
                <option value="" selected="" disabled="">Choose ...</option>
                <?php foreach ($levels2 as $partys): ?>
                  <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>

          </div>
        </div>
        <!--  -->
        <div class="form-group">
          <div class="row">

           <div class="col-lg-2">
            <label>Work Profit</label>
            <select class="form-control select2" id="WorkProfit">
              <option value="" selected="" disabled="">Choose ...</option>
              <?php foreach ($levels2 as $partys): ?>
                <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div> 

          <div class="col-lg-2">
            <label>Provision For Taxation</label>
            <select class="form-control select2" id="Provision">
              <option value="" selected="" disabled="">Choose ...</option>
              <?php foreach ($levels2 as $partys): ?>
                <option value="<?php echo $partys['l2']; ?>"><?php echo $partys['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div> 

          <!--  -->
        </div>
      </div>
      <!--  -->
      <!-- balance sheet end -->
      <!-- VOucher setting start -->
      <legend style="margin-top: 30px;">Voucher Setting</legend>
      <div class="form-group">
        <div class="row">

          <div class="col-lg-2" >
            <label>Default Print</label>
            <select class="form-control select2" id="defaultprint">
              <option value="A4"   selected="" > A4</option>
              <option value="A4Half"   selected="" >A4 Half</option>
              <option value="A4Urdu"   selected="" >A4 Urdu</option>
              <option value="A4HalfUrdu"   selected="" >A4 Half Urdu</option>
              <option value="ThermalEnglish"   selected="" >Thermal English</option>
              <option value="ThermalUrdu"   selected="" >Thermal Urdu</option>
              <option value="AccountVoucher"   selected="" >Account Voucher</option>
              

              
              
              
            </select>
          </div>
          <div class="col-lg-2">
           <label>Print Out</label>
           <div class="form-group">                                                                
            <div class="input-group">
             <span class="switch-addon">Print ?</span>
             <input type="checkbox" checked="" class="bs_switch" id="switchPreview">
           </div>
         </div>
       </div>
       
       <div class="col-lg-2">
        <label>Previous Balance</label>

        <div class="form-group">                                                                
          <div class="input-group">
            <span class="switch-addon">Pre Bal?</span>
            <input type="checkbox" checked="" class="bs_switch" id="switchPreBal">
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <label>Company Header</label>

        <div class="form-group">                                                                
          <div class="input-group">
            <span class="switch-addon">Header?</span>
            <input type="checkbox" checked="" class="bs_switch" id="switchHeader">
          </div>
        </div>
      </div>
      



    </div>
  </div>
  <!--  -->
  <div class="form-group">
    <div class="row">
      <div class="col-lg-2">
       <label>Stock Status</label>
       <div class="form-group">                                                                
        <div class="input-group">
         <span class="switch-addon">Status ?</span>
         <input type="checkbox" checked="" class="bs_switch" id="switchStatus">
       </div>
     </div>
   </div>
   <div class="col-lg-2">
     <label>Item Image</label>
     <div class="form-group">                                                                
      <div class="input-group">
       <span class="switch-addon">Image ?</span>
       <input type="checkbox" checked="" class="bs_switch" id="switchImage">
     </div>
   </div>
 </div>
 <div class="col-lg-2">
   <label>Optional Feilds</label>
   <div class="form-group">                                                                
    <div class="input-group">
     <span class="switch-addon">Feilds ?</span>
     <input type="checkbox" checked="" class="bs_switch" id="switchFeilds">
   </div>
 </div>
</div>
<div class="col-lg-2">
 <label>Last 5 Prices</label>
 <div class="form-group">                                                                
  <div class="input-group">
   <span class="switch-addon">Prices ?</span>
   <input type="checkbox" checked="" class="bs_switch" id="switchPrices">
 </div>
</div>
</div>

</div>
</div>

<legend style="margin-top: 30px;">Voucher Summary Fields</legend>

<div class="form-group">
  <div class="row">
    <div class="col-lg-2">
     <label>Discount</label>
     <div class="form-group">                                                                
      <div class="input-group">
       <span class="switch-addon">Hide ?</span>
       <input type="checkbox" checked="" class="bs_switch" id="switchDiscount">
     </div>
   </div>
 </div>
 <div class="col-lg-2">
   <label>Expenses</label>
   <div class="form-group">                                                                
    <div class="input-group">
     <span class="switch-addon">Hide ?</span>
     <input type="checkbox" checked="" class="bs_switch" id="switchExpense">
   </div>
 </div>
</div>
<div class="col-lg-2">
 <label>Tax</label>
 <div class="form-group">                                                                
  <div class="input-group">
   <span class="switch-addon">Hide ?</span>
   <input type="checkbox" checked="" class="bs_switch" id="switchTaxVoucher">
 </div>
</div>
</div>
<div class="col-lg-2">
 <label>PaidAmount</label>
 <div class="form-group">                                                                
  <div class="input-group">
   <span class="switch-addon">Hide ?</span>
   <input type="checkbox" checked="" class="bs_switch" id="switchPaid">
 </div>
</div>
</div>

</div>
</div>
<!--  -->
<!-- VOucher setting end -->
<!-- Tax setting strat -->
<legend style="margin-top: 30px;">Tax Setting</legend>
<div class="form-group">
  <div class="row">
    
   <div class="col-lg-2">
     <label>By Default Tax Option</label>
     <div class="form-group">                                                                
      <div class="input-group">
       <span class="switch-addon">Register ?</span>
       <input type="checkbox" checked="" class="bs_switch" id="switchOption">
     </div>
   </div>
 </div>
 <div class="col-lg-2">
   <label>Tax Hide/Show</label>
   <div class="form-group">                                                                
    <div class="input-group">
     <span class="switch-addon">Tax ?</span>
     <input type="checkbox" checked="" class="bs_switch" id="switchTax">
   </div>
 </div>
</div>

</div>
</div>
<!--  -->
<div class="form-group">
  <div class="row">
   
   <div class="col-lg-2">
    <div class="input-group">
     <span class="input-group-addon txt-addon">Sale Tax</span>
     <input class="form-control input-sm" id="txtSaleTax" type="text">
   </div>
 </div>
 <div class="col-lg-2">
  <div class="input-group">
   <span class="input-group-addon txt-addon">Further Tax</span>
   <input class="form-control input-sm" id="txtFurtherTax" type="text">
 </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <span class="input-group-addon txt-addon">Extra Tax</span>
   <input class="form-control input-sm" id="txtExtraTax" type="text">
 </div>
</div>
<div class="col-lg-2">
  <div class="input-group">
   <span class="input-group-addon txt-addon">Holding Tax</span>
   <input class="form-control input-sm" id="txtHoldingTax" type="text">
 </div>
</div>


</div>
</div>
<!--  -->
<div class="form-group">
  <div class="row">
    <div class="col-lg-2">
      <label>Sale Tax</label>
      <select class="form-control select2" id="sale_tax">
        <option value="" selected="" disabled="">Choose ...</option>
        <?php foreach ($party as $partys): ?>
          <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-lg-2">
      <label>Further Tax</label>
      <select class="form-control select2" id="Further_tax">
        <option value="" selected="" disabled="">Choose ...</option>
        <?php foreach ($party as $partys): ?>
          <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-lg-2">
      <label>Extra Tax</label>
      <select class="form-control select2" id="Extra_tax">
        <option value="" selected="" disabled="">Choose ...</option>
        <?php foreach ($party as $partys): ?>
          <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-lg-2">
      <label>Holding Tax</label>
      <select class="form-control select2" id="Holding_tax">
        <option value="" selected="" disabled="">Choose ...</option>
        <?php foreach ($party as $partys): ?>
          <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
        <?php endforeach; ?>
      </select>
    </div>



  </div>
</div>
<!--  -->
<!-- Tax setting end -->
</div>
</form><!-- end of form -->
</div><!-- end of col -->
</div><!-- end of row -->
<div class="row">
  <div class="col-lg-12">
    <div class="pull-right">
      <a class="btn btn-default btnSave"><i class="fa fa-save"></i> Save F10</a>
      <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
    </div><!-- pull-right -->
  </div><!-- end of col --> 
</div><!-- end of row -->
</div><!-- end of col -->
</div><!-- container-fluid -->
</div><!-- page-content -->
</div><!--main-wrapper -->