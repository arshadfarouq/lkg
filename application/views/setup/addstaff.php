<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

  <div class="page_bar">
    <div class="row">
      <div class="col-md-12">
        	<h1 class="page_title">Add Staff</h1>
        	<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
			<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
      </div>
    </div>
  </div>

  <div class="page_content">
    <div class="container-fluid">

      <div class="col-md-12">

        <form action="">

          	<ul class="nav nav-pills">
            	<li class="active"><a href="#basicInformation" data-toggle="tab">Basic Information</a></li>
            	<!-- <li><a href="#salarydeductions" data-toggle="tab">Salary &amp; Deductions</a></li> -->
            	<li><a href="#qualification" data-toggle="tab">Qualifications</a></li>
            	<li><a href="#experience" data-toggle="tab">Experience</a></li>
          	</ul>

        <div class="tab-content">
            <div class="tab-pane active" id="basicInformation">

	            <div class="row">
	                <div class="col-lg-12">
	                  	<div class="panel panel-default">
	                  		<div class="panel-body">

	                  			<div class="row">
	                  				<div class="col-lg-9">

										<div class="row"></div>
										<div class="row"></div>
										<div class="row"></div>
										<div class="row"></div>
					                    <div class="row">

					                        <div class="col-lg-3">
					                          	<div class="input-group">
					                            	<span class="input-group-addon">Id</span>
					                            	<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['staff']['update']; ?>' id="txtStaffId">
				                                    <input type="hidden" id="txtMaxStaffIdHidden">
				                                    <input type="hidden" id="txtStaffIdHidden">
				                                    <input type="hidden" id="txtPIdHidden">
						                        </div>
					                        </div>

					                        <div class="col-lg-3">
					                            <div class="form-group">
					                              	<div class="col-lg-3">
							                            <div class="input-group">
							                              <span class="switch-addon">Is active?</span>
							                              <input type="checkbox" checked="" class="bs_switch active_switch" id="gender">
							                            </div>
							                        </div>
					                            </div>
					                        </div>
					                    </div>

					                    <div class="row">

					                    	<div class="col-lg-3">
					                            <div class="input-group">
					                                <span class="input-group-addon">Date</span>
					                                <input class="form-control ts_datepicker" type="text" id="current_date">
					                            </div>
					                        </div>

					                        <div class="col-lg-4">
					                            <div class="input-group">
					                                <span class="input-group-addon">Type</span>
					                                <select class="form-control" id="type_dropdown">
					                                	<option value="" disabled="" selected="">Choose Type</option>
					                                	<?php foreach ($types as $type): ?>
			                                              	<option value="<?php echo $type['type']; ?>"><?php echo $type['type']; ?></option>
			                                          	<?php endforeach ?>
					                                </select>
					                                <span class="input-group-btn">
					                                	<a href="#TypeModel" data-toggle="modal" class="btn btn-primary">+</a>
					                                </span>
					                            </div>
					                        </div>

					                        <div class="col-lg-5">
					                            <div class="input-group">
					                                <span class="input-group-addon">Agreement</span>
					                                <select class="form-control" id="agreement_dropdown">
					                                	<option value="" disabled="" selected="">Choose Agreement</option>
					                                	<?php foreach ($agreements as $agreement): ?>
			                                              	<option value="<?php echo $agreement['agreement']; ?>"><?php echo $agreement['agreement']; ?></option>
			                                          	<?php endforeach ?>
					                                </select>
					                                <span class="input-group-btn">
					                                	<a href="#AgreementModel" data-toggle="modal" class="btn btn-primary">+</a>
					                                </span>
					                            </div>
					                        </div>
					                    </div>
	                  				</div>
	                  				<div class="col-lg-3">
                  						<div style="background: #F5F6F7;padding: 15px;border: 1px solid #ccc;box-shadow: 1px 1px 1px #000;">
				                            <div class="row">
				                              	<div class="col-lg-12">
				                                  	<div class="studentImageWrap">
				                                      	<img src="<?php echo base_url('assets/img/student.jpg'); ?>" alt="student" style="margin: auto;display: block;" id="staffImageDisplay">
				                                  	</div>
				                              	</div>
				                            </div>

				                            <div class="row">
				                              	<div class="col-lg-12">
				                                  	<input type="file" id="staffImage">
				                              	</div>
				                            </div>
                  						</div>
	                  				</div>
	                  			</div>

			                </div>
	                  	</div>
	                </div>
	            </div>  <!-- end of row 1 of basic information -->

              <div class="row">
                  <div class="col-lg-6">
                      <div class="panel panel-default">
                          <div class="panel-heading">General Information</div>

                          <div class="panel-body">

                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                        <span class="input-group-addon">Name</span>
                                        <input type="text" class="form-control" placeholder="Staff name" id="txtName"/>
                                    </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Father Name</span>
                                          <input type="text" class="form-control" placeholder="Father name" id="txtFatherName">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Gender</span>
                                          <select class="form-control" id="gender_dropdown">
                                            <option value='male'>Male</option>
                                            <option value='female'>Female</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          	<span class="input-group-addon">Martial Status</span>
                                          	<select class="form-control" id="marital_dropdown">
                                            	<option value='single'>Single</option>
                                            	<option value='married'>Married</option>
                                            	<option value='divorced'>Divorced</option>
                                            	<option value='widowed'>Widowed</option>
                                          	</select>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">Religion</span>
	                                        <select class="form-control" id="religion_dropdown">
	                                            <option value="" disabled="" selected="">Choose Religion</option>
	                                            <?php foreach ($religions as $religion): ?>
	                                              	<option value="<?php echo $religion['religion']; ?>"><?php echo $religion['religion']; ?></option>
	                                          	<?php endforeach ?>
	                                        </select>
	                                        <span class="input-group-btn">
	                                            <a href="#ReligionModel" data-toggle="modal" class="btn btn-primary">+</a>
	                                        </span>
	                                    </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">CNIC</span>
                                          <input class="form-control num" type="text" placeholder="Staff national ID card no" id="txtcnic" maxlength="13">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Date of Birth</span>
                                          <input class="form-control ts_datepicker" type="text" id="birth_date">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="input-group">
                                          <span class="input-group-addon">Date of Joining</span>
                                          <input class="form-control ts_datepicker" type="text" id="joining_date">
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>    <!-- end of general information -->
                  <div class="col-lg-6">
                  		<div class="row">
                  			<div class="col-lg-12">
                  				<div class="panel panel-default">
		                        <div class="panel-heading">Contact Information</div>

		                        <div class="panel-body">
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Permanent Address</span>
		                                          <input type="text" class="form-control" id="txtPermanetAddress">
		                                      </div>
		                                  </div>
		                              </div>
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Present Address</span>
		                                          <input type="text" class="form-control" id="txtAddress">
		                                      </div>
		                                  </div>
		                              </div>
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Phone No</span>
		                                          <input type="text" class="form-control num" placeholder="Phone no" id="txtPhoneNo">
		                                      </div>
		                                  </div>
		                              </div>
		                              <div class="row">
		                                  <div class="col-lg-12">
		                                      <div class="input-group">
		                                          <span class="input-group-addon">Mobile No</span>
		                                          <input type="text" class="form-control num" placeholder="Mobile No" id="txtMobileNo">
		                                      </div>
		                                  </div>
		                              </div>
		                          </div>
		                      </div>
                  			</div>
                  		</div>

                  		<div class="row">
                  			<div class="col-lg-12">
			              		<div class="panel panel-default">
			              			<div class="panel-heading">Bank Account Information</div>
			              			<div class="panel-body">
			              				<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Bank Name</span>
						                            <select class="form-control" id="bank_dropdown">
						                                <option value="" disabled="" selected="">Choose Bank</option>
						                                <?php foreach ($banks as $bank): ?>
			                                              	<option value="<?php echo $bank['bankname']; ?>"><?php echo $bank['bankname']; ?></option>
			                                          	<?php endforeach ?>
						                            </select>
						                            <span class="input-group-btn">
						                            	<a href="#BankModel" data-toggle="modal" class="btn btn-primary">+</a>
						                            </span>
			              						</div>
			              					</div>
			              				</div>

										<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Account #</span>
			              							<input type="text" class="form-control" id="txtAccountNo">
			              						</div>
			              					</div>
			              				</div>

										<div class="row">
			              					<div class="col-lg-12">
			              						<div class="input-group">
			              							<span class="input-group-addon">Salary</span>
			              							<input type="text" class="form-control" id="txtSalary">
			              						</div>
			              					</div>
			              				</div>
			              			</div>
			              		</div>
                  			</div>
                  		</div>
                  </div>    <!-- end of contact information -->
              </div>  <!-- end of row general information -->

            </div>    <!-- end of basicInformation -->

            <!-- <div class="tab-pane" id="salarydeductions">
            	<div class="row">
	              	<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading">Salary</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">BS</span>
											<input type="text" class="form-control num" id='txtbs'>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Designation</span>
											<input type="text" class="form-control" id="txtDesignation">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Initial Pay</span>
											<input type="text" class="form-control num calc" id='txtinipay'>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Adhoc1</span>
											<input type="text" class="form-control num" id='txtadhoc1'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Basic Pay</span>
											<input type="text" class="form-control num calc" id='txtbpay'>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Adhoc 2</span>
											<input type="text" class="form-control num calc" id='txtadhoc2'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">House Rent</span>
											<input type="text" class="form-control num calc" id='txthrent'>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Sc All</span>
											<input type="text" class="form-control num calc" id='txtscall'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Med. Allow.</span>
											<input type="text" class="form-control num calc" id='txtmedallow'>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Public Sc All</span>
											<input type="text" class="form-control num calc" id='txtpublicsall'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Conv. Allow.</span>
											<input type="text" class="form-control num calc" id='txtconvallow'>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">SA All</span>
											<input type="text" class="form-control num calc" id='txtsaall'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Entertainment</span>
											<input type="text" class="form-control num calc" id='txtentertain'>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Arrear</span>
											<input type="text" class="form-control num calc" id='txtarrears'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Charge</span>
											<input type="text" class="form-control num calc" id='txtcharg'>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon cus-group-addon">Dearness</span>
											<input type="text" class="form-control num calc" id='txtdearness'>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon ">House Health</span>
											<input type="text" class="form-control num calc" id='txthouseh'>
										</div>
									</div>
								</div>

								<div class="row">
									<hr>
									<div class="col-lg-6">
									</div>
									<div class="col-lg-6">
										<div class="input-group pull-right">
											<span class="input-group-addon">Total Pay</span>
											<input type="text" class="form-control num" readonly="readonly" id='txttotalpay'>
										</div>
									</div>
								</div>
							</div>
						</div>
	              	</div>
	              	<div class="col-lg-4">
	              		<div class="panel panel-default">
	              			<div class="panel-heading">Deductions</div>
	              			<div class="panel-body">
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">P Fund</span>
											<input type="text" class="form-control num calc2" id='txtpfund'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Income Tax</span>
											<input type="text" class="form-control num calc2" id='txtincome'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Loan</span>
											<input type="text" class="form-control num calc2" id='txtloan'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">S. Count</span>
											<input type="text" class="form-control num calc2" id='txtscont'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Recovery Leaves</span>
											<input type="text" class="form-control num calc2" id='txtrecovery'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Hostel</span>
											<input type="text" class="form-control num calc2" id='txthostle'>
										</div>
	              					</div>
	              				</div>
	              				<div class="row">
	              					<div class="col-lg-12">
	              						<div class="input-group">
											<span class="input-group-addon cus-group-addon2">Pessi</span>
											<input type="text" class="form-control num calc2" id='txtpessi'>
										</div>
	              					</div>
	              				</div>

								<div class="row">
									<hr>
									<div class="col-lg-2">
									</div>
									<div class="col-lg-10">
										<div class="input-group pull-right">
											<span class="input-group-addon">Total Deductions</span>
											<input type="text" class="form-control" readonly="readonly" id='txttdeduc'>
										</div>
									</div>
								</div>
	              			</div>
	              		</div>
	              	</div>
              	</div>

              	<div class="row">
              		<div class="col-lg-12">
	              		<div class="panel panel-default">
	              			<div class="panel-body">
	              				<div class="row">
	              					<div class="col-lg-8"></div>
	              					<div class="col-lg-4">
	              						<div class="input-group pull-right">
											<span class="input-group-addon">Net Pay</span>
											<input type="text" class="form-control" readonly="readonly" id='txtnetpay'>
										</div>
	              					</div>
	              				</div>
	              			</div>
	              		</div>
              		</div>
              	</div>
            </div> -->    <!-- end of salary -->

            <div class="tab-pane" id="qualification">
				<div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="row">

                              	<div class="col-lg-3" style="width:270px;">
                                	<div class="input-group">
                                  		<span class="input-group-addon">Quali./Job</span>
                                  		<input type="text" class="form-control input-sm" style="width:170px;" id='txtQuali'>
                                	</div>
                              	</div>

                              	<div class="col-lg-2" style="width:145px;">
                                	<div class="input-group">
                                  		<span class="input-group-addon">Division</span>
                                  		<input type="text" class="form-control input-sm" style="width:60px;" id='txtDivision'>
                                	</div>
                              	</div>

                              <div class="col-lg-2" style="width:145px;">
                                <div class="input-group" >
                                  <span class="input-group-addon">Year</span>
                                  <input type="text" class="form-control input-sm num" style="width:80px;" id='txtYear'>
                                </div>
                              </div>
                              <div class="col-lg-2" style="width:288px;">
                                <div class="input-group">
                                  <span class="input-group-addon">Name of Insti.</span>
                                  <input type="text" class="form-control input-sm" style="width:170px;" id='txtInstitute'>
                                </div>
                              </div>
                              <div class="col-lg-2" style="width:355px;">
                                <div class="input-group">
                                  <span class="input-group-addon">Major Subjects</span>
                                  <input type="text" class="form-control input-sm" style="width:228px;" id='txtMSubjects'>
                                </div>
                              </div>
                              <div class="col-lg-1">
                                <div class="input-group">
                                  <a href='' class="btn btn-primary input-sm" id='btnAddQuali'>+</a>
                                </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-12">
                                  <table class="table table-striped table-hover" id='qualification-table'>
                                      <thead>
                                          <tr>
                                              <th class="thwidth3">Qualification/Job</th>
                                              <th class="thwidth4">Division</th>
                                              <th class="thwidth5">Year</th>
                                              <th class="thwidth6">Name of Institution</th>
                                              <th class="thwidth2">Major Subjects</th>
                                              <th></th>
                                          </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                          </div>
                      </div>
                  </div>
                </div>  <!-- end of admission test row 2 -->
            </div>     <!-- end of familyBackground -->

            <div class="tab-pane" id="experience">

                <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-lg-5">
                                <div class="input-group">
                                  <span class="input-group-addon">Job Held</span>
                                  <input type="text" class="form-control" id='txtJobHeld'>
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="input-group">
                                  <span class="input-group-addon">From</span>
                                  <input class="form-control ts_datepicker" type="text" id='from_date'>
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="input-group">
                                  <span class="input-group-addon">To</span>
                                  <input class="form-control ts_datepicker" type="text" id='to_date'>
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="input-group">
                                  <span class="input-group-addon">Pay Draws</span>
                                  <input type="text" class="form-control num" id='txtPayDraws'>
                                </div>
                              </div>
                              <div class="col-lg-1">
                                <div class="input-group">
                                  <a href='' class="btn btn-primary" id='btnAddExp'>+</a>
                                </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-12">
                                  <table class="table table-striped table-hover" id='experience-table'>
                                      <thead>
                                          <tr>
                                              <th>Job Held</th>
                                              <th>From</th>
                                              <th>To</th>
                                              <th>Pay Draws</th>
                                              <th></th>
                                          </tr>
                                      </thead>
                                      <tbody></tbody>
                                  </table>
                              </div>
                          </div>
                          </div>
                      </div>
                  </div>
                </div>  <!-- end of admission test row 2 -->

            </div>    <!-- end of admissionTest -->
          </div>

          <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['staff']['insert']; ?>'>
                      <i class="fa fa-save"></i>
                     Save</a>
                     <a href='' class="btn btn-default btnReset">
                      <i class="fa fa-refresh"></i>
                     Reset</a>
                  </div>
              </div>
            </div>
          </div>    <!-- end of a href='' row -->


        </form>   <!-- end of form -->

      </div>  <!-- end of col -->
    </div>  <!-- end of container fluid -->
  </div>   <!-- end of page_content -->
</div>

<div id="TypeModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Type</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Type</span>
	        <input type="text" class="form-control" id="txtNewType">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewType"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>

<div id="AgreementModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Agreement</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Agreement</span>
	        <input type="text" class="form-control" id="txtNewAgreement">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewAgreement"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>

<div id="ReligionModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Religion</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Religion</span>
	        <input type="text" class="form-control" id="txtNewReligion">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewReligion"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>

<div id="BankModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h3 id="myModalLabel">Add Bank</h3>
	    </div>
	    <div class="modal-body">

	      <div class="input-group">
	        <span class="input-group-addon">Bank</span>
	        <input type="text" class="form-control" id="txtNewBank">
	      </div>
	    </div>
	    <div class="modal-footer">
	      <div class="pull-right">
	        <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
	        <a class="btn btn-default btnNewBank"><i class="fa fa-plus"></i> Add</a>
	      </div>
	    </div>
	</div>
</div>