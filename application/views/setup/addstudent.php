<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

  <div class="page_bar">
    <div class="row">
      <div class="col-lg-6">
        <h1 class="page_title">Add Student</h1>
        <input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
        <input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
        <input type="hidden" class='address' value='<?php echo $this->session->userdata('name'); ?>'>
      </div>
      <div class="col-lg-6">
        <div class="pull-right">
          <a class="btn btn-success  btnSave" data-insertbtn='<?php echo $vouchers['student']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
          <a class="btn btn-warning  btnReset"><i class="fa fa-refresh"></i> Reset</a>
          <a class="btn btn-info  btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
          <a class="btn btn-info  btnCardPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Card Print</a> 

          <a href="#student-lookup" data-toggle="modal" class="btn btn-lg  btn-default btnsearchstudent "><i class="fa fa-search"></i>&nbsp;Student F1</a>                   

        </div>
      </div>
    </div>
  </div>

  <div class="page_content">
    <div class="container-fluid">

      <div class="col-md-12">

        <form action="">

          <ul class="nav nav-pills">
            <li class="active"><a href="#basicInformation" data-toggle="tab">Basic Information</a></li>
            <li><a href="#kinship" data-toggle="tab">Kinship</a></li>
            <li><a href="#familyBackground" data-toggle="tab">Family Background</a></li>
            <li><a href="#admissionTest" data-toggle="tab">Admission Test</a></li>
            <li><a href="#import" data-toggle="tab">Import</a></li>
            

          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="basicInformation">

              <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-body">


                      <div class="row">
                        <div class="col-lg-9">


                          <div class="row"></div>
                          <div class="row"></div>
                          <div class="row"></div>

                          <div class="row" style="margin-top:-20px;">
                            <form role="form" >
                              <div class="col-lg-2" >
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Id</label>
                                  <input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['student']['update']; ?>' id="txtStudentId">
                                  <input type="hidden" id="txtMaxStudentIdHidden">
                                  <input type="hidden" id="txtStudentIdHidden">
                                  <input type="hidden" id="txtPIdHidden">
                                </div>
                              </div>

                              <div class="col-lg-4">
                                <div class="form-group" style="margin-top:28px;">
                                  <span class="switch-addon">Is active?</span>
                                  <input type="checkbox" checked="" class="bs_switch" id="active">
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Admission&nbsp;Date</label>
                                  <input class="form-control ts_datepicker" type="text" id="txtAdmiDate">
                                </div>
                              </div>
                            </div>

                          </div>

                          <div class="col-lg-2" style="background: #F5F6F7;padding: 15px;border: 1px solid #ccc;box-shadow: 1px 1px 1px #000;">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="studentImageWrap">
                                  <img src="<?php echo base_url('assets/img/student.jpg'); ?>" alt="student" style="margin: auto;display: block;" id="studentImageDisplay">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-lg-12">
                                <input type="file" id="studentImage">
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>

                    </div>
                  </div>
                </div>
              </div>  <!-- end of row 1 of basic information -->

              <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#General" role="tab" data-toggle="tab">General Information</a></li>
                        <li><a href="#Contact" role="tab" data-toggle="tab">Contact Information</a></li>
                      </ul>
                    </div>

                    <div class="panel-body">
                      <div class="tab-content">
                        <div class="tab-pane fade in active" id="General">
                          <div class="row">
                            <div class="col-lg-8">
                              <form role="form">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Name</label>
                                      <input type="text" class="form-control" placeholder="Student name" id="txtStudentName"/>
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Father Name</label>
                                      <input type="text" class="form-control" placeholder="Father name" id="txtFatherName">
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Date of Birth</label>
                                      <input class="form-control ts_datepicker" type="text" id="txtDOB">
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Gender</label>
                                      <select class="form-control" id="gender_dropdown">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Roll No</label>
                                      <input type="text" class="form-control" placeholder="Student roll no" id="txtRollNo">
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Adms. In Class</label>
                                      <select class="form-control" id="admiClass_dropdown">
                                        <option value="" disabled="" selected="">Choose admission class</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Present Class</label>
                                      <select class="form-control" id="presentClass_dropdown">
                                        <option value="" disabled="" selected="">Choose present class</option>
                                      </select>
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Section</label>
                                      <select class="form-control" id="section_dropdown">
                                        <option value="" disabled="" selected="">Choose section</option>
                                        <?php foreach ($sections as $section): ?>
                                          <option value="<?php echo $section['secid']; ?>"><?php echo $section['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Category</label>
                                      <select class="form-control" id="feeCategory_dropdown">
                                        <option value="" disabled="" selected="">Choose Category</option>
                                        <?php foreach ($feeCategories as $feeCategory): ?>
                                          <option value="<?php echo $feeCategory['fcid']; ?>"><?php echo $feeCategory['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Fee</label>
                                      <select class="form-control" id="feeType_dropdown">
                                        <option value="" disabled="">Choose Fee</option>
                                        <option value="1/2 Fee Paying">1/2 Fee Paying</option>
                                        <option value="1/3 Fee Paying">1/3 Fee Paying</option>
                                        <option value="3/4 Fee Paying">3/4 Fee Paying</option>
                                        <option value="Full Fee Paying" selected="">Full Fee Paying</option>
                                        <option value="No Fee Paying">No Fee Paying</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Remarks</label>
                                      <input type="text" placeholder="Remarks about student" class="form-control" id="txtRemarks">
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="Contact">
                          <div class="row">
                            <div class="col-lg-8">
                              <form role="form">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Present Address</label>
                                      <input type="text" class="form-control" placeholder="Home address" id="txtAddress">
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Permanent Address</label>
                                      <input type="text" class="form-control" placeholder="Other address" id="txtbusinessAddress">
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Phone No</label>
                                      <input type="text" class="form-control num" placeholder="Phone no" id="txtPhoneNo">
                                    </div>
                                    <div class="col-lg-6">
                                      <label for="exampleInputEmail1">Mobile No</label>
                                      <input type="text" class="form-control num" placeholder="Mobile No" id="txtMobileNo">
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--tab content end -->
                    </div>
                  </div>
                </div>    <!-- end of general information -->
                <!-- end of contact information -->
              </div>  <!-- end of row general information -->
            </div>    <!-- end of basicInformation -->
            


            <div class="tab-pane" id="import">

              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <form role="form">
                        <div class="form-group">
                          <div class="row">

                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Adms. In Class</label>
                              <select class="form-control" id="imp_admiClass_dropdown">
                                <option value="" disabled="" selected="">Choose admission class</option>
                              </select>
                            </div>
                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Category</label>
                              <select class="form-control" id="impfeeCategory_dropdown">
                                <option value="" disabled="" selected="">Choose Category</option>
                                <?php foreach ($feeCategories as $feeCategory): ?>
                                  <option value="<?php echo $feeCategory['fcid']; ?>"><?php echo $feeCategory['name']; ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Fee</label>
                              <select class="form-control" id="imp_feeType_dropdown">
                                <option value="" disabled="" selected="">Choose Fee</option>
                                <option value="1/2 Fee Paying">1/2 Fee Paying</option>
                                <option value="1/3 Fee Paying">1/3 Fee Paying</option>
                                <option value="3/4 Fee Paying">3/4 Fee Paying</option>
                                <option value="Full Fee Paying" >Full Fee Paying</option>
                                <option value="No Fee Paying">No Fee Paying</option>
                              </select>
                            </div>
                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Present Class</label>
                              <select class="form-control" id="imp_presentClass_dropdown">
                                <option value="" disabled="" selected="">Choose present class</option>
                              </select>
                            </div>
                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Section</label>
                              <select class="form-control" id="imp_section_dropdown">
                                <option value="" disabled="" selected="">Choose section</option>
                              </select>
                            </div>
                          </form>
                          <div class="col-lg-2">

                            <form action="<?php echo site_url('Upload/do_upload');?> "  method="post"  enctype="multipart/form-data" id="upload_file" class="upload_files">
                              <label for="title">Title</label>
                              <input type="text" name="title" id="title" value="" />

                              <label for="userfile">File</label>
                              <input type="file" name="userfile" id="userfile" size="20" />

                              <input type="submit" style="margin-top: 16px;border-radius: 5px;" class="btn btn-success" name="submit" id="submit" />
                            </form>
                          </div>

                        </div>
                      </div>
                      <form role="form">
                        <div class="form-group">

                        </div>


                        <div class="form-group">
                          <div class="row">
                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Name</label>
                              <select class="form-control" id="drp_Name">
                                <option value="" disabled="" selected="">Choose admission class</option>

                              </select>
                            </div>
                            <div class="col-lg-2">
                              <label for="exampleInputEmail1">Father Name</label>
                              <select class="form-control" id="drp_FatherName">
                               <option value="" disabled="" selected="">Choose admission class</option>

                             </select>
                           </div>
                           <div class="col-lg-2">
                            <label for="exampleInputEmail1">Date of Birth</label>
                            <select class="form-control" id="drp_Dob">
                             <option value="" disabled="" selected="">Choose DOB Colum</option>

                           </select>
                         </div>
                         <div class="col-lg-2">
                          <label for="exampleInputEmail1">Gender</label>
                          <select class="form-control" id="drp_gender">
                            <option value="" disabled="" selected="">Choose Gender Colum</option>

                          </select>
                        </div>
                        <div class="col-lg-2">
                          <label for="exampleInputEmail1">Roll No</label>
                          <select class="form-control" id="drp_RollNo">
                            <option value="" disabled="" selected="">Choose Roll No Colum</option>

                          </select>
                        </div>
                        <div class="col-lg-2">
                          <label for="exampleInputEmail1">Remarks</label>
                          <select class="form-control" id="drp_Remarks">
                            <option value="" disabled="" selected="">Choose Remarks Colum</option>

                          </select>
                        </div>

                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-2">
                          <label for="exampleInputEmail1">Present Address</label>
                          <select class="form-control" id="drp_presentAddress">
                            <option value="" disabled="" selected="">Choose Present Address Colum</option>
                          </select>
                        </div>
                        <div class="col-lg-2">
                          <label for="exampleInputEmail1">Permanent Address</label>
                          <select class="form-control" id="drp_permanentAddress">
                            <option value="" disabled="" selected="">Choose Permanent Address Colum</option>
                          </select>
                        </div>
                        <div class="col-lg-2">
                          <label for="exampleInputEmail1">Phone No</label>
                          <select class="form-control" id="drp_phoneNo">
                            <option value="" disabled="" selected="">Choose Phone NO Colum</option>
                          </select>
                        </div>
                        <div class="col-lg-2">
                          <label for="exampleInputEmail1">Mobile No</label>
                          <select class="form-control" id="drp_mobileNo">
                            <option value="" disabled="" selected="">Choose  Mobile NO Colum</option>
                          </select>
                        </div>
                        <div class="col-lg-2">
                         <input type="button" value="Import" style="margin-top: 25px;border-radius: 5px;" class="btn btn-success" name="" id="btnSaveExcel">
                       </div>
                       <!-- </div> -->


                     </div>
                   </div>
                   <!-- </div> -->

                 </form>

                 <div class="row">
                  <div class="col-lg-12 " style="">
                    <div id="container"></div>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>     <!-- end of import -->

      <div class="tab-pane" id="familyBackground">

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-8">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <label>Father Occupation</label>
                      <input type="text" class="form-control" id="txtFatherOccupation">
                    </div>
                    <div class="col-lg-6">
                      <label>Father's CNIC</label>
                      <input type="text" class="form-control num" placeholder="CNIC of father" id="txtFatherCNIC">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-6">
                      <label>Mother</label>
                      <input type="text" class="form-control" placeholder="Mother Name" id="txtMotherName">
                    </div>
                    <div class="col-lg-6">
                      <label>Mother Tongue</label>
                      <input type="text" class="form-control" placeholder="Mother tongue" id="txtMotherTongue">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="input-group fullWidth">
                  <textarea name="" class="form-control" rows="5" placeholder="If child lives with one parent only, then please indicate the circumstances" id="txtParent"></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="input-group fullWidth">
                  <textarea name="" class="form-control" rows="5" placeholder="Other children in family (name, age, parent school/college)" id="txtChildren"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>     <!-- end of familyBackground -->

      <div class="tab-pane" id="kinship">

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-2">
                          <label>Stdid</label>
                          <select class="form-control select2" id="kinStdid_dropdown">
                            <option value="" disabled="" selected="">Choose stdid</option>
                            <?php foreach ($students as $student): ?>
                              <option value="<?php echo $student['stdid']; ?>"><?php echo $student['stdid']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                        <div class="col-lg-4">
                          <label>Name</label>
                          <select class="form-control select2" id="kinName_dropdown">
                            <option value="" disabled="" selected="">Choose Name</option>
                            <?php foreach ($students as $student): ?>
                              <option value="<?php echo $student['stdid']; ?>"><?php echo $student['student_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                        <div class="col-lg-3">
                          <label>Relation</label>
                          <input type="text" class="form-control" id="txtKinRelation">
                        </div>
                        <div class="col-lg-2">

                          <label for="">Add</label>                    
                          <a href="" class="btn btn-primary" id="btnAddKinshipToTable"><i class="fa fa-arrow-circle-down"></i></a>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <table class="table table-striped" id="kinship_table">
                      <thead>
                        <tr>
                          <th>Stdid</th>
                          <th>Name</th>
                          <th>Relation</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>  <!-- end of admission test row 2 -->

      </div>    <!-- end of admissionTest -->

      <!-- <iframe src="../../../assets/Applicants.xlsx" width="100%" height="500"></iframe> -->
      <div class="tab-pane" id="admissionTest">

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-10">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-4">
                          <label>Previous School</label>
                          <select class="form-control" id="previousSchool_dropdown">
                            <option value="" disabled="" selected="">Choose school</option>
                            <?php foreach ($previousSchools as $previousSchool): ?>
                              <option value="<?php echo $previousSchool['pschool']; ?>"><?php echo $previousSchool['pschool']; ?></option>
                            <?php endforeach ?>
                          </select>


                        </div>
                        <div class="col-lg-1" style="margin-top:25px;margin-left:-15px; padding: 5px 5px 5px 5px;">
                          <a href="#PreviousSchoolModel" data-toggle="modal" class="btn btn-primary">+</a>
                        </div>
                        <div class="col-lg-4">
                          <label>Test for Class</label>
                          <select class="form-control" id="testforclass_dropdown">
                            <option value="" disabled="" selected="">Choose test class</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>  <!-- end of admission test row 1 -->

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body">
                            <!-- <div class="row">
                              <div class="col-lg-5">
                                <div class="input-group">
                                  <span class="input-group-addon">Choose Subject</span>
                                  <select class="form-control" id="testsubject_dropdown">
                                      <option value="" disabled="" selected="">Choose subject</option>
                                      <?php foreach ($subjects as $subject): ?>
                                          <option value="<?php echo $subject['name']; ?>" data-sbid="<?php echo $subject['sbid']; ?>"><?php echo $subject['name']; ?></option>
                                      <?php endforeach ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="input-group">
                                  <span class="input-group-addon">Marks</span>
                                  <input type="text" class="form-control num" id="txtTestSubjectMarks">
                                </div>
                              </div>
                              <div class="col-lg-2">
                                <div class="input-group">
                                  <a href="" class="btn btn-primary" id="btnAddTestSubjToTable">+</a>
                                </div>
                              </div>
                            </div> -->
                            <div class="row">
                              <div class="col-lg-12">
                                <table class="table table-striped" id="testsubjects_table">
                                  <thead>
                                    <tr>
                                      <th>Subject</th>
                                      <th>Marks</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>English</td><td><input type="text" id='EnglishMarks' value="" class="table-input num"></td>
                                    </tr>
                                    <tr>
                                      <td>Math</td><td><input type="text" id='MathMarks' value="" class="table-input num"></td>
                                    </tr>
                                    <tr>
                                      <td>Science</td><td><input type="text" id='ScienceMarks' value="" class="table-input num"></td>
                                    </tr>
                                    <tr>
                                      <td>Social Studies</td><td><input type="text" id='SocialStudiesMarks' value="" class="table-input num"></td>
                                    </tr>
                                    <tr>
                                      <td>General Knowledge</td><td><input type="text" id='GeneralKnowledgeMarks' value="" class="table-input num"></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>

                            <div class="row"></div>
                            <div class="row">
                              <div class="col-lg-8">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-4">
                                     <label>Total Marks</label>
                                     <input type="text" class="form-control num" id="txtTotalMarks" value='100' disabled="disabled">
                                   </div>
                                   <div class="col-lg-4">
                                    <label>Obtained Marks</label>
                                    <input type="text" class="form-control num" disabled="disabled" id="txtObtainedMarks" value="0">
                                  </div>
                                  <div class="col-lg-4">
                                    <label>Status</label>
                                    <input type="text" class="form-control" disabled="disabled" id="txtStatusResult">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="input-group fullWidth">
                                <textarea name="" class="form-control" rows="5" placeholder="Remarks about test" id="txtRemarksTest"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>  <!-- end of admission test row 2 -->


                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label>Admission</label>
                        <select class="form-control" id="allowed_dropdown">
                          <option value="Admission Granted">Admission Granted</option>
                          <option value="Admission Disallowed">Admission Disallowed</option>
                        </select>
                      </div>
                    </div>
                    <!-- <div class="col-lg-6">
                        <div class="input-group">
                            <span class="input-group-addon">Allowed for Class</span>
                            <select class="form-control" id="allowedforclass_dropdown">
                                <option value="" disabled="" selected="">Choose allowed class</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group">
                            <span class="input-group-addon">Section</span>
                            <select class="form-control" id="testforsection_dropdown">
                                <option value="" disabled="" selected="">Choose section</option>
                            </select>
                        </div>
                      </div> -->
                    </div>
                  </div>    <!-- end of admissionTest -->
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="panel panel-default">
                      <div class="panel-body">
                        <a class="btn btn-success  btnSave" data-insertbtn='<?php echo $vouchers['student']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning  btnReset"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-info  btnPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print"></i> Print</a>
                        <a class="btn btn-default  btnCardPrint" data-printtbtn='<?php echo $vouchers['student']['print']; ?>'><i class="fa fa-print "></i> Card Print</a>
                        

                        <a href="#student-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchstudent "><i class="fa fa-search"></i>&nbsp;Student F1</a>                    

                      </div>
                    </div>
                  </div>
                </div>    <!-- end of button row -->


              </form>   <!-- end of form -->

            </div>  <!-- end of col -->
          </div>  <!-- end of container fluid -->
        </div>   <!-- end of page_content -->
      </div>

      <div id="student-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h3 id="myModalLabel">Student Lookup</h3>
            </div>

            <div class="modal-body">
              <table class="table table-striped modal-table" id ="tblStudents">

                <thead>
                  <tr style="font-size:16px;">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th>Class</th>
                    <th>Section</th>
                    <th style='width:3px;'>Actions</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <div class="modal-footer">

              <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>

      <div id="PreviousSchoolModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="model-contentwrapper">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Add Previous School</h3>
          </div>
          <div class="modal-body">

            <div class="input-group">
              <span class="input-group-addon">Previous School</span>
              <input type="text" class="form-control" id="txtNewPreviousSchool">
            </div>
          </div>
          <div class="modal-footer">
            <div class="pull-right">
              <a class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
              <a class="btn btn-default btnNewPreviousSchool"><i class="fa fa-plus"></i> Add</a>
            </div>
          </div>
        </div>
      </div>