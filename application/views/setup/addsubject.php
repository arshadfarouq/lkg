<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">
	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-11">
						<h1 class="page_title">Subject</h1>
					</div>
					<div class="col-lg-1">
						<!-- Button trigger modal -->
						<button class="btn btn-primary btn-openmodal btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">Add</button>
					</div>
				</div>
				
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
		</div>
	</div>
	<div class="page_content">
		<div class="container-fluid">
			<div class="col-md-12">
			
			<!--.......................starting of tabel.........................-->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<table class="table table-striped table-hover ar-datatable">
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Name</th>
										<th>Description</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php $counter = 1; foreach ($subjects as $subject): ?>
										<tr>														
											<td><?php echo $counter++; ?></td>
											<td><?php echo $subject['name']; ?></td>
											<td><?php echo $subject['description']; ?></td>
											<td><a href="" class="btn btn-primary btn-edit-subject showallupdatebtn"  data-showallupdatebtn='<?php echo $vouchers['subject']['update']; ?>' data-sbid="<?php echo $subject['sbid']; ?>"><span class="fa fa-edit"></span></a></td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!--model-->
			<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
				    <div class="modal-content">
					   <div class="modal-header">
					      <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
	    					<h4 class="modal-title" id="myModalLabel">Add/Update Subject</h4>
	  					</div>
	  					<div class="modal-body">
	    					<div class="container-fluid">
			                    <div class="col-lg-11 col-lg-offset-1">

			                        <div class="row">
				                        	<form role="form ">
				                        	<div class="form-group">
				                        		<div class="row">
				                        			<div class="col-lg-2">
				                        				<label>Subject Id</label>
				                        				<input type="number" id="txtSubjectId" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['subject']['update']; ?>'>
														<input type="hidden" id="txtMaxSubjectIdHidden">
														<input type="hidden" id="txtSubjectIdHidden">
				                        			</div>
				                        		</div>
				                        	</div><!--form-group-->

				                        	<div class="form-group">
				                        		<div class="row">
				                        			<div class="col-lg-8">
				                        				<label>Name</label>
				                        				<input type="text" class="form-control" id="txtName" placeholder="Subject name">
														<input type="hidden" id="txtNameHidden">
				                        			</div>
				                        		</div>
				                        	</div><!--form-group-->

				                        	<div class="form-group">
				                        		<div class="row">
				                        			<div class="col-lg-8">
				                        				<label>Description</label>
				                        				<input type="text" class="form-control" placeholder="Subject description" id="description">
				                        			</div>
				                        		</div>
				                        	</div><!--form-group-->

				                        		<div class="row hide">
																			<div class="col-lg-2">
																			</div>
																			<div class="col-lg-4">
																				<select class="form-control" id="allSubjects">
																					<?php foreach ($subjects as $subject): ?>
																						<option><?php echo $subject['name']; ?></option>
																					<?php endforeach ?>
																				</select>
																			</div>
																		</div>
				                        	</form>  
			                        </div><!--end of row-->

			                    </div><!--end of col-lg-11-->
					        </div><!--container-fluid-->
					    </div>
						<div class="modal-footer">
	        				<div class="pull-right">
								<a class="btn btnSave btn-success btn-lg" data-insertbtn='<?php echo $vouchers['subject']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
								<a class="btn btnReset btn-warning btn-lg"><i class="fa fa-refresh"></i> Reset</a>
								<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
							</div>
      					</div><!--modal-footer-->
					</div><!--modal-content-->
  				</div><!--modal-dialogue-->	
			</div><!--modal-->
			<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<form action="">
									<div class="row">
										<div class="col-lg-6">
											<div class="pull-right">
												<a class="btn btn-default btnSave btn-success btn-lg" data-insertbtn='<?php echo $vouchers['subject']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
												<a class="btn btn-default btnReset btn-warning btn-lg"><i class="fa fa-refresh"></i> Reset</a>
											</div>
										</div> 	<!-- end of col -->
									</div>	<!-- end of row -->
								</form>	<!-- end of form -->
							</div>	<!-- end of panel-body -->
						</div>	<!-- end of panel -->
					</div>  <!-- end of col -->
			</div>	<!-- end of row -->
			</div><!-- end of col -->
		</div><!-- container-fluid -->
	</div><!-- page-content -->
</div><!-- main wrapper -->
