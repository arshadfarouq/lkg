<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-6">
				<h1 class="page_title">Assign Subject to Class</h1>
				<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
			</div>
			<div class="col-lg-6">
				<div class="pull-right">
					<a href='' class="btn btn-lg btn-success btnSave" data-insertbtn='<?php echo $vouchers['assign_subject_to_class']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a href='' class="btn btn-lg btn-warning btnDelete" data-deletetbtn='<?php echo $vouchers['assign_subject_to_class']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					<a href='' class="btn btn-lg btn-danger btnReset"><i class="fa fa-refresh"></i> Reset</a>
				</div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">

									<form action="">

										<div class="row">
											<div class="col-lg-1">
												<label for="">Id</label>
												<input type="number" id="txtdcno" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['assign_subject_to_class']['update']; ?>' />
												<input type="hidden" id="txtMaxdcnoHidden"/>
												<input type="hidden" id="txtdcnoHidden"/>
											</div>
											<div class="col-lg-3">
        										<label for="">Class</label>
        										<select class="form-control" id="class_dropdown">
				                                    <option value="" disabled="" selected="">Choose class</option>
				                                </select>
											</div>
										</div>

										<div class="row"></div>
										<div class="container-wrap">
											<div class="row">
					                            <div class="col-lg-4">
				                                  	<label for="">Subject</label>
				                                  	<select class="form-control" id="subjects_dropdown">
				                                      	<option value="" disabled="" selected="">Choose subject</option>
				                                      	<?php foreach ($subjects as $subject): ?>
				                                          	<option value="<?php echo $subject['sbid']; ?>" data-sbid="<?php echo $subject['sbid']; ?>"><?php echo $subject['name']; ?></option>
				                                      	<?php endforeach ?>
				                                  	</select>
					                            </div>
						                        <div class="col-lg-1" style="margin-top: 26px;">
													<a href="" class="btn btn-primary" id="btnAddSubject">+</a>
						                        </div>
					                    	</div>
										</div>
										<div class="row"></div>


										<div class="row">
				                            <div class="col-lg-12">
				                                <table class="table table-striped" id="subjects_table">
				                                    <thead>
				                                        <tr>
				                                            <th>Subject Name</th>
				                                            <th>Action</th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>

				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>

										<div class="row">
											<div class="col-lg-12">
												<div class="pull-right">
													<a href='' class="btn btn-lg btn-success btnSave" data-insertbtn='<?php echo $vouchers['assign_subject_to_class']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
													<a href='' class="btn btn-lg btn-warning btnDelete" data-deletetbtn='<?php echo $vouchers['assign_subject_to_class']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
													<a href='' class="btn btn-lg btn-danger btnReset"><i class="fa fa-refresh"></i> Reset</a>
												</div>
											</div> 	<!-- end of col -->
										</div>	<!-- end of row -->


									</form>	<!-- end of form -->

								</div>	<!-- end of panel-body -->
							</div>	<!-- end of panel -->
						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>