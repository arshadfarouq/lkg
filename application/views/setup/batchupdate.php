<!-- main content -->
<div id="main_wrapper">

  	<div class="page_bar">
    	<div class="row">
      	<div class="col-md-12">
        	<h1 class="page_title">Batch Update</h1>
          <input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
          <input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
      	</div>
    	</div>
  	</div>

  	<div class="page_content">
    	<div class="container-fluid">

			<div class="row">
      			<div class="col-lg-12">

        			<form action="">
            			<div class="row">
            				<div class="col-lg-12">
            					<div class="panel panel-default">
            						<div class="panel-body">
            							<div class="row">
            								<div class="col-lg-3">
            										<label for="">Class</label>
            										<select class="form-control" id="class_dropdown">
                                    <option value="" disabled="" selected="">Choose class</option>
                                </select>
            								</div>
            								<div class="col-lg-3">
            										<label for="">Section</label>
            										<select class="form-control" id="section_dropdown">
                                    <option value="" disabled="" selected="">Choose section</option>
                                </select>
            								</div>
            								<div class="col-lg-6" style='margin-top: 20px;'>

                              <div class="pull-right">
                									<a href="" class="btn btn-lg btn-primary btnSearch">
        				                      	<i class="fa fa-search"></i>
        				                    Search</a>
        				                     <a href="" class="btn btn-lg btn-warning btnReset">
        				                      	<i class="fa fa-refresh"></i>
        				                    Reset</a>
                              </div>

            								</div>
                          </div>  <!-- end of row -->

            						</div>	<!-- end of panel body -->
            					</div>  <!-- end of panel body -->
            				</div>
            			</div>
	        		</form>   <!-- end of form -->

    	  		</div>  <!-- end of col -->
      		</div>

      		<div class="row">
      			<div class="col-lg-12">
      				<div class="panel panel-default">
      					<div class="panel-body">
      						<table class="table table-hover table-striped ar-datatable" id="studentdata-table">
      							<thead>
      								<tr>
      									<th>Sr#</th>
      									<th>Stdid</th>
      									<th>Name</th>
      									<th>Father Name</th>
      									<th>Roll #</th>
      									<th>Cell #</th>
      									<th></th>
      								</tr>
      							</thead>
      							<tbody>
      							</tbody>
      						</table>
      					</div>
      				</div>
      			</div>
      		</div>
    	</div>  <!-- end of container fluid -->
  	</div>   <!-- end of page_content -->
</div>