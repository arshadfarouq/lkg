<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

  	<div class="page_bar">
    	<div class="row">
      		<div class="col-md-9">
        		<h1 class="page_title">Struck off / Readmit Student</h1>
        		<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
      		</div>
      	</div>
 	</div>

  	<div class="page_content">
    	<div class="container-fluid">

      		<div class="col-md-12">

        		<form action="">

        			<div class="tab-content">

			            <div class="tab-pane active fade in" id="basicInformation">

				            <div class="row">
				                <div class="col-lg-12">
				                  	<div class="panel panel-default">
				                  		<div class="panel-body">

					                        <div class="row">
						                        <div class="col-lg-2">
					                                <select class="form-control"  id="sr_dropdown">
					                                	<option value="struckoff">Struck off</option>
					                                	<option value="readmit">Readmit</option>
					                                </select>
						                        </div>
						                    </div>

						                    <div class="row">
						                        <div class="col-lg-3">
						                            <div class="form-group">
						                                <label>Date</label>
						                                <input class="form-control ts_datepicker" type="text" id="current_date">
						                            </div>
						                        </div>
						                        <div class="col-lg-3">
						                            <div class="form-group">
						                              	<label>Admission #</label>
				                            			<select class="form-control"  id="stdid_dropdown">
						                                	<option value="" disabled="" selected="">Choose Admission Id</option>
				                                          	<?php foreach ($students as $student): ?>
				                                              	<option value="<?php echo $student['stdid']; ?>" data-name='<?php echo $student['student_name']; ?>'><?php echo $student['stdid']; ?></option>
				                                          	<?php endforeach ?>
						                                </select>
						                            </div>
						                        </div>
						                        <div class="col-lg-2" style='margin-top:30px;'>
						                        	<span id='txtName'>No Student selected</span>
						                        </div>
						                    </div>
						                    <div class="row">
						                        <div class="col-lg-8">
				                            		<textarea class="form-control" id="txtReason" placeholder='Struck off/Readmission reason'></textarea>
						                        </div>
						                    </div>

						                    <div class="row">
									            <div class="col-lg-12">
							                    	<a class="btn btn-primary btn-lg btnSave" data-insertbtn='<?php echo $vouchers['Struckoff_Readmission']['insert']; ?>'><i class="fa fa-save"></i> Save</a>
							                    	<a class="btn btn-warning  btn-lg btnReset"><i class="fa fa-refresh"></i> Reset</a>
									            </div>
									        </div>    <!-- end of button row -->

				                  		</div>
				                	</div>
				           		</div>  <!-- end of row 1 of basic information -->
			            	</div>

          				</div>
          			</div>
        		</form>   <!-- end of form -->

      		</div>  <!-- end of col -->
    	</div>  <!-- end of container fluid -->
  	</div>   <!-- end of page_content -->
</div>