<div id="main_wrapper">

  	<div class="page_bar">
    	<div class="row">
      		<div class="col-md-12">
        		<h1 class="page_title">Promote Students</h1>
        		<input type="hidden" class='brid' value='<?php echo $this->session->userdata('brid'); ?>'>
				<input type="hidden" class='uid' value='<?php echo $this->session->userdata('uid'); ?>'>
      		</div>
    	</div>  
  	</div>

  	<div class="page_content">
    	<div class="container-fluid">

    		<div class="row">
    			<div class="col-lg-12">
							
					<div class="row">
						<div class="col-lg-6">
									
									<div class="row">
										<div class="col-lg-12">
											

											<div class="row">
												<div class="col-lg-12">
												
													<div class="panel panel-default">
														<div class="panel-heading">From</div>
														<div class="panel-body">
													
													<div class="row">
														<div class="col-lg-6">
															<label for="">Class</label>
															<select class="form-control" id="fromClass_dropdown">
							                                    <option value="" disabled="" selected="">Choose class</option>
							                                </select>
														</div>
														<div class="col-lg-6">
															<label for="">Section</label>
															<select class="form-control" id="fromSection_dropdown">
							                                    <option value="" disabled="" selected="">Choose section</option>
							                                </select>
														</div>
													</div>

													<div class="row">
														<div class="col-lg-12">
															<a href='' class="btn btn-default btnSearch">
				                      							<i class="fa fa-search"></i>
				                    						Search</a>
				                    						<a href='' class="btn btn-default btnPromoteAll">
				                      							<i class="ion-arrow-graph-up-right"></i>
				                    						Promote All</a>
				                    						<a href="" class="btn btn-default btnFromReset">
					    				                      	<i class="fa fa-refresh"></i>
					    				                    Reset</a>
														</div>
													</div>
														</div>		<!-- end of panel body -->
													</div>	<!-- end of panel -->

												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													
													<div class="panel panel-default">
														<div class="panel-body">
															
															<table class="table table-hover table-striped fromStudentData-table">
																<thead>
																	<th>Sr#</th>
																	<th>Roll #</th>
																	<th>Name</th>
																	<th>Category</th>
																	<th>Fee Type</th>
																	<th></th>
																</thead>

																<tbody>																	
																	
																</tbody>		<!-- end of tbody -->

															</table>		<!-- end of table -->

														</div>		<!-- end of panel body -->
													</div>	<!-- end of panel -->

												</div>
											</div>

										</div>	<!-- end of col-lg-12 -->
									</div>	<!-- end of row -->

						</div>		<!-- end of left panel -->

						<div class="col-lg-6">

							<div class="row">
								<div class="col-lg-12">									

									<div class="row">
										<div class="col-lg-12">
										
											<div class="panel panel-default">
												<div class="panel-heading">To</div>
												<div class="panel-body">
											
											<div class="row">
												<div class="col-lg-6">
													<label for="">Class</label>
													<select class="form-control" id="toClass_dropdown">
					                                    <option value="" disabled="" selected="">Choose class</option>
					                                </select>
												</div>
												<div class="col-lg-6">
													<label for="">Section</label>
													<select class="form-control" id="toSection_dropdown">
					                                    <option value="" disabled="" selected="">Choose section</option>
					                                </select>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<a href='' class="btn btn-default btnUpdate">
		                      							<i class="fa fa-pencil"></i>
		                    						Update</a>
		                    						<a href='' class="btn btn-default btnDemoteAll">
		                      							<i class="ion-arrow-graph-down-left"></i>
		                    						Demote All</a>
		                    						<a href="" class="btn btn-default btnToReset">
			    				                      	<i class="fa fa-refresh"></i>
			    				                    Reset</a>
												</div>
											</div>
												</div>		<!-- end of panel body -->
											</div>	<!-- end of panel -->

										</div>
									</div>

									<div class="row">
										<div class="col-lg-12">
											
											<div class="panel panel-default">
												<div class="panel-body">
													
													<table class="table table-hover table-striped toStudentData-table">
														<thead>
															<th>Sr#</th>
															<th>Roll #</th>
															<th>Name</th>
															<th>Category</th>
															<th>Fee Type</th>
															<th></th>
														</thead>

														<tbody>

														</tbody>		<!-- end of tbody -->

													</table>		<!-- end of table -->

												</div>		<!-- end of panel body -->
											</div>	<!-- end of panel -->

										</div>
									</div>

								</div>	<!-- end of col-lg-12 -->
							</div>	<!-- end of row -->
									
						</div>		<!-- end of right panel -->
					</div>

    			</div>	<!-- end of col-lg-12 -->
    		</div>	<!-- end of row -->

    	</div>	<!-- end of container-fluid -->
    </div>	<!-- end of page_content -->
</div>