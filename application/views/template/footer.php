		<div class="loader hide">
			<img src="<?php echo base_url('assets/img/loader.gif') ?>" alt="">
			<span>Please Wait...</span>
		</div>

		<!-- jQuery -->
		<script src=" <?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
		<script src=" <?php echo base_url('assets/js/shortcut.js'); ?>"></script>
		<script src=" <?php echo base_url('assets/js/ajaxfileupload.js'); ?>"></script>

		<!-- easing -->
		<script src=" <?php echo base_url('assets/js/jquery.easing.1.3.min.js'); ?>"></script>
		<!-- bootstrap js plugins -->
		<script src=" <?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
		<!-- top dropdown navigation -->
		<!--<script src=" <?php /*echo base_url('assets/js/tinynav.min.js');*/ ?>"></script>-->
		<!-- perfect scrollbar -->
		<script src=" <?php echo base_url('assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js'); ?>"></script>

		<!-- common functions -->
		<script src=" <?php echo base_url('assets/js/tisa_common.js'); ?>"></script>

		<!-- datatables -->
		<script src=" <?php echo base_url('assets/lib/DataTables/media/js/jquery.dataTables.min.js'); ?>"></script>
		<script src=" <?php echo base_url('assets/lib/DataTables/media/js/dataTables.bootstrap.js'); ?>"></script>
		<!--<script src=" <?php /*echo base_url('assets/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js');*/ ?>"></script>-->
		<!--<script src=" <?php /*echo base_url('assets/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js');*/ ?>"></script>-->
		<!--<script src=" <?php /*echo base_url('assets/js/apps/tisa_datatables.js');*/ ?>"></script>-->

		<!-- style switcher -->
		<!--<script src=" <?php /*echo base_url('assets/js/tisa_style_switcher.js');*/ ?>"></script>-->

		<!-- page specific plugins -->

		<!--  timepicker -->
		<script src="<?php echo base_url('assets/lib/bootstrap-timepicker/js/bootstrap-timepicker.js') ?>"></script>
		<!--  datepicker -->
		<script src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
		<!--  bootstrap switches -->
		<script src="<?php echo base_url('assets/lib/bootstrap-switch/build/js/bootstrap-switch.min.js'); ?>"></script>
		<!-- multiselect, tagging -->
		<script src="<?php echo base_url('assets/lib/select2/select2.min.js'); ?>"></script>
		<!-- textarea autosize -->
		<!--<script src="<?php /*echo base_url('assets/lib/autosize/jquery.autosize.min.js');*/ ?>"></script>-->
		<!-- handlebar.js  -->
		<script src="<?php echo base_url('assets/js/handlebars.js'); ?>"></script>

		<!-- form extended elements functions -->
		<!--<script src="<?php /*echo base_url('assets/js/apps/tisa_extended_elements.js');*/ ?>"></script>-->

		<!-- custom javascript -->
		<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/app_modules/general.js'); ?>"></script>
		<!-- CVS -->
		<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>

		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-36251023-1']);
			_gaq.push(['_setDomainName', 'jqueryscript.net']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>
				<!-- add js -->
		<script src="<?php echo base_url('assets/lib/autocomplete/jquery.auto-complete.js'); ?>"></script>
		<script src="<?php echo base_url('assets/lib/confirm-js/jquery-confirm.min.js'); ?>"></script>

		<?php if (isset($modules)): ?>
			<?php foreach ($modules as $module): ?>
				<script src="<?php echo base_url('assets/js/app_modules/' . $module . '.js'); ?>"></script>
			<?php endforeach; ?>
		<?php endif; ?>


	</body>
	</html>
