<?php

$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo isset($page_title)? $page_title :'Al Nahar Solution';?></title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

	<link rel="shortcut icon" href="<?php echo base_url('assets/img/header-logo mainnn.png'); ?>">

	<!-- bootstrap framework -->
	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">

	<!-- custom icons -->
	<!-- font awesome icons -->
	<link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" media="screen">
	<!-- <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" media="screen"> -->
	<!-- ionicons -->
	<link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet" media="screen">


	<!-- datatables -->
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/DataTables/media/css/jquery.dataTables.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css'); ?>">
	<!-- flags -->
	<link rel="stylesheet" href="<?php echo base_url('assets/icons/flags/flags.css'); ?>">
	<!-- datepicker -->
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker3.css'); ?>">
	<!-- bootstrap switches -->
	<link href="<?php echo base_url('assets/lib/bootstrap-switch/build/css/bootstrap3/bootstrap-switch.css'); ?>" rel="stylesheet">
	<!-- multiselect, tagging -->
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/select2/select2.css'); ?>">
	<!-- notifiiton -->
	<link href="<?php echo base_url('assets/css/f_clone_Notify.css'); ?>" rel="stylesheet" media="screen">
	<!-- end -->
	<!-- main stylesheet -->


	<link href="<?php echo base_url('assets/css/style_22.css'); ?>" rel="stylesheet" media="screen">
	<link href="<?php echo base_url('assets/css/sweet-alert.css'); ?>" rel="stylesheet" media="screen">
	<!-- custom css -->
	<link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/morris.css'); ?>">
	
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/autocomplete/jquery.auto-complete.css'); ?>">
	<link href="<?php echo base_url('assets/css/jquery-confirm.min.css'); ?>" rel="stylesheet" media="screen">

	<script>
		var base_url = '<?php echo base_url(); ?>';
	</script>

</head>
<body>  
	<div class="loader">
		<div class="loader_overlay"></div>
		<img class="loader_img" src="<?php echo base_url('assets/img/ajaxloader.png'); ?>" alt="">
	</div>  	
	<!-- top bar -->
	<header class="navbar navbar-fixed-top" role="banner"><!-- navbar-fixed-top -->
		
			<div class="container-fluid">
				<div class="navbar-header">
				<span style="margin: 0px 10px !important;">
						Welcome <span><?php echo ( $this->session->userdata('fullname') ) ? $this->session->userdata('fullname') : $this->session->userdata('uname'); ?></span>!
						<?php if ($this->session->userdata('name')): ?>
							You are operating through <span><?php echo $this->session->userdata('name'); ?></span>! 
							<?php if ($this->session->userdata('address')): ?>
								<span> Adress: <?php echo $this->session->userdata('address'); ?></span>
							<?php endif ?>
						<?php endif ?>
					</span>
					
					<a href="<?php echo base_url('index.php/'); ?>" class="navbar-brand"><img src="<?php echo base_url('assets/img/header-logo.png'); ?>" alt="Logo"></a>
				</div>
				<ul class="nav navbar-nav navbar-right" style='margin-top: 6px;'>
					<li><a href="<?php echo base_url('index.php/user/logout') ?>"><span class="navbar_el_icon ion-log-out"></span> <span class="navbar_el_title">Logout</span></a></li>
				</ul>
			</div>
		
	</header>