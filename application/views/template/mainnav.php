<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
$reports = $desc['reports'];
?>
<!-- side navigation -->
<nav id="side_nav">
	<ul>
		
		<li class='voucher-container showon-hover'>
			<a href="#">
				<span class="ion-ios7-cog"></span>
				<span class="nav_title">Setup</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">
					<?php if (isset($vouchers['branch']['branch']) == 1 || isset($vouchers['class']['class']) == 1 || isset($vouchers['section']['section']) == 1 || isset($vouchers['fee_category']['fee_category']) == 1 || isset($vouchers['charges']['charges']) == 1 || isset($vouchers['staff']['staff']) == 1 || isset($vouchers['previllages']['previllages']) == 1 || isset($vouchers['add_new_user']['add_new_user']) == 1): ?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<h4 class="panel_heading panlhed">Add New</h4>
										</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<ul>
											<?php if (isset($vouchers['branch']['branch']) == 1 && $this->session->userdata('type') == 'Super Admin'): ?>
												<li class='voucher branch'> <a href="<?php echo base_url('index.php/branch/add'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Branch</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['class']['class']) == 1): ?>
												<li class='voucher class'> <a href="<?php echo base_url('index.php/group/addClass'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Class</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['section']['section']) == 1): ?>
												<li class='voucher section'> <a href="<?php echo base_url('index.php/group/addSection'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Section</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['fee_category']['fee_category']) == 1): ?>
												<li class='voucher fee_category'> <a href="<?php echo base_url('index.php/fee/add'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Fee Category</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['charges']['charges']) == 1): ?>
												<li class='voucher charges'> <a href="<?php echo base_url('index.php/charge/add'); ?>"><span class="side_icon ion-ios7-pricetag-outline"></span> Charges</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['staff']['staff']) == 1): ?>
												<li class='voucher staff'> <a href="<?php echo base_url('index.php/staff/add') ?>"><span class="side_icon ion-ios7-person-outline"></span> Staff</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['previllages']['previllages']) == 1 && $this->session->userdata('type') == 'Super Admin'): ?>
												<li class='voucher previllages'> <a href="<?php echo base_url('index.php/user/privillages'); ?>"><span class="side-icon fa fa-wrench"></span> Previllages</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['add_new_user']['add_new_user']) == 1 && $this->session->userdata('type') == 'Super Admin'): ?>
												<li class='voucher add_new_user'> <a href="<?php echo base_url('index.php/user/add'); ?>"><span class="side-icon fa fa-wrench"></span> Add New User</a> </li>
											<?php endif ?>
										</ul>
									<?php endif ?>
									<?php if (isset($vouchers['level']['level']) == 1 || isset($vouchers['account']['account']) == 1): ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h4 class="panel_heading panlhed">Account</h4>
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<ul>
											<?php if (isset($vouchers['level']['level']) == 1): ?>
												<li class='voucher level'> <a href="<?php echo base_url('index.php/level/add'); ?>"><span class="side_icon ion-levels"></span> Level</a> </li>
											<?php endif ?>
											<?php if (isset($vouchers['account']['account']) == 1): ?>
												<li class='voucher account'> <a href="<?php echo base_url('index.php/account/add'); ?>"><span class="side_icon ion-ios7-personadd-outline"></span> Account</a> </li>
											<?php endif ?>
											<?php if ($vouchers['level']['level'] == 1): ?>
												<li class='voucher level'> <a href="<?php echo base_url('index.php/setting_configuration') ?>"><span class="side_icon ion-ios7-person-outline"></span>Setting Configuration</a> </li>
											<?php endif ?>
										</ul>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>

		<li class='voucher-container'>
			<a href="#">
				<span class="ion-android-book"></span>
				<span class="nav_title">Subject</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePurOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Add New</h4>
									</a>
								</h4>
							</div>
							<div id="collapsePurOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<?php if (isset($vouchers['subject']['subject']) == 1): ?>
											<li class='voucher subject'> <a href="<?php echo base_url('index.php/subject/add'); ?>"><span class="side_icon ion-ios7-copy-outline"></span> Subject</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['assign_subject_to_class']['assign_subject_to_class']) == 1): ?>
											<li class='voucher assign_subject_to_class'> <a href="<?php echo base_url('index.php/subject/assignToClass'); ?>"><span class="side_icon ion-document"></span> Assign Subject to Class</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['assign_subject_to_teacher']['assign_subject_to_teacher']) == 1): ?>
											<li class='voucher assign_subject_to_teacher'> <a href="<?php echo base_url('index.php/subject/assignToTeacher'); ?>"><span class="side_icon fa fa-female"></span> Assign Subject to Teacher</a> </li>
										<?php endif ?>
									</ul>
									<?php if (isset($reports['subject_view']) == 1 || isset($reports['subject_assigned_detail_class_wise']) == 1 || isset($reports['subject_assigned_detail_teacher_wise']) == 1): ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePurTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h4 class="panel_heading panlhed">Reports</h4>
										</a>
									</h4>
								</div>
								<div id="collapsePurTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<ul>
											<?php if (isset($reports['subject_view']) == 1): ?>
												<li class='report subject_view'> <a href="<?php echo base_url('index.php/report/subjectView'); ?>"> Subject View</a> </li>
											<?php endif ?>
											<?php if (isset($reports['subject_assigned_detail_class_wise']) == 1): ?>
												<li class='report subject_assigned_detail_class_wise'> <a href="<?php echo base_url('index.php/report/subjectViewClassWise'); ?>"> Subject Assigned Detail (Class Wise)</a> </li>
											<?php endif ?>
											<?php if (isset($reports['subject_assigned_detail_teacher_wise']) == 1): ?>
												<li class='report subject_assigned_detail_teacher_wise'> <a href="<?php echo base_url('index.php/report/subjectViewTeacherWise'); ?>"> Subject Assigned Detail (Teacher Wise)</a> </li>
											<?php endif ?>
										</ul>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</li>
		<li class='voucher-container'>
			<a href="#">
				<span class="ion-ios7-people-outline"></span>
				<span class="nav_title">Student Management</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseContractOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Vouchers</h4>
									</a>
								</h4>
							</div>
							<div id="collapseContractOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<?php if (isset($vouchers['student']['student']) == 1): ?>
											<li class='voucher student'> <a href="<?php echo base_url('index.php/student/addStudent'); ?>"><span class="side_icon ion-ios7-people-outline"></span> Student</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['promote_students']['promote_students']) == 1): ?>
											<li class='voucher promote_students'> <a href="<?php echo base_url('index.php/student/transfer'); ?>"><span class="side_icon ion-ios7-navigate-outline"></span> Promote Students</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['batch_update']['batch_update']) == 1): ?>
											<li class='voucher batch_update'> <a href="<?php echo base_url('index.php/student/batchUpdate'); ?>"><span class="side_icon ion-ios7-people-outline"></span> Batch Update</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['Struckoff_Readmission']['Struckoff_Readmission']) == 1): ?>
											<li class='voucher Struckoff_Readmission'> <a href="<?php echo base_url('index.php/student/struckoffReadmit'); ?>"><span class="side_icon ion-ios7-people-outline"></span> Struckoff / Readmission</a> </li>
										<?php endif ?>
									</ul>
									<?php if (isset($reports['student_status']) == 1): ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseContractTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h4 class="panel_heading panlhed">Reports</h4>
										</a>
									</h4>
								</div>
								<div id="collapseContractTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<ul>
										<?php if (isset($reports['charges_definitaion']) == 1): ?>
						    					<li class='report charges_definitaion'> <a href="<?php echo base_url('index.php/report/student'); ?>"> Student Report</a> </li>
						    				<?php endif ?>
											<li class='report student_status'> <a href="<?php echo base_url('index.php/report/studentStatus'); ?>"> Status</a> </li>

											<li class='report adimssion_withdrawl_report'> <a href="<?php echo base_url('index.php/report/admissionWithdrawl'); ?>"> Adimssion Withdrawl Report</a> </li>
										</ul>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>

		<li class='voucher-container'>
			<a href="#">
				<span class="ion-ios7-copy-outline"></span>
				<span class="nav_title">Fee Management</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOrderOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Vouchers</h4>
									</a>
								</h4>
							</div>
							<div id="collapseOrderOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<?php if (isset($vouchers['assign_fee_to_class']['assign_fee_to_class']) == 1): ?>
											<li class='voucher assign_fee_to_class'> <a href="<?php echo base_url('index.php/fee/assignToClass'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Assign Fee to Class</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['monthly_fee_issuance']['monthly_fee_issuance']) == 1): ?>
											<li class='voucher monthly_fee_issuance'> <a href="<?php echo base_url('index.php/fee/issuance'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Monthly Fee Issuance</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['detailed_fee_reciept']['detailed_fee_reciept']) == 1): ?>
											<li class='voucher detailed_fee_reciept'> <a href="<?php echo base_url('index.php/fee/recieve'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Detailed Fee Reciept</a> </li>
										<?php endif ?>
										<?php if (isset($vouchers['admission_charges']['admission_charges']) == 1): ?>
											<li class='voucher admission_charges'> <a href="<?php echo base_url('index.php/fee/admission'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Admission Charges</a> </li>
										<?php endif ?>


										<?php if (isset($vouchers['fee_concession']['fee_concession']) == 1): ?>
											<li class='voucher fee_concession'> <a href="<?php echo base_url('index.php/concession'); ?>"><span class="side_icon ion-ios7-plus-empty"></span>Fee Concession</a> </li>
										<?php endif ?>

<<<<<<< HEAD



									   <!-- <?php if (isset($vouchers['fee_concession']['fee_concession']) == 1): ?>
											<li class='voucher fee_concession'> <a href="<?php echo base_url('index.php/concession'); ?>"><span class="side_icon ion-ios7-plus-empty"></span>Fee Concession</a> </li>
										<?php endif ?> --> 


=======
>>>>>>> 6b40ffc15cc7b94db85a65628c5d7dcccb9f6ef1
						            </ul>



						            <?php if (isset($reports['charges_definitaion']) == 1 || isset($reports['monthly_fee_assign']) == 1 || isset($reports['monthly_fee_assign_charges_wise']) == 1 || isset($reports['student_fee_defaulter']) == 1 || isset($reports['fee_assign']) == 1 || isset($reports['fee_concession']) == 1 || isset($reports['fee_rec_student_wise']) == 1 || isset($reports['fee_rec_charges_wise']) == 1): ?>
						            </div>
						        </div>
						    </div>
						    <div class="panel panel-default">
						    	<div class="panel-heading" role="tab" id="headingTwo">
						    		<h4 class="panel-title">
						    			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOrderTwo" aria-expanded="false" aria-controls="collapseTwo">
						    				<h4 class="panel_heading panlhed">Reports</h4>
						    			</a>
						    		</h4>
						    	</div>
						    	<div id="collapseOrderTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						    		<div class="panel-body">
						    			<ul>
						    			<?php if (isset($reports['charges_definitaion']) == 1): ?>
						    					<li class='report charges_definitaion'> <a href="<?php echo base_url('index.php/report/concession'); ?>"> Fee Concession</a> </li>
						    				<?php endif ?>
						    				<?php if (isset($reports['charges_definitaion']) == 1): ?>
						    					<li class='report charges_definitaion'> <a href="<?php echo base_url('index.php/report/chargesDefinition'); ?>"> Charges Definitaion</a> </li>
						    				<?php endif ?>
						    				<?php if (isset($reports['monthly_fee_assign']) == 1): ?>
						    					<li class='report monthly_fee_assign'> <a href="<?php echo base_url('index.php/report/monthlyFeeAssign'); ?>"> Monthly Fee Assign</a> </li>
						    				<?php endif ?>
						    				<!-- <?php if (isset($reports['monthly_fee_assign_charges_wise']) == 1): ?>
						    					<li class='report monthly_fee_assign_charges_wise'> <a href="<?php echo base_url('index.php/report/monthlyFeeAssignChargesWise'); ?>"> Monthly Fee Assign Charges Wise</a> </li>
						    				<?php endif ?> -->
						    				<?php if (isset($reports['student_fee_defaulter']) == 1): ?>
						    					<li class='report student_fee_defaulter'> <a href="<?php echo base_url('index.php/report/defaulters'); ?>">  Fee Defaulter</a> </li>
						    				<?php endif ?>
						    				<?php // if ($reports['student_fee_advance'] == 1): ?>
						    				<!-- <li class='report student_fee_advance'> <a href="<?php echo base_url('index.php/branch/add'); ?>"> Student Fee Advance</a> </li> -->
						    				<?php // endif ?>
						    				<!-- <?php if (isset($reports['fee_assign']) == 1): ?>
						    					<li class='report fee_assign'> <a href="<?php echo base_url('index.php/report/feeassign'); ?>"> Fee Assign</a> </li>
						    				<?php endif ?> -->
						    				<!-- <?php if (isset($reports['fee_concession']) == 1): ?>
						    					<li class='report fee_concession'> <a href="<?php echo base_url('index.php/report/feeconcession'); ?>"> Fee Concession</a> </li>
						    				<?php endif ?> -->
						    				<?php if (isset($reports['fee_rec_student_wise']) == 1): ?>
						    					<li class='report fee_rec_student_wise'> <a href="<?php echo base_url('index.php/report/feeRecStudentWise'); ?>"> Fee Recevie Report</a> </li>
						    				<?php endif ?>
						    				 <?php if (isset($reports['fee_rec_charges_wise']) == 1): ?>
						    					<li class='report fee_rec_charges_wise'> <a href="<?php echo base_url('index.php/report/Admission'); ?>"> Admission</a> </li>
						    				<?php endif ?>
						    				<?php // if ($reports['fee_fine'] == 1): ?>
						    				<!-- <li class='report fee_fine'> <a href="<?php echo base_url('index.php/branch/add'); ?>"> Fee Fine</a> </li> -->
						    				<?php // endif ?>
						    			</ul>
						    		<?php endif ?>
						    	</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class='voucher-container'>
			<a href="#">
				<span class="ion-edit"></span>
				<span class="nav_title">Examination Management</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSaleOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Vouchers</h4>
									</a>
								</h4>
							</div>
							<div id="collapseSaleOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<?php if (isset($vouchers['add_result']['add_result']) == 1): ?>
											<li class='voucher add_result'> <a href="<?php echo base_url('index.php/result/add'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Add Result</a> </li>
										<?php endif ?>
									</ul>
									<?php if (isset($reports['result_subject_wise']) == 1 || isset($reports['student_result']) == 1 || isset($reports['overall_semester_result']) == 1 || isset($reports['student_result_teacher_wise']) == 1): ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSaleTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h4 class="panel_heading panlhed">Reports</h4>
										</a>
									</h4>
								</div>
								<div id="collapseSaleTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<ul>
											<?php if (isset($reports['result_subject_wise']) == 1): ?>
												<li class='report result_subject_wise'> <a href="<?php echo base_url('index.php/report/resultSubjectWise'); ?>"><span class="side_icon ion-paperclip"></span> Result (Subject Wise)</a> </li>
											<?php endif ?>
											<?php if (isset($reports['student_result']) == 1): ?>
												<li class='report student_result'> <a href="<?php echo base_url('index.php/report/studentResult'); ?>"><span class="side_icon ion-paperclip"></span> Student Result</a> </li>
											<?php endif ?>
											<?php if (isset($reports['overall_semester_result']) == 1): ?>
												<li class='report overall_semester_result'> <a href="<?php echo base_url('index.php/report/overallSemResult'); ?>"><span class="side_icon ion-paperclip"></span> Overall Semester Result</a> </li>
											<?php endif ?>
											<?php if (isset($reports['student_result_teacher_wise']) == 1): ?>
												<li class='report student_result_teacher_wise'> <a href="<?php echo base_url('index.php/report/stuResultTeacherWise'); ?>"><span class="side_icon ion-paperclip"></span> Student Result (Teacher Wise)</a> </li>
											<?php endif ?>
										</ul>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>






				</div>
			</div>
		</li>

		<!-- <li class='voucher-container'>
			<a href="#">
				<span class="ion-ios7-cog"></span>
				<span class="nav_title">Job</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">
					<ul>
						<li class='voucher account'> <a href="<?php echo base_url('index.php/job'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Define Job</a> </li>
						<li class='voucher account'> <a href="<?php echo base_url('index.php/jobexpense'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Job Expenses</a> </li>
					</ul>
				</div>
			</div>
		</li> -->

		<li class='voucher-container'>
			<a href="#">
				<span class="ion-android-checkmark"></span>
				<span class="nav_title">Attendance</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInvOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Voucher</h4>
									</a>
								</h4>
							</div>
							<div id="collapseInvOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<?php if (isset($vouchers['student_attendance']['student_attendance']) == 1): ?>
											<li class='voucher student_attendance'> <a href="<?php echo base_url('index.php/attendance/student'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Student Attendance</a> </li>
										<?php endif ?>
										<?php // if ($vouchers['staff_attendance']['staff_attendance'] == 1): ?>
										<!-- <li class='voucher staff_attendance'> <a href="<?php echo base_url('index.php/attendance/staff'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Staff Attendance</a> </li> -->
										<?php // endif ?>
									</ul>
									<?php if (isset($reports['student_attendance_status_wise']) == 1 || isset($reports['monthly_attendance']) == 1 || isset($reports['student_attendance_month_wise']) == 1): ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInvTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h4 class="panel_heading panlhed">Reports</h4>
										</a>
									</h4>
								</div>
								<div id="collapseInvTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<ul>
											<?php if (isset($reports['student_attendance_status_wise']) == 1): ?>
												<li class='report student_attendance_status_wise'> <a href="<?php echo base_url('index.php/report/StuAtndncStatusWise'); ?>"><span class="side_icon ion-paperclip"></span> Student Attendance Status Wise</a> </li>
											<?php endif ?>
											<?php if (isset($reports['monthly_attendance']) == 1): ?>
												<li class='report monthly_attendance'> <a href="<?php echo base_url('index.php/report/monthlyAttendanceReport') ?>"><span class="side_icon ion-paperclip"></span> Monthly Attendance</a> </li>
											<?php endif ?>
											<?php if (isset($reports['student_attendance_month_wise']) == 1): ?>
												<li class='report student_attendance_month_wise'> <a href="<?php echo base_url('index.php/report/StuAtndncMonthWise') ?>"><span class="side_icon ion-paperclip"></span> Student Attendance Month Wise</a> </li>
											<?php endif ?>
											<li class='report daily_attendance'> <a href="<?php echo base_url('index.php/report/dailyAttendanceReport') ?>"><span class="side_icon ion-paperclip"></span> Daily Attendance</a> </li>

										</ul>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>




				</div>

			</div>
		</li>

		<li class='voucher-container'>
			<a href="#">
				<span class="ion-android-note"></span>
				<span class="nav_title">Accounts</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseStitOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Voucher</h4>
									</a>
								</h4>
							</div>
							<div id="collapseStitOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul>
										<?php if ($vouchers['cash_payment_receipt']['cash_payment_receipt'] == 1): ?>
											<li class='voucher cash_payment_receipt'> <a href="<?php echo base_url('index.php/payment'); ?>"><span class=""></span> Cash Payment/Receipt</a> </li>
										<?php endif ?>
										<?php if ($vouchers['chequepaidvoucher']['chequepaidvoucher'] == 1): ?>
											<li class='voucher chequepaidvoucher'> <a href="<?php echo base_url('index.php/payment/chequeIssue'); ?>"><span class=""></span> Cheque Paid</a> </li>
										<?php endif ?>
										<?php if ($vouchers['chequereceiptvoucher']['chequereceiptvoucher'] == 1): ?>
											<li class='voucher chequereceiptvoucher'> <a href="<?php echo base_url('index.php/payment/chequeReceive'); ?>"><span class=""></span> Cheque Receive</a> </li>
										<?php endif ?>
										<?php if ($vouchers['jvvoucher']['jvvoucher'] == 1): ?>
											<li class='voucher jvvoucher'> <a href="<?php echo base_url('index.php/jv'); ?>"><span class=""></span> Journal Voucher</a> </li>
										<?php endif ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseStitTwo" aria-expanded="false" aria-controls="collapseTwo">
										<h4 class="panel_heading panlhed">Reports</h4>
									</a>
								</h4>
							</div>
							<div id="collapseStitTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<ul>
										
										<?php if ($reports['account_ledger'] == 1): ?>
											<li class='report account_ledger'> <a href="<?php echo base_url('index.php/report/accountLedger'); ?>"> Account Ledger</a> </li>
										<?php endif ?>
										<?php if ($reports['cashflowstatement'] == 1): ?>
											<li class='report cashflowstatement'> <a href="<?php echo base_url('index.php/report/cashFlowStatement'); ?>"> Cash Flow Statement</a> </li>
										<?php endif ?>
										<?php if ($reports['trial_balance'] == 1): ?>
											<li class='report trial_balance'> <a href="<?php echo base_url('index.php/trial_balance'); ?>"> Trial Balance</a> </li>
										<?php endif ?>
										<?php if ($reports['accountreports'] == 1): ?>
											<li class='report accountreports'> <a href="<?php echo base_url('index.php/report/accounts'); ?>"> Account Reports</a> </li>
										<?php endif ?>
										<?php if ($reports['agingsheetreport'] == 1): ?>
											<li class="report agingsheetreport"><a href="<?php echo base_url('index.php/report/agingSheet'); ?>"> <i class="icon icon-calendar"></i> Aging Sheet</a></li>
										<?php endif; ?>
										<?php if ($reports['invoiceagingreport'] == 1): ?>

											<li class="invoiceagingreport"><a href="<?php echo base_url('index.php/report/invoiceAging'); ?>" name="subMenuLink"> <i class="icon icon-calendar"></i> Invoice Aging</a></li>
										<?php endif; ?>
										<?php if ($reports['coa'] == 1): ?>
											<li class='report coa'> <a href="<?php echo base_url('index.php/report/chartOfAccounts'); ?>"> Chart of Accounts</a> </li>
										<?php endif ?>
										<?php if ($reports['chequereports'] == 1): ?>
											<li class='report chequereports'> <a href="<?php echo base_url('index.php/report/cheques'); ?>"> Cheque Reports</a> </li>
										<?php endif ?>
										
										<?php if ($reports['profitloss'] == 1): ?>
											<li class='report profitloss'> <a href="<?php echo base_url('index.php/report/detailedprofitloss'); ?>"> Profit/Loss and Balance Sheet</a> </li>
										<?php endif ?>
										
									</ul>
									<!-- <ul>
										<li class='report account_ledger'> <a href="<?php echo base_url('index.php/report/accountLedger'); ?>"> Account Ledger</a> </li>
										<li class='report account_ledger'> <a href="<?php echo base_url('index.php/report/cashFlowStatement'); ?>"> Cash Flow Statement</a> </li>
										<li class='report trial_balance'> <a href="<?php echo base_url('index.php/trial_balance'); ?>"> Trial Balance</a> </li>
										<li class='report trial_balance'> <a href="<?php echo base_url('index.php/report/accounts'); ?>"> Account Reports</a> </li>
										<li class='report trial_balance'> <a href="<?php echo base_url('index.php/report/chartOfAccounts'); ?>"> Chart of Accounts</a> </li>
										<li class='report trial_balance'> <a href="<?php echo base_url('index.php/report/cheques'); ?>"> Cheque Reports</a> </li>
									</ul> -->
								</div>
							</div>
						</div>
					</div>




				</div>

			</div>
		</li>

		<li class='voucher-container'>
			<a href="#">
				<span class="ion-ios7-albums-outline"></span>
				<span class="nav_title">Reports</span>
			</a>
			<div class="sub_panel">
				<div class="side_inner">

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccOne" aria-expanded="true" aria-controls="collapseOne">
										<h4 class="panel_heading panlhed">Vouchers</h4>
									</a>
								</h4>
							</div>
							<div id="collapseAccOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<?php if (isset($reports['staff_status']) == 1): ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccTwo" aria-expanded="false" aria-controls="collapseTwo">
											<h4 class="panel_heading panlhed">Reports</h4>
										</a>
									</h4>
								</div>
								<div id="collapseAccTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<ul>
											<?php if (isset($reports['staff_status']) == 1): ?>
												<li class='report staff_status'> <a href="<?php echo base_url('index.php/report/staffStatus'); ?>"> Status</a> </li>
											<?php endif ?>
										</ul>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>




				</div>
			</div>
		</li>
		<!-- sss -->
		

	</ul>
</nav>


