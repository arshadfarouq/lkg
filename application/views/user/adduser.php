<!-- main content -->
<div id="main_wrapper">
  	<div class="page_bar">
    	<div class="row">
      		<div class="col-md-12">
      			<div class="row">
      				<div class="col-lg-11">
      					<h1 class="page_title">Add User</h1>
      				</div>
      				<div class="col-lg-1">
      					<!-- Button trigger modal -->
						<button class="btn btn-primary btn-openmodal btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">Add</button>
      				</div>
      			</div>
      		</div>
    	</div>
  	</div><!--page-bar-->

  	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">

				<!--.......................starting of tabel.........................-->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<table class="table table-striped table-hover ar-datatable">
									<thead>
										<tr>
											<th>Sr#</th>
											<th>Name</th>
											<th>Username</th>
											<th>Mobile</th>
											<th>Role</th>
											<th>Email</th>
											<th>Branch</th>
											<th>Usertype</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $counter = 1; foreach ($users as $user): ?>
											<tr>
												<td><?php echo $counter++; ?></td>
												<td><?php echo $user['fullname'] ?></td>
												<td><?php echo $user['uname']; ?></td>
												<td><?php echo $user['mobile']; ?></td>
												<td><?php echo $user['role_name']; ?></td>
												<td><?php echo $user['email']; ?></td>
												<td><?php echo $user['branch_name']; ?></td>
												<td><?php echo $user['type']; ?></td>
												<td><a href="" class="btn btn-primary btn-edit-user" data-uid="<?php echo $user['uid']; ?>"><span class="fa fa-edit"></span></a></td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div><!--panel body-->
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  	<div class="modal-dialog modal-lg">
				    	<div class="modal-content">
					   		<div class="modal-header" style="background:#64b92a !important; color:white !important;padding-bottom:20px !important;">
					      		<button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
        						<h4 class="modal-title" id="myModalLabel">Add/Update User</h4>
      						</div>
      						
      						<div class="modal-body">
        						<div class="container-fluid">
                      				<div class="col-lg-11 col-lg-offset-1">
                        				<div class="row">
				                        	
				                        	<form role="form">

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-2">
				                        					<label>User Id</label>
				                        					<input type="number" class="form-control num" id="txtId">
						                                    <input type="hidden" id="txtMaxIdHidden">
						                                    <input type="hidden" id="txtIdHidden">
				                        				</div>
				                        			</div>
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-5">
						                        			<label>User Name</label>
						                        			<input type="text" class="form-control" id="txtUsername">
					                        			</div>
					                        			<div class="col-lg-5">
						                        			<label>Password</label>
						                        			<input type="password" class="form-control" id="txtPassowrd">
					                        			</div>
				                        			</div>
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-5">
						                        			<label>Full Name</label>
						                        			<input type="text" class="form-control" id="txtFullName">
					                        			</div>
					                        			<div class="col-lg-5">
						                        			<label>Mobile#</label>
						                        			<input type="text" class="form-control num" id="txtMobileNo">
					                        			</div>
				                        			</div>
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-5">
				                        					<label>Role</label>
				                        					<select class="form-control" id="role_dropdown">
																<option value="" disabled="" selected="">Choose role</option>
																<?php foreach ($rolegroups as $rolegroup): ?>
																	<option value="<?php echo $rolegroup['rgid']; ?>"><?php echo $rolegroup['name']; ?></option>
																<?php endforeach; ?>
															</select>
				                        				</div>
					                        			<div class="col-lg-5">
						                        			<label>Email</label>
						                        			<input type="text" class="form-control" id="txtEmail">
					                        			</div>
				                        			</div>
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">
				                        				<div class="col-lg-5">
				                        					<label>Branch</label>
				                        					<select class="form-control select2" id="branch_dropdown">
																<option value="" disabled="" selected="">Choose branch</option>
																<?php foreach ($branches as $branch): ?>
																	<option value="<?php echo $branch['brid']; ?>"><?php echo $branch['name']; ?></option>
																<?php endforeach; ?>
															</select>
				                        				</div><!--end of col-->
				                        				<div class="col-lg-5">
				                        					<label>User Type</label>
				                        					<select class="form-control" id="usertype_dropdown">
																<option value="" disabled="" selected="">Choose user type</option>
																<option value="Super Admin">Super Admin</option>
																<option value="User">User</option>
																<option value="Gatekeeper">Gatekeeper</option>
															</select>
				                        				</div>
				                        			</div><!--end of row-->
				                        		</div><!--form-group-->

				                        		<div class="form-group">
				                        			<div class="row">

				                        			</div><!--end of row-->
				                        		</div><!--form-group-->
				                        	</form>
                        				</div><!--end of row-->
                      				</div><!--end of col-lg-11-->
					        	</div><!--container-fluid-->
					      	</div><!--model-body-->

							<div class="modal-footer">
		        				<div class="pull-right">
		        					<a href='' class="btn btnSave btn-success btn-lg" data-insertbtn='<?php echo $vouchers['subject']['insert']; ?>'> <i class="fa fa-save"></i>Save</a>
		        					<a href='' class="btn btnReset btn-warning btn-lg"> <i class="fa fa-refresh"></i>Reset</a>
									<a href='' class="btn btn-lg btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</a>
								</div>
	      					</div><!--modal-footer-->
						</div><!--modal-content-->
      				</div><!--modal-dialogue-->
				</div><!--modal-->
			</div><!--col-lg-12-->
		</div><!--container-fluid-->
	</div><!--page-content-->
</div><!--main-wrapper-->