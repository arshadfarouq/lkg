<img src="<?php echo base_url('assets/img/an.png') ?>" alt="AlNaharSolution" id='loginpagelogo'>
<!-- <img src="<?php echo base_url('assets/img/Logo.gif') ?>" alt="AlNaharSolution" id='loginpagelogo'> -->

<div class="login_container">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='post' autocomplete="off">
		<h1 class="login_heading">Sign In</h1>
		<div class="form-group">
			<input type="text" class="form-control " id="txtUsername" placeholder='Username' name='uname'>
		</div>
		<div class="form-group">
			<input type="password" class="form-control " id="txtPassowrd" placeholder='Password' name='pass'>
		</div>
		<div class="submit_section">
			<input type="submit" value="Sign In" class='btn btn-primary btnSignin'>
			<!-- <a class="btn btn-lg btn-success btn-block btnSignin">Sign In</a> -->
		</div>
		<div class="errors_section">
			<?php echo validation_errors(); ?>
		</div>
		<div class="builtby">
		
			<span>Digitalized By: <a href="http://www.alnaharsolutions.com" target="_blank">Al-Nahar Solutions</a></span>
		</div>
	</form>
</div>
