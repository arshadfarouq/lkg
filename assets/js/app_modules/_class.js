var _Class = function() {

	var settings = {
		txtClassId : $('#txtClassId'),
		txtMaxClassIdHidden : $('#txtMaxClassIdHidden'),
		txtClassIdHidden : $('#txtClassIdHidden'),

		txtClassName : $('#txtClassName'),
		txtClassNameHidden : $('#txtClassNameHidden'),

		txtBranchName : $('.brid'),
		allClasses : $('#allClasses'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnEditClass : $('.btn-edit-class')
	};

	// fetch all the classes based on the selected branch name
	var fetchBranchNames = function( brid ) {

		// if nothing is found inside database then clear the previous data
		$('#allClasses').children('option').remove();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// if nothing is found inside database then clear the previous data
					$('#allClasses').children('option').remove();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += '<option>'+ elem.name +'</option>';
		});
		$(classNamesOptions).appendTo(allClasses);
	}

	var save = function( classObj ) {

		$.ajax({
			url : base_url + 'index.php/group/saveClass',
			type : 'POST',
			data : { 'classDetail' : classObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Class saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		$(settings.txtClassId).val('');
		$(settings.txtMaxClassIdHidden).val('');
		$(settings.txtClassIdHidden).val('');
		$(settings.txtClassName).val('');
		$(settings.txtClassNameHidden).val('');

		getMaxId();
		general.hideLoader();
	}

	var fetch = function(claid) {

		var brid = $('.brid').val();
		$.ajax({
			url : base_url + 'index.php/group/fetchClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(data) {

		$.each(data, function(index, elem){

			$(settings.txtClassId).val(elem.claid);
			$(settings.txtClassIdHidden).val(elem.claid);

			$(settings.txtClassName).val(elem.name);
			$(settings.txtClassNameHidden).val(elem.name);

			$(settings.txtBranchName).val(elem.brid);

			fetchBranchNames(elem.brid);
		});
	}

	// gets the maxid of the voucher
	var getMaxId = function() {

		var brid = $('.brid').val();
		$.ajax({
			url : base_url + 'index.php/group/getMaxId',
			type : 'POST',
			data : { 'table' : 'class', 'brid' : brid},
			dataType : 'JSON',
			success : function(data) {

				$(txtClassId).val(data);
				$(txtMaxClassIdHidden).val(data);
				$(txtClassIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var name = $.trim($(settings.txtClassName).val());

		// remove the error class first
		$(settings.txtClassName).removeClass('inputerror');

		if ( name === '' ) {
			$(settings.txtClassName).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var isFieldValid = function() {
		var errorFlag = false;
		var name = settings.txtClassName;		// get the current fee category name entered by the user
		var claid = settings.txtClassIdHidden;		// hidden claid
		var maxId = settings.txtMaxClassIdHidden;		// hidden max claid
		var txtnameHidden = settings.txtClassNameHidden;		// hidden fee category name

		var clasNames = new Array();
		// get all branch names from the hidden list
		$("#allClasses option").each(function(){
			clasNames.push($(this).text().trim().toLowerCase());
		});

		// if both values are not equal then we are in update mode
		if (claid.val() !== maxId.val()) {

			$.each(clasNames, function(index, elem){

				if (txtnameHidden.val().toLowerCase() !== elem.toLowerCase() && name.val().toLowerCase() === elem.toLowerCase()) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});

		} else {	// if both are equal then we are in save mode

			$.each(clasNames, function(index, elem){

				if (name.val().trim().toLowerCase() === elem) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});
		}

		return errorFlag;
	}

	// returns the fee category object to save into database
	var getSaveClassObject = function() {

		var obj = {

			claid : $.trim($(settings.txtClassIdHidden).val()),
			name : $.trim($(settings.txtClassName).val()),
			brid : $.trim($(settings.txtBranchName).val())
		};
		return obj;
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			// when save button is clicked
			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});

			// when the reset button is clicked
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			// when text is chenged inside the id textbox
			$(txtClassId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(txtClassId).val().trim() !== "" ) {

						var claid = $.trim($(txtClassId).val());
						fetch(claid);
					}
				}
			});

			// when edit button is clicked inside the table view
			$(settings.btnEditClass).on('click', function(e) {
				e.preventDefault();				
				fetch($(this).data('claid'));		// get the class detail by id
			});

			var brid = $(settings.txtBranchName).val();
			fetchBranchNames( brid );	// fetch all the classes based on the selected branch name
			getMaxId();		// gets the max id of voucher
		},

		// makes the voucher ready to save
		initSave : function() {

			var classObj = getSaveClassObject();	// returns the class detail object to save into database
			var isValid = validateSave();			// checks for the empty fields

			if ( !isValid ) {

				// check if the fee category name is already used??	if false
				if ( !isFieldValid() ) {
					// save the data in to the database
					save( classObj );
				} else {	// if fee category name is already used then show error
					alert("Class name already used.");
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(txtClassId).val('');
			$(txtClassName).val('');
			$(txtClassNameHidden).val('');
			$(txtBranchName).val('');

			getMaxId();		// gets the max id of voucher
			general.setPrivillages();*/

			general.reloadWindow();
		}
	};
};

var _class = new _Class();
_class.init();