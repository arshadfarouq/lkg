var AddResult = function() {

	var settings = {

		totalCharges : 0,

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),
		subject_dropdown : $('#subject_dropdown'),
		term_dropdown : $('#term_dropdown'),
		txtTeacherName : $('#txtTeacherName'),

		current_date : $('#current_date'),
		txtTotalMarks : $('#txtTotalMarks'),
		txtSession : $('#txtSession'),

		exam_table : $('#exam-table'),

		btnSearch : $('.btnSearch'),
		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnAddSubject : $('#btnAddSubject'),
		btnDelete : $('.btnDelete')
	};

	var save = function( results, dcno ) {

		$.ajax({
			url : base_url + 'index.php/result/save',
			type : 'POST',
			data : { 'results' : results, 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Result saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var search = function( brid, claid, secid ) {

		$.ajax({
			url : base_url + 'index.php/student/fetchStudentByBranchClassSection',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid },
			dataType : 'JSON',
			success : function(data) {

				$(settings.exam_table).dataTable().fnClearTable();
				if (data.error === 'false') {
					alert('No record found.');
				} else {
					populateTable(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateTable = function(data) {

		var counter = 1;
		$.each(data, function(index, elem) {

			$(settings.exam_table).dataTable().fnAddData( [
				counter++,
				"<span class='stdid'>"+ elem.stdid +"</span>",
				"<span class='name'>"+ elem.name +"</span>",
				"<input type='text' class='tableInputCell obmarks num' maxLength='5'/>",
				"<span class='perc'></span>",
				"<input type='text' class='tableInputCell status'/>",
				"<input type='text' class='tableInputCell remarks'/>" ]
			);
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/result/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	//-------------------------- branch, class, subjects, teacher and section --------------------------//
	var fetchTeacherName = function(brid, claid, secid, sbid) {

		// clear the previous data
		removeTeacher();
		$.ajax({
			url : base_url + 'index.php/subject/fetchTeacherNameByBrClsSecNSbj',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid, 'secid' : secid, 'sbid' : sbid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateTeacherName(data);
				} else {
					$(settings.txtTeacherName).val('No record found');
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var removeTeacher = function() {
		$(settings.txtTeacherName).val('');
		$(settings.txtTeacherName).data('staid', '');
	}
	var populateTeacherName = function(data) {
		$(settings.txtTeacherName).val(data.teacher_name);
		$(settings.txtTeacherName).data('staid', data.staid);
	}
	var fetchSectionNames = function( brid, claid, sec ) {

		// clear the previous data
		removeSectionOptions();
		removeTeacher();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchSubjects = function(brid, claid) {

		// clear the previous data
		removeSubjectOptions();
		removeTeacher();

		$.ajax({
			url : base_url + 'index.php/subject/fetchSubjectByBrNCls',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSubjectNames(data);
				} else {
					removeSubjectOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var removeSubjectOptions = function() {

		$(settings.subject_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-secid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	var populateSubjectNames = function( data ) {

		var subjectNamesOptions = "";
		$.each(data, function( index, elem ) {

			subjectNamesOptions += "<option value='"+ elem.sbid +"' data-sbid='"+ elem.sbid +"'>"+ elem.name +"</option>";
		});

		$(subjectNamesOptions).appendTo(settings.subject_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();
		removeTeacher();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	//--------------------- end of branch, class and section ------------------------//

	var getSaveObject = function() {

		var results = [];

		var _dcno = $(settings.txtdcnoHidden).val();
		var _brid = $(settings.branch_dropdown).val();
		var _secid = $(settings.section_dropdown).val();
		var _claid = $(settings.class_dropdown).val();
		var _sbid = $(settings.subject_dropdown).val();
		var _term = $(settings.term_dropdown).val();
		var _tmakrs = $(settings.txtTotalMarks).val();
		var _staid = $(settings.txtTeacherName).data('staid');
		var _session = $(settings.txtSession).val();
		var _date = $(settings.current_date).val();

		$(settings.exam_table).find('tbody tr').each(function(index, elem) {

			var _stdid = $.trim($(this).closest('tr').find('span.stdid').text());
			var _obmarks = $.trim($(this).closest('tr').find('input.obmarks').val());
			var _perc = $.trim($(this).closest('tr').find('span.perc').text());
			var _status = $.trim($(this).closest('tr').find('input.status').val());
			var _remarks = $.trim($(this).closest('tr').find('input.remarks').val());

			var result = {

				dcno : _dcno,
				brid : _brid,
				stdid : _stdid,
				secid : _secid,
				claid : _claid,
				sbid : _sbid,
				tmarks : _tmakrs,
				obmarks : _obmarks,
				remarks : _remarks,
				date : _date,
				term : _term,
				staid : _staid,
				session : _session,
				grade : '',
				percentage : _perc,
				status : _status
			}

			results.push(result);
		});

		return results;
	}
	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var branch_dropdown = $.trim($(settings.branch_dropdown).val());
		var class_dropdown = $(settings.class_dropdown).val();
		var section_dropdown = $.trim($(settings.section_dropdown).val());
		var subject_dropdown = $.trim($(settings.subject_dropdown).val());
		var current_date = $.trim($(settings.current_date).val());
		var txtSession = $.trim($(settings.txtSession).val());
		var term_dropdown = $.trim($(settings.term_dropdown).val());

		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.section_dropdown).removeClass('inputerror');
		$(settings.subject_dropdown).removeClass('inputerror');
		$(settings.current_date).removeClass('inputerror');
		$(settings.txtSession).removeClass('inputerror');
		$(settings.term_dropdown).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( subject_dropdown === '' || subject_dropdown === null ) {
			$(settings.subject_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( current_date === '' || current_date === null ) {
			$(settings.current_date).addClass('inputerror');
			errorFlag = true;
		}
		if ( txtSession === '' || txtSession === null ) {
			$(settings.txtSession).addClass('inputerror');
			errorFlag = true;
		}
		if ( term_dropdown === '' || term_dropdown === null ) {
			$(settings.term_dropdown).addClass('inputerror');
			errorFlag = true;
		}


		return errorFlag;
	}

	var validateSearch = function() {

		var errorFlag = false;
		var branch_dropdown = $.trim($(settings.branch_dropdown).val());
		var class_dropdown = $(settings.class_dropdown).val();
		var section_dropdown = $.trim($(settings.section_dropdown).val());
		var subject_dropdown = $.trim($(settings.subject_dropdown).val());

		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.section_dropdown).removeClass('inputerror');
		$(settings.subject_dropdown).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( subject_dropdown === '' || subject_dropdown === null ) {
			$(settings.subject_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/result/fetch',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {
				
				reset();
				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var reset = function() {

		$('.inputerror').removeClass('inputerror');
		$(settings.class_dropdown).val('');
		$(settings.section_dropdown).val('');
		$(settings.subjects_dropdown).val('');
		$(settings.teacher_dropdown).val('');

		$(settings.txtTotalMarks).val('');
		$(settings.current_date).datepicker('update', new Date());
		$(settings.txtSession).val('');
		$(settings.term_dropdown).val('');

		$(settings.exam_table).dataTable().fnClearTable();
		removeSectionOptions();
		removeTeacher();
		removeSubjectOptions();
		$(settings.exam_table).dataTable().fnClearTable();
	}

	var populateData = function(data) {

		$(settings.txtdcno).val(data[0]['dcno']);
		$(settings.txtdcnoHidden).val(data[0]['dcno']);

		fetchClassNames(data[0]['brid']);
		$(settings.class_dropdown).val(data[0]['claid']);

		fetchSectionNames(data[0]['brid'], data[0]['claid']);
		$(settings.section_dropdown).val(data[0]['secid']);

		fetchSubjects(data[0]['brid'], data[0]['claid']);
		$(settings.subject_dropdown).val(data[0]['sbid']);

		fetchTeacherName(data[0]['brid'], data[0]['claid'], data[0]['secid'], data[0]['sbid']);

		$(settings.txtTotalMarks).val(parseFloat(data[0]['tmarks']).toFixed(2));
		$(settings.current_date).datepicker('update', data[0]['date'].substring(0, 10));
		$(settings.txtSession).val(data[0]['session']);
		$(settings.term_dropdown).val(data[0]['term']);

		var counter = 1;
		$.each(data, function(index, elem) {

			$(settings.exam_table).dataTable().fnAddData( [
				counter++,
				"<span class='stdid'>"+ elem.stdid +"</span>",
				"<span class='name'>"+ elem.student_name +"</span>",
				"<input type='text' class='tableInputCell obmarks num' maxLength='5' value='"+ parseFloat(elem.obmarks).toFixed(2) +"'/>",
				"<span class='perc'>"+ parseFloat(elem.percentage).toFixed(2) +"</span>",
				"<input type='text' class='tableInputCell status' value='"+ elem.status +"'/>",
				"<input type='text' class='tableInputCell remarks' value='"+ elem.remarks +"'/>" ]
			);
		});
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/result/delete',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('#txtdcno').on('change', function() {
				var dcno = $.trim($(this).val());
				fetch(dcno);
			});

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});
			$(settings.btnSearch).on('click',  function(e) {
				e.preventDefault();

				self.initSearch();
			});
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();

				var dcno = $(settings.txtdcno).val();
				deleteVoucher(dcno);
			});

			//////////////////////////////////////////
			// block all keys other than numeric //
			//////////////////////////////////////////
			$('table').on('keypress', '.num', function(e) {
				var numericKeys = [];
		        var pressedKey  = e.which;

		        for (i = 48; i < 58; i++) {
		            numericKeys.push(i);
		        }

		        numericKeys.push(46);
		        numericKeys.push(8);
		        numericKeys.push(0);

		        if (!(numericKeys.indexOf(pressedKey ) >= 0)) {
		            e.preventDefault();
		        }
			});

			// when fetchSectionNames is changed
			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
					fetchSubjects(brid, claid);
				}
			});
			$(settings.subject_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = ($(settings.section_dropdown).val() === '') ? -1 : $(settings.section_dropdown).val();
				var sbid = $(settings.subject_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null ) && (secid !== "" || secid !== null ) && (sbid !== "" || sbid !== null ) ) {
					fetchTeacherName(brid, claid, secid, sbid);
				}
			});








			$(settings.txtdcno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtdcno).val().trim() !== "" ) {

						var dcno = $.trim($(settings.txtdcno).val());
						fetch(dcno);
					}
				}
			});
			$(settings.exam_table).on('input', '.obmarks', function() {

				var tMarks = $.trim($(settings.txtTotalMarks).val());
				tMarks = ( tMarks === '') ? parseFloat(0) : parseFloat(tMarks);

				var obMarks = $.trim($(this).val());
				obMarks = ( obMarks === '') ? parseFloat(0) : parseFloat(obMarks);

				var perc = (obMarks / tMarks) * 100;
				$(this).closest('tr').find('.perc').text(perc.toFixed(2));

				if (perc >= 40)  {
					$(this).closest('tr').find('input.status').val("Pass");
				} else {
					$(this).closest('tr').find('input.status').val("Fail");
				}
			});

			getMaxId();
			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.exam_table).find("tbody tr :not(.dataTables_empty)").length;
				if (rowsCount > 0 ) {

					var saveObj = getSaveObject();
					var dcno = $(settings.txtdcnoHidden).val();
					save( saveObj, dcno );
				} else {
					alert('No data found to save.');
				}
			} else {
				alert('Correct the errors...');
			}
		},

		initSearch : function() {

			var error = validateSearch();

			if (!error) {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(settings.section_dropdown).val();

				search( brid, claid, secid );
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.branch_dropdown).val('');
			$(settings.class_dropdown).val('');
			$(settings.section_dropdown).val('');
			$(settings.subjects_dropdown).val('');
			$(settings.teacher_dropdown).val('');

			$(settings.txtTotalMarks).val('');
			$(settings.current_date).datepicker('update', new Date());
			$(settings.txtSession).val('');
			$(settings.term_dropdown).val('');

			$(settings.exam_table).dataTable().fnClearTable();
			removeOptions();
			removeSectionOptions();
			removeTeacher();
			removeSubjectOptions();

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var addResult = new AddResult();
addResult.init();