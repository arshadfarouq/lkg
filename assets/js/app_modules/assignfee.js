var AssignFee = function() {

	var settings = {

		totalCharges : 0,

		txtFeeAssignId : $('#txtFeeAssignId'),
		txtMaxFeeAssignIdHidden : $('#txtMaxFeeAssignIdHidden'),
		txtFeeAssignIdHidden : $('#txtFeeAssignIdHidden'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		feeCategory_dropdown : $('#feeCategory_dropdown'),
		particulars_dropdown : $('#particulars_dropdown'),

		txtCharges : $('#txtCharges'),
		
		charges_table : $('#charges_table'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnAddCharges : $('#btnAddCharges'),
		btnDelete : $('.btnDelete')
	};

	var save = function( assignFeeObj, chargesDetail) {

		$.ajax({
			url : base_url + 'index.php/fee/saveAssignFeetoClass',
			type : 'POST',
			data : { 'main' : assignFeeObj, 'detail' : chargesDetail, 'claid': $('#class_dropdown').val(), 'fcid' : $(settings.feeCategory_dropdown).val(), 'dcno': $('#txtFeeAssignId').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error == 'duplicate') {
					alert('Fee already assigned!');
				} else {

					if (data.error === 'true') {
						alert('An internal error occured while saving branch. Please try again.');
					} else {
						alert('Voucher saved successfully.');
						general.reloadWindow();
					}
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/fee/getMaxFeeAssignId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtFeeAssignId).val(data);
				$(settings.txtMaxFeeAssignIdHidden).val(data);
				$(settings.txtFeeAssignIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(class_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});
		$(classNamesOptions).appendTo(class_dropdown);
	}

	var validateEntry = function() {


		var errorFlag = false;
		var particulars = $(settings.particulars_dropdown).val();
		var amount = $(settings.txtCharges).val();

		// remove the error class first
		$(settings.particulars_dropdown).removeClass('inputerror');
		$(settings.txtCharges).removeClass('inputerror');

		if ( particulars === '' || particulars === null ) {
			$(settings.particulars_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( amount === '' || amount === null ) {
			$(settings.txtCharges).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(chid, amount, particulars) {

		var row = "";
		row = 	"<tr> <td> "+ particulars +"</td> <td> "+ amount +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-chid="+ chid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo(settings.charges_table);
	}

	var getSaveAssignFeeObject = function() {

		var d = new Date();
		var _date = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

		var assignFeeObj = {

			faid : $(settings.txtFeeAssignIdHidden).val(),
			fadate : _date,
			claid : $(settings.class_dropdown).val(),
			fcid : $(settings.feeCategory_dropdown).val(),
			brid : $(settings.branch_dropdown).val()
		}

		return assignFeeObj;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var class_dropdown = $(settings.class_dropdown).val();
		var feeCategory_dropdown = $.trim($(settings.feeCategory_dropdown).val());
		var branch_dropdown = $.trim($(settings.branch_dropdown).val());

		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.feeCategory_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( feeCategory_dropdown === '' || feeCategory_dropdown === null ) {
			$(settings.feeCategory_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var getAssignFeeDetailObj = function() {

		var chargesDetail = [];

		$(settings.charges_table).find('tbody tr').each(function() {

			var _amount = $.trim($(this).closest('tr').find('td').eq(1).text());
			var _chid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('chid'));

			var charge = {

				chid : _chid,
				amount : _amount,
				brid : $('.brid').val()
			};
			chargesDetail.push(charge);
		});

		return chargesDetail;
	}

	var fetch = function(faid) {

		$.ajax({
			url : base_url + 'index.php/fee/fetchFeeAssign',
			type : 'POST',
			data : { 'faid' : faid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateMainData(data.main);
					populateDetailData(data.detail);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateMainData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtFeeAssignId).val(elem.faid);
			$(settings.txtFeeAssignIdHidden).val(elem.faid);
			$(settings.class_dropdown).val(elem.claid);
			$(settings.feeCategory_dropdown).val(elem.fcid);
		});
	}

	var populateDetailData = function(data) {

		$(settings.charges_table).find('tbody tr').remove();
		$.each(data, function(index, elem) {			
			appendToTable(elem.chid, elem.amount, elem.description);			
		});
	}

	var deleteVoucher = function(faid) {

		$.ajax({
			url : base_url + 'index.php/fee/deleteVoucher',
			type : 'POST',
			data : { 'faid' : faid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('#txtFeeAssignId').on('change', function() {
				var faid = $.trim($(settings.txtFeeAssignId).val());						
				fetch(faid);
			});

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();
				
				self.initSave();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$(settings.particulars_dropdown).on('change', function() {

				var amount = $(this).find('option:selected').data('amount');
				$(settings.txtCharges).val(amount);
			});

			$(settings.btnAddCharges).on('click', function(e) {
				e.preventDefault();

				var chid = $(settings.particulars_dropdown).val();
				var amount = $(settings.txtCharges).val();
				var particulars = $(settings.particulars_dropdown).find('option:selected').text();

				var error = validateEntry();
				if (!error) {

					// calculate the sum and show it
					/*settings.totalMarks += parseInt(marks);
					$(settings.txtObtainedMarks).val(settings.totalMarks);*/

					appendToTable(chid, amount, particulars);

					$(settings.particulars_dropdown).val('');
					$(settings.txtCharges).val('');

				} else {
					alert('Correct the errors...');
				}
			});

			// when btnRowRemove is clicked
			$(settings.charges_table).on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				/*var marks = $.trim($(this).closest('tr').find('td').eq(1).text());

				settings.totalMarks -= parseInt(marks);
				$(settings.txtObtainedMarks).val(settings.totalMarks);*/

				$(this).closest('tr').remove();
			});

			$(settings.charges_table).on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				
				var amount = $.trim($(this).closest('tr').find('td').eq(1).text());
				var chid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('chid'));

				$(settings.particulars_dropdown).val(chid);
				var amount = $(settings.txtCharges).val(amount);

				/*settings.totalMarks -= parseInt(marks);
				$(settings.txtObtainedMarks).val(settings.totalMarks);*/

				$(this).closest('tr').remove();
			});

			// when text is changed in txtStudentId
			$(settings.txtFeeAssignId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtFeeAssignId).val().trim() !== "" ) {

						var faid = $.trim($(settings.txtFeeAssignId).val());						
						fetch(faid);
					}
				}
			});

			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();
				
				var faid = $(settings.txtFeeAssignId).val();
				deleteVoucher(faid);
			});

			getMaxId();
			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		// prepares the data to save it into the database
		initSave : function() {

			var assignFeeObj = getSaveAssignFeeObject();
			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.charges_table).find('tbody tr').length;

				if (rowsCount > 0 ) {

					var chargesDetail = getAssignFeeDetailObj();
					save( assignFeeObj, chargesDetail);
				} else {
					alert('Please charges defined.');	
				}				
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.class_dropdown).val('');
			$(settings.feeCategory_dropdown).val('');
			$(settings.branch_dropdown).val('');
			$(settings.charges_table).find('tbody tr').remove();
			removeOptions();

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var assignFee = new AssignFee();
assignFee.init();