var AssignSubjectClass = function() {

	var settings = {

		totalCharges : 0,

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		subjects_dropdown : $('#subjects_dropdown'),

		subjects_table : $('#subjects_table'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnAddSubject : $('#btnAddSubject'),
		btnDelete : $('.btnDelete')
	};

	var save = function( subjectAssignObj, dcno ) {

		$.ajax({
			url : base_url + 'index.php/subject/saveAssignSubjecttoClass',
			type : 'POST',
			data : { 'subjectAssignObj' : subjectAssignObj, 'dcno' : dcno, 'claid' : $('#class_dropdown').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error == 'duplicate') {
					alert('Subjects already assigned to class!');
				} else {

					if (data.error === 'true') {
						alert('An internal error occured while saving branch. Please try again.');
					} else {
						alert('Subjects assigned successfully.');
						general.reloadWindow();
					}
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/subject/getMaxIdSubjectClass',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(class_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});
		$(classNamesOptions).appendTo(class_dropdown);
	}

	var validateEntry = function() {


		var errorFlag = false;
		var name = $(settings.subjects_dropdown).val();

		// remove the error class first
		$(settings.subjects_dropdown).removeClass('inputerror');

		if ( name === '' || name === null ) {
			$(settings.subjects_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(sbid, name) {

		var row = "";
		row = 	"<tr> <td> "+ name +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-sbid="+ sbid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo(settings.subjects_table);
	}

	var getSaveObject = function() {

		var d = new Date();
		var _date = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

		var subjectAssig = [];

		$(settings.subjects_table).find('tbody tr').each(function() {

			var _sbid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('sbid'));

			var obj = {

				dcno : $(settings.txtdcnoHidden).val(),
				date : _date,
				claid : $(settings.class_dropdown).val(),
				brid : $(settings.branch_dropdown).val(),
				sbid : _sbid,
			}

			subjectAssig.push(obj);
		});

		return subjectAssig;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var class_dropdown = $(settings.class_dropdown).val();
		var branch_dropdown = $.trim($(settings.branch_dropdown).val());

		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/subject/fetchAssignedSubjectstoClass',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$(settings.subjects_table).find('tbody tr').remove();

		$(settings.txtdcno).val(data[0]['dcno']);
		$(settings.txtdcnoHidden).val(data[0]['dcno']);

		fetchClassNames(data[0]['brid']);
		$(settings.class_dropdown).val(data[0]['claid']);

		$.each(data, function(index, elem) {
			appendToTable(elem.sbid, elem.name);
		});
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/subject/deleteVoucher',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var isSubjectAlreadyAdded = function(name) {

		var errorFlag = false;

		$(settings.subjects_table).find('tbody tr').each(function(index, elem) {

			var _name = $.trim($(elem).find('td').eq(0).text());
			if (name == _name) {
				console.log(name);
				console.log(_name);
				errorFlag = true;
			}
		});

		return errorFlag;
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('#txtdcno').on('change', function() {
				var dcno = $.trim($(settings.txtdcno).val());
				fetch(dcno);
			});

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$(settings.subjects_dropdown).on('change', function() {

				var amount = $(this).find('option:selected').data('amount');
				$(settings.txtCharges).val(amount);
			});

			$(settings.btnAddSubject).on('click', function(e) {
				e.preventDefault();

				var sbid = $(settings.subjects_dropdown).val();
				var name = $(settings.subjects_dropdown).find('option:selected').text();

				var error = validateEntry();
				if (!error) {

					error = isSubjectAlreadyAdded(name);
					if (!error) {

						appendToTable(sbid, name);
						$(settings.subjects_dropdown).val('');
					} else {
						alert('Subject already added.');
					}
				} else {
					alert('Correct the errors...');
				}
			});

			// when btnRowRemove is clicked
			$(settings.subjects_table).on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				$(this).closest('tr').remove();
			});

			$(settings.subjects_table).on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var sbid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('sbid'));

				$(settings.subjects_dropdown).val(sbid);
				$(this).closest('tr').remove();
			});

			// when text is changed in txtStudentId
			$(settings.txtdcno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtdcno).val().trim() !== "" ) {

						var dcno = $.trim($(settings.txtdcno).val());
						fetch(dcno);
					}
				}
			});

			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();

				var dcno = $(settings.txtdcno).val();
				deleteVoucher(dcno);
			});
			// $('#subjects_dropdown').select2({ maximumSelectionSize: 3 });

			getMaxId();
			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.subjects_table).find('tbody tr').length;

				if (rowsCount > 0 ) {

					var subjectAssignObj = getSaveObject();
					var dcno = $(settings.txtdcnoHidden).val();

					save( subjectAssignObj, dcno );
				} else {
					alert('Please add some subjects.');
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.class_dropdown).val('');
			$(settings.branch_dropdown).val('');
			$(settings.subjects_table).find('tbody tr').remove();
			removeOptions();

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var assignSubjectClass = new AssignSubjectClass();
assignSubjectClass.init();