var AssignSubjectTeacher = function() {

	var settings = {

		totalCharges : 0,

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),
		subjects_dropdown : $('#subjects_dropdown'),
		teacher_dropdown : $('#teacher_dropdown'),

		btnAddST : $('#btnAddST'),
		subjects_table : $('#subjects_table'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnAddSubject : $('#btnAddSubject'),
		btnDelete : $('.btnDelete')
	};

	var save = function( sdetail, dcno ) {

		$.ajax({
			url : base_url + 'index.php/subject/saveAssignSubjecttoTeacher',
			type : 'POST',
			data : { 'sdetail' : sdetail, 'dcno' : dcno, 'claid': $('#class_dropdown').val(), 'secid': $('#section_dropdown').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error == 'duplicate') {
					alert('Subjects already assigned!');
				} else {
					if (data.error === 'true') {
						alert('An internal error occured while saving voucher. Please try again.');
					} else {
						alert('Subjects assigned successfully.');
						general.reloadWindow();
					}
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/subject/getMaxIdSubjectTeacher',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	//-------------------------- branch, class and section --------------------------//
	// fetch all the sections based on the selected class name
	var fetchSectionNames = function( brid, claid, sec ) {

		// clear the previous data
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			async : false,
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-secid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			async : false,
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	//--------------------- end of branch, class and section ------------------------//

	var validateEntry = function() {


		var errorFlag = false;
		var name = $(settings.subjects_dropdown).val();
		var teacher = $(settings.teacher_dropdown).val();

		// remove the error class first
		$(settings.subjects_dropdown).removeClass('inputerror');
		$(settings.teacher_dropdown).removeClass('inputerror');

		if ( name === '' || name === null ) {
			$(settings.subjects_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( teacher === '' || teacher === null ) {
			$(settings.teacher_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(sbid, subject, staid, teacher) {

		var row = "";
		row = 	"<tr> <td> "+ subject +"</td> <td> "+ teacher +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-sbid="+ sbid +" data-staid="+ staid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo(settings.subjects_table);
	}

	var getSaveObject = function() {

		var d = new Date();
		var _date = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

		var sdetail = [];

		$(settings.subjects_table).find('tbody tr').each(function() {

			var _sbid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('sbid'));
			var _staid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('staid'));

			var obj = {

				dcno : $(settings.txtdcnoHidden).val(),
				date : _date,
				claid : $(settings.class_dropdown).val(),
				brid : $(settings.branch_dropdown).val(),
				secid : $(settings.section_dropdown).val(),
				sbid : _sbid,
				staid : _staid,
			}

			sdetail.push(obj);
		});

		return sdetail;
	}
	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var branch_dropdown = $.trim($(settings.branch_dropdown).val());
		var class_dropdown = $(settings.class_dropdown).val();
		var section_dropdown = $.trim($(settings.section_dropdown).val());

		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}


		return errorFlag;
	}

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/subject/fetchAssignedSubjectstoTeacher',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				$(settings.subjects_table).find('tbody tr').remove();
				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$(settings.txtdcno).val(data[0]['dcno']);
		$(settings.txtdcnoHidden).val(data[0]['dcno']);

		$(settings.branch_dropdown).val(data[0]['brid']);

		fetchClassNames(data[0]['brid']);
		$(settings.class_dropdown).val(data[0]['claid']);

		fetchSectionNames(data[0]['brid'], data[0]['claid']);
		$(settings.section_dropdown).val(data[0]['secid']);

		$.each(data, function(index, elem) {

			appendToTable(elem.sbid, elem.subject_name, elem.staid, elem.teacher_name);
		});
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/subject/deleteVoucherST',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('#txtdcno').on('change', function() {
				var dcno = $.trim($(settings.txtdcno).val());
				fetch(dcno);
			});

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();

				var dcno = $(settings.txtdcno).val();
				deleteVoucher(dcno);
			});

			// when fetchSectionNames is changed
			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			$(settings.btnAddST).on('click', function(e) {
				e.preventDefault();

				var sbid = $(settings.subjects_dropdown).val();
				var subject = $(settings.subjects_dropdown).find('option:selected').text();
				var staid = $(settings.teacher_dropdown).val();
				var teacher = $(settings.teacher_dropdown).find('option:selected').text();

				var error = validateEntry();
				if (!error) {

					appendToTable(sbid, subject, staid, teacher);
					$(settings.subjects_dropdown).val('');
					$(settings.teacher_dropdown).val('');
				} else {
					alert('Correct the errors...');
				}
			});
			$(settings.subjects_table).on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				$(this).closest('tr').remove();
			});
			$(settings.subjects_table).on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var sbid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('sbid'));
				var staid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('staid'));

				$(settings.subjects_dropdown).val(sbid);
				$(settings.teacher_dropdown).val(staid);
				$(this).closest('tr').remove();
			});



			$(settings.txtdcno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtdcno).val().trim() !== "" ) {

						var dcno = $.trim($(settings.txtdcno).val());
						fetch(dcno);
					}
				}
			});

			getMaxId();
			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.subjects_table).find('tbody tr').length;

				if (rowsCount > 0 ) {

					var saveObj = getSaveObject();
					var dcno = $(settings.txtdcnoHidden).val();

					save( saveObj, dcno );
				} else {
					alert('No data found to save.');
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.branch_dropdown).val('');
			$(settings.class_dropdown).val('');
			$(settings.section_dropdown).val('');
			$(settings.subjects_dropdown).val('');
			$(settings.teacher_dropdown).val('');
			$(settings.subjects_table).find('tbody tr').remove();
			removeOptions();
			removeSectionOptions();

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var assignSubjectTeacher = new AssignSubjectTeacher();
assignSubjectTeacher.init();