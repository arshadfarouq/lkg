var StuAtndncStatusWise = function() {

	var settings = {

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid, sec ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	var validateSearch = function() {

		var errorFlag = false;
		var branch_dropdown = $(settings.branch_dropdown).val();

		// remove the error class first
		$(settings.branch_dropdown).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(brid) {

		$.ajax({
			url : base_url + 'index.php/attendance/dailyAttendanceReport',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				$('#atnd-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		$.each(data, function(index, elem) {
			row = 	"<tr>"+
						"<td>"+ elem.branch_name +"</td>"+
						"<td>"+ elem.class_name +"</td>"+
						"<td>"+ elem.section_name +"</td>"+
						"<td>"+ elem.total_students +"</td>"+
						"<td>"+ elem.absent +"</td>"+
						"<td>"+ elem.present +"</td>"+
						"<td>"+ elem.leave +"</td></tr>";
			$(row).appendTo('#atnd-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;
			// alert('sadads');

			$(".btnPrint").on('click', function () {
              	var etype = 'dailyAttendanceReport';
		       
				// alert(className);	            
              	
        	    var url = base_url + 'index.php/doc/pdf_dailAttandanceReport/' +etype ;
        	    // alert(url);
              	window.open(url);
              });
			self.initSearch();
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var brid = $(settings.branch_dropdown).val();

				search(brid);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.branch_dropdown).val('');
			$(settings.class_dropdown).val('');
			$(settings.section_dropdown).val('');
			$(settings.stdid_dropdown).val('');

			removeOptions();
			removeSectionOptions();
			removeStdidOptions();

			// removes all rows
			$('#atnd-table').find('tbody tr').remove();*/
			general.reloadWindow();
		}

	};
};


var stuAtndncStatusWise = new StuAtndncStatusWise();
stuAtndncStatusWise.init();