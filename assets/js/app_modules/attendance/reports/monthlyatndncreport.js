var MonthlyAttendanceReport = function() {

	var settings = {

		month_year_picker : $('.month_year_picker'),
		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid, sec ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	var validateSearch = function() {

		var errorFlag = false;
		var branch_dropdown = $(settings.branch_dropdown).val();
		var class_dropdown = $(settings.class_dropdown).val();
		var section_dropdown = $(settings.section_dropdown).val();
		var month_year_picker = $(settings.month_year_picker).val();

		// remove the error class first
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.section_dropdown).removeClass('inputerror');
		$(settings.month_year_picker).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( month_year_picker === '' || month_year_picker === null ) {
			$(settings.month_year_picker).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(month, year, brid, claid, secid) {

		$.ajax({
			url : base_url + 'index.php/attendance/monthlyAttendanceReport',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid, 'month' : month, 'year' : year },
			dataType : 'JSON',
			success : function(data) {

				$('#atnd-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var month_year =  $.trim($('.month_year_picker').val());
		var month = parseInt(month_year.substring(0, 2));
		var year = parseInt(month_year.substring(3));
		var days = general.getDaysInMonth(month, year);
		var stdid = "";
		var counter = 1;
		// alert(days);

		$.each(data, function(index, elem) {

			if (elem.stdid != stdid) {
				
				var cols = new Array(days);
				console.log("Array is" +cols);
				$.each(data, function(ind, el) {

					if (el.stdid == elem.stdid) {
						cols[el.day] = el.status.substring(0, 1);
						// console.log(cols[el.day]);
						// alert(cols[el.day]);
					}
				});


				var row = "<tr><td class='txtcenter level4row'>"+ (counter++) +"</td><td class='txtcenter level4row tdstuName'>"+ elem.student_name +"</td>";
				for ( i = 1; i<=cols.length; i++) {
					console.log(typeof(cols[i]));

					if (i%2 == 0) {
						row += "<td class='txtcenter' style='background: #e6f3fd;'>"+ ((typeof(cols[i]) == "undefined") ? '-' : cols[i]) +"</td>";
						// alert(row);
					} else {
						row += "<td class='txtcenter' style='background: #fff;'>"+ ((typeof(cols[i]) == "undefined") ? '-' : cols[i]) +"</td>";
					}
				}
				row += "</tr>";
				$(row).appendTo('#atnd-table tbody');

				stdid = elem.stdid;
			}
		});
	}

	var populateTableHeader = function(month, year) {
		$('#atnd-table thead').find('tr').remove();
		$('#atnd-table').find('tbody tr').remove();
		var days = general.getDaysInMonth(month, year);

		var cols = "<tr><th class='txtcenter level4row'>Sr#</th><th class='txtcenter level4row thstudName'>Student Name</th>";
		for ( i = 1; i<=days; i++) {

			if (i%2 == 0) {
				cols += "<th class='txtcenter' style='background: #e6f3fd;'>"+ i +"</th>";
			} else {
				cols += "<th class='txtcenter' style='background: #fff;'>"+ i +"</th>";
			}
		}
		cols += "</tr>";

		$(cols).appendTo('#atnd-table thead');
	}

	return {

		init : function () {
			this.bindUI();
			// alert('ddd');
		},

		bindUI : function() {

			var self = this;

			$(".btnPrint").on('click', function () {
				// alert('sd');
				// Pdf Print
              	/*var etype = 'monthlyAttendanceReport';
	            var month_year =  $.trim($('.month_year_picker').val());
	            var month = parseInt(month_year.substring(0, 2));
	            var year = parseInt(month_year.substring(3));
	             month--;

	            var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	            var brid = $(settings.branch_dropdown).val();
	            var claid = $(settings.class_dropdown).val();
	            var secid = $(settings.section_dropdown).val();
              	var month = monthNames[month];


              	var month_yearOne =  $.trim($('.month_year_picker').val());
              	var monthOne = parseInt(month_yearOne.substring(0, 2));
              	var yearOne = parseInt(month_yearOne.substring(3));
              	var daysOne = general.getDaysInMonth(monthOne, yearOne);
              	var className = $('#class_dropdown option:selected').text(); 
              	var SectionName = $('#section_dropdown option:selected').text();
              	// alert(month);
              	// alert(year);
              	// alert(claid);
              	// alert(secid);
              	// alert(monthOne);
              	// alert(yearOne);
        	    var url = base_url + 'index.php/doc/pdf_monthlyAttendanceReport/' +etype +  '/' + year +  '/' + month +  '/' + brid +  '/' + claid  +  '/' + secid +  '/' + month_yearOne +  '/' + monthOne +  '/' + yearOne +  '/' + daysOne +  '/' + className +  '/' + SectionName ;
				// alert(url);
              	window.open(url);*/
              	window.open(base_url + 'application/views/reportPrints/monthlyAttendanceReport.php', "Monthly Attendance", "width=1300, height=842");

              });

			// when fetchSectionNames is changed
			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				// clear tables

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.month_year_picker').datepicker().on('changeDate', function() {

				var month_year =  $.trim($('.month_year_picker').val());
				var month = parseInt(month_year.substring(0, 2));
				var year = parseInt(month_year.substring(3));

				populateTableHeader(month, year);
			});

			var d = new Date();
			var year = d.getFullYear();
			var month = d.getMonth();
			month++;
			populateTableHeader(month, year);

			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var month_year =  $.trim($('.month_year_picker').val());
				var month = parseInt(month_year.substring(0, 2));
				var year = parseInt(month_year.substring(3));
				month--;

				var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(settings.section_dropdown).val();

				alert(month);
				alert(year);
				// alert(claid);
				// alert(secid);

				search(monthNames[month], year, brid, claid, secid);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.branch_dropdown).val('');
			// $(settings.class_dropdown).val('');
			// $(settings.section_dropdown).val('');
			// removeOptions();
			// removeSectionOptions();

			// // removes all rows
			// $('#atnd-table').find('tbody tr').remove();
			// 
			general.reloadWindow();
		}

	};
};


var monthlyAttendanceReport = new MonthlyAttendanceReport();
monthlyAttendanceReport.init();