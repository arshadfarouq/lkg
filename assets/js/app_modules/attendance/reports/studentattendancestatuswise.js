var StuAtndncStatusWise = function() {

	var settings = {

		from_date : $('#from_date'),
		to_date : $('#to_date'),
		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),
		stdid_dropdown : $('#stdid_dropdown'),
		status_dropdown : $('#status_dropdown'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var fetchStdid = function(col, brid, claid, secid) {

		$.ajax({
			url : base_url + 'index.php/student/fetchStudentByBranchClassSection',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid, 'secid' : secid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateStdid(data);
				} else {
					removeStdidOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateStdid = function(data) {

		var stdidOptions = "";
		$.each(data, function( index, elem ) {

			stdidOptions += "<option value='"+ elem.stdid +"' >"+ elem.stdid +" - "+ elem.name +"</option>";
		});

		$(stdidOptions).appendTo(settings.stdid_dropdown);
	}
	var removeStdidOptions = function() {

		$(stdid_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeStdidOptions();
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid, sec ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	var validateSearch = function() {

		var errorFlag = false;
		var status_dropdown = $(settings.status_dropdown).val();
		var from_date = $(settings.from_date).val();
		var to_date = $(settings.to_date).val();


		// remove the error class first
		$(settings.status_dropdown).removeClass('inputerror');
		$(settings.from_date).removeClass('inputerror');
		$(settings.to_date).removeClass('inputerror');

		if ( status_dropdown === '' || status_dropdown === null ) {
			$(settings.status_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( from_date === '' || from_date === null ) {
			$(settings.from_date).addClass('inputerror');
			errorFlag = true;
		}
		if ( to_date === '' || to_date === null ) {
			$(settings.to_date).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(from, to, brid, claid, secid, stdid, status) {

		$.ajax({
			url : base_url + 'index.php/attendance/studentAttendanceStatusWiseReport',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid, 'stdid' : stdid, 'status' : status, 'from' : from, 'to' : to },
			dataType : 'JSON',
			success : function(data) {

				$('#atnd-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var branch_name = "";
		var class_name = "";
		var section_name = "";
		var stdid = "";
		var counter = 1;

		$.each(data, function(index, elem) {

			if (elem.branch_name !== branch_name) {

				var row = 	"<tr class='level1'>"+
							"<td colspan='3' class='level1row'>"+ elem.branch_name +"</td>"+
							"</tr>";

				$(row).appendTo('#atnd-table tbody');
				branch_name = elem.branch_name;
			}

			if (elem.class_name !== class_name) {

				var row = 	"<tr class='level2'>"+
							"<td colspan='3' class='level2row'>"+ elem.class_name +"</td>"+
							"</tr>";

				$(row).appendTo('#atnd-table tbody');
				class_name = elem.class_name;
			}

			if (elem.section_name !== section_name) {

				var row = 	"<tr class='level3'>"+
							"<td colspan='3' class='level3row'>"+ elem.section_name +"</td>"+
							"</tr>";

				$(row).appendTo('#atnd-table tbody');
				section_name = elem.section_name;
			}

			if (elem.stdid != stdid) {

				var row = 	"<tr class='level4'>"+
							"<td colspan='3' class='level4row'>"+ elem.student_name +"</td>"+
							"</tr>";

				$(row).appendTo('#atnd-table tbody');
				stdid = elem.stdid;
			}

			row = 	"<tr>"+
					"<td>"+ (counter++) +"</td>"+
					"<td>"+ elem.date +"</td>"+
					"<td>"+ elem.status +"</td></tr>";
			$(row).appendTo('#atnd-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {
			// alert('dd');

			var self = this;

			$(".btnPrint").on('click', function () {
              	var etype = 'studentAttandanceStatus';
		        var from = $(settings.from_date).val();
		        var to = $(settings.to_date).val();
		        var brid = $(settings.branch_dropdown).val();
		        var claid = $(settings.class_dropdown).val();
		        var secid = $(settings.section_dropdown).val();
		        var stdid = $(settings.stdid_dropdown).val();
		        var status = $(settings.status_dropdown).val();
	            var className = $('#class_dropdown option:selected').text();
	            var sectionName = $('#section_dropdown option:selected').text();
	            var statusName = $('#status_dropdown option:selected').text();
	            from= from.replace('/','-');
	            from= from.replace('/','-');
	            to= to.replace('/','-');
	            to= to.replace('/','-');
				// alert(className);	            
              	
        	    var url = base_url + 'index.php/doc/pdf_studentattandanceStatusWise/' +etype +  '/' + from +  '/' + to +  '/' + brid +  '/' + claid + '/' + secid +  '/' + stdid +  '/' + status + '/' + className  + '/' + sectionName + '/' + statusName;
              	window.open(url);
              });

			// when fetchSectionNames is changed
			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				// clear tables

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			$(settings.section_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(this).val();

				fetchStdid('stdid', brid, claid, secid);
			});

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var from = $(settings.from_date).val();
				var to = $(settings.to_date).val();
				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(settings.section_dropdown).val();
				var stdid = $(settings.stdid_dropdown).val();
				var status = $(settings.status_dropdown).val();

				search(from, to, brid, claid, secid, stdid, status);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.branch_dropdown).val('');
			// $(settings.class_dropdown).val('');
			// $(settings.section_dropdown).val('');
			// $(settings.stdid_dropdown).val('');

			// removeOptions();
			// removeSectionOptions();
			// removeStdidOptions();

			// // removes all rows
			// $('#atnd-table').find('tbody tr').remove();
			
			general.reloadWindow();
		}

	};
};


var stuAtndncStatusWise = new StuAtndncStatusWise();
stuAtndncStatusWise.init();