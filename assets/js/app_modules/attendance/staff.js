var StaffAttendance = function() {

	var settings = {

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		branch_dropdown : $('.brid'),
		status_dropdown : $('#status_dropdown'),

		counter : 1,

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
		btnSave : $('.btnSave'),
		btnDelete : $('.btnDelete')
	};

	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/attendance/getMaxStaffAtndId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSearch = function() {

		var errorFlag = false;
		return errorFlag;
	}

	var search = function(brid) {

		$.ajax({
			url : base_url + 'index.php/staff/fetchAllTeachers',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				$('#atnd-table').find('tbody tr :not(.dataTables_empty)').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		// removes all rows
		//$('#atnd-table').find('tbody tr :not(.dataTables_empty)').remove();
		var status = $(settings.status_dropdown).val();

		$.each(data, function(index, elem) {

			$('#atnd-table').dataTable().fnAddData( [
				settings.counter++,
				"<span class='name' data-staid='"+ elem.staid +"'>"+ elem.name +"</span>",
				"<span class='designation'>"+ elem.type +"</span>",
				"<input type='text' class='tableInputCell atnd-status' list='status' value='"+ status +"'/>" ]
			);
		});
	}

	var save = function( atndcs, dcno ) {

		$.ajax({
			url : base_url + 'index.php/attendance/saveStaff',
			type : 'POST',
			data : { 'atndcs' : atndcs, 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Attendance saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var post = function( atndcVouchers ) {

		var obj = { 'vouchers' : atndcVouchers };
		$.ajax({
			url : base_url + 'index.php/attendance/postStaff',
			type : 'POST',
			data : { 'postData' : JSON.stringify( obj ) },
			dataType : 'JSON',
			success : function(data) {

				if (data.length === 0) {
					alert('Attendance saved successfully.');
					general.reloadWindow();
				} else {
					showErrorMessage(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var showErrorMessage = function(data) {

		var date = "";
		var brid = "";
		var message = "Following voucher were already saved...\n";
		$.each(data, function(index, elem) {

			if (date !== elem.date || brid !== elem.brid) {
				
				message += elem.date + '        =>        ' + elem.branch_name + '\n';

				date = elem.date;	
				brid = elem.brid;
			}
		});

		alert(message);
		general.reloadWindow();
	}

	var isVoucherAlreadySaved = function(date, dcno) {

		var response = false;
		$.ajax({

			url : base_url + 'index.php/attendance/isVoucherAlreadySaved',
			data : {'date': date, 'dcno' : dcno},
			async : false,
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				response = data;
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});

		return response;
	}

	var getSaveObject = function() {

		var atndcs = [];

		var d = new Date();
		var t = d.toTimeString().substr(0, 8);
		var _postdate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDay() + " " + t;

		var _dcno = $(settings.txtdcnoHidden).val();
		var _brid = $(settings.branch_dropdown).val();
		var _date = $('#current_date').val();

		$('#atnd-table').find('tbody tr').each(function(index, elem) {

			var _staid = $.trim($(this).closest('tr').find('span.name').data('staid'));
			var _status = $.trim($(this).closest('tr').find('input.atnd-status').val());

			var atnd = {

				dcno : _dcno,
				brid : _brid,
				staid : _staid,
				status : _status,
				postdate : _postdate,
				date : _date,
				etype : 'vr_atnd'
			}

			atndcs.push(atnd);
		});

		return atndcs;
	}

	var getSavePostObject = function() {

		var atndcVouchers = [];
		var atndcs = [];
		var _from = $('#from_date').val();
		var _to = $('#to_date').val();
		var _dates = dateRange(_from, _to);

		var _dcno = $(settings.txtdcnoHidden).val();
		var _brid = $(settings.branch_dropdown).val();

		var d = new Date();
		var t = d.toTimeString().substr(0, 8);
		var _postdate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDay() + " " + t;

		$('#atnd-table').find('tbody tr').each(function(index, elem) {
			var _staid = $.trim($(this).closest('tr').find('span.name').data('staid'));
			var _status = $.trim($(this).closest('tr').find('input.atnd-status').val());

			var atnd = {};
			atnd.dcno = _dcno;
			atnd.brid = _brid;
			atnd.staid = _staid;
			atnd.status = _status;
			atnd.postdate = _postdate;
			atnd.date = '';
			atnd.etype = 'vr_atnd';

			atndcs.push(atnd);
		});

		// loop through each date and make entery on each single date
		$.each(_dates, function(index, elemdate) {
			console.log(elemdate);

			var atndVoucher = [];

			$.each(atndcs, function(index, elem) {
				var t = $.extend({}, elem);
				t.date = elemdate;
				atndVoucher.push(t);
			});
			atndcVouchers.push(atndVoucher);
		});

		return atndcVouchers;
	}

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/attendance/fetchStaff',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				settings.counter = 1;
				$('#atnd-table').find('tbody tr :not(.dataTables_empty)').remove();
				if (data === 'false') {
					alert('No data found');
				} else {
					populateVchrData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateVchrData = function(data) {

		$(settings.txtdcno).val(data[0]['dcno']);
		$(settings.txtdcnoHidden).val(data[0]['dcno']);

		$('#current_date').datepicker('update', data[0]['date'].substring(0, 10));

		$.each(data, function(index, elem) {

			$('#atnd-table').dataTable().fnAddData( [
				settings.counter++,
				"<span class='name' data-staid='"+ elem.staid +"'>"+ elem.staff_name +"</span>",
				"<span class='designation'>"+ elem.type +"</span>",
				"<input type='text' class='tableInputCell atnd-status' list='status' value='"+ elem.status +"'/>" ]
			);
		});
	}

	var getbrids = function() {

		var brids = [];
		$('#atnd-table tbody tr span.branch_name').each(function(index, elem){
			var brid = $(this).data('brid');
			if($.inArray(brid, brids) == -1){
				brids.push(brid);
			};
		});

		return brids;
	}

	var isDepartmentAlreadyAdded = function(brid) {

		var error = false;

		$('#atnd-table tbody tr span.branch_name').each(function(index, elem){
			var _brid = $(this).data('brid');
			if (_brid == brid) {
				error = true;
			}
		});

		return error;
	}

	var dateRange = function(from, to) {

		var dates = general.getDateRange(from, to);

		var _dates = [];
		$.each(dates, function(index, elem) {

			var d = elem.getDate();
			var y = elem.getFullYear();
			var m = elem.getMonth() + 1;

			var _date = y + '-' + m + '-' + d;
			_dates.push(_date);
		});

		return _dates;
	}

	var print = function() {
		window.open(base_url + 'application/views/print/attendancevoucher.php', 'Attendance Voucher', 'width=720, height=850');
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/attendance/deleteAttendance',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'vr_atnd' },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});
			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetFrom();
			});

			$(settings.btnDelete).on('click', function(e) {
				e.preventDefault();

				if ( $(settings.txtdcno).val().trim() !== "" ) {
					var dcno = $.trim($(settings.txtdcno).val());
					deleteVoucher(dcno);
				}
			});

			$(settings.txtdcno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtdcno).val().trim() !== "" ) {

						var dcno = $.trim($(settings.txtdcno).val());
						fetch(dcno);
					}
				}
			});

			$('.btnPost').on('click', function(e) {
				e.preventDefault();
				self.initPost();
			});

			$('#autoattendance').on('click', function() {
				$('.post-container').toggleClass('hide');
			});

			$('.btnPrint').on('click', function(e) {
				e.preventDefault();
				print();
			});

			getMaxId();
		},

		initPost : function() {

			var rowsCount = $('#atnd-table').find("tbody tr :not(.dataTables_empty)").length;
			if (rowsCount > 0 ) {

				var from = $('#from_date').val().split('/').join('-');
				var to = $('#to_date').val().split('/').join('-');
				if (from > to) {
					alert('Starting date can\'t be less than ending date!');
				} else {
					var saveObj = getSavePostObject();

					post(saveObj);
				}
			} else {
				alert('No data found to save.');
			}
		},

		initSave : function() {

			var rowsCount = $('#atnd-table').find("tbody tr :not(.dataTables_empty)").length;
			if (rowsCount > 0 ) {

				var saveObj = getSaveObject();
				var dcno = $(settings.txtdcnoHidden).val();

				var date = $('#current_date').val();

				// get all the department id from table
				// var brids = getbrids();

				// this check is made to check that if we are updating the voucher or not if both max val and hidden
				// val are equal its mean that we are saving a new voucher else wise updating voucher
				// and if we are updaing the voucher then the dcno is filled and send to the query along with the brids (department ids)
				// to check that whether the new added departments attendance is already saved or not
				var _dcno = "";
				if ($('#txtMaxdcnoHidden').val() != $('#txtdcnoHidden').val()) {
					_dcno = dcno;
				}
				// checks if voucher is already saved or not
				var isSaved = isVoucherAlreadySaved(date, _dcno);
				$('#atnd-table').find('.duplicate').removeClass('duplicate');

				if (isSaved == false) {
					save( saveObj, dcno );
				} else {
					/*$('#atnd-table tbody tr span.branch_name').each(function(index, elem) {
						var brid = $(elem).data('brid');
						if ($.inArray(brid, isSaved)) {
							$(elem).closest('tr').find('td').addClass('duplicate');
						} else {
							$(elem).closest('tr').find('td').removeClass('duplicate');
						}
					});*/

					alert('Attendnce on that date is already saved.');
				}
			} else {
				alert('No data found to save.');
			}
		},

		initSearch : function() {

			var brid = $(settings.branch_dropdown).val();
			var error = isDepartmentAlreadyAdded(brid);

			if (!error) {
				error = validateSearch();
				if (!error) {
					search(brid);
				} else {
					alert('Correct the errors...');
				}
			} else {
				alert('Department already added!');
			}
		},

		resetFrom : function() {

			$('.inputerror').removeClass('inputerror');
			$('#current_date').datepicker('update', new Date());
			$(settings.branch_dropdown).val('');

			settings.counter = 1;
			// removes all rows
			$('#atnd-table').dataTable().fnClearTable();;

			getMaxId();
			general.setPrivillages();
		}
	}

};

var staffAttendance = new StaffAttendance();
staffAttendance.init();