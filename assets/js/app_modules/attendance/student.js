var StudentAttendance = function() {

	var settings = {

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),

 		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
		btnSave : $('.btnSave')
 	};

 	var getMaxId = function() {

 		$.ajax({

 			url : base_url + 'index.php/attendance/getMaxId',
 			type : 'POST',
 			dataType : 'JSON',
 			success : function(data) {

 				$(settings.txtdcno).val(data);
 				$(settings.txtMaxdcnoHidden).val(data);
 				$(settings.txtdcnoHidden).val(data);
 			}, error : function(xhr, status, error) {
 				console.log(xhr.responseText);
 			}
 		});
 	}

 	// checks for the empty fields
 	var validateSearch = function() {

 		var errorFlag = false;
 		var branch_dropdown = $(settings.branch_dropdown).val();
 		var class_dropdown = $.trim($(settings.class_dropdown).val());
 		var section_dropdown = $.trim($(settings.section_dropdown).val());

 		// remove the error class first
 		$(settings.branch_dropdown).removeClass('inputerror');
 		$(settings.class_dropdown).removeClass('inputerror');
 		$(settings.section_dropdown).removeClass('inputerror');

 		if ( branch_dropdown === '' || branch_dropdown === null ) {
 			$(settings.branch_dropdown).addClass('inputerror');
 			errorFlag = true;
 		}

 		if ( class_dropdown === '' || class_dropdown === null ) {
 			$(settings.class_dropdown).addClass('inputerror');
 			errorFlag = true;
 		}

 		if ( section_dropdown === '' || section_dropdown === null ) {
 			$(settings.section_dropdown).addClass('inputerror');
 			errorFlag = true;
 		}

 		return errorFlag;
 	}

 	// fetch all the sections based on the selected class name
 	var fetchSectionNames = function( brid, claid, sec ) {

 		// clear the previous data
 		removeSectionOptions();

 		$.ajax({
 			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
 			type : 'POST',
 			data : { 'claid' : claid, 'brid' : brid },
 			async : false,
 			dataType : 'JSON',
 			success : function(data) {

 				if (data !== 'false') {
 					populateSectionNames(data);
 				} else {
 					removeSectionOptions();
 				}

 			}, error : function(xhr, status, error) {
 				console.log(xhr.responseText);
 			}
 		});
 	}

 	// removes the options from the select tag
 	var removeSectionOptions = function() {

 		$(settings.section_dropdown).children('option').each(function(){
 			if ($(this).val() !== "") {
 				$(this).remove();
 			}
 		});
 	}

 	var populateSectionNames = function( data, sec ) {

 		var sectionNamesOptions = "";
 		$.each(data, function( index, elem ) {

 			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
 		});

 		$(sectionNamesOptions).appendTo(settings.section_dropdown);
 	}

 	// fetch all the classes based on the selected branch name
 	var fetchClassNames = function( brid ) {

 		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
 					removeOptions();
 				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
 	var removeOptions = function() {

 		$(settings.class_dropdown).children('option').each(function(){
 			if ($(this).val() !== "") {
 				$(this).remove();
 			}
 		});
 	}

 	var populateClassNames = function( data ) {

 		var classNamesOptions = "";
 		$.each(data, function( index, elem ) {

 			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
 		});

 		$(classNamesOptions).appendTo(class_dropdown);
 	}

 	var search = function(brid, claid, secid) {

 		$.ajax({
 			url : base_url + 'index.php/student/fetchStudentByBranchClassSection',
 			type : 'POST',
 			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid },
 			async : false,
 			dataType : 'JSON',
 			success : function(data) {

 				if (data === 'false') {
 					alert('No record found.');
 				} else {
 					populateData(data);
 				}
 			}, error : function(xhr, status, error) {
 				console.log(xhr.responseText);
 			}

 		});
 	}

 	var populateData = function(data) {

		// removes all rows
 		$('#atnd-table tbody tr').remove();
 		var rows = "";
 		$.each(data, function(index, elem) {

 			rows += 	"<tr><td>"+ (index + 1) +"</td>" +
 						"<td><span class='stdid'>"+ elem.stdid +"</span></td>" +
 						"<td><span class='name'>"+ elem.name +"</span></td>" +
 						"<td><span class='fname'>"+ elem.fname +"</span></td>" +
 						"<td><input type='text' class='tableInputCell atnd-status' list='status' value='Present'/></td>" +
 						"<td><input type='text' class='tableInputCell description'/></td></tr>";
 		});
		$(rows).appendTo('#atnd-table tbody');
 	}

 	var save = function( atndcs, dcno ) {

 		var _claid = $(settings.class_dropdown).val();
 		var _secid = $(settings.section_dropdown).val();
 		var _date = $('#current_date').val();

 		$.ajax({
 			url : base_url + 'index.php/attendance/save',
 			type : 'POST',
 			data : { 'atndcs' : atndcs, 'dcno' : dcno, 'claid': _claid, 'secid': _secid, 'date': _date },
 			dataType : 'JSON',
 			success : function(data) {

 				if (data == 'duplicate') {

 					alert('Attendance voucher already saved successfully.');
 				} else {

	 				if (data.error === 'true') {
	 					alert('An internal error occured while saving branch. Please try again.');
	 				} else {
	 					alert('Attendance saved successfully.');
	 					general.reloadWindow();
	 				}
 				}
 			}, error : function(xhr, status, error) {
 				console.log(xhr.responseText);
 			}
 		});
 	}

 	var getSaveObject = function() {

 		var atndcs = [];

 		var _dcno = $(settings.txtdcnoHidden).val();
 		var _brid = $(settings.branch_dropdown).val();
 		var _claid = $(settings.class_dropdown).val();
 		var _secid = $(settings.section_dropdown).val();
 		var _date = $('#current_date').val();

 		$('#atnd-table').find('tbody tr').each(function(index, elem) {

 			var _stdid = $.trim($(this).closest('tr').find('span.stdid').text());
 			var _name = $.trim($(this).closest('tr').find('span.name').val());
 			var _fname = $.trim($(this).closest('tr').find('span.fname').text());
 			var _status = $.trim($(this).closest('tr').find('input.atnd-status').val());
 			var _description = $.trim($(this).closest('tr').find('input.description').val());

 			var atnd = {

 				dcno : _dcno,
 				brid : _brid,
 				secid : _secid,
 				claid : _claid,
 				stdid : _stdid,
 				status : _status,
 				description : _description,
 				date : _date
 			}

 			atndcs.push(atnd);
 		});

 		atndcs = JSON.stringify(atndcs);
 		return atndcs;
 	}

 	var fetch = function(dcno) {

 		$.ajax({
 			url : base_url + 'index.php/attendance/fetch',
 			type : 'POST',
 			data : { 'dcno' : dcno },
 			dataType : 'JSON',
 			success : function(data) {

 				$('#atnd-table').find('tbody tr').remove();
 				if (data === 'false') {
 					alert('No data found');
 				} else {
 					populateVchrData(data);
 					$('.btnSave').attr('disabled', false);
 					general.setUpdatePrivillage();
 				}
 			}, error : function(xhr, status, error) {
 				console.log(xhr.responseText);
 			}
 		});
 	}

 	var populateVchrData = function(data) {

 		$(settings.txtdcno).val(data[0]['dcno']);
 		$(settings.txtdcnoHidden).val(data[0]['dcno']);

 		fetchClassNames(data[0]['brid']);
 		$(settings.class_dropdown).val(data[0]['claid']);

 		fetchSectionNames(data[0]['brid'], data[0]['claid']);
 		$(settings.section_dropdown).val(data[0]['secid']);

 		$('#current_date').datepicker('update', data[0]['date'].substring(0, 10));

 		// removes all rows
 		$('#atnd-table tbody tr').remove();
 		var rows = "";
 		$.each(data, function(index, elem) {

			rows +=		"<tr><td>"+ (index + 1) +"</td>" +
 						"<td><span class='stdid'>"+ elem.stdid +"</span></td>" +
		 				"<td><span class='name'>"+ elem.name +"</span></td>" +
		 				"<td><span class='fname'>"+ elem.fname +"</span></td>" +
		 				"<td><input type='text' class='tableInputCell atnd-status' list='status' value='"+ elem.status +"'/></td>" +
		 				"<td><input type='text' class='tableInputCell description' value='"+ elem.description +"'/></td></tr>";
 		});
 		$(rows).appendTo('#atnd-table tbody');
 	}

 	return {

 		init : function() {

 			this.bindUI();
 		},

 		bindUI : function() {

 			var self = this;

 			$('#txtdcno').on('change', function() {
				var dcno = $.trim($(settings.txtdcno).val());
 				fetch(dcno);
 			});

 			$(settings.btnSave).on('click', function(e) {
 				e.preventDefault();
 				self.initSave();
 			});
 			$(settings.btnSearch).on('click', function(e) {
 				e.preventDefault();
 				self.initSearch();
 			});
 			$(settings.btnReset).on('click', function(e) {
 				e.preventDefault();
 				self.resetFrom();
 			});

 			// when fetchSectionNames is changed
 			$(settings.class_dropdown).on('change', function() {

 				var brid = $(settings.branch_dropdown).val();
 				var claid = $(settings.class_dropdown).val();

 				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
 					fetchSectionNames(brid, claid);
 				}
 			});

 			$(settings.txtdcno).on('keypress', function(e) {

 				// check if enter key is pressed
 				if (e.keyCode === 13) {
 					e.preventDefault();
 					// get the based on the id entered by the user
 					if ( $(settings.txtdcno).val().trim() !== "" ) {

 						var dcno = $.trim($(settings.txtdcno).val());
 						fetch(dcno);
 					}
 				}
 			});

 			// $('body').on('keydown', function(e) {
 			// 	if(e.which == '13') {
 			// 		$('.btnSearch').trigger('click');
 			// 	}
 			// });

 			// $('#atnd-table').on('click', 'tr', function(){
 			// 	var val = $(this).find('.atnd-status').val();
 			// 	if (val == 'Present') {
 			// 		$(this).find('.atnd-status').val('Absent');
 			// 	} else if (val == 'Absent') {
 			// 		$(this).find('.atnd-status').val('Leave');
 			// 	} else {
 			// 		$(this).find('.atnd-status').val('Present');
 			// 	}
 			// });

 			getMaxId();
 			var brid = $(settings.branch_dropdown).val();
 			fetchClassNames(brid);
 		},

 		initSave : function() {
 			var error = validateSearch();

 			if (!error) {

 				var rowsCount = $('#atnd-table').find("tbody tr").length;
 				if (rowsCount > 0 ) {

 					var saveObj = getSaveObject();
 					var dcno = $(settings.txtdcnoHidden).val();

 					save( saveObj, dcno );
 				} else {
 					alert('No data found to save.');
 				}
 			} else {
 				alert('Correct the errors...');
 			}
 		},

 		initSearch : function() {
 			var isValid = validateSearch();

 			if (!isValid) {

 				var brid = $(settings.branch_dropdown).val();
 				var claid = $(settings.class_dropdown).val();
 				var secid = $(settings.section_dropdown).val();

 				search(brid, claid, secid);
 			} else {
 				alert('Correct the errors...');
 			}
 		},

 		resetFrom : function() {

			// $('.inputerror').removeClass('inputerror');
			// $('#current_date').datepicker('update', new Date());
			// $(settings.branch_dropdown).val('');
			// $(settings.class_dropdown).val('');
			// $(settings.section_dropdown).val('');

			//  removes all rows
			// $('#atnd-table').dataTable().fnClearTable();;

			// getMaxId();
			// general.setPrivillages();
 			general.reloadWindow();
 		}
 	}

 };

 var studentAttendance = new StudentAttendance();
 studentAttendance.init();