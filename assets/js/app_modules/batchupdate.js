var BatchUpdate = function() {

	var settings = {

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),
		studentdata_table : $('#studentdata-table'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset')
	};

	// checks for the empty fields
	var validateSearch = function() {

		var errorFlag = false;
		var class_dropdown = $.trim($(settings.class_dropdown).val());
		var section_dropdown = $.trim($(settings.section_dropdown).val());

		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.section_dropdown).removeClass('inputerror');

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// fetch all the sections based on the selected class name
	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			async : false,
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeSectionOptions = function() {
		$(settings.section_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});
		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			async : false,
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(class_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});
		$(classNamesOptions).appendTo(class_dropdown);
	}

	var search = function(brid, claid, secid) {

		$.ajax({
			url : base_url + 'index.php/student/fetchStudentByBranchClassSection',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		// removes all rows
		$(settings.studentdata_table).dataTable().fnClearTable();

		var counter = 0;
		$.each(data, function(index, elem) {

			// add row to the datatable
			$(settings.studentdata_table).dataTable().fnAddData( [
				(counter+1),
				elem.stdid,
				elem.name,
				elem.fname,
				//"<div ContentEditable='true'>"+ elem.rollno +"</div>",
				//"<div ContentEditable='true'>"+ elem.mobile +"</div>",
				"<input type='text' value='"+ elem.rollno +"' class='table-input num'>",
				"<input type='text' value='"+ elem.mobile +"' class='table-input num'>",
				"<a href='' class='btn btn-primary btnRowUpdate' data-stdid='"+ elem.stdid +"'><span class='ion-android-checkmark'></span></a></td>" ]
			);
		});
	}

	// saves the data into the database
	var save = function( studentObj ) {

		$.ajax({
			url : base_url + 'index.php/student/updateBatch',
			type : 'POST',
			data : {'studentObj' : studentObj},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Data updated successfully.');
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getUpdateStudentObj = function(_this) {

		var studentObj = {

			stdid : _this.closest('tr').find('td').eq('1').text(),
			name : _this.closest('tr').find('td').eq('2').text(),
			fname : _this.closest('tr').find('td').eq('3').text(),
			rollno : _this.closest('tr').find('td').eq('4').find('input').val(),
			mobile : _this.closest('tr').find('td').eq('5').find('input').val()
		};

		return studentObj;
	}

	return {

		init : function() {

			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.resetVoucher();
			});

			// when btnSave is clicked
			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.initSearch();
			});

			// when btnRowUpdate is clicked
			$(settings.studentdata_table).on('click', '.btnRowUpdate', function(e) {
				e.preventDefault();

				var studentObj = getUpdateStudentObj($(this));

				save(studentObj);
			});

			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(settings.section_dropdown).val();

				search(brid, claid, secid);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.branch_dropdown).val('');
			// $(settings.class_dropdown).val('');
			// $(settings.section_dropdown).val('');

			// // removes all rows
			// $(settings.studentdata_table).dataTable().fnClearTable();
			// 
			general.reloadWindow();
		}
	}

};

var batchUpdate = new BatchUpdate();
batchUpdate.init();