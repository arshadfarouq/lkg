var Branch = function() {

	var saveBranch = function ( branchObj ) {

		$.ajax({
			url : base_url + 'index.php/branch/save',
			type : 'POST',
			data : { branchDetail : branchObj },
			dataType: 'JSON',
			success : function(data) {
				console.log(data);
				if (data.error === "true") {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Branch saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error: function(xhr, status, error) {
				alert('An internal error occured while saving branch. Please try again.');
				console.log(xhr.responseText);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		$('#txtname').val('');
		$('#txtnameHidden').val('');
		$('#txtphoneNo').val('');
		$('#txtAcTitle').val('');
		$('#txtCreditAc').val('');
		$('#txtaddress').val('');
		$("#txtopeningDate").datepicker("update", new Date());
		$('#txttotalRooms').val('');
		$('#txtBankName').val('');
		$('#txttotalStudents').val('');

		setMaxId();
		general.hideLoader();
	}

	var fetchBranch = function(brid) {

		$.ajax({
			url : base_url + 'index.php/branch/fetchBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$.each(data, function(index, elem){

			$('#txtbranchId').val(elem.brid);
			$('#txtbranchIdHidden').val(elem.brid);
			$('#txtname').val(elem.name);
			$('#txtnameHidden').val(elem.name);
			$('#txtphoneNo').val(elem.phone_no);
			$('#txtaddress').val(elem.address);
			$("#txtopeningDate").datepicker("update", elem.odate.substring(0, 10));
			$('#txttotalRooms').val(elem.classes);
			$('#txtAcTitle').val(elem.actitle);
			$('#txtCreditAc').val(elem.creditac);
			$('#txtBankName').val(elem.bank);
			$('#txttotalStudents').val(elem.students);
		});
	}

	var setMaxId = function() {

		// debugger

		$.ajax({
			url 	: base_url + 'index.php/branch/getMaxId',
			type	: 'POST',
			dataType: 'JSON',
			success	: function(data) {
				$('#txtbranchId').val(data);
				$('#txtMaxbranchIdHidden').val(data);
				$('#txtbranchIdHidden').val(data);
			}, error: function( request, status, error ) {
				alert("Error while fetching branch max id.");
			}
		});

	}

	var validateSaveBranch  = function () {

		var errorFlag = false;

		var name = $('#txtname');

		if ( !name.val() ) {
			name.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// check if the branch name entered by user already defined???
	var isFieldValid = function() {

		var errorFlag = false;
		var name = $('#txtname');		// get the current branch name entered by the user
		var brid = $('#txtbranchIdHidden');		// hidden branch id
		var maxId = $('#txtMaxbranchIdHidden');		// hidden max branch id
		var txtnameHidden = $('#txtnameHidden');		// hidden branch name		

		var branchNames = new Array();
		// get all branch names from the hidden list
		$("#allBranches option").each(function(){
			branchNames.push($(this).text().trim().toLowerCase());
		});

		// if both values are not equal then we are in update mode
		if (brid.val() !== maxId.val()) {

			$.each(branchNames, function(index, elem){

				if (txtnameHidden.val().toLowerCase() !== elem.toLowerCase() && name.val().toLowerCase() === elem.toLowerCase()) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});

		} else {	// if both are equal then we are in save mode

			$.each(branchNames, function(index, elem){

				if (name.val().trim().toLowerCase() === elem) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});
		}

		return errorFlag;
	}

	var getSaveBranchObj = function () {

		var obj = {};
		obj.brid = $.trim($('#txtbranchIdHidden').val());
		obj.name = $.trim($('#txtname').val());
		obj.phone_no = $.trim($('#txtphoneNo').val());
		obj.address = $.trim($('#txtaddress').val());
		obj.odate = $.trim($('#txtopeningDate').val());
		obj.classes = $.trim($('#txttotalRooms').val());
		obj.actitle = $.trim($('#txtAcTitle').val());
		obj.creditac = $.trim($('#txtCreditAc').val());
		obj.bank = $.trim($('#txtBankName').val());
		obj.students = $.trim($('#txttotalStudents').val());

		return obj;
	}


	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('.btnSave').on('click', function(e){
				e.preventDefault();
				self.initSaveBranch();
			});

			$('.btnReset').on('click', function(e) {
				
				e.preventDefault();
				self.resetVoucher();
			});

			$('#txtbranchId').on('keypress', function(e) {
				// when enter key is pressed
				if (e.keyCode === 13) {
					// check if textbox is empty
					if ($('#txtbranchId').val().trim() !== "") {
						var brid = $.trim($('#txtbranchId').val());
						fetchBranch(brid);	// get the branch detail based on id
					}
				}
			});

			$('.btn-edit-branch').on('click', function(e){
				e.preventDefault();
				fetchBranch($(this).data('brid'));	// get the branch detail based on id
			});

			setMaxId();
		},

		initSaveBranch : function () {
			var branchObj = getSaveBranchObj();
			var isValid = validateSaveBranch();

			// checks for empty fields
			if ( !isValid ) {

				// checks if branch name is already used
				if (!isFieldValid()) {					
					saveBranch( branchObj );
				} else {
					alert("Branch name already used.");
				}

			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher  : function() {			
			/*$('.inputerror').removeClass('inputerror');
			$('#txtbranchId').val('');
			$('#txtname').val('');
			$('#txtnameHidden').val('');
			$('#txtphoneNo').val('');
			$('#txtAcTitle').val('');
			$('#txtCreditAc').val('');
			$('#txtaddress').val('');
			$("#txtopeningDate").datepicker("update", new Date());
			$('#txttotalRooms').val('');

			setMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}

	};
};


var branch = new Branch();
branch.init();