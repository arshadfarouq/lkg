var Charge = function() {

	var settings = {

		txtChargeId : $('#txtChargeId'),
		txtMaxChargeIdHidden : $('#txtMaxChargeIdHidden'),
		txtChargeIdHidden : $('#txtChargeIdHidden'),

		txtBranchName : $('.brid'),
		txtAccountName : $('#txtAccountName'),
		txtChargeType : $('#txtChargeType'),

		txtParticulars : $('#txtParticulars'),

		txtCharge : $('#txtCharge'),

		txtTypeNew : $('#txtTypeNew'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnEditCharge : $('.btn-edit-charge'),
		btnNewChargeType : $('.btnNewChargeType')
	};

	var getMaxId = function() {

		var brid = $('.brid').val();
		$.ajax({

			url : base_url + 'index.php/charge/getMaxId',
			type : 'POST',
			data : {'brid': brid},
			dataType : 'JSON',
			success : function(data) {

				$(txtChargeId).val(data);
				$(txtMaxChargeIdHidden).val(data);
				$(txtChargeIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveChargeObj = function () {

		var obj = {

			chid : $.trim($(settings.txtChargeIdHidden).val()),
			description : $.trim($(settings.txtParticulars).val()),
			brid : $.trim($(settings.txtBranchName).val()),
			pid : $.trim($('#hfPartyId').val()),
			charges : $.trim($(settings.txtCharge).val()),
			type : $.trim($(settings.txtChargeType).val())
		};

		return obj;
	}

	// saves the data into the database
	var save = function( chargeObj ) {

		$.ajax({
			url : base_url + 'index.php/charge/save',
			type : 'POST',
			data : { 'chargeDetail' : chargeObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Charge saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		
		$(settings.txtChargeId).val('');
		$(settings.txtMaxChargeIdHidden).val('');
		$(settings.txtChargeIdHidden).val('');
		$(settings.txtChargeType).val('');
		$(settings.txtParticulars).val('');
		$(settings.txtCharge).val('');
		$(settings.txtAccountName).val('');

		getMaxId();
		general.hideLoader();
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var txtBranchName = $.trim($(settings.txtBranchName).val());
		var txtChargeType = $.trim($(settings.txtChargeType).val());
		var txtCharge = $.trim($(settings.txtCharge).val());
		var partyEl = $('#hfPartyId');

		// remove the error class first
		$(settings.txtBranchName).removeClass('inputerror');
		$(settings.txtChargeType).removeClass('inputerror');
		$(settings.txtCharge).removeClass('inputerror');
		$(settings.txtAccountName).removeClass('inputerror');

		if ( txtBranchName === '' ) {
			$(settings.txtBranchName).addClass('inputerror');
			errorFlag = true;
		}
		if ( txtChargeType === '' ) {
			$(settings.txtChargeType).addClass('inputerror');
			errorFlag = true;
		}
		if ( txtCharge === '' ) {
			$(settings.txtCharge).addClass('inputerror');
			errorFlag = true;
		}
		if ( !partyEl.val() ) {
			$('#txtPartyId').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetch = function(chid) {

		var brid = $('.brid').val();

		$.ajax({
			url : base_url + 'index.php/charge/fetchCharge',
			type : 'POST',
			data : { 'chid' : chid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(data) {

		console.log(data);

		$.each(data, function(index, elem){

			$(settings.txtChargeId).val(elem.chid);
			$(settings.txtChargeIdHidden).val(elem.chid);

			$('#hfPartyId').val(data[0]['pid']);
			$('#txtPartyId').val(data[0]['party_name']);




			$("#hfPartyBalance").val(data[0]['party_balance']);
			$("#hfPartyCity").val(data[0]['party_city']);
			$("#hfPartyAddress").val(data[0]['party_address']);
			$("#hfPartyCityArea").val(data[0]['party_cityarea']);
			$("#hfPartyMobile").val(data[0]['party_mobile']);
			$("#hfPartyUname").val(data[0]['party_uname']);
			$("#hfPartyLimit").val(data[0]['party_limit']);
			$(settings.txtChargeType).val(elem.type);

			$(settings.txtParticulars).val(elem.description);

			$(settings.txtCharge).val(elem.charges);
		});
	}
	var clearPartyData = function (){

		$("#hfPartyId").val("");
		$("#hfPartyBalance").val("");
		$("#hfPartyCity").val("");
		$("#hfPartyAddress").val("");
		$("#hfPartyCityArea").val("");
		$("#hfPartyMobile").val("");
		$("#hfPartyUname").val("");
		$("#hfPartyLimit").val("");
		$("#hfPartyName").val("");
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;
			$('#txtPartyId').on('input',function(){
				if($(this).val() == ''){
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
				}
			});

			$('#txtPartyId').on('focusout',function(){
				if($(this).val() != ''){
					var partyID = $('#hfPartyId').val();
					if(partyID == '' || partyID == null){
						$('#txtPartyId').addClass('inputerror');
						$('#txtPartyId').focus();
						$("#imgPartyLoader").show();
					}
				}
				else{
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
				}
			});
			shortcut.add("F10", function(e) {
				e.preventDefault();
				self.initSave();
			});
			

			// $('#txtQty').val(1);


			var countParty = 0;
			$('input[id="txtPartyId"]').autoComplete({
				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/account/searchAccount',
							type: 'POST',
							data: {
								search: search,
								type : 'sale order',
							},
							dataType: 'JSON',
							beforeSend: function (data) {
								$(".loader").hide();
								$("#imgPartyLoader").show();
								countParty = 0;
							},
							success: function (data) {
								if(data == ''){
									$('#txtPartyId').addClass('inputerror');
									clearPartyData();
								}
								else{
									$('#txtPartyId').removeClass('inputerror');
									response(data);
									$("#imgPartyLoader").hide();
								}
							}
						});
					}
					else
					{
						clearPartyData();
					}
				},
				renderItem: function (party, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (party.name).toLowerCase() && countParty == 0) || (search.toLowerCase() != (party.name).toLowerCase() && countParty == 0))
					{
						selected = "selected";
					}
					countParty++;
					clearPartyData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-email="' + party.email + '" data-party_id="' + party.pid + '" data-credit="' + party.balance + '" data-city="' + party.city +
					'" data-address="'+ party.address + '" data-cityarea="' + party.cityarea + '" data-mobile="' + party.mobile + '" data-uname="' + party.uname +
					'" data-limit="' + party.limit + '" data-name="' + party.name +
					'">' + party.name.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, party)
				{	
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
					$("#hfPartyId").val(party.data('party_id'));
					$("#hfPartyBalance").val(party.data('credit'));
					$("#hfPartyCity").val(party.data('city'));
					$("#hfPartyAddress").val(party.data('address'));
					$("#hfPartyCityArea").val(party.data('cityarea'));
					$("#hfPartyMobile").val(party.data('mobile'));
					$("#hfPartyUname").val(party.data('uname'));
					$("#hfPartyLimit").val(party.data('limit'));
					$("#hfPartyName").val(party.data('name'));
					$("#txtPartyId").val(party.data('name'));
					$("#txtPartyEmail").val(party.data('email'));


					var partyId = party.data('party_id');
					var partyBalance = party.data('credit');
					var partyCity = party.data('city');
					var partyAddress = party.data('address');
					var partyCityarea = party.data('cityarea');
					var partyMobile = party.data('mobile');
					var partyUname = party.data('uname');
					var partyLimit = party.data('limit');
					var partyName = party.data('name');

					

				}
			});


			// when save button is clicked
			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});

			// when reset button is clicked
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			// when text is chenged inside the id textbox
			$(settings.txtChargeId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtChargeId).val().trim() !== "" ) {

						var chid = $.trim($(settings.txtChargeId).val());
						fetch(chid);
					}
				}
			});

			// when add charge type button is clicked
			$('#btnAddChargeType').on('click', function() {

				// reset the value of the txtTypeNew
				$(txtTypeNew).val('');
			});
			$('.btn-edit-charge').on('click', function(e) {
				e.preventDefault();
				fetch($(this).data('chid'));
				$('a[href="#add_Charges"]').trigger('click');
			});

			// when modal add button is clicked
			$('.btnNewChargeType').on('click', function(e) {
				e.preventDefault();

				// get the new charge type
				var txtTypeNew = $.trim($(settings.txtTypeNew).val());

				// check if the new charge type entered is empty or not
				if (txtTypeNew !== '') {
					// append the value to the charges combobox
					$("<option value='"+ txtTypeNew +"' selected>"+ txtTypeNew +"</option>").appendTo(txtChargeType);
				}

				// performs click on the dismiss button of modal
				$(this).siblings().eq(0).trigger('click');
			});


			// when edit button is clicked inside the table view
			$(settings.btnEditCharge).on('click', function(e) {
				e.preventDefault();
				fetch($(this).data('chid'));		// get the subject detail by id
			});

			getMaxId();
		},

		// makes the voucher ready to save
		initSave : function() {
			var chargeObj = getSaveChargeObj();	// returns the charge detail object to save into database
			var isValid = validateSave();			// checks for the empty fields

			if (!isValid) {
				save( chargeObj );		// saves the detail into the database
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {
			general.reloadWindow();
		}
	};
};

var charge = new Charge();
charge.init();