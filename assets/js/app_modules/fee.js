var Fee = function() {

	var settings = {
		txtFeeCatId : $('#txtFeeCatId'),
		txtMaxFeeCatIdHidden : $('#txtMaxFeeCatIdHidden'),
		txtFeeCatIdHidden : $('#txtFeeCatIdHidden'),

		txtName : $('#txtName'),
		txtNameHidden : $('#txtNameHidden'),

		txtDescription : $('#txtDescription'),

		allFeeCategories : $('#allFeeCategories'),

		btn_edit_feeCategory : $('.btn-edit-feeCategory'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset')
	};

	var save = function( feeCatObj ) {

		$.ajax({
			url : base_url + 'index.php/fee/save',
			dataType : 'JSON',
			data : { feeCatDetail : feeCatObj },
			type : 'POST',
			success: function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Fee category saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		
		$(settings.txtFeeCatId).val('');
		$(settings.txtMaxFeeCatIdHidden).val('');
		$(settings.txtFeeCatIdHidden).val('');
		$(settings.txtName).val('');
		$(settings.txtNameHidden).val('');
		$(settings.txtDescription).val('');
		$(settings.allFeeCategories).val('');

		setMaxId();
		general.hideLoader();
	}

	var fetch = function(fcid) {

		var brid = $('.brid').val();
		$.ajax({
			url : base_url + 'index.php/fee/fetchFeeCategory',
			type : 'POST',
			data : { 'fcid' : fcid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$.each(data, function(index, elem){

			$(txtFeeCatId).val(elem.fcid);
			$(txtFeeCatIdHidden).val(elem.fcid);

			$(txtName).val(elem.name);
			$(txtNameHidden).val(elem.name);

			$(txtDescription).val(elem.description);
		});
	}

	// get the max id
	var setMaxId = function() {

		var brid = $('.brid').val();
		$.ajax({
			url : base_url + 'index.php/fee/getMaxId',
			type : 'POST',
			data : { 'brid': brid },
			dataType : 'JSON',
			success : function(data) {
				$(txtFeeCatId).val(data);
				$(txtMaxFeeCatIdHidden).val(data);
				$(txtFeeCatIdHidden).val(data);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var name = $.trim($(settings.txtName).val());

		if ( name === "" ) {
			$(settings.txtName).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	//  checks if the name is already used?
	var isFieldValid = function() {

		var errorFlag = false;
		var name = settings.txtName;		// get the current fee category name entered by the user
		var fcid = settings.txtFeeCatIdHidden;		// hidden fcid
		var maxId = settings.txtMaxFeeCatIdHidden;		// hidden max fcid
		var txtnameHidden = settings.txtNameHidden;		// hidden fee category name

		var feeCategoriesNames = new Array();
		// get all branch names from the hidden list
		$("#allFeeCategories option").each(function(){
			feeCategoriesNames.push($(this).text().trim().toLowerCase());
		});

		// if both values are not equal then we are in update mode
		if (fcid.val() !== maxId.val()) {

			$.each(feeCategoriesNames, function(index, elem){

				if (txtnameHidden.val().toLowerCase() !== elem.toLowerCase() && name.val().toLowerCase() === elem.toLowerCase()) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});

		} else {	// if both are equal then we are in save mode

			$.each(feeCategoriesNames, function(index, elem){

				if (name.val().trim().toLowerCase() === elem) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});
		}

		return errorFlag;
	}

	// returns the fee category object to save into database
	var getSaveFeeCategoryObj = function() {

		return obj = {

			fcid : $.trim($(txtFeeCatIdHidden).val()),
			name : $.trim($(txtName).val()),
			description : $.trim($(txtDescription).val()),
			brid : $.trim($('.brid').val())
		};
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {
			var self = this;

			// when save button is clicked
			$(settings.btnSave).on('click', function(e){
				e.preventDefault();		// prevent the default behaviour of the link
				self.initSave();		// makes the voucher ready to save
			});

			// when reset button is clicked
			$(settings.btnReset).on('click', function(e){
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			// when text is changed inside the fee category id
			$(txtFeeCatId).on('keypress', function(e) {

				// check if enter key is pressed
				if ( e.keyCode === 13 ) {

					// get the based on the id entered by the user
					if ( $(txtFeeCatId).val().trim() !== "" ) {

						var fcid = $.trim($(txtFeeCatId).val());
						fetch(fcid);
					}
				}
			});

			// when edit button is clicked inside the table
			$(settings.btn_edit_feeCategory).on('click', function(e) {
				e.preventDefault();
				fetch($(this).data('fcid'));		// get the fee category detail by id
			});

			setMaxId();		// gets the max id of voucher
		},

		// makes the voucher ready to save
		initSave : function() {
			var feeCatObj = getSaveFeeCategoryObj();	// returns the fee category object to save into database
			var isValid = validateSave();		// checks for the empty fields

			if ( !isValid ) {

				// check if the fee category name is already used??	if false
				if (!isFieldValid()) {

					// save the data in to the database
					save( feeCatObj );

				} else {	// if fee category name is already used then show error
					alert("Category name already used.");
				}

			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {
			/*$('.inputerror').removeClass('inputerror');
			$(txtFeeCatId).val('');
			$(txtName).val('');
			$(txtNameHidden).val('');
			$(txtDescription).val('');
			$(allFeeCategories).val('');

			setMaxId();		// gets the max id of voucher
			general.setPrivillages();*/

			general.reloadWindow();
		}
	};

};

var fee = new Fee();
fee.init();