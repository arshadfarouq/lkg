var AssignFee = function() {

	var settings = {

		totalAmount : 0,
		totalConcession : 0,
		totalNetAmount : 0,
		total : 0,

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		stdid_dropdown : $('#stdid_dropdown'),

		txtBranchName : $('#txtBranchName'),
		txtClassName : $('#txtClassName'),
		txtSectionName : $('#txtSectionName'),
		txtFeeCategory : $('#txtFeeCategory'),

		particulars_dropdown : $('#particulars_dropdown'),
		txtCharges : $('#txtCharges'),
		txtConcession : $('#txtConcession'),
		txtTotal : $('#txtTotal'),

		current_date : $('#current_date'),
		last_date : $('#last_date'),
		txtLateFee : $('#txtLateFee'),

		charges_table : $('#charges_table'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnAddCharges : $('#btnAddCharges'),
		btnDelete : $('.btnDelete')
	};

	var save = function( admiObj) {

		$.ajax({

			url : base_url + 'index.php/fee/saveAdmissionCharges',
			type : 'POST',
			data : { 'ledgers' : admiObj.l, 'main' : admiObj.m, 'detail' : admiObj.d, 'dcno' : admiObj.dcno, 'etype' : 'admission charges' },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Voucher saved successfully.');
					general.reloadWindow();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var Print_FeeRecevie = function() {
		
		window.open(base_url + 'application/views/print/admissionChargesPrint.php', 'Admission Charges', 'width=600, height=1000');
		

	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/fee/getMaxFeeIssuanceId',
			type : 'POST',
			data : { 'etype' : 'admission charges' },
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateEntry = function() {


		var errorFlag = false;
		var particulars = $(settings.particulars_dropdown).val();
		var amount = $(settings.txtCharges).val();

		// remove the error class first
		$(settings.particulars_dropdown).removeClass('inputerror');
		$(settings.txtCharges).removeClass('inputerror');

		if ( particulars === '' || particulars === null ) {
			$(settings.particulars_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( amount === '' || amount === null ) {
			$(settings.txtCharges).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var populateFooter = function(amount, concession, total) {

		concession = (concession == '') ? 0 : concession;
		settings.totalAmount = parseFloat(settings.totalAmount) + parseFloat(amount);
		settings.totalConcession = parseFloat(settings.totalConcession) + parseFloat(concession);
		settings.totalNetAmount = parseFloat(settings.totalNetAmount) + parseFloat(total);

		$('#txtTotalAmount').text(settings.totalAmount);
		$('#txtTotalConcession').text(settings.totalConcession);
		$('#txtTotalNetAmount').text(settings.totalNetAmount);
	}

	var appendToTable = function(chid, amount, particulars, pid, concession, total) {

		populateFooter(amount, concession, total);
		
		var srno = $('#charges_table tbody tr').length;
		

		var row = "";
		row = 	"<tr>"+
<<<<<<< HEAD
						"<td class='particulars'> "+ particulars +"</td>"+
						"<td class='charges'> "+ amount +"</td>"+
						"<td class='concession'> "+ concession +"</td>"+
						"<td class='total'> "+ total +"</td>"+
						"<td><a href='' class='btn btn-primary btnRowEdit' data-chid="+ chid +" data-pid="+ pid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>"+
				"</tr>";
=======
		"<td> "+ srno +"</td>"+
		"<td> "+ particulars +"</td>"+
		"<td> "+ amount +"</td>"+
		"<td> "+ concession +"</td>"+
		"<td> "+ total +"</td>"+
		"<td><a href='' class='btn btn-primary btnRowEdit' data-chid="+ chid +" data-pid="+ pid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>"+
		"</tr>";
>>>>>>> 6b40ffc15cc7b94db85a65628c5d7dcccb9f6ef1
		$(row).insertBefore('#charges_table tbody tr.footer');
	}

	var getSaveObject = function() {

		var feeMain = {};
		var feeDetail = [];
		var ledgers = [];

		var _stdid = $(settings.stdid_dropdown).val();
		var _sel = $(settings.stdid_dropdown).find('option:selected');
		var _student_name = _sel.data('student_name');
		var _class_name = _sel.data('class_name');
		var _claid = _sel.data('claid');
		var _brid = _sel.data('brid');
		var _secid = _sel.data('secid');
		var _fid = _sel.data('fid');
		var _pid = _sel.data('pid');

		var d = new Date();
		var _date = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();


		feeMain.fmid = '';
		feeMain.stdid = _stdid;
		feeMain.totfee = $.trim($('#txtTotalNetAmount').text());
		feeMain.date = $(settings.current_date).val();
		feeMain.etype = 'admission charges';
		feeMain.dcno = $(settings.txtdcnoHidden).val();
		feeMain.claid = _claid;
		feeMain.brid = _brid;
		feeMain.secid = _secid;
		feeMain.datefrom = _date;
		feeMain.dateto = _date;
		feeMain.lastdate = $(settings.last_date).val();
		feeMain.latefee = $(settings.txtLateFee).val();
		feeMain.fid = _fid;

		
		$(charges_table).find('tbody tr').each(function( index, elem ) {

			if (!$(elem).hasClass('footer')) {

				var _chid = $.trim($(elem).find('td a.btnRowEdit').data('chid'));
				var _pid_ = $.trim($(elem).find('td a.btnRowEdit').data('chid'));
				var name = $.trim($(elem).find('td').eq(1).text());
				var amount = $.trim($(elem).find('td').eq(2).text());
				var concession = $.trim($(elem).find('td').eq(3).text());
				var total = $.trim($(elem).find('td').eq(4).text());

				var detail = {};
				detail.chno = _chid;
				detail.amount = amount;
				detail.concession = concession;
				detail.namount = total;
				detail.brid = _brid;
				feeDetail.push(detail);

				var pledger = {};
				pledger.pledid = '';
				pledger.pid = _pid;
				pledger.description = _student_name + " ID " + _stdid + " " + _class_name;
				pledger.date = _date;
				pledger.debit = total;
				pledger.credit = 0;
				pledger.dcno = $(settings.txtdcnoHidden).val();
				pledger.etype = 'admission charges';
				pledger.chid = '';
				pledger.pid_key = _pid;
				pledger.brid = _brid;
				ledgers.push(pledger);

				var pledger = {};
				pledger.pledid = '';
				pledger.pid = _pid_;
				pledger.description = name;
				pledger.date = _date;
				pledger.debit = 0;
				pledger.credit = total;
				pledger.dcno = $(settings.txtdcnoHidden).val();
				pledger.etype = 'admission charges';
				pledger.chid = _chid;
				pledger.pid_key = _pid;
				pledger.brid = _brid;
				ledgers.push(pledger);
			}
		});

		var data = {};
		data.m = feeMain;
		data.d = feeDetail;
		data.l = ledgers;
		data.dcno = $(settings.txtdcnoHidden).val();

		return data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var stdid_dropdown = $(settings.stdid_dropdown).val();

		// remove the error class first
		$(settings.stdid_dropdown).removeClass('inputerror');

		if ( stdid_dropdown === '' || stdid_dropdown === null ) {
			$(settings.stdid_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchRecords = function(dcno) {

		$.ajax({

			url : base_url + 'index.php/fee/fetchStudentsWithCharges',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'admission charges' },
			dataType : 'JSON',
			success : function(data) {

				reset();

				if (data === 'false') {
					alert('No record found.');
					$('.btnSave').removeAttr('disabled');
				} else {
					populateConfigurationData(data.students);
					populcateTableBody(data.charges);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var reset = function() {
		$('.inputerror').removeClass('inputerror');
		$(settings.stdid_dropdown).val('');
		$(settings.current_date).datepicker('update', new Date);
		$(settings.last_date).datepicker('update', new Date);

		settings.totalAmount = 0;
		settings.totalConcession = 0;
		settings.totalNetAmount = 0;
		settings.total = 0;

		clearTable();

		$(settings.txtLateFee).val('');

		$(settings.txtBranchName).val('');
		$(settings.txtClassName).val('');
		$(settings.txtSectionName).val('');
		$(settings.txtFeeCategory).val('');
	}

	var populateConfigurationData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtdcno).val(elem.dcno);
			$(settings.txtdcnoHidden).val(elem.dcno);

			$(settings.stdid_dropdown).val(elem.stdid);
			var stdid = $(settings.stdid_dropdown).val();
			var sel = $(settings.stdid_dropdown).find('option:selected');
			var student_name = sel.data('student_name');
			var claid = sel.data('claid');
			var brid = sel.data('brid');
			var secid = sel.data('secid');
			var fid = sel.data('fid');
			var pid = sel.data('pid');
			var class_name = sel.data('class_name');
			var fc_name = sel.data('fc_name');
			var branch_name = sel.data('branch_name');
			var section_name = sel.data('section_name');

			setStudentValues(class_name, fc_name, branch_name, section_name,student_name);

			$(settings.current_date).datepicker('update', elem.date.substring(0, 10));
			$(settings.last_date).datepicker('update', elem.lastdate.substring(0, 10));
			$(settings.txtLateFee).val(elem.latefee);
		});
	}

	var clearTable = function() {

		$(settings.charges_table).find('tbody tr').each(function(index, elem) {

			if (!$(elem).hasClass('footer')) {
				$(elem).remove();
			}
		});

		$('#txtTotalAmount').text('----');
		$('#txtTotalConcession').text('----');
		$('#txtTotalNetAmount').text('----');
	}

	var populcateTableBody = function(charges) {

		clearTable();
		$.each(charges, function(index, elem) {

			appendToTable(elem.chid, elem.amount, elem.description, elem.charges_pid, elem.concession, elem.namount);
		});
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/fee/deleteVoucherOtherIsuAdmi',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'admission charges' },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var setStudentValues = function(class_name, fc_name, branch_name, section_name,student_name) {

		$(settings.txtBranchName).val(branch_name);
		$(settings.txtClassName).val(class_name);
		$(settings.txtSectionName).val(section_name);
		$(settings.txtFeeCategory).val(fc_name);
		$('#txtStudentName').val(student_name);


	}

	var changeTotal = function() {

		var charges = ($(settings.txtCharges).val() == '' ) ?  0 : $(settings.txtCharges).val();
		var concession = ($(settings.txtConcession).val() == '' ) ?  0 : $(settings.txtConcession).val();
		var total = parseFloat(charges) - parseFloat(concession);

		$(settings.txtTotal).val(total);
	}

	var fetchLookupstudents = function () {
		$.ajax({
			url : base_url + 'index.php/student/searchStudentAll',
			type: 'POST',
			data: {'search': '', 'type':'student'},
			dataType: 'JSON',
			success: function (data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataLoookupStudent(data);
				}
			}, error: function (xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataLoookupStudent = function (data) {

		if (typeof dTable != 'undefined') {
			dTable.fnDestroy();
			$('#tblStudents > tbody tr').empty();
		}

		var html = "";
		$.each(data, function (index, elem) {

			html += "<tr>";
			html += "<td width='14%;'>"+ elem.stdid +"<input type='hidden' name='hfModalStudentId' value='"+elem.stdid+"' ></td>";
			html += "<td>"+ elem.student_name +"</td>";
			html += "<td>"+ elem.mobile +"</td>";
			html += "<td>"+ elem.address +"</td>";
			html += "<td>"+ elem.class_name +"</td>";
			html += "<td>"+ elem.section_name +"</td>";
			html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateStudent'><i class='fa fa-search'></i></a></td>";
			html += "</tr>";
		});

		$("#tblStudents > tbody").html('');
		$("#tblStudents > tbody").append(html);
		bindGridStudents();
	}
	var bindGridStudents = function() {

		$('.modal-lookup .populateStudent').on('click', function () {
			var stdid = $(this).closest('tr').find('input[name=hfModalStudentId]').val();
			$('#stdid_dropdown').val(stdid);
			$('#stdid_dropdown').trigger('change');
			$('#particulars_dropdown').select2('open');

			
		});

		var dontSort = [];
		$('#tblStudents thead th').each(function () {
			if ($(this).hasClass('no_sort')) {
				dontSort.push({ "bSortable": false });
			} else {
				dontSort.push(null);
			}
		});
		dTable = $('#tblStudents').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 10,
            "oTableTools": {
            	"sSwfPath": "js/copy_cvs_xls_pdf.swf",
            	"aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
		$.extend($.fn.dataTableExt.oStdClasses, {
			"s`": "dataTables_wrapper form-inline"
		});
	}


	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('.btnsearchstudent').on('click',function(e){
				e.preventDefault();

				var length = $('#tblStudents > tbody tr').length;
				
				if(length <= 1){
					fetchLookupstudents();
				}
			});

			shortcut.add("F1", function(e) {
				e.preventDefault();
				$('a[href="#student-lookup"]').trigger('click');
			});

			$('#txtdcno').on('change', function() {
				settings.totalAmount = 0;
				settings.totalConcession = 0;
				settings.totalNetAmount = 0;
				settings.total = 0;

				var dcno = $.trim($(this).val());
				fetchRecords(dcno);
			});
			$(".btnPrint").on('click', function (e) {
				
				e.preventDefault();
				Print_FeeRecevie();

			});

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});

			$(settings.stdid_dropdown).on('change', function() {

				var stdid = $(this).val();
				var sel = $(settings.stdid_dropdown).find('option:selected');
				
				var student_name = sel.data('student_name');
				var claid = sel.data('claid');
				var brid = sel.data('brid');
				var secid = sel.data('secid');
				var fid = sel.data('fid');
				var pid = sel.data('pid');
				var class_name = sel.data('class_name');
				var fc_name = sel.data('fc_name');
				var branch_name = sel.data('branch_name');
				var section_name = sel.data('section_name');

				setStudentValues(class_name, fc_name, branch_name, section_name,student_name);
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$(settings.particulars_dropdown).on('change', function() {
				var amount = $(this).find('option:selected').data('amount');
				$(settings.txtCharges).val(amount);
				changeTotal();
			});

			$(settings.txtCharges).on('keyup', function() {
				changeTotal();
			});

			$(settings.txtConcession).on('keyup', function() {
				changeTotal();
			});

			$(settings.btnAddCharges).on('click', function(e) {
				e.preventDefault();

				var chid = $(settings.particulars_dropdown).val();
				var amount = $(settings.txtCharges).val();
				var particulars = $(settings.particulars_dropdown).find('option:selected').text();
				var pid = $(settings.particulars_dropdown).find('option:selected').data('pid');
				var concession = $.trim($(settings.txtConcession).val());
				var total = $.trim($(settings.txtTotal).val());

				var error = validateEntry();
				if (!error) {

					appendToTable(chid, amount, particulars, pid, concession, total);

					$(settings.particulars_dropdown).val('');
					$(settings.txtCharges).val('');
					$(settings.txtConcession).val('');
					$(settings.txtTotal).val('');
					$('#particulars_dropdown').select2('open');

				} else {
					alert('Correct the errors...');
				}
			});

			// when btnRowRemove is clicked
			$(settings.charges_table).on('click', '.btnRowRemove', function(e) {
				e.preventDefault();

				var amount = $.trim($(this).closest('tr').find('td').eq(2).text());
				var concession = $.trim($(this).closest('tr').find('td').eq(3).text());
				var total = $.trim($(this).closest('tr').find('td').eq(4).text());
				populateFooter(parseFloat(amount) * -1, parseFloat(concession) * -1, parseFloat(total) * -1);

				$(this).closest('tr').remove();
			});

			$(settings.charges_table).on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var chid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('chid'));
				var amount = $.trim($(this).closest('tr').find('td').eq(2).text());
				var concession = $.trim($(this).closest('tr').find('td').eq(3).text());
				var total = $.trim($(this).closest('tr').find('td').eq(4).text());

				concession = (concession == '') ? 0 : concession;
				populateFooter(parseFloat(amount) * -1, parseFloat(concession) * -1, parseFloat(total) * -1);

				$(settings.particulars_dropdown).select2('val',chid);
				$(settings.txtCharges).val(amount);
				$(txtConcession).val(concession);
				$(txtTotal).val(total);
				settings.total = parseFloat(total);

				$(settings.particulars_dropdown).select2('opnen');


				$(this).closest('tr').remove();
			});

			// when text is changed in txtStudentId
			$(settings.txtdcno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtdcno).val().trim() !== "" ) {

						settings.totalAmount = 0;
						settings.totalConcession = 0;
						settings.totalNetAmount = 0;
						settings.total = 0;

						var dcno = $.trim($(settings.txtdcno).val());
						fetchRecords(dcno);
					}
				}
			});

			$('#txtConcession,#txtCharges').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					
					$('#btnAddCharges').click();
				}
			});

			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();

				var dcno = $(settings.txtdcno).val();
				deleteVoucher(dcno);
			});

			getMaxId();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var admiObj = getSaveObject();
			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.charges_table).find('tbody tr').length;

				if (rowsCount > 1 ) {

					save(admiObj);
				} else {
					alert('No data found to save.');
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.stdid_dropdown).val('');
			$(settings.current_date).datepicker('update', new Date);
			$(settings.last_date).datepicker('update', new Date);

			settings.totalAmount = 0;
			settings.totalConcession = 0;
			settings.totalNetAmount = 0;
			settings.total = 0;

			clearTable();

			$(settings.txtLateFee).val('');

			$(settings.txtBranchName).val('');
			$(settings.txtClassName).val('');
			$(settings.txtSectionName).val('');
			$(settings.txtFeeCategory).val('');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var assignFee = new AssignFee();
assignFee.init();