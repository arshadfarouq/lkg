var FeeIssuance = function() {

	var settings = {

		txtdcno : $('#txtdcno'),
		txtMaxdcnoHidden : $('#txtMaxdcnoHidden'),
		txtdcnoHidden : $('#txtdcnoHidden'),

		fromMonth_date : $('#fromMonth_date'),
		toMonth_date : $('#toMonth_date'),
		last_date : $('#last_date'),
		current_date : $('#current_date'),

		txtLateFeeFine : $('#txtLateFeeFine'),

		branch_name : $('#txtbranchname'),
		branch_Address : $('#txtbranchAddress'),
		branch_Phone : $('#txtbranchPhone'),


		category_dropdown : $('#category_dropdown'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),

		students_table : $('#students_table'),

		btnSearch : $('.btnSearch'),
		btnSave : $('.btnSave'),
		btnDelete : $('.btnDelete'),
		btnPrint : $('.btnPrint'),
		btnReset : $('.btnReset')
	}

	// gets the max id of the voucher
	var getMaxId = function() {
		$.ajax({

			url : base_url + 'index.php/fee/getMaxFeeIssuanceId',
			type : 'POST',
			data : { 'etype' : 'fee issue' },
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtdcno).val(data);
				$(settings.txtMaxdcnoHidden).val(data);
				$(settings.txtdcnoHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	

	var getSaveObject = function() {

		var studs = [];
		var ledgers = [];

		$(students_table).find('tbody tr').each(function( index, elem ) {

			var stuData = {};
			var studMain = {};

			studMain.fmid = '';
			studMain.stdid = $(elem).find('td').eq(0).text();
			studMain.totfee = $(elem).find('td').last().text();
			studMain.date = $(settings.current_date).val();
			studMain.etype = 'fee issue';
			studMain.dcno = $(settings.txtdcnoHidden).val();
			studMain.claid = $(settings.class_dropdown).val();
			studMain.brid = $('.brid').val();
			studMain.secid = $(elem).find('td').eq(0).data('secid');
			studMain.datefrom = $(settings.fromMonth_date).val();
			studMain.dateto = $(settings.toMonth_date).val();
			studMain.lastdate = $(settings.last_date).val();
			studMain.latefee = $(settings.txtLateFeeFine).val();
			studMain.fid = $(elem).find('td').eq(0).data('fid');

			var studDetail = [];
			$(elem).find('.charges').each(function(indexC, elemC) {

				var detail = {
					chno : $(elemC).data('chid'),
					arears : $(elemC).data('arears'),
					amount : $(elemC).text(),
					brid : $('.brid').val(),
					namount : $(elem).find('td').last().text(),
				};

				var pledger = {};
				pledger.pledid = '';
				pledger.pid = $(elemC).data('cpid');
				pledger.description = '';
				pledger.date = $(settings.current_date).val();
				pledger.debit = '';
				pledger.credit = $(elemC).text();
				pledger.dcno = $(settings.txtdcnoHidden).val();
				pledger.etype = 'fee issue';
				pledger.brid = $('.brid').val();
				pledger.company_id= $('.company_id').val();
				pledger.chid = $(elemC).data('chid');
				pledger.pid_key = $(elem).find('td').eq(0).data('pid');
				// alert(pledger.pid_key);

				ledgers.push(pledger);

				var pledger = {};
				pledger.pledid = '';
				pledger.pid = $(elem).find('td').eq(0).data('pid');
				pledger.description = '';
				pledger.date = $(settings.current_date).val();
				pledger.debit = $(elemC).text();
				pledger.credit = '';
				pledger.dcno = $(settings.txtdcnoHidden).val();
				pledger.etype = 'fee issue';
				pledger.brid = $('.brid').val();
				pledger.company_id= $('.company_id').val();
				pledger.chid = $(elemC).data('chid');
				pledger.secid = $(elem).find('td').eq(0).data('secid');
				pledger.pid_key = $(elemC).data('cpid');
				ledgers.push(pledger);

				studDetail.push(detail);
			});

			stuData.main = studMain;
			stuData.detail = studDetail;

			studs.push(stuData);
		});

		var data = {};
		data.studs = JSON.stringify(studs);
		data.ledgers = JSON.stringify(ledgers);
		data.dcno = $(settings.txtdcnoHidden).val();

		return data;
	}
	var validateSave = function() {

		var errorFlag = false;
		var category_dropdown = $.trim($(settings.category_dropdown).val());
		var class_dropdown = $.trim($(settings.class_dropdown).val());
		var branch_dropdown = $.trim($('.brid').val());

		var fromMonth_date = $.trim($(settings.fromMonth_date).val());
		var toMonth_date = $.trim($(settings.toMonth_date).val());
		var last_date = $.trim($(settings.last_date).val());
		var current_date = $.trim($(settings.current_date).val());

		// remove the error class first
		$(settings.category_dropdown).removeClass('inputerror');
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.fromMonth_date).removeClass('inputerror');
		$(settings.toMonth_date).removeClass('inputerror');
		$(settings.last_date).removeClass('inputerror');
		$(settings.current_date).removeClass('inputerror');

		if ( category_dropdown === '' || category_dropdown === null ) {
			$(settings.category_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( fromMonth_date === '' || fromMonth_date === null ) {
			$(settings.fromMonth_date).addClass('inputerror');
			errorFlag = true;
		}

		if ( toMonth_date === '' || toMonth_date === null ) {
			$(settings.toMonth_date).addClass('inputerror');
			errorFlag = true;
		}

		if ( last_date === '' || last_date === null ) {
			$(settings.last_date).addClass('inputerror');
			errorFlag = true;
		}

		if ( current_date === '' || current_date === null ) {
			$(settings.current_date).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var save = function(saveObj) {

		$.ajax({

			url : base_url + 'index.php/fee/saveFeeIssuance',
			type : 'POST',
			data : { 'ledgers' : saveObj.ledgers, 'studs' : saveObj.studs, 'dcno' : saveObj.dcno, 'etype' : 'fee issue' },
			dataType : 'JSON',
			success : function(data) {

				console.log(data);

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					if (confirm("Do you want to take print?")) {
						printVouchers();
					} else {
						general.reloadWindow();
					}
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var printVouchers = function() {
		
// alert('pakista');
window.open(base_url + 'application/views/print/feevouchers.php', 'Admission Form', 'width=900, height=842');
		// // hides the challan numbers so that they can be accessed later on to print
		// var hiddenData = "<div class='printData' style='display: none;'>";
		// $.each(data, function(index, elem) {
		// 	hiddenData += "<p><span class='id'>"+ elem.id +"</span><span class='stdid'>"+ elem.stdid +"</span><span class='arears'>"+ elem.arears +"</span></p>";
		// });
		// hiddenData += "</div>";
		// $(hiddenData).appendTo('body');

		// // open the print window
		// FeeIssuance.child = window.open(base_url + 'application/views/print/feevouchers.php', 'Fee Vouchers', 'width=900, height=842')
		// FeeIssuance.timer = setInterval(checkChild, 500);
	}

	// reloads the window when the child window is closed
	var checkChild = function() {

		if (FeeIssuance.child.closed) {
			clearInterval(FeeIssuance.timer);
			general.reloadWindow();
		}
	}

	var fetchRecords = function(dcno) {

		$.ajax({

			url : base_url + 'index.php/fee/fetchStudentsWithCharges',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'fee issue' },
			dataType : 'JSON',
			success : function(data) {
				
				$('#students_table tbody tr').remove();
				
				if (data === 'false') {
					alert('No record found.');
				} else {

					populcateTableHead(data.charges);
					populateConfigurationData(data.students);
					populcateTableBody(data.students, data.charges);

					// convert the table to the datatable
					// $(settings.students_table).dataTable();
					// $('.btnSave').attr('disabled', true);
					general.setUpdatePrivillage();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateConfigurationData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtdcno).val(elem.dcno);
			$(settings.txtdcnoHidden).val(elem.dcno);

			$(settings.current_date).datepicker('update', elem.date.substring(0, 10));
			$(settings.fromMonth_date).datepicker('update', elem.datefrom.substring(0, 10));
			$(settings.toMonth_date).datepicker('update', elem.dateto.substring(0, 10));
			$(settings.last_date).datepicker('update', elem.lastdate.substring(0, 10));
			$(settings.txtLateFeeFine).val(elem.latefee);

			$(settings.branch_name).val(elem.Brach_name);
			
			$(settings.branch_Address).val(elem.address);
			$(settings.branch_Phone).val(elem.phone_no);

			$(settings.category_dropdown).val(elem.fid);
			$(settings.class_dropdown).val(elem.claid);
		});
	}


	var search = function(searchObj) {

		$.ajax({

			url : base_url + 'index.php/fee/getStudentsWithChargesForFeeIssuance',
			type : 'POST',
			data : { 'brid' : searchObj.brid, 'claid' : searchObj.claid, 'fcid' : searchObj.fcid ,'from':$('#fromMonth_date').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No record found.');
				} else {
					console.table(data.concessions);
					populcateTableHeadSave(data.charges);
					populcateTableBodySave(data.students, data.charges,data.concessions);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populcateTableHeadSave = function(data) {

		var cols = [];
		var counter = 1;
		$.each(data, function(index, elem) {

			if (counter === 1) {
				cols.push('STDID');
				cols.push('Name');
				cols.push('Section');

				cols.push(elem.description);
			} else {
				cols.push(elem.description);
			}

			counter++;
		});
		cols.push('Total');
		tableHead(cols);
	}
	var populcateTableBodySave = function (students, charges,concessions) {

		var row = "";
		$.each(students, function(index, elem) {

			var totalFee = 0.0;

			row +=  "<tr><td data-pid='"+ elem.pid +"' data-secid='"+ elem.secid +"' data-brid='"+ elem.brid +"' data-fid='"+ elem.fid +"'>" + elem.stdid + "</td>" +
			"<td>" + elem.name + "</td>" +
			"<td>" + elem.section_name + "</td>";

			var concession=0;
			var conAmount=0;

			$.each(charges, function(index, elemC) {
				concession = getConcession(concessions,elemC.chid,elem.stdid); 
				if(parseFloat(concession)!=0 && parseFloat(elemC.amount)!=0)
					conAmount = parseInt(parseInt(elemC.amount) * parseFloat(concession)/100);
				else
					conAmount=0;

				row +=	"<td data-arears='"+ elem.arears +"' data-chid='"+ elemC.chid +"' data-cpid='"+ elemC.charges_pid +"' class='charges'>" + parseInt(parseInt(elemC.amount) - parseInt(conAmount)) + "</td>";
				totalFee += parseInt(elemC.amount) - parseInt(conAmount);		// calculates the netamount
			});

			if (elem.feetype === "1/2 Fee Paying") {
				totalFee = ((1 / 2) * totalFee);
			} else if (elem.feetype === "1/3 Fee Paying") {
				totalFee = ((1 / 3) * totalFee);
			} else if (elem.feetype === "3/4 Fee Paying") {
				totalFee = ((3 / 4) * totalFee);
			} else if (elem.feetype === "Full Fee Paying") {
				totalFee = totalFee;
			} else {
				totalFee = 0;
			}

			row += "<td>"+ parseFloat(totalFee).toFixed(0) +"</td></tr>";

		});

		$(row).appendTo(settings.students_table).find('tbody');
	}

	var getConcession = function (concessions, chno,stdid) {

		var valReturn = 0;
		$.each(concessions, function(index, elem) {

		
			if (parseInt(elem.stdid) === parseInt(stdid) && parseInt(elem.chno)===parseInt(chno) ) {
				valReturn = parseFloat(elem.concession);
				return valReturn;
			}
		});

		return valReturn;
		
	}

	var fetch = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/fee/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa ,  'etype':'fee issue'},
			dataType : 'JSON',
			success : function(data) {

				// resetFields();
				$('#txtOrderNo').val('');
				if (data === 'false') {
					alert('No data found.');
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var resetFields=function()
	{

	}
	var populateData = function (students, charges) {

		var row = "";
		$.each(students, function(index, elem) {

			var totalFee = 0.0;

			row +=  "<tr><td data-arears='"+ elem.arears +"' data-pid='"+ elem.pid +"' data-secid='"+ elem.secid +"' data-brid='"+ elem.brid +"' data-fid='"+ elem.fid +"'>" + elem.stdid + "</td>" +
			"<td>" + elem.name + "</td>" +
			"<td>" + elem.section_name + "</td>";


			$.each(charges, function(index, elemC) {

				row +=	"<td data-arears='"+ elem.arears +"' data-chid='"+ elemC.chid +"' data-cpid='"+ elemC.charges_pid +"' class='charges'>" + parseFloat(elemC.amount).toFixed(2) + "</td>";
				totalFee += parseInt(elemC.amount);		// calculates the netamount
			});

			if (elem.feetype === "1/2 Fee Paying") {
				totalFee = ((1 / 2) * totalFee);
			} else if (elem.feetype === "1/3 Fee Paying") {
				totalFee = ((1 / 3) * totalFee);
			} else if (elem.feetype === "3/4 Fee Paying") {
				totalFee = ((3 / 4) * totalFee);
			} else if (elem.feetype === "Full Fee Paying") {
				totalFee = totalFee;
			} else {
				totalFee = 0;
			}

			row += "<td>"+ parseFloat(totalFee).toFixed(0) +"</td></tr>";

		});

		$(row).appendTo(settings.students_table).find('tbody');
	}
	var populcateTableHead = function(data) {

		console.log(data);

		var cols = [];
		var counter = 1;
		var description = [];
		$.each(data, function(index, elem) {

			if (counter == 1) {
				cols.push('STDID');
				cols.push('Name');
				cols.push('Section');
			}

			var index = description.indexOf(elem.description);
			if (index == '-1') {
				cols.push(elem.description);
				description.push(elem.description);
			}

			counter++;
		});
		cols.push('Total');
		tableHead(cols);
	}
	var populcateTableBody = function (students, charges) {

		var row = "";
		$.each(students, function(index, elem) {

			var totalFee = 0.0;

			row +=  "<tr><td class='firstrow' data-arears='"+ elem.arears +"' data-fdid='"+ elem.fdid +"' data-pid='"+ elem.pid +"' data-secid='"+ elem.secid +"' data-brid='"+ elem.brid +"' data-fid='"+ elem.fid +"'>" + elem.stdid + "</td>" +
			"<td>" + elem.name + "</td>" +
			"<td>" + elem.section_name + "</td>";


			$.each(charges, function(index, elemC) {
				if (elem.stdid == elemC.stdid) {
					row +=	"<td data-arears='"+ elem.arears +"' data-chid='"+ elemC.chid +"' data-cpid='"+ elemC.charges_pid +"' class='charges'>" + parseFloat(elemC.amount).toFixed(2) + "</td>";
					totalFee += parseInt(elemC.amount);		// calculates the netamount
				}
			});

			if (elem.feetype === "1/2 Fee Paying") {
				totalFee = ((1 / 2) * totalFee);
			} else if (elem.feetype === "1/3 Fee Paying") {
				totalFee = ((1 / 3) * totalFee);
			} else if (elem.feetype === "3/4 Fee Paying") {
				totalFee = ((3 / 4) * totalFee);
			} else if (elem.feetype === "Full Fee Paying") {
				totalFee = totalFee;
			} else {
				totalFee = 0;
			}

			row += "<td>"+ parseFloat(totalFee).toFixed(0) +"</td></tr>";

		});

		$(row).appendTo(settings.students_table).find('tbody');
	}
	var tableHead = function(cols) {

		var row = "<tr>";
		for (var i = 0; i < cols.length; i++) {

			if (i > 2 && i != (cols.length-1)) {
				row += "<th class='charges'>"+ cols[i] +"</th>";
			} else {
				row += "<th>"+ cols[i] +"</th>";
			}
		};
		row += "</tr>";

		$(row).appendTo('#students_table thead').find('');
	}


	var getSearchObject = function() {

		var obj = {

			fcid : $(settings.category_dropdown).val(),
			brid : $('.brid').val(),
			claid : $(settings.class_dropdown).val()
		}

		return obj;
	}

	// checks for the empty fields
	var validateSearch = function() {

		var errorFlag = false;
		var category_dropdown = $.trim($(settings.category_dropdown).val());
		var class_dropdown = $.trim($(settings.class_dropdown).val());
		var branch_dropdown = $.trim($('.brid').val());

		// remove the error class first
		$(settings.category_dropdown).removeClass('inputerror');
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');

		if ( category_dropdown === '' || category_dropdown === null ) {
			$(settings.category_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( ) {

		// removes the options from the select tag
		removeOptions();

		var brid = $('.brid').val();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			async : false,
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(class_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";

		});
		$(classNamesOptions).appendTo(class_dropdown);
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/fee/deleteVoucherOtherIsuAdmi',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'fee issue' },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$(settings.btnDelete).on('click', function(e) {
				e.preventDefault();

				var dcno = $(settings.txtdcno).val();
				if (dcno !== '') {
					deleteVoucher(dcno);
				}
			});
			$(settings.btnPrint).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				 // alert('pakistan');
				 printVouchers();
				});

			$(settings.txtdcno).on('keypress', function(e) {

				if (e.keyCode === 13) {
					e.preventDefault();

					var dcno = $(this).val();
					if (dcno !== '') {

						$(settings.students_table).find('tr').remove();
						// $('.btnSave').attr('disabled','disabled');
						fetchRecords( dcno );
					}
				}
			});
			$('.form-control').keypress(function (e) {

				if (e.which == 13) {
					e.preventDefault();
				}
			});
			// $(settings.txtdcno).on('change', function(e) {

			// 	if (e.keyCode === 13) {
			// 		e.preventDefault();

			// 		var dcno = $(this).val();
			// 		if (dcno !== '') {

			// 			$(settings.students_table).find('tr').remove();
			// 			$('.btnSave').attr('disabled','disabled');
			// 			fetchRecords( dcno );
			// 		}
			// 	}
			// });
			$(settings.txtdcno).on('change', function() {
				fetchRecords($(this).val());
			});

			getMaxId();
			
			fetchClassNames('');
		},

		// prepares the data to save it into the database
		initSearch : function() {

			var searchObj = getSearchObject();
			var error = validateSearch();

			if (!error) {
				// clear the previous rows
				$(settings.students_table).find('tr').remove();
				search( searchObj );
			} else {
				alert('Correct the errors...');
			}
		},

		initSave : function() {

			var saveObj = getSaveObject();
			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.students_table).find('tbody tr').length;
				if (rowsCount == 0) {
					alert('No data found to save.');
				} else {
					save(saveObj);
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.txtdcno).val('');
			$(settings.txtdcnoHidden).val('');
			$(settings.txtMaxdcnoHidden).val('');

			$(settings.current_date).datepicker('update', new Date);
			$(settings.fromMonth_date).datepicker('update', new Date);
			$(settings.toMonth_date).datepicker('update', new Date);
			$(settings.last_date).datepicker('update', new Date);
			$(settings.txtLateFeeFine).val('');

			$(settings.category_dropdown).val('');
			$(settings.branch_dropdown).val('');
			$(settings.class_dropdown).val('');
			removeOptions();

			var rowsCount = $(settings.students_table).find('tr').length;
			if (rowsCount > 0) {
				$(settings.students_table).dataTable().fnDestroy();
			}
			$(settings.students_table).find('tr').remove();
			$('.btnSave').removeAttr('disabled');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	};
};

var feeIssuance = new FeeIssuance();
feeIssuance.init();