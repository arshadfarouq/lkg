var FeeRecieve = function() {

	var settings = {

		netAmount : 0,

		txtFeeRecId : $('#txtFeeRecId'),
		txtMaxFeeRecIdHidden : $('#txtMaxFeeRecIdHidden'),
		txtFeeRecIdHidden : $('#txtFeeRecIdHidden'),

		txtStdid : $('#txtStdid'),
		txtClassName : $('#txtClassName'),
		txtSectionName : $('#txtSectionName'),
		txtName : $('#txtName'),
		txtFatherName : $('#txtFatherName'),

		txtvrno : $('#txtvrno'),
		txtvrnoHidden : $('#txtvrnoHidden'),
		current_date : $('#current_date'),
		txtNetAmount : $('#txtNetAmount'),
		txtDiscount : $('#txtDiscount'),
		txtOther : $('#txtOther'),

		accounts_dropdown : $('#accounts_dropdown'),
		particulars_dropdown : $('#particulars_dropdown'),

		txtCharges : $('#txtCharges'),

		charges_table : $('#charges_table'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnAddCharges : $('#btnAddCharges'),
		btnDelete : $('.btnDelete')
	};

	var save = function(feeRecObj) {


		$.ajax({
			
			url : base_url + 'index.php/fee/saveFeeRecieve',
			type : 'POST',
			data : { 'feerec' : feeRecObj.feedata, 'ledgers' : JSON.stringify(feeRecObj.ledger), 'dcno' : feeRecObj.dcno, 'etype' : 'frv' },
			dataType : 'json',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving branch. Please try again.');
				}else if (data.error === 'duplicate') {
					alert('Duplicate Chalan No Found... Please try other.');
					general.reloadWindow();
				} else {
					alert('Voucher saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchFeeAssigned = function(fmid) {

		$.ajax({

			url : base_url + 'index.php/fee/fetchFeeIssue',
			type : 'POST',
			data : { 'fmid' : fmid, 'etype' : 'fee issue' },
			dataType : 'JSON',
			success : function(data) {

				$(settings.charges_table).find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					console.log(data);
					populateConfiDataForFeeIssue(data.charges);
					populateTableData(data.charges);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchFeeIssueByChalan = function(dcno,stdid) {

		$.ajax({

			url : base_url + 'index.php/fee/fetchFeeIssueByChalan',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : 'fee issue','stdid':stdid },
			dataType : 'JSON',
			success : function(data) {

				$(settings.charges_table).find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					var chalanno =  $('#txtvrno').val();
					resetFields();
					getMaxId();
					$('#txtvrno').val(chalanno);
					populateConfiDataForFeeIssue(data.charges);
					populateTableData(data.charges);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var Print_FeeRecevie = function() {
		
		window.open(base_url + 'application/views/print/feereceive.php', 'Fee Recevie', 'width=600, height=1000');
		

	}
	var fetchRecords = function(dcno) {

		$.ajax({

			url : base_url + 'index.php/fee/fetchFeeRecieve',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				$(settings.charges_table).find('tbody tr').remove();
				$('#txtNetAmount').val('');
				
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateConfigurationData(data.feerec);
					populateTableData(data.charges);
					$('.btnSave').attr('disabled', false);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateTableData = function(data) {

		$.each(data, function(index, elem) {
			appendToTable(elem.chid, parseFloat(elem.charge_amount).toFixed(2), elem.charge_name, elem.pid);
		});
	}

	var populateConfiDataForFeeIssue = function(data) {

		$(settings.txtvrnoHidden).val($('#txtvrno').val());

		$(settings.txtNetAmount).val(parseFloat(data[0]['namount']).toFixed(2));
		$(settings.accounts_dropdown).val(data[0]['student_pid']);

		fetch(data[0]['student_pid']);
		// $(settings.current_date).datepicker('update', data[0]['date'].substring(0, 10));

		$('#txtArears').val(parseFloat(data[0]['arears']).toFixed(0));

		console.log(data[0]['lastdate'] < $('#current_date').val());


		if( data[0]['lastdate'] < $('#current_date').val())
			$('#txtOther').val(parseFloat(data[0]['latefee']).toFixed(0));

		settings.netAmount = data[0]['namount'];
	}

	var populateConfigurationData = function(data) {

		$(settings.txtFeeRecId).val(data.frid);
		$(settings.txtFeeRecIdHidden).val(data.frid);

		$(settings.txtDiscount).val(data.discount);
		$(settings.txtOther).val(data.fine);

		$(settings.txtvrno).val(data.rno);

		$('#txtArears').val(data.arears);

		$('#txtUserName').val(data.user_name);


		var date_time =data.date_time;
		date_time = new Date(date_time).toLocaleString()
		$('#txtPostingDate').val(date_time);


		$(settings.txtvrnoHidden).val(data.rno);

		$(settings.txtNetAmount).val(data.netamount);
		$(settings.accounts_dropdown).val(data.pid);
		fetch(data.pid);
		$(settings.current_date).datepicker('update', data.date.substring(0, 10));

		settings.netAmount = data.netamount;
	}

	var fetch = function(pid) {

		$.ajax({
			url : base_url + 'index.php/student/fetchStudentByPid',
			type : 'POST',
			data : { 'pid' : pid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateStudentData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateStudentData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtStdid).val(elem.stdid);

			$(settings.txtClassName).val(elem.class_name);
			$(settings.txtClassName).data('claid', elem.claid);

			$(settings.txtSectionName).val(elem.section_name);
			$(settings.txtSectionName).data('secid', elem.secid);

			$(settings.txtName).val(elem.name);
			$(settings.txtName).data('rollno', elem.rollno);

			$(settings.txtFatherName).val(elem.fname);
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/fee/getMaxFeeRecId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtFeeRecId).val(data);
				$(settings.txtMaxFeeRecIdHidden).val(data);
				$(settings.txtFeeRecIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateEntry = function() {


		var errorFlag = false;
		var particulars = $(settings.particulars_dropdown).val();
		var amount = $(settings.txtCharges).val();

		// remove the error class first
		$(settings.particulars_dropdown).removeClass('inputerror');
		$(settings.txtCharges).removeClass('inputerror');

		if ( particulars === '' || particulars === null ) {
			$(settings.particulars_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( amount === '' || amount === null ) {
			$(settings.txtCharges).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(chid, amount, particulars, pid) {

		var row = "";
		row = 	"<tr> <td class='particulars'> "+ particulars +"</td> <td class='charges'> "+ amount +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-pid="+ pid +" data-chid="+ chid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo(settings.charges_table);
	}

	var getChargesDetail = function(chargeName) {

		var _pid = "";
		$.ajax({

			url : base_url + 'index.php/account/fetchAccountByName',
			type : 'POST',
			data : { 'name' : chargeName},
			dataType : 'JSON',
			async : false,
			success : function(data) {
				_pid = data.pid;
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});

		return _pid;
	}

	

	var getSaveObject = function() {

		var ledgers = [];
		var feerec = {};

		feerec.frid = $('#txtFeeRecIdHidden').val();
		feerec.rno = $('#txtvrno').val();
		feerec.stdid = $('#txtStdid').val();
		feerec.netamount = $('#txtNetAmount').val();
		feerec.date = $('#current_date').val();
		feerec.discount = $('#txtDiscount').val();
		feerec.fine = $('#txtOther').val();
		feerec.pid = $('#accounts_dropdown').val();
		feerec.brid = $('.brid').val();
		feerec.arears = $('#txtArears').val();

		feerec.uid = $('.uid').val();


		$(charges_table).find('tbody tr').each(function( index, elem ) {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#accounts_dropdown').val();
			pledger.description = $(elem).find('td').find('.particulars').text();
			pledger.date = $('#current_date').val();
			pledger.debit = '';
			pledger.credit = $(elem).find('td').eq(1).text();
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
<<<<<<< HEAD

			pledger.chid =  $(elem).data('chid');

			pledger.chid = '';
			pledger.uid = $('.uid').val();

			pledger.pid_key = $(settings.accounts_dropdown).val();
=======
			pledger.chid =  $(elem).data('chid');
			pledger.chid = '';
			pledger.uid = $('.uid').val();
			pledger.pid_key = $('#accounts_dropdown').val();
>>>>>>> 6b40ffc15cc7b94db85a65628c5d7dcccb9f6ef1
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $(elem).find('td').last().find('a').first().data('pid');
			pledger.description = $(elem).find('td').find('.particulars').text();
			pledger.date = $('#current_date').val();
			pledger.debit = $(elem).find('td').eq(1).text();
			pledger.credit = '';
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
			pledger.chid = $(elem).find('td').last().find('a').first().data('chid');
			pledger.pid_key = $('#accounts_dropdown').val();
			pledger.brid = $('.brid').val();
			pledger.uid = $('.uid').val();

			ledgers.push(pledger);
		});

		var discount = getNumVal($(settings.txtDiscount));
		if (parseFloat(discount) != 0) {

			var pledger = {};
			pledger.pledid = '';

			pledger.pid = $(settings.accounts_dropdown).val();
			pledger.description = '';
			pledger.date = $(settings.current_date).val();
			pledger.debit = discount;
			pledger.credit = ''
			pledger.dcno = $(settings.txtFeeRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = getChargesDetail('discount');
			pledger.description = '';
			pledger.date = $(settings.current_date).val();
			pledger.debit = '';
			pledger.credit = discount;

			pledger.pid = $('#feereceiveid').val();
			pledger.description = '';
			pledger.date = $('#current_date').val();
			pledger.credit = discount;
			pledger.debit = ''
<<<<<<< HEAD

			pledger.dcno = $(settings.txtFeeRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
=======
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
>>>>>>> 6b40ffc15cc7b94db85a65628c5d7dcccb9f6ef1
			pledger.etype = 'frv';
			pledger.chid = '';

			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		}

		var other = getNumVal($(settings.txtOther));
		if (parseFloat(other) != 0) {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $(settings.accounts_dropdown).val();
			pledger.description = '';
			pledger.date = $(settings.current_date).val();
			pledger.debit = other;
			pledger.credit = ''
			pledger.dcno = $(settings.txtFeeRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = getChargesDetail('fines');

			pledger.pid_key = $('#discountid').val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#discountid').val();
			pledger.description = '';
			pledger.date = $('#current_date').val();
			pledger.credit = '';
			pledger.debit = discount;
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $('#accounts_dropdown').val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		}

		var other = getNumVal($(settings.txtOther));
		if (parseFloat(other) != 0) {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#feereceiveid').val();
			pledger.description = '';
			pledger.date = $('#current_date').val();
			pledger.debit = other;
			pledger.credit = ''
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $('#accounts_dropdown').val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#latefeefine').val();

			pledger.description = '';
			pledger.date = $('#current_date').val();
			pledger.debit = '';
			pledger.credit = other;
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $('#accounts_dropdown').val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		}



		var arears = getNumVal($('#txtArears'));
		if (parseFloat(arears) != 0) {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#feereceiveid').val();
			pledger.description = '';
			pledger.date = $('#current_date').val();
			pledger.debit = arears;
			pledger.credit = ''
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $('#accounts_dropdown').val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#accounts_dropdown').val();
			pledger.description = '';
			pledger.date = $('#current_date').val();
			pledger.debit = '';
			pledger.credit = arears;
			pledger.dcno = $('#txtFeeRecIdHidden').val();
			pledger.invoice = $('#txtvrno').val();
			pledger.etype = 'frv';
			pledger.chid = '';
			pledger.pid_key = $('#accounts_dropdown').val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		}



		var data = {};
		data.feedata = feerec;
		data.ledger = ledgers;
		data.dcno = $('#txtFeeRecIdHidden').val();

		return data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var accounts_dropdown = $('#accounts_dropdown').val();

		// remove the error class first
		$('#accounts_dropdown').removeClass('inputerror');

		if ( accounts_dropdown === '' || accounts_dropdown === null ) {
			$('#accounts_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var getAssignFeeDetailObj = function() {

		var chargesDetail = [];

		$(settings.charges_table).find('tbody tr').each(function() {

			var _amount = $.trim($(this).closest('tr').find('td').eq(1).text());
			var _chid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('chid'));

			var charge = {

				chid : _chid,
				amount : _amount
			};
			chargesDetail.push(charge);
		});

		return chargesDetail;
	}

	var deleteVoucher = function(frid) {

		$.ajax({
			url : base_url + 'index.php/fee/deleteFeeRecieveVoucher',
			type : 'POST',
			data : { 'frid' : frid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}


	var showNetAmount = function() {

		var _total = 0;
		$('#charges_table').find('tbody tr').each(function( index, elem ) {
			var amnt = $(elem).find('td').eq(1).text();
			_total = parseFloat(_total) + parseFloat(amnt);
		});

		var discount = ($('#txtDiscount').val() == '' ) ?  0 : $('#txtDiscount').val();
		var other = ($('#txtOther').val() == '' ) ?  0 : $('#txtOther').val();
		var total = (parseFloat(_total) + parseFloat(other)) - parseFloat(discount);
		$('#txtNetAmount').val(total);
	}

	//  checks if the name is already used?
	var isFieldValid = function() {

		var errorFlag = false;
		var name = settings.txtvrno;		// get the current vrnoentered by the user
		var lidHidden = $('#txtMaxFeeRecIdHidden').val();		// hidden id
		var maxId = settings.txtMaxFeeRecIdHidden;		// hidden max id
		var txtnameHidden = settings.txtvrnoHidden;		// hidden vrno name

		// remove the previous classes
		name.removeClass('inputerror');

		// if both values are not equal then we are in update mode
		if (lidHidden.val() !== maxId.val()) {

			$.ajax({
				url : base_url + 'index.php/fee/updateVrnoaCheck',
				type : 'POST',
				data : { 'txtnameHidden' : txtnameHidden.val(), 'name': name.val() },
				dataType : 'JSON',
				async : false,
				success : function(data) {

					if (data.error === 'true') {
						name.addClass('inputerror');
						errorFlag = true;
					}

				}, error : function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});

		} else {	// if both are equal then we are in save mode

			$.ajax({
				url : base_url + 'index.php/fee/simpleVrnoCheck',
				type : 'POST',
				data : { 'name': name.val().toLowerCase() },
				dataType : 'JSON',
				async : false,
				success : function(data) {

					if (data.error === 'true') {
						name.addClass('inputerror');
						errorFlag = true;
					}
				}, error : function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}

		return errorFlag;
	}

	var resetFields = function() {

		$(settings.txtFeeRecId).val('');
		$(settings.txtMaxFeeRecIdHidden).val('');
		$(settings.txtFeeRecIdHidden).val('');

		$(settings.txtvrno).val('');

		$(settings.txtStdid).val('');
		$(settings.txtClassName).val('');
		$(settings.txtSectionName).val('');
		$(settings.txtName).val('');
		$(settings.txtFatherName).val('');
		$('#txtName').val('');

		$(settings.txtNetAmount).val('');
		$(settings.txtDiscount).val('');
		$(settings.txtOther).val('');

		$('#txtArears').val('');


		$(settings.accounts_dropdown).val('');
		$(settings.particulars_dropdown).val('');

		$(settings.charges_table).find('tbody tr').remove();
		$(settings.txtNetAmount).val('0');

	}



	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {
			var self = this;

			$(".btnPrint").on('click', function (e) {
				// alert('sd');
				e.preventDefault();
				Print_FeeRecevie();

			});
			shortcut.add("F5", function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			shortcut.add("F12", function(e) {
				e.preventDefault();
				
				self.InitDelete();
				
			});

			shortcut.add("ctrl+s", function(e) {
				e.preventDefault();
				self.initSave();
			});

			shortcut.add("ctrl+d", function(e) {
				e.preventDefault();
				
				self.InitDelete();
				

			});

			$('#txtFeeRecId').on('change', function() {
				var dcno = $(this).val();
				fetchRecords(dcno);
			});

			$('.btnSave').on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$(settings.particulars_dropdown).on('change', function() {

				var amount = $(this).find('option:selected').data('amount');
				$(settings.txtCharges).val(amount);
			});

			$(settings.btnAddCharges).on('click', function(e) {
				e.preventDefault();

				var chid = $(settings.particulars_dropdown).val();
				var amount = $(settings.txtCharges).val();
				var particulars = $(settings.particulars_dropdown).find('option:selected').text();
				var pid = $(settings.particulars_dropdown).find('option:selected').data('pid');

				var error = validateEntry();
				if (!error) {

					appendToTable(chid, amount, particulars, pid);
					showNetAmount();

					$(settings.particulars_dropdown).val('');
					$(settings.txtCharges).val('');

				} else {
					alert('Correct the errors...');
				}
			});

			// when btnRowRemove is clicked
			$(settings.charges_table).on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var amount = $.trim($(this).closest('tr').find('td').eq(1).text());
				$(this).closest('tr').remove();
				showNetAmount();
			});

			$(settings.charges_table).on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var amount = $.trim($(this).closest('tr').find('td').eq(1).text());
				var chid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('chid'));

				$(settings.particulars_dropdown).val(chid);
				$(settings.txtCharges).val(amount);

				$(this).closest('tr').remove();
				showNetAmount();
			});

			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();
				self.InitDelete();
				
			});

			$(settings.accounts_dropdown).on('change', function() {

				var pid = $(this).val();
				if (pid !== '') {
					fetch(pid);
				}
			});

			$(settings.txtDiscount).on('keyup', function(e) {
				showNetAmount();
			});

			$(settings.txtOther).on('keyup', function(e) {
				showNetAmount();
			});

			$(settings.txtFeeRecId).on('keypress', function(e) {

				if (e.keyCode === 13) {

					var dcno = $(settings.txtFeeRecId).val();
					if (dcno !== '') {
						fetchRecords(dcno);
					}
				}
			});

			$(settings.txtvrno).on('keypress', function(e) {

				if (e.keyCode === 13) {

					var dcno_text = $('#txtvrno').val();
					if (dcno_text !== '' && parseInt(dcno_text.length) >4 ) {
						var dcno=0;
						var stdid=0;

						dcno = parseInt(dcno_text.substring(0, 4));
						stdid = parseInt(dcno_text.substring(4, dcno_text.length));
						fetchFeeIssueByChalan(dcno,stdid);
					}else{
						alert('Please select valid ')
					}
				}
			});

			getMaxId();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var feeRecObj = getSaveObject();
			var error = validateSave();

			if (!error) {

				var rowsCount = $(settings.charges_table).find('tbody tr').length;

				if (rowsCount > 0 ) {

					// var vrno = $('#txtvrno').val();
					// if (vrno == '') {


						save(feeRecObj);
					// } else {

					// 	var error = isFieldValid();
					// 	if ( !error ) {

					// 		save(feeRecObj);
					// 	} else {

					// 		alert("Voucher# already used.");
					// 	}
					// }
				} else {

					alert('No record found to save.');
				}
			} else {
				alert('Correct the errors...');
			}
		},

		InitDelete : function(){
			if  ($('.btnDelete').data('deletetbtn')==0 ){
				alert('Sorry! you have not delete rights..........');
			}else{
				var frid = $(settings.txtFeeRecId).val();
				if (frid !== '') {
					
					if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(frid);
				}
			}
		},


		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			settings.netAmount = 0;

			$(settings.txtFeeRecId).val('');
			$(settings.txtMaxFeeRecIdHidden).val('');
			$(settings.txtFeeRecIdHidden).val('');

			$(settings.txtvrno).val('');

			$(settings.txtStdid).val('');
			$(settings.txtClassName).val('');
			$(settings.txtSectionName).val('');
			$(settings.txtName).val('');
			$(settings.txtFatherName).val('');

			$(settings.current_date).datepicker('update', new Date);
			$(settings.txtNetAmount).val('');
			$(settings.txtDiscount).val('');
			$(settings.txtOther).val('');

			$(settings.accounts_dropdown).val('');
			$(settings.particulars_dropdown).val('');

			$(settings.charges_table).find('tbody tr').remove();
			$(settings.txtNetAmount).val('0');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var feeRecieve = new FeeRecieve();
feeRecieve.init();