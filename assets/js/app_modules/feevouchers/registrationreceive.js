var RegistrationReceive = function() {

	var settings = {

		netAmount : 0,

		txtRegRecId : $('#txtRegRecId'),
		txtMaxRegRecIdHidden : $('#txtMaxRegRecIdHidden'),
		txtRegRecIdHidden : $('#txtRegRecIdHidden'),

		txtvrno : $('#txtvrno'),

		accounts_dropdown : $('#accounts_dropdown'),

		current_date : $('#current_date'),


		txtStdid : $('#txtStdid'),
		txtName : $('#txtName'),
		txtFatherName : $('#txtFatherName'),
		txtClassName : $('#txtClassName'),

		particulars_dropdown : $('#particulars_dropdown'),
		txtCharges : $('#txtCharges'),
		btnAddCharges : $('#btnAddCharges'),
		charges_table : $('#charges_table'),

		txtNetAmount : $('#txtNetAmount'),
		txtDiscount : $('#txtDiscount'),
		txtOther : $('#txtOther'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnDelete : $('.btnDelete')
	};

	var fetchRecords = function(dcno) {

		$.ajax({

			url : base_url + 'index.php/registration/fetchRegRecieve',
			type : 'POST',
			data : { 'dcno' : dcno },
			dataType : 'JSON',
			success : function(data) {

				settings.netAmount = 0;
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateConfigurationData(data.regrec);
					populateTableData(data.charges);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateTableData = function(data) {

		$(settings.charges_table).find('tbody tr').remove();
		$.each(data, function(index, elem) {
			appendToTable(elem.chid, elem.charge_amount, elem.charge_name, elem.pid);
			settings.netAmount = parseFloat(settings.netAmount) + parseFloat(elem.charge_amount);
		});
	}

	var populateConfigurationData = function(data) {

		$(settings.txtRegRecId).val(data.frid);
		$(settings.txtRegRecIdHidden).val(data.frid);

		$(settings.txtDiscount).val(data.discount);
		$(settings.txtOther).val(data.fine);

		$(settings.txtvrno).val(data.rno);
		$(settings.txtvrnoHidden).val(data.rno);

		$(settings.txtNetAmount).val(data.amount);
		$(settings.accounts_dropdown).val(data.pid);
		fetchThorughParty(data.pid);
		$(settings.current_date).datepicker('update', data.date.substring(0, 10));
	}

	var save = function(regObj) {

		$.ajax({
			url : base_url + 'index.php/registration/saveRegRecieve',
			type : 'POST',
			data : { 'regrec' : regObj.feedata, 'ledgers' : regObj.ledger, 'dcno' : regObj.dcno, 'etype' : 'frvproc' },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Voucher saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/registration/getMaxRegReceiveId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtRegRecId).val(data);
				$(settings.txtMaxRegRecIdHidden).val(data);
				$(settings.txtRegRecIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getChargesDetail = function(chargeName) {

		var _pid = "";
		$.ajax({

			url : base_url + 'index.php/account/fetchAccountByName',
			type : 'POST',
			data : { 'name' : chargeName},
			dataType : 'JSON',
			async : false,
			success : function(data) {
				_pid = data.pid;
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});

		return _pid;
	}

	var validateEntry = function() {


		var errorFlag = false;
		var particulars = $(settings.particulars_dropdown).val();
		var amount = $(settings.txtCharges).val();

		// remove the error class first
		$(settings.particulars_dropdown).removeClass('inputerror');
		$(settings.txtCharges).removeClass('inputerror');

		if ( particulars === '' || particulars === null ) {
			$(settings.particulars_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( amount === '' || amount === null ) {
			$(settings.txtCharges).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var getSaveObject = function() {

		var feerecproc = {};
		var ledgers = [];

		feerecproc.frid = $(settings.txtRegRecIdHidden).val();
		feerecproc.rno = $(settings.txtvrno).val();
		feerecproc.stdid = $(settings.txtStdid).val();
		feerecproc.amount = $(settings.txtNetAmount).val();
		feerecproc.date = $(settings.current_date).val();
		feerecproc.discount = $(settings.txtDiscount).val();
		feerecproc.fine = $(settings.txtOther).val();
		feerecproc.pid = $(settings.accounts_dropdown).val();
		feerecproc.brid = $('.brid').val();

		$(charges_table).find('tbody tr').each(function( index, elem ) {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $(settings.accounts_dropdown).val();
			pledger.description = '';
			pledger.date = $(settings.current_date).val();
			pledger.debit = $(elem).find('td').eq(1).text();
			pledger.credit = '';
			pledger.dcno = $(settings.txtRegRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frvproc';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $(elem).find('td').last().find('a').first().data('pid');
			pledger.description = $.trim($(elem).find('td').eq(0).text());
			pledger.date = $(settings.current_date).val();
			pledger.debit = '';
			pledger.credit = $(elem).find('td').eq(1).text();
			pledger.dcno = $(settings.txtRegRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frvproc';
			pledger.chid = $(elem).find('td').last().find('a').first().data('chid');
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		});

		var discount = $(settings.txtDiscount).val();
		if (discount != '' && discount != '0.0000') {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $(settings.accounts_dropdown).val();
			pledger.description = '';
			pledger.date = $(settings.current_date).val();
			pledger.debit = '';
			pledger.credit = discount;
			pledger.dcno = $(settings.txtRegRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frvproc';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = getChargesDetail('discount');
			pledger.description = 'discount';
			pledger.date = $(settings.current_date).val();
			pledger.debit = discount;
			pledger.credit = '';
			pledger.dcno = $(settings.txtRegRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frvproc';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		}

		var other = $(settings.txtOther).val();
		if (other != '' && other != '0.0000') {

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $(settings.accounts_dropdown).val();
			pledger.description = '';
			pledger.date = $(settings.current_date).val();
			pledger.debit = other;
			pledger.credit = '';
			pledger.dcno = $(settings.txtRegRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frvproc';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = getChargesDetail('fines');
			pledger.description = 'fines';
			pledger.date = $(settings.current_date).val();
			pledger.debit = '';
			pledger.credit = other;
			pledger.dcno = $(settings.txtRegRecIdHidden).val();
			pledger.invoice = $(settings.txtvrno).val();
			pledger.etype = 'frvproc';
			pledger.chid = '';
			pledger.pid_key = $(settings.accounts_dropdown).val();
			pledger.brid = $('.brid').val();
			ledgers.push(pledger);
		}

		var data = {};
		data.feedata = feerecproc;
		data.ledger = ledgers;
		data.dcno = $(settings.txtRegRecIdHidden).val();

		return data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var accounts_dropdown = $(settings.accounts_dropdown).val();

		// remove the error class first
		$(settings.accounts_dropdown).removeClass('inputerror');

		if ( accounts_dropdown === '' || accounts_dropdown === null ) {
			$(settings.accounts_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchRegDetail = function(rid) {

		$.ajax({
			url : base_url + 'index.php/registration/fetchStudentCharges',
			type : 'POST',
			data : { 'rid' : rid },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					$(settings.charges_table).find('tbody tr').remove();
					populateRegData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateRegData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtName).val(elem.name);
			$(settings.txtFatherName).val(elem.fname);
			$(settings.txtClassName).val(elem.class_name);

			settings.netAmount = parseFloat(elem.fee);
			showNetAmount();

			appendToTable(elem.chid, elem.fee, elem.description, elem.pid);
		});
	}

	var showNetAmount = function() {

		var discount = ($(settings.txtDiscount).val() == '' ) ?  0 : $(settings.txtDiscount).val();
		var other = ($(settings.txtOther).val() == '' ) ?  0 : $(settings.txtOther).val();
		var net = (parseFloat(settings.netAmount) + parseFloat(other)) - parseFloat(discount);

		$('#txtNetAmount').val(net);
	}

	var appendToTable = function(chid, amount, particulars, pid) {
		var row = "";
		row = 	"<tr> <td> "+ particulars +"</td> <td> "+ amount +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-pid="+ pid +" data-chid="+ chid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo(settings.charges_table);
	}

	var fetchThorughParty = function(pid) {

		$.ajax({
			url : base_url + 'index.php/student/fetchStudentByPid',
			type : 'POST',
			data : { 'pid' : pid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateStudentData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var clearStudentData = function() {

		$(settings.accounts_dropdown).val('');
		$(settings.txtStdid).val('');
		$(settings.txtClassName).val('');
		$(settings.txtSectionName).val('');
		$(settings.txtName).val('');
		$(settings.txtFatherName).val('');
	}

	var populateStudentData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtStdid).val(elem.stdid);

			$(settings.txtClassName).val(elem.class_name);
			$(settings.txtClassName).data('claid', elem.claid);

			$(settings.txtSectionName).val(elem.section_name);
			$(settings.txtSectionName).data('secid', elem.secid);

			$(settings.txtName).val(elem.name);
			$(settings.txtName).data('rollno', elem.rollno);

			$(settings.txtFatherName).val(elem.fname);
		});
	}

	var deleteVoucher = function(rid) {

		$.ajax({
			url : base_url + 'index.php/registration/deleteStudentChargeVoucher',
			type : 'POST',
			data : { 'rid' : rid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var deleteVoucher = function(frid) {

		$.ajax({
			url : base_url + 'index.php/registration/deleteRegRecieveVoucher',
			type : 'POST',
			data : { 'frid' : frid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			// when text is changed in txtStudentId
			$(settings.txtRegRecId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtRegRecId).val().trim() !== "" ) {

						var rid = $.trim($(settings.txtRegRecId).val());
						fetchRecords(rid);
					}
				}
			});

			$(settings.accounts_dropdown).on('change', function() {

				var pid = $(this).val();
				if (pid !== '') {
					$(settings.txtvrno).val('');
					fetchThorughParty(pid);
				}
			});

			// when text is changed in txtStudentId
			$(settings.txtvrno).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtvrno).val().trim() !== "" ) {

						var rid = $.trim($(settings.txtvrno).val());
						fetchRegDetail(rid);
					}
				}
			});
			$(settings.particulars_dropdown).on('change', function() {			

				var amount = $(this).find('option:selected').data('amount');
				$(settings.txtCharges).val(amount);
			});
			$(settings.btnAddCharges).on('click', function(e) {
				e.preventDefault();

				var chid = $(settings.particulars_dropdown).val();
				var particulars = $(settings.particulars_dropdown).find('option:selected').text();
				var pid = $(settings.particulars_dropdown).find('option:selected').data('pid');
				var amount = $.trim($(txtCharges).val());

				var error = validateEntry();
				if (!error) {

					// calculate the sum and show it
					settings.netAmount = parseFloat(settings.netAmount) + parseFloat(amount);
					showNetAmount();

					appendToTable(chid, amount, particulars, pid);


					$(settings.particulars_dropdown).val('');
					$(settings.txtCharges).val('');
				} else {
					alert('Correct the errors...');
				}
			});
			// when btnRowRemove is clicked
			$(settings.charges_table).on('click', '.btnRowRemove', function(e) {
				e.preventDefault();

				var amount = $.trim($(this).closest('tr').find('td').eq(1).text());

				settings.netAmount = parseFloat(settings.netAmount) - parseFloat(amount);
				showNetAmount();

				$(this).closest('tr').remove();
			});
			$(settings.charges_table).on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var chid = $.trim($(this).closest('tr').find('td a.btnRowEdit').data('chid'));
				var amount = $.trim($(this).closest('tr').find('td').eq(1).text());

				settings.netAmount = parseFloat(settings.netAmount) - parseFloat(amount);
				showNetAmount();

				$(settings.particulars_dropdown).val(chid);
				$(settings.txtCharges).val(amount);

				$(this).closest('tr').remove();
			});
			$(settings.txtDiscount).on('keyup', function(e) {
				showNetAmount();
			});
			$(settings.txtOther).on('keyup', function(e) {
				showNetAmount();
			});

			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();

				var frid = $(settings.txtRegRecId).val();
				if (frid !== '') {
					deleteVoucher(frid);
				}
			});

			getMaxId();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var regObj = getSaveObject();

			var error = validateSave();

			if (!error) {

				var rows = $(settings.charges_table).find('tbody tr').length;
				if (rows > 0) {
					save( regObj );
				} else {
					alert('No record found to save.');
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');

			$(settings.txtRegRecId).val('');
			$(settings.txtMaxRegRecIdHidden).val('');
			$(settings.txtRegRecIdHidden).val('');

			$(settings.txtNetAmount).val('');
			$(settings.txtDiscount).val('');
			$(settings.txtOther).val('');

			$(settings.current_date).datepicker("update", new Date);
			clearStudentData();
			$(settings.charges_table).find('tbody tr').remove();

			settings.netAmount = 0;
			$(settings.txtvrno).val('');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var registrationReceive = new RegistrationReceive();
registrationReceive.init();