var Registration = function() {

	var settings = {

		txtRegtId : $('#txtRegtId'),
		txtMaxRegIdHidden : $('#txtMaxRegIdHidden'),
		txtRegIdHidden : $('#txtRegIdHidden'),

		txtRegDate : $('#txtRegDate'),
		txtName : $('#txtName'),
		gender_dropdown : $('#gender_dropdown'),
		txtFatherName : $('#txtFatherName'),
		txtAddress : $('#txtAddress'),
		txtMobile : $('#txtMobile'),

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),

		dob_date : $('#dob_date'),

		txtFee : $('#txtFee'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnDelete : $('.btnDelete')
	};

	var save = function( regObj ) {

		$.ajax({
			url : base_url + 'index.php/charge/saveStudentChargesVoucher',
			type : 'POST',
			data : { 'regObj' : regObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Changes saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/charge/getMaxStudentChargesId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtRegtId).val(data);
				$(settings.txtMaxRegIdHidden).val(data);
				$(settings.txtRegIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(class_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});
		$(classNamesOptions).appendTo(class_dropdown);
	}

	var validateEntry = function() {


		var errorFlag = false;
		var name = $(settings.subjects_dropdown).val();

		// remove the error class first
		$(settings.subjects_dropdown).removeClass('inputerror');

		if ( name === '' || name === null ) {
			$(settings.subjects_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(sbid, name) {

		var row = "";
		row = 	"<tr> <td> "+ name +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-sbid="+ sbid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo(settings.subjects_table);
	}

	var getSaveObject = function() {

		var obj = {

			rid : $.trim($(settings.txtRegIdHidden).val()),
			name : $.trim($(settings.txtName).val()),
			fname : $.trim($(settings.txtFatherName).val()),
			mobile : $.trim($(settings.txtMobile).val()),
			address : $.trim($(settings.txtAddress).val()),
			claid : $.trim($(settings.class_dropdown).val()),
			brid : $.trim($(settings.branch_dropdown).val()),
			chid : $.trim($(settings.txtFee).data('chid')),
			gender : $.trim($(settings.gender_dropdown).val()),
			date : $.trim($(settings.txtRegDate).val()),
			dob : $.trim($(settings.dob_date).val()),
			fee : $.trim($(settings.txtFee).val())
		}

		return obj;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var txtName = $(settings.txtName).val();
		var txtFatherName = $.trim($(settings.txtFatherName).val());
		var class_dropdown = $.trim($(settings.class_dropdown).val());
		var branch_dropdown = $.trim($(settings.branch_dropdown).val());
		var txtFee = $.trim($(settings.txtFee).val());

		// remove the error class first
		$(settings.txtName).removeClass('inputerror');
		$(settings.txtFatherName).removeClass('inputerror');
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.txtFee).removeClass('inputerror');

		if ( txtName === '' || txtName === null ) {
			$(settings.txtName).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtFatherName === '' || txtFatherName === null ) {
			$(settings.txtFatherName).addClass('inputerror');
			errorFlag = true;
		}

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtFee === '' || txtFee === null ) {
			$(settings.txtFee).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetch = function(rid) {

		$.ajax({
			url : base_url + 'index.php/charge/fetchStudentCharges',
			type : 'POST',
			data : { 'rid' : rid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$.each(data, function(index, elem) {

			$(settings.txtRegtId).val(elem.rid);
			$(settings.txtRegIdHidden).val(elem.rid);

			$(settings.branch_dropdown).val(elem.brid);
			fetchClassNames(elem.brid);
			$(settings.class_dropdown).val(elem.claid);

			$(settings.txtName).val(elem.name);
			$(settings.txtFatherName).val(elem.fname);			
			$(settings.gender_dropdown).val(elem.gender);
			$(settings.txtAddress).val(elem.address);
			$(settings.txtMobile).val(elem.mobile);
			$(settings.txtRegDate).datepicker("update", elem.date.substring(0, 10));
			$(settings.dob_date).datepicker("update", elem.dob.substring(0, 10));
			$(settings.txtFee).val(elem.fee);
		});
	}

	var deleteVoucher = function(rid) {

		$.ajax({
			url : base_url + 'index.php/charge/deleteStudentChargeVoucher',
			type : 'POST',
			data : { 'rid' : rid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.btnSave).on('click',  function(e) {
				e.preventDefault();

				self.initSave();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			// when text is changed in txtStudentId
			$(settings.txtRegtId).on('keypress', function(e) {
				
				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $(settings.txtRegtId).val().trim() !== "" ) {						
						
						var rid = $.trim($(settings.txtRegtId).val());
						fetch(rid);
					}
				}
			});

			$(settings.btnDelete).on('click', function(e){
				e.preventDefault();

				var rid = $(settings.txtRegtId).val();				
				deleteVoucher(rid);
			});

			getMaxId();
			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		// prepares the data to save it into the database
		initSave : function() {

			var regObj = getSaveObject();

			var error = validateSave();

			if (!error) {

				save( regObj );
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.txtRegtId).val('');
			$(settings.txtMaxRegIdHidden).val('');
			$(settings.txtRegIdHidden).val('');

			$(txtRegDate).datepicker("update", new Date);
			$(settings.txtName).val('');
			$(settings.gender_dropdown).val('male');
			$(settings.txtFatherName).val('');
			$(settings.txtAddress).val('');
			$(settings.txtMobile).val('');
			$(settings.class_dropdown).val('');
			$(settings.branch_dropdown).val('');
			$(settings.txtFee).val('');
			removeOptions();

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var registration = new Registration();
registration.init();