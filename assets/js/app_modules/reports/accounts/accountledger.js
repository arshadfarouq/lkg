var AccountLedger = function() {

	var fetchOpeningBalance = function(_to, _pid) {
		$.ajax({
			url : base_url + 'index.php/account/fetchPartyOpeningBalance',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				var opbal=data[0]['OPENING_BALANCE'];

				$('.opening-bal').html(parseFloat(opbal).toFixed(0));
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var showRunningTotal = function(_to, _pid) {
		$.ajax({
			url : base_url + 'index.php/account/fetchRunningTotal',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				
				$(data).each(function(index,elem){

					$('#datatable_example').dataTable().fnAddData( [
						"<span></span>",
						"<span></span>",
						"<span></span>",
						"<span>Total Balance</span>",
						"<span></span>",
						"<span></span>",
						"<span>"+ ((elem.RTotal == null) ? 0 : elem.RTotal) +"</span>" ]
					);
				});

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});	
	}

	var search = function(_from, _to, _pid,_brid) {

		$.ajax({
			url : base_url + 'index.php/account/getAccLedgerReport',
			type : 'POST',
			data : {'from': _from, 'to' : _to, 'pid' : _pid, 'brid' : _brid},
			dataType : 'JSON',
			success : function(data) {

				// removes all rows
				$('#datatable_example').find('tbody tr :not(.dataTables_empty)').remove();

				if (data == 'false') {
					showRunningTotal(_to, _pid);
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$('.running-total').html('0');
	    $('.net-debit').html('0');
	    $('.net-credit').html('0');
	    $('#datatable_example tbody tr').remove();
		// "<span>"+ (index+1) +"</span>",
		// "<span>"+ elem.PARTY_ID +"</span>",
		
		var netDebit = 0;
		var netCredit = 0;
		var netRTotal = 0;

		$.each(data, function(index, elem) {

			netDebit += parseFloat(elem.DEBIT);
			netCredit += parseFloat(elem.CREDIT);
			if (index == (data.length - 1)) {
				netRTotal = elem.RTotal;
			};
			var drcr='';
			if (parseFloat(elem.RTotal) > 0) {
				drcr = "Dr";
			} else {
				drcr = "Cr";
			}
			var voucher_type = '';
    		
            if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                voucher_type = base_url +'index.php/saleorder/Sale_Invoice?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                voucher_type = base_url +'index.php/jv?vrnoa=' + elem.VRNOA;
            } else if ( ( elem.ETYPE.toLowerCase() == 'cpv' ) || ( elem.ETYPE.toLowerCase() == 'crv' ) ) {
                voucher_type = base_url +'index.php/payment?vrnoa=' + elem.VRNOA + '&etype=' + elem.ETYPE.toLowerCase();
            } else if ( ( elem.ETYPE.toLowerCase() == 'pd_issue' ) ) {
                voucher_type = base_url +'index.php/payment/chequeIssue?vrnoa=' + elem.VRNOA;
            } else if ( ( elem.ETYPE.toLowerCase() == 'pd_receive' ) ) {
                voucher_type = base_url +'index.php/payment/chequeReceive?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'purchase' ) {
                voucher_type = base_url +'index.php/purchase?vrnoa=' + elem.VRNOA ;
            } else if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                voucher_type = base_url +'index.php/saleorder/Sale_Invoice?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'salereturn' ) {
                voucher_type = base_url +'index.php/salereturn?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'purchasereturn' ) {
                voucher_type = base_url +'index.php/purchasereturn?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'pur_import' ) {
                voucher_type = base_url +'index.php/purchase/import?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'assembling' ) {
                voucher_type = base_url +'index.php/item/assdeass?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'navigation' ) {
                voucher_type = base_url +'index.php/stocknavigation?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'production' ) {
                voucher_type = base_url +'index.php/production?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'consumption' ) {
                voucher_type = base_url +'index.php/consumption?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'materialreturn' ) {
                voucher_type = base_url +'index.php/materialreturn?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'moulding' ) {
                voucher_type = base_url +'index.php/moulding?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'order_loading' ) {
                voucher_type = base_url +'index.php/saleorder/partsloading?vrnoa=' + elem.VRNOA;
            }
            else {
                voucher_type = elem.VRNOA + '-' + elem.ETYPE;
            }
            appendToTable(elem.VRDATE.substr(0, 10), elem.VRNOA + '-' + elem.ETYPE ,voucher_type, elem.DESCRIPTION, elem.DEBIT, elem.CREDIT, elem.RTotal, drcr)
			// $('#datatable_example').dataTable().fnAddData( [
			// 	"<span data-title='Amount'>"+ elem.VRDATE.substr(0, 10) +"</span>",
			// 	"<span>"+ voucher_type +"</span>",
			// 	"<span>"+ elem.DESCRIPTION +"</span>",
			// 	"<span>"+ elem.DEBIT +"</span>",
			// 	"<span>"+ elem.CREDIT +"</span>",
			// 	"<span style='text-align: right;'>"+ elem.RTotal +"</span>",
			// 	"<span>"+ drcr +"</span>" ]
			// );
		});
	
		$('.running-total').html(parseFloat(netRTotal).toFixed(0));
		$('.net-debit').html(parseFloat(netDebit).toFixed(0));
		$('.net-credit').html(parseFloat(netCredit).toFixed(0));
	}

	var appendToTable = function(vrdate, voucher_type,hreff, description, debit, credit, balance, drcr) {
		var srno = $('#datatable_example tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno numeric' data-title='Sr#'> "+ srno +"</td>" +
						"<td class='srno numeric' data-title='Date' > "+ vrdate +"</td>" +
						"<td class='voucher' data-title='Voucher'> <a href='"+ hreff + "'>" + voucher_type + "</a></td>" +
				 		// "<td><a href='"+ hreff +"' class='hreff' data-title='Voucher'><span class='fa fa-edit'></span></a> </td>" +
				 		"<td class='qty numeric' data-title='description'>  "+ description +"</td>" +
					 	"<td class='weight numeric' data-title='Debit' style='text-align: right;'> "+ debit +"</td>" +
					 	"<td class='rate numeric' data-title='Creidt' style='text-align: right;'> "+ credit +"</td>" +
					 	"<td class='amount numeric' data-title='Balance' style='text-align: right;' > "+ balance +"</td>" +
					 	"<td class='amount numeric' data-title='Dr/Cr' > "+ drcr +"</td>" +
				 	"</tr>";
		$(row).appendTo('#datatable_example');
	}


	var validateSearch = function() {

		var errorFlag = false;
		var from_date = $('#from_date').val();
		var to_date = $('#to_date').val();
		var pid = $('#hfPartyId').val();

		// remove the error class first
		$('#from_date').removeClass('inputerror');
		$('#to_date').removeClass('inputerror');
		$('#txtPartyId').removeClass('inputerror');

		if ( from_date === '' || from_date === null ) {
			$('#from_date').addClass('inputerror');
			errorFlag = true;
		}
		if ( to_date === '' || to_date === null ) {
			$('#to_date').addClass('inputerror');
			errorFlag = true;
		}
		if ( pid === '' || pid === null ) {
			$('#txtPartyId').addClass('inputerror');
			errorFlag = true;
		}
		if (from_date > to_date ){
            $('#from_date').addClass('inputerror');
            alert('Starting date must Be less than ending date.........')
            errorFlag = true;   
        }

		return errorFlag;
	}

	var printReport = function() {
		var error = validateSearch();
		if (!error) {
			var _from = $('#from_date').val();
			var _to = $('#to_date').val();
			var _pid = $('#hfPartyId').val();
			_from= _from.replace('/','-');
			_from= _from.replace('/','-');
			_to= _to.replace('/','-');
			_to= _to.replace('/','-');

			var company_id =$('#cid').val();
			// alert(company_id);

			var user = $('#uname').val();
			var hd = ($('#switchPrintHeader').bootstrapSwitch('state') === true) ? '1' : '0';
			
			var url = base_url + 'index.php/doc/pdf_ledger/' + _from + '/' + _to  + '/' + _pid + '/'+ company_id + '/'  + '-1' + '/' + user+ '/' + hd;
			window.open(url);

		} else {
				alert('Correct the errors...');
				}
		// window.open(base_url + 'application/views/reportprints/ledger.php', "Sale Report", "width=1000, height=842");
		// window.open(base_url + 'application/views/reportPrints/ledger.php', "Sale Report", "width=1000, height=842");
	}
	var sendMail = function() {

	    var _data = {};
	    $('#datatable_example').prop('border', '1');
	    _data.table = $('#datatable_example').prop('outerHTML');
	    $('#datatable_example').removeAttr('border');
	    
		var _email = $('#txtPartyEmail').val();
		var _name = $('#hfPartyName').val();
		var _address = $('#hfPartyAddress').val();
		var _mobile = $('#hfPartyMobile').val();
		var _accountid = $('#hfPartyId').val();


		var _pid = $('#hfPartyId').val();


	    _data.accTitle = _name;
	    _data.accCode = _accountid;
	    _data.contactNo =_mobile;
	    _data.address = _address;
	    
	    
	    _data.from = $('#from_date').val();
	    _data.to = $('#to_date').val();
	    _data.type = 'Account Ledger Report‏';
	    _data.email = $('#txtAddEmail').val();
	    // alert(_data);
	    console.log(_data);
	    $.ajax({
	        url : base_url + 'index.php/email',
	        type : 'POST',
	        dataType : 'JSON',
	        data : _data,
	        success: function(result) {
	            console.log(result);
	        }, error: function(error) {
	            alert(error +'call');
	            alert('Error '+ error);
	        }
	    });

	    // close the modal dialog
	    $('#btnSendEmail').siblings('button').trigger('click');
	}

		var Account_Flow = function() {
		var error = validateSearch();
		if (!error) {
			var _from = $('#from_date').val();
			var _to = $('#to_date').val();
			var _pid = $('#hfPartyId').val();
			_from= _from.replace('/','-');
			_from= _from.replace('/','-');
			_to= _to.replace('/','-');
			_to= _to.replace('/','-');

			var companyid =$('#cid').val();
			var user = $('#uname').val();
			
			var url = base_url + 'index.php/doc/Account_Flow/' + _from + '/' + _to  + '/' + _pid + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);

		} else {
				alert('Correct the errors...');
				}
	
	}
	var clearPartyData = function (){

		$("#hfPartyId").val("");
		$("#hfPartyBalance").val("");
		$("#hfPartyCity").val("");
		$("#hfPartyAddress").val("");
		$("#hfPartyCityArea").val("");
		$("#hfPartyMobile").val("");
		$("#hfPartyUname").val("");
		$("#hfPartyLimit").val("");
		$("#hfPartyName").val("");
		$("#txtPartyEmail").val("");

	}
	var fetchLookupAccounts = function () {
		$.ajax({
			url : base_url + 'index.php/account/searchAccountAll',
			type: 'POST',
			data: {'search': '', 'type':'suppliers'},
			dataType: 'JSON',
			success: function (data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataLoookupAccount(data);
				}
			}, error: function (xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataLoookupAccount = function (data) {

		if (typeof dTable != 'undefined') {
			dTable.fnDestroy();
			$('#tblAccounts > tbody tr').empty();
		}

		var html = "";
		$.each(data, function (index, elem) {

			html += "<tr>";
			html += "<td width='14%;'>"+ elem.pid +"<input type='hidden' name='hfModalPartyId' value='"+elem.pid+"' ></td>";
			html += "<td>"+ elem.name +"</td>";
			html += "<td>"+ elem.mobile +"</td>";
			html += "<td>"+ elem.address +"</td>";
			html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateAccount'><i class='fa fa-search'></i></a></td>";
			html += "</tr>";
		});

		$("#tblAccounts > tbody").html('');
		$("#tblAccounts > tbody").append(html);
		bindGridAccounts();
	}
	var bindGridAccounts = function() {

		$('.modal-lookup .populateAccount').on('click', function () {
			var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
			ShowAccountData(party_id); 				
		});

		var dontSort = [];
		$('#tblAccounts thead th').each(function () {
			if ($(this).hasClass('no_sort')) {
				dontSort.push({ "bSortable": false });
			} else {
				dontSort.push(null);
			}
		});
		dTable = $('#tblAccounts').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 10,
            "oTableTools": {
            	"sSwfPath": "js/copy_cvs_xls_pdf.swf",
            	"aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
		$.extend($.fn.dataTableExt.oStdClasses, {
			"s`": "dataTables_wrapper form-inline"
		});
	}

	var ShowAccountData = function(pid){

		$.ajax({
			type: "POST",
			url: base_url + 'index.php/account/getAccountinfobyid',
			data: {
				pid: pid
			}
		}).done(function (result) {
			console.log(result);
			$("#imgPartyLoader").hide();
			var party = result;
			console.log(party);
			if (party != false)
			{

				$('#txtPartyId').removeClass('inputerror');
				$("#imgPartyLoader").hide();
				$("#hfPartyId").val(party[0]['party_id']);
				$("#hfPartyBalance").val(party[0]['balance']);
				$("#hfPartyCity").val(party[0]['city']);
				$("#hfPartyAddress").val(party[0]['address']);
				$("#hfPartyCityArea").val(party[0]['cityarea']);
				$("#hfPartyMobile").val(party[0]['mobile']);
				$("#hfPartyUname").val(party[0]['uname']);
				$("#hfPartyLimit").val(party[0]['limit']);
				$("#hfPartyName").val(party[0]['name']);
				$("#txtPartyId").val(party[0]['name']);
				$("#txtPartyEmail").val(party[0]['email']);


				// $('#party_p').html(' Balance is ' + party[0]['balance'] +'  <br/>' + party[0]['city']  + '<br/>' + party[0]['address'] + ' ' + party[0]['cityarea'] + '<br/> ' + party[0]['mobile']  );

			}

		});
	} 

	return {

		init : function () {
			this.bindUI();
			// this.bindModalPartyGrid();
		},

		bindUI : function() {

			var self = this;

			$("#switchPrintHeader").bootstrapSwitch('offText', 'No');
			$("#switchPrintHeader").bootstrapSwitch('onText', 'Yes');


			$('#from_date').val('2014/01/01');

			$('.btnsearchparty').on('click',function(e){
				e.preventDefault();

				var length = $('#tblAccounts > tbody tr').length;
				
				if(length <= 1){
					fetchLookupAccounts();
				}
			});

			
			$('#btnSendEmail').on('click', function() {
			    sendMail();
			});

			$('.btnSearch').on('click', function(e){
				e.preventDefault();
				self.initSearch();
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnPrint').on('click', function(e) {
				e.preventDefault();
				printReport();
			});

			$('.btnPrint2').on('click', function(e) {
				e.preventDefault();
				Account_Flow();
			});
			$('.btnPrint3').on('click', function(e) {
				e.preventDefault();
				window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Purchase Report", 'width=1000, height=842');
			});
			$('.btnPrintUrdu').on('click', function(e) {
				e.preventDefault();
				window.open(base_url + 'application/views/reportprints/AccountLedgerUrdu.php', "Purchase Report", 'width=1000, height=842');
			});
			shortcut.add("F9", function(e) {
				e.preventDefault();
				printReport();
			});
			shortcut.add("F8", function(e) {
				e.preventDefault();
				Account_Flow();
			});
			shortcut.add("F6", function(e) {
				e.preventDefault();
    			self.initSearch();
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});
			$('#txtPartyId').on('input',function(){
				if($(this).val() == ''){
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
				}
			});

			$('#txtPartyId').on('focusout',function(){
				if($(this).val() != ''){
					var partyID = $('#hfPartyId').val();
					if(partyID == '' || partyID == null){
						$('#txtPartyId').addClass('inputerror');
						$('#txtPartyId').focus();
						$("#imgPartyLoader").show();
					}
				}
				else{
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
				}
			});

			

			// $('#txtQty').val(1);


			var countParty = 0;
			$('input[id="txtPartyId"]').autoComplete({
				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/account/searchAccount',
							type: 'POST',
							data: {
								search: search,
								type : 'sale order',
							},
							dataType: 'JSON',
							beforeSend: function (data) {
								$(".loader").hide();
								$("#imgPartyLoader").show();
								countParty = 0;
							},
							success: function (data) {
								if(data == ''){
									$('#txtPartyId').addClass('inputerror');
									clearPartyData();
								}
								else{
									$('#txtPartyId').removeClass('inputerror');
									response(data);
									$("#imgPartyLoader").hide();
								}
							}
						});
					}
					else
					{
						clearPartyData();
					}
				},
				renderItem: function (party, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (party.name).toLowerCase() && countParty == 0) || (search.toLowerCase() != (party.name).toLowerCase() && countParty == 0))
					{
						selected = "selected";
					}
					countParty++;
					clearPartyData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '"  data-email="' + party.email + '" data-party_id="' + party.pid + '" data-credit="' + party.balance + '" data-city="' + party.city +
					'" data-address="'+ party.address + '" data-cityarea="' + party.cityarea + '" data-mobile="' + party.mobile + '" data-uname="' + party.uname +
					'" data-limit="' + party.limit + '" data-name="' + party.name +
					'">' + party.name.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, party)
				{	
					$('#txtPartyId').removeClass('inputerror');
					$("#imgPartyLoader").hide();
					$("#hfPartyId").val(party.data('party_id'));
					$("#hfPartyBalance").val(party.data('credit'));
					$("#hfPartyCity").val(party.data('city'));
					$("#hfPartyAddress").val(party.data('address'));
					$("#hfPartyCityArea").val(party.data('cityarea'));
					$("#hfPartyMobile").val(party.data('mobile'));
					$("#hfPartyUname").val(party.data('uname'));
					$("#hfPartyLimit").val(party.data('limit'));
					$("#hfPartyName").val(party.data('name'));
					$("#txtPartyId").val(party.data('name'));
					$("#txtPartyEmail").val(party.data('email'));


					var partyId = party.data('party_id');
					var partyBalance = party.data('credit');
					var partyCity = party.data('city');
					var partyAddress = party.data('address');
					var partyCityarea = party.data('cityarea');
					var partyMobile = party.data('mobile');
					var partyUname = party.data('uname');
					var partyLimit = party.data('limit');
					var partyName = party.data('name');
					

					// $('#party_p').html(' Balance is ' + partyBalance +'  <br/>' + partyCity  + '<br/>' + partyAddress + ' ' + partyCityarea + '<br/> ' + partyMobile  );

				}
			});
		},

		initSearch : function() {
			var error = validateSearch();

			if (!error) {

				var _from = $('#from_date').val();
				var _to = $('#to_date').val();
				var _pid = $('#hfPartyId').val();
				var _brid = $('.brid').val();
				var _email = $('#txtPartyEmail').val();
				$('#txtAddEmail').val(_email);
				fetchOpeningBalance(_from, _pid);

				search(_from, _to, _pid,_brid);
			} else {
				alert('Correct the errors...');
			}
		},
		

		resetVoucher : function() {
			$('#txtPartyId').val('');
			clearPartyData();
			$('table tbody tr').remove();
			$('#txtPartyId').focus();

		}

	};
};


var accountLedger = new AccountLedger();
accountLedger.init();