Acct =function() {
    var getcrit = function (){

        var accid=$("#drpAccountID").select2("val");
        
        var userid=$('#drpuserId').select2("val");
       
        // Account
        var txtCity=$("#drpCity").select2("val");
        var txtCityArea=$('#drpCityArea').select2("val");

        

        
        var l1id=$('#drpl1Id').select2("val");
        var l2id=$('#drpl2Id').select2("val");
        var l3id=$('#drpl3Id').select2("val");
        // End Account
       
        var crit ='';
       
        
        if (accid!=''){
                crit +='AND party.pid in (' + accid +') ';
        }else{

            if (txtCity!=''){
                var qry = " ( ";

                $.each(txtCity,function(number){
                     qry +=  " party.city like '%" + txtCity[number] + "%' OR ";

                });
                qry = qry.slice(0,-3);

                crit +='AND '+ qry +' )';
            }
            if (txtCityArea!='') {
                var qry = "";
                $.each(txtCityArea,function(number){
                     qry +=  "'" + txtCityArea[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND party.cityarea in (' + qry +') '
            }
            if (l1id!='') {
                crit +='AND leveltbl1.l1 in (' + l1id +') ';
            }
            if (l2id!='') {
                crit +='AND leveltbl2.l2 in (' + l2id+ ') ';
            }
            if (l3id!='') {
                crit +='AND party.level3 in (' + l3id+ ') ';
            }
        }
        if (userid!=''){
                crit +='AND user.uid in (' + userid +') ';
        }

         
        
        return crit;

   }


      return { init : function () {

        acct.populateDate();
        acct.bindUI();

        $('.advanced-filter').hide();
        $("#cpv_datatable_example").hide();
        $(".printBtn").hide();

        $('.ReportViews').addClass('active');
        $('.AccountReports').addClass('active');
    },

    bindUI : function (){
        // $('#from').val('2014/01/01');
        
        $('.btnAdvaced').on('click', function(ev) {
            ev.preventDefault();
            
            $('.panel-group1').toggleClass("panelDisplay");
        });


        $(".reset-rept").on("click", function () {

            $('.grand-sum').html(0);
            $('.payments-sum').html(0);
            $('.opening-bal-block').hide();
            $('.receipts-sum').html(0);
            $('.closing-bal-block').hide();
            $('.pimports-sum').html(0);
            $('.closing-bal').html(0);
            $('.purchasereturns-sum').html(0);
            $('.opening-bal').html(0);
            $('.purchases-sum').html(0);
            $('.sales-sum').html(0);
            $('.cash-sales-sum').html(0);
            $('.discount-sales-sum').html(0);

            $('.grand-total').html(0);
            $('.grand-debit').html(0);
            $('.grand-credit').html(0);
            $('.grand-lcy').html(0);
            $('.grand-fcy').html(0);

            $('#CPVRows').empty();

            acct.resetReport();
        });

        $('#drpCompanyId').on('change', function (){

            $('.grand-sum').html(0);
            $('.closing-bal-block').hide();
            $('.receipts-sum').html(0);
            $('.closing-bal').html(0);
            $('.opening-bal').html(0);
            $('.grand-total').html(0);
            $('.payments-sum').html(0);
            $('.pimports-sum').html(0);
            $('.grand-debit').html(0);
            $('.purchases-sum').html(0);
            $('.sales-sum').html(0);
            $('.cash-sales-sum').html(0);
            $('.discount-sales-sum').html(0);

            $('.grand-credit').html(0);
            $('.grand-lcy').html(0);
            $('.grand-fcy').html(0);
            $('.purchasereturns-sum').html(0);

            $('#CPVRows').empty();
        });

        $('.printCpvCrvBtn').on('click', function(ev){

            acct.showAllRows();
            ev.preventDefault();

            window.open(base_url + 'application/views/reportprints/cpvcrv.php', "Daybook Report", "width=1000, height=842");
        });

        $('.printDayBook').on('click', function ( ev ) {

            acct.showAllRows();
            ev.preventDefault();
            window.open(base_url + 'application/views/reportprints/dayBook.php', "Daybook Report", "width=1000, height=842");
        });

        $('.printPayRcvBtn').on('click', function( ev ){

            acct.showAllRows();

            ev.preventDefault();
            window.open(base_url + 'application/views/reportprints/payableReceivable.php', "Payable/Receivable Report", "width=1000, height=842");
        });
        $('.printPayRcvBtnUrdu').on('click', function( ev ){

            acct.showAllRows();

            ev.preventDefault();
            window.open(base_url + 'application/views/reportprints/payableReceivableUrdu.php', "Payable/Receivable Report", "width=1000, height=842");
        });

        $('input[name=etype]').on("change", function () {

            $('.grand-sum').html(0);
            $('.closing-bal').html(0);
            $('.opening-bal').html(0);
            $('.payments-sum').html(0);
            $('.purchases-sum').html(0);
            $('.sales-sum').html(0);
            $('.cash-sales-sum').html(0);
            $('.discount-sales-sum').html(0);

            $('.pimports-sum').html(0);
            $('.grand-total').html(0);
            $('.grand-debit').html(0);
            $('.purchasereturns-sum').html(0);
            $('.grand-credit').html(0);
            $('.receipts-sum').html(0);
            $('.grand-lcy').html(0);
            $('.grand-fcy').html(0);

            $('#CPVRows').empty();

            var checkedElVal = $('input[name=etype]:checked').val();

            if ((checkedElVal == "receiveable") || (checkedElVal == "payable")) {

                $('.printCpvCrvBtn').hide();

                if ($(".groupby-filter").is(":visible")) {
                    $(".groupby-filter").hide();
                }
                
                if ($(".groupby-filter-pr").is(":hidden")) {
                    $(".groupby-filter-pr").show();
                }

                if ($('.printPayRcvBtn').is(':hidden')) {
                    $('.printPayRcvBtn').show();
                };
                if ($('.printPayRcvBtnUrdu').is(':hidden')) {
                    $('.printPayRcvBtnUrdu').show();
                };
                $("#city").prop("checked", true);


                $('.printDayBook').hide();
            }
            else {

                if ($(".groupby-filter-pr").is(":visible")) {
                    $(".groupby-filter-pr").hide();
                }
                $("#date").prop("checked", true);

                if ($(".groupby-filter").is(":hidden")) {
                    $(".groupby-filter").show();
                    $("input[value=party]").parent("label").show();
                }

                $("input[value=party]").parent("label").show();

                if ($('.printPayRcvBtn').is(':visible')) {
                    $('.printPayRcvBtn').hide();
                };
                if ($('.printPayRcvBtnUrdu').is(':visible')) {
                    $('.printPayRcvBtnUrdu').hide();
                };

                if ((checkedElVal === 'cpv') || (checkedElVal === 'crv') || (checkedElVal === 'expense')) {
                    $('.printCpvCrvBtn').show();
                } else {
                    $('.printCpvCrvBtn').hide();
                }

                if ((checkedElVal === 'daybook') || (checkedElVal === 'jv')) {
                    $('.printDayBook').show()
                } else {
                    // $('.printDayBook').hide();
                }
            }
        });

$(".show-rept").on("click", function (e) {

    $('.grand-sum').html(0);
    $('.salereturns-sum').html(0);
    $('.payments-sum').html(0);
    $('.receipts-sum').html(0);
    $('.closing-bal').html(0);
    $('.purchases-sum').html(0);
    $('.sales-sum').html(0);
    $('.cash-sales-sum').html(0);
    $('.discount-sales-sum').html(0);

    $('.opening-bal').html(0);
    $('.pimports-sum').html(0);
    $('.grand-total').html(0);
    $('.purchasereturns-sum').html(0);
    $('.grand-debit').html(0);
    $('.grand-credit').html(0);
    $('.grand-lcy').html(0);
    $('.grand-fcy').html(0);
    $('.pimports-sum').html(0);

    $('#CPVRows').empty();

    e.preventDefault();

    var what = acct.getCurrentView();
    var etype = acct.getEtype();
    var from = $("#from").val();
    var to = $("#to").val();

    if ((etype === 'cpv') || (etype === 'crv')) {
        acct.fetchCashReport(from, to, etype, what);
    }
    else if (etype === 'jv') {
        acct.fetchJVReport(from, to, etype, what);
    }
    else if (etype === 'expense') {
        acct.fetchExpenseReport(from, to, etype, what);
    }
    else if (etype === 'daybook') {

        // acct.fetchPurchaseTotal(from, to);
        //acct.fetchPurchaseImportTotal(from, to);
        //acct.fetchSaleTotal(from, to);
        // acct.fetchCashSaleTotal(from, to);
       // acct.fetchDiscountSaleTotal(from, to);
        acct.fetchPaymentRangeSum(from, to);
        acct.fetchReceiptRangeSum(from, to);
        //acct.fetchPurchaseReturnTotal(from, to);
       // acct.fetchSaleReturnTotal(from, to);
        acct.fetchOpeningBalance(from);
        acct.fetchClosingBalance(to);
        acct.fetchDayBookReport(from, to, etype, what);
    }
    else if ((etype === 'payable') || (etype === 'receiveable')) {
        acct.fetchPayRecvReport(from, to, etype);
    }
});

},

showAllRows : function (){

    var oSettings = acct.dTable.fnSettings();
    oSettings._iDisplayLength = 50000;

    acct.dTable.fnDraw();
},

fetchPayRecvReport : function (from, to, etype ){

    $('.grand-amount-block').show();
    $('.grand-amount').html(0);
    $('.opening-bal-block').hide();
    $('.opening-bal').html(0);
    $('.purchases-sum').html(0);
    $('.sales-sum').html(0);
    $('.cash-sales-sum').html(0);
    $('.discount-sales-sum').html(0);

    $('.pimports-sum').html(0);
    $('.purchasereturns-sum').html(0);
    $('.salereturns-sum').html(0);
    $('.closing-bal-block').hide();
    $('.closing-bal').html(0);

    $('.grand-debcred-block').hide();
    $('.grand-debit').html(0);
    $('.grand-credit').html(0);

    if (typeof acct.dTable != 'undefined') {
        acct.dTable.fnDestroy();
        $('#CPVRows').empty();
    }

    $("#cpv_datatable_example").show();
    $(".printBtn").show();

    var what = acct.getCurrentView();
    var crit = getcrit();

    $.ajax({
        url: base_url + 'index.php/report/fetchPayRecvReportData',
        type: 'POST',
        dataType: 'JSON',
        data: { from: from, to : to, etype : etype, company_id : $('#cid').val() ,'what':what ,'crit' :crit},

        beforeSend: function(){
            console.log(this.data);
        },

        success : function(data){

                // debugger

                if (data.length !== 0) {

                    var reportThead = $("#cpv_datatable_example thead");
                    var reportRows = $("#CPVRows");

                    
                    var source = $('#pr-head-template').html();
                    var template = Handlebars.compile(source);
                    var html = template({});



                    var prevVoucher = "";
                    var prevVoucherMatch = "";

                    var netSum = 0;
                    var grossSum = 0;

                    reportThead.html(html);


                    $(data).each(function (index, elem){

                        if ((etype === 'payable') && (elem.BALANCE > 0)) return true;

                        else if ((etype === 'receiveable') && (elem.BALANCE < 0)) return true;

                        var obj = {};

                        obj.SERIAL = reportRows.find('tr').length + 1;
                        obj.PARTY = elem.ACCOUNT_NAME;
                        obj.URDUNAME = elem.URDU_NAME;

                        obj.PHONE_OFF = elem.PHONE_OFF;
                        obj.ADDRESS = elem.ADDRESS;
                        obj.EMAIL = elem.EMAIL;
                        obj.MOBILE = elem.MOBILE;
                        obj.BALANCE = parseFloat(elem.BALANCE).toFixed(0);
                        
                        prevVoucherMatch=elem.VOUCHER;
                        if (prevVoucher != prevVoucherMatch) {
                            if (index !== 0) {


                                var source   = $("#pr-row-template-group-footer").html();
                                var template = Handlebars.compile(source);
                                var html = template({TotalName : 'Sub Total:',BalanceTotal : (grossSum).toFixed(0)});

                                reportRows.append(html);
                            }


                            var source   = $("#pr-row-template-group-head").html();
                            var template = Handlebars.compile(source);
                            var html = template({GroupName : prevVoucherMatch });

                            reportRows.append(html);




                            grossSum = 0;
                            grossDebit='';
                            grossCredit='';
                            grossClBalance='';


                            prevVoucher = prevVoucherMatch;
                        }
                        netSum += parseFloat(obj.BALANCE);
                        grossSum += parseFloat(obj.BALANCE);


                  

                        var source = $("#pr-row-template").html();
                        var template = Handlebars.compile(source);
                        var html = template(obj);

                        reportRows.append(html);

                        if (index === (data.length -1)) {

                            var source   = $("#pr-row-template-group-footer").html();
                            var template = Handlebars.compile(source);
                            var html = template({TotalName : 'Sub Total:',BalanceTotal : (grossSum).toFixed(0)});

                            reportRows.append(html);


                            var source   = $("#pr-row-template-group-footer").html();
                            var template = Handlebars.compile(source);
                            var html = template({TotalName : 'Grand Total:',BalanceTotal : (netSum).toFixed(0)});

                            reportRows.append(html);


                        };
                    });

$('.grand-amount').html(netSum.toFixed(2));


}

acct.bindGrid();
},

error : function ( error ){
    console.log("Error: " + error);
}
});
},
fetchSaleTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to ,'etype':''},

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.sales-sum').html(0);
            }
            else{
                $('.sales-sum').html(-parseFloat(data[0].PURCHASES_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},
fetchCashSaleTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to ,'etype':''},

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.cash-sales-sum').html(0);
            }
            else{
                $('.cash-sales-sum').html(parseFloat(data[0].PURCHASES_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},

fetchDiscountSaleTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to ,'etype':''},

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.discount-sales-sum').html(0);
            }
            else{
                $('.discount-sales-sum').html(parseFloat(data[0].PURCHASES_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},


fetchReceiptRangeSum : function ( from , to) {

   $.ajax({

       url: base_url + 'index.php/payment/fetchReceiptRangeSum',
       type: 'POST',
       dataType: 'JSON',
       data : { from : from, to : to },

       beforeSend: function(){ },
       success : function(data){

           if (data.length === 0) {
               $('.receipts-sum').html(0);
           }
           else{
               $('.receipts-sum').html(isNaN(parseFloat(data[0].RECEIPT_TOTAL).toFixed(0)) ? 0 : parseFloat(data[0].RECEIPT_TOTAL).toFixed(0) );
           }
       },

       error : function ( error ){
           alert("Error: " + error);
       }
   });

},

fetchPaymentRangeSum : function ( from , to) {

   $.ajax({

       url: base_url + 'index.php/payment/fetchPaymentRangeSum',
       type: 'POST',
       dataType: 'JSON',
       data : { from : from, to : to },

       beforeSend: function(){ },
       success : function(data){

           if (data.length === 0) {
               $('.payments-sum').html(0);
           }
           else{
               $('.payments-sum').html(isNaN(parseFloat(data[0].PAYMENT_TOTAL).toFixed(0)) ? 0 : parseFloat(data[0].PAYMENT_TOTAL).toFixed(0) );
           }
       },

       error : function ( error ){
           alert("Error: " + error);
       }
   });

},

fetchSaleReturnTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to,'etype':'' },

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.salereturns-sum').html(0);
            }
            else{
                $('.salereturns-sum').html(parseFloat(data[0].PURCHASES_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},

fetchPurchaseReturnTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to ,'etype':''},

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.purchasereturns-sum').html(0);
            }
            else{
                $('.purchasereturns-sum').html(-parseFloat(data[0].PURCHASES_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},

fetchPurchaseImportTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchImportRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to },

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.pimports-sum').html(0);
            }
            else{
                $('.pimports-sum').html(parseFloat(data[0].PIMPORTS_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},

fetchPurchaseTotal : function ( from, to) {
    $.ajax({

        url: base_url + 'index.php/purchase/fetchRangeSum',
        type: 'POST',
        dataType: 'JSON',
        data : { from : from, to : to,'etype':'' },

        beforeSend: function(){ },
        success : function(data){

            if (data.length === 0) {
                $('.purchases-sum').html(0);
            }
            else{
                $('.purchases-sum').html(parseFloat(data[0].PURCHASES_TOTAL).toFixed(0));
            }
        },

        error : function ( error ){
            alert("Error: " + error);
        }
    });
},

fetchClosingBalance : function (to) {
    $.ajax({
        url: base_url + 'index.php/account/fetchClosingBalance',
        type: 'POST',
        dataType: 'JSON',
        data: { to: to },

        beforeSend: function(){ },

        success : function(data){
            console.log(data);
            $('.closing-bal').html(data[0]['CLOSING_BALANCE']);
        },

        error : function ( error ){
            alert("Error showing closing balance: " + JSON.parse(error));
        }
    });
},

fetchOpeningBalance : function (from) {
    $.ajax({
        url: base_url + 'index.php/account/fetchOpeningBalance',
        type: 'POST',
        dataType: 'JSON',
        data: { to: from },

        beforeSend: function(){ },

        success : function(data){
            console.log(data);
            $('.opening-bal').html(data[0]['OPENING_BALANCE']);
        },

        error : function ( error ){
            alert("Error showing opening balance: " + JSON.parse(error));
        }
    });
},

fetchDayBookReport : function (from, to, etype, what){

        // Unhide following from daybook report
        $('.opening-bal').closest('.blue').show();
        $('.closing-bal').closest('.blue').show();
        $('.purchases-sum').closest('.blue').show();
        $('.sales-sum').closest('.blue').show();
        $('.purchasereturns-sum').closest('.blue').show();
        $('.salereturns-sum').closest('.blue').show();
        $('.payments-sum').closest('.blue').show();
        $('.receipts-sum').closest('.blue').show();

        // functionality doesn't exist, so let it stay hidden
        $('.pimports-sum').closest('.blue').hide();

        //////////////////////////////////////////////



        $('.grand-amount-block').hide();
        $('.grand-amount').html(0);
        $('.closing-bal-block').show();
        $('.closing-bal').html(0);

        $('.grand-debcred-block').show();
        $('.pimports-sum').html(0);
        $('.opening-bal-block').show();

        $('.purchases-sum').html(0);
        $('.salereturns-sum').html(0);
        $('.sales-sum').html(0);
        $('.opening-bal').html(0);
        $('.grand-debit').html(0);
        $('.purchasereturns-sum').html(0);
        $('.grand-credit').html(0);

        if (typeof acct.dTable != 'undefined') {
            acct.dTable.fnDestroy();
            $('#CPVRows').empty();
        }

        $("#cpv_datatable_example").show();
        $(".printBtn").show();

        $.ajax({
            url: base_url + 'index.php/report/fetchDayBoookReportData',
            type: 'POST',
            dataType: 'JSON',
            data: { from: from, to : to, etype : etype, what : what,company_id: $('#cid').val() },

            beforeSend: function(){
                console.log(this.data);
            },

            success : function(data){

                // debugger

                var prevDate = '';
                var prevParty = '';
                var prevInvoice = '';

                var subCredit = 0;
                var netCredit = 0;

                var subDebit = 0;
                var netDebit = 0;

                if (data.length !== 0) {

                    var reportThead = $("#cpv_datatable_example thead");
                    var reportRows = $("#CPVRows");

                    // Show the table head
                    var source = $('#db-head-template').html();
                    var template = Handlebars.compile(source);
                    var html = template({});

                    reportThead.html(html);


                    $(data).each(function (index, elem){

                        var obj = {};

                        obj.SERIAL = index + 1;
                        // obj.VRNOA = ( elem.VRNOA ) ? (elem.VRNOA + '-' + elem.ETYPE) : '-';
                        
                        
                        // alert(elem.ETYPE);
                        if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/saleorder/Sale_Invoice?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/journal?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( ( elem.ETYPE.toLowerCase() == 'cpv' ) || ( elem.ETYPE.toLowerCase() == 'crv' ) ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment?vrnoa=' + elem.VRNOA + '&ETYPE=' + elem.ETYPE.toLowerCase() + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( ( elem.ETYPE.toLowerCase() == 'pd_issue' ) ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeIssue?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( ( elem.ETYPE.toLowerCase() == 'pd_receive' ) ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeReceive?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'purchase' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/purchase?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'salereturn' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/salereturn?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'purchasereturn' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/purchasereturn?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'pur_import' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/purchase/import?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'assembling' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/item/assdeass?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'navigation' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/stocknavigation?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'production' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/production?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'consumption' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/consumption?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'materialreturn' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/materialreturn?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'moulding' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/moulding?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'order_loading' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/saleorder/partsloading?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'cpv' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'crv' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/jv?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'pd_receive' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeReceive?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        } else if ( elem.ETYPE.toLowerCase() == 'pd_issue' ) {
                            obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeIssue?vrnoa=' + elem.VRNOA + '">' + elem.VRNOA + '-' + elem.ETYPE + '</a>';
                        }

                        else {
                            obj.vrnoa = elem.vrnoa + '-' + elem.ETYPE;
                        }

                        obj.REMARKS = ( elem.REMARKS  ? elem.REMARKS : '-')
                        obj.DATE = ( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                        obj.PARTY = ( elem.PARTY ) ? elem.PARTY : '-';
                        obj.DEBIT = ( elem.DEBIT ) ? elem.DEBIT : '0';
                        obj.CREDIT = ( elem.CREDIT ) ? elem.CREDIT : '0';


                        if ((what === 'date') && (prevDate !== elem.DATE)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#daybook-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = 0;
                                subDebit = 0;
                            };

                            // echo out the date head
                            obj.DATE1=( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                            var source = $("#db-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevDate = elem.DATE;
                        }
                        else if ((what === 'user') && (prevDate !== elem.uname)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#daybook-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = 0;
                                subDebit = 0;
                            };

                            // echo out the date head
                            obj.DATE1=elem.uname;
                            var source = $("#db-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevDate = elem.uname;
                        }
                        else if ((what === 'invoice') && (prevInvoice !== elem.VRNOA)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#daybook-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = 0;
                                subDebit = 0;
                            };

                            // echo out the invoice head
                            var source = $("#db-ihead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevInvoice = elem.VRNOA;
                        }
                        else if ((what === 'party') && (prevParty !== elem.PARTY)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#daybook-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = 0;
                                subDebit = 0;
                            };

                            // echo out the party head
                            var source = $("#db-phead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevParty = elem.PARTY;
                        }

                        // echo out the report item.
                        var source = $("#db-row-template").html();
                        var template = Handlebars.compile(source);
                        var html = template(obj);

                        reportRows.append(html);

                        // Add the sums
                        netDebit += parseFloat(obj.DEBIT);
                        subDebit += parseFloat(obj.DEBIT);

                        netCredit += parseFloat(obj.CREDIT);
                        subCredit += parseFloat(obj.CREDIT);

                        if (index === ( data.length-1 )) {

                            // echo out the date head
                            var source = $("#daybook-subsum-template").html();
                            var template = Handlebars.compile(source);
                            var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                            reportRows.append(html);

                            subCredit = 0;
                            subDebit = 0;

                        };
                    });


$('.grand-debit').html(netDebit.toFixed(0));
$('.grand-credit').html(netCredit.toFixed(0));

                    // echo out the date head
                    var source = $("#daybook-netsum-template").html();
                    var template = Handlebars.compile(source);
                    var html = template({ NET_CREDIT : netCredit.toFixed(0), NET_DEBIT : netDebit.toFixed(0) });

                    reportRows.append(html);
                }

                acct.bindGrid();
                
            },

            error : function ( error ){
                console.log("Error: " + error);
            }
        });
},

fetchExpenseReport : function (from, to, etype, what){

    $('.grand-amount-block').show();
    $('.grand-amount').html(0);
    $('.purchases-sum').html(0);
    $('.sales-sum').html(0);
    $('.closing-bal-block').hide();
    $('.closing-bal').html(0);
    $('.salereturns-sum').html(0);

    $('.opening-bal-block').hide();
    $('.opening-bal').html(0);
    $('.purchasereturns-sum').html(0);

    $('.grand-debcred-block').hide();
    $('.grand-debit').html(0);
    $('.grand-credit').html(0);

    if (typeof acct.dTable != 'undefined') {
        acct.dTable.fnDestroy();
        $('#CPVRows').empty();
    }

    $("#cpv_datatable_example").show();
    $(".printBtn").show();
    var crit = getcrit();


    $.ajax({
        url: base_url + 'index.php/report/fetchExpenseReportData',
        type: 'POST',
        dataType: 'JSON',
        data: { from: from, to : to, etype : etype, what : what , company_id:$('#cid').val(),'crit':crit },

        beforeSend: function(){
            console.log(this.data);
        },

        success : function(data){

                // debugger

                var prevDate = '';
                var prevParty = '';
                var prevInvoice = '';

                var subSum = 0;
                var netSum = 0;

                if (data.length !== 0) {

                    var reportThead = $("#cpv_datatable_example thead");
                    var reportRows = $("#CPVRows");

                    // Show the table head
                    var source = $('#payment-head-template').html();
                    var template = Handlebars.compile(source);
                    var html = template({});

                    reportThead.html(html);


                    $(data).each(function (index, elem){

                        var obj = {};

                        obj.SERIAL = reportRows.find('tr').length + 1;
                        obj.REMARKS = ( elem.REMARKS  ? elem.REMARKS : '-')
                        obj.DATE = ( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                        obj.PARTY = ( elem.PARTY ) ? elem.PARTY : '-';
                        obj.AMOUNT = ( elem.DEBIT ) ? elem.DEBIT : '0';
                        obj.VRNOA = ( elem.VRNOA ) ? (elem.VRNOA + '-' + elem.ETYPE.toUpperCase()) : '-';

                        // if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/sale?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/journal?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'cpv' ) || ( elem.ETYPE.toLowerCase() == 'crv' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment?vrnoa=' + obj.VRNOA + '&etype=' + elem.ETYPE.toLowerCase() + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'pd_issue' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeIssue?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'pd_receive' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeReceive?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'purchase' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchase?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'salereturn' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/salereturn?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'purchasereturn' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchasereturn?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'pur_import' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchase/import?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'assembling' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/item/assdeass?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // }else if ( elem.ETYPE.toLowerCase() == 'navigation' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/stocknavigation?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // }
                        // else {
                        //     obj.VRNOA = obj.VRNOA + '-' + elem.ETYPE;
                        // }

                        if ((what === 'date') && (prevDate !== elem.DATE)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the date head
                            obj.DATE1=( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                            var source = $("#payment-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevDate = elem.DATE;

                        }
                        else if ((what === 'user') && (prevDate !== elem.uname)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the date head
                            obj.DATE1=elem.uname;
                            var source = $("#payment-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevDate = elem.uname;

                        }
                        else if ((what === 'invoice') && (prevInvoice !== elem.VRNOA)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the invoice head
                            var source = $("#payment-ihead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevInvoice = elem.VRNOA;



                        }
                        else if ((what === 'party') && (prevParty !== elem.PARTY)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the party head
                            var source = $("#payment-phead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevParty = elem.PARTY;


                        }

                        // echo out the report item.
                        var source = $("#payment-row-template").html();
                        var template = Handlebars.compile(source);
                        var html = template(obj);

                        reportRows.append(html);


                        subSum += parseFloat(obj.AMOUNT);
                        netSum += parseFloat(obj.AMOUNT);

                        if (index === ( data.length - 1 )) {

                            // echo out the date head
                            var source = $("#payment-subsum-template").html();
                            var template = Handlebars.compile(source);
                            var html = template({ SUBSUM : subSum.toFixed(0) });

                            reportRows.append(html);

                        };
                    });

$('.grand-amount').html(netSum.toFixed(0));

var source = $("#payment-netsum-template").html();
var template = Handlebars.compile(source);
var html = template({ NETSUM : netSum.toFixed(0) });

reportRows.append(html);

}

acct.bindGrid();
},

error : function ( error ){
    console.log("Error: " + error);
}
});
},

fetchJVReport : function (from, to, etype, what){

        // Unhide following from daybook report
        $('.opening-bal').closest('.blue').hide();
        $('.closing-bal').closest('.blue').hide();
        $('.purchases-sum').closest('.blue').hide();
        $('.sales-sum').closest('.blue').hide();
        $('.purchasereturns-sum').closest('.blue').hide();
        $('.salereturns-sum').closest('.blue').hide();
        $('.payments-sum').closest('.blue').hide();
        $('.receipts-sum').closest('.blue').hide();
        $('.pimports-sum').closest('.blue').hide();

        $('.grand-amount-block').hide();
        $('.grand-amount').html(0);
        $('.purchases-sum').html(0);
        $('.purchasereturns-sum').html(0);
        $('.sales-sum').html(0);
        $('.closing-bal-block').hide();
        $('.closing-bal').html(0);

        $('.salereturns-sum').html(0);

        $('.opening-bal-block').hide();
        $('.opening-bal').html(0);

        $('.grand-debcred-block').show();
        $('.grand-debit').html(0);
        $('.grand-credit').html(0);

        if (typeof acct.dTable != 'undefined') {
            acct.dTable.fnDestroy();
            $('#CPVRows').empty();
        }

        $("#cpv_datatable_example").show();
        $(".printBtn").show();
        var crit = getcrit();

        $.ajax({
            url: base_url + 'index.php/report/fetchJVReportData',
            type: 'POST',
            dataType: 'JSON',
            data: { from: from, to : to, etype : etype, what : what ,company_id:$('#cid').val(),'crit':crit},

            beforeSend: function(){
                console.log(this.data);
            },

            success : function(data){

                // debugger

                var prevDate = '';
                var prevParty = '';
                var prevInvoice = '';

                var netCredit = 0;
                var subCredit = 0;

                var netDebit = 0;
                var subDebit = 0;

                if (data.length !== 0) {

                    var reportThead = $("#cpv_datatable_example thead");
                    var reportRows = $("#CPVRows");

                    // Show the table head
                    var source = $('#jv-head-template').html();
                    var template = Handlebars.compile(source);
                    var html = template({});

                    reportThead.html(html);


                    $(data).each(function (index, elem){

                        var obj = {};

                        obj.SERIAL = reportRows.find('tr').length + 1;
                        obj.REMARKS = ( elem.REMARKS  ? elem.REMARKS : '-')
                        obj.DATE = ( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                        obj.PARTY = ( elem.PARTY ) ? elem.PARTY : '-';
                        obj.DEBIT = ( elem.DEBIT) ? elem.DEBIT : '0';
                        obj.CREDIT = ( elem.CREDIT) ? elem.CREDIT : '0';

                        obj.VRNOA = ( elem.VRNOA ) ? (elem.VRNOA + '-' + elem.ETYPE.toUpperCase()) : '-';

                        // if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/sale?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/journal?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'cpv' ) || ( elem.ETYPE.toLowerCase() == 'crv' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment?vrnoa=' + obj.VRNOA + '&etype=' + elem.ETYPE.toLowerCase() + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'pd_issue' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeIssue?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'pd_receive' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeReceive?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'purchase' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchase?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'salereturn' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/salereturn?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'purchasereturn' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchasereturn?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'pur_import' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchase/import?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'assembling' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/item/assdeass?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // }else if ( elem.ETYPE.toLowerCase() == 'navigation' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/stocknavigation?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // }
                        // else {
                        //     obj.VRNOA = obj.VRNOA + '-' + elem.ETYPE;
                        // }

                        if ((what === 'date') && (prevDate !== elem.DATE)) {

                            if (index !== 0) {
                                // echo out the invoice head
                                var source = $("#jv-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = subDebit = 0;
                            };

                            // echo out the date head
                            obj.DATE1=( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                            var source = $("#jv-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);


                            prevDate = elem.DATE;
                        }
                        else if ((what === 'user') && (prevDate !== elem.uname)) {

                            if (index !== 0) {
                                // echo out the invoice head
                                var source = $("#jv-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = subDebit = 0;
                            };

                            // echo out the date head
                            obj.DATE1=elem.uname;
                            var source = $("#jv-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);


                            prevDate = elem.uname;
                        }
                        else if ((what === 'invoice') && (prevInvoice !== elem.VRNOA)) {

                            if (index !== 0) {
                                // echo out the invoice head
                                var source = $("#jv-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = subDebit = 0;
                            };

                            // echo out the invoice head
                            var source = $("#jv-ihead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevInvoice = elem.VRNOA;
                        }
                        else if ((what === 'party') && (prevParty !== elem.PARTY)) {

                            if (index !== 0) {
                                // echo out the invoice head
                                var source = $("#jv-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                                reportRows.append(html);

                                subCredit = subDebit = 0;
                            };

                            // echo out the party head
                            var source = $("#jv-phead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);


                            prevParty = elem.PARTY;
                        }

                        // echo out the report item.
                        var source = $("#jv-row-template").html();
                        var template = Handlebars.compile(source);
                        var html = template(obj);

                        reportRows.append(html);

                        subCredit += parseFloat(obj.CREDIT);
                        netCredit += parseFloat(obj.CREDIT);

                        subDebit += parseFloat(obj.DEBIT);
                        netDebit += parseFloat(obj.DEBIT);

                        if (index === ( data.length-1 )) {

                            // echo out the invoice head
                            var source = $("#jv-subsum-template").html();
                            var template = Handlebars.compile(source);
                            var html = template({ SUB_CREDIT : subCredit, SUB_DEBIT : subDebit });

                            reportRows.append(html);

                            subCredit = subDebit = 0;

                        };
                    });

                    // echo out the invoice head
                    var source = $("#jv-netsum-template").html();
                    var template = Handlebars.compile(source);
                    var html = template({ NET_CREDIT : netCredit, NET_DEBIT : netDebit });

                    reportRows.append(html);

                    $('.grand-debit').html(netDebit);
                    $('.grand-credit').html(netCredit);

                    netCredit = netDebit = 0;

                }

                acct.bindGrid();
            },

            error : function ( error ){
                console.log("Error: " + error);
            }
        });
},

fetchCashReport : function (from, to, etype, what){

    $('.grand-amount-block').show();
    $('.grand-amount').html(0);
    $('.salereturns-sum').html(0);
    $('.purchases-sum').html(0);
    $('.purchasereturns-sum').html(0);
    $('.sales-sum').html(0);
    $('.closing-bal-block').hide();
    $('.closing-bal').html(0);

    $('.opening-bal-block').hide();
    $('.opening-bal').html(0);

    $('.grand-debcred-block').hide();
    $('.grand-debit').html(0);
    $('.grand-credit').html(0);

    if (typeof acct.dTable != 'undefined') {
        acct.dTable.fnDestroy();
        $('#CPVRows').empty();
    }

    $("#cpv_datatable_example").show();
    $(".printBtn").show();
    
    var crit = getcrit();

    $.ajax({
        url: base_url + 'index.php/report/fetchCashReportData',
        type: 'POST',
        dataType: 'JSON',
        data: { from: from, to : to, etype : etype, what : what,company_id:$('#cid').val(),'crit':crit },

        beforeSend: function(){
            console.log(this.data);
        },

        success : function(data){

                // debugger

                var prevDate = '';
                var prevParty = '';
                var prevInvoice = '';

                var netSum = 0;

                if (data.length !== 0) {

                    var reportThead = $("#cpv_datatable_example thead");
                    var reportRows = $("#CPVRows");

                    // Show the table head
                    var source = $('#payment-head-template').html();
                    var template = Handlebars.compile(source);
                    var html = template({});

                    reportThead.html(html);

                    var subSum = 0;


                    $(data).each(function (index, elem){

                        var obj = {};

                        obj.SERIAL = index + 1;
                        obj.REMARKS = ( elem.REMARKS  ? elem.REMARKS : '-')
                        obj.DATE = ( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                        obj.PARTY = ( elem.PARTY ) ? elem.PARTY : '-';
                        obj.AMOUNT = ( elem.AMOUNT) ? elem.AMOUNT : 0;

                        obj.VRNOA = ( elem.VRNOA ) ? (elem.VRNOA + '-' + elem.ETYPE.toUpperCase()) : '-';

                        // if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/sale?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/journal?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'cpv' ) || ( elem.ETYPE.toLowerCase() == 'crv' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment?vrnoa=' + obj.VRNOA + '&etype=' + elem.ETYPE.toLowerCase() + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'pd_issue' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeIssue?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( ( elem.ETYPE.toLowerCase() == 'pd_receive' ) ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/payment/chequeReceive?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'purchase' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchase?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'salereturn' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/salereturn?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'purchasereturn' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchasereturn?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'pur_import' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/purchase/import?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // } else if ( elem.ETYPE.toLowerCase() == 'assembling' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/item/assdeass?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // }else if ( elem.ETYPE.toLowerCase() == 'navigation' ) {
                        //     obj.VRNOA = '<a href="' + base_url + 'index.php/stocknavigation?vrnoa=' + obj.VRNOA + '">' + obj.VRNOA + '-' + elem.ETYPE.toUpperCase() + '</a>';
                        // }
                        // else {
                        //     obj.VRNOA = obj.VRNOA + '-' + elem.ETYPE;
                        // }

                        netSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);

                        if ((what === 'date') && (prevDate !== elem.DATE)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum.toFixed(0) });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the date head
                            obj.DATE1=( elem.DATE ) ? elem.DATE.substring(0,10) : '-';
                            var source = $("#payment-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevDate = elem.DATE;
                        }
                        if ((what === 'user') && (prevDate !== elem.uname)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum.toFixed(0) });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the date head
                            obj.DATE1=elem.uname;
                            var source = $("#payment-dhead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevDate = elem.uname;
                        }
                        else if ((what === 'invoice') && (prevInvoice !== elem.VRNOA)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum.toFixed(0) });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the invoice head
                            var source = $("#payment-ihead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevInvoice = elem.VRNOA;
                        }
                        else if ((what === 'party') && (prevParty !== elem.PARTY)) {

                            if (index !== 0) {
                                // echo out the date head
                                var source = $("#payment-subsum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({ SUBSUM : subSum.toFixed(0) });

                                reportRows.append(html);

                                subSum = 0;
                            };

                            // echo out the party head
                            var source = $("#payment-phead-template").html();
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            reportRows.append(html);

                            prevParty = elem.PARTY;
                        }

                        // echo out the report item.
                        var source = $("#payment-row-template").html();
                        var template = Handlebars.compile(source);
                        var html = template(obj);

                        reportRows.append(html);


                        subSum += isNaN( parseFloat(obj.AMOUNT) ) ? 0 : parseFloat(obj.AMOUNT);

                        if (index === (data.length-1)) {
                            // echo out the date head
                            var source = $("#payment-subsum-template").html();
                            var template = Handlebars.compile(source);
                            var html = template({ SUBSUM : subSum.toFixed(0) });

                            reportRows.append(html);

                            subSum = 0;
                        };
                    });

                    // echo out the report sum.
                    var source = $("#payment-netsum-template").html();
                    var template = Handlebars.compile(source);
                    var html = template({ NETSUM : netSum.toFixed(0) });

                    reportRows.append(html);

                }

                $('.grand-amount').html(netSum.toFixed(0));

                acct.bindGrid();
            },

            error : function ( error ){
                console.log("Error: " + error);
            }
        });

},

bindGrid : function() {
        // $("input[type=checkbox], input:radio, input:file").uniform();
        var dontSort = [];
        $('#cpv_datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        acct.dTable = $('#cpv_datatable_example').dataTable({
            // Uncomment, if problems with datatble.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Account Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    },

    resetReport : function (){
        $("#cpv_datatable_example").fadeOut();
        $(".transaction-btn").addClass("btn-primary").siblings(".btn-primary").removeClass("btn-primary");
        $(".advanced-filter").hide();
        $(".printBtn").fadeOut();
    },

    populateDate : function () {

        var d = new Date();

        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();

        var curr_date = curr_year + '/' + curr_month + '/' + curr_date;

        $('#from').val(curr_date);
        $('#to').val(curr_date);
    },

    getCurrentReportType : function () {
        return $('input[name=etype]:checked').parent("label").text().trim();
    },

    getEtype : function () {
        return $('input[name=etype]:checked').val();
    },

    validateShowReport : function () {

        var etype = acct.getEtype();
        var flag = true;

        var from = $("#from").val();
        var to = $("#to").val();

        if (typeof (etype) == "undefined") {
            alert("Please chose the report type");
            flag = false;
        }

        if (Date.parse(from) > Date.parse(to)) {
            alert("Invalid date Range Selected. Please select a valid date range.");
            flag = false;
        }

        return flag;
    },

    getCurrentView : function () {
        return $('input[name=grouping]:checked').val();
    }
}
};
var acct = new Acct();
acct.init();