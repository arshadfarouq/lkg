var AccountLedger = function() {

  var validateSearch = function() {

    var errorFlag = false;
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    //var pid = $('#name_dropdown').val();

    // remove the error class first
    $('#from_date').removeClass('inputerror');
    $('#to_date').removeClass('inputerror');
    $('#name_dropdown').removeClass('inputerror');

    if ( from_date === '' || from_date === null ) {
      $('#from_date').addClass('inputerror');
      errorFlag = true;
    }
    if ( to_date === '' || to_date === null ) {
      $('#to_date').addClass('inputerror');
      errorFlag = true;
    }
    // if ( pid === '' || pid === null ) {
    //  $('#name_dropdown').addClass('inputerror');
    //  errorFlag = true;
    // }
    if (from_date > to_date ){
      $('#from_date').addClass('inputerror');
      alert('Starting date must Be less than ending date.........')
      errorFlag = true;   
    }

    return errorFlag;
  }

  var Account_Flow = function() {
    var error = validateSearch();
    if (!error) {
      var dt1 = $('#from_date').val();
      var dt2 = $('#to_date').val();
      
      dt1= dt1.replace('/','-');
      dt1= dt1.replace('/','-');
      dt2= dt2.replace('/','-');
      dt2= dt2.replace('/','-');
      var party_id=1;//$('#name_dropdown').val();
      var company_id =$('#cid').val();
      var user_print = $('#uname').val();
      
      var url = base_url + 'index.php/doc/AccountFlow/' + dt1 + '/' + dt2  + '/' +  company_id  + '/'  + party_id + '/' + '-1' + '/' + user_print;
      window.open(url);

    } else {
      alert('Correct the errors...');
    }
    // window.open(base_url + 'application/views/reportprints/ledger.php', "Sale Report", "width=1000, height=842");
    // window.open(base_url + 'application/views/reportPrints/ledger.php', "Sale Report", "width=1000, height=842");
  }


  return {

    init : function () {
      this.bindUI();
      this.bindModalPartyGrid();
    },

    bindUI : function() {

      var self = this;
      $('#from_date').val('2014/01/01');
      $('.modal-lookup .populateAccount').on('click', function(){
        // alert('dfsfsdf');
        var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
        $("#name_dropdown").select2("val", party_id);         
      });

      $('.btnPrint2').on('click', function(e) {
        e.preventDefault();
        Account_Flow();
      });
      
      shortcut.add("F6", function() {
        e.preventDefault();
        alert();
        $('.btnPrint2').trigger('click');
      });

    }
  };
};


var accountLedger = new AccountLedger();
accountLedger.init();