var chartOfAccount = {

	init : function(){
		chartOfAccount.bindUI();
	},
	showAllRows : function (){

        var oSettings = chartOfAccount.dTable.fnSettings();
        oSettings._iDisplayLength = 50000;

        chartOfAccount.dTable.fnDraw();
    },


	bindUI : function (){

		$(document).on('ready', function(){
			chartOfAccount.populateCOAGrid();
		});
		$('#btnPrint').on('click' ,function(ev){
			chartOfAccount.showAllRows();
            ev.preventDefault();

            window.open(base_url + 'application/views/reportprints/coaPrint.php', "Chart Of Accounts", "width=1000, height=842");
		});
		shortcut.add("F9", function() {
    			$('#btnPrint').trigger('click');
		});



	},

	populateCOAGrid : function () {

        if (typeof chartOfAccount.dTable != 'undefined') {
            chartOfAccount.dTable.fnDestroy();
            $('#chartOfAccountRows').empty();
        }

		$.ajax({
			url: base_url + 'index.php/report/getChartOfAccounts',
			type: 'POST',
			dataType : 'JSON',
			success : function (data) {

				if (data.length !== 0) {

					var prevL1 = '';
					var prevL2 = '';
					var prevL3 = '';

					$(data).each(function(index,elem){

						// debugger

						var origAcctId = elem.ACCOUNT_ID;

						if (origAcctId.substr(0,2) !== prevL1) {

							prevL1 = origAcctId.substr(0,2);

							elem.ACCOUNT_ID = prevL1;

							var source   = $("#ledger-level1-template").html();
							var template = Handlebars.compile(source);
							var l1row = template(elem);

							$('#chartOfAccountRows').append(l1row);
						}

						if (origAcctId.substr(0,5) !== prevL2) {

							prevL2 = origAcctId.substr(0,5);

							elem.ACCOUNT_ID = prevL2;

							var source   = $("#ledger-level2-template").html();
							var template = Handlebars.compile(source);
							var l2Row = template(elem);

							$('#chartOfAccountRows').append(l2Row);
						}

						if (origAcctId.substr(0,8) !== prevL3) {

							prevL3 = origAcctId.substr(0,8);	

							elem.ACCOUNT_ID = prevL3;										

							var source   = $("#ledger-level3-template").html();
							var template = Handlebars.compile(source);
							var l3Row = template(elem);

							$('#chartOfAccountRows').append(l3Row);
						}

						elem.ACCOUNT_ID = origAcctId;

						var source   = $("#chartOfAccountRow-template").html();
						var template = Handlebars.compile(source);
						var html = template(elem);

						$('#chartOfAccountRows').append(html);
					});
				}
				else{
					alert("No record found.");
				}

				chartOfAccount.bindGrid();

			},
			error : function (error){
				alert("Error : " + error);
			}
		});
	},

	bindGrid : function (){

        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        chartOfAccount.dTable = $('#datatable_example').dataTable({
            // Uncomment, if problems found with datatable
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "iDisplayLength" : 100,
            "oTableTools": {
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Chart of Accounts" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });		

	}
};

$(document).ready(function(){
	chartOfAccount.init();
});