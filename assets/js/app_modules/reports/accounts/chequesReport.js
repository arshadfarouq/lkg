var inventory = {
    
    init : function (){
        inventory.bindUI();
    },

    bindUI : function () {
        $(".btnReset").on('click', function (){

            $('.grand-sum').html(0);
            $('.grand-total').html(0);
            $('.grand-debit').html(0);
            $('.grand-credit').html(0);
            $('.grand-lcy').html(0);
            $('.grand-fcy').html(0);

            $('#inventoryRows').empty();

            inventory.resetReport();
        });
        shortcut.add("F6", function() {
                $('.btnSearch').trigger('click');
        });
        shortcut.add("F9", function() {
            $('.btnPrint').trigger('click');
        });

        shortcut.add("F5", function() {
            self.resetVoucher();
        });

        $('#from_date').val('2014/01/01');

        $('.btnPrint').on('click', function( e ){
            e.preventDefault();
            inventory.showAllRows();
            
            window.open(base_url + 'application/views/reportprints/vouchers_cheque_reports.php', "Purchase Report", 'width=1000, height=842');
        });

        $(".btnSearch").on('click', function (){

            $('.grand-sum').html(0);
            $('.grand-total').html(0);
            $('.grand-debit').html(0);
            $('.grand-credit').html(0);
            $('.grand-lcy').html(0);
            $('.grand-fcy').html(0);

            $('#inventoryRows').empty();

            var fromEl = $('#from_date');
            var toEl = $('#to_date');
            var type = inventory.getReportType();           

            if (inventory.validDateRange(fromEl.val(), toEl.val())) {
                inventory.fetchReportData(fromEl.val(), toEl.val(), type);
            }
            else{
                fromEl.addClass('input-error');
                toEl.addClass('input-error');
            }

        });
    },

    getReportType : function () {
        return $('input[name=report_type]:checked').val().toLowerCase();
    },

    fetchReportData : function (startDate, endDate, type) {

        $('.grand-total').html('0');

        if (typeof inventory.dTable != 'undefined') {
            inventory.dTable.fnDestroy();
            $('#inventoryRows').empty();
        } 
        $.ajax({
                url: base_url + "index.php/report/getChequeReportData",                
                data: { startDate : startDate, endDate : endDate, type : type,company_id : $('#cid').val() },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                complete: function () { },
                success: function (data) {

                    if (data.length !== 0) {

                        var htmls = '';
                        var grandTotal = 0;

                        var handler = $('#cheque-template').html();
                        var template = Handlebars.compile(handler);

                        $(data).each(function(index, elem){

                            elem.VRDATE = ( elem.VRDATE ) ? elem.VRDATE.substr(0,10) : 'Not Available';
                            elem.CHEQUE_DATE = ( elem.CHEQUE_DATE ) ? elem.CHEQUE_DATE.substr(0,10) : 'Not Available';

                            grandTotal += parseFloat(elem.AMOUNT) ? parseFloat(elem.AMOUNT) : 0;

                            elem.AMOUNT = elem.AMOUNT;
                            var html = template(elem);
                            htmls += html;

                        });

                        $('.grand-total').html( grandTotal );
                        $('#inventoryRows').append(htmls);
                    }

                    inventory.bindGrid();
                },

                error: function (result) {
                    //$("*").css("cursor", "auto");
                    $("#loading").hide();
                    alert("Error:" + result);
                }
            });     

    },

    bindGrid : function() {
        // $("input[type=checkbox], input:radio, input:file").uniform();
        var dontSort = [];
        $('#datatable_inventory thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        inventory.dTable = $('#datatable_inventory').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Cheques" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    },

    showAllRows : function (){

        var oSettings = inventory.dTable.fnSettings();
        oSettings._iDisplayLength = 50000;

        inventory.dTable.fnDraw();
    },

    validDateRange  : function (from, to){
        if(Date.parse(from) > Date.parse(to)){
           return false
        }
        else{
           return true;
        }
    },  

    resetReport : function (){
        $(".printBtn").hide();
        $("#datatable_inventory").hide();
    }
};

$(document).ready(function(){
    inventory.init();
});