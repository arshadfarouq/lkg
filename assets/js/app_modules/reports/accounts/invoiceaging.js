var aging = {

	groupingConfigs : {
	      account : { template : '#voucher-phead-template', fieldName : 'account' }
	},

	init : function () {
		aging.bindUI();
	},

	bindUI : function () {

		$('#btnSendEmail').on('click', function() {
			aging.showAllRows();
			aging.sendMail();
		});

		$('.btnExcel').on('click', function() {
			aging.showAllRows();
		    general.exportExcel('datatable_example', 'Invoice Aging Report');
		});

		$('#btnShow').on('click', function () {

			var from = $('#from_date').val();
			var to = $('#to_date').val();
			var reportType = $('#reptType').val();
			var pid = $('#drpAccount').val();

			if ( Date.parse( from ) > Date.parse(to) ) {
				alert("Invalid date range selected!");
			} else {
				aging.fetchAgingData( from, to, reportType, pid );
			}

		});
		$('#btnPrint').on('click', function( ev ){
			ev.preventDefault();
			window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Aging Sheet", "width=1000, height=842");
			// window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Stock Report", 'width=1000, height=842');
		});
	},

	showAllRows : function (){

	    var oSettings = aging.dTable.fnSettings();
	    oSettings._iDisplayLength = 50000;

	    aging.dTable.fnDraw();
	},

	bindGrid : function() {
	    // $("input[type=checkbox], input:radio, input:file").uniform();
	    var dontSort = [];
	    $('#datatable_example thead th').each(function () {
	        if ($(this).hasClass('no_sort')) {
	            dontSort.push({ "bSortable": false });
	        } else {
	            dontSort.push(null);
	        }
	    });

	    aging.dTable = $('#datatable_example').dataTable({
	        // Uncomment, if problems found with datatable
	        // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
	        "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
	        "aaSorting": [[0, "asc"]],
	        "bPaginate": true,
	        "sPaginationType": "full_numbers",
	        "bJQueryUI": false,
	        "aoColumns": dontSort,
	        "bSort": false,
	        "iDisplayLength" : 100,
	        "oTableTools": {
	            "sSwfPath": "js/copy_cvs_xls_pdf.swf",
	            "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Sale Report" }]
	        }
	    });
	    $.extend($.fn.dataTableExt.oStdClasses, {
	        "s`": "dataTables_wrapper form-inline"
	    });

	},

	sendMail : function() {


		var _data = {};
		$('#datatable_example').prop('border', '1');
		_data.table = $('#datatable_example').prop('outerHTML');
		$('#datatable_example').removeAttr('border');

		_data.accTitle = $('select[name="drpAccount"]').find('option:selected').text();
		_data.accCode = $('select[name="drpAccId"]').find('option:selected').data('acccode');
		_data.contactNo = $('select[name="drpAccId"]').find('option:selected').data('contact');
		_data.contactNo = (_data.contactNo == "") ? 'N/A' : _data.contactNo;
		_data.address = $('select[name="drpAccId"]').find('option:selected').data('address');
		_data.address = (_data.address == "") ? 'N/A' : _data.address;

		_data.from = $('#from_date').val();
		_data.to = $('#to_date').val();
		_data.type = 'Invoice Aging Report - ' + $('select[name="drpReptType"]').find('option:selected').text();
		_data.email = $('#txtAddEmail').val();

		$.ajax({
			url : base_url + 'index.php/email',
			type : 'POST',
			dataType : 'JSON',
			data : _data,
			success: function(result) {
				console.log(result);
			}, error: function(error) {
				alert('Error '+ error);
			}
		});

		// close the modal dialog
		$('#btnSendEmail').siblings('button').trigger('click');
	},

	fetchAgingData : function ( from, to, reportType, pid ) {

		var groupBy = 'account';

		if (typeof aging.dTable != 'undefined') {
		    aging.dTable.fnDestroy();
		    $('.saleRows').empty();
		}


		$.ajax({
			url : base_url + 'index.php/report/fetchInvoiceAgingData',
			method : 'POST',
			dataType : 'JSON',
			data : { from : from, to : to, reportType : reportType, party_id : pid, company_id : $('.cid').val() },
			beforeSend : function () { },
			success : function ( result ) {

				if (result.length !== 0) {

				    var groupingConfig = aging.groupingConfigs[groupBy];
				    var saleRows = $('.saleRows');
				    var prevL1Val = '';
				    var prevL2Val = '';
				    var counter = 1;
				    var netSale = 0;
				    var netbal = 0;

				    $(result).each(function(index, elem){
				        if (groupingConfig)
				        {
				          if (prevL1Val != elem[groupingConfig.fieldName])
				          {
				          	if ( index !== 0 ) {
					          	var source = $( '#voucher-total-template' ).html();
					          	template = Handlebars.compile( source );
					          	html = template({ net_balance : netbal });

					          	saleRows.append( html );
				          	};

				            // Create the heading for this new group.
				            var source   = $( groupingConfig.template ).html();
				            var template = Handlebars.compile(source);
				            var html = template(elem);

				            saleRows.append(html);
				            //Reset the previous group value
				            prevL1Val = elem[groupingConfig.fieldName];

				            netbal = 0;
				          }

				          // if ( groupingConfig.subGrouping && prevL2Val != elem[groupingConfig.subGrouping.fieldName])
				          // {
				          //   // Create the heading for this new group.
				          //   var source   = $( groupingConfig.subGrouping.template ).html();
				          //   var template = Handlebars.compile(source);
				          //   var html = template(elem);

				          //   saleRows.append(html);
				          //   //Reset the previous group value
				          //   prevL2Val = elem[groupingConfig.subGrouping.fieldName];
				          // }
				        }

				        elem.serial = counter++;

				        netbal += parseFloat( elem.balance );

				        // Create the item
				        var source   = $( '#voucher-item-template' ).html();
				        var template = Handlebars.compile(source);
				        var html = template(elem);

				        saleRows.append(html);

				        netSale += parseFloat( elem.amount ) || 0;
				    });

					var source = $( '#voucher-total-template' ).html();
					template = Handlebars.compile( source );
					html = template({ net_balance : netbal });

					saleRows.append( html );
				};

				aging.bindGrid();

			},
			complete : function () { }
		});
	}

};
aging.init();