var ResultSubjectWise = function() {

	var fetchResults = function() {

		$.ajax({
			url : base_url + 'index.php/result/resultSubjectWiseReport',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {
				populateData(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		var session = "";
		var section_name = "";
		var term = "";

		$.each(data, function(index, elem) {

			if (elem.session !== session) {

				var row = 	"<tr class='level1'>"+
							"<td colspan='4' class='level1row'>"+ elem.session +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				session = elem.session;
			}

			if (elem.section_name !== section_name) {

				var row = 	"<tr class='level2'>"+
							"<td colspan='4' class='level2row'>"+ elem.section_name +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				section_name = elem.section_name;
			}

			if (elem.term !== term) {

				var row = 	"<tr class='level3'>"+
							"<td class='level3row'></td>"+
							"<td colspan='3' class='level3row'>"+ elem.term +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				term = elem.term;
			}

			row = 	"<tr>"+
					"<td>"+ elem.dcno +"</td>"+
					"<td></td>"+
					"<td>"+ elem.subject_name +"</td>"+
					"<td>"+ parseFloat(elem.tmarks).toFixed(2) +"</td></tr>";
			$(row).appendTo('#result-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;
			$(window).on('load', function(){
				fetchResults();
			});
			$(".btnPrint").on('click', function () {
              	var etype = 'resultSubjectWise_pdf';
              	// var from = $(settings.from_date).val();
              	// var to = $(settings.to_date).val();
              	
              	// from= from.replace('/','-');
              	// from= from.replace('/','-');
              	// to= to.replace('/','-');
              	// to= to.replace('/','-');
        	    var url = base_url + 'index.php/doc/pdf_resultSubjectWise/' + etype;
        	    // var url = base_url + 'index.php/doc/pdf_resultSubjectWise/' +etype +  '/' + from +  '/' + to ;
              	window.open(url);
              });

		}
	};
};


var resultSubjectWise = new ResultSubjectWise();
resultSubjectWise.init();