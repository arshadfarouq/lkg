var OverSemResult = function() {

	var settings = {

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),
		session_dropdown : $('#session_dropdown'),
		stdid_dropdown : $('#stdid_dropdown'),
		term_dropdown : $('#term_dropdown'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var fetchStdid = function(col, brid, claid, secid) {

		$.ajax({
			url : base_url + 'index.php/result/fetchColByBrClsNSec',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid, 'secid' : secid, 'col' : col },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateStdid(data);
				} else {
					alert('pakist');
					 // removeStdidOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateStdid = function(data) {

		var stdidOptions = "";
		$.each(data, function( index, elem ) {

			stdidOptions += "<option value='"+ elem.srno +"' >"+ elem.srno +"</option>";
		});

		$(stdidOptions).appendTo(settings.stdid_dropdown);
	}
	var removeStdidOptions = function() {

		$(stdid_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var fetchSession = function(col ,brid, claid, secid) {

		$.ajax({
			url : base_url + 'index.php/result/fetchColByBrClsNSec',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid, 'secid' : secid, 'col' : col },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSession(data);
				} else {
					removeSessionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateSession = function(data) {

		var sessionOptions = "";
		$.each(data, function( index, elem ) {

			sessionOptions += "<option value='"+ elem.session +"' >"+ elem.session +"</option>";
		});

		$(sessionOptions).appendTo(settings.session_dropdown);
	}
	var removeSessionOptions = function() {

		$(session_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeStdidOptions();
		removeSessionOptions();
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid, sec ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	var validateSearch = function() {

		var errorFlag = false;
		var class_dropdown = $(settings.class_dropdown).val();
		var section_dropdown = $(settings.section_dropdown).val();
		var session_dropdown = $(settings.session_dropdown).val();
		var stdid_dropdown = $(settings.stdid_dropdown).val();
		var term_dropdown = $(settings.term_dropdown).val();


		// remove the error class first
		$(settings.class_dropdown).removeClass('inputerror');
		$(settings.section_dropdown).removeClass('inputerror');
		$(settings.session_dropdown).removeClass('inputerror');
		$(settings.stdid_dropdown).removeClass('inputerror');
		$(settings.term_dropdown).removeClass('inputerror');

		if ( class_dropdown === '' || class_dropdown === null ) {
			$(settings.class_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( session_dropdown === '' || session_dropdown === null ) {
			$(settings.session_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( stdid_dropdown === '' || stdid_dropdown === null ) {
			$(settings.stdid_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( term_dropdown === '' || term_dropdown === null ) {
			$(settings.term_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(brid, claid, secid, session, stdid, term) {

		$.ajax({
			url : base_url + 'index.php/result/studentResultReport',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid, 'session' : session, 'stdid' : stdid, 'term' : term },
			dataType : 'JSON',
			success : function(data) {

				$('#result-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var total_obmarks = 0;
		var total_tmarks = 0;

		var counter = 1;
		$.each(data, function(index, elem) {

			total_obmarks = parseFloat(total_obmarks) + parseFloat(elem.obmarks);
			total_tmarks = parseFloat(total_tmarks) + parseFloat(elem.tmarks);

			row = 	"<tr class='txtcenter'>"+
					"<td class='txtcenter level4row'>"+ (counter++) +"</td>"+
					"<td class='txtcenter level4row'>"+ elem.subject_name +"</td>"+
					"<td class='txtcenter'>"+ parseFloat(elem.tmarks).toFixed(2) +"</td>"+
					"<td class='txtcenter'>"+ parseFloat(elem.obmarks).toFixed(2) +"</td>"+
					"<td class='txtcenter'>"+ parseFloat(elem.percentage).toFixed(2) +"</td>"+
					"<td class='txtcenter'>"+ elem.remarks +"</td></tr>";

			$(row).appendTo('#result-table tbody');
		});

		row = 	"<tr class='txtcenter level4row'>"+
				"<td class='txtcenter txtbold level4row'></td>"+
				"<td class='txtcenter txtbold level4row'>Total</td>"+
				"<td class='txtcenter txtbold level4row'>"+ parseFloat(total_tmarks).toFixed(2) +"</td>"+
				"<td class='txtcenter txtbold level4row'>"+ parseFloat(total_obmarks).toFixed(2) +"</td>"+
				"<td class='txtcenter txtbold level4row'>"+ ((parseFloat(total_obmarks) / parseFloat(total_tmarks)) * 100).toFixed(2) +"</td>"+
				"<td class='txtcenter txtbold level4row'></td></tr>";

		$(row).appendTo('#result-table tbody');
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(".btnPrint").on('click', function () {
              	var etype = 'studentResult';
	            var brid = $(settings.branch_dropdown).val();
	            var claid = $(settings.class_dropdown).val();
	            var secid = $(settings.section_dropdown).val();
	            var session = $(settings.session_dropdown).val();
	            var stdid = $(settings.stdid_dropdown).val();
	            var term = $(settings.term_dropdown).val();
	            var className = $('#class_dropdown option:selected').text();
	            var sectionName = $('#section_dropdown option:selected').text();
				// alert(className);	            
              	
        	    var url = base_url + 'index.php/doc/pdf_studentResult/' +etype +  '/' + brid +  '/' + claid +  '/' + secid +  '/' + session + '/' + stdid +  '/' + term +  '/' + className + '/' + sectionName;
              	window.open(url);
              });

			// when fetchSectionNames is changed
			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				// clear tables

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			$(settings.section_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(this).val();

				fetchStdid('stdid', brid, claid, secid);
				fetchSession('session' ,brid, claid, secid);
			});

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				var secid = $(settings.section_dropdown).val();
				var session = $(settings.session_dropdown).val();
				var stdid = $(settings.stdid_dropdown).val();
				var term = $(settings.term_dropdown).val();

				search(brid, claid, secid, session, stdid, term);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.branch_dropdown).val('');
			// $(settings.class_dropdown).val('');
			// $(settings.section_dropdown).val('');
			// $(settings.session_dropdown).val('');
			// $(settings.stdid_dropdown).val('');
			// $(settings.term_dropdown).val('');

			// removeOptions();
			// removeSectionOptions();
			// removeSessionOptions();
			// removeStdidOptions();

			// // removes all rows
			// $('#result-table').find('tbody tr').remove();
			// 
			general.reloadWindow();
		}

	};
};


var overSemResult = new OverSemResult();
overSemResult.init();