var ResultSubjectWise = function() {

	var fetchResults = function() {

		$.ajax({
			url : base_url + 'index.php/result/stuResultTeacherWiseReport',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {
				populateData(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		var branch_name = "";
		var teacher_name = "";
		var section_name = "";
		var subject_name = "";

		$.each(data, function(index, elem) {

			if (elem.branch_name !== branch_name) {

				var row = 	"<tr class='level1'>"+
							"<td colspan='7' class='level1row'>"+ elem.branch_name +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				branch_name = elem.branch_name;
			}

			if (elem.teacher_name !== teacher_name) {

				var row = 	"<tr class='level2'>"+
							"<td colspan='7' class='level2row'>"+ elem.teacher_name +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				teacher_name = elem.teacher_name;
			}

			if (elem.section_name !== section_name) {

				var row = 	"<tr class='level3'>"+
							"<td colspan='7' class='level3row'>"+ elem.section_name +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				section_name = elem.section_name;
			}

			if (elem.subject_name !== subject_name) {

				var row = 	"<tr class='level4'>"+
							"<td colspan='7' class='level4row'>"+ elem.subject_name +"</td>"+
							"</tr>";

				$(row).appendTo('#result-table tbody');
				subject_name = elem.subject_name;
			}

			row = 	"<tr>"+
					"<td>"+ elem.date +"</td>"+
					"<td>"+ elem.student_name +"</td>"+
					"<td>"+ elem.term +"</td>"+
					"<td>"+ parseFloat(elem.obmarks).toFixed(2) +"</td>"+
					"<td>"+ parseFloat(elem.tmarks).toFixed(2) +"</td>"+
					"<td>"+ parseFloat(elem.percentage).toFixed(2) +"</td>"+
					"<td>"+ elem.remarks +"</td></tr>";
			$(row).appendTo('#result-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(".btnPrint").on('click', function () {
	          	var etype = 'studentResultTeacherWise';

	    	    var url = base_url + 'index.php/doc/pdf_studentResultTeacherWise/' +etype ;
	          	window.open(url);
	          });
			$(window).on('load', function(){
				fetchResults();
			});

		}
	};
};


var resultSubjectWise = new ResultSubjectWise();
resultSubjectWise.init();