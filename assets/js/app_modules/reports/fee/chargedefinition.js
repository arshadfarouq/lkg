var ChargeDefinition = function() {

	var fetchBranch = function() {

		$.ajax({
			url : base_url + 'index.php/charge/chargesDefinitionReport',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {
				populateData(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		var source = $('#chargedef').html();
		var template = Handlebars.compile(source);
		$('#chargedef-table tbody').html(template(data));
		
		$('#chargedef-table').dataTable();
	}

	return {

		init : function () {
			this.bindUI();
			// alert('sdsd');
		},

		bindUI : function() {

			var self = this;
			$(window).on('load', function(){
				fetchBranch();
			});
			$(".btnPrint").on('click', function () {
              	var etype = 'chargedef';
              	


              	// var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype + '/' + $('#txtVrno').val() + '/' + $('.cid').val()  + '/' + print_bal ;
              	// var url = base_url + 'index.php/doc/pdf_SubjectViewReport/' +etype +  '/' + from +  '/' + to ;
              	var url = base_url + 'application/views/reportprints/chargeDefintionReport.php/' +etype ;
              	window.open(url);
  				// window.open(base_url + 'application/views/reportPrints/voucher.php', "Sale Report", "width=616, height=842");
              });

			// $(".btnPrint").on('click', function () {
			// 	// alert('sd');
   //            	var etype = 'chargedef';
              	
   //            	// var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype + '/' + $('#txtVrno').val() + '/' + $('.cid').val()  + '/' + print_bal ;
   //            	var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype ;
   //            	window.open(url);
  	// 			// window.open(base_url + 'application/views/reportPrints/voucher.php', "Sale Report", "width=616, height=842");
   //            });
			
		}
	};
};


var chargeDefinition = new ChargeDefinition();
chargeDefinition.init();