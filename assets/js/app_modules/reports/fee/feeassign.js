var MonthlyFeeAssign = function() {

	var settings = {

		from_date : $('#from_date'),
		to_date : $('#to_date'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var validateSearch = function() {

		var errorFlag = false;
		var from_date = $(settings.from_date).val();
		var to_date = $(settings.to_date).val();
		

		// remove the error class first
		$(settings.from_date).removeClass('inputerror');
		$(settings.to_date).removeClass('inputerror');

		if ( from_date === '' || from_date === null ) {
			$(settings.from_date).addClass('inputerror');
			errorFlag = true;
		}
		if ( to_date === '' || to_date === null ) {
			$(settings.to_date).addClass('inputerror');
			errorFlag = true;
		}	
	
		return errorFlag;
	}
	var search = function(from, to) {

		$.ajax({
			url : base_url + 'index.php/fee/feeAssignReport',
			type : 'POST',
			data : { 'from' : from, 'to' : to },
			dataType : 'JSON',
			success : function(data) {

				$('#fee-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var class_name = "";
		var fee_category = "";
		var counter = 1;

		$.each(data, function(index, elem) {

			if (elem.fee_category !== fee_category) {

				var row = 	"<tr>"+
							"<td colspan='6' style='background: #0769bf;color: #fff;'>"+ elem.fee_category +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				fee_category = elem.fee_category;
				class_name = "";	// reset class name to again print the class name
			}

			if (elem.class_name !== class_name) {

				var row = 	"<tr>"+
							"<td colspan='6' style='background: #3fa1f7;color: #fff;'>"+ elem.class_name +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				class_name = elem.class_name;
			}

			row = 	"<tr>"+
					"<td>"+ (counter++) +"</td>"+
					"<td>"+ elem.date +"</td>"+
					"<td>"+ elem.faid +"</td>"+
					"<td>"+ elem.description +"</td>"+
					"<td>"+ parseFloat(elem.amount).toFixed(2) +"</td></tr>";
			$(row).appendTo('#fee-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
			alert('sdada');
		},

		bindUI : function() {

			var self = this;

			$(".btnPrint").on('click', function () {
				// alert('sd');
              	var etype = 'FeeAssign';
              	var from = $(settings.from_date).val();
              	var to = $(settings.to_date).val();
              	// alert(from);
              	// alert(to);
              	from= from.replace('/','-');
              	from= from.replace('/','-');

              	to= to.replace('/','-');
              	to= to.replace('/','-');


              	// var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype + '/' + $('#txtVrno').val() + '/' + $('.cid').val()  + '/' + print_bal ;
              	var url = base_url + 'index.php/doc/pdf_FeeAssign/' +etype +  '/' + from +  '/' + to ;
              	window.open(url);
  				// window.open(base_url + 'application/views/reportPrints/voucher.php', "Sale Report", "width=616, height=842");
              });
			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var from = $(settings.from_date).val();
				var to = $(settings.to_date).val();

				search(from, to);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$(settings.from_date).datepicker('update', new Date());
			$(settings.to_date).datepicker('update', new Date());

			// removes all rows
			$('#fee-table').find('tbody tr').remove();
		}

	};
};


var monthlyFeeAssign = new MonthlyFeeAssign();
monthlyFeeAssign.init();