var MonthlyFeeAssign = function() {

	var settings = {

		from_date : $('#from_date'),
		to_date : $('#to_date'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var validateSearch = function() {

		var errorFlag = false;
		var from_date = $(settings.from_date).val();
		var to_date = $(settings.to_date).val();


		// remove the error class first
		$(settings.from_date).removeClass('inputerror');
		$(settings.to_date).removeClass('inputerror');

		if ( from_date === '' || from_date === null ) {
			$(settings.from_date).addClass('inputerror');
			errorFlag = true;
		}
		if ( to_date === '' || to_date === null ) {
			$(settings.to_date).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var search = function(from, to) {

		$.ajax({
			url : base_url + 'index.php/fee/feeConcessionReport',
			type : 'POST',
			data : { 'from' : from, 'to' : to },
			dataType : 'JSON',
			success : function(data) {

				$('#fee-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var branch_name = "";
		var class_name = "";
		var section_name = "";
		var name = "";
		var fname = "";
		var chk = false;

		var fee = 0;
		var concession = 0;
		var namount = 0;

		$.each(data, function(index, elem) {

			if (elem.branch_name !== branch_name) {

				var row = 	"<tr class='level1'>"+
							"<td colspan='4' style='background: #0769bf;color: #fff;'>"+ elem.branch_name +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				branch_name = elem.branch_name;
			}

			if (elem.class_name !== class_name) {

				var row = 	"<tr class='level2'>"+
							"<td colspan='4' style='background: #3fa1f7;color: #fff;'>"+ elem.class_name +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				class_name = elem.class_name;
			}

			if (elem.section_name !== section_name) {

				var row = 	"<tr class='level3'>"+
							"<td colspan='4' style='background: #8ec7fa;color: #fff;'>"+ elem.section_name +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				section_name = elem.section_name;
			}

			if (elem.name !== name || elem.fname !== fname) {

				var row = 	"<tr class='level4'>"+
							"<td style='background: #d2e9fd;color: #333;'>"+ elem.stdid +"</td>"+
							"<td colspan='2' style='background: #d2e9fd;color: #333;'>"+ elem.name +"</td>"+
							"<td style='background: #d2e9fd;color: #333;'>"+ elem.fname +"</td>"+
							"</tr>";
				$(row).appendTo('#fee-table tbody');
				name = elem.name;
				fname = elem.fname;
			}

			row = 	"<tr>"+
					"<td>"+ elem.description +"</td>"+
					"<td>"+ parseFloat(elem.amount).toFixed(2) +"</td>"+
					"<td>"+ parseFloat(elem.concession).toFixed(2) +"</td>"+
					"<td>"+ parseFloat(elem.namount).toFixed(2) +"</td></tr>";

			fee = parseFloat(fee) + parseFloat(elem.amount);
			concession = parseFloat(concession) + parseFloat(elem.concession);
			namount = parseFloat(namount) + parseFloat(elem.namount);
			$(row).appendTo('#fee-table tbody');

			if ( data.length == (index+1) || data[index+1]['name'] !== name || data[index+1]['fname'] !== fname) {
				
				showStudentTotal(fee, concession, namount);
				fee = 0;
				concession = 0;
				namount = 0;
			}
		});
	}

	var showStudentTotal = function(fee, concession, namount) {

		var row = 	"<tr class='level5'>"+
					"<td style='background: #e6f3fe;color: #333;'>Total</td>"+
					"<td style='background: #e6f3fe;color: #333;'>"+ fee +"</td>"+
					"<td style='background: #e6f3fe;color: #333;'>"+ concession +"</td>"+
					"<td style='background: #e6f3fe;color: #333;'>"+ namount +"</td>"+
					"</tr>";
		$(row).appendTo('#fee-table tbody');
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			$(".btnPrint").on('click', function () {
				// alert('sd');
              	var etype = 'FeeConcessionReport';
              	var from = $(settings.from_date).val();
              	var to = $(settings.to_date).val();
              	
              	from= from.replace('/','-');
              	from= from.replace('/','-');
              	to= to.replace('/','-');
              	to= to.replace('/','-');
              	// alert(to);
        	    var url = base_url + 'index.php/doc/pdf_FeeAssignReport/' +etype +  '/' + from +  '/' + to ;
              	window.open(url);
              });
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var from = $(settings.from_date).val();
				var to = $(settings.to_date).val();

				search(from, to);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$(settings.from_date).datepicker('update', new Date());
			$(settings.to_date).datepicker('update', new Date());

			// removes all rows
			$('#fee-table').find('tbody tr').remove();
		}

	};
};


var monthlyFeeAssign = new MonthlyFeeAssign();
monthlyFeeAssign.init();