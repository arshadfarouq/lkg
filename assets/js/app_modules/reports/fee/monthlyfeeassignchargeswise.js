var MonthlyFeeAssignChargesWise = function() {

	var settings = {

		from_date : $('#from_date'),
		to_date : $('#to_date'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var validateSearch = function() {

		var errorFlag = false;
		var from_date = $(settings.from_date).val();
		var to_date = $(settings.to_date).val();
		

		// remove the error class first
		$(settings.from_date).removeClass('inputerror');
		$(settings.to_date).removeClass('inputerror');

		if ( from_date === '' || from_date === null ) {
			$(settings.from_date).addClass('inputerror');
			errorFlag = true;
		}
		if ( to_date === '' || to_date === null ) {
			$(settings.to_date).addClass('inputerror');
			errorFlag = true;
		}	
	
		return errorFlag;
	}
	var search = function(from, to) {

		$.ajax({
			url : base_url + 'index.php/fee/monthlyFeeIssuanceReportChargesWise',
			type : 'POST',
			data : { 'from' : from, 'to' : to },
			dataType : 'JSON',
			success : function(data) {

				$('#fee-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var description = "";

		var class_name = "";
		var sumClass = 0;

		var chk = false;

		$.each(data, function(index, elem) {

			if (elem.description !== description) {

				if (chk) {
					showClassSum(sumClass, class_name);
					sumClass = 0;
				}				
				chk = true;

				var row = 	"<tr>"+
							"<td colspan='2' style='background: #0769bf;color: #fff;'>"+ elem.description +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				description = elem.description;
				class_name = "";

			}

			if (elem.class_name !== class_name) {

				var row = 	"<tr>"+
							"<td colspan='2' style='background: #3fa1f7;color: #fff;'>"+ elem.class_name +"</td>"+
							"</tr>";

				$(row).appendTo('#fee-table tbody');
				class_name = elem.class_name;
			}

			row = 	"<tr>"+
					"<td>"+ elem.section_name +"</td>"+
					"<td>"+ parseFloat(elem.amount).toFixed(2) +"</td></tr>";
			$(row).appendTo('#fee-table tbody');

			sumClass = parseFloat(sumClass) + parseFloat(elem.amount);
		});

		showClassSum(sumClass, class_name);
	}

	var showClassSum = function(class_total, class_name) {

		var row = 	"<tr>"+
					"<td style='background: #e6f3fe;;color: #000;padding-left: 20px;'>"+ class_name +"</td>"+
					"<td style='background: #e6f3fe;;color: #000;'> Total: "+ class_total +"</td>"+
					"</tr>";
		$(row).appendTo('#fee-table tbody');
	}

	return {

		init : function () {
			this.bindUI();
			// alert('hggsd');
		},

		bindUI : function() {

			var self = this;

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			$(".btnPrint").on('click', function () {
				// alert('sd');
              	var etype = 'monthlyFeeAssignReportChargesWise';
              	var from = $(settings.from_date).val();
              	var to = $(settings.to_date).val();
              	// alert(from);
              	// alert(to);
              	from= from.replace('/','-');
              	from= from.replace('/','-');
              	to= to.replace('/','-');
              	to= to.replace('/','-');
              	var url = base_url + 'index.php/doc/pdf_monthlyFeeAssignChargesWiseReport/' +etype +  '/' + from +  '/' + to ;
              	window.open(url);
              });
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var from = $(settings.from_date).val();
				var to = $(settings.to_date).val();

				search(from, to);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$(settings.from_date).datepicker('update', new Date());
			$(settings.to_date).datepicker('update', new Date());

			// removes all rows
			$('#fee-table').find('tbody tr').remove();
		}

	};
};


var monthlyFeeAssignChargesWise = new MonthlyFeeAssignChargesWise();
monthlyFeeAssignChargesWise.init();