var StaffStatus = function() {

	var settings = {

		branch_dropdown : $('.brid'),
		status_dropdown : $('#status_dropdown'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	var validateSearch = function() {

		var errorFlag = false;
		return errorFlag;
	}

	var search = function(brid, status) {

		$.ajax({
			url : base_url + 'index.php/staff/fetchStaffReportByStatus',
			type : 'POST',
			data : { 'brid' : brid, 'status' : status },
			dataType : 'JSON',
			success : function(data) {

				$('#staff-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var branch_name = "";
		var type = "";
		var counter = 1;
		$.each(data, function(index, elem) {

			if (elem.branch_name !== branch_name) {

				var row = 	"<tr>"+
							"<td colspan='8' style='background: #756565;color: #fff;'>"+ elem.branch_name +"</td>"+
							"</tr>";

				$(row).appendTo('#staff-table tbody');
				branch_name = elem.branch_name;
			}

			if (elem.type !== type) {

				var row = 	"<tr>"+
							"<td colspan='8' style='background: rgba(90, 96, 111, 0.5);color: #fff;'>"+ elem.type +"</td>"+
							"</tr>";

				$(row).appendTo('#staff-table tbody');
				type = elem.type;
			}

			row = 	"<tr>"+
					"<td>"+ (counter++) +"</td>"+
					"<td>"+ elem.staid +"</td>"+
					"<td>"+ elem.name +"</td>"+
					"<td>"+ elem.phone +"</td>"+
					"<td>"+ elem.mobile +"</td>"+
					"<td>"+ elem.address +"</td>"+
					"<td>"+ elem.quali +"</td>"+
					"<td>"+ elem.experience +"</td></tr>";
			$(row).appendTo('#staff-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			// when selection is changed in fromBranch_dropdown
			$(settings.branch_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();

				// clear tables

				if ( brid !== "" || brid !== null) {
					fetchClassNames(brid);
				}
			});

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var brid = ($(settings.branch_dropdown).val() === null) ? '' : $(settings.branch_dropdown).val();
				var status = $(settings.status_dropdown).val();

				search(brid, status);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$(settings.branch_dropdown).val('');
			$(settings.status_dropdown).val('1');

			// removes all rows
			$('#staff-table').find('tbody tr').remove();
		}

	};
};


var staffStatus = new StaffStatus();
staffStatus.init();