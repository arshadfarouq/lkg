var StudentStatus = function() {

	var settings = {

		branch_dropdown : $('.brid'),
		class_dropdown : $('#class_dropdown'),
		section_dropdown : $('#section_dropdown'),
		status_dropdown : $('#status_dropdown'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnReset : $('.btnReset'),
	};

	// fetch all the sections based on the selected class name
	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeSectionOptions = function() {

		var dropdown = settings.section_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		$(sectionNamesOptions).appendTo(settings.section_dropdown);
	}
	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid, sec ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	// removes the options from the select tag
	var removeOptions = function() {

		var dropdown = class_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}
	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		console.log(data);
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		$(classNamesOptions).appendTo(class_dropdown);
	}
	var validateSearch = function() {

		var errorFlag = false;
		var branch_dropdown = $(settings.branch_dropdown).val();
		var status_dropdown = $(settings.status_dropdown).val();
		

		// remove the error class first
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.status_dropdown).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}
		if ( status_dropdown === '' || status_dropdown === null ) {
			$(settings.status_dropdown).addClass('inputerror');
			errorFlag = true;
		}	
	
		return errorFlag;
	}

	var search = function(brid, claid, secid, status) {

		$.ajax({
			url : base_url + 'index.php/student/fetchStuReportByStatus',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid, 'status' : status },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No record found.');
				} else {
					$('#students-table').find('tbody tr').remove();
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		var class_name = "";
		var section_name = "";
		var counter = 1;
		$.each(data, function(index, elem) {

			if (elem.class_name !== class_name) {

				var row = 	"<tr>"+
							"<td colspan='6' style='background: #756565;color: #fff;'>"+ elem.class_name +"</td>"+
							"</tr>";

				$(row).appendTo('#students-table tbody');
				class_name = elem.class_name;
			}

			if (elem.section_name !== section_name) {

				var row = 	"<tr>"+
							"<td colspan='6' style='background: rgba(90, 96, 111, 0.5);color: #fff;'>"+ elem.section_name +"</td>"+
							"</tr>";

				$(row).appendTo('#students-table tbody');
				section_name = elem.section_name;
			}

			row = 	"<tr>"+
					"<td>"+ (counter++) +"</td>"+
					"<td>"+ elem.stdid +"</td>"+
					"<td>"+ elem.name +"</td>"+
					"<td>"+ elem.fname +"</td>"+
					"<td>"+ elem.category +"</td>"+
					"<td>"+ elem.feetype +"</td></tr>";
			$(row).appendTo('#students-table tbody');
		});

		//$('#students-table').dataTable({"bSort": false, 'iDisplayLength': 100});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(".btnPrint").on('click', function () {
				// alert('sd');
              	var etype = 'studentStatusReport';
            	var brid = ($(settings.branch_dropdown).val() === null) ? '' : $(settings.branch_dropdown).val();
            	var claid = ($(settings.class_dropdown).val() === null) ? '' : $(settings.class_dropdown).val();
            	var secid = ($(settings.section_dropdown).val() === null) ? '' : $(settings.section_dropdown).val();
            	var status = $(settings.status_dropdown).val();

            	var className = ($('#class_dropdown').val() === null) ? '' : $('#status_dropdown option:selected').text();
            	var sectionName = ($('#section_dropdown').val() === null) ? '' : $('#section_dropdown option:selected').text();
            	var statusName = ($('#status_dropdown').val() === null) ? '' : $('#status_dropdown option:selected').text();



              	// var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype + '/' + $('#txtVrno').val() + '/' + $('.cid').val()  + '/' + print_bal ;
              	var url = base_url + 'index.php/doc/pdf_studentStatusReport/' +etype +  '/' + brid +  '/' + claid +  '/' + secid +  '/' + status  +  '/' + className +  '/' + sectionName +  '/' + statusName ;
              	window.open(url);
  				// window.open(base_url + 'application/views/reportPrints/voucher.php', "Sale Report", "width=616, height=842");
              });

			// when fetchSectionNames is changed
			$(settings.class_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.class_dropdown).val();
				// clear tables

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var brid = ($(settings.branch_dropdown).val() === null) ? '' : $(settings.branch_dropdown).val();
				var claid = ($(settings.class_dropdown).val() === null) ? '' : $(settings.class_dropdown).val();
				var secid = ($(settings.section_dropdown).val() === null) ? '' : $(settings.section_dropdown).val();
				var status = $(settings.status_dropdown).val();

				search(brid, claid, secid, status);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.branch_dropdown).val('');
			// $(settings.class_dropdown).val('');
			// $(settings.section_dropdown).val('');
			// $(settings.status_dropdown).val('');

			// removeOptions();
			// removeSectionOptions();

			// // removes all rows
			// $('#students-table').find('tbody tr').remove();
			// 
			general.reloadWindow();
		}

	};
};


var studentStatus = new StudentStatus();
studentStatus.init();