var SubjectView = function() {

	var fetchSubjects = function() {

		$.ajax({
			url : base_url + 'index.php/subject/subjectViewReport',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {
				populateData(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		var branch_name = "";
		var class_name = "";
		var counter = 1;

		$.each(data, function(index, elem) {

			if (elem.branch_name !== branch_name) {

				var row = 	"<tr class='level1'>"+
							"<td colspan='3' style='background: #0769bf;color: #fff;'>"+ elem.branch_name +"</td>"+
							"</tr>";

				$(row).appendTo('#subjectview-table tbody');
				branch_name = elem.branch_name;
			}

			if (elem.class_name !== class_name) {

				var row = 	"<tr class='level2'>"+
							"<td colspan='3' style='background: #3fa1f7;color: #fff;'>"+ elem.class_name +"</td>"+
							"</tr>";

				$(row).appendTo('#subjectview-table tbody');
				class_name = elem.class_name;
			}

			row = 	"<tr>"+
					"<td>"+ (counter++) +"</td>"+
					"<td>"+ elem.subject_name +"</td>"+
					"<td>"+ elem.dcno +"</td></tr>";
			$(row).appendTo('#subjectview-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
			// alert('ddds');
		},

		bindUI : function() {

			var self = this;
			$(".btnPrint").on('click', function () {
              	var etype = 'SubjectViewReport';
              	


              	// var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype + '/' + $('#txtVrno').val() + '/' + $('.cid').val()  + '/' + print_bal ;
              	// var url = base_url + 'index.php/doc/pdf_SubjectViewReport/' +etype +  '/' + from +  '/' + to ;
              	var url = base_url + 'index.php/doc/pdf_SubjectViewReport/' +etype ;
              	window.open(url);
  				// window.open(base_url + 'application/views/reportPrints/voucher.php', "Sale Report", "width=616, height=842");
              });

			$(window).on('load', function(){
				fetchSubjects();
			});

		}
	};
};


var subjectView = new SubjectView();
subjectView.init();