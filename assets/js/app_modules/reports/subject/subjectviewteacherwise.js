var SubjectView = function() {

	var fetchSubjects = function() {

		$.ajax({
			url : base_url + 'index.php/subject/subjectViewTeacherWiseReport',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {
				populateData(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		var branch_name = "";
		var teacher_name = "";
		var counter = 1;

		$.each(data, function(index, elem) {

			if (elem.branch_name !== branch_name) {

				var row = 	"<tr class='level1'>"+
							"<td colspan='4' style='background: #0769bf;color: #fff;'>"+ elem.branch_name +"</td>"+
							"</tr>";

				$(row).appendTo('#subjectview-table tbody');
				branch_name = elem.branch_name;
			}

			if (elem.teacher_name !== teacher_name) {

				var row = 	"<tr class='level2'>"+
							"<td colspan='4' style='background: #3fa1f7;color: #fff;'>"+ elem.teacher_name +"</td>"+
							"</tr>";

				$(row).appendTo('#subjectview-table tbody');
				teacher_name = elem.teacher_name;
			}

			row = 	"<tr>"+
					"<td>"+ (counter++) +"</td>"+
					"<td>"+ elem.subject_name +"</td>"+
					"<td>"+ elem.class_name +"</td>"+
					"<td>"+ elem.section_name +"</td></tr>";
			$(row).appendTo('#subjectview-table tbody');
		});
	}

	return {

		init : function () {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;
			$(".btnPrint").on('click', function () {
              	var etype = 'SubjectAssignedDetailTeacherWise';
              	
              	// var url = base_url + 'index.php/doc/pdf_singlevoucher/' + etype + '/' + $('#txtVrno').val() + '/' + $('.cid').val()  + '/' + print_bal ;
              	// var url = base_url + 'index.php/doc/pdf_SubjectViewReport/' +etype +  '/' + from +  '/' + to ;
              	var url = base_url + 'index.php/doc/pdf_SubjectAssignedDetailTeacherWise/' +etype ;
              	window.open(url);
  				// window.open(base_url + 'application/views/reportPrints/voucher.php', "Sale Report", "width=616, height=842");
              });

			$(window).on('load', function(){
				fetchSubjects();
			});

		}
	};
};


var subjectView = new SubjectView();
subjectView.init();