var Section = function() {

	var settings = {

		txtSectionId : $('#txtSectionId'),
		txtSectionIdHidden : $('#txtSectionIdHidden'),
		txtMaxSectionIdHidden : $('#txtMaxSectionIdHidden'),

		txtSectionNameHidden : $('#txtSectionNameHidden'),
		txtSectionName : $('#txtSectionName'),

		txtRoomNo : $('#txtRoomNo'),

		txtBranchName : $('.brid'),
		txtClassName : $('#txtClassName'),
		allSections : $('#allSections'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnEditSection : $('.btn-edit-section')		
	};

	// fetch all the sections based on the selected class name
	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		$(allSections).children('option').remove();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {

					// if nothing is found inside database then clear the previous data
					$(allSections).children('option').remove();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});
		$(sectionNamesOptions).appendTo(allSections);
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(txtClassName).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});
		$(classNamesOptions).appendTo(txtClassName);
	}

	// saves the data into the database
	var save = function( sectionObj ) {

		$.ajax({
			url : base_url + 'index.php/group/saveSection',
			type : 'POST',
			data : { 'sectionDetail' : sectionObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Section saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		$(settings.txtSectionId).val('');
		$(settings.txtSectionIdHidden).val('');
		$(settings.txtMaxSectionIdHidden).val('');
		$(settings.txtSectionName).val('');
		$(settings.txtSectionNameHidden).val('');
		$(txtClassName).val('');
		$(txtRoomNo).val('');		

		setMaxId();
		general.hideLoader();
	}

	var fetch = function(secid) {

		var brid = $('.brid').val();		
		$.ajax({
			url : base_url + 'index.php/group/fetchSection',
			type : 'POST',
			data : { 'secid' : secid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(data) {

		$.each(data, function(index, elem){

			$(settings.txtSectionId).val(elem.secid);
			$(settings.txtSectionIdHidden).val(elem.secid);

			$(settings.txtSectionName).val(elem.name);
			$(settings.txtSectionNameHidden).val(elem.name);

			$(settings.txtBranchName).val(elem.brid);

			fetchClassNames(elem.brid);
			$(settings.txtClassName).val(elem.claid);

			fetchSectionNames(elem.brid, elem.claid);

			$(settings.txtRoomNo).val(elem.room);
		});
	}

	// gets the maxid of the voucher
	var setMaxId = function() {

		var brid = $('.brid').val();
		$.ajax({
			url : base_url + 'index.php/group/getMaxId',
			type : 'POST',
			data : { 'table' : 'section', 'brid': brid},
			dataType : 'JSON',
			success : function(data) {

				$(txtSectionId).val(data);
				$(txtSectionIdHidden).val(data);
				$(txtMaxSectionIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var sectionName = $.trim($(settings.txtSectionName).val());
		var claid = $.trim($(settings.txtClassName).val());

		// remove the error class first
		$(settings.txtSectionName).removeClass('inputerror');
		$(settings.txtClassName).removeClass('inputerror');

		if ( sectionName === '' ) {
			$(settings.txtSectionName).addClass('inputerror');
			errorFlag = true;
		}

		if ( claid === '' ) {
			$(settings.txtClassName).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var isFieldValid = function() {

		var errorFlag = false;
		var name = settings.txtSectionName;		// get the current fee category name entered by the user
		var secid = settings.txtSectionIdHidden;		// hidden secid
		var maxId = settings.txtMaxSectionIdHidden;		// hidden max secid
		var txtnameHidden = settings.txtSectionNameHidden;		// hidden fee category name

		var sectionNames = new Array();
		// get all branch names from the hidden list
		$("#allSections option").each(function(){
			sectionNames.push($(this).text().trim().toLowerCase());
		});

		// if both values are not equal then we are in update mode
		if (secid.val() !== maxId.val()) {

			$.each(sectionNames, function(index, elem){

				if (txtnameHidden.val().toLowerCase() !== elem.toLowerCase() && name.val().toLowerCase() === elem.toLowerCase()) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});

		} else {	// if both are equal then we are in save mode

			$.each(sectionNames, function(index, elem){

				if (name.val().trim().toLowerCase() === elem) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});
		}

		return errorFlag;
	}

	// returns the fee category object to save into database
	var getSaveSectionObject = function() {

		var obj = {

			secid : $.trim($(settings.txtSectionIdHidden).val()),
			name : $.trim($(settings.txtSectionName).val()),
			brid : $.trim($(settings.txtBranchName).val()),
			claid : $.trim($(settings.txtClassName).val()),
			room : $.trim($(settings.txtRoomNo).val())
		};
		return obj;
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			// when save button is clicked
			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});

			// when the reset button is clicked
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			// when selection is changed inside the class combobox
			$(settings.txtClassName).on('change', function(e) {

				// check if no branch is selected
				if ( $(settings.txtClassName).val() !== '' ) {

					var brid = $(settings.txtBranchName).val();
					var claid = $(settings.txtClassName).val();
					fetchSectionNames( brid, claid );	// fetch all the sections based on the selected class name and branch name
				}
			});

			// when text is chenged inside the id textbox
			$(settings.txtSectionId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtSectionId).val().trim() !== "" ) {

						var secid = $.trim($(settings.txtSectionId).val());
						fetch(secid);
					}
				}
			});

			// when edit button is clicked inside the table view
			$(settings.btnEditSection).on('click', function(e) {
				e.preventDefault();				
				fetch($(this).data('secid'));		// get the class detail by id
			});

			setMaxId();		// gets the max id of voucher
			fetchClassNames( $(settings.txtBranchName).val());	// fetch all the classes based on the selected branch name
		},

		// makes the voucher ready to save
		initSave : function() {

			var sectionObj = getSaveSectionObject();	// returns the class detail object to save into database
			var isValid = validateSave();			// checks for the empty fields

			if ( !isValid ) {

				// check if the section name is already used??	if false
				if ( !isFieldValid() ) {
					// save the data in to the database
					save( sectionObj );
				} else {	// if fee section name is already used then show error
					alert("Section name already used.");
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$(settings.txtSectionId).val('');
			$(settings.txtSectionName).val('');
			$(settings.txtSectionNameHidden).val('');
			$(txtBranchName).val('');
			$(txtClassName).val('');
			$(txtRoomNo).val('');

			setMaxId();		// gets the max id of voucher
			general.setPrivillages();*/

			general.reloadWindow();
		}
	};
};

var section = new Section();
section.init();