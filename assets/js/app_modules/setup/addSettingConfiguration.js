var settingConfiguration = function(){
	var settings = {
		switchledgerdes : $('#switchledgerdes'),
		switchPreview : $('#switchPreview'),
		switchPreBal : $("#switchPreBal"),
		switchHeader : $("#switchHeader"),
		switchStatus : $("#switchStatus"),
		switchImage : $("#switchImage"),
		switchFeilds : $("#switchFeilds"),
		switchPrices : $("#switchPrices"),
		switchOption : $("#switchOption"),
		switchTax : $("#switchTax")
	};
	var validateSave = function() {

		var flag 			= false;
		var sale	    	= $('#sale');
		var purchase 		= $('#purchase');
		var salereturn 		= $('#saleReturn');
		var purchaseReturn 	= $('#purchaseReturn');
		var discount 		= $('#discount');
		var expenses 		= $('#expenses');
		var tax 			= $('#tax');
		var cash 			= $('#cash');
		var freight 		= $('#freight');
		var gst             = $('#gst');
		var st_challan      = $('#st_challan');
		var inc_tax         = $('#inc_tax');
		var commission      = $('#commission');
		var itemdiscount 	= $('#itemdiscount');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		// if ( !sale.val() ) {
		// 	sale.addClass('inputerror');
		// 	flag = true;
		// }

		// if ( !purchase.val() ) {
		// 	purchase.addClass('inputerror');
		// 	flag = true;
		// }

		// if ( !salereturn.val() ) {
		// 	salereturn.addClass('inputerror');
		// 	flag = true;
		// }
		// if ( !purchaseReturn.val() ) {
		// 	purchaseReturn.addClass('inputerror');
		// 	flag = true;
		// }
		// if ( !itemdiscount.val() ) {
		// 	itemdiscount.addClass('inputerror');
		// 	flag = true;
		// }

		
		return flag;
	}


	var getSaveObject = function() {
      // alert('pakistan');
		var obj 	= {};
		obj.supplier 	= $('#supplier').val();
		//alert(obj.supplier);
 		obj.customers	= $('#customers').val();
		obj.sale      		= $('#sale').val();
		//alert(obj.sale);
		obj.purchase		= $('#purchase').val();
		obj.salereturn		= $('#saleReturn').val();
		obj.purchasereturn 	= $('#purchaseReturn').val();
		//obj.discount		= $('#discount').val();
		obj.expenses 		= $('#expenses').val();
		obj.tax 			= $('#tax').val();
		obj.cash 			= $('#cash').val();
		obj.freight 		= $('#freight').val();
		//obj.gst             = $('#gst').val();
		//obj.st_challan      = $('#st_challan').val();
		//obj.inc_tax         = $('#inc_tax').val();
		obj.commission      = $('#commission').val();
 		obj.itemdiscount 	= $('#itemsdiscount').val();
 		///general
 		
 		//obj.cashinhand	= $('#cashinhand').val();
 		//obj.itemsdiscount	= $('#itemsdiscount').val();
 		obj.invoicediscount	= $('#invoicediscount').val();
 		//obj.txtfreight 	= $('#txtfreight').val();
 		obj.creditcard 	= $('#creditcard').val();
 		obj.bankcharges 	= $('#bankcharges').val();
 		
 		obj.switchledgerdes = ($(settings.switchledgerdes).bootstrapSwitch('state') === true) ? '1' : '0';
 		//Balance sheet
 		obj.assets	= $('#assets').val();
 		obj.liabilities	= $('#Liabilities').val();
 		obj.inocome	= $('#inocome').val();
 		obj.txtexpenses 	= $('#txtexpenses').val();
 		obj.saleincome 	= $('#saleincome').val();
 		obj.switchprices =  ($(settings.switchPrices).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.costOfgoodssold 	= $('#costOfgOodssOld').val();
 		obj.switchfeilds =  ($(settings.switchFeilds).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.operatingexpenses	= $('#OperatingExpenses').val();
 		obj.switchimage  =  ($(settings.switchImage).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.otherexpenses 	= $('#OtherExpenses').val();
 		obj.switchstockstatus =  ($(settings.switchStatus).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.otherincome 	= $('#Otherincome').val();
 		obj.financecost 	= $('#financecost').val();
 		obj.switchHeader =  ($(settings.switchHeader).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.workProfit 	= $('#WorkProfit').val();
 		obj.switchPreBal =  ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.provision 	= $('#Provision').val();
 		//
 		obj.defaultprint 	= $('#defaultprint').val();
 		obj.switchpreview = ($(settings.switchPreview).bootstrapSwitch('state') === true) ? '1' : '0';
 	
 		//Tax Setting
 		obj.txtsaletax 	= $('#txtSaleTax').val();
 		obj.txtfurthertax 	= $('#txtFurtherTax').val();
 		obj.txtextratax 	= $('#txtExtraTax').val();
 		obj.txtholdingtax 	= $('#txtHoldingTax').val();
 		obj.switchOption = ($(settings.switchOption).bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.sale_tax 	= $('#sale_tax').val();
 		obj.switchTax = ($(settings.switchTax).bootstrapSwitch('state') === true) ? '1' : '0';

 		obj.further_tax 	= $('#Further_tax').val();
        obj.extra_tax 	= $('#Extra_tax').val();
        obj.holding_tax 	= $('#Holding_tax').val();

 		obj.voucherdiscount = ($('#switchDiscount').bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.vouchertax = ($('#switchTaxVoucher').bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.voucherpaid = ($('#switchPaid').bootstrapSwitch('state') === true) ? '1' : '0';
 		obj.voucherexpense = ($('#switchExpense').bootstrapSwitch('state') === true) ? '1' : '0';


        obj.feereceive 	= $('#feereceive').val();
        obj.latefeefine 	= $('#latefeefine').val();
        obj.arears 	= $('#arears').val();

       		//console.log(obj);
		 return obj;
	}

	  /////////////////////////////////////////////////////
	 ///////////   Saving Data     ///////////////////////
	/////////////////////////////////////////////////////

	var save = function(obj) {
		//alert('save');
		$.ajax({
			url 	: base_url + "index.php/setting_configuration/save",
			type 	: 'POST',
			data 	: { 'obj' : obj },
			dataType: 'JSON',
			success : function(data) {
			
					if (data.error === 'true') {
						general.ShowAlertNew('Attention Please!','An internal error occured while saving voucher.....');
					} else {
						alert('Voucher saved successfully.');
						 general.reloadWindow();
					}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}


	  /////////////////////////////////////////////////
	 /////////    Fetching Records   /////////////////
	/////////////////////////////////////////////////

	var fetch = function() {

		$.ajax({

			url  : base_url + 'index.php/setting_configuration/fetch',
			type : 'POST',
			data : {  },
			dataType : 'JSON',
			success : function(data) {

			    //resetfields();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data){
        
        $.each(data, function(index, elem){

		$('#txtId').select2('val',data[0]['id']);
		$('#sale').select2('val',data[0]['sale']);
		$('#purchase').select2('val',data[0]['purchase']);
		$('#saleReturn').select2('val',data[0]['salereturn']);
		$('#purchaseReturn').select2('val',data[0]['purchasereturn']);
		//$('#discount').select2('val',data[0]['discount']);
		$('#expenses').select2('val',data[0]['expenses']);
		$('#tax').select2('val',data[0]['tax']);
		$('#cash').select2('val',data[0]['cash']);
		$('#freight').select2('val',data[0]['freight']);
		//$('#gst').select2('val',data[0]['gst']);
		//$('#st_challan').select2('val',data[0]['st_challan']);
		//$('#inc_tax').select2('val',data[0]['inc_tax']);
		$('#commission').select2('val',data[0]['commission']);
		$('#itemsdiscount').select2('val',data[0]['itemdiscount']);
		$('#supplier').select2('val',data[0]['supplier']);
		$('#customers').select2('val',data[0]['customers']);
		//$('#cashinhand').select2('val',data[0]['cashinhand']);
		//$('#itemsdiscount').select2('val',data[0]['itemsdiscount']);
		$('#invoicediscount').select2('val',data[0]['invoicediscount']);
		//$('#txtfreight').select2('val',data[0]['txtfreight']);
		$('#creditcard').select2('val',data[0]['creditcard']);
		$('#bankcharges').select2('val',data[0]['bankcharges']);

		(data[0]['switchledgerdes'] === "1") ? $(settings.switchledgerdes).bootstrapSwitch('state', true) : $(settings.switchledgerdes).bootstrapSwitch('state', false);
		$('#assets').select2('val',data[0]['assets']);
		$('#Liabilities').select2('val',data[0]['liabilities']);
		$('#inocome').select2('val',data[0]['inocome']);
		$('#txtexpenses').select2('val',data[0]['txtexpenses']);
		$('#saleincome').select2('val',data[0]['saleincome']);
	    (data[0]['switchprices'] === "1") ? $(settings.switchPrices).bootstrapSwitch('state', true) : $(settings.switchPrices).bootstrapSwitch('state', false);
		$('#costOfgOodssOld').select2('val',data[0]['costofgoodssold']);
		(data[0]['switchfeilds'] === "1") ? $(settings.switchFeilds).bootstrapSwitch('state', true) : $(settings.switchFeilds).bootstrapSwitch('state', false);
		$('#OperatingExpenses').select2('val',data[0]['operatingexpenses']);
		 (data[0]['switchimage'] === "1") ? $(settings.switchImage).bootstrapSwitch('state', true) : $(settings.switchImage).bootstrapSwitch('state', false);
		$('#OtherExpenses').select2('val',data[0]['otherexpenses']);
		(data[0]['switchstockstatus'] === "1") ? $(settings.switchStatus).bootstrapSwitch('state', true) : $(settings.switchStatus).bootstrapSwitch('state', false);
		$('#financecost').select2('val',data[0]['financecost']);
        (data[0]['switchheader'] === "1") ? $(settings.switchHeader).bootstrapSwitch('state', true) : $(settings.switchHeader).bootstrapSwitch('state', false);
		$('#Otherincome').select2('val',data[0]['otherincome']);
		$('#WorkProfit').select2('val',data[0]['workprofit']);
		 (data[0]['switchprebal'] === "1") ? $(settings.switchPreBal).bootstrapSwitch('state', true) : $(settings.switchPreBal).bootstrapSwitch('state', false);
		$('#Provision').select2('val',data[0]['provision']);
		$('#defaultprint').select2('val',data[0]['defaultprint']);
		 (data[0]['switchpreview'] === "1") ? $(settings.switchPreview).bootstrapSwitch('state', true) : $(settings.switchPreview).bootstrapSwitch('state', false);
		$('#txtSaleTax').val(data[0]['txtsaletax']);
		$('#txtFurtherTax').val(data[0]['txtfurthertax']);
		
		$('#txtExtraTax').val(data[0]['txtextratax']);
		(data[0]['switchoption'] === "1") ? $(settings.switchOption).bootstrapSwitch('state', true) : $(settings.switchOption).bootstrapSwitch('state', false);
        (data[0]['switchtax'] === "1") ? $(settings.switchTax).bootstrapSwitch('state', true) : $(settings.switchTax).bootstrapSwitch('state', false);
        
		$('#txtHoldingTax').val(data[0]['txtholdingtax']);
		$('#sale_tax').select2('val',data[0]['sale_tax']);
		 
		$('#Further_tax').select2('val',data[0]['further_tax']);
		$('#Extra_tax').select2('val',data[0]['extra_tax']);
		$('#Holding_tax').select2('val',data[0]['holding_tax']);

        (data[0]['voucherdiscount'] === "1") ? $('#switchDiscount').bootstrapSwitch('state', true) : $('#switchDiscount').bootstrapSwitch('state', false);
        (data[0]['voucherexpense'] === "1") ? $('#switchExpense').bootstrapSwitch('state', true) : $('#switchExpense').bootstrapSwitch('state', false);
        (data[0]['vouchertax'] === "1") ? $('#switchTaxVoucher').bootstrapSwitch('state', true) : $('#switchTaxVoucher').bootstrapSwitch('state', false);
        (data[0]['voucherpaid'] === "1") ? $('#switchPaid').bootstrapSwitch('state', true) : $('#switchPaid').bootstrapSwitch('state', false);

		$('#arears').select2('val',data[0]['arears']);
		$('#latefeefine').select2('val',data[0]['latefeefine']);
		$('#feereceive').select2('val',data[0]['feereceive']);
		



		});

	}
	




	return{
		init : function(){
			this.bindUI();
		},
		bindUI : function(){
			var self = this;
			// $('#sale,#purchase,#saleReturn,#purchaseReturn,#discount,#expenses,#tax,#cash,#freight,#itemdiscount,#gst,#st_challan,#inc_tax,#commission,#supplier,#customers,#cashinhand,#itemsdiscount,#invoicediscount,#txtfreight,#creditcard,#bankcharges,#assets,#Liabilities,#inocome,#txtexpenses,#saleincome,#costOfgOodssOld,#Provision,#OperatingExpenses,#OtherExpenses,#Otherincome,#financecost,#WorkProfit,#Otherincome,#defaultprint,#sale_tax,#Further_tax,#Extra_tax,#Holding_tax').select2();
			
			$('.btnSave').on('click', function(e) {
				e.preventDefault();

				self.initSave();
			});
            $("#switchImage").bootstrapSwitch('offText', 'No');
			$("#switchImage").bootstrapSwitch('onText', 'Yes');
        
            $("#switchStatus").bootstrapSwitch('offText', 'No');
			$("#switchStatus").bootstrapSwitch('onText', 'Yes');

            $("#switchHeader").bootstrapSwitch('offText', 'No');
			$("#switchHeader").bootstrapSwitch('onText', 'Yes');

			$("#switchPreBal").bootstrapSwitch('offText', 'No');
			$("#switchPreBal").bootstrapSwitch('onText', 'Yes');

			$("#switchledgerdes").bootstrapSwitch('offText', 'No');
			$("#switchledgerdes").bootstrapSwitch('onText', 'Yes');
			
			$("#switchPreview").bootstrapSwitch('offText', 'No');
			$("#switchPreview").bootstrapSwitch('onText', 'Yes');


			$("#switchFeilds").bootstrapSwitch('offText', 'No');
			$("#switchFeilds").bootstrapSwitch('onText', 'Yes');

			$("#switchPrices").bootstrapSwitch('offText', 'No');
			$("#switchPrices").bootstrapSwitch('onText', 'Yes');

			$("#switchOption").bootstrapSwitch('offText', 'No');
			$("#switchOption").bootstrapSwitch('onText', 'Yes');

			$("#switchPaid").bootstrapSwitch('offText', 'No');
			$("#switchPaid").bootstrapSwitch('onText', 'Yes');

			$("#switchDiscount").bootstrapSwitch('offText', 'No');
			$("#switchDiscount").bootstrapSwitch('onText', 'Yes');

			$("#switchExpense").bootstrapSwitch('offText', 'No');
			$("#switchExpense").bootstrapSwitch('onText', 'Yes');

			$("#switchTaxVoucher").bootstrapSwitch('offText', 'No');
			$("#switchTaxVoucher").bootstrapSwitch('onText', 'Yes');

			$("#switchTax").bootstrapSwitch('offText', 'No');
			$("#switchTax").bootstrapSwitch('onText', 'Yes');


			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			shortcut.add('F10',function(e){
				e.preventDefault();
				self.initSave();
			});
			shortcut.add("ctrl+s", function(e) {
				e.preventDefault();
				self.initSave();
			});
			shortcut.add('F5',function(e){
				preventDefault();
				self.resetVoucher();
			});

			fetch();
		},
		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();	// returns the object to save into database
			var error   = validateSave();	// checks for the empty fields
			//alert('out' + error);
			if ( !error ) {
				//alert('inter');
				//alert(saveObj);
				save(saveObj);
			}else{
				alert('Enter Into Empty Fields..........');
			} 
		},
		// resets the voucher
		resetVoucher : function() {
			
			general.reloadWindow();
		},

	}

};
var setting = new settingConfiguration();
setting.init();