var Staff = function() {

	var settings = {

		totalMarks : 0,

		// first tab
		txtStaffId : $('#txtStaffId'),
		txtMaxStaffIdHidden : $('#txtMaxStaffIdHidden'),
		txtStaffIdHidden : $('#txtStaffIdHidden'),
		txtPIdHidden : $('#txtPIdHidden'),

		staffImage : $('#staffImage'),
		branch_dropdown : $('.brid'),
		type_dropdown : $('#type_dropdown'),
		agreement_dropdown : $('#agreement_dropdown'),
		religion_dropdown : $('#religion_dropdown'),
		bank_dropdown : $('#bank_dropdown'),
		active_switch : $('.active_switch'),
		current_date : $('#current_date'),
		txtName : $('#txtName'),
		txtFatherName : $('#txtFatherName'),
		gender_dropdown : $('#gender_dropdown'),
		marital_dropdown : $('#marital_dropdown'),
		txtcnic : $('#txtcnic'),
		birth_date : $('#birth_date'),
		joining_date : $('#joining_date'),
		txtAddress : $('#txtAddress'),
		txtPhoneNo : $('#txtPhoneNo'),
		txtMobileNo : $('#txtMobileNo'),
		txtAccountNo : $('#txtAccountNo'),
		txtSalary : $('#txtSalary'),


		// qualification tab
		txtQuali : $('#txtQuali'),
		txtDivision : $('#txtDivision'),
		txtYear : $('#txtYear'),
		txtInstitute : $('#txtInstitute'),
		txtMSubjects : $('#txtMSubjects'),
		btnAddQuali : $('#btnAddQuali'),
		qualification_table : $('#qualification-table'),

		// experience tab
		txtJobHeld : $('#txtJobHeld'),
		from_date : $('#from_date'),
		to_date : $('#to_date'),
		txtPayDraws : $('#txtPayDraws'),
		btnAddExp : $('#btnAddExp'),
		experience_table : $('#experience-table'),


		// buttons
		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnPrint : $('.btnPrint'),
		btnCardPrint : $('.btnCardPrint')
	};

	// saves the data into the database
	var save = function( staffObj ) {

		$.ajax({
			url : base_url + 'index.php/staff/save',
			type : 'POST',
			data : staffObj,
			processData : false,
			contentType : false,
			dataType : 'JSON',
			success : function(data) {

				alert('Staff saved successfully.');
				general.reloadWindow();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the image uploaded and show it to the user
	var getImage = function() {

		var file = $(settings.staffImage).get(0).files[0];
		if (file) {
			if (!!file.type.match(/image.*/)) {
                if ( window.FileReader ) {
                    reader = new FileReader();
                    reader.onloadend = function (e) {
                        //showUploadedItem(e.target.result, file.fileName);
                        $('#staffImageDisplay').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
		};

		return file;
	}

	var fetch = function(staid) {

		$.ajax({
			url : base_url + 'index.php/staff/fetchStaff',
			type : 'POST',
			data : { 'staid' : staid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {

					$('#experience-table tbody tr').remove();
					$('#qualification-table tbody tr').remove();
					populateStaffData(data.staff);
					populateSalaryData(data.salary);
					populateQualification(data.quali);
					populateExperience(data.exp);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateExperience = function(exp) {

		$.each(exp, function(index, elem) {

			appendToExpTable(elem.job, elem.from.substring(0, 10), elem.to.substring(0, 10), elem.pd1);
		});
	}
	var populateQualification = function(quali) {

		$.each(quali, function(index, elem) {

			appendToQualiTable(elem.quali, elem.grade, elem.year, elem.institute, elem.subject);
		});
	}
	var populateSalaryData = function(salary) {

		$.each(salary, function(index, elem) {

			$('#txtbs').val(elem.bs);
			$('#txtDesignation').val(elem.designation);
			$('#bank_dropdown').val(elem.bankname);
			$('#txtAccountNo').val(elem.acno);

			$('#txtbpay').val(parseFloat(elem.bpay).toFixed(2));
			$('#txtinipay').val(parseFloat(elem.inipay).toFixed(2));
			$('#txthrent').val(parseFloat(elem.hrent).toFixed(2));
			$('#txtconvallow').val(parseFloat(elem.convallow).toFixed(2));
			$('#txtmedallow').val(parseFloat(elem.medallow).toFixed(2));
			$('#txtentertain').val(parseFloat(elem.entertain).toFixed(2));
			$('#txtcharg').val(parseFloat(elem.charge).toFixed(2));
			$('#txtnetpay').val(parseFloat(elem.netpay).toFixed(2));
			$('#txthouseh').val(parseFloat(elem.househ).toFixed(2));
			$('#txtscall').val(parseFloat(elem.scall).toFixed(2));
			$('#txtpublicsall').val(parseFloat(elem.publicsall).toFixed(2));
			$('#txtsaall').val(parseFloat(elem.saall).toFixed(2));
			$('#txtdearness').val(parseFloat(elem.dearness).toFixed(2));
			$('#txtadhoc1').val(parseFloat(elem.adhoc1).toFixed(2));
			$('#txtadhoc2').val(parseFloat(elem.adhoc2).toFixed(2));
			$('#txtarrears').val(parseFloat(elem.arrears).toFixed(2));
			$('#txtpfund').val(parseFloat(elem.pfund).toFixed(2));
			$('#txtincome').val(parseFloat(elem.income).toFixed(2));
			$('#txthostle').val(parseFloat(elem.hostel).toFixed(2));
			$('#txtpessi').val(parseFloat(elem.pessi).toFixed(2));
			$('#txtscont').val(parseFloat(elem.scont).toFixed(2));
			$('#txtrecovery').val(parseFloat(elem.recovery).toFixed(2));
			$('#txttotalpay').val(parseFloat(elem.totalpay).toFixed(2));
			$('#txttdeduc').val(parseFloat(elem.tdeduc).toFixed(2));
			$('#txtloan').val(parseFloat(elem.loan).toFixed(2));
		});
	}
	var populateStaffData = function(staff) {

		$.each(staff, function(index, elem) {

			$(settings.txtStaffId).val(elem.staid);
			$(settings.txtStaffIdHidden).val(elem.staid);
			$(settings.txtPIdHidden).val(elem.pid);

			$(settings.branch_dropdown).val(elem.brid);
			$(settings.current_date).datepicker("update", elem.date.substring(0, 10));
			(elem.active === "1") ? $(settings.active).bootstrapSwitch('state', true) : $(settings.active_switch).bootstrapSwitch('state', false);
			$(settings.type_dropdown).val(elem.type);
			$(settings.agreement_dropdown).val(elem.agreement);

			$(settings.txtName).val(elem.name);
			$(settings.txtFatherName).val(elem.fname);
			$(settings.gender_dropdown).val(elem.gender);
			$('#txtPermanetAddress').val(elem.cast);
			$(settings.marital_dropdown).val(elem.mstatus);
			$(settings.religion_dropdown).val(elem.religion);
			$(settings.txtcnic).val(elem.cnic);
			$(settings.birth_date).datepicker("update", elem.birthdate.substring(0, 10));
			$(settings.joining_date).datepicker("update", elem.jdate.substring(0, 10));
			$(settings.txtAddress).val(elem.address);
			$(settings.txtPhoneNo).val(elem.phone);
			$(settings.txtMobileNo).val(elem.mobile);

			$(settings.txtSalary).val(elem.salary);

			 // set image
			if (elem.photo !== "") {
				$('#staffImageDisplay').attr('src', base_url + '/assets/uploads/staff/' + elem.photo);
			} else {
				$('#staffImageDisplay').attr('src', base_url + '/assets/img/student.jpg');
			}
		});
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		var brid = $('.brid').val();
		$.ajax({

			url : base_url + 'index.php/staff/getMaxId',
			type : 'POST',
			data : {'brid' : brid},
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtStaffId).val(data);
				$(settings.txtMaxStaffIdHidden).val(data);
				$(settings.txtStaffIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveStaffObject = function() {

		//---------------------------- staff inforamtion -----------------------//
		var staffObj = {
			staid : $.trim($(settings.txtStaffIdHidden).val()),

			brid : $.trim($(settings.branch_dropdown).val()),
			name : $.trim($(settings.txtName).val()),
			fname : $.trim($(settings.txtFatherName).val()),
			religion : $.trim($(settings.religion_dropdown).val()),
			address : $.trim($(settings.txtAddress).val()),
			phone : $.trim($(settings.txtPhoneNo).val()),
			cast : $.trim($('#txtPermanetAddress').val()),			
			birthdate : $.trim($(settings.birth_date).val()),
			jdate : $.trim($(settings.joining_date).val()),
			type : $.trim($(settings.type_dropdown).val()),
			salary : $.trim($(settings.txtSalary).val()),
			date : $.trim($(settings.current_date).val()),
			mobile : $.trim($(settings.txtMobileNo).val()),
			agreement : $.trim($(settings.agreement_dropdown).val()),
			gender : $.trim($(settings.gender_dropdown).val()),
			mstatus : $.trim($(settings.marital_dropdown).val()),
			active : ($(settings.active_switch).bootstrapSwitch('state') === true) ? 1 : 0,
			cnic : $.trim($(settings.txtcnic).val()),
			pid : $.trim($(settings.txtPIdHidden).val())
		};

		//---------------------------- salary inforamtion -----------------------//
		var salaryObj = {

			staid : $.trim($(settings.txtStaffIdHidden).val()),
			brid : $.trim($(settings.branch_dropdown).val()),
			bs : $.trim($('#txtbs').val()),
			designation : $.trim($('#txtDesignation').val()),
			bpay : $.trim($('#txtbpay').val()),
			inipay : $.trim($('#txtinipay').val()),
			hrent : $.trim($('#txthrent').val()),
			convallow : $.trim($('#txtconvallow').val()),
			medallow : $.trim($('#txtmedallow').val()),
			entertain : $.trim($('#txtentertain').val()),
			charge : $.trim($('#txtcharg').val()),
			bankname : $(settings.bank_dropdown).val(),
			acno : $.trim((settings.txtAccountNo).val()),
			netpay : $.trim($('#txtnetpay').val()),
			househ : $.trim($('#txthouseh').val()),
			scall : $.trim($('#txtscall').val()),
			publicsall : $.trim($('#txtpublicsall').val()),
			saall : $.trim($('#txtsaall').val()),
			dearness : $.trim($('#txtdearness').val()),
			adhoc1 : $.trim($('#txtadhoc1').val()),
			adhoc2 : $.trim($('#txtadhoc2').val()),
			arrears : $.trim($('#txtarrears').val()),
			pfund : $.trim($('#txtpfund').val()),
			income : $.trim($('#txtincome').val()),
			hostel : $.trim($('#txthostle').val()),
			pessi : $.trim($('#txtpessi').val()),
			scont : $.trim($('#txtscont').val()),
			recovery : $.trim($('#txtrecovery').val()),
			dcno : $.trim($(settings.txtStaffIdHidden).val()),
			totalpay : $.trim($('#txttotalpay').val()),
			tdeduc : $.trim($('#txttdeduc').val()),
			loan : $.trim($('#txtloan').val())
		};

		//---------------------------- qualification inforamtion -----------------------//
		var qualifications = [];
		var rows = $('#qualification-table tbody').find('tr').length;
		if ( rows > 0 ) {

			// loop through each row
			$('#qualification-table tbody').find('tr').each(function(index, elem) {

				var q = {
					staid : $.trim($(settings.txtStaffIdHidden).val()),
					quali : $.trim($(elem).closest('tr').find('td').eq(0).text()),
					grade : $.trim($(elem).closest('tr').find('td').eq(1).text()),
					year : $.trim($(elem).closest('tr').find('td').eq(2).text()),
					subject : $.trim($(elem).closest('tr').find('td').eq(4).text()),
					institute : $.trim($(elem).closest('tr').find('td').eq(3).text())
				};
				qualifications.push(q);
			});
		}

		//---------------------------- experience inforamtion -----------------------//
		var experiences = [];
		var rows = $('#experience-table tbody').find('tr').length;
		if ( rows > 0 ) {

			// loop through each row
			$('#experience-table tbody').find('tr').each(function(index, elem) {

				var e = {
					staid : $.trim($(settings.txtStaffIdHidden).val()),
					job : $.trim($(elem).closest('tr').find('td').eq(0).text()),
					from : $.trim($(elem).closest('tr').find('td').eq(1).text()),
					to : $.trim($(elem).closest('tr').find('td').eq(2).text()),
					pd1 : $.trim($(elem).closest('tr').find('td').eq(3).text())
				};
				experiences.push(e);
			});
		}

		//---------------------------- account inforamtion -----------------------//
		var account = {

			pid : $.trim($(settings.txtPIdHidden).val()),
			active : '1',
			name : $.trim($(settings.txtName).val()),
			brid : $.trim($(settings.branch_dropdown).val()),
			level3 : '',
			dcno : $.trim($(settings.txtStaffIdHidden).val()),
			address : $.trim($(settings.txtAddress).val()),
			cnic : $.trim($(settings.txtcnic).val()),
			phone : $.trim($(settings.txtPhoneNo).val()),
			etype : 'staff',
			mobile : $.trim($(settings.txtMobileNo).val())
		};

		var staff = JSON.stringify(staffObj);
		var salary = JSON.stringify(salaryObj);
		var quali = JSON.stringify(qualifications);
		var exp = JSON.stringify(experiences);
		var acc = JSON.stringify(account);

		var form_data = new FormData();
		form_data.append('staff', staff);
		form_data.append('salary', salary);
		form_data.append('quali', quali);
		form_data.append('exp', exp);
		form_data.append('acc', acc);

		form_data.append("photo", getImage());

		return form_data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var branch_dropdown = $(settings.branch_dropdown).val();
		var type_dropdown = $.trim($(settings.type_dropdown).val());
		var agreement_dropdown = $.trim($(settings.agreement_dropdown).val());
		var txtFatherName = $.trim($(settings.txtFatherName).val());
		var txtName = $.trim($(settings.txtName).val());
		var religion_dropdown = $.trim($(settings.religion_dropdown).val());
		var txtcnic = $.trim($(settings.txtcnic).val());

		var bank_dropdown = $.trim($(settings.bank_dropdown).val());
		var txtAccountNo = $.trim($(settings.txtAccountNo).val());
		var txtSalary = $.trim($(settings.txtSalary).val());

		// remove the error class first
		$(settings.branch_dropdown).removeClass('inputerror');
		$(settings.type_dropdown).removeClass('inputerror');
		$(settings.agreement_dropdown).removeClass('inputerror');
		$(settings.txtFatherName).removeClass('inputerror');
		$(settings.txtName).removeClass('inputerror');
		$(settings.religion_dropdown).removeClass('inputerror');
		$(settings.txtcnic).removeClass('inputerror');
		$(settings.bank_dropdown).removeClass('inputerror');
		$(settings.txtAccountNo).removeClass('inputerror');
		$(settings.txtSalary).removeClass('inputerror');

		if ( branch_dropdown === '' || branch_dropdown === null ) {
			$(settings.branch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( type_dropdown === '' || type_dropdown === null ) {
			$(settings.type_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( agreement_dropdown === '' || agreement_dropdown === null ) {
			$(settings.agreement_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtName === '' || txtName === null ) {
			$(settings.txtName).addClass('inputerror');
			errorFlag = true;
		}

		if ( religion_dropdown === '' || religion_dropdown === null ) {
			$(settings.religion_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtcnic === '' || txtcnic === null ) {
			$(settings.txtcnic).addClass('inputerror');
			errorFlag = true;
		}

		if ( bank_dropdown === '' || bank_dropdown === null ) {
			$(settings.bank_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtAccountNo === '' || txtAccountNo === null ) {
			$(settings.txtAccountNo).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtSalary === '' || txtSalary === null ) {
			$(settings.txtSalary).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtFatherName === '' || txtFatherName === null ) {
			$(settings.txtFatherName).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var validateQualifaication = function() {


		var errorFlag = false;
		var quali = $.trim($(settings.txtQuali).val());
		var div = $.trim($(settings.txtDivision).val());
		var year = $.trim($(settings.txtYear).val());
		var insti = $.trim($(settings.txtInstitute).val());
		var subj = $.trim($(settings.txtMSubjects).val());

		// remove the error class first
		$(settings.txtQuali).removeClass('inputerror');
		$(settings.txtDivision).removeClass('inputerror');
		$(settings.txtYear).removeClass('inputerror');
		$(settings.txtInstitute).removeClass('inputerror');
		$(settings.txtMSubjects).removeClass('inputerror');

		if ( quali === '' || quali === null ) {
			$(settings.txtQuali).addClass('inputerror');
			errorFlag = true;
		}

		if ( div === '' || div === null ) {
			$(settings.txtDivision).addClass('inputerror');
			errorFlag = true;
		}

		if ( year === '' || year === null ) {
			$(settings.txtYear).addClass('inputerror');
			errorFlag = true;
		}

		if ( insti === '' || insti === null ) {
			$(settings.txtInstitute).addClass('inputerror');
			errorFlag = true;
		}

		if ( subj === '' || subj === null ) {
			$(settings.txtMSubjects).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToQualiTable = function(quali, div, year, insti, subj) {

		var row = "";
		row = 	"<tr> <td> "+ quali +"</td> <td> "+ div +"</td> <td> "+ year +"</td> <td> "+ insti +"</td> <td> "+ subj +"</td> <td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo("#qualification-table tbody");
	}

	var validateExperience = function() {

		var errorFlag = false;
		var jobHeld = $.trim($(settings.txtJobHeld).val());
		var from = $.trim($(settings.from_date).val());
		var to = $.trim($(settings.to_date).val());
		var payDraws = $.trim($(settings.txtPayDraws).val());

		// remove the error class first
		$(settings.txtJobHeld).removeClass('inputerror');
		$(settings.from_date).removeClass('inputerror');
		$(settings.to_date).removeClass('inputerror');
		$(settings.txtPayDraws).removeClass('inputerror');

		if ( jobHeld === '' || jobHeld === null ) {
			$(settings.txtJobHeld).addClass('inputerror');
			errorFlag = true;
		}

		if ( from === '' || from === null ) {
			$(settings.from_date).addClass('inputerror');
			errorFlag = true;
		}

		if ( to === '' || to === null ) {
			$(settings.to_date).addClass('inputerror');
			errorFlag = true;
		}

		if ( payDraws === '' || payDraws === null ) {
			$(settings.txtPayDraws).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToExpTable = function(jobHeld, from, to, paydraws) {

		var row = "";
		row = 	"<tr> <td> "+ jobHeld +"</td> <td> "+ from +"</td> <td> "+ to +"</td> <td> "+ paydraws +"</td> <td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo("#experience-table tbody");
	}

	var calc = function() {

		var inipay = (isNaN(parseFloat($('#txtinipay').val())) == true) ? 0 : parseFloat($('#txtinipay').val());
		var bpay = (isNaN(parseFloat($('#txtbpay').val())) == true) ? 0 : parseFloat($('#txtbpay').val());

		var thrent = (inipay * 30) / 100;
		$('#txthrent').val(thrent);

		if ($.trim($('#txtdesignation').val()) !== 'principal') {

			var convallow = (inipay * 15) / 100;
		    $('#txtconvallow').val(convallow);
		}

		var medallow = (inipay * 10) / 100;
	    $('#txtmedallow').val(medallow);

	    var pfund = (bpay * 15) / 100;
	    $('#txtpfund').val(pfund);


        var adhoc2 = (bpay * 30) / 100;
		$('#txtadhoc2').val(adhoc2);

		if (parseInt($.trim($('#txtbs').val())) > 0 && parseInt($.trim($('#txtbs').val())) <= 16) {
			var adhoc1 = (bpay * 20) / 100;
		    $('#txtadhoc1').val(adhoc1);
		} else if ( parseInt($.trim($('#txtbs').val())) > 17 ) {
			var adhoc1 = (bpay * 15) / 100;
		    $('#txtadhoc1').val(adhoc1);
		}

		var bpay = (isNaN(parseFloat($('#txtbpay').val())) == true) ? 0 : parseFloat($('#txtbpay').val());
		var hrent = (isNaN(parseFloat($('#txthrent').val())) == true) ? 0 : parseFloat($('#txthrent').val());
		var medallow = (isNaN(parseFloat($('#txtmedallow').val())) == true) ? 0 : parseFloat($('#txtmedallow').val());
		var entertain = (isNaN(parseFloat($('#txtentertain').val())) == true) ? 0 : parseFloat($('#txtentertain').val());
		var charg = (isNaN(parseFloat($('#txtcharg').val())) == true) ? 0 : parseFloat($('#txtcharg').val());
		var househ = (isNaN(parseFloat($('#txthouseh').val())) == true) ? 0 : parseFloat($('#txthouseh').val());
		var convallow = (isNaN(parseFloat($('#txtconvallow').val())) == true) ? 0 : parseFloat($('#txtconvallow').val());
		var scall = (isNaN(parseFloat($('#txtscall').val())) == true) ? 0 : parseFloat($('#txtscall').val());
		var publicsall = (isNaN(parseFloat($('#txtpublicsall').val())) == true) ? 0 : parseFloat($('#txtpublicsall').val());
		var saall = (isNaN(parseFloat($('#txtsaall').val())) == true) ? 0 : parseFloat($('#txtsaall').val());
		var adhoc1 = (isNaN(parseFloat($('#txtadhoc1').val())) == true) ? 0 : parseFloat($('#txtadhoc1').val());
		var arrears = (isNaN(parseFloat($('#txtarrears').val())) == true) ? 0 : parseFloat($('#txtarrears').val());
		var adhoc2 = (isNaN(parseFloat($('#txtadhoc2').val())) == true) ? 0 : parseFloat($('#txtadhoc2').val());
		var dearness = (isNaN(parseFloat($('#txtdearness').val())) == true) ? 0 : parseFloat($('#txtdearness').val());
		var totalpay = 	bpay+hrent+ medallow+ entertain+ charg+ househ+ convallow+ scall+ publicsall+ saall+ adhoc1+ arrears+ adhoc2+ dearness;
		$('#txttotalpay').val(totalpay);

        if ($.trim($('#txtpfund').val()) != '') {
        	calc2();
        }
	}
	var calc2 = function() {

		var income = (isNaN(parseFloat($('#txtincome').val())) == true) ? 0 : parseFloat($('#txtincome').val());
		var loan = (isNaN(parseFloat($('#txtloan').val())) == true) ? 0 : parseFloat($('#txtloan').val());
		var scont = (isNaN(parseFloat($('#txtscont').val())) == true) ? 0 : parseFloat($('#txtscont').val());
		var pfund = (isNaN(parseFloat($('#txtpfund').val())) == true) ? 0 : parseFloat($('#txtpfund').val());
		var recovery = (isNaN(parseFloat($('#txtrecovery').val())) == true) ? 0 : parseFloat($('#txtrecovery').val());
		var hostle = (isNaN(parseFloat($('#txthostle').val())) == true) ? 0 : parseFloat($('#txthostle').val());
		var pessi = (isNaN(parseFloat($('#txtpessi').val())) == true) ? 0 : parseFloat($('#txtpessi').val());
		var tdeduc = 	income + loan + scont + pfund + recovery + hostle + pessi;
		$('#txttdeduc').val(tdeduc);

		var totalpay = (isNaN(parseFloat($('#txttotalpay').val())) == true) ? 0 : parseFloat($('#txttotalpay').val());
		var tdeduc = (isNaN(parseFloat($('#txttdeduc').val())) == true) ? 0 : parseFloat($('#txttdeduc').val());
		var netpay = 	totalpay - tdeduc;
		$('#txtnetpay').val(netpay);
		$(settings.txtSalary).val(netpay);
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			// when btnSave is clicked
			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.initSave();
			});

			// when btnReset is clicked
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.resetVoucher();
			});

			// when text is changed in txtStaffId
			$(settings.txtStaffId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtStaffId).val().trim() !== "" ) {

						var staid = $.trim($(settings.txtStaffId).val());
						fetch(staid);
					}
				}
			});

			//----------------------- when image is changed -----------------------//
			$(settings.staffImage).on('change', function() {

				getImage();
			});


			//----------------------- model -----------------------//
			$("a[href='#TypeModel']").on('click', function() {
				$('#txtNewType').val('');
			});
			$("a[href='#AgreementModel']").on('click', function() {
				$('#txtNewAgreement').val('');
			});
			$("a[href='#ReligionModel']").on('click', function() {
				$('#txtNewReligion').val('');
			});
			$("a[href='#BankModel']").on('click', function() {
				$('#txtNewBank').val('');
			});
			$('.btnNewType').on('click', function() {

				if ($('#txtNewType').val() !== "") {

					var newType = "<option value='"+ $('#txtNewType').val() +"' selected>"+ $('#txtNewType').val() + "</option>";

					$(newType).appendTo(settings.type_dropdown);
					$(this).siblings().trigger('click');
				}
			});
			$('.btnNewAgreement').on('click', function() {

				if ($('#txtNewAgreement').val() !== "") {

					var newAgreement = "<option value='"+ $('#txtNewAgreement').val() +"' selected>"+ $('#txtNewAgreement').val() + "</option>";

					$(newAgreement).appendTo(settings.agreement_dropdown);
					$(this).siblings().trigger('click');
				}
			});
			$('.btnNewReligion').on('click', function() {

				if ($('#txtNewReligion').val() !== "") {

					var newReligion = "<option value='"+ $('#txtNewReligion').val() +"' selected>"+ $('#txtNewReligion').val() + "</option>";

					$(newReligion).appendTo(settings.religion_dropdown);
					$(this).siblings().trigger('click');
				}
			});
			$('.btnNewBank').on('click', function() {

				if ($('#txtNewBank').val() !== "") {

					var newBank = "<option value='"+ $('#txtNewBank').val() +"' selected>"+ $('#txtNewBank').val() + "</option>";

					$(newBank).appendTo(settings.bank_dropdown);
					$(this).siblings().trigger('click');
				}
			});


			//------------------- qualifaication  tab -----------------------//
			$(settings.btnAddQuali).on('click', function(e) {

				e.preventDefault();
				var quali = $.trim($(settings.txtQuali).val());
				var div = $.trim($(settings.txtDivision).val());
				var year = $.trim($(settings.txtYear).val());
				var insti = $.trim($(settings.txtInstitute).val());
				var subj = $.trim($(settings.txtMSubjects).val());

				var error = validateQualifaication();
				if (!error) {

					appendToQualiTable(quali, div, year, insti, subj);
					$(settings.txtQuali).val('');
					$(settings.txtDivision).val('');
					$(settings.txtYear).val('');
					$(settings.txtInstitute).val('');
					$(settings.txtMSubjects).val('');

				} else {
					alert('Correct the errors...');
				}
			});
			$('table#qualification-table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();
				var quali = $.trim($(this).closest('tr').find('td').eq(0).text());
				var div = $.trim($(this).closest('tr').find('td').eq(1).text());
				var year = $.trim($(this).closest('tr').find('td').eq(2).text());
				var insti = $.trim($(this).closest('tr').find('td').eq(3).text());
				var subj = $.trim($(this).closest('tr').find('td').eq(4).text());

				$(settings.txtQuali).val(quali);
				$(settings.txtDivision).val(div);
				$(settings.txtYear).val(year);
				$(settings.txtInstitute).val(insti);
				$(settings.txtMSubjects).val(subj);

				$(this).closest('tr').remove();
			});
			$('table#qualification-table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				$(this).closest('tr').remove();
			});

			//------------------- experience  tab -----------------------//
			$(settings.btnAddExp).on('click', function(e) {

				e.preventDefault();
				var jobHeld = $.trim($(settings.txtJobHeld).val());
				var from = $.trim($(settings.from_date).val());
				var to = $.trim($(settings.to_date).val());
				var paydraws = $.trim($(settings.txtPayDraws).val());

				var error = validateExperience();
				if (!error) {

					appendToExpTable(jobHeld, from, to, paydraws);
					$(settings.txtJobHeld).val('');
					$(settings.from_date).datepicker('update', new Date());
					$(settings.to_date).datepicker('update', new Date());
					$(settings.txtPayDraws).val('');

				} else {
					alert('Correct the errors...');
				}
			});
			$('table#experience-table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();
				var jobHeld = $.trim($(this).closest('tr').find('td').eq(0).text());
				var from = $.trim($(this).closest('tr').find('td').eq(1).text());
				var to = $.trim($(this).closest('tr').find('td').eq(2).text());
				var paydraws = $.trim($(this).closest('tr').find('td').eq(3).text());

				$(settings.txtJobHeld).val(jobHeld);
				$(settings.from_date).datepicker('update', from);
				$(settings.to_date).datepicker('update', to);
				$(settings.txtPayDraws).val(paydraws);

				$(this).closest('tr').remove();
			});
			$('table#experience-table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				$(this).closest('tr').remove();
			});

			//------------------ salary calculation -----------------------------//
			$('#txtinipay').on('input', function() {

				$('#txtbpay').val($(this).val());
				$('#txtnetpay').val($(this).val());
				calc();
			});
			$('.calc').on('input', function(){
				calc();
			});
			$('.calc2').on('input', function(){
				calc2();
			});

			getMaxId();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var staffObj = getSaveStaffObject();
			var error = validateSave();

			if (!error) {

				save( staffObj );
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');

			$(settings.txtStaffId).val('');
			$(settings.txtMaxStaffIdHidden).val('');
			$(settings.txtStaffIdHidden).val('');
			$(settings.txtPIdHidden).val('');
			
			$(settings.branch_dropdown).val('');
			$(settings.type_dropdown).val('');
			$(settings.agreement_dropdown).val('');
			$(settings.religion_dropdown).val('');
			$(settings.bank_dropdown).val('');
			$(settings.active_switch).bootstrapSwitch('state', true);
			$(settings.current_date).datepicker("update", new Date());
			$(settings.txtName).val('');
			$(settings.txtFatherName).val('');
			$(settings.gender_dropdown).val('male');
			$(settings.marital_dropdown).val('single');
			$(settings.txtcnic).val('');
			$(settings.birth_date).datepicker("update", new Date());
			$(settings.joining_date).datepicker("update", new Date());
			$(settings.txtAddress).val('');
			$(settings.txtPhoneNo).val('');
			$(settings.txtMobileNo).val('');
			$(settings.txtAccountNo).val('');
			$(settings.txtSalary).val('');

			$('#txtbs').val('');
			$('#txtDesignation').val('');
			$('#bank_dropdown').val('');
			$('#txtAccountNo').val('');

			$('#txtbpay').val('');
			$('#txtinipay').val('');
			$('#txthrent').val('');
			$('#txtconvallow').val('');
			$('#txtmedallow').val('');
			$('#txtentertain').val('');
			$('#txtcharg').val('');
			$('#txtnetpay').val('');
			$('#txthouseh').val('');
			$('#txtscall').val('');
			$('#txtpublicsall').val('');
			$('#txtsaall').val('');
			$('#txtdearness').val('');
			$('#txtadhoc1').val('');
			$('#txtadhoc2').val('');
			$('#txtarrears').val('');
			$('#txtpfund').val('');
			$('#txtincome').val('');
			$('#txthostle').val('');
			$('#txtpessi').val('');
			$('#txtscont').val('');
			$('#txtrecovery').val('');
			$('#txttotalpay').val('');
			$('#txttdeduc').val('');
			$('#txtloan').val('');

			$('#experience-table tbody tr').remove();
			$('#qualification-table tbody tr').remove();
			$('#staffImageDisplay').attr('src', base_url + 'assets/img/student.jpg');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var staff = new Staff();
staff.init();