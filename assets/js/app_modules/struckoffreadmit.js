var StruckOffReadmit = function() {

	var validateSave = function() {

		var errorFlag = false;
		var stdid = $.trim($('#stdid_dropdown').val());
		var reason = $.trim($('#txtReason').val());

		// remove the error class first
		$('#stdid_dropdown').removeClass('inputerror');
		$('#txtReason').removeClass('inputerror');

		if ( stdid === '' || stdid === null ) {
			$('#stdid_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( reason === '' || reason === null ) {
			$('#txtReason').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var save = function(saveObj) {

		$.ajax({
			url : base_url + 'index.php/student/updateBatch',
			type : 'POST',
			data : { 'studentObj' : saveObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Changes saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		})

	};

	var getSaveObj = function() {

		var obj = {};
		obj.stdid = $.trim($('#stdid_dropdown').val());
		obj.struckoff = ($('#sr_dropdown').val() === 'struckoff') ? 0 : 1;
		obj.adddate = $('#current_date').val();
		obj.remarks = $.trim($('#txtReason').val());

		return obj;
	}

	return {
		
		init : function() {
			this.bindUI();
		},

		bindUI : function() {
			var self = this;
			
			$('#stdid_dropdown').on('change', function() {
				var name = $.trim($(this).find('option:selected').data('name'));
				$('#txtName').text(name);
			});

			$('.btnSave').on('click', function(e) {
				e.preventDefault();

				self.initSave();
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
		},

		initSave : function() {

			var saveObj = getSaveObj();
			var error = validateSave();

			if (!error) {
				save(saveObj);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher: function() {
			/*$('#stdid_dropdown').val('');
			$('#sr_dropdown').val('struckoff');
			$('#current_date').datepicker('update', new Date());
			$('#txtReason').val('');
			$('#txtName').text('No Student selected');
			general.setPrivillages();*/

			general.reloadWindow();
		}
	};
};

var struckOffReadmit = new StruckOffReadmit();
struckOffReadmit.init();