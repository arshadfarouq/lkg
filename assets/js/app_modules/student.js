var Student = function() {

	var settings = {

		totalMarks : 0,

		// fields
		txtStudentId : $('#txtStudentId'),
		txtMaxStudentIdHidden : $('#txtMaxStudentIdHidden'),
		txtStudentIdHidden : $('#txtStudentIdHidden'),
		txtPIdHidden : $('#txtPIdHidden'),

		branch_dropdown : $('.brid'),
		txtAdmiDate : $('#txtAdmiDate'),
		active : $('#active'),
		txtStudentName : $('#txtStudentName'),
		txtFatherName : $('#txtFatherName'),
		txtDOB : $('#txtDOB'),
		gender_dropdown : $('#gender_dropdown'),
		txtRollNo : $('#txtRollNo'),
		admiClass_dropdown : $('#admiClass_dropdown'),
		presentClass_dropdown : $('#presentClass_dropdown'),
		section_dropdown : $('#section_dropdown'),
		feeCategory_dropdown : $('#feeCategory_dropdown'),
		feeType_dropdown : $('#feeType_dropdown'),
		txtAddress : $('#txtAddress'),
		txtbusinessAddress : $('#txtbusinessAddress'),
		txtPhoneNo : $('#txtPhoneNo'),
		txtMobileNo : $('#txtMobileNo'),
		txtRemarks : $('#txtRemarks'),
		studentImage : $('#studentImage'),
		imp_admiClass_dropdown : $('#imp_admiClass_dropdown'),
		imp_presentClass_dropdown : $('#imp_presentClass_dropdown'),
		imp_section_dropdown : $('#imp_section_dropdown'),
		


		txtFatherOccupation : $('#txtFatherOccupation'),
		txtFatherCNIC : $('#txtFatherCNIC'),
		txtMotherName : $('#txtMotherName'),
		txtMotherTongue : $('#txtMotherTongue'),
		txtParent : $('#txtParent'),
		txtChildren : $('#txtChildren'),


		previousSchool_dropdown : $('#previousSchool_dropdown'),
		testforclass_dropdown : $('#testforclass_dropdown'),
		testforsection_dropdown : $('#testforsection_dropdown'),
		txtObtainedMarks : $('#txtObtainedMarks'),
		txtTotalMarks : $('#txtTotalMarks'),
		txtRemarksTest : $('#txtRemarksTest'),
		testsubjects_table : $('#testsubjects_table'),

		// buttons
		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnPrint : $('.btnPrint'),
		btnCardPrint : $('.btnCardPrint')
	};

	// saves the data into the database
	var save = function( studentObj ) {

		$.ajax({
			url : base_url + 'index.php/student/save',
			type : 'POST',
			data : studentObj,
			processData : false,
			contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Student saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
		// saves the data into the database
		var saveExcel = function( studentExcelObj ) {

			$.ajax({
				url : base_url + 'index.php/student/saveExecel',
				type : 'POST',
				data : {studentExcelObj : studentExcelObj},
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			beforeSend : function(){
				// console.log(stockMainData);
				// console.log('studentExcelObj is   ' + studentExcelObj);
			},
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Student File saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
		}

	// gets the image uploaded and show it to the user
	var getImage = function() {

		var file = $(settings.studentImage).get(0).files[0];
		if (file) {
			if (!!file.type.match(/image.*/)) {
				if ( window.FileReader ) {
					reader = new FileReader();
					reader.onloadend = function (e) {
                        //showUploadedItem(e.target.result, file.fileName);
                        $('#studentImageDisplay').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        };

        return file;
    }

    var fetch = function(stdid) {

    	$.ajax({
    		url : base_url + 'index.php/student/fetchStudent',
    		type : 'POST',
    		data : { 'stdid' : stdid },
    		dataType : 'JSON',
    		success : function(data) {

				// reset the student form
				reset();

				if (data === 'false') {
					alert('No data found');
				} else {
					populateStudentData(data.studentDetail);
					populateTestData(data.testDetail);
					populateKinshipData(data.kinships);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
    }

    var populateKinshipData = function(kinships) {

    	$.each(kinships, function(index, elem) {
    		appendToTableKinship(elem.kstdid, elem.name, elem.relation);
    	});
    }

    var populateTestData = function(data) {

    	var otherMarks = 0;
    	$.each(data, function(index, elem) {

			// appendToTable(elem.sbname, elem.marks);
			var name = elem.sbname.split(' ').join('');
			name = '#'+name+"Marks";
			console.log(name);
			$(name).val(elem.marks);

			$(settings.testforclass_dropdown).val(elem.tclaid);
			$(settings.txtTotalMarks).val(elem.tm);
			$(settings.txtObtainedMarks).val(elem.om);
			$(settings.txtRemarksTest).val(elem.description);

			fetchSectionNamesForAllowedClass($(settings.branch_dropdown).val(), elem.aclaid);
			$(settings.testforsection_dropdown).val(elem.secid);


			otherMarks = parseInt(otherMarks) + parseInt(elem.marks);
		});
    	if (otherMarks >= '50') {				
    		$('#txtStatusResult').val("Pass");
    		$('#allowed_dropdown').val('Admission Granted');
    	} else {
    		$('#txtStatusResult').val("Fail");
    		$('#allowed_dropdown').val('Admission Disallowed');
    	}
    }

    var populateStudentData = function(data) {

    	$.each(data, function(index, elem) {

    		$(settings.txtStudentIdHidden).val(elem.stdid);
    		$(settings.txtPIdHidden).val(elem.pid);

    		$(settings.txtAdmiDate).datepicker("update", elem.adddate.substring(0, 10));
    		(elem.active === "1") ? $(settings.active).bootstrapSwitch('state', true) : $(settings.active).bootstrapSwitch('state', false);
    		$(settings.txtStudentName).val(elem.name);
    		$(settings.txtFatherName).val(elem.fname);
    		$(settings.txtDOB).datepicker("update", elem.birthdate.substring(0, 10));
    		$(settings.gender_dropdown).val(elem.gender);
    		$(settings.txtRollNo).val(elem.rollno);

    		$(settings.admiClass_dropdown).val(elem.adsecid);
    		$(settings.presentClass_dropdown).val(elem.claid);

    		fetchSectionNames(elem.brid, elem.adsecid);
    		$(settings.section_dropdown).val(elem.secid);

    		$(settings.feeCategory_dropdown).val(elem.fid);
    		$(settings.feeType_dropdown).val(elem.feetype);
    		$(settings.txtAddress).val(elem.address);
    		$(settings.txtbusinessAddress).val(elem.fjob);
    		$(settings.txtPhoneNo).val(elem.phone);
    		$(settings.txtMobileNo).val(elem.mobile);
    		$(settings.txtRemarks).val(elem.remarks);

    		$(settings.txtFatherOccupation).val(elem.focc);
    		$(settings.txtFatherCNIC).val(elem.fnic);
    		$(settings.txtMotherName).val(elem.mother);
    		$(settings.txtMotherTongue).val(elem.mtl);
    		$(settings.txtParent).val(elem.parent);
    		$(settings.txtChildren).val(elem.children);

    		$(settings.previousSchool_dropdown).val(elem.pschool);

			 // set image
			 if (elem.photo !== "") {
			 	$('#studentImageDisplay').attr('src', base_url + '/assets/uploads/students/' + elem.photo);
			 }
			});
    }

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/student/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$(settings.txtStudentId).val(data);
				$(settings.txtMaxStudentIdHidden).val(data);
				$(settings.txtStudentIdHidden).val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// fetch all the sections based on the selected class name
	var fetchSectionNames = function( brid, claid ) {

		// clear the previous data
		removeSectionOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data);
				} else {
					removeSectionOptions();
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// fetch all the sections based on the selected class name
	var fetchSectionNamesForAllowedClass = function( brid, claid ) {

		// clear the previous data
		$(settings.testforsection_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			data : { 'claid' : claid, 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {

					$(settings.testforsection_dropdown).children('option').each(function(){
						if ($(this).val() !== "") {
							$(this).remove();
						}
					});

					var sectionNamesOptions = "";
					$.each(data, function( index, elem ) {

						sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
					});

					$(settings.testforsection_dropdown).append(sectionNamesOptions);

					// $(sectionNamesOptions).appendTo(settings.testforsection_dropdown);



				} else {

					// clear the previous data
					$(settings.testforsection_dropdown).children('option').each(function(){
						if ($(this).val() !== "") {
							$(this).remove();
						}
					});
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeSectionOptions = function() {
		$(settings.section_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateSectionNames = function( data ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
			$(sectionNamesOptions).appendTo('#imp_section_dropdown');
		// alert('saasd');
	});
		$(sectionNamesOptions).appendTo(settings.section_dropdown);
		$(sectionNamesOptions).appendTo('#imp_section_dropdown');
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid ) {

		// removes the options from the select tag
		removeOptions();

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			data : { 'brid' : brid },
			async : false,
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data );
				} else {
					// removes the options from the select tag
					removeOptions();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function() {
		$(presentClass_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});

		$(testforclass_dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
			
			$(classNamesOptions).appendTo('#imp_admiClass_dropdown');
			$(classNamesOptions).appendTo('#imp_presentClass_dropdown');
			// alert('dsadas');
		});
		$(classNamesOptions).appendTo(admiClass_dropdown);

		$(classNamesOptions).appendTo(presentClass_dropdown);

		$(classNamesOptions).appendTo(testforclass_dropdown);

		// $(classNamesOptions).append('#imp_admiClass_dropdown');
		// $(classNamesOptions).append('#imp_presentClass_dropdown');
		// $(classNamesOptions).append('#imp_presentClass_dropdown');
	}

	var getSaveStudentObject = function() {

		var studentObj = {
			stdid : $.trim($(settings.txtStudentIdHidden).val()),

			brid : $.trim($(settings.branch_dropdown).val()),
			adddate : $(settings.txtAdmiDate).val(),
			active : ($(settings.active).bootstrapSwitch('state') === true) ? 1 : 0,
			name : $.trim($(settings.txtStudentName).val()),
			fname : $.trim($(settings.txtFatherName).val()),
			birthdate : $.trim($(settings.txtDOB).val()),
			gender : $.trim($(settings.gender_dropdown).val()),
			rollno : $.trim($(settings.txtRollNo).val()),
			adsecid : $.trim($(settings.admiClass_dropdown).val()),
			claid : $.trim($(settings.presentClass_dropdown).val()),
			secid : $.trim($(settings.section_dropdown).val()),
			fid : $.trim($(settings.feeCategory_dropdown).val()),
			feetype : $.trim($(settings.feeType_dropdown).val()),
			address : $.trim($(settings.txtAddress).val()),
			fjob : $.trim($(settings.txtbusinessAddress).val()),
			phone : $.trim($(settings.txtPhoneNo).val()),
			pid : $(settings.txtPIdHidden).val(),
			mobile : $.trim($(settings.txtMobileNo).val()),
			remarks : $.trim($(settings.txtRemarks).val()),
			fullname : $.trim($(settings.txtStudentName).val()) + " S/O " + $.trim($(settings.txtFatherName).val()) + " " + $.trim($(settings.txtStudentIdHidden).val()),

			focc : $.trim($(settings.txtFatherOccupation).val()),
			fnic : $.trim($(settings.txtFatherCNIC).val()),
			mother : $.trim($(settings.txtMotherName).val()),
			mtl : $.trim($(settings.txtMotherTongue).val()),
			parent : $.trim($(settings.txtParent).val()),
			children : $.trim($(settings.txtChildren).val()),

			pschool : $.trim($(settings.previousSchool_dropdown).val()),
			struckoff : general.getCurrentDate()
		};

		// creating kinship object
		var kinships = [];
		var rows = $('#kinship_table tbody').find('tr').length;
		if (rows > 0) {

			$('#kinship_table tbody tr').each(function(index, elem) {

				var ks = {};
				ks.brid = $.trim($(settings.branch_dropdown).val());
				ks.stdid = $.trim($(settings.txtStudentIdHidden).val());
				ks.kstdid = $.trim($(this).find('td.ksstdid').text());
				ks.relation = $.trim($(this).find('td.relation').text());
				kinships.push(ks);				
			});
		}

		// admission test information
		var admTestDetailObj = new Array();
		var admTestMainObj = {};

		var rows = $('#testsubjects_table tbody').find('tr').length;

		// check if we have some data to save
		if ( rows > 0 ) {

			var d = new Date();
			var curDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();

			admTestMainObj.aid = '';
			admTestMainObj.stdid = $.trim($(settings.txtStudentIdHidden).val());
			admTestMainObj.tclaid = $.trim($(settings.testforclass_dropdown).val());
			admTestMainObj.dcno = '0';
			admTestMainObj.name = $.trim($(settings.txtStudentName).val());
			admTestMainObj.fname = $.trim($(settings.txtFatherName).val());
			admTestMainObj.tm = $.trim($(settings.txtTotalMarks).val());
			admTestMainObj.om = $.trim($(settings.txtObtainedMarks).val());
			admTestMainObj.description = $.trim($(settings.txtRemarksTest).val());
			admTestMainObj.date = curDate;
			admTestMainObj.phoneno = $.trim($(settings.txtPhoneNo).val());
			admTestMainObj.status = $('#txtStatusResult').val();
			admTestMainObj.secid = $(settings.testforsection_dropdown).val();
			admTestMainObj.brid = $(settings.branch_dropdown).val();

			// loop through each row
			$('#testsubjects_table tbody').find('tr').each(function() {

				var _marks = $(this).find('td').eq(1).find('input').val();
				var _sbid = $(this).find('td').eq(0).text();

				var admTestDetail = {

					aid : '',
					sbname : _sbid,
					marks :_marks
				};

				admTestDetailObj.push(admTestDetail);
			});
		}

		studentObj.admiMain =  JSON.stringify(admTestMainObj);
		studentObj.admiDetail = JSON.stringify(admTestDetailObj);
		studentObj.kinships = JSON.stringify(kinships);

		var form_data = new FormData();

		for ( var key in studentObj ) {
			form_data.append(key, studentObj[key]);
		}

		form_data.append("photo", getImage());

		return form_data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var txtStudentName = $.trim($(settings.txtStudentName).val());
		var txtFatherName = $.trim($(settings.txtFatherName).val());
		var gender_dropdown = $(settings.gender_dropdown).val();
		var txtRollNo = $.trim($(settings.txtRollNo).val());
		var admiClass_dropdown = $(settings.admiClass_dropdown).val();
		var presentClass_dropdown = $(settings.presentClass_dropdown).val();
		var section_dropdown = $(settings.section_dropdown).val();
		var feeCategory_dropdown = $(settings.feeCategory_dropdown).val();
		var feeType_dropdown = $(settings.feeType_dropdown).val();

		// remove the error class first
		$(settings.txtStudentName).removeClass('inputerror');
		$(settings.txtFatherName).removeClass('inputerror');
		$(settings.gender_dropdown).removeClass('inputerror');
		$(settings.txtRollNo).removeClass('inputerror');
		$(settings.admiClass_dropdown).removeClass('inputerror');
		$(settings.presentClass_dropdown).removeClass('inputerror');
		$(settings.section_dropdown).removeClass('inputerror');
		$(settings.feeCategory_dropdown).removeClass('inputerror');
		$(settings.feeType_dropdown).removeClass('inputerror');

		if ( txtStudentName === '' || txtStudentName === null ) {
			$(settings.txtStudentName).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtFatherName === '' || txtFatherName === null ) {
			$(settings.txtFatherName).addClass('inputerror');
			errorFlag = true;
		}

		if ( gender_dropdown === '' || gender_dropdown === null ) {
			$(settings.gender_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( txtRollNo === '' || txtRollNo === null ) {
			$(settings.txtRollNo).addClass('inputerror');
			errorFlag = true;
		}

		if ( admiClass_dropdown === '' || admiClass_dropdown === null ) {
			$(settings.admiClass_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( presentClass_dropdown === '' || presentClass_dropdown === null ) {
			$(settings.presentClass_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( section_dropdown === '' || section_dropdown === null ) {
			$(settings.section_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( feeCategory_dropdown === '' || feeCategory_dropdown === null ) {
			$(settings.feeCategory_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( feeType_dropdown === '' || feeType_dropdown === null ) {
			$(settings.feeType_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	// checks for the empty fields
	var validateSaveExcel = function() {

		var errorFlag = false;

		var adsecid 	=  $('#imp_admiClass_dropdown').val();
		var claid 		=  $('#imp_presentClass_dropdown').val();
		var secid 		=  $('#imp_section_dropdown').val();
		var fid 		=  $('#impfeeCategory_dropdown').val();
		var feetype 	=  $('#imp_feeType_dropdown').val();
		var Name 		= $('#drp_Name').val();

		// remove the error class first
		$('#imp_admiClass_dropdown').removeClass('inputerror');
		$('#imp_presentClass_dropdown').removeClass('inputerror');
		$('#imp_section_dropdown').removeClass('inputerror');
		$('#impfeeCategory_dropdown').removeClass('inputerror');
		$('#imp_feeType_dropdown').removeClass('inputerror');
		$('#drp_Name').removeClass('inputerror');

		if ( adsecid === '' || adsecid === null ) {
			$('#imp_admiClass_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( claid === '' || claid === null ) {
			$('#imp_presentClass_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( secid === '' || secid === null ) {
			$('#imp_section_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( fid === '' || fid === null ) {
			$('#impfeeCategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( feetype === '' || feetype === null ) {
			$('#imp_feeType_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( Name === '' || Name === null ) {
			$('#drp_Name').addClass('inputerror');
			errorFlag = true;
		}


		return errorFlag;
	}
	var getSaveExcelStudentObject = function() {
		var Deal 		= [];	
		// var	getname 		=  "'"+$.trim($("#drp_Name option:selected").text()) + "'"  ;
		var	getname 		=  $.trim($("#drp_Name option:selected").text())   ;
		var getname = '.'+getname;
		var getname =(getname);
		// alert(getname);
		// var allListElements =(getname);
		var getfname 		= $.trim($('#drp_FatherName option:selected').text());
		var getfname = '.'+getfname;
		var getfname =(getfname);

		var getbirthdate 	= $.trim($('#drp_Dob option:selected').text());
		var getbirthdate = '.'+getbirthdate;
		var getbirthdate =(getbirthdate);

		var getgetgender 		= $.trim($('#drp_gender option:selected').text());
		var getgetgender = '.'+getgetgender;
		var getgetgender =(getgetgender);

		var getrollno 		= $.trim($('#drp_RollNo option:selected').text());
		var getrollno = '.'+getrollno;
		var getrollno =(getrollno);

		var getfeetype 	= $.trim($('#imp_feeType_dropdown option:selected').val());
		var getfeetype = '.'+getfeetype;
		var getfeetype =(getfeetype);

		var getaddress 	= $.trim($('#drp_presentAddress option:selected').text());
		var getaddress = '.'+getaddress;
		var getaddress =(getaddress);

		var getmobile 		= $.trim($('#drp_mobileNo option:selected').text());
		var getmobile = '.'+getmobile;
		var getmobile =(getmobile);

		var getphone 		= $.trim($('#drp_phoneNo option:selected').text());
		var getphone = '.'+getphone;
		var getphone =(getphone);

		var getremarks 	= $.trim($('#drp_Remarks option:selected').text());
		var getremarks = '.'+getremarks;
		var getremarks =(getremarks);

		var getfjob 		= $.trim($('#drp_permanentAddress option:selected').text());
		var getfjob = '.'+getfjob;
		var getfjob =(getfjob);
		$('#table_cvs tbody tr').each(function(index, elem){
			var studentObj = {};
			if ($('#drp_Name').val() !== ''){
				// alert(getname);
				studentObj.name 		= $.trim( $(this).find(getname).text() ); 
				// studentObj.name 		= $.trim( $(this).find('.name').text() ); 
				// alert(studentObj.name);
			}
			if($('#drp_FatherName').val() !== ''){
				studentObj.fname 		= $.trim( $(this).find(getfname).text() ); 

			} if($('#drp_Dob').val() !== ''){
				studentObj.birthdate 	= $.trim( $(this).find(getbirthdate).text() );

			} if($('#drp_gender').val() !== ''){
				studentObj.gender 		= $.trim( $(this).find(getgetgender).text() ); 

			} if($('#drp_RollNo').val() !== ''){
				studentObj.rollno 		= $.trim( $(this).find(getrollno).text() );

			} if($('#drp_Remarks').val() !== ''){
				studentObj.remarks 		= $.trim( $(this).find(getremarks).text() ); 

			} if($('#drp_presentAddress').val() !== ''){
				studentObj.address 		= $.trim( $(this).find(getaddress).text() );

			} if($('#drp_permanentAddress').val() !== ''){
				studentObj.fjob 		= $.trim( $(this).find(getfjob).text() );

			} if($('#drp_mobileNo').val() !== ''){
				studentObj.mobile 		= $.trim( $(this).find(getmobile).text() ); 

			} if($('#drp_phoneNo').val() !== ''){
				studentObj.phone 		= $.trim( $(this).find(getphone).text() ); 

			}

				/*studentObj.name 		= $.trim( $(this).find('.name').text() ); 
				studentObj.fname 		= $.trim( $(this).find('.phone').text() ); 

				studentObj.birthdate 	= $.trim( $(this).find('.nickname').text() );
				studentObj.gender 		= $.trim( $(this).find('.state').text() ); 

				studentObj.rollno 		= $.trim( $(this).find('.name').text() );

				studentObj.remarks 		= $.trim( $(this).find('.phone').text() ); 

				studentObj.address 		= $.trim( $(this).find('.nickname').text() );

				studentObj.fjob 		= $.trim( $(this).find('.name').text() );

				studentObj.mobile 		= $.trim( $(this).find('.state').text() ); 

				studentObj.phone 		= $.trim( $(this).find('.name').text() ); */


				studentObj.adsecid 		= $.trim( $('#imp_admiClass_dropdown').val() );
				studentObj.claid 		= $.trim( $('#imp_presentClass_dropdown').val() );
				studentObj.secid 		= $.trim( $('#imp_section_dropdown').val() );
				studentObj.fid 			= $.trim( $('#impfeeCategory_dropdown').val() );
				studentObj.feetype 		= $.trim( $('#imp_feeType_dropdown').val() );

				Deal.push(studentObj);	

			});
			// console.log(Deal);
			return  Deal;

		}

		var validateTestSubjects = function() {


			var errorFlag = false;
			var testSubj = $('#testsubject_dropdown').val();
			var marks = $('#txtTestSubjectMarks').val();

		// remove the error class first
		$('#testsubject_dropdown').removeClass('inputerror');
		$('#txtTestSubjectMarks').removeClass('inputerror');

		if ( testSubj === '' || testSubj === null ) {
			$('#testsubject_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( marks === '' || marks === null ) {
			$('#txtTestSubjectMarks').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(sbid, marks) {

		var row = "";
		row = 	"<tr> <td> "+ sbid +"</td> <td> "+ marks +"</td> <td><a href='' class='btn btn-primary btnRowEdit' data-sbid="+ sbid +"><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo("#testsubjects_table tbody");
	}

	var appendToTableKinship = function(stdid, name, relation) {


		var row = "";
		row = 	"<tr> <td class='ksstdid'> "+ stdid +"</td> <td class='name'> "+ name +"</td>  <td class='relation'> "+ relation +"</td> <td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo("#kinship_table tbody");
	}

	var printAdmissionForm = function() {
		window.open(base_url + 'application/views/print/admission.php', 'Admission Form', 'width=900, height=842');
	}

	var printStudentCard = function() {
		window.open(base_url + 'application/views/print/studentcard.php', 'Student Card', 'width=900, height=842;')
	}
	var fetchLookupstudents = function () {
		$.ajax({
			url : base_url + 'index.php/student/searchStudentAll',
			type: 'POST',
			data: {'search': '', 'type':'student'},
			dataType: 'JSON',
			success: function (data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataLoookupStudent(data);
				}
			}, error: function (xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataLoookupStudent = function (data) {

		if (typeof dTable != 'undefined') {
			dTable.fnDestroy();
			$('#tblStudents > tbody tr').empty();
		}

		var html = "";
		$.each(data, function (index, elem) {

			html += "<tr>";
			html += "<td width='14%;'>"+ elem.stdid +"<input type='hidden' name='hfModalStudentId' value='"+elem.stdid+"' ></td>";
			html += "<td>"+ elem.student_name +"</td>";
			html += "<td>"+ elem.mobile +"</td>";
			html += "<td>"+ elem.address +"</td>";
			html += "<td>"+ elem.class_name +"</td>";
			html += "<td>"+ elem.section_name +"</td>";
			html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateStudent'><i class='fa fa-search'></i></a></td>";
			html += "</tr>";
		});

		$("#tblStudents > tbody").html('');
		$("#tblStudents > tbody").append(html);
		bindGridStudents();
	}
	var bindGridStudents = function() {

		$('.modal-lookup .populateStudent').on('click', function () {
			var stdid = $(this).closest('tr').find('input[name=hfModalStudentId]').val();
			$('#txtStudentId').val(stdid);
			
			fetch(stdid); 				
		});

		var dontSort = [];
		$('#tblStudents thead th').each(function () {
			if ($(this).hasClass('no_sort')) {
				dontSort.push({ "bSortable": false });
			} else {
				dontSort.push(null);
			}
		});
		dTable = $('#tblStudents').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 10,
            "oTableTools": {
            	"sSwfPath": "js/copy_cvs_xls_pdf.swf",
            	"aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
		$.extend($.fn.dataTableExt.oStdClasses, {
			"s`": "dataTables_wrapper form-inline"
		});
	}
	var reset = function() {

		$(settings.txtAdmiDate).datepicker("update", new Date());
		$(settings.active).bootstrapSwitch('state', true);
		$(settings.active).val('');
		$(settings.txtStudentName).val('');
		$(settings.txtFatherName).val('');
		$(settings.txtDOB).val('');
		$(settings.gender_dropdown).val('');
		$(settings.txtRollNo).val('');
		$(settings.admiClass_dropdown).val('');
		$(settings.presentClass_dropdown).val('');
		$(settings.section_dropdown).val('');
		$(settings.feeCategory_dropdown).val('');
		$(settings.feeType_dropdown).val('');
		$(settings.txtAddress).val('');
		$(settings.txtbusinessAddress).val('');
		$(settings.txtPhoneNo).val('');
		$(settings.txtMobileNo).val('');
		$(settings.txtRemarks).val('');

		$(settings.txtFatherOccupation).val('');
		$(settings.txtFatherCNIC).val('');
		$(settings.txtMotherName).val('');
		$(settings.txtMotherTongue).val('');
		$(settings.txtParent).val('');
		$(settings.txtChildren).val('');

		$(settings.previousSchool_dropdown).val('');
		$(settings.testforclass_dropdown).val('');
		$(settings.testforsection_dropdown).val('');
		$(settings.txtObtainedMarks).val('');
		$(settings.txtTotalMarks).val('');
		$(settings.txtRemarksTest).val('');

		settings.totalMarks = 0;

		$('#studentImageDisplay').attr('src', base_url + 'assets/img/student.jpg');
		$('#kinship_table tbody tr').remove();
	}

	return {

		init : function() {
			this.bindUI();
			// alert('dss');
		},

		bindUI : function() {

			var self = this;

			$("#active").bootstrapSwitch('offText', 'No');
			$("#active").bootstrapSwitch('onText', 'Yes');

			$('#show_sales').DataTable();

			$('body').on('click','#show_sales tbody #btn-edit-sale',function(e){
				e.preventDefault();
				var td 						= $(this).closest('tr.tblRow').find('td.tblCol');
				// alert(td);
				// console.log(td);
				var stdid 					= $.trim(td.eq(0).text());
				// alert(stdid);
				$("a[href='#basicInformation']").trigger('click');
				// $('#sale_table').find('tbody').remove();
				// $('.t_qty').text('');
				// $('.t_amount').text('');
				if (stdid !== null || stdid !== '') {
					$('#txtStudentId').val(stdid);
					fetch(stdid);
				}
			});
			shortcut.add("F1", function(e) {
				e.preventDefault();
				$('a[href="#student-lookup"]').trigger('click');
			});
			// when btnSave is clicked
			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.initSave();
			});
			$('#btnSaveExcel').on('click', function(e) {
				e.preventDefault();
				var $trs = $(".table_cvs");
				if (document.getElementById("table_cvs") !== null) {
					// alert('data exits');
					self.initSaveExcel();
				}
				else{
					alert('Table is empty Please upload Some File');
				}
				// $(this).closest('tr').remove();
			});

			$('form #upload_file').submit(function(e) {
			// $('#submit').on('click',function(e){
				var fileName = $('#userfile').val().split('\\').pop();
				e.preventDefault();
				$.ajaxFileUpload({
						// url 			:'./upload/upload_file/',
						// base_url + 'index.php/student/save', 
						url 			: base_url + 'index.php/upload/upload_file/', 
						secureuri		:false,
						fileElementId	:'userfile',
						dataType		: 'JSON',
						data			: {
							'title'				: $('#title').val()
						},
						success	: function (data)
						{
							alert('Sus');
							// console.log(data);
							// alert(data.file_name);
							// alert(data.file_name);
							// alert($('#userfile').val().split('\\').pop());

							// alert(data[0]['file_name']);
							// console.table(data);
							$.get('http://localhost:82/sc_campus/trunk/sc_campus/files/'+fileName, function(data) {
								$('.table_cvs').find('tbody').remove();
								// start the table	
								var html = '<table class="table table-bordered table-striped table-hover table_cvs" id="table_cvs">';

								// split into lines
								var rows = data.split("\n");

								var list ;
								var flag = false;
								var dataNames = [];
								// parse lines
								rows.forEach( function getvalues(ourrow) {
									// start a table row
									html += "<tr>";
									
									

									// split line into columns
									var columns = ourrow.split(",");
									for (var i = 0; i <= 100; i++) {
										// if (columns[i] !== 'undefined') {
											if (typeof columns[i] != 'undefined') {
												if (flag === false ) {
											  		// alert(flag);
											  		list += columns[i];
											  		var optdrp_Name 		 = "<option value='name'  >"+ columns[i] + "</option>";
											  		var optdrp_FatherName 	 = "<option value='fname'  >"+ columns[i] + "</option>";
											  		var optdrp_Dob 	 		 = "<option value='dob'  >"+ columns[i] + "</option>";
											  		var optdrp_gender 		 = "<option value='gender'  >"+ columns[i] + "</option>";
											  		var optdrp_RollNo 	 	 = "<option value='rollno'  >"+ columns[i] + "</option>";
											  		var optdrp_Remarks 	 	 = "<option value='remarks'  >"+ columns[i] + "</option>";
											  		var optdrp_presentAddress 	= "<option value='presentAddress'  >"+ columns[i] + "</option>";
											  		var optdrp_permanentAddress = "<option value='permanentAddress'  >"+ columns[i] + "</option>";
											  		var optdrp_phoneNo 	 		= "<option value='phoneno'  >"+ columns[i] + "</option>";
											  		var optdrp_mobileNo 	 	= "<option value='mobileNo'  >"+ columns[i] + "</option>";
											  		$(optdrp_Name).appendTo('#drp_Name');
											  		$(optdrp_FatherName).appendTo('#drp_FatherName');
											  		$(optdrp_Dob).appendTo('#drp_Dob');
											  		$(optdrp_gender).appendTo('#drp_gender');
											  		$(optdrp_RollNo).appendTo('#drp_RollNo');
											  		$(optdrp_Remarks).appendTo('#drp_Remarks');
											  		$(optdrp_presentAddress).appendTo('#drp_presentAddress');
											  		$(optdrp_permanentAddress).appendTo('#drp_permanentAddress');
											  		$(optdrp_phoneNo).appendTo('#drp_phoneNo');
											  		$(optdrp_mobileNo).appendTo('#drp_mobileNo');
											  		// alert(columns[i]);
											  		dataNames.push(columns[i])

											  		// alert(list);
											  		
											  	}
											  		// console.log(dataNames);



											  		html += "<td class='"+dataNames[i] +"'>" + columns[i] + "</td>";

											  	}
											  	else{
											  		flag = true;
											  		break;
											  	}
											  }
									// alert(list);
									html += "</tr>";
									// html += "<td>" + columns[0] + "</td>";
									// html += "<td>" + columns[1] + "</td>";
									// html += "<td>" + columns[2] + "</td>";
									// html += "<td>" + columns[3] + "</td>";
									// html += "<td>" + columns[4] + "</td>";
									// html += "<td>" + columns[6] + "</td>";
									// html += "<td>" + columns[7] + "</td>";
									// html += "<td>" + columns[8] + "</td>";
									// html += "<td>" + columns[9] + "</td>";
									// html += "<td>" + columns[10] + "</td>";
									// html += "<td>" + columns[11] + "</td>";
									
									// close row
									// html += "</tr>";		
								})
								// close table
								html += "</table>";
													// alert(list);


								// insert into div
								$('#container').append(html);
								
							});
							if(data.status != 'error')
							{
								$('#files').html('<p>Reloading files...</p>');
								// refresh_files();
								$('#title').val('');
							}
							// alert(data.msg);
						},
						error : function(xhr, status, error) {
							alert('fail');
							console.log(xhr.responseText);
						}
					});
return false;
});



// 			$.get('http://localhost:82/sc_campus/trunk/sc_campus/files/Applicants.csv', function(data) {

// 				// start the table	
// 				var html = '<table class="table table-bordered">';

// 				// split into lines
// 				var rows = data.split("\n");

// 				var list ;
// 				var flag = false;
// 				// parse lines
// 				rows.forEach( function getvalues(ourrow) {
// 					// start a table row
// 					html += "<tr>";



// 					// split line into columns
// 					var columns = ourrow.split(",");
// 					// var i = 100;
// 					for (var i = 1; i <= 100; i++) {
// 						// if (columns[i] !== 'undefined') {
// 							  if (typeof columns[i] != 'undefined') {
// 							  	if (flag === false ) {
// 							  		// alert(flag);
// 							  		list += columns[i];
// ;
// 							  		// alert(columns[i]);
// 							  	}


// 							html += "<td class='"+list[i]+"'>" + columns[i] + "</td>";

// 						}
// 						else{
// 							flag = true;
// 							// alert('end'+ i + flag);
// 							// return false;
// 							break;
// 						}
// 					}
// 					// alert(list);
// 					html += "</tr>";
// 					// html += "<td>" + columns[0] + "</td>";
// 					// html += "<td>" + columns[1] + "</td>";
// 					// html += "<td>" + columns[2] + "</td>";
// 					// html += "<td>" + columns[3] + "</td>";
// 					// html += "<td>" + columns[4] + "</td>";
// 					// html += "<td>" + columns[6] + "</td>";
// 					// html += "<td>" + columns[7] + "</td>";
// 					// html += "<td>" + columns[8] + "</td>";
// 					// html += "<td>" + columns[9] + "</td>";
// 					// html += "<td>" + columns[10] + "</td>";
// 					// html += "<td>" + columns[11] + "</td>";

// 					// close row
// 					// html += "</tr>";		
// 				})
// 				// close table
// 				html += "</table>";
// 									// alert(list);


// 				// insert into div
// 				$('#container').append(html);

// 			});
			// when btnReset is clicked
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.resetVoucher();
			});
			// when btnPrint is clicked
			$(settings.btnPrint).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				printAdmissionForm();
			});
			// when btnCardPrint is clicked
			$(settings.btnCardPrint).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				printStudentCard();
			});

			// when fetchSectionNames is changed
			$(settings.admiClass_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.admiClass_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});
			$(settings.admiClass_dropdown).on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $(settings.admiClass_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});
			$('#imp_admiClass_dropdown').on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $('#imp_admiClass_dropdown').val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});
			$('.btnsearchstudent').on('click',function(e){
				e.preventDefault();

				var length = $('#tblStudents > tbody tr').length;
				
				if(length <= 1){
					fetchLookupstudents();
				}
			});
			$('#imp_admiClass_dropdown').on('change', function() {

				var brid = $(settings.branch_dropdown).val();
				var claid = $('#imp_admiClass_dropdown').val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid);
				}
			});

			// when text is changed in txtStudentId
			$(settings.txtStudentId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtStudentId).val().trim() !== "" ) {

						var stdid = $.trim($(settings.txtStudentId).val());
						fetch(stdid);
					}
				}
			});
			
			$('#txtStudentId').on('change', function() {
				fetch($(this).val());
			});


			// when image is changed
			$(settings.studentImage).on('change', function() {

				getImage();
			});

			// when #PreviousSchoolModel is clicked
			$("a[href='#PreviousSchoolModel']").on('click', function() {
				$('#txtNewPreviousSchool').val('');
			});

			// when btnNewPreviousSchool is clicked
			$('.btnNewPreviousSchool').on('click', function() {

				if ($('#txtNewPreviousSchool').val() !== "") {

					var previousSchool = "<option value='"+ $('#txtNewPreviousSchool').val() +"' selected>"+ $('#txtNewPreviousSchool').val() + "</option>";

					$(previousSchool).appendTo(settings.previousSchool_dropdown);
					$(this).siblings().trigger('click');
				}
			});

			$('#kinStdid_dropdown').on('change', function() {
				var stdid = $(this).val();
				$('#kinName_dropdown').val(stdid);
			});

			$('#kinName_dropdown').on('change', function() {
				var stdid = $(this).val();
				$('#kinStdid_dropdown').val(stdid);
			});

			$('#btnAddKinshipToTable').on('click', function(e) {
				e.preventDefault();

				var stdid = $('#kinStdid_dropdown').val();
				var name = $('#kinName_dropdown').find('option:selected').text();
				var relation = $('#txtKinRelation').val();

				var error = false;
				$('.inputerror').removeClass('inputerror');
				if (stdid == "" || stdid == null) {
					$('#kinName_dropdown').addClass('inputerror');
					error = true;
				}
				if (relation == "" || relation == null) {
					$('#txtKinRelation').addClass('inputerror');
					error = true;
				}

				if (!error) {
					appendToTableKinship(stdid, name, relation);

					$('#kinStdid_dropdown').val('');
					$('#kinName_dropdown').val('');
					$('#txtKinRelation').val('');
				} else {
					alert('Correct the errors!');
				}
			});

			// when btnRowRemove is clicked
			$('#kinship_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				$(this).closest('tr').remove();
			});

			$('#kinship_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var stdid = $.trim($(this).closest('tr').find('td').eq(0).text());
				var name = $.trim($(this).closest('tr').find('td').eq(1).text());
				var relation = $.trim($(this).closest('tr').find('td').eq(2).text());

				$('#kinStdid_dropdown').val(stdid);
				$('#kinName_dropdown').val(stdid);
				$('#txtKinRelation').val(relation);
				$(this).closest('tr').remove();
			});

			$('#testsubjects_table').on('input', '.table-input', function() {
				var marks = ($.trim($(this).closest('tr').find('td').eq(1).find('input').val()) == "") ? 0 : $.trim($(this).closest('tr').find('td').eq(1).find('input').val());

				var otherMarks = 0;
				otherMarks = parseInt(otherMarks) + parseInt(marks);
				$(this).closest('tr').siblings('tr').each(function(index, elem){
					var m = ($.trim($(this).find('td').eq(1).find('input').val()) == "") ? 0 : $.trim($(this).find('td').eq(1).find('input').val());
					otherMarks = parseInt(otherMarks) + parseInt(m);
				});

				if (otherMarks >= '50') {
					$('#txtStatusResult').val("Pass");
					$('#allowed_dropdown').val('Admission Granted');
				} else {
					$('#txtStatusResult').val("Fail");
					$('#allowed_dropdown').val('Admission Disallowed');
				}

				$(settings.txtObtainedMarks).val(otherMarks);
			});

			getMaxId();
			var brid = $(settings.branch_dropdown).val();
			fetchClassNames(brid);
		},
		// prepares the data to save it into the database
		initSaveExcel : function() {

			var studentExcelObj = new getSaveExcelStudentObject();
			var trs = $(".table_cvs");
			var isValid = validateSaveExcel();

			if (trs.length !== 0 && !isValid ) {
				saveExcel(studentExcelObj);
				// alert('tbody exits');
				// save( studentObj );
			} else {
				alert('Data not exits And Correct the errors ');
			}
		},

		// prepares the data to save it into the database
		initSave : function() {

			var studentObj = getSaveStudentObject();
			var isValid = validateSave();

			if (!isValid) {

				save(studentObj);
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');

			$(settings.txtStudentId).val('');
			$(settings.txtMaxStudentIdHidden).val('');
			$(settings.txtStudentIdHidden).val('');

			$(settings.branch_dropdown).val('');
			$(settings.txtAdmiDate).datepicker("update", new Date());
			$(settings.active).bootstrapSwitch('state', true);
			$(settings.active).val('');
			$(settings.txtStudentName).val('');
			$(settings.txtFatherName).val('');
			$(settings.txtDOB).val('');
			$(settings.gender_dropdown).val('');
			$(settings.txtRollNo).val('');
			$(settings.admiClass_dropdown).val('');
			$(settings.presentClass_dropdown).val('');
			$(settings.section_dropdown).val('');
			$(settings.feeCategory_dropdown).val('');
			$(settings.feeType_dropdown).val('');
			$(settings.txtAddress).val('');
			$(settings.txtbusinessAddress).val('');
			$(settings.txtPhoneNo).val('');
			$(settings.txtMobileNo).val('');
			$(settings.txtRemarks).val('');

			 $(settings.txtFatherOccupation).val('');
			 $(settings.txtFatherCNIC).val('');
			 $(settings.txtMotherName).val('');
			 $(settings.txtMotherTongue).val('');
			 $(settings.txtParent).val('');
			 $(settings.txtChildren).val('');

			 $(settings.previousSchool_dropdown).val('');
			 $(settings.testforclass_dropdown).val('');
			 $(settings.allowedforclass_dropdown).val('');
			 $(settings.testforsection_dropdown).val('');
			 $(settings.txtObtainedMarks).val('');
			 $(settings.txtTotalMarks).val('');
			 $(settings.txtRemarksTest).val('');
			 $(settings.testsubjects_table).find('tbody tr').remove();

			 $('#studentImageDisplay').attr('src', base_url + 'assets/img/student.jpg');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	}

};

var student = new Student();
student.init();