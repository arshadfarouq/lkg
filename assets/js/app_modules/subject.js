var Subject = function() {

	var settings = {

		txtSubjectId : $('#txtSubjectId'),
		txtMaxSubjectIdHidden : $('#txtMaxSubjectIdHidden'),
		txtSubjectIdHidden : $('#txtSubjectIdHidden'),

		txtName : $('#txtName'),
		txtNameHidden : $('#txtNameHidden'),

		txtDescription : $('#description'),

		allSubjects : $('#allSubjects'),

		btnSave : $('.btnSave'),
		btnReset : $('.btnReset'),
		btnEditSubject : $('.btn-edit-subject')
	};

	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/subject/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#txtSubjectId').val(data);
				$('#txtMaxSubjectIdHidden').val(data);
				$('#txtSubjectIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveSubjectObj = function () {

		var obj = {

			sbid : $.trim($(settings.txtSubjectIdHidden).val()),
			name : $.trim($(settings.txtName).val()),
			description : $.trim($(settings.txtDescription).val()),
			brid : $('.brid').val()
		};

		return obj;
	}

	// checks for the duality of the name
	var isFieldValid = function() {

		var errorFlag = false;
		var name = settings.txtName;		// get the current subject name entered by the user
		var sbid = settings.txtSubjectIdHidden;		// hidden sbid
		var maxId = settings.txtMaxSubjectIdHidden;		// hidden max sbid
		var txtnameHidden = settings.txtNameHidden;		// hidden fee category name

		var subjectNames = new Array();
		// get all branch names from the hidden list
		$("#allSubjects option").each(function(){
			subjectNames.push($(this).text().trim().toLowerCase());
		});

		// if both values are not equal then we are in update mode
		if (sbid.val() !== maxId.val()) {

			$.each(subjectNames, function(index, elem){

				if (txtnameHidden.val().toLowerCase() !== elem.toLowerCase() && name.val().toLowerCase() === elem.toLowerCase()) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});

		} else {	// if both are equal then we are in save mode

			$.each(subjectNames, function(index, elem){

				if (name.val().trim().toLowerCase() === elem) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});
		}

		return errorFlag;
	}

	// saves the data into the database
	var save = function( subjectObj ) {

		$.ajax({
			url : base_url + 'index.php/subject/save',
			type : 'POST',
			data : { 'subjectDetail' : subjectObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Subject saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		
		$('#txtSubjectId').val('');
		$('#txtMaxSubjectIdHidden').val('');
		$('#txtSubjectIdHidden').val('');
		$('#txtName').val('');
		$('#txtNameHidden').val('');
		$('#description').val('');

		getMaxId();
		general.hideLoader();
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var txtName = $.trim($(settings.txtName).val());

		// remove the error class first
		$(settings.txtName).removeClass('inputerror');

		if ( txtName === '' ) {
			$(settings.txtName).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetch = function(sbid) {

		$.ajax({
			url : base_url + 'index.php/subject/fetchSubject',
			type : 'POST',
			data : { 'sbid' : sbid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(data) {

		$.each(data, function(index, elem){

			$(settings.txtSubjectId).val(elem.sbid);
			$(settings.txtSubjectIdHidden).val(elem.sbid);

			$(settings.txtName).val(elem.name);
			$(settings.txtNameHidden).val(elem.name);

			$(settings.txtDescription).val(elem.description);
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			// when save button is clicked
			$(settings.btnSave).on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});

			// when reset button is clicked
			$(settings.btnReset).on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			// when text is chenged inside the id textbox
			$(settings.txtSubjectId).on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $(settings.txtSubjectId).val().trim() !== "" ) {

						var sbid = $.trim($(settings.txtSubjectId).val());
						fetch(sbid);
					}
				}
			});

			// when edit button is clicked inside the table view
			$(settings.btnEditSubject).on('click', function(e) {
				e.preventDefault();				
				fetch($(this).data('sbid'));		// get the subject detail by id
			});

			getMaxId();
		},

		// makes the voucher ready to save
		initSave : function() {
			var subjectObj = getSaveSubjectObj();	// returns the subject detail object to save into database
			var isValid = validateSave();			// checks for the empty fields

			if (!isValid) {

				// check if the subject name is already used??	if false
				if ( !isFieldValid() ) {
					// save the data in to the database
					save( subjectObj );
				} else {	// if subject name is already used then show error
					alert("Subject name already used.");
				}
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$('#txtSubjectId').val('');
			$('#txtName').val('');
			$('#txtNameHidden').val('');
			$('#description').val('');

			getMaxId();
			general.setPrivillages();*/

			general.reloadWindow();
		}
	};
};

var subject = new Subject();
subject.init();