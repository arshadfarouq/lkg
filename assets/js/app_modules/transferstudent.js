var BatchUpdate = function() {

	var settings = {

		fromBranch_dropdown : $('.brid'),
		fromClass_dropdown : $('#fromClass_dropdown'),
		fromSection_dropdown : $('#fromSection_dropdown'),
		fromStudentData_table : $('.fromStudentData-table'),

		toBranch_dropdown : $('.brid'),
		toClass_dropdown : $('#toClass_dropdown'),
		toSection_dropdown : $('#toSection_dropdown'),
		toStudentData_table : $('.toStudentData-table'),

		// buttons
		btnSearch : $('.btnSearch'),
		btnFromReset : $('.btnFromReset'),
		btnToReset : $('.btnToReset'),
		btnPromoteAll : $('.btnPromoteAll'),
		btnDemoteAll : $('.btnDemoteAll'),
		btnUpdate : $('.btnUpdate')
	};

	// checks for the empty fields
	var validateSearch = function() {

		var errorFlag = false;
		var fromBranch_dropdown = $(settings.fromBranch_dropdown).val();
		var fromClass_dropdown = $.trim($(settings.fromClass_dropdown).val());
		var fromSection_dropdown = $.trim($(settings.fromSection_dropdown).val());

		// remove the error class first
		$(settings.fromBranch_dropdown).removeClass('inputerror');
		$(settings.fromClass_dropdown).removeClass('inputerror');
		$(settings.fromSection_dropdown).removeClass('inputerror');

		if ( fromBranch_dropdown === '' || fromBranch_dropdown === null ) {
			$(settings.fromBranch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( fromClass_dropdown === '' || fromClass_dropdown === null ) {
			$(settings.fromClass_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( fromSection_dropdown === '' || fromSection_dropdown === null ) {
			$(settings.fromSection_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// fetch all the sections based on the selected class name
	var fetchSectionNames = function( brid, claid, sec ) {

		// clear the previous data
		removeSectionOptions(sec);

		$.ajax({
			url : base_url + 'index.php/group/fetchSectionsByBranchAndClass',
			type : 'POST',
			async : false,
			data : { 'claid' : claid, 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if (data !== 'false') {
					populateSectionNames(data, sec);
				} else {
					removeSectionOptions(sec);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeSectionOptions = function(sec) {

		var dropdown = (sec === 'from') ? settings.fromSection_dropdown : settings.toSection_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateSectionNames = function( data, sec ) {

		var sectionNamesOptions = "";
		$.each(data, function( index, elem ) {

			sectionNamesOptions += "<option value='"+ elem.secid +"' data-claid='"+ elem.secid +"'>"+ elem.name +"</option>";
		});

		(sec === 'from') ? $(sectionNamesOptions).appendTo(settings.fromSection_dropdown) : $(sectionNamesOptions).appendTo(settings.toSection_dropdown);
	}

	// fetch all the classes based on the selected branch name
	var fetchClassNames = function( brid, sec ) {

		// removes the options from the select tag
		removeOptions(sec);

		$.ajax({
			url : base_url + 'index.php/group/fetchClassesByBranch',
			type : 'POST',
			async : false,
			data : { 'brid' : brid },
			dataType : 'JSON',
			success : function(data) {

				if ( data !== 'false' ) {
					populateClassNames( data, sec );
				} else {
					// removes the options from the select tag
					removeOptions(sec);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// removes the options from the select tag
	var removeOptions = function(sec) {

		var dropdown = (sec === 'from') ? fromClass_dropdown : toClass_dropdown;

		$(dropdown).children('option').each(function(){
			if ($(this).val() !== "") {
				$(this).remove();
			}
		});
	}

	var populateClassNames = function( data, sec ) {

		var classNamesOptions = "";
		$.each(data, function( index, elem ) {

			classNamesOptions += "<option value='"+ elem.claid +"' data-claid='"+ elem.claid +"'>"+ elem.name +"</option>";
		});

		(sec === 'from') ? $(classNamesOptions).appendTo(fromClass_dropdown) : $(classNamesOptions).appendTo(toClass_dropdown);
	}

	var search = function(brid, claid, secid) {
		
		console.log(brid+" "+ claid+" "+ secid);
		$.ajax({
			url : base_url + 'index.php/student/fetchStudentByBranchClassSection',
			type : 'POST',
			data : { 'brid' : brid, 'claid' : claid, 'secid' : secid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		// removes all rows
		$(settings.fromStudentData_table).find('tbody tr').remove();

		var counter = 0;
		var row = "";
		$.each(data, function(index, elem) {

			// add row to the datatable
			row += "<tr>"
				+"<td>"+ (counter+1) +"</td>"
				+"<td>"+ elem.rollno +"</td>"
				+"<td>"+ elem.name +"</td>"
				+"<td>"+ elem.category +"</td>"
				+"<td>"+ elem.feetype +"</td>"
				+"<td><a href='' class='btn btn-primary btnRowPromote' data-stdid='"+ elem.stdid +"'><span class='ion-android-checkmark'></span></a></td></tr>";
		});

		$(row).appendTo(settings.fromStudentData_table).find('tbody');

	}

	var promote_demoteRow = function( _this, sec) {

		var table = (sec === 'from') ? $(settings.fromStudentData_table) : $(settings.toStudentData_table);

		var to = (sec === 'from') ? $(settings.toStudentData_table) : $(settings.fromStudentData_table);

		var sr = _this.closest('tr').find('td').eq('0').text();
		var rollno = _this.closest('tr').find('td').eq('1').text();
		var name = _this.closest('tr').find('td').eq('2').text();
		var category = _this.closest('tr').find('td').eq('3').text();
		var feetype = _this.closest('tr').find('td').eq('4').text();
		var stdid = _this.closest('tr').find('td').eq('5').find('a').data('stdid');

		var _class = "";
		var icon = "";
		if (sec === 'from') {
			_class = 'btnRowDemote';
			icon = 'ion-android-close';
		} else {
			_class = 'btnRowPromote';
			icon = 'ion-android-checkmark';
		}

		row = "<tr>"
				+"<td>"+ sr +"</td>"
				+"<td>"+ rollno +"</td>"
				+"<td>"+ name +"</td>"
				+"<td>"+ category +"</td>"
				+"<td>"+ feetype +"</td>"
				+"<td><a href='' class='btn btn-primary "+ _class +"' data-stdid='"+ stdid +"'><span class='"+ icon +"'></span></a></td></tr>";

		var totbody = $(to).find('tbody');
		$(row).appendTo(totbody);
		_this.closest('tr').remove();
	}

	var promote_demote = function(sec) {

		var table = (sec === 'from') ? $(settings.fromStudentData_table) : $(settings.toStudentData_table);

		var rowsCount = $(table).find('tbody tr').length;

		if ( rowsCount > 0 ) {

			var to = (sec === 'from') ? $(settings.toStudentData_table) : $(settings.fromStudentData_table);

			var totbody = $(to).find('tbody');
			var rows = $(table).find('tbody').html();
			$(rows).appendTo(totbody);

			$(to).find('tbody tr').find('td a').each(function() {

				if (sec === 'from') {
					$(this).removeClass('btnRowPromote').addClass('btnRowDemote');
				} else {
					$(this).removeClass('btnRowDemote').addClass('btnRowPromote');
				}

				$(this).find('span').each(function() {

					if (sec === 'from') {
						$(this).removeClass('ion-android-checkmark').addClass('ion-android-close');
					} else {
						$(this).removeClass('ion-android-close').addClass('ion-android-checkmark');
					}
				});
			});

			$(table).find('tbody tr').remove();
		} else {

			var out = ( sec === 'from' ) ? 'promote' : 'demote';
			alert('No record to ' + out);
		}
	}

	var validateUpdate = function() {

		var errorFlag = false;
		var toBranch_dropdown = $(settings.toBranch_dropdown).val();
		var toClass_dropdown = $.trim($(settings.toClass_dropdown).val());
		var toSection_dropdown = $.trim($(settings.toSection_dropdown).val());

		// remove the error class first
		$(settings.toBranch_dropdown).removeClass('inputerror');
		$(settings.toClass_dropdown).removeClass('inputerror');
		$(settings.toSection_dropdown).removeClass('inputerror');

		if ( toBranch_dropdown === '' || toBranch_dropdown === null ) {
			$(settings.toBranch_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( toClass_dropdown === '' || toClass_dropdown === null ) {
			$(settings.toClass_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		if ( toSection_dropdown === '' || toSection_dropdown === null ) {
			$(settings.toSection_dropdown).addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var getUpdateStudentObj = function() {

		var students = [];

		$(settings.toStudentData_table).find('tbody tr').each(function() {
			
			var student = {

				stdid : $(this).closest('tr').find('td').eq('5').find('a').data('stdid'),
				brid : $(settings.toBranch_dropdown).val(),
				claid : $(settings.toClass_dropdown).val(),
				secid : $(settings.toSection_dropdown).val()
			};

			students.push(student);
		});

		return students;
	}

	var update = function(studentObj) {

		$.ajax({

			url : base_url + 'index.php/student/saveMultiple',
			type : 'POST',
			data : { 'studentsDetail' : studentObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === "true") {
					alert('An internal error occured while saving branch. Please try again.');
				} else {
					alert('Students promoted successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init : function() {

			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$(settings.btnUpdate).on('click', function(e) {

				e.preventDefault();
				self.initUpdate();
			});

			// when fetchSectionNames is changed
			$(settings.fromClass_dropdown).on('change', function() {

				var brid = $(settings.fromBranch_dropdown).val();
				var claid = $(settings.fromClass_dropdown).val();
				// clear tables
				$(settings.fromStudentData_table).find('tbody tr').remove();
				$(settings.toStudentData_table).find('tbody tr').remove();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid, 'from');
				}
			});

			$(settings.fromSection_dropdown).on('change', function() {

				// clear tables
				$(settings.fromStudentData_table).find('tbody tr').remove();
				$(settings.toStudentData_table).find('tbody tr').remove();
			});

			// when fetchSectionNames is changed
			$(settings.toClass_dropdown).on('change', function() {

				var brid = $(settings.toBranch_dropdown).val();
				var claid = $(settings.toClass_dropdown).val();

				if ((brid !== "" || brid !== null ) && (claid !== "" || claid !== null )) {
					fetchSectionNames(brid, claid, 'to');
				}
			});

			// when btnFromReset is clicked
			$(settings.btnFromReset).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.resetFrom();
			});

			// when btnFromReset is clicked
			$(settings.btnToReset).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.resetTo();
			});

			// when btnSave is clicked
			$(settings.btnSearch).on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.initSearch();
			});

			$(settings.btnPromoteAll).on('click', function(e) {
				e.preventDefault();

				promote_demote('from');
			});

			$(settings.btnDemoteAll).on('click', function(e) {
				e.preventDefault();

				promote_demote('to');
			});

			$(settings.fromStudentData_table).on('click', '.btnRowPromote', function(e) {
				e.preventDefault();

				promote_demoteRow( $(this) ,'from');
			});

			$(settings.toStudentData_table).on('click', '.btnRowDemote', function(e) {
				e.preventDefault();

				promote_demoteRow( $(this), 'to');
			});

			var brid = $(settings.toBranch_dropdown).val();
			fetchClassNames(brid, 'to');
			fetchClassNames(brid, 'from');

		},

		initUpdate : function() {

			var rowsCount = $(settings.toStudentData_table).find('tbody tr').length;

			if (rowsCount > 0) {

				var studentObj = getUpdateStudentObj();
				var error = validateUpdate();

				if (!error) {

					update(studentObj);
				} else {
					alert('Correct the errors...');
				}
			} else {
				alert('No record found to update.');
			}
		},

		initSearch : function() {
			var isValid = validateSearch();

			if (!isValid) {

				var brid = $(settings.fromBranch_dropdown).val();
				var claid = $(settings.fromClass_dropdown).val();
				var secid = $(settings.fromSection_dropdown).val();

				search(brid, claid, secid);
			} else {
				alert('Correct the errors...');
			}
		},

		resetFrom : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.fromBranch_dropdown).val('');
			// $(settings.fromClass_dropdown).val('');
			// $(settings.fromSection_dropdown).val('');

			// // removes all rows
			// $(settings.fromStudentData_table).find('tbody tr').remove();
			general.reloadWindow();
		},

		resetTo : function() {

			// $('.inputerror').removeClass('inputerror');
			// $(settings.toBranch_dropdown).val('');
			// $(settings.toClass_dropdown).val('');
			// $(settings.toSection_dropdown).val('');

			// // removes all rows
			// $(settings.toStudentData_table).find('tbody tr').remove();
			general.reloadWindow();
		}
	}

};

var batchUpdate = new BatchUpdate();
batchUpdate.init();