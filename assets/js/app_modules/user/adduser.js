var PrivillagesGroup = function(){

	var getMaxId = function() {

		$.ajax({
			url: base_url + 'index.php/user/getMaxId',
			type: 'POST',
			dataType: 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#txtIdHidden').val(data);
			}, error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveObj = function() {

		var data = {};
		data.uid = $.trim($('#txtIdHidden').val());
		data.uname = $.trim($('#txtUsername').val());
		data.pass = $.trim($('#txtPassowrd').val());
		data.fullname = $.trim($('#txtFullName').val());
		data.email = $.trim($('#txtEmail').val());
		data.mobile = $.trim($('#txtMobileNo').val());
		data.rgid = $.trim($('#role_dropdown').val());
		data.type = $.trim($('#usertype_dropdown').val());
		data.brid = $.trim($('#branch_dropdown').val());

		return data;
	}

	var validateSave = function() {

		var errorFlag = false;
		var name = $.trim($('#txtUsername').val());
		var pass = $.trim($('#txtPassowrd').val());
		var role = $.trim($('#role_dropdown').val());
		var type = $.trim($('#usertype_dropdown').val());

		// remove the error class first
		$('#txtName').removeClass('inputerror');
		$('#usertype_dropdown').removeClass('inputerror');

		if ( name === '' || name === null ) {
			$('#txtUsername').addClass('inputerror');
			errorFlag = true;
		}
		if ( pass === '' || pass === null ) {
			$('#txtPassowrd').addClass('inputerror');
			errorFlag = true;
		}
		if ( role === '' || role === null ) {
			$('#role_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( type === '' || type === null ) {
			$('#usertype_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var save = function(obj) {

		$.ajax({
			url: base_url + 'index.php/user/save',
			type: 'POST',
			data: {'user': obj},
			dataType: 'JSON',
			success: function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('User saved successfully.');
					resetVoucher();
					// general.reloadWindow();
				}
			}, error: function(xhr, status, error) {
				// console.log(xhr.responseText);
					general.error_handling(xhr,status);
			}
		});
	}

	var resetVoucher = function() {

		general.showLoader();
		$('.inputerror').removeClass('inputerror');
		
		$('#txtId').val('');
		$('#txtMaxIdHidden').val('');
		$('#txtIdHidden').val('');
		$('#txtUsername').val('');
		$('#txtPassowrd').val('');
		$('#txtFullName').val('');
		$('#txtMobileNo').val('');
		$('#role_dropdown').val('');
		$('#txtEmail').val('');
		$('#branch_dropdown').val('');
		$('#usertype_dropdown').val('');

		getMaxId();
		general.hideLoader();
	}

	var fetch = function(uid) {

		$.ajax({
			url : base_url + 'index.php/user/fetch',
			type : 'POST',
			data : { 'uid' : uid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
					try {
						$('.btn-openmodal').trigger('click');
					} catch(e) {
						console.log(e);
					}
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$('#txtIdHidden').val(data.uid);
		$('#txtId').val(data.uid);
		$('#txtUsername').val(data.uname);
		$('#txtPassowrd').val(data.pass);
		$('#txtFullName').val(data.fullname);
		$('#txtEmail').val(data.email);
		$('#txtMobileNo').val(data.mobile);
		$('#role_dropdown').val(data.rgid);
		$('#usertype_dropdown').val(data.type);
		$('#branch_dropdown').val(data.brid);
	}

	return {

		init: function() {
			this.bindUI();
		},

		bindUI: function() {
			var self = this;

			$('.btnSave').on('click', function(e) {
				e.preventDefault();
				self.initSave();
			});

			$('#txtId').on('keypress', function(e) {

				if (e.keyCode == 13) {
					var uid = $('#txtId').val();
					fetch(uid);
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btn-edit-user').on('click', function(e){
				e.preventDefault();
				fetch($(this).data('uid'));
			});

			getMaxId();
		},

		initSave: function() {

			var obj = getSaveObj();
			var error = validateSave();

			if (!error) {
				save(obj);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher: function() {
			general.reloadWindow();
		}
	}

};

var privillagesGroup = new PrivillagesGroup();
privillagesGroup.init();