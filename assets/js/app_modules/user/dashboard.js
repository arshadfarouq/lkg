var Dashboard = function() {

	var fetch = function(staid) {

		$.ajax({
			url : base_url + 'index.php/attendance/fetchStaffForTimeInOut',
			type : 'POST',
			data : { 'staid' : staid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No a valid id entered!');
				} else {
					$('#timeInOutModelBtn').trigger('click');
					populateStaffData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateStaffData = function(staff) {

		$('#staid').text(staff[0]['staid']);
		$('#did').text(staff[0]['did']);
		$('#shid').text(staff[0]['shid']);
		$('#staffdept').text(staff[0]['dept_name']);
		$('#staffname').text(staff[0]['name']);
		$('#staffcnic').text(staff[0]['cnic']);
		$('#staffdesignation').text(staff[0]['type']);
		$('#staffshift').text(staff[0]['shift_name']);

		// set image
		if (staff[0]['photo'] !== "") {
			$('#staffImageDisplay').attr('src', base_url + '/assets/uploads/staff/' + staff[0]['photo']);
		} else {
			$('#staffImageDisplay').attr('src', base_url + '/assets/img/student.jpg');
		}

		$.each(staff, function(index, elem) {
			if (elem.tin != "00:00:00") {
				$('#stafftin').text(elem.tin);
			}
			if (staff.length != 1) {
				if (elem.tout != "00:00:00") {
					$('#stafftout').text(elem.tout);
				}
			}
			if (staff.length == 1) {
				$('#stafftout').closest('p').addClass('hide');
			} else {
				$('#stafftout').closest('p').removeClass('hide');
			}
		});
	}

	var search = function() {

		$.ajax({
			url : base_url + 'index.php/attendance/dailyAttendanceReport',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#atnd-table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No record found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}

		});
	}

	var populateData = function(data) {

		$.each(data, function(index, elem) {
			row = 	"<tr>"+
						"<td>"+ elem.branch_name +"</td>"+
						"<td>"+ elem.class_name +"</td>"+
						"<td>"+ elem.section_name +"</td>"+
						"<td>"+ elem.total_students +"</td>"+
						"<td>"+ elem.absent +"</td>"+
						"<td>"+ elem.present +"</td>"+
						"<td>"+ elem.leave +"</td></tr>";
			$(row).appendTo('#atnd-table tbody');
		});
	}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			$('#txtStaffId').on('keypress', function(e) {
				if (e.keyCode === 13) {
					if ($('#txtStaffId').val().trim() !== "" ) {

						var staid = $.trim($('#txtStaffId').val());
						fetch(staid);
					}
				}
			});

			$('#TimeInOutModel').on('hidden.bs.modal', function () {
				$('#txtStaffId').val('');
			});

			// search();
		}		
	};
};

var dashboard = new Dashboard();
dashboard.init();